/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.controller;

import com.twc.wms.dao.QSELLERDao;
import com.twc.wms.dao.QSETDao;
import com.twc.wms.dao.QdestDao;
import com.twc.wms.dao.QdestXMSDao;
import com.twc.wms.dao.WHDao;
import com.twc.wms.dao.XMSWIDETAILDao;
//import com.twc.wms.dao.XMSWIDETAILDao;
import com.twc.wms.dao.XMSWIHEADDao;
import com.twc.wms.entity.QSELLER;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.twc.wms.entity.QSET;
import com.twc.wms.entity.Qdest;
import com.twc.wms.entity.WH;
import com.twc.wms.entity.XMSWIDETAIL;
//import com.twc.wms.entity.XMSWIDETAIL;
import com.twc.wms.entity.XMSWIHEAD;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpSession;

/**
 *
 * @author wien
 */
public class EditControllerXMS800 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static final String PAGE_VIEW = "../views/editXMS800.jsp";

    public EditControllerXMS800() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        request.setAttribute("PROGRAMNAME", "XMS800/C");
        String forward = "";
        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "Summary Truck by Internal(Production). Display");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        String qno = request.getParameter("qno");
        request.setAttribute("qno", qno);

        XMSWIHEAD head = new XMSWIHEADDao().findByQno(qno);

        String shipDate = head.getXMSWIHSPDT();
        String dest = head.getXMSWIHDEST();
        String round = head.getXMSWIHROUND();
        String lino = head.getXMSWIHLICENNO();
        String wh = head.getXMSWIHWHS();

        WH uawh = new WHDao().findByUid(wh);
        List<WH> pList2 = new WHDao().findAll();
        request.setAttribute("MCList", pList2);
        request.setAttribute("wh", wh);
        request.setAttribute("whn", uawh.getName());

        List<Qdest> linoList = new XMSWIHEADDao().findLino(wh, shipDate);
        request.setAttribute("linoList", linoList);

        List<Qdest> roundList = new XMSWIHEADDao().findRound(wh, shipDate);
        request.setAttribute("roundList", roundList);

        List<Qdest> shipDateList = new XMSWIHEADDao().findShipDate();
        request.setAttribute("shipDateList", shipDateList);

        Qdest qd = new QdestDao().findByCod(dest);
        request.setAttribute("dest", dest);

        request.setAttribute("round", round);
        request.setAttribute("lino", lino);
        String userid = request.getParameter("userid");

        List<Qdest> destList = new XMSWIHEADDao().findByType(wh, shipDate);
        request.setAttribute("destList", destList);
        request.setAttribute("shipDate", shipDate);
        String shipDate2 = shipDate;
        if (shipDate != null) {
            shipDate2 = shipDate.split("-")[2] + "/" + shipDate.split("-")[1] + "/" + shipDate.split("-")[0];
        }
        request.setAttribute("shipDate2", shipDate2);

        List<XMSWIDETAIL> detList = new ArrayList<XMSWIDETAIL>();
        detList = new XMSWIDETAILDao().findByQno(qno);

        request.setAttribute("destn", qd.getDesc());

        int XMSWIDBAGsum = 0;
        int XMSWIDROLLsum = 0;
        int XMSWIDBOXsum = 0;
        int XMSWIDPCSsum = 0;
        int XMSWIDTOTsum = 0;

        if (!detList.isEmpty()) {
            request.setAttribute("driver", detList.get(0).getXMSWIDDRIVER());
            request.setAttribute("follower", detList.get(0).getXMSWIDFOLLOWER());

            for (int i = 0; i < detList.size(); i++) {

                XMSWIDBAGsum += Integer.parseInt(detList.get(i).getXMSWIDBAG().trim().equals("-") ? "0" : detList.get(i).getXMSWIDBAG().replace(",", ""));
                XMSWIDROLLsum += Integer.parseInt(detList.get(i).getXMSWIDROLL().trim().equals("-") ? "0" : detList.get(i).getXMSWIDROLL().replace(",", ""));
                XMSWIDBOXsum += Integer.parseInt(detList.get(i).getXMSWIDBOX().trim().equals("-") ? "0" : detList.get(i).getXMSWIDBOX().replace(",", ""));
                XMSWIDPCSsum += Integer.parseInt(detList.get(i).getXMSWIDPCS().trim().equals("-") ? "0" : detList.get(i).getXMSWIDPCS().replace(",", ""));
                XMSWIDTOTsum += Integer.parseInt(detList.get(i).getXMSWIDTOT().trim().equals("-") ? "0" : detList.get(i).getXMSWIDTOT().replace(",", ""));

            }
        }

        request.setAttribute("XMSWIDBAGsum", XMSWIDBAGsum);
        request.setAttribute("XMSWIDROLLsum", XMSWIDROLLsum);
        request.setAttribute("XMSWIDBOXsum", XMSWIDBOXsum);
        request.setAttribute("XMSWIDPCSsum", XMSWIDPCSsum);
        request.setAttribute("XMSWIDTOTsum", XMSWIDTOTsum);

        request.setAttribute("detList", detList);
        if (XMSWIDTOTsum > 0) {
            request.setAttribute("added", "");
        } else {
            request.setAttribute("added", "display: none;");
        }

        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        request.setAttribute("PROGRAMNAME", "XMS800/C");
        String forward = "";
        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "Summary Truck by Internal(Production). Display");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        String qno = request.getParameter("qno");
        request.setAttribute("qno", qno);
        String shipDate = request.getParameter("shipDate");
        String dest = request.getParameter("dest");
        String round = request.getParameter("round");
        String lino = request.getParameter("lino");

        String wh = request.getParameter("wh");
        WH uawh = new WHDao().findByUid(wh);
        List<WH> pList2 = new WHDao().findAll();
        request.setAttribute("MCList", pList2);
        request.setAttribute("wh", wh);
        request.setAttribute("whn", uawh.getName());

        List<Qdest> linoList = new XMSWIHEADDao().findLino(wh, shipDate);
        request.setAttribute("linoList", linoList);

        List<Qdest> roundList = new XMSWIHEADDao().findRound(wh, shipDate);
        request.setAttribute("roundList", roundList);

        List<Qdest> shipDateList = new XMSWIHEADDao().findShipDate();
        request.setAttribute("shipDateList", shipDateList);

        Qdest qd = new QdestDao().findByCod(dest);
        request.setAttribute("dest", dest);

        request.setAttribute("round", round);
        request.setAttribute("lino", lino);
        String userid = request.getParameter("userid");

        List<Qdest> destList = new XMSWIHEADDao().findByType(wh, shipDate);
        request.setAttribute("destList", destList);
        request.setAttribute("shipDate", shipDate);
        String shipDate2 = shipDate;
        if (shipDate != null) {
            shipDate2 = shipDate.split("-")[2] + "/" + shipDate.split("-")[1] + "/" + shipDate.split("-")[0];
        }
        request.setAttribute("shipDate2", shipDate2);

        List<XMSWIDETAIL> detList = new ArrayList<XMSWIDETAIL>();

        detList = new XMSWIDETAILDao().findFromOT(lino, shipDate, wh, round);

        int XMSWIDBAGsum = 0;
        int XMSWIDROLLsum = 0;
        int XMSWIDBOXsum = 0;
        int XMSWIDPCSsum = 0;
        int XMSWIDTOTsum = 0;

        if (!detList.isEmpty()) {
            request.setAttribute("driver", detList.get(0).getXMSWIDDRIVER());
            request.setAttribute("follower", detList.get(0).getXMSWIDFOLLOWER());

            new XMSWIDETAILDao().delete(qno);

            for (int i = 0; i < detList.size(); i++) {

                XMSWIDETAIL detail = new XMSWIDETAIL();

                detail.setXMSWIDQNO(qno);
                detail.setXMSWIDLINE(Integer.toString(i + 1));
                detail.setXMSWIDDEST(detList.get(i).getXMSWIDDEST());
                detail.setXMSWIDDESTN(detList.get(i).getXMSWIDDESTN());
                detail.setXMSWIDDRIVER(detList.get(i).getXMSWIDDRIVER());
                detail.setXMSWIDFOLLOWER(detList.get(i).getXMSWIDFOLLOWER());
                detail.setXMSWIDBAG(detList.get(i).getXMSWIDBAG().replace("-", "0"));
                detail.setXMSWIDROLL(detList.get(i).getXMSWIDROLL().replace("-", "0"));
                detail.setXMSWIDBOX(detList.get(i).getXMSWIDBOX().replace("-", "0"));
                detail.setXMSWIDPCS(detList.get(i).getXMSWIDPCS().replace("-", "0"));
                detail.setXMSWIDTOT(detList.get(i).getXMSWIDTOT().replace("-", "0"));
                detail.setXMSWIDUSER(userid);

                new XMSWIDETAILDao().add(detail);

                XMSWIDBAGsum += Integer.parseInt(detList.get(i).getXMSWIDBAG().trim().equals("-") ? "0" : detList.get(i).getXMSWIDBAG().replace(",", ""));
                XMSWIDROLLsum += Integer.parseInt(detList.get(i).getXMSWIDROLL().trim().equals("-") ? "0" : detList.get(i).getXMSWIDROLL().replace(",", ""));
                XMSWIDBOXsum += Integer.parseInt(detList.get(i).getXMSWIDBOX().trim().equals("-") ? "0" : detList.get(i).getXMSWIDBOX().replace(",", ""));
                XMSWIDPCSsum += Integer.parseInt(detList.get(i).getXMSWIDPCS().trim().equals("-") ? "0" : detList.get(i).getXMSWIDPCS().replace(",", ""));
                XMSWIDTOTsum += Integer.parseInt(detList.get(i).getXMSWIDTOT().trim().equals("-") ? "0" : detList.get(i).getXMSWIDTOT().replace(",", ""));

            }
        }

        request.setAttribute("XMSWIDBAGsum", XMSWIDBAGsum);
        request.setAttribute("XMSWIDROLLsum", XMSWIDROLLsum);
        request.setAttribute("XMSWIDBOXsum", XMSWIDBOXsum);
        request.setAttribute("XMSWIDPCSsum", XMSWIDPCSsum);
        request.setAttribute("XMSWIDTOTsum", XMSWIDTOTsum);

        boolean added = false;

        if (XMSWIDTOTsum > 0) {
            added = true;
//            XMSWIHEAD head = new XMSWIHEAD(qno, shipDate, dest, round, lino, detList.get(0).getXMSWIDDRIVER(), detList.get(0).getXMSWIDFOLLOWER(), Integer.toString(XMSWIDTOT1sum), Integer.toString(XMSWIDTOT2sum), Integer.toString(XMSWIDTOT3sum), Integer.toString(XMSWIDTOTsum), userid);
//            if (new XMSWIHEADDao().add(head)) {
//                added = true;
//                request.setAttribute("qno", qno);
//            } else {
//                new XMSWIDETAILDao().delete(qno);
//            }
        }

        request.setAttribute("detList", detList);
        if (added) {
            request.setAttribute("added", "");
        } else {
            request.setAttribute("added", "display: none;");
        }

        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);

    }
}
