/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.controller;

import com.twc.wms.dao.ISM800Dao;
import com.twc.wms.entity.ISMSDWH;
import java.io.IOException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author 93176
 */
public class DisplayControllerISM800 extends HttpServlet {

    private static final String PAGE_VIEW = "../views/displayISM800.jsp";

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        request.setAttribute("PROGRAMNAME", "ISM800");
        String forward = "";
        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "Sales Order Download. Process");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
//        
//        String uid = request.getParameter("uid");
        List<ISMSDWH> aheadList = new ISM800Dao().findAll();
        request.setAttribute("aheadList", aheadList);
//    ********
        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

}
