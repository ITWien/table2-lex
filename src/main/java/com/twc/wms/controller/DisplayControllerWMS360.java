/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.controller;

import com.twc.wms.dao.QIDETAILDao;
import com.twc.wms.dao.WHDao;
import com.twc.wms.entity.WH;
import com.twc.wms.dao.QIHEADDao;
import com.twc.wms.dao.QdestDao;
import com.twc.wms.entity.MSSMATN;
import com.twc.wms.entity.QIDETAIL;
import com.twc.wms.entity.Qdest;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author wien
 */
public class DisplayControllerWMS360 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static final String PAGE_VIEW = "../views/displayWMS360.jsp";

    public DisplayControllerWMS360() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        request.setAttribute("PROGRAMNAME", "WMS360");
        String forward = "";
        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "RM Issue. Destination Received");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
//    ********
        String wh = request.getParameter("wh");
        String dest = request.getParameter("dest");
        String shipDate = request.getParameter("shipDate");
        String round = request.getParameter("round");

        WHDao daofwhn = new WHDao();
        WH uawh = daofwhn.findByUid(wh);

        QdestDao daodes = new QdestDao();
        Qdest uade = daodes.findByCod(dest);

        if (wh != null) {
            QIDETAILDao daodt = new QIDETAILDao();
            List<QIDETAIL> detList = daodt.findDetail360(wh, dest, shipDate, round);
            request.setAttribute("detList", detList);
        }

        request.setAttribute("wh", wh);
        request.setAttribute("whn", uawh.getName());
        request.setAttribute("dest", dest);
        request.setAttribute("destn", uade.getDesc());
        request.setAttribute("shipDate", shipDate);
        request.setAttribute("round", round);
        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("PROGRAMNAME", "WMS360");
        String forward = "";
        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "RM Issue. Destination Received");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
//    ********
        String wh = request.getParameter("wh");
        String dest = request.getParameter("dest");
        String ship = request.getParameter("ship");
        String shipDate = "";
        if (ship != null) {
            shipDate = ship.substring(0, 4) + "-" + ship.substring(4, 6) + "-" + ship.substring(6, 8);
        }
        String round = request.getParameter("round");

        WHDao daofwhn = new WHDao();
        WH uawh = daofwhn.findByUid(wh);

        QdestDao daodes = new QdestDao();
        Qdest uade = daodes.findByCod(dest);

        QIDETAILDao daodt = new QIDETAILDao();
        List<QIDETAIL> detList = daodt.findDetail360(wh, dest, shipDate, round);
        request.setAttribute("detList", detList);

        request.setAttribute("wh", wh);
        request.setAttribute("whn", uawh.getName());
        request.setAttribute("dest", dest);
        request.setAttribute("destn", uade.getDesc());
        request.setAttribute("shipDate", shipDate);
        request.setAttribute("round", round);
        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);
    }

}
