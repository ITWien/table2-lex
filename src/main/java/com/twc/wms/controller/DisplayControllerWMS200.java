/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.controller;

import com.twc.wms.dao.WMSMASDDAO;
import com.twc.wms.dao.WMSMASHDAO;
import com.twc.wms.dao.WMSMASMDAO;
import com.twc.wms.dao.WMSMATNDAO;
import com.twc.wms.dao.WarehouseDao;
import com.twc.wms.entity.WMSMASD;
import com.twc.wms.entity.WMSMASH;
import com.twc.wms.entity.Warehouse;
import com.twc.wms.utils.ThaiConverter;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FileUtils;

/**
 *
 * @author wien
 */
public class DisplayControllerWMS200 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static final String PAGE_VIEW = "../views/displayWMS200.jsp";

    public DisplayControllerWMS200() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        request.setAttribute("PROGRAMNAME", "WMS200");
        String forward = "";
        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "Stock RM");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
//    ********
        WarehouseDao dao = new WarehouseDao();
        List<Warehouse> pList = dao.selectWarehouse();
        request.setAttribute("WHList", pList);

        String a = "";
        String s = "";

        if (request.getParameter("a") != null) {
            a = request.getParameter("a");
        }
        if (request.getParameter("s") != null) {
            s = request.getParameter("s");
        }

        if (s.equalsIgnoreCase("success")) {
            request.setAttribute("statusRe", "<b>Update successful.</b>");
        } else if (s.equalsIgnoreCase("fail")) {
            request.setAttribute("statusRe", "<b>Update fail.</b>");
        }

        if (a.equalsIgnoreCase("list")) {
            String POFG = request.getParameter("POFG");
            request.setAttribute("POFG", POFG);
            request.setAttribute("maxGroup", "Recent group set : " + new WMSMASHDAO().getMaxGroup(POFG));

            List<WMSMASH> HDList = new WMSMASHDAO().getEmptyGroup(POFG);

            request.setAttribute("HDList", HDList);

            if (!HDList.isEmpty()) {
                request.setAttribute("POFG", HDList.get(0).getPOFG());
            }

        } else if (a.equalsIgnoreCase("update")) {
            String POFG = request.getParameter("POFG");
            String GRPNO = request.getParameter("GRPNO");
            String WHS = request.getParameter("WHS");
            if (new WMSMASHDAO().updateGroupSet(GRPNO, POFG, WHS)) {
                response.setHeader("Refresh", "0;/TABLE2/WMS200/display?s=success");
            } else {
                response.setHeader("Refresh", "0;/TABLE2/WMS200/display?s=fail");
            }

        }

        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        final ServletContext servletContext = request.getSession().getServletContext();
        final boolean isMultipart = ServletFileUpload.isMultipartContent(request);

        List<WMSMASH> HDList = new ArrayList<WMSMASH>();

        if (isMultipart) {
            final FileItemFactory factory = (FileItemFactory) new DiskFileItemFactory();
            final ServletFileUpload upload = new ServletFileUpload(factory);
            try {
                final List<FileItem> multiparts = (List<FileItem>) upload.parseRequest(request);
                for (final FileItem item : multiparts) {
                    if (!item.isFormField()) {
                        final String nameIn = "FileIn.txt";
//                        final String nameOut = "FileOut.txt";
                        final File fileIn = new File(servletContext.getRealPath("resources") + File.separator + nameIn);
//                        final File fileOut = new File(servletContext.getRealPath("resources") + File.separator + nameOut);
                        item.write(fileIn);
                        final byte[] buf = new byte[4096];
                        final FileInputStream fis = new FileInputStream(fileIn);
                        try {
                            final ThaiConverter convert = new ThaiConverter();
                            final FileInputStream fis2 = new FileInputStream(fileIn);
                            final byte[] contents = new byte[fis2.available()];
                            fis2.read(contents, 0, contents.length);
                            final String asString = new String(contents);
                            final byte[] newBytes = asString.getBytes();
//                            final FileOutputStream fos = new FileOutputStream(fileOut);
//                            fos.write(newBytes);
                            fis2.close();
//                            fos.close();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

//                        File file = new File(servletContext.getRealPath("resources") + File.separator + nameOut);
//                        String content = FileUtils.readFileToString(file, "WINDOWS-874");
//                        FileUtils.write(file, content, "UTF-8");
                        final InputStream stream = servletContext.getResourceAsStream("resources" + File.separator + nameIn);
                        if (stream == null) {
                            continue;
                        }
                        final InputStreamReader isReader = new InputStreamReader(stream, "UTF-8");
                        final BufferedReader bfReader = new BufferedReader(isReader);
                        String text = "";
                        int cnt = 0;

                        while ((text = bfReader.readLine()) != null) {
                            String[] splitText = text.split("\\|");

                            if (splitText[0].contains("H") || splitText[0].contains("h")) {
                                WMSMASH h = new WMSMASH();
                                WMSMASH sh = new WMSMASH();

                                h.setLINETYP(splitText[0]);
                                h.setVBELN(splitText[1]);
                                sh.setVBELN(splitText[1]);

                                String date1 = "";
                                if (!splitText[2].trim().equals("")) {
                                    date1 = splitText[2].split("\\.")[2]
                                            + "-" + splitText[2].split("\\.")[1]
                                            + "-" + splitText[2].split("\\.")[0];
                                }
                                h.setAUDAT(date1);

                                String date1s = "";
                                if (!splitText[2].trim().equals("")) {
                                    date1s = splitText[2].split("\\.")[0]
                                            + "-" + splitText[2].split("\\.")[1]
                                            + "-" + splitText[2].split("\\.")[2];
                                }
                                sh.setAUDAT(date1s);

                                h.setAUART(splitText[3]);
                                h.setVTWEG(splitText[4]);
                                h.setBWART(splitText[5]);
                                h.setKUNNR(splitText[6]);

                                ThaiConverter tcNAME1 = new ThaiConverter();
                                h.setNAME1(tcNAME1.toUnicode(splitText[7]));

                                ThaiConverter tcNAME2 = new ThaiConverter();
                                h.setNAME2(tcNAME2.toUnicode(splitText[8]));

                                ThaiConverter tcADDR1 = new ThaiConverter();
                                h.setADDR1(tcADDR1.toUnicode(splitText[9]));

                                ThaiConverter tcADDR2 = new ThaiConverter();
                                h.setADDR2(tcADDR2.toUnicode(splitText[10]));

                                h.setBSTKD(splitText[11]);
                                sh.setBSTKD(splitText[11]);
                                h.setSUBMI(splitText[12]);
                                h.setMATLOT(splitText[13]);
                                h.setTAXNO(splitText[14]);
                                h.setBRANCH01(splitText[15]);

                                String date2 = "";
                                if (!splitText[16].trim().equals("")) {
                                    date2 = splitText[16].split("\\.")[2]
                                            + "-" + splitText[16].split("\\.")[1]
                                            + "-" + splitText[16].split("\\.")[0];
                                }

                                h.setDATUM(date2 + " " + splitText[17]);

                                h.setSTYLE(splitText[13].split("(\\#)|(\\/)")[0].trim());
                                h.setCOLOR(splitText[13].split("(\\#)|(\\/)")[2].trim());
                                h.setLOT(splitText[13].split("(\\#)|(\\/)")[1].trim());
                                h.setAMTFG(splitText[13].split("(\\#)|(\\/)")[3].trim());

                                h.setPOFG(splitText[22]);
                                sh.setPOFG(splitText[22]);

                                h.setGRPNO("");
                                h.setMWHSE("");

                                HDList.add(sh);

                                WMSMASHDAO daoH = new WMSMASHDAO();
                                if (!daoH.check(h).equals("f")) {
                                    WMSMASHDAO daoHadd = new WMSMASHDAO();
                                    daoHadd.add(h);
                                }

                                WMSMASDDAO daoD = new WMSMASDDAO();
                                cnt = Integer.parseInt(daoD.findISSUENO(h.getVBELN())) + 1;

                            } else if (splitText[0].contains("D") || splitText[0].contains("d")) {
                                WMSMASD d = new WMSMASD();

                                d.setLINETYP(splitText[0]);
                                d.setVBELN(splitText[1]);
                                d.setPOSNR(splitText[2]);
                                d.setMATNR(splitText[3]);
                                d.setARKTX(splitText[4]);
                                d.setWERKS(splitText[5]);
                                d.setBWTAR(splitText[6]);
                                d.setLGORT(splitText[7]);
                                d.setKWMENG(splitText[8].replace(",", ""));
                                d.setVRKME(splitText[9]);
                                d.setPRICE(splitText[10].replace(",", ""));
                                d.setLGPBE(splitText[11]);

                                ThaiConverter tcMATCNAME = new ThaiConverter();
                                d.setMATCNAME(tcMATCNAME.toUnicode(splitText[12]));

                                d.setMATWID(splitText[14]);
                                d.setMATCOM(splitText[15]);
                                d.setUNR01(splitText[16].replace(",", ""));
                                d.setUNR03(splitText[17].replace(",", ""));
                                d.setISSUENO(Integer.toString(cnt));

                                String lastIT = splitText[19];

                                WMSMASDDAO daoD = new WMSMASDDAO();
                                if (daoD.check(d).equals("f")) {
                                    WMSMASDDAO daoD2 = new WMSMASDDAO();
                                    float f = Float.parseFloat(daoD2.findKWMENG(d));
                                    Calendar localCalendar;
                                    if (f != Float.parseFloat(d.getKWMENG())) {
                                        localCalendar = Calendar.getInstance();
                                        WMSMASDDAO daoD3 = new WMSMASDDAO();
                                        daoD3.addFact(d, f, "0 - Changed", localCalendar.getTime(), Integer.parseInt(new WMSMASDDAO().findLineIssueNo(d)));
                                    }

                                    new WMSMASDDAO().update(d);
                                    new WMSMASMDAO().update(d);
//                                    new WMSMATNDAO().update(d);

                                    if (!lastIT.trim().equals("")) {
                                        localCalendar = Calendar.getInstance();
                                        new WMSMASDDAO().addFact(d, 0.0F, "1 - Deleted", localCalendar.getTime(), Integer.parseInt(new WMSMASDDAO().findLineIssueNo(d)));
                                        new WMSMASDDAO().deleteDetail(d);
                                    }

                                } else {
                                    if (lastIT.trim().equals("")) {
                                        new WMSMASDDAO().add(d);
                                    }

                                    if (new WMSMASMDAO().checkDuplicate(d).equals("f")) {
                                        new WMSMASMDAO().update(d);
                                    } else {
                                        new WMSMASMDAO().add(d);
                                    }

//                                    if (new WMSMATNDAO().checkDuplicate(d).equals("f")) {
//                                        new WMSMATNDAO().update(d);
//                                    } else {
//                                        new WMSMATNDAO().add(d);
//                                    }
                                }
                            }

                        }

                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        request.setAttribute("PROGRAMNAME", "WMS200");
        String forward = "";
        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "Stock RM");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
//    ********

//        for (int i = 0; i < HDList.size(); i++) {
//            System.out.println(HDList.get(i).getVBELN());
//            System.out.println(HDList.get(i).getAUDAT());
//            System.out.println(HDList.get(i).getBSTKD());
//            System.out.println(HDList.get(i).getPOFG());
//
//        }
        request.setAttribute("HDList", HDList);
        if (!HDList.isEmpty()) {
            request.setAttribute("POFG", HDList.get(0).getPOFG());
        }

        WarehouseDao dao = new WarehouseDao();
        List<Warehouse> pList = dao.selectWarehouse();
        request.setAttribute("WHList", pList);

        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);

    }

}
