/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.controller;

import com.twc.wms.dao.PGMUDao;
import com.twc.wms.dao.QUSMTCTRLDao;
import com.twc.wms.dao.UserDao;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import com.twc.wms.entity.PGMU;
import com.twc.wms.entity.UserAuth;

/**
 *
 * @author wien
 */
public class NMatControllerWMS009 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static final String PAGE_VIEW = "../views/NmatWMS009.jsp";

    public NMatControllerWMS009() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("PROGRAMNAME", "WMS009/CP");
        String forward = "";
        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "User Authorization Connect Mat Control. Display");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        String uid = request.getParameter("uid");

        QUSMTCTRLDao dao = new QUSMTCTRLDao();
        List<PGMU> pList = dao.findAll(uid);

        UserDao dao1 = new UserDao();
        List<UserAuth> pList1 = dao1.findAll();

        UserDao dao2 = new UserDao();
        UserAuth ua = dao2.findByUid(uid);

        request.setAttribute("id", uid);
        request.setAttribute("name", ua.getName());

        RequestDispatcher view = request.getRequestDispatcher(forward);
        request.setAttribute("UAList", pList);
        request.setAttribute("MCList", pList1);
        view.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");

        String uid = request.getParameter("user");

        request.setAttribute("PROGRAMNAME", "WMS009/CP");
        String forward = "";
        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "User Authorization Connect Mat Control. Display");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        QUSMTCTRLDao dao = new QUSMTCTRLDao();
        List<PGMU> pList = dao.findAll(uid);

        UserDao dao1 = new UserDao();
        List<UserAuth> pList1 = dao1.findAll();

        UserDao dao2 = new UserDao();
        UserAuth ua = dao2.findByUid(uid);

        request.setAttribute("id", uid);
        request.setAttribute("name", ua.getName());

        RequestDispatcher view = request.getRequestDispatcher(forward);
        request.setAttribute("UAList", pList);
        request.setAttribute("MCList", pList1);
        view.forward(request, response);
    }

}
