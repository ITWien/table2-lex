/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.controller;

import com.twc.wms.dao.QIDETAILDao;
import com.twc.wms.dao.QdestDao;
import com.twc.wms.dao.WHDao;
import com.twc.wms.entity.QIDETAIL;
import com.twc.wms.entity.Qdest;
import com.twc.wms.entity.WH;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRResultSetDataSource;
import net.sf.jasperreports.engine.JasperRunManager;

/**
 *
 * @author wien
 */
public class Print2ControllerWMS350 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static final String FILE_PATH = "/report/";
    private static final String FILE_DEST = "/report/";
    private static final String FILE_FONT = "/resources/vendors/fonts/";

    public Print2ControllerWMS350() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");

        String[] id = request.getParameterValues("selectCk");
        String shipDate = request.getParameter("shipDate");
        String wh = request.getParameter("wh");
        String dest = request.getParameter("dest");

        WHDao daofwhn = new WHDao();
        WH uawh = daofwhn.findByUid(wh);

        QdestDao daodes = new QdestDao();
        Qdest uade = daodes.findByCod(dest);

        HttpSession session = request.getSession(true);
        String userid = (String) session.getAttribute("uid");

        if (id != null) {

            try {

                QIDETAILDao dao = new QIDETAILDao();
                ResultSet result = dao.findForPrint2WMS350(id, wh, dest, shipDate);

                ServletOutputStream servletOutputStream = response.getOutputStream();
                File reportFile = new File(getServletConfig().getServletContext()
                        .getRealPath("/resources/jasper/WMS350_2.jasper"));
                byte[] bytes;

                HashMap<String, Object> map = new HashMap<String, Object>();
                map.put("WH", wh);
                map.put("DEST", dest);
                map.put("WHN", uawh.getName());
                map.put("DESTN", uade.getDesc());
                map.put("SHIPDATE", shipDate);

                try {
                    bytes = JasperRunManager.runReportToPdf(reportFile.getPath(), map, new JRResultSetDataSource(result));
                    response.setContentType("application/pdf");
                    response.setContentLength(bytes.length);

                    servletOutputStream.write(bytes, 0, bytes.length);
                    servletOutputStream.flush();
                    servletOutputStream.close();

                } catch (JRException e) {
                    StringWriter stringWriter = new StringWriter();
                    PrintWriter printWriter = new PrintWriter(stringWriter);
                    e.printStackTrace(printWriter);
                    response.setContentType("text/plain");
                    response.getOutputStream().print(stringWriter.toString());

                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

}
