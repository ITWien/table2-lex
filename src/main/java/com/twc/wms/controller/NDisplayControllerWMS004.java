/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.controller;

import com.twc.wms.dao.MSSMATNDao;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import com.twc.wms.entity.SAPMAS;
import com.twc.wms.dao.SAPMASDao;
import com.twc.wms.entity.MSSMATN;

/**
 *
 * @author wien
 */
public class NDisplayControllerWMS004 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static final String PAGE_VIEW = "../views/NdisplayWMS004.jsp";

    public NDisplayControllerWMS004() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("PROGRAMNAME", "WMS004/N");
        String forward = "";
        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "Mat Ctrl Connect Mat Code. Display");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        String mat = request.getParameter("uid");
//        SAPMASDao dao = new SAPMASDao();
//        List<SAPMAS> pList = dao.findByMc(mat);
        MSSMATNDao dao1 = new MSSMATNDao();
        MSSMATN ua = dao1.findByUid(mat);

        request.setAttribute("mc", mat);
        request.setAttribute("name", ua.getName());

        MSSMATNDao dao2 = new MSSMATNDao();
        List<MSSMATN> pList2 = dao2.findAll();

        RequestDispatcher view = request.getRequestDispatcher(forward);
//        request.setAttribute("MCNList", pList);
        request.setAttribute("MCList", pList2);
        view.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");

        String mat = request.getParameter("matctrl");
        String smatcod = request.getParameter("searchMatCode");

        request.setAttribute("PROGRAMNAME", "WMS004/N");
        String forward = "";
        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "Mat Ctrl Connect Mat Code. Display");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        SAPMASDao dao = new SAPMASDao();
        List<SAPMAS> pList = dao.findByMc(mat,smatcod);
        request.setAttribute("searchVal", smatcod);
        
        MSSMATNDao dao1 = new MSSMATNDao();
        MSSMATN ua = dao1.findByUid(mat);

        request.setAttribute("mc", mat);
        request.setAttribute("name", ua.getName());

        MSSMATNDao dao2 = new MSSMATNDao();
        List<MSSMATN> pList2 = dao2.findAll();

        RequestDispatcher view = request.getRequestDispatcher(forward);
        request.setAttribute("MCNList", pList);
        request.setAttribute("MCList", pList2);
        view.forward(request, response);
    }

}
