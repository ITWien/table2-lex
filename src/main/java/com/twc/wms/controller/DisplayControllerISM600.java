package com.twc.wms.controller;

import com.twc.wms.dao.ISM600Dao;
import com.twc.wms.dao.QdestDao;
import com.twc.wms.dao.WHDao;
import com.twc.wms.entity.XIDETAIL;
import com.twc.wms.entity.Qdest;
import com.twc.wms.entity.WH;
import java.io.IOException;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author 93176
 */
public class DisplayControllerISM600 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static final String PAGE_VIEW = "../views/displayISM600.jsp";

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        request.setAttribute("PROGRAMNAME", "ISM600");
        String forward = "";
        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "Sales Order Transportation.Process");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
//    ********
        String wh = request.getParameter("wh");
        String dest = request.getParameter("dest");
        String mat = request.getParameter("mat");
        String shipDate = request.getParameter("shipDate");
        String sts = request.getParameter("sts");
        String MY = request.getParameter("MY");

        if (MY == null) {
            Locale lc = new Locale("us", "US");
            Calendar c = Calendar.getInstance(lc);
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH) + 1;

            String y = Integer.toString(year);

            String m = Integer.toString(month);
            if (m.length() < 2) {
                m = "0" + m;
            }
            MY = y + "-" + m;
        }

        if (mat == null) {
            mat = "All";
        } else {
            if (mat.trim().equals("")) {
                mat = "All";
            }
        }

        if (dest != null) {
            dest = dest.split(" : ")[0];
        }

        System.out.println(wh);
        System.out.println(dest);
        System.out.println(shipDate);

        WHDao dao2 = new WHDao();
        List<WH> pList2 = dao2.findAll();

        WHDao daofwhn = new WHDao();
        WH uawh = daofwhn.findByUid(wh);

        QdestDao dao = new QdestDao();
        List<Qdest> destList = dao.findByWhs(wh);

        List<XIDETAIL> matList = new ISM600Dao().findMat600X(wh, dest);  //XIDETAIL XIHEAD

        QdestDao daodes = new QdestDao();
        Qdest uade = daodes.findByCod(dest);

        ISM600Dao daodt = new ISM600Dao();

        if (sts == null || sts.trim().equals("")) {
            sts = "2";
        }

        if (sts.equals("2")) {
            request.setAttribute("disno6", "display:none;");
//            request.setAttribute("disno5", "display:none;");
        } else if (sts.equals("5")) {
            request.setAttribute("disno5", "display:none;");
        } else if (sts.equals("6")) {
//            response.setHeader("Refresh", "0;/TABLE2/XMS350/display");
            request.setAttribute("disno6", "display:none;");
        }

        if ((wh != null && dest != null) && (!wh.equals("") && !dest.equals(""))) {
            List<XIDETAIL> detList = daodt.findDetail600X(wh, dest, shipDate, sts, mat.split(" ")[0], MY);
            request.setAttribute("detList", detList);
            if (!detList.isEmpty()) {
                XIDETAIL detListSum = new XIDETAIL();
                detListSum.setBox(detList.get(detList.size() - 1).getBox());
                detListSum.setBag(detList.get(detList.size() - 1).getBag());
                detListSum.setRoll(detList.get(detList.size() - 1).getRoll());
                detListSum.setM3(detList.get(detList.size() - 1).getM3());
                detListSum.setQty(detList.get(detList.size() - 1).getQty());
                request.setAttribute("detListSum", detListSum);
                detList.remove(detList.size() - 1);
            }
        }

        request.setAttribute("wh", wh);
        request.setAttribute("whn", uawh.getName());
        request.setAttribute("dest", dest);
        request.setAttribute("sts", sts);
        request.setAttribute("mat", new String(mat.getBytes("iso-8859-1"), "UTF-8"));
        request.setAttribute("destn", uade.getDesc());
        request.setAttribute("shipDate", shipDate);
        request.setAttribute("MY", MY);
        RequestDispatcher view = request.getRequestDispatcher(forward);
        request.setAttribute("MCList", pList2);
        request.setAttribute("destList", destList);
        request.setAttribute("matList", matList);

        view.forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        request.setAttribute("PROGRAMNAME", "ISM600");
        String forward = "";
        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "Sales Order Transportation.Process");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
//    ********
        String wh = request.getParameter("wh");
        String dest = request.getParameter("dest");
        String mat = request.getParameter("mat");
        String shipDate = request.getParameter("shipDate");
        String sts = request.getParameter("sts");
        String MY = request.getParameter("MY");

        if (MY == null) {
            Locale lc = new Locale("us", "US");
            Calendar c = Calendar.getInstance(lc);
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH) + 1;

            String y = Integer.toString(year);

            String m = Integer.toString(month);
            if (m.length() < 2) {
                m = "0" + m;
            }
            MY = y + "-" + m;
        }

        if (dest != null) {
            dest = dest.split(" : ")[0];
        }

        if (mat == null) {
            mat = "All";
        } else {
            if (mat.trim().equals("")) {
                mat = "All";
            }
        }

        System.out.println(wh);
        System.out.println(dest);
        System.out.println("post MY " + MY);
        System.out.println(shipDate);

        WHDao dao2 = new WHDao();
        List<WH> pList2 = dao2.findAll();

        WHDao daofwhn = new WHDao();
        WH uawh = daofwhn.findByUid(wh);

        QdestDao dao = new QdestDao();
        List<Qdest> destList = dao.findByWhs(wh);

        List<XIDETAIL> matList = new ISM600Dao().findMat600X(wh, dest);

        QdestDao daodes = new QdestDao();
        Qdest uade = daodes.findByCod(dest);

        if (sts == null || sts.trim().equals("")) {
            sts = "2";
        }

        String ck[] = request.getParameterValues("selectCk");

        if (ck != null) {
            sts = "5";

            for (int i = 0; i < ck.length; i++) {
                new ISM600Dao().rcQI2To5("5", ck[i].split("-")[0], ck[i].split("-")[1], ck[i].split("-")[4]);
            }

            request.setAttribute("disno5", "display:none;");
        } else {
            if (sts.equals("2")) {
                request.setAttribute("disno6", "display:none;");
//                request.setAttribute("disno5", "display:none;");
            } else if (sts.equals("6")) {
                request.setAttribute("disno6", "display:none;");
            }
        }

        ISM600Dao daodt = new ISM600Dao();
        if ((wh != null && dest != null) && (!wh.equals("") && !dest.equals(""))) {
            List<XIDETAIL> detList = daodt.findDetail600X(wh, dest, shipDate, sts, mat.split(" ")[0], MY);
            request.setAttribute("detList", detList);
            if (!detList.isEmpty()) {
                XIDETAIL detListSum = new XIDETAIL();
                detListSum.setBox(detList.get(detList.size() - 1).getBox());
                detListSum.setBag(detList.get(detList.size() - 1).getBag());
                detListSum.setRoll(detList.get(detList.size() - 1).getRoll());
                detListSum.setM3(detList.get(detList.size() - 1).getM3());
                detListSum.setQty(detList.get(detList.size() - 1).getQty());
                request.setAttribute("detListSum", detListSum);
                detList.remove(detList.size() - 1);
            }
        }

        request.setAttribute("wh", wh);
        request.setAttribute("whn", uawh.getName());
        request.setAttribute("dest", dest);
        request.setAttribute("sts", sts);
        request.setAttribute("mat", new String(mat.getBytes("iso-8859-1"), "UTF-8"));
        request.setAttribute("destn", uade.getDesc());
        request.setAttribute("shipDate", shipDate);
        request.setAttribute("MY", MY);
        RequestDispatcher view = request.getRequestDispatcher(forward);
        request.setAttribute("MCList", pList2);
        request.setAttribute("destList", destList);
        request.setAttribute("matList", matList);

        view.forward(request, response);

    }

}
