/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package com.twc.wms.controller;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author 93176
 */
public class DisplayControllerISM910 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static final String PAGE_VIEW = "../views/displayISM910.jsp";

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        request.setAttribute("PROGRAMNAME", "ISM910");
        String forward = "";
        try {
            String action = request.getParameter("action");
            String orderNo = request.getParameter("orderNo");
            String cusNo = request.getParameter("cusNo");
            String docNo = request.getParameter("docNo");
            String styFG = request.getParameter("styFG");
            String LotNo = request.getParameter("LotNo");
            String find = request.getParameter("find");
            String fromWhere = request.getParameter("from");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "Sale Order Information. Tracking");
                request.setAttribute("orderNo", orderNo);
                request.setAttribute("cusNo", cusNo);
                request.setAttribute("docNo", docNo);
                request.setAttribute("styFG", styFG);
                request.setAttribute("LotNo", LotNo);
                request.setAttribute("find", find);
                request.setAttribute("from", fromWhere);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }

}
