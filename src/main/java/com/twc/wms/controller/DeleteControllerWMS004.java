/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.controller;

import com.twc.wms.dao.MSSMATNDao;
import com.twc.wms.dao.WarehouseDao;
import com.twc.wms.entity.MSSMATN;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.twc.wms.entity.Warehouse;
import java.util.List;

/**
 *
 * @author wien
 */
public class DeleteControllerWMS004 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static final String PAGE_VIEW = "../views/deleteWMS004.jsp";

    public DeleteControllerWMS004() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("PROGRAMNAME", "WMS004/D");
        String forward = "";
        String id = request.getParameter("uid");
        MSSMATNDao dao = new MSSMATNDao();
        WarehouseDao dao2 = new WarehouseDao();
        MSSMATN ua = dao.findByUid(id);
        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "Material Control Master. Display");
                request.setAttribute("Eid", ua.getUid());
                request.setAttribute("Ename", ua.getName());
                
                request.setAttribute("Ewh", ua.getWarehouse());
                String whname = dao2.findWHname(ua.getWarehouse());
                request.setAttribute("Ewhn", whname);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        WarehouseDao dao1 = new WarehouseDao();
        List<Warehouse> pList = dao1.selectWarehouse();

        RequestDispatcher view = request.getRequestDispatcher(forward);
        request.setAttribute("WHList", pList);
        view.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String id = request.getParameter("uid");

        MSSMATNDao dao = new MSSMATNDao();

        dao.delete(id);

        response.setHeader("Refresh", "0;/TABLE2/WMS004/display");
    }
}
