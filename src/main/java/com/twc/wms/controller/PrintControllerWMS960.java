/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.controller;

import com.twc.wms.dao.QUSMTCTRLDao;
import com.twc.wms.entity.WMS960;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import javax.servlet.ServletContext;
import javax.servlet.ServletOutputStream;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.xssf.usermodel.XSSFFont;

/**
 *
 * @author wien
 */
public class PrintControllerWMS960 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static final String FILE_PATH = "/report/";
    private static final int BUFSIZE = 4096;
    DecimalFormat formatDou = new DecimalFormat("#,###,###,##0.00");

    public PrintControllerWMS960() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");

        String matct = request.getParameter("matct");
        String plantF = request.getParameter("plantF");
        String plantT = request.getParameter("plantT");
        String uid = request.getParameter("uid");

//        uid = "90309";
        //        ***************EXCEL*************************************
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet("Summary Onhand Report");

        Map<String, Object[]> data = new TreeMap<String, Object[]>();

        int ni = 101;

        Calendar cal = Calendar.getInstance();
        Timestamp timestamp = new Timestamp(cal.getTimeInMillis());

        List<WMS960> rp960List = new QUSMTCTRLDao().findAll960Print(uid, matct, plantF);

        String matName = "";
        String userName = "";

        if (!rp960List.isEmpty()) {
            matName = rp960List.get(0).getMatCtN();
            userName = rp960List.get(0).getUserN();

        }

        data.put(Integer.toString(ni++), new Object[]{"<B>" + "WMS960", "<B>" + "Summary Onhand Report", "<B>" + timestamp.toString()});
        data.put(Integer.toString(ni++), new Object[]{""});
        data.put(Integer.toString(ni++), new Object[]{"<B>" + "Mat Ctrl", matct.trim() + " : " + matName, "<B>" + "Plant", plantF, "<B>" + "User", uid.trim() + " : " + userName});
        data.put(Integer.toString(ni++), new Object[]{"<B>" + "Mat Code", "<B>" + "Description", "<B>" + "MC(1+2+3)", "<B>" + "1 : Received", "<B>" + "2 : Complete", "<B>" + "3 : Issue"});

        Double sum123 = 0.00;
        Double sum1 = 0.00;
        Double sum2 = 0.00;
        Double sum3 = 0.00;

        for (int i = 0; i < rp960List.size(); i++) {
            data.put(Integer.toString(ni++), new Object[]{rp960List.get(i).getMatCo(),
                rp960List.get(i).getDesc(),
                "<R>" + formatDou.format(Double.parseDouble(rp960List.get(i).getSts123())),
                "<R>" + formatDou.format(Double.parseDouble(rp960List.get(i).getSts1())),
                "<R>" + formatDou.format(Double.parseDouble(rp960List.get(i).getSts2())),
                "<R>" + formatDou.format(Double.parseDouble(rp960List.get(i).getSts3()))});

            sum123 += Double.parseDouble(rp960List.get(i).getSts123().replace(",", ""));
            sum1 += Double.parseDouble(rp960List.get(i).getSts1().replace(",", ""));
            sum2 += Double.parseDouble(rp960List.get(i).getSts2().replace(",", ""));
            sum3 += Double.parseDouble(rp960List.get(i).getSts3().replace(",", ""));
        }

        data.put(Integer.toString(ni++), new Object[]{"",
            "<U><B>" + "Total",
            "<U><R>" + formatDou.format(sum123),
            "<U><R>" + formatDou.format(sum1),
            "<U><R>" + formatDou.format(sum2),
            "<U><R>" + formatDou.format(sum3)});

        sum123 = 0.00;
        sum1 = 0.00;
        sum2 = 0.00;
        sum3 = 0.00;

        data.put(Integer.toString(ni++), new Object[]{""});

        if (!plantF.equals(plantT)) {
            List<WMS960> rp960List2 = new QUSMTCTRLDao().findAll960Print(uid, matct, plantT);

            String matName2 = "";
            String userName2 = "";

            if (!rp960List2.isEmpty()) {
                matName2 = rp960List2.get(0).getMatCtN();
                userName2 = rp960List2.get(0).getUserN();

            }

            data.put(Integer.toString(ni++), new Object[]{"<B>" + "Mat Ctrl", matct.trim() + " : " + matName2, "<B>" + "Plant", plantT, "<B>" + "User", uid.trim() + " : " + userName2});
            data.put(Integer.toString(ni++), new Object[]{"<B>" + "Mat Code", "<B>" + "Description", "<B>" + "MC(1+2+3)", "<B>" + "1 : Received", "<B>" + "2 : Complete", "<B>" + "3 : Issue"});

            for (int i = 0; i < rp960List2.size(); i++) {
                data.put(Integer.toString(ni++), new Object[]{rp960List2.get(i).getMatCo(),
                    rp960List2.get(i).getDesc(),
                    "<R>" + formatDou.format(Double.parseDouble(rp960List2.get(i).getSts123())),
                    "<R>" + formatDou.format(Double.parseDouble(rp960List2.get(i).getSts1())),
                    "<R>" + formatDou.format(Double.parseDouble(rp960List2.get(i).getSts2())),
                    "<R>" + formatDou.format(Double.parseDouble(rp960List2.get(i).getSts3()))});

                sum123 += Double.parseDouble(rp960List2.get(i).getSts123().replace(",", ""));
                sum1 += Double.parseDouble(rp960List2.get(i).getSts1().replace(",", ""));
                sum2 += Double.parseDouble(rp960List2.get(i).getSts2().replace(",", ""));
                sum3 += Double.parseDouble(rp960List2.get(i).getSts3().replace(",", ""));
            }

            data.put(Integer.toString(ni++), new Object[]{"",
                "<U><B>" + "Total",
                "<U><R>" + formatDou.format(sum123),
                "<U><R>" + formatDou.format(sum1),
                "<U><R>" + formatDou.format(sum2),
                "<U><R>" + formatDou.format(sum3)});
        }

        //Set Column Width
        sheet.setColumnWidth(0, 5000);
        sheet.setColumnWidth(1, 13000);
        sheet.setColumnWidth(2, 4000);
        sheet.setColumnWidth(3, 4000);
        sheet.setColumnWidth(4, 4000);
        sheet.setColumnWidth(5, 4000);
//        sheet.setColumnWidth(6, 2000);
//        sheet.setColumnWidth(7, 5000);
//        sheet.setColumnWidth(8, 5000);
//        sheet.setColumnWidth(9, 5000);
//        sheet.setColumnWidth(10, 5000);
        XSSFFont my_font = workbook.createFont();
        my_font.setBold(true);

        // Iterate over data and write to sheet 
        Set<String> keyset = data.keySet();
        int rownum = 0;
        for (String key : keyset) {
            // this creates a new row in the sheet 
            Row row = sheet.createRow(rownum++);
            Object[] objArr = data.get(key);
            int cellnum = 0;
            for (Object obj : objArr) {
                // this line creates a cell in the next column of that row 
                Cell cell = row.createCell(cellnum++);
                cell.setCellValue((String) obj);

                CellStyle style_tmp = workbook.createCellStyle();

                if (cell.getStringCellValue().contains("<R>")) {
                    cell.setCellValue(cell.getStringCellValue().replace("<R>", ""));
                    style_tmp.setAlignment(HorizontalAlignment.RIGHT);
                }
                if (cell.getStringCellValue().contains("<B>")) {
                    cell.setCellValue(cell.getStringCellValue().replace("<B>", ""));
                    style_tmp.setFont(my_font);
                }
                if (cell.getStringCellValue().contains("<U>")) {
                    cell.setCellValue(cell.getStringCellValue().replace("<U>", ""));
                    style_tmp.setBorderBottom(BorderStyle.THIN);
                }
                cell.setCellStyle(style_tmp);
            }
        }

        try {
            ServletContext servletContextEX = getServletContext();    // new by ji
            String realPathEX = servletContextEX.getRealPath(FILE_PATH);        // new by ji
            String saperateEX = realPathEX.contains(":") ? "\\" : "/";
            String pathEX = realPathEX + saperateEX + "SUMMARY_ONHAND_REPORT.xlsx";

            FileOutputStream outputStream = new FileOutputStream(pathEX);
            workbook.write(outputStream);
            workbook.close();

//            ******************************************************
            File fileEX = new File(pathEX);
            int length = 0;
            ServletOutputStream outStream = response.getOutputStream();
            ServletContext context = getServletConfig().getServletContext();
            String mimetype = context.getMimeType(pathEX);

            // sets response content type
            if (mimetype == null) {
                mimetype = "application/octet-stream";
            }
            response.setContentType(mimetype);
            response.setContentLength((int) fileEX.length());
            String fileNameEX = (new File(pathEX)).getName();

            // sets HTTP header
            response.setHeader("Content-Disposition", "attachment; filename=\"" + fileNameEX + "\"");

            byte[] byteBuffer = new byte[BUFSIZE];
            DataInputStream in = new DataInputStream(new FileInputStream(fileEX));

            // reads the file's bytes and writes them to the response stream
            while ((in != null) && ((length = in.read(byteBuffer)) != -1)) {
                outStream.write(byteBuffer, 0, length);
            }

            in.close();
            outStream.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

}
