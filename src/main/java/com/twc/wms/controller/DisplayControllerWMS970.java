/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.controller;

import com.twc.wms.dao.QIDETAILDao;
import com.twc.wms.dao.WHDao;
import com.twc.wms.entity.WH;
import com.twc.wms.dao.QIHEADDao;
import com.twc.wms.dao.QdestDao;
import com.twc.wms.dao.UserDao;
import com.twc.wms.dao.WMS970Dao;
import com.twc.wms.dao.XMSWIHEADDao;
import com.twc.wms.entity.MSSMATN;
import com.twc.wms.entity.QIDETAIL;
import com.twc.wms.entity.Qdest;
import com.twc.wms.entity.UserAuth;
import com.twc.wms.entity.WMS970;
import com.twc.wms.entity.XMSWIHEAD;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.poi.sl.draw.geom.Path;

/**
 *
 * @author wien
 */
public class DisplayControllerWMS970 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static final String PAGE_VIEW = "../views/displayWMS970.jsp";
    private static final String FILE_PATH = "WMS/images/raw-materials/";

    public DisplayControllerWMS970() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        request.setAttribute("PROGRAMNAME", "WMS970");
        String forward = "";
        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "Onhand by RM Style. Display");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
//    ********
        request.setAttribute("plant", "1000");
        request.setAttribute("so", "matCode");
        request.setAttribute("son", "1 : �����ѵ�شԺ");
        request.setAttribute("dimensionListShow", "hidden");

        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        request.setAttribute("PROGRAMNAME", "WMS970");
        String forward = "";
        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "Onhand by RM Style. Display");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
//    ********
        String rm = request.getParameter("rm");
        String plant = request.getParameter("plant");
        String so = request.getParameter("so");

        if (rm.length() >= 8) {
            if (rm.contains("|")) {
                rm = rm.split("\\|")[3];
                rm = rm.substring(0, 8);
            } else {
                rm = rm.substring(0, 8);
            }
        }

        request.setAttribute("rm", rm);
        request.setAttribute("plant", plant);
        request.setAttribute("so", so);
        if (so.equals("matCode")) {
            request.setAttribute("son", "1 : �����ѵ�شԺ");
        }

        String path = "";
        ServletContext servletContext = getServletContext();    // new by ji
        String realPath = servletContext.getRealPath(FILE_PATH);        // new by ji
        String saperate = realPath.contains(":") ? "\\" : "/";
        path = realPath + saperate + "noimages.jpg";        // new by ji

        realPath = realPath.replace("TABLE2/", "");
        request.setAttribute("realPath", realPath);

        File folder = new File(realPath);
        File[] listOfFiles = folder.listFiles();

        List<WMS970> headList = new WMS970Dao().findQty(rm, plant);
        for (int i = 0; i < headList.size(); i++) {
            headList.get(i).setDetList(new WMS970Dao().findQtyDet(rm, plant, headList.get(i).getCOL()));

            List<String> imageList = new ArrayList<String>();

            if (listOfFiles != null) {
                for (int j = 0; j < listOfFiles.length; j++) {
                    if (listOfFiles[j].isFile()) {
                        if (listOfFiles[j].getName().contains(rm + headList.get(i).getCOL())) {
                            imageList.add(listOfFiles[j].getName());
                        }
                    }
                }

                headList.get(i).setNoi(imageList.size());
                headList.get(i).setImageList(imageList);
            }
        }
        request.setAttribute("headList", headList);

        List<WMS970> dimensionList = new WMS970Dao().findDimension(rm);
        request.setAttribute("dimensionList", dimensionList);

        String dls = "hidden";
        if (dimensionList.size() > 0) {
            dls = "";
        }
        request.setAttribute("dimensionListShow", dls);

        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);
    }

}
