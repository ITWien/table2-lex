/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.controller;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.twc.wms.dao.QRKHEADDao;
import com.twc.wms.entity.QRKHEAD;
import com.twc.wms.dao.WarehouseDao;
import com.twc.wms.entity.Warehouse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author wien
 */
public class EditControllerWMS008 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static final String PAGE_VIEW = "../views/editWMS008.jsp";

    public EditControllerWMS008() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("PROGRAMNAME", "WMS008/E");
        String forward = "";
        String wh = request.getParameter("wh");
        String rkno = request.getParameter("rkno");
        QRKHEADDao dao = new QRKHEADDao();
        QRKHEAD ua = dao.findByCod(wh, rkno);
        WarehouseDao dao2 = new WarehouseDao();
        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "Rack Master. Display");
                request.setAttribute("rkno", ua.getDesc());
                request.setAttribute("zone", ua.getName());

                request.setAttribute("wh", wh);
                String whname = dao2.findWHname(wh);
                request.setAttribute("whn", whname);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");

        String rkno = request.getParameter("rkno");
        String zone = request.getParameter("zone");
        String wh = request.getParameter("warehouse");

        String userid = request.getParameter("userid");

        String sendMessage = "";
        String forward = "";

        request.setAttribute("PROGRAMNAME", "WMS008/E");
        request.setAttribute("PROGRAMDESC", "Rack Master. Display");

        QRKHEADDao dao = new QRKHEADDao();
        QRKHEAD p = new QRKHEAD(wh, rkno, zone);

        if (dao.edit(p, userid)) {
            response.setHeader("Refresh", "0;/TABLE2/WMS008/display");
        } else {
            sendMessage = "<script type=\"text/javascript\">\n"
                    + "            var show2 = function () {\n"
                    + "                $('#myModal2').modal('show');\n"
                    + "            };\n"
                    + "\n"
                    + "            window.setTimeout(show2, 0);\n"
                    + "\n"
                    + "        </script>";
            forward = PAGE_VIEW;
            request.setAttribute("sendMessage", sendMessage);

            RequestDispatcher view = request.getRequestDispatcher(forward);
            view.forward(request, response);
        }
    }

}
