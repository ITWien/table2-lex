/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.controller;

import com.twc.wms.dao.QUSMTCTRLDao;
import com.twc.wms.dao.WMSPRTDao;
import com.twc.wms.dao.WMSTOT930Dao;
import com.twc.wms.entity.WMS910PRT;
import com.twc.wms.entity.WMS960;
import com.twc.wms.entity.WMSTOT930;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import javax.servlet.ServletContext;
import javax.servlet.ServletOutputStream;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFFont;

/**
 *
 * @author wien
 */
public class PrintControllerWMSPRT extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static final String FILE_PATH = "/report/";
    private static final int BUFSIZE = 4096;
    DecimalFormat formatDou0 = new DecimalFormat("#,###,###,##0");
    DecimalFormat formatDou = new DecimalFormat("#,###,###,##0.00");
    DecimalFormat formatDou3 = new DecimalFormat("#,###,###,##0.000");
    DecimalFormat formatDou6 = new DecimalFormat("0.000000");

    public PrintControllerWMSPRT() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");

        String page = request.getParameter("page");
        String action = request.getParameter("action");
        String warehouse = request.getParameter("warehouse");
        String matCtrl = request.getParameter("matCtrl");
        String onhandDate = request.getParameter("onhandDate");
        String uid = request.getParameter("uid");

        System.out.println("print910");

        String dateOn = "";
        if (onhandDate != null) {
            String[] date = onhandDate.split("/");
            dateOn = date[2] + date[1] + date[0];
        }

//        uid = "90309";
        //        ***************EXCEL*************************************
        XSSFWorkbook workbook = new XSSFWorkbook();

        Double totSku = 0.00;
        Double totPck = 0.00;
        Double totPer = 0.00;
        Double totAmt = 0.00;
        Double cntSku = 0.00;
        Double cntPck = 0.00;
        Double cntPer = 0.00;
        Double cntAmt = 0.00;
        Double corSku = 0.00;
        Double corPck = 0.00;
        Double corPer = 0.00;
        Double corAmt = 0.00;
        Double incSku = 0.00;
        Double incPck = 0.00;
        Double incPer = 0.00;
        Double incAmt = 0.00;
        Double dimSku = 0.00;
        Double dimPck = 0.00;
        Double dimPer = 0.00;
        Double dimAmt = 0.00;
        Double dioSku = 0.00;
        Double dioPck = 0.00;
        Double dioPer = 0.00;
        Double dioAmt = 0.00;

        //insert total this line
        List<WMS910PRT> rp910List = new WMSPRTDao().showWMS910(warehouse, onhandDate, matCtrl, action);

        String wh = "";
        String mat = "";
        String plant = "";
        String store = "";

        if (!rp910List.isEmpty()) {
            wh = rp910List.get(0).getSAPWHS() + " : " + rp910List.get(0).getSAPWHSDESC();
            mat = rp910List.get(0).getSAPMTCTRL() + " : " + rp910List.get(0).getSAPMCTRLDESC();
            plant = rp910List.get(0).getSAPPLANT();
            store = rp910List.get(0).getSAPLOCATION();
        }

        for (int index = 0; index < 7; index++) {

            String sheetName = "";
            XSSFSheet sheet = null;

            if (index == 0) {
                sheetName = "TOTAL";
                sheet = workbook.createSheet(sheetName);
            } else if (index == 6) {
                sheetName = "SUMMARY";
                sheet = workbook.createSheet(sheetName);
            }

//            if (index == 0) {
//                sheetName = "TOTAL";
//            } else if (index == 1) {
//                sheetName = "COUNTING";
//            } else if (index == 2) {
//                sheetName = "CORRECT";
//            } else if (index == 3) {
//                sheetName = "INCORRECT";
//            } else if (index == 4) {
//                sheetName = "Diff-";
//            } else if (index == 5) {
//                sheetName = "Diff+";
//            } else if (index == 6) {
//                sheetName = "SUMMARY";
//            }
            Map<String, Object[]> data = new TreeMap<String, Object[]>();

            int ni = 100001;

            data.put(Integer.toString(ni++), new Object[]{"", "", "", "", "", "Onhand Comparison. Display (SAP : TABLET)"});
            data.put(Integer.toString(ni++), new Object[]{""});
            data.put(Integer.toString(ni++), new Object[]{"", "Warehouse", wh, "", "", "Onhand Date", onhandDate});
            data.put(Integer.toString(ni++), new Object[]{"", "MAT.CTRL", mat, "", "", "PLANT", plant, "STOR", store});

            if (index != 6) {
                data.put(Integer.toString(ni++), new Object[]{
                    "NO.",
                    "ITEM-ID",
                    "DESCRIPTION",
                    "VT",
                    "UNIT",
                    "UNRES",
                    "QI",
                    "UNR+QI",
                    "SAP",
                    "WMS",
                    "Diff ขาด",
                    "Diff เกิน",
                    "AVG.",
                    "SAP AMT.",
                    "WMS AMT.",
                    "จำนวนเงิน/ขาด",
                    "จำนวนเงิน/เกิน",});
            } else {
                data.put(Integer.toString(ni++), new Object[]{""});
            }

            Double sumamt = 0.00;
            Double sumsap = 0.00;
            Double sumwms = 0.00;
            Double sumdiffmiss = 0.00;
            Double sumdiffover = 0.00;
            Double sumamtmiss = 0.00;
            Double sumamtover = 0.00;
            Double sumWmsAmt = 0.00;

            int lineNo = 0;

//            System.out.println("index " + index);
            for (int i = 0; i < rp910List.size(); i++) {

                if (index == 0) {
//                    System.out.println("TOTAL");
                    data.put(Integer.toString(ni++), new Object[]{
                        Integer.toString(++lineNo),
                        rp910List.get(i).getSAPMAT(),
                        rp910List.get(i).getSAPDESC(),
                        rp910List.get(i).getSAPVAL(),
                        rp910List.get(i).getSAPBUN(),
                        rp910List.get(i).getSAPPUNRES(),
                        rp910List.get(i).getQI(),
                        rp910List.get(i).getSAPQI(),
                        rp910List.get(i).getSAPTOTAL(),
                        rp910List.get(i).getQCKSUM(),
                        rp910List.get(i).getDIFFMISS(),
                        rp910List.get(i).getDIFFOVER(),
                        rp910List.get(i).getSAPAVG(),
                        rp910List.get(i).getSAPAMOUNT(),
                        rp910List.get(i).getWMS_AMT(),
                        rp910List.get(i).getAMTMISS(),
                        rp910List.get(i).getAMTOVER(),});

                    sumamt += Double.parseDouble(rp910List.get(i).getSAPAMOUNT() != null ? (rp910List.get(i).getSAPAMOUNT().replace(",", "")) : "0");
                    sumsap += Double.parseDouble(rp910List.get(i).getSAPTOTAL() != null ? (rp910List.get(i).getSAPTOTAL().replace(",", "")) : "0");
                    sumwms += Double.parseDouble(rp910List.get(i).getQCKSUM() != null ? (rp910List.get(i).getQCKSUM().replace(",", "")) : "0");
                    sumdiffmiss += Double.parseDouble(rp910List.get(i).getDIFFMISS() != null ? (rp910List.get(i).getDIFFMISS().replace(",", "")) : "0");
                    sumdiffover += Double.parseDouble(rp910List.get(i).getDIFFOVER() != null ? (rp910List.get(i).getDIFFOVER().replace(",", "")) : "0");
                    sumamtmiss += Double.parseDouble(rp910List.get(i).getAMTMISS() != null ? (rp910List.get(i).getAMTMISS().replace(",", "")) : "0");
                    sumamtover += Double.parseDouble(rp910List.get(i).getAMTOVER() != null ? (rp910List.get(i).getAMTOVER().replace(",", "")) : "0");
                    sumWmsAmt += Double.parseDouble(rp910List.get(i).getWMS_AMT() != null ? (rp910List.get(i).getWMS_AMT().replace(",", "")) : "0");

//                    if (!rp910List.get(i).getSAPVAL().equals("")) {
                        totSku++;
//                    }

                } else if (index == 1) {
//                    System.out.println("COUNTING");
                    Double WMS = Double.parseDouble(rp910List.get(i).getQCKSUM() != null ? (rp910List.get(i).getQCKSUM().replace(",", "")) : "0");
                    if (WMS != 0) {
//                        data.put(Integer.toString(ni++), new Object[]{
//                            Integer.toString(++lineNo),
//                            rp910List.get(i).getSAPMAT(),
//                            rp910List.get(i).getSAPDESC(),
//                            rp910List.get(i).getSAPVAL(),
//                            rp910List.get(i).getSAPBUN(),
//                            rp910List.get(i).getSAPPUNRES(),
//                            rp910List.get(i).getQI(),
//                            rp910List.get(i).getSAPQI(),
//                            rp910List.get(i).getSAPTOTAL(),
//                            rp910List.get(i).getQCKSUM(),
//                            rp910List.get(i).getDIFFMISS(),
//                            rp910List.get(i).getDIFFOVER(),
//                            rp910List.get(i).getSAPAVG(),
//                            rp910List.get(i).getSAPAMOUNT(),
//                            rp910List.get(i).getWMS_AMT(),
//                            rp910List.get(i).getAMTMISS(),
//                            rp910List.get(i).getAMTOVER(),});
//
                        sumamt += Double.parseDouble(rp910List.get(i).getSAPAMOUNT() != null ? (rp910List.get(i).getSAPAMOUNT().replace(",", "")) : "0");
                        sumsap += Double.parseDouble(rp910List.get(i).getSAPTOTAL() != null ? (rp910List.get(i).getSAPTOTAL().replace(",", "")) : "0");
                        sumwms += Double.parseDouble(rp910List.get(i).getQCKSUM() != null ? (rp910List.get(i).getQCKSUM().replace(",", "")) : "0");
                        sumdiffmiss += Double.parseDouble(rp910List.get(i).getDIFFMISS() != null ? (rp910List.get(i).getDIFFMISS().replace(",", "")) : "0");
                        sumdiffover += Double.parseDouble(rp910List.get(i).getDIFFOVER() != null ? (rp910List.get(i).getDIFFOVER().replace(",", "")) : "0");
                        sumamtmiss += Double.parseDouble(rp910List.get(i).getAMTMISS() != null ? (rp910List.get(i).getAMTMISS().replace(",", "")) : "0");
                        sumamtover += Double.parseDouble(rp910List.get(i).getAMTOVER() != null ? (rp910List.get(i).getAMTOVER().replace(",", "")) : "0");
                        sumWmsAmt += Double.parseDouble(rp910List.get(i).getWMS_AMT() != null ? (rp910List.get(i).getWMS_AMT().replace(",", "")) : "0");

//                        if (!rp910List.get(i).getSAPVAL().equals("")) {
                            cntSku++;
//                        }

                    }
                } else if (index == 2) {
//                    System.out.println("CORRECT");
                    Double SAP = Double.parseDouble(rp910List.get(i).getSAPTOTAL() != null ? (rp910List.get(i).getSAPTOTAL().replace(",", "")) : "0");
                    Double WMS = Double.parseDouble(rp910List.get(i).getQCKSUM() != null ? (rp910List.get(i).getQCKSUM().replace(",", "")) : "0");

                    if (Double.compare(SAP, WMS) == 0) {
//                        data.put(Integer.toString(ni++), new Object[]{
//                            Integer.toString(++lineNo),
//                            rp910List.get(i).getSAPMAT(),
//                            rp910List.get(i).getSAPDESC(),
//                            rp910List.get(i).getSAPVAL(),
//                            rp910List.get(i).getSAPBUN(),
//                            rp910List.get(i).getSAPPUNRES(),
//                            rp910List.get(i).getQI(),
//                            rp910List.get(i).getSAPQI(),
//                            rp910List.get(i).getSAPTOTAL(),
//                            rp910List.get(i).getQCKSUM(),
//                            rp910List.get(i).getDIFFMISS(),
//                            rp910List.get(i).getDIFFOVER(),
//                            rp910List.get(i).getSAPAVG(),
//                            rp910List.get(i).getSAPAMOUNT(),
//                            rp910List.get(i).getWMS_AMT(),
//                            rp910List.get(i).getAMTMISS(),
//                            rp910List.get(i).getAMTOVER(),});
//
                        sumamt += Double.parseDouble(rp910List.get(i).getSAPAMOUNT() != null ? (rp910List.get(i).getSAPAMOUNT().replace(",", "")) : "0");
                        sumsap += Double.parseDouble(rp910List.get(i).getSAPTOTAL() != null ? (rp910List.get(i).getSAPTOTAL().replace(",", "")) : "0");
                        sumwms += Double.parseDouble(rp910List.get(i).getQCKSUM() != null ? (rp910List.get(i).getQCKSUM().replace(",", "")) : "0");
                        sumdiffmiss += Double.parseDouble(rp910List.get(i).getDIFFMISS() != null ? (rp910List.get(i).getDIFFMISS().replace(",", "")) : "0");
                        sumdiffover += Double.parseDouble(rp910List.get(i).getDIFFOVER() != null ? (rp910List.get(i).getDIFFOVER().replace(",", "")) : "0");
                        sumamtmiss += Double.parseDouble(rp910List.get(i).getAMTMISS() != null ? (rp910List.get(i).getAMTMISS().replace(",", "")) : "0");
                        sumamtover += Double.parseDouble(rp910List.get(i).getAMTOVER() != null ? (rp910List.get(i).getAMTOVER().replace(",", "")) : "0");
                        sumWmsAmt += Double.parseDouble(rp910List.get(i).getWMS_AMT() != null ? (rp910List.get(i).getWMS_AMT().replace(",", "")) : "0");

//                        if (!rp910List.get(i).getSAPVAL().equals("")) {
                            corSku++;
//                        }

                        corPck += Double.parseDouble(rp910List.get(i).getPACKPCS_COR() != null ? (rp910List.get(i).getPACKPCS_COR().replace(",", "")) : "0");

                    }
                } else if (index == 3) {
//                    System.out.println("INCORRECT");
                    Double SAP = Double.parseDouble(rp910List.get(i).getSAPTOTAL() != null ? (rp910List.get(i).getSAPTOTAL().replace(",", "")) : "0");
                    Double WMS = Double.parseDouble(rp910List.get(i).getQCKSUM() != null ? (rp910List.get(i).getQCKSUM().replace(",", "")) : "0");

                    if (Double.compare(SAP, WMS) != 0) {
//                        data.put(Integer.toString(ni++), new Object[]{
//                            Integer.toString(++lineNo),
//                            rp910List.get(i).getSAPMAT(),
//                            rp910List.get(i).getSAPDESC(),
//                            rp910List.get(i).getSAPVAL(),
//                            rp910List.get(i).getSAPBUN(),
//                            rp910List.get(i).getSAPPUNRES(),
//                            rp910List.get(i).getQI(),
//                            rp910List.get(i).getSAPQI(),
//                            rp910List.get(i).getSAPTOTAL(),
//                            rp910List.get(i).getQCKSUM(),
//                            rp910List.get(i).getDIFFMISS(),
//                            rp910List.get(i).getDIFFOVER(),
//                            rp910List.get(i).getSAPAVG(),
//                            rp910List.get(i).getSAPAMOUNT(),
//                            rp910List.get(i).getWMS_AMT(),
//                            rp910List.get(i).getAMTMISS(),
//                            rp910List.get(i).getAMTOVER(),});
//
                        sumamt += Double.parseDouble(rp910List.get(i).getSAPAMOUNT() != null ? (rp910List.get(i).getSAPAMOUNT().replace(",", "")) : "0");
                        sumsap += Double.parseDouble(rp910List.get(i).getSAPTOTAL() != null ? (rp910List.get(i).getSAPTOTAL().replace(",", "")) : "0");
                        sumwms += Double.parseDouble(rp910List.get(i).getQCKSUM() != null ? (rp910List.get(i).getQCKSUM().replace(",", "")) : "0");
                        sumdiffmiss += Double.parseDouble(rp910List.get(i).getDIFFMISS() != null ? (rp910List.get(i).getDIFFMISS().replace(",", "")) : "0");
                        sumdiffover += Double.parseDouble(rp910List.get(i).getDIFFOVER() != null ? (rp910List.get(i).getDIFFOVER().replace(",", "")) : "0");
                        sumamtmiss += Double.parseDouble(rp910List.get(i).getAMTMISS() != null ? (rp910List.get(i).getAMTMISS().replace(",", "")) : "0");
                        sumamtover += Double.parseDouble(rp910List.get(i).getAMTOVER() != null ? (rp910List.get(i).getAMTOVER().replace(",", "")) : "0");
                        sumWmsAmt += Double.parseDouble(rp910List.get(i).getWMS_AMT() != null ? (rp910List.get(i).getWMS_AMT().replace(",", "")) : "0");

//                        if (!rp910List.get(i).getSAPVAL().equals("")) {
                            incSku++;
//                        }

                    }
                } else if (index == 4) {
//                    System.out.println("DIFF-");
                    Double SAP = Double.parseDouble(rp910List.get(i).getSAPTOTAL() != null ? (rp910List.get(i).getSAPTOTAL().replace(",", "")) : "0");
                    Double WMS = Double.parseDouble(rp910List.get(i).getQCKSUM() != null ? (rp910List.get(i).getQCKSUM().replace(",", "")) : "0");
                    Double DIFFMISS = Double.parseDouble(rp910List.get(i).getDIFFMISS() != null ? (rp910List.get(i).getDIFFMISS().replace(",", "")) : "0");

                    if (Double.compare(SAP, WMS) != 0) {
                        if (DIFFMISS != 0) {
//                            data.put(Integer.toString(ni++), new Object[]{
//                                Integer.toString(++lineNo),
//                                rp910List.get(i).getSAPMAT(),
//                                rp910List.get(i).getSAPDESC(),
//                                rp910List.get(i).getSAPVAL(),
//                                rp910List.get(i).getSAPBUN(),
//                                rp910List.get(i).getSAPPUNRES(),
//                                rp910List.get(i).getQI(),
//                                rp910List.get(i).getSAPQI(),
//                                rp910List.get(i).getSAPTOTAL(),
//                                rp910List.get(i).getQCKSUM(),
//                                rp910List.get(i).getDIFFMISS(),
//                                rp910List.get(i).getDIFFOVER(),
//                                rp910List.get(i).getSAPAVG(),
//                                rp910List.get(i).getSAPAMOUNT(),
//                                rp910List.get(i).getWMS_AMT(),
//                                rp910List.get(i).getAMTMISS(),
//                                rp910List.get(i).getAMTOVER(),});
//
                            sumamt += Double.parseDouble(rp910List.get(i).getSAPAMOUNT() != null ? (rp910List.get(i).getSAPAMOUNT().replace(",", "")) : "0");
                            sumsap += Double.parseDouble(rp910List.get(i).getSAPTOTAL() != null ? (rp910List.get(i).getSAPTOTAL().replace(",", "")) : "0");
                            sumwms += Double.parseDouble(rp910List.get(i).getQCKSUM() != null ? (rp910List.get(i).getQCKSUM().replace(",", "")) : "0");
                            sumdiffmiss += Double.parseDouble(rp910List.get(i).getDIFFMISS() != null ? (rp910List.get(i).getDIFFMISS().replace(",", "")) : "0");
                            sumdiffover += Double.parseDouble(rp910List.get(i).getDIFFOVER() != null ? (rp910List.get(i).getDIFFOVER().replace(",", "")) : "0");
                            sumamtmiss += Double.parseDouble(rp910List.get(i).getAMTMISS() != null ? (rp910List.get(i).getAMTMISS().replace(",", "")) : "0");
                            sumamtover += Double.parseDouble(rp910List.get(i).getAMTOVER() != null ? (rp910List.get(i).getAMTOVER().replace(",", "")) : "0");
                            sumWmsAmt += Double.parseDouble(rp910List.get(i).getWMS_AMT() != null ? (rp910List.get(i).getWMS_AMT().replace(",", "")) : "0");

//                            if (!rp910List.get(i).getSAPVAL().equals("")) {
                                dimSku++;
//                            }

                        }
                    }
                } else if (index == 5) {
//                    System.out.println("DIFF+");
                    Double SAP = Double.parseDouble(rp910List.get(i).getSAPTOTAL() != null ? (rp910List.get(i).getSAPTOTAL().replace(",", "")) : "0");
                    Double WMS = Double.parseDouble(rp910List.get(i).getQCKSUM() != null ? (rp910List.get(i).getQCKSUM().replace(",", "")) : "0");
                    Double DIFFOVER = Double.parseDouble(rp910List.get(i).getDIFFOVER() != null ? (rp910List.get(i).getDIFFOVER().replace(",", "")) : "0");

                    if (Double.compare(SAP, WMS) != 0) {
                        if (DIFFOVER != 0) {
//                            data.put(Integer.toString(ni++), new Object[]{
//                                Integer.toString(++lineNo),
//                                rp910List.get(i).getSAPMAT(),
//                                rp910List.get(i).getSAPDESC(),
//                                rp910List.get(i).getSAPVAL(),
//                                rp910List.get(i).getSAPBUN(),
//                                rp910List.get(i).getSAPPUNRES(),
//                                rp910List.get(i).getQI(),
//                                rp910List.get(i).getSAPQI(),
//                                rp910List.get(i).getSAPTOTAL(),
//                                rp910List.get(i).getQCKSUM(),
//                                rp910List.get(i).getDIFFMISS(),
//                                rp910List.get(i).getDIFFOVER(),
//                                rp910List.get(i).getSAPAVG(),
//                                rp910List.get(i).getSAPAMOUNT(),
//                                rp910List.get(i).getWMS_AMT(),
//                                rp910List.get(i).getAMTMISS(),
//                                rp910List.get(i).getAMTOVER(),});
//
                            sumamt += Double.parseDouble(rp910List.get(i).getSAPAMOUNT() != null ? (rp910List.get(i).getSAPAMOUNT().replace(",", "")) : "0");
                            sumsap += Double.parseDouble(rp910List.get(i).getSAPTOTAL() != null ? (rp910List.get(i).getSAPTOTAL().replace(",", "")) : "0");
                            sumwms += Double.parseDouble(rp910List.get(i).getQCKSUM() != null ? (rp910List.get(i).getQCKSUM().replace(",", "")) : "0");
                            sumdiffmiss += Double.parseDouble(rp910List.get(i).getDIFFMISS() != null ? (rp910List.get(i).getDIFFMISS().replace(",", "")) : "0");
                            sumdiffover += Double.parseDouble(rp910List.get(i).getDIFFOVER() != null ? (rp910List.get(i).getDIFFOVER().replace(",", "")) : "0");
                            sumamtmiss += Double.parseDouble(rp910List.get(i).getAMTMISS() != null ? (rp910List.get(i).getAMTMISS().replace(",", "")) : "0");
                            sumamtover += Double.parseDouble(rp910List.get(i).getAMTOVER() != null ? (rp910List.get(i).getAMTOVER().replace(",", "")) : "0");
                            sumWmsAmt += Double.parseDouble(rp910List.get(i).getWMS_AMT() != null ? (rp910List.get(i).getWMS_AMT().replace(",", "")) : "0");

//                            if (!rp910List.get(i).getSAPVAL().equals("")) {
                                dioSku++;
//                            }

                        }
                    }
                }

                cntPck = Double.parseDouble(rp910List.get(i).getPACKPCS());

            }

            if (index != 6) {
                data.put(Integer.toString(ni++), new Object[]{"", "", "", "", "", "", "",
                    "Total",
                    formatDou3.format(sumsap),
                    formatDou3.format(sumwms),
                    formatDou3.format(sumdiffmiss),
                    formatDou3.format(sumdiffover),
                    "",
                    formatDou.format(sumamt),
                    formatDou.format(sumWmsAmt),
                    formatDou.format(sumamtmiss),
                    formatDou.format(sumamtover)});

                if (index == 0) {
                    totAmt = sumamt;
                } else if (index == 1) {
                    cntAmt = sumWmsAmt;
                } else if (index == 2) {
                    corAmt = sumWmsAmt;
                } else if (index == 3) {
                    incAmt = sumamtover - sumamtmiss;
                } else if (index == 4) {
                    dimAmt = sumamtmiss;
                } else if (index == 5) {
                    dioAmt = sumamtover;
                }
            } else {
                totPer = (totSku / totSku) * 100;
                cntPer = (cntSku / totSku) * 100;
                corPer = (corSku / totSku) * 100;
                incPer = (incSku / totSku) * 100;
                dimPer = (dimSku / totSku) * 100;
                dioPer = (dioSku / totSku) * 100;

                data.put(Integer.toString(ni++), new Object[]{"", "", "SKU<TH>", "PACKAGE<TH>", "%<TH>", "AMOUNT<TH>"});
                data.put(Integer.toString(ni++), new Object[]{"", "TOTAL SKU<TH>", formatDou0.format(totSku) + "<TD>", "-<TD>", formatDou3.format(totPer) + "<TD>", formatDou.format(totAmt) + "<TD>"});
                data.put(Integer.toString(ni++), new Object[]{"", "COUNTING SKU<TH>", formatDou0.format(cntSku) + "<TD>", formatDou0.format(cntPck) + "<TD>", formatDou3.format(cntPer) + "<TD>", formatDou.format(cntAmt) + "<TD>"});
                data.put(Integer.toString(ni++), new Object[]{"", "CORRECT<TH>", formatDou0.format(corSku) + "<TD>", formatDou0.format(corPck) + "<TD>", formatDou3.format(corPer) + "<TD>", formatDou.format(corAmt) + "<TD>"});
                data.put(Integer.toString(ni++), new Object[]{"", "INCORRECT<TH>", formatDou0.format(incSku) + "<TD>", "-<TD>", formatDou3.format(incPer) + "<TD>", formatDou.format(incAmt) + "<TD>"});
                data.put(Integer.toString(ni++), new Object[]{"", "DIFF -<TH>", formatDou0.format(dimSku) + "<TD>", "-<TD>", formatDou3.format(dimPer) + "<TD>", "-" + formatDou.format(dimAmt) + "<TD>"});
                data.put(Integer.toString(ni++), new Object[]{"", "DIFF +<TH>", formatDou0.format(dioSku) + "<TD>", "-<TD>", formatDou3.format(dioPer) + "<TD>", formatDou.format(dioAmt) + "<TD>"});

                WMSTOT930 tot = new WMSTOT930();

                tot.setTCOM("TWC");
                tot.setTOHDT(dateOn);
                tot.setTPLANT(plant);
                tot.setTWHSE(warehouse);
                tot.setTMTCL(matCtrl);
                tot.setTSKU(formatDou6.format(totSku));
                tot.setTPCK(formatDou6.format(totPck));
                tot.setTPER(formatDou6.format(totPer));
                tot.setTAMT(formatDou6.format(totAmt));
                tot.setTCOSKU(formatDou6.format(cntSku));
                tot.setTCOPCK(formatDou6.format(cntPck));
                tot.setTCOPER(formatDou6.format(cntPer));
                tot.setTCOAMT(formatDou6.format(cntAmt));
                tot.setTCRSKU(formatDou6.format(corSku));
                tot.setTCRPCK(formatDou6.format(corPck));
                tot.setTCRPER(formatDou6.format(corPer));
                tot.setTCRAMT(formatDou6.format(corAmt));
                tot.setTICSKU(formatDou6.format(incSku));
                tot.setTICPCK(formatDou6.format(incPck));
                tot.setTICPER(formatDou6.format(incPer));
                tot.setTICAMT(formatDou6.format(incAmt));
                tot.setTDMSKU(formatDou6.format(dimSku));
                tot.setTDMPCK(formatDou6.format(dimPck));
                tot.setTDMPER(formatDou6.format(dimPer));
                tot.setTDMAMT(formatDou6.format(dimAmt));
                tot.setTDPSKU(formatDou6.format(dioSku));
                tot.setTDPPCK(formatDou6.format(dioPck));
                tot.setTDPPER(formatDou6.format(dioPer));
                tot.setTDPAMT(formatDou6.format(dioAmt));
                tot.setTUSER(uid);

//                new WMSTOT930Dao().delete(tot);
//
//                new WMSTOT930Dao().add(tot);
            }

            sumamt = 0.00;
            sumsap = 0.00;
            sumwms = 0.00;
            sumdiffmiss = 0.00;
            sumdiffover = 0.00;
            sumamtmiss = 0.00;
            sumamtover = 0.00;
            sumWmsAmt = 0.00;

            data.put(Integer.toString(ni++), new Object[]{""});

            if (sheet != null) {

                if (index != 6) {
                    //Set Column Width
                    sheet.setColumnWidth(0, 1500);
                    sheet.setColumnWidth(1, 4000);
                    sheet.setColumnWidth(2, 6000);
                    sheet.setColumnWidth(3, 1500);
                    sheet.setColumnWidth(4, 1500);
                    sheet.setColumnWidth(5, 3000);
                    sheet.setColumnWidth(6, 3000);
                    sheet.setColumnWidth(7, 3000);
                    sheet.setColumnWidth(8, 3000);
                    sheet.setColumnWidth(9, 3000);
                    sheet.setColumnWidth(10, 3000);
                    sheet.setColumnWidth(11, 3000);
                    sheet.setColumnWidth(12, 3000);
                    sheet.setColumnWidth(13, 3000);
                    sheet.setColumnWidth(14, 3000);
                    sheet.setColumnWidth(15, 4000);
                    sheet.setColumnWidth(16, 4000);
                } else {
                    //Set Column Width
                    sheet.setColumnWidth(0, 1500);
                    sheet.setColumnWidth(1, 4000);
                    sheet.setColumnWidth(2, 3000);
                    sheet.setColumnWidth(3, 3000);
                    sheet.setColumnWidth(4, 3000);
                    sheet.setColumnWidth(5, 3000);
                    sheet.setColumnWidth(6, 3000);
                    sheet.setColumnWidth(7, 3000);
                    sheet.setColumnWidth(8, 3000);
                    sheet.setColumnWidth(9, 3000);
                    sheet.setColumnWidth(10, 3000);
                    sheet.setColumnWidth(11, 3000);
                    sheet.setColumnWidth(12, 3000);
                    sheet.setColumnWidth(13, 3000);
                    sheet.setColumnWidth(14, 3000);
                    sheet.setColumnWidth(15, 4000);
                    sheet.setColumnWidth(16, 4000);

                    sheet.addMergedRegion(new CellRangeAddress(2, 2, 2, 3));

                }

//
                XSSFFont my_font = workbook.createFont();
//        my_font.setBold(true);
                my_font.setFontHeight(11);

                XSSFFont my_font_bold = workbook.createFont();
                my_font_bold.setBold(true);
                my_font_bold.setFontHeight(11);

                CellStyle style_tmp_1 = workbook.createCellStyle();
                style_tmp_1.setFont(my_font_bold);

                CellStyle style_tmp_2 = workbook.createCellStyle();
                style_tmp_2.setFont(my_font_bold);
                style_tmp_2.setAlignment(HorizontalAlignment.CENTER);

                CellStyle style_tmp_3 = workbook.createCellStyle();
                style_tmp_3.setFont(my_font);

                CellStyle style_tmp_4 = workbook.createCellStyle();
                style_tmp_4.setFont(my_font);
                style_tmp_4.setAlignment(HorizontalAlignment.CENTER);

                CellStyle style_tmp_5 = workbook.createCellStyle();
                style_tmp_5.setFont(my_font);
                style_tmp_5.setAlignment(HorizontalAlignment.RIGHT);

                CellStyle style_tmp_6 = workbook.createCellStyle();
                style_tmp_6.setFont(my_font_bold);

                CellStyle style_tmp_7 = workbook.createCellStyle();
                style_tmp_7.setAlignment(HorizontalAlignment.RIGHT);

                CellStyle style_row = workbook.createCellStyle();
                style_row.setFillForegroundColor(IndexedColors.RED.index);
                style_row.setFont(my_font);
                style_row.setAlignment(HorizontalAlignment.CENTER);
                style_row.setFillPattern(FillPatternType.SOLID_FOREGROUND);

                // Iterate over data and write to sheet
                Set<String> keyset = data.keySet();
                int rownum = 0;
                for (String key : keyset) {
                    // this creates a new row in the sheet
                    Row row = sheet.createRow(rownum++);
                    Object[] objArr = data.get(key);
                    int cellnum = 0;
                    for (Object obj : objArr) {
                        // this line creates a cell in the next column of that row
                        Cell cell = row.createCell(cellnum++);
                        cell.setCellValue((String) obj);

                        if (rownum <= 5) {
                            cell.setCellStyle(style_tmp_1);
                            if (rownum == 5) {
                                cell.setCellStyle(style_tmp_2);
                            }
                        } else {
                            cell.setCellStyle(style_tmp_3);
                            if (cellnum == 1 || cellnum == 4 || cellnum == 5) {
                                cell.setCellStyle(style_tmp_4);
                            } else if (cellnum > 5) {
                                cell.setCellStyle(style_tmp_5);
                            }
                        }
                        
//                        System.out.println("index " + index);

                        if (index != 6) {
                            if (rownum > 5 && rownum <= ((ni - 100001) - 2)) {

                                if (cell.getStringCellValue().equals("")) {
//                                    System.out.println("last2 " + row.getLastCellNum());
//                                    for (int i = 0; i < row.getLastCellNum(); i++) {//For each cell in the row 
//                                        row.getCell(i).setCellStyle(style_row);//Set the style
//                                    }
//                                    System.out.println("rowNo " + row.getRowNum());
//                                    System.out.println("rownum " + rownum);
                                    
//                                    if (rownum == 88) {
//                                        cell.setCellStyle(style_row);
//                                    }
                                }
                                
                            }
                        }

////                        for (int i = 0; i < row.getLastCellNum(); i++) {
//                            if (rownum > 5 && rownum <= ((ni - 100001) - 2)) {
//                                if (cell.getStringCellValue().contains("")) {
////                                    cell.setCellStyle(style_row);
//                                    row.setRowStyle(style_row);
//                                }
//                            }
////                        }
                        if (cell.getStringCellValue().contains("Total")) {
                            cell.setCellStyle(style_tmp_6);
                        }

                        if (cell.getStringCellValue().contains("<TH>")) {
                            cell.setCellStyle(style_tmp_6);
                            cell.setCellValue(cell.getStringCellValue().replace("<TH>", ""));
                        }

                        if (cell.getStringCellValue().contains("<TD>")) {
                            cell.setCellStyle(style_tmp_7);
                            cell.setCellValue(cell.getStringCellValue().replace("<TD>", ""));
                        }

                        cell.setCellValue(cell.getStringCellValue().replace("", ""));
                    }
                }

            }

        }

        try {
            ServletContext servletContextEX = getServletContext();    // new by ji
            String realPathEX = servletContextEX.getRealPath(FILE_PATH);        // new by ji
            String saperateEX = realPathEX.contains(":") ? "\\" : "/";
            String pathEX = realPathEX + saperateEX + "Onhand_Comparison_Display_(SAP_TABLET).xlsx";

            FileOutputStream outputStream = new FileOutputStream(pathEX);
            workbook.write(outputStream);
            workbook.close();

//            ******************************************************
            File fileEX = new File(pathEX);
            int length = 0;
            ServletOutputStream outStream = response.getOutputStream();
            ServletContext context = getServletConfig().getServletContext();
            String mimetype = context.getMimeType(pathEX);

            // sets response content type
            if (mimetype == null) {
                mimetype = "application/octet-stream";
            }
            response.setContentType(mimetype);
            response.setContentLength((int) fileEX.length());
            String fileNameEX = (new File(pathEX)).getName();

            // sets HTTP header
            response.setHeader("Content-Disposition", "attachment; filename=\"" + fileNameEX + "\"");

            byte[] byteBuffer = new byte[BUFSIZE];
            DataInputStream in = new DataInputStream(new FileInputStream(fileEX));

            // reads the file's bytes and writes them to the response stream
            while ((in != null) && ((length = in.read(byteBuffer)) != -1)) {
                outStream.write(byteBuffer, 0, length);
            }

            in.close();
            outStream.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

}
