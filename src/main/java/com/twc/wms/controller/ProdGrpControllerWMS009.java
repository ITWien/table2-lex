/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.controller;

import com.twc.wms.dao.QUSPDGRPDao;
import com.twc.wms.dao.UserDao;
import com.twc.wms.entity.QUSPDGRP;
import com.twc.wms.entity.UserAuth;
import java.io.IOException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author 93176
 */
public class ProdGrpControllerWMS009 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static final String PAGE_VIEW = "../views/ProdgrpWMS009.jsp";

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        request.setAttribute("PROGRAMNAME", "WMS009/PG");
        request.setAttribute("PROGRAMDESC", "User Authorization Connect Prod Grp. Display");

        String uid = request.getParameter("uid");

        QUSPDGRPDao dao = new QUSPDGRPDao();
        List<QUSPDGRP> pList = dao.findProdGrpByUid(uid);

        UserDao dao1 = new UserDao();
        List<UserAuth> pList1 = dao1.findAll();

        UserDao dao2 = new UserDao();
        UserAuth ua = dao2.findByUid(uid);

        request.setAttribute("id", uid);
        request.setAttribute("name", ua.getName());

        request.setAttribute("UAList", pList);
        request.setAttribute("MCList", pList1);

        RequestDispatcher view = request.getRequestDispatcher(PAGE_VIEW);
        view.forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String uid = request.getParameter("user");

        request.setAttribute("PROGRAMNAME", "WMS009/PG");
        request.setAttribute("PROGRAMDESC", "User Authorization Connect Prod Grp. Display");

        QUSPDGRPDao dao = new QUSPDGRPDao();
        List<QUSPDGRP> pList = dao.findProdGrpByUid(uid);

        UserDao dao1 = new UserDao();
        List<UserAuth> pList1 = dao1.findAll();

        UserDao dao2 = new UserDao();
        UserAuth ua = dao2.findByUid(uid);

        request.setAttribute("id", uid);
        request.setAttribute("name", ua.getName());

        RequestDispatcher view = request.getRequestDispatcher(PAGE_VIEW);
        request.setAttribute("UAList", pList);
        request.setAttribute("MCList", pList1);
        view.forward(request, response);

    }

}
