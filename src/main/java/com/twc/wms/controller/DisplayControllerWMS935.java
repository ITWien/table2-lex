/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.controller;

import com.twc.wms.dao.WHDao;
import com.twc.wms.dao.WMS935Dao;
import com.twc.wms.entity.SAPONH;
import com.twc.wms.entity.WH;
import java.io.IOException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author wien
 */
public class DisplayControllerWMS935 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static final String PAGE_VIEW = "../views/displayWMS935.jsp";

    public DisplayControllerWMS935() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        request.setAttribute("PROGRAMNAME", "WMS935");
        String forward = "";
        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "Summary Stock Checking. Print");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
//    ********
        List<WH> whList = new WHDao().findAll();
        request.setAttribute("whList", whList);

        List<SAPONH> onhandDate1000List = new WMS935Dao().findOnhandDate("1000");
        request.setAttribute("onhandDate1000List", onhandDate1000List);

        List<SAPONH> onhandDate1050List = new WMS935Dao().findOnhandDate("1050");
        request.setAttribute("onhandDate1050List", onhandDate1050List);

        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

}
