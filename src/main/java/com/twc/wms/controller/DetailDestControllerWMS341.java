/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.controller;

import com.twc.wms.dao.MSSMATNDao;
import com.twc.wms.dao.QIDETAILDao;
import com.twc.wms.entity.QIDETAIL;
import com.twc.wms.dao.QIHEADDao;
import com.twc.wms.dao.QdestDao;
import com.twc.wms.entity.MSSMATN;
import com.twc.wms.entity.QIHEAD;
import com.twc.wms.entity.Qdest;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author wien
 */
public class DetailDestControllerWMS341 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static final String PAGE_VIEW = "../views/detailDestWMS341.jsp";

    public DetailDestControllerWMS341() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("PROGRAMNAME", "WMS341");
        String forward = "";
        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "RM Issue. Logistics Reprint");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
//    ********
        String dest = request.getParameter("dest");
        String wh = request.getParameter("wh");
        String mat = request.getParameter("mat");

        String[] destcod = dest.split(" : ");
        String[] whcod = wh.split(" : ");

        MSSMATNDao daomc = new MSSMATNDao();
        MSSMATN ua = daomc.findByUid(mat);

        QdestDao daode = new QdestDao();
        Qdest de = daode.findByCod(dest);

        request.setAttribute("wh", new String(wh.getBytes("iso-8859-1"), "UTF-8"));
        request.setAttribute("dest", dest + " : " + de.getDesc());
        request.setAttribute("mat", mat + " : " + ua.getName());
        request.setAttribute("whcod", whcod[0]);

        QIHEADDao dao = new QIHEADDao();
        List<MSSMATN> hd = new ArrayList<MSSMATN>();

        if (mat.contains(" : ")) {
            String[] matcod = mat.split(" : ");
            hd = dao.findMatCForDest341(destcod[0], whcod[0], matcod[0]);
        } else {
            String[] matcod = mat.split(" ");
            hd = dao.findMatCForDest341(destcod[0], whcod[0], matcod[0]);
        }

        request.setAttribute("detList", hd);

        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

}
