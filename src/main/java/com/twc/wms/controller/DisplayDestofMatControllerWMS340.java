/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.controller;

import com.twc.wms.dao.QIHEADDao;
import com.twc.wms.dao.WHDao;
import com.twc.wms.entity.MSSMATN;
import com.twc.wms.entity.WH;
import java.io.IOException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author wien
 */
public class DisplayDestofMatControllerWMS340 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static final String PAGE_VIEW = "../views/displayDestofMatWMS340.jsp";

    public DisplayDestofMatControllerWMS340() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");

        request.setAttribute("PROGRAMNAME", "WMS340");
        String forward = "";
        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "RM Issue. Logistics Receipt");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
//    ********
        String wh = request.getParameter("WHS");
        String mc = request.getParameter("MTCTRL");
        String whFT = request.getParameter("wh");

        String[] WHpart = whFT.split("-");

        WHDao dao2 = new WHDao();
        WH ua2 = dao2.findByUid(WHpart[0]);

        WHDao dao3 = new WHDao();
        WH ua3 = dao3.findByUid(WHpart[1]);

        request.setAttribute("wh", wh);
        request.setAttribute("mc", mc);
        request.setAttribute("mcF", WHpart[0]);
        request.setAttribute("nameF", ua2.getName());
        request.setAttribute("mcT", WHpart[1]);
        request.setAttribute("nameT", ua3.getName());

        QIHEADDao dao = new QIHEADDao();
        List<MSSMATN> mcList = dao.findSubMenuForMat(mc, wh);

        RequestDispatcher view = request.getRequestDispatcher(forward);
        request.setAttribute("mcList", mcList);
        view.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

}
