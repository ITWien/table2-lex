/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.controller;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.twc.wms.dao.QRKFEATUREDao;
import com.twc.wms.entity.QRKFEATURE;
import com.twc.wms.dao.QRKDETAILDao;
import com.twc.wms.dao.QRKHEADDao;
import com.twc.wms.entity.QRKDETAIL;
import com.twc.wms.dao.WarehouseDao;
import com.twc.wms.entity.QRKHEAD;
import com.twc.wms.entity.Warehouse;
import java.util.List;
import java.util.ArrayList;
import javax.servlet.http.HttpSession;

/**
 *
 * @author wien
 */
public class ConnectControllerWMS008 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static final String PAGE_VIEW = "../views/connectWMS008.jsp";

    public ConnectControllerWMS008() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("PROGRAMNAME", "WMS008/F");
        String forward = "";

        String wh = request.getParameter("wh");
        if (wh == null) {
            wh = "TWC";
        }

        QRKFEATUREDao dao = new QRKFEATUREDao();
        QRKFEATURE blank = new QRKFEATURE(wh, "", "", "", "", "", "", "", "");

        QRKFEATUREDao dao3 = new QRKFEATUREDao();
        if (dao3.check(wh).equals("t")) {
            dao.add(blank, "TUSER");
        }

        QRKFEATUREDao dao1 = new QRKFEATUREDao();
        QRKFEATURE ft = dao1.findByCod(wh);

        WarehouseDao dao2 = new WarehouseDao();

        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "Rack Connect Feature. Process");
                request.setAttribute("wh", ft.getWH());
                request.setAttribute("whn", dao2.findWHname(ft.getWH()));
                request.setAttribute("Srkno", ft.getSrkno());
                request.setAttribute("Frkno", ft.getFrkno());
                request.setAttribute("Sside", ft.getSside());
                request.setAttribute("Fside", ft.getFside());
                request.setAttribute("Scolumn", ft.getScolumn());
                request.setAttribute("Fcolumn", ft.getFcolumn());
                request.setAttribute("Srow", ft.getSrow());
                request.setAttribute("Frow", ft.getFrow());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        WarehouseDao dao4 = new WarehouseDao();
        List<Warehouse> pList = dao4.selectWarehouse();

        RequestDispatcher view = request.getRequestDispatcher(forward);
        request.setAttribute("WHList", pList);
        view.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");

        String wh = request.getParameter("wh");

        String sendMessage = "";
        String forward = "";

        String userid = request.getParameter("userid");

        request.setAttribute("PROGRAMNAME", "WMS008/F");
        request.setAttribute("PROGRAMDESC", "Rack Connect Feature. Process");

        String Srkno = request.getParameter("Srkno");
        String Frkno = request.getParameter("Frkno");
        String Sside = request.getParameter("Sside");
        String Fside = request.getParameter("Fside");
        String Scolumn = request.getParameter("Scolumn");
        String Fcolumn = request.getParameter("Fcolumn");
        String Srow = request.getParameter("Srow");
        String Frow = request.getParameter("Frow");

        if (Srkno.trim().length() == 1) {
            Srkno = "0" + Srkno;
        }

        if (Frkno.trim().length() == 1) {
            Frkno = "0" + Frkno;
        }

        if (Srkno.trim().equals("") && Frkno.trim().equals("")) {
            Srkno = "00";
            Frkno = "00";
        } else if (!Srkno.trim().equals("") && Frkno.trim().equals("")) {
            Frkno = Srkno.trim();
        } else if (Srkno.trim().equals("") && !Frkno.trim().equals("")) {
            Srkno = Frkno.trim();
        }

        if (Scolumn.trim().equals("") && Fcolumn.trim().equals("")) {
            Scolumn = "A";
            Fcolumn = "A";
        } else if (!Scolumn.trim().equals("") && Fcolumn.trim().equals("")) {
            Fcolumn = Scolumn.trim();
        } else if (Scolumn.trim().equals("") && !Fcolumn.trim().equals("")) {
            Scolumn = Fcolumn.trim();
        }

        if (Srow.trim().equals("") && Frow.trim().equals("")) {
            Srow = "1";
            Frow = "1";
        } else if (!Srow.trim().equals("") && Frow.trim().equals("")) {
            Frow = Srow.trim();
        } else if (Srow.trim().equals("") && !Frow.trim().equals("")) {
            Srow = Frow.trim();
        }

        QRKFEATUREDao dao = new QRKFEATUREDao();
        QRKFEATURE p = new QRKFEATURE(wh, Srkno, Frkno, Sside, Fside, Scolumn, Fcolumn, Srow, Frow);

        int startRkno = Integer.parseInt(Srkno);
        int finishRkno = Integer.parseInt(Frkno);

        int lenSide = 0;
        if (Sside.equals("L") && Fside.equals("R")) {
            lenSide = 2;
        } else if ((Sside.equals("L") && !Fside.equals("R")) || (!Sside.equals("L") && Fside.equals("R"))) {
            lenSide = 1;
        } else if (!Sside.equals("L") && !Fside.equals("R")) {
            lenSide = 1;
        }

        char sc = Scolumn.charAt(0);
        int startCol = sc;
        char fc = Fcolumn.charAt(0);
        int finishCol = fc;

        int startRow = Integer.parseInt(Srow);
        int finishRow = Integer.parseInt(Frow);

        List<QRKHEAD> nullZone = new ArrayList<QRKHEAD>();
        List<QRKHEAD> AddedRkno = new ArrayList<QRKHEAD>();
        List<QRKHEAD> DuplRkno = new ArrayList<QRKHEAD>();

        for (int i = startRkno; i <= finishRkno; i++) {
            for (int j = 0; j < lenSide; j++) {
                for (int k = startCol; k <= finishCol; k++) {
                    for (int l = startRow; l <= finishRow; l++) {
                        String WH = wh;
                        String RNO = Integer.toString(i);
                        if (RNO.trim().length() == 1) {
                            RNO = "0" + RNO;
                        }

                        QRKHEADDao dao4 = new QRKHEADDao();
                        QRKHEAD ua = dao4.findByCod(WH, RNO);
                        String ZONE = ua.getName();
                        if (ZONE == null) {
                            if (!nullZone.isEmpty()) {
                                if (!nullZone.get(nullZone.size() - 1).getDesc().equals(RNO)) {
                                    QRKHEAD nuz = new QRKHEAD();
                                    nuz.setDesc(RNO);
                                    nullZone.add(nuz);
                                }
                            } else {
                                QRKHEAD nuz = new QRKHEAD();
                                nuz.setDesc(RNO);
                                nullZone.add(nuz);
                            }
                        }

                        String SIDE = null;

                        if (lenSide == 1) {
                            if (j == 0) {
                                if (Sside.equals("L")) {
                                    SIDE = Sside;
                                } else if (Fside.equals("R")) {
                                    SIDE = Fside;
                                } else {
                                    SIDE = null;
                                }
                            }
                        } else if (lenSide == 2) {
                            if (j == 0) {
                                SIDE = Sside;
                            } else if (j == 1) {
                                SIDE = Fside;
                            }
                        }

                        String COL = Character.toString((char) k);
                        String ROW = Integer.toString(l);
                        QRKDETAILDao daoG = new QRKDETAILDao();
                        QRKDETAIL x = new QRKDETAIL(ZONE, WH, RNO, SIDE, COL, ROW);
                        String LOC = RNO + SIDE + COL + ROW;
                        if (daoG.check(WH, ZONE, LOC).equals("t")) {
                            if (daoG.add(x, userid)) {
                                if (!AddedRkno.isEmpty()) {
                                    if (!AddedRkno.get(AddedRkno.size() - 1).getDesc().equals(RNO)) {
                                        QRKHEAD nuz = new QRKHEAD();
                                        nuz.setDesc(RNO);
                                        AddedRkno.add(nuz);
                                    }
                                } else {
                                    QRKHEAD nuz = new QRKHEAD();
                                    nuz.setDesc(RNO);
                                    AddedRkno.add(nuz);
                                }
                            }
                        } else {
                            if (!DuplRkno.isEmpty()) {
                                if (!DuplRkno.get(DuplRkno.size() - 1).getDesc().equals(RNO)) {
                                    QRKHEAD nuz = new QRKHEAD();
                                    nuz.setDesc(RNO);
                                    DuplRkno.add(nuz);
                                }
                            } else {
                                QRKHEAD nuz = new QRKHEAD();
                                nuz.setDesc(RNO);
                                DuplRkno.add(nuz);
                            }
                        }
                    }
                }
            }
        }
        sendMessage = "<script type=\"text/javascript\">\n"
                + "            var show3 = function () {\n"
                + "                $('#myModal3').modal('show');\n"
                + "            };\n"
                + "\n"
                + "            window.setTimeout(show3, 0);\n"
                + "\n"
                + "        </script>";
        forward = PAGE_VIEW;
        request.setAttribute("sendMessage", sendMessage);

        dao.edit(p, userid);

        RequestDispatcher view = request.getRequestDispatcher(forward);
        request.setAttribute("nuzList", nullZone);
        request.setAttribute("addList", AddedRkno);
        request.setAttribute("dupList", DuplRkno);
        request.setAttribute("wh", wh);
        view.forward(request, response);
    }

}
