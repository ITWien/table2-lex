/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.controller;

import com.twc.wms.dao.QRKHEADDao;
import com.twc.wms.dao.WarehouseDao;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.twc.wms.entity.QRKHEAD;
import com.twc.wms.entity.Warehouse;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpSession;

/**
 *
 * @author wien
 */
public class CreateControllerWMS008 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static final String PAGE_VIEW = "../views/createWMS008.jsp";

    public CreateControllerWMS008() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("PROGRAMNAME", "WMS008/C");
        String forward = "";
        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "Rack Master. Display");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        WarehouseDao dao = new WarehouseDao();
        List<Warehouse> pList = dao.selectWarehouse();

        RequestDispatcher view = request.getRequestDispatcher(forward);
        request.setAttribute("WHList", pList);
        view.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");

        String sendMessage = "";
        String forward = "";

        String rkno = request.getParameter("rkno");
        String rkno2 = request.getParameter("rkno2");
        String zone = request.getParameter("zone");
        String wh = request.getParameter("warehouse");

        String userid = request.getParameter("userid");

        if (rkno.trim().length() == 1) {
            rkno = "0" + rkno;
        }
        if (rkno2.trim().length() == 1) {
            rkno2 = "0" + rkno2;
        }

        request.setAttribute("PROGRAMNAME", "WMS008/C");
        request.setAttribute("PROGRAMDESC", "Rack Master. Display");

        List<QRKHEAD> AddedRkno = new ArrayList<QRKHEAD>();
        List<QRKHEAD> DuplRkno = new ArrayList<QRKHEAD>();

        int startRkno = Integer.parseInt(rkno);
        int finishRkno = Integer.parseInt(rkno2);

        for (int i = startRkno; i <= finishRkno; i++) {
            String RNO = Integer.toString(i);
            if (RNO.trim().length() == 1) {
                RNO = "0" + RNO;
            }

            QRKHEAD ua = new QRKHEAD(wh, RNO, zone);
            QRKHEADDao dao = new QRKHEADDao();

            if (dao.check(RNO, wh).equals("t")) {
                if (dao.add(ua, userid)) {
                    if (!AddedRkno.isEmpty()) {
                        if (!AddedRkno.get(AddedRkno.size() - 1).getDesc().equals(RNO)) {
                            QRKHEAD nuz = new QRKHEAD();
                            nuz.setDesc(RNO);
                            AddedRkno.add(nuz);
                        }
                    } else {
                        QRKHEAD nuz = new QRKHEAD();
                        nuz.setDesc(RNO);
                        AddedRkno.add(nuz);
                    }
                }
            } else {
                if (!DuplRkno.isEmpty()) {
                    if (!DuplRkno.get(DuplRkno.size() - 1).getDesc().equals(RNO)) {
                        QRKHEAD nuz = new QRKHEAD();
                        nuz.setDesc(RNO);
                        DuplRkno.add(nuz);
                    }
                } else {
                    QRKHEAD nuz = new QRKHEAD();
                    nuz.setDesc(RNO);
                    DuplRkno.add(nuz);
                }
            }
        }

        sendMessage = "<script type=\"text/javascript\">\n"
                + "            var show3 = function () {\n"
                + "                $('#myModal3').modal('show');\n"
                + "            };\n"
                + "\n"
                + "            window.setTimeout(show3, 0);\n"
                + "\n"
                + "        </script>";
        forward = PAGE_VIEW;
        request.setAttribute("sendMessage", sendMessage);

        RequestDispatcher view = request.getRequestDispatcher(forward);
        request.setAttribute("addList", AddedRkno);
        request.setAttribute("dupList", DuplRkno);
        view.forward(request, response);
    }
}
