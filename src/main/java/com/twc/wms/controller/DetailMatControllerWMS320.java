/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.controller;

import com.twc.wms.dao.MSSMATNDao;
import com.twc.wms.dao.QIDETAILDao;
import com.twc.wms.entity.QIDETAIL;
import com.twc.wms.dao.QIHEADDao;
import com.twc.wms.dao.WarehouseDao;
import com.twc.wms.entity.MSSMATN;
import com.twc.wms.entity.QIHEAD;
import com.twc.wms.entity.Warehouse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author wien
 */
public class DetailMatControllerWMS320 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static final String PAGE_VIEW = "../views/detailMatWMS320.jsp";

    public DetailMatControllerWMS320() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("PROGRAMNAME", "WMS320");
        String forward = "";
        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "RM Issue. Logistics Forecast");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
//    ********
        String mat = request.getParameter("mat");
        String wh = request.getParameter("wh");
        String dest = request.getParameter("dest");

        WarehouseDao dao2 = new WarehouseDao();
        String whn = dao2.findWHname(wh);

        MSSMATNDao dao1 = new MSSMATNDao();
        MSSMATN ob1 = dao1.findByUid(mat);

        request.setAttribute("wh", wh);
        request.setAttribute("mat", mat);
        request.setAttribute("whn", whn);
        request.setAttribute("matn", ob1.getName());

        QIHEADDao dao = new QIHEADDao();
        List<MSSMATN> hd = new ArrayList<MSSMATN>();

        if (dest.contains(" : ")) {
            String[] destcod = dest.split(" : ");
            hd = dao.findDestForMat(mat, wh, destcod[0]);
        } else {
            hd = dao.findDestForMatAll(mat, wh);
        }

        request.setAttribute("detList", hd);

        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

}
