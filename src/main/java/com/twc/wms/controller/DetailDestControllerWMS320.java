/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.controller;

import com.twc.wms.dao.QIDETAILDao;
import com.twc.wms.entity.QIDETAIL;
import com.twc.wms.dao.QIHEADDao;
import com.twc.wms.entity.MSSMATN;
import com.twc.wms.entity.QIHEAD;
import java.io.IOException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author wien
 */
public class DetailDestControllerWMS320 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static final String PAGE_VIEW = "../views/detailDestWMS320.jsp";

    public DetailDestControllerWMS320() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("PROGRAMNAME", "WMS320");
        String forward = "";
        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "RM Issue. Logistics Forecast");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
//    ********
        String dest = request.getParameter("dest");
        String wh = request.getParameter("wh");

        String[] destcod = dest.split(" : ");
        String[] whcod = wh.split(" : ");

        request.setAttribute("wh", new String(wh.getBytes("iso-8859-1"), "UTF-8"));
        request.setAttribute("dest", new String(dest.getBytes("iso-8859-1"), "UTF-8"));

        QIHEADDao dao = new QIHEADDao();
        List<MSSMATN> hd = dao.findMatCForDest(destcod[0], whcod[0]);

        request.setAttribute("detList", hd);

        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

}
