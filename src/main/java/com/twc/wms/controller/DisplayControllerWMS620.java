/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.controller;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.twc.wms.dao.WarehouseDao;
import com.twc.wms.entity.Warehouse;
import com.twc.wms.dao.QRMTRA_2Dao;
import com.twc.wms.entity.QRMTRA_2;
import java.util.List;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;
import com.twc.wms.dao.QRKDETAILDao;
import com.twc.wms.entity.QRKDETAIL;
import java.io.FileOutputStream;
import com.itextpdf.text.Font.FontFamily;
import javax.servlet.ServletContext;

/**
 *
 * @author wien
 */
public class DisplayControllerWMS620 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static final String PAGE_VIEW = "../views/displayWMS620.jsp";
    private static final String FILE_PATH = "/report/QR/";

    public DisplayControllerWMS620() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("PROGRAMNAME", "WMS620");
        String forward = "";
        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "RM Issue. Report");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
//    ********

        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

}
