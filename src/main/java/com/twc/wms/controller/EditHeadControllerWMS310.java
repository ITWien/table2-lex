/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.controller;

import com.twc.wms.dao.QIDETAILDao;
import com.twc.wms.entity.QIDETAIL;
import com.twc.wms.dao.QIHEADDao;
import com.twc.wms.dao.QMVTDao;
import com.twc.wms.dao.QdestDao;
import com.twc.wms.entity.QIHEAD;
import com.twc.wms.entity.Qdest;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author wien
 */
public class EditHeadControllerWMS310 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;

    public EditHeadControllerWMS310() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");

        String qno = request.getParameter("queueAdd");
        String wh = request.getParameter("warehouseAdd");
        String mvt = request.getParameter("mvtAdd");
        String dest = request.getParameter("destAdd");

//        System.out.println(qno);
//        System.out.println(wh);
//        System.out.println(mvt);
//        System.out.println(dest.split(" ")[0]);
        QIHEADDao dao3 = new QIHEADDao();
        dao3.UpdateMvtDest(mvt, dest.split(" ")[0], wh, qno);

        response.setHeader("Refresh", "0;/TABLE2/WMS310/edit?qno=" + qno + "&wh=" + wh);
    }

}
