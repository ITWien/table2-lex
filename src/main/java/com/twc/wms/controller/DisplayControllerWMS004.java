/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.controller;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import com.twc.wms.entity.MSSMATN;
import com.twc.wms.dao.MSSMATNDao;
import javax.servlet.http.HttpSession;

/**
 *
 * @author wien
 */
public class DisplayControllerWMS004 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static final String PAGE_VIEW = "../views/displayWMS004.jsp";

    public DisplayControllerWMS004() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("PROGRAMNAME", "WMS004");
        String forward = "";

        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "Material Control Master. Display");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        MSSMATNDao dao = new MSSMATNDao();
        List<MSSMATN> pList = dao.findAll();

        RequestDispatcher view = request.getRequestDispatcher(forward);
        request.setAttribute("MCList", pList);
        view.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

}
