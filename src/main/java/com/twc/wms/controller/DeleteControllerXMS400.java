/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.controller;

import com.twc.wms.dao.QRMMATSTSDao;
import com.twc.wms.dao.QRMSTSDao;
import com.twc.wms.dao.QdestDao;
import com.twc.wms.dao.XMSOTDETAILDao;
import com.twc.wms.dao.XMSOTHEADDao;
import com.twc.wms.entity.QDESTYP;
import com.twc.wms.entity.Qdest;
import java.io.IOException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author wien
 */
public class DeleteControllerXMS400 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;

    public DeleteControllerXMS400() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String qno = request.getParameter("qno");
        String wh = request.getParameter("wh");
        String shipDate = request.getParameter("shipDate");

        new XMSOTHEADDao().delete(qno);
        new XMSOTDETAILDao().delete(qno);

        response.setHeader("Refresh", "0;/TABLE2/XMS400/display?wh=" + wh + "&shipDate=" + shipDate);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

}
