package com.twc.wms.controller;

import com.twc.wms.dao.ISM600Dao;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author 93176
 */
public class HoldControllerISM600 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        request.setCharacterEncoding("utf-8");

        String[] id = request.getParameterValues("selectCk");
        String shipDate = request.getParameter("shipDate");
        String wh = request.getParameter("wh");
        String dest = request.getParameter("dest");

        String userid = request.getParameter("userid");

        if (id != null) {
            for (int i = 0; i < id.length; i++) {
                String mvt = new ISM600Dao().findMVT(wh.trim(), id[i].trim().split("-")[1]);
                if (id[i].trim().split("-")[0].length() < 17) {
                    ISM600Dao daoq = new ISM600Dao();
                    List<String> idList = daoq.UngroupQRID(id[i].trim());
                    for (int j = 0; j < idList.size(); j++) {
                        ISM600Dao dao2 = new ISM600Dao();
                        if (dao2.HoldISM600(idList.get(j).trim(), userid)) {
                            ISM600Dao up = new ISM600Dao();
                            if (!mvt.trim().equals("601")) {
                                up.UpdateStatus(idList.get(j).split("-")[0], "5"); // มี update ใน QRMMAS ด้วย
                            }
                        }

                        ISM600Dao dao = new ISM600Dao();
                        String[] mnx = dao.getSTS(wh.trim(), id[i].split("-")[1].trim());

                        ISM600Dao dao4 = new ISM600Dao();
                        String tqty = dao4.getTQY(wh.trim(), id[i].split("-")[1].trim());

                        DecimalFormat formatDou = new DecimalFormat("#,##0.00");
                        if (tqty == null) {
                            double tot = Double.parseDouble("0.000");
                            tqty = formatDou.format(tot);
                        } else {
                            double tot = Double.parseDouble(tqty);
                            tqty = formatDou.format(tot);
                        }

                        ISM600Dao dao3 = new ISM600Dao();
                        dao3.SETSTS(mnx[0], mnx[1], wh.trim(), id[i].split("-")[1].trim(), tqty.trim().replace(",", ""));

                    }
                } else if (id[i].trim().split("-")[0].length() == 17) {
                    ISM600Dao dao2 = new ISM600Dao();
                    if (dao2.HoldISM600(id[i].trim(), userid)) {
                        ISM600Dao up = new ISM600Dao();
                        if (!mvt.trim().equals("601")) {
                            up.UpdateStatus(id[i].split("-")[0], "5"); // มี update ใน QRMMAS ด้วย
                        }
                    }

                    ISM600Dao dao = new ISM600Dao();
                    String[] mnx = dao.getSTS(wh.trim(), id[i].split("-")[1].trim());

                    ISM600Dao dao4 = new ISM600Dao();
                    String tqty = dao4.getTQY(wh.trim(), id[i].split("-")[1].trim());

                    DecimalFormat formatDou = new DecimalFormat("#,##0.00");
                    if (tqty == null) {
                        double tot = Double.parseDouble("0.000");
                        tqty = formatDou.format(tot);
                    } else {
                        double tot = Double.parseDouble(tqty);
                        tqty = formatDou.format(tot);
                    }

                    ISM600Dao dao3 = new ISM600Dao();
                    dao3.SETSTS(mnx[0], mnx[1], wh.trim(), id[i].split("-")[1].trim(), tqty.trim().replace(",", ""));

                }
            }
        }

        response.setHeader("Refresh", "0;/TABLE2/ISM600/display?shipDate=" + shipDate + "&wh=" + wh + "&dest=" + dest);

    }

}
