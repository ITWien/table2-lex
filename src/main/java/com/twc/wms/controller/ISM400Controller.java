/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.controller;

import com.twc.wms.dao.MASMDAO;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author wien
 */
public class ISM400Controller extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static String LOGIN = "../views/loginISM400.jsp";
    private static String APPROVED = "../views/approvedISM400.jsp";
    private MASMDAO MASMDAO;

    public ISM400Controller() {
        super();
        MASMDAO = new MASMDAO();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        request.setAttribute("PROGRAMNAME", "ISM400");
        request.setAttribute("PROGRAMDESC", "Sales Order Approval. Process");

        String forward = "";
        String page = "";
        String action = "";

        HttpSession session = request.getSession(false);

        if (session != null && session.getAttribute("user") != null) {

            String USER = (String) session.getAttribute("user");
            request.setAttribute("USER", USER);
            forward = APPROVED;

            if (request.getParameter("a") != null) {
                action = request.getParameter("a");
            }

            if (action.equalsIgnoreCase("list")) {

                String POFG = request.getParameter("POFG");
                String VIEW = request.getParameter("VIEW");
                String GRPNO = request.getParameter("GRPNO");
                String ISSUENO = request.getParameter("ISSUENO");

                request.setAttribute("POFG", POFG);
                request.setAttribute("VIEW", VIEW);
                request.setAttribute("GRPNO", GRPNO);
                request.setAttribute("ISSUENO", ISSUENO);
                request.setAttribute("listIssue", MASMDAO.getListApproved(POFG, VIEW, GRPNO, ISSUENO));

            }

        } else {
            forward = LOGIN;
        }

        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

}
