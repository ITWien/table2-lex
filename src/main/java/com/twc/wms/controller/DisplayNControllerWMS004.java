/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.controller;

import com.twc.wms.dao.SAPMASDao;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.twc.wms.entity.SAPMAS;

/**
 *
 * @author wien
 */
public class DisplayNControllerWMS004 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static final String PAGE_VIEW = "../views/displayNWMS004.jsp";

    public DisplayNControllerWMS004() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("PROGRAMNAME", "WMS004/ND");
        String forward = "";
        String mat = request.getParameter("uid");
        String vt = request.getParameter("vt");
        String plant = request.getParameter("plant");
        SAPMASDao dao = new SAPMASDao();
        SAPMAS ua = dao.findAll(mat, vt, plant);
        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "Mat Ctrl Connect Mat Code. Display");
                request.setAttribute("MatCode", ua.getMatcode());
                request.setAttribute("Desc", ua.getDesc());
                request.setAttribute("Plant", ua.getPlant());
                request.setAttribute("Vtype", ua.getVtype());
                request.setAttribute("Mtype", ua.getMtype());
                request.setAttribute("UM", ua.getUm());
                request.setAttribute("Mgroup", ua.getMgroup());
                request.setAttribute("MgroupDesc", ua.getMgdesc());
                request.setAttribute("MatCtrl", ua.getMatCtrl());
                request.setAttribute("MatCtrlDesc", ua.getMatCtrlName());
                request.setAttribute("Pgroup", ua.getPgroup());
                request.setAttribute("PgroupName", ua.getPgroupName());
                request.setAttribute("Location", ua.getLocation());

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
