/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.controller;

import com.twc.wms.dao.QIDETAILDao;
import com.twc.wms.entity.QIDETAIL;
import com.twc.wms.dao.QIHEADDao;
import com.twc.wms.dao.QRMMASDao;
import com.twc.wms.entity.QIHEAD;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author wien
 */
public class CancelControllerWMS340 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;

    public CancelControllerWMS340() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");

        String matCtrl = request.getParameter("matCtrl");
        String[] id = request.getParameterValues("selectCk-" + matCtrl);
        String mat = request.getParameter("mat");
        String wh = request.getParameter("wh");
        String dest = request.getParameter("dest");
        String remark = request.getParameter("remark-" + matCtrl);

        String userid = request.getParameter("userid");

        if (id != null) {
            for (int i = 0; i < id.length; i++) {
                if (id[i].trim().split("-")[0].length() < 17) {
                    QIDETAILDao daoq = new QIDETAILDao();
                    List<String> idList = daoq.UngroupQRID(id[i].trim());
                    for (int j = 0; j < idList.size(); j++) {
                        QIDETAILDao dao2 = new QIDETAILDao();
                        if (dao2.cancel(idList.get(j).trim(), userid, remark)) {
                            QRMMASDao up = new QRMMASDao();
                            up.UpdateStatus(idList.get(j).split("-")[0], "2");
                        }

                        QIDETAILDao dao = new QIDETAILDao();
                        String[] mnx = dao.getSTS(wh.trim(), id[i].split("-")[1].trim());

                        QIDETAILDao dao4 = new QIDETAILDao();
                        String tqty = dao4.getTQY(wh.trim(), id[i].split("-")[1].trim());

                        DecimalFormat formatDou = new DecimalFormat("#,##0.00");
                        if (tqty == null) {
                            double tot = Double.parseDouble("0.000");
                            tqty = formatDou.format(tot);
                        } else {
                            double tot = Double.parseDouble(tqty);
                            tqty = formatDou.format(tot);
                        }

                        QIHEADDao dao3 = new QIHEADDao();
                        dao3.SETSTS(mnx[0], mnx[1], wh.trim(), id[i].split("-")[1].trim(), tqty.trim().replace(",", ""));

                    }
                } else if (id[i].trim().split("-")[0].length() == 17) {
                    QIDETAILDao dao2 = new QIDETAILDao();
                    if (dao2.cancel(id[i].trim(), userid, remark)) {
                        QRMMASDao up = new QRMMASDao();
                        up.UpdateStatus(id[i].split("-")[0], "2");
                    }

                    QIDETAILDao dao = new QIDETAILDao();
                    String[] mnx = dao.getSTS(wh.trim(), id[i].split("-")[1].trim());

                    QIDETAILDao dao4 = new QIDETAILDao();
                    String tqty = dao4.getTQY(wh.trim(), id[i].split("-")[1].trim());

                    DecimalFormat formatDou = new DecimalFormat("#,##0.00");
                    if (tqty == null) {
                        double tot = Double.parseDouble("0.000");
                        tqty = formatDou.format(tot);
                    } else {
                        double tot = Double.parseDouble(tqty);
                        tqty = formatDou.format(tot);
                    }

                    QIHEADDao dao3 = new QIHEADDao();
                    dao3.SETSTS(mnx[0], mnx[1], wh.trim(), id[i].split("-")[1].trim(), tqty.trim().replace(",", ""));

                }
            }
        }

        String[] m1 = matCtrl.split(" ");
        String[] m2 = mat.split(" ");

        String[] destcod = dest.split(" ");
        String[] whcod = wh.split(" ");
        String[] matcod = mat.split(" ");

        if (m1[0].equals(m2[0]) || m2[0].equals("TOTAL")) {
            response.setHeader("Refresh", "0;/TABLE2/WMS340/detailDest?mat=" + matcod[0] + "&wh=" + whcod[0] + "&dest=" + destcod[0]);
        } else {
            response.setHeader("Refresh", "0;/TABLE2/WMS340/detailMat?mat=" + matcod[0] + "&wh=" + whcod[0] + "&dest=" + destcod[0]);
        }
    }

}
