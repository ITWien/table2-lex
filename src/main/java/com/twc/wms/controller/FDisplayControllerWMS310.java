/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.controller;

import com.twc.wms.dao.WHDao;
import com.twc.wms.entity.WH;
import com.twc.wms.dao.QIHEADDao;
import com.twc.wms.entity.QIHEAD;
import java.io.IOException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author wien
 */
public class FDisplayControllerWMS310 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static final String PAGE_VIEW = "../views/FdisplayWMS310.jsp";

    public FDisplayControllerWMS310() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("PROGRAMNAME", "WMS310");
        String forward = "";
        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "RM Issue. Approval");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
//    ********
        WHDao dao2 = new WHDao();
        List<WH> pList2 = dao2.findAll();

        String wh = request.getParameter("WHS");
        String matCtrl = request.getParameter("MTCTRL");
        String whF = request.getParameter("whF");
        String whT = request.getParameter("whT");

        request.setAttribute("wh", wh);
        request.setAttribute("mc", matCtrl);

        WHDao dao1 = new WHDao();
        WH ua = dao1.findByUid(wh);

        request.setAttribute("whn", ua.getName());

        QIHEADDao dao4 = new QIHEADDao();
        List<QIHEAD> MTCList = dao4.findAll(wh, matCtrl);

        request.setAttribute("whF", whF);
        request.setAttribute("whT", whT);

        RequestDispatcher view = request.getRequestDispatcher(forward);
        request.setAttribute("MCList", pList2);
        request.setAttribute("MTCList", MTCList);
        view.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");

        request.setAttribute("PROGRAMNAME", "WMS310");
        String forward = "";
        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "RM Issue. Approval");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
//    ********
        WHDao dao2 = new WHDao();
        List<WH> pList2 = dao2.findAll();

        String wh = request.getParameter("warehouse");
        String matCtrl = request.getParameter("matCtrl");

        request.setAttribute("wh", wh);
        request.setAttribute("mc", matCtrl);

        WHDao dao1 = new WHDao();
        WH ua = dao1.findByUid(wh);

        request.setAttribute("whn", ua.getName());

        QIHEADDao dao4 = new QIHEADDao();
        List<QIHEAD> MTCList = dao4.findAll(wh, matCtrl);

        RequestDispatcher view = request.getRequestDispatcher(forward);
        request.setAttribute("MCList", pList2);
        request.setAttribute("MTCList", MTCList);
        view.forward(request, response);
    }

}
