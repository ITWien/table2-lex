/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.controller;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.twc.wms.dao.WarehouseDao;
import com.twc.wms.entity.Warehouse;
import com.twc.wms.dao.QRMTRA_3Dao;
import com.twc.wms.entity.QRMTRA_3;
import java.util.List;

import com.itextpdf.text.*;
import com.itextpdf.*;
import com.itextpdf.text.pdf.*;
import java.io.FileOutputStream;
import com.itextpdf.text.Font.FontFamily;
import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.ServletContext;
import com.itextpdf.text.pdf.PRStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.ResultSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.ArrayList;
import java.text.DecimalFormat;
import java.util.HashMap;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRResultSetDataSource;
import net.sf.jasperreports.engine.JasperRunManager;

/**
 *
 * @author wien
 */
public class PrintControllerWMS620 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static final String PAGE_VIEW = "../views/printWMS620.jsp";
    private static final String FILE_PATH = "/report/";
    private static final String FILE_DEST = "/report/";
    private static final String FILE_FONT = "/resources/vendors/fonts/";
    private static final DecimalFormat formatDou = new DecimalFormat("#,###.000");
    private static final DecimalFormat formatInt = new DecimalFormat("#,###");

    public PrintControllerWMS620() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("PROGRAMNAME", "WMS620");
        String forward = "";
        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "RM Issue. Report");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
//    ********
        WarehouseDao dao = new WarehouseDao();
        List<Warehouse> pList = dao.selectWarehouse();

        QRMTRA_3Dao dao2 = new QRMTRA_3Dao();
        List<QRMTRA_3> qmList = dao2.selectPO();
//        System.out.println(qmList.size());

        QRMTRA_3Dao dao3 = new QRMTRA_3Dao();
        List<QRMTRA_3> taxList = dao3.selectTax();
//        System.out.println(taxList.size());

        QRMTRA_3Dao dao4 = new QRMTRA_3Dao();
        List<QRMTRA_3> plantList = dao4.selectPlant();
//        System.out.println(plantList.size());

        QRMTRA_3Dao dao5 = new QRMTRA_3Dao();
        List<QRMTRA_3> matList = dao5.selectMatCode();
//        System.out.println(matList.size());

        QRMTRA_3Dao dao6 = new QRMTRA_3Dao();
        List<QRMTRA_3> usList = dao6.selectUser();
//        System.out.println(usList.size());

        RequestDispatcher view = request.getRequestDispatcher(forward);
        request.setAttribute("WHList", pList);
        request.setAttribute("QMList", qmList);
        request.setAttribute("taxList", taxList);
        request.setAttribute("plantList", plantList);
        request.setAttribute("matList", matList);
        request.setAttribute("usList", usList);
        view.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        request.setCharacterEncoding("utf-8");

        request.setAttribute("PROGRAMNAME", "WMS620");
        request.setAttribute("PROGRAMDESC", "RM Issue. Report");

        String Swh = request.getParameter("Swh");
        String Fwh = request.getParameter("Fwh");
        String StranDate = request.getParameter("StranDate");
        String FtranDate = request.getParameter("FtranDate");
        String Splant = request.getParameter("Splant");
        String Fplant = request.getParameter("Fplant");
        String Spo = request.getParameter("Spo");
        String Fpo = request.getParameter("Fpo");
        String Stax = request.getParameter("Stax");
        String Ftax = request.getParameter("Ftax");
        String SmatCode = request.getParameter("SmatCode");
        String FmatCode = request.getParameter("FmatCode");
        String Suser = request.getParameter("Suser");
        String Fuser = request.getParameter("Fuser");

        if (!Swh.trim().equals("") && Fwh.trim().equals("")) {
            Fwh = Swh;
        } else if (Swh.trim().equals("") && !Fwh.trim().equals("")) {
            Swh = Fwh;
        }

        if (FtranDate.trim().equals("")) {
            FtranDate = StranDate;
        }

        if (!Splant.trim().equals("") && Fplant.trim().equals("")) {
            Fplant = Splant;
        } else if (Splant.trim().equals("") && !Fplant.trim().equals("")) {
            Splant = Fplant;
        }

        if (!Spo.trim().equals("") && Fpo.trim().equals("")) {
            Fpo = Spo;
        } else if (Spo.trim().equals("") && !Fpo.trim().equals("")) {
            Spo = Fpo;
        }

        if (!Stax.trim().equals("") && Ftax.trim().equals("")) {
            Ftax = Stax;
        } else if (Stax.trim().equals("") && !Ftax.trim().equals("")) {
            Stax = Ftax;
        }

        if (!SmatCode.trim().equals("") && FmatCode.trim().equals("")) {
            FmatCode = SmatCode;
        } else if (SmatCode.trim().equals("") && !FmatCode.trim().equals("")) {
            SmatCode = FmatCode;
        }

        if (!Suser.trim().equals("") && Fuser.trim().equals("")) {
            Fuser = Suser;
        } else if (Suser.trim().equals("") && !Fuser.trim().equals("")) {
            Suser = Fuser;
        }

// ************************************** SUMMARY ****************************************************************
        try {
            String Sdd = StranDate.substring(8, 10);
            int Smm = Integer.parseInt(StranDate.substring(5, 7));
            String Syyyy = StranDate.substring(0, 4);
            String Sdate = Sdd + " " + ConvertMonth(Smm) + " " + Syyyy;

            String Fdd = FtranDate.substring(8, 10);
            int Fmm = Integer.parseInt(FtranDate.substring(5, 7));
            String Fyyyy = FtranDate.substring(0, 4);
            String Fdate = Fdd + " " + ConvertMonth(Fmm) + " " + Fyyyy;

            QRMTRA_3Dao dao5 = new QRMTRA_3Dao();
            ResultSet result = dao5.findTranDate(StranDate, FtranDate, Swh, Fwh, Splant, Fplant,
                    Spo, Fpo, Stax, Ftax, SmatCode, FmatCode, Suser, Fuser);

            String tdbDate = "";
            if (Sdate.equals(Fdate)) {
                tdbDate = Sdate;
            } else if ((Smm == Fmm) && (Syyyy.equals(Fyyyy))) {
                tdbDate = Sdd + " - " + Fdd + " " + ConvertMonth(Smm) + " " + Syyyy;
            } else {
                tdbDate = Sdate + " - " + Fdate;
            }

            File reportFile = new File(getServletConfig().getServletContext()
                    .getRealPath("/resources/jasper/WMS620.jasper"));
            byte[] bytes;

            HashMap<String, Object> map = new HashMap<String, Object>();
            map.put("SELECTDATE", tdbDate);

            try {
                bytes = JasperRunManager.runReportToPdf(reportFile.getPath(), map, new JRResultSetDataSource(result));

                ServletContext servletContext2 = getServletContext();
                String realPath2 = servletContext2.getRealPath(FILE_DEST);
                String saperate2 = realPath2.contains(":") ? "\\" : "/";
                String path2 = realPath2 + saperate2 + "WMS620_Summary.pdf";

                FileOutputStream out = new FileOutputStream(path2);
                out.write(bytes, 0, bytes.length);
//                out.flush();
//                out.close();
            } catch (JRException e) {
                StringWriter stringWriter = new StringWriter();
                PrintWriter printWriter = new PrintWriter(stringWriter);
                e.printStackTrace(printWriter);
//                response.setContentType("text/plain");
//                response.getOutputStream().print(stringWriter.toString());

            }

        } catch (Exception e) {
            System.err.println(e.toString());
        }
// ************************************** SUMMARY ****************************************************************

// ************************************** DETAIL ****************************************************************
        try {

            String Sdd = StranDate.substring(8, 10);
            int Smm = Integer.parseInt(StranDate.substring(5, 7));
            String Syyyy = StranDate.substring(0, 4);
            String Sdate = Sdd + " " + ConvertMonth(Smm) + " " + Syyyy;

            String Fdd = FtranDate.substring(8, 10);
            int Fmm = Integer.parseInt(FtranDate.substring(5, 7));
            String Fyyyy = FtranDate.substring(0, 4);
            String Fdate = Fdd + " " + ConvertMonth(Fmm) + " " + Fyyyy;

            QRMTRA_3Dao dao5 = new QRMTRA_3Dao();
            ResultSet result = dao5.findDetail(StranDate, FtranDate, Swh, Fwh, Splant, Fplant,
                    Spo, Fpo, Stax, Ftax, SmatCode, FmatCode, Suser, Fuser);

            String tdbDate = "";
            if (Sdate.equals(Fdate)) {
                tdbDate = Sdate;
            } else if ((Smm == Fmm) && (Syyyy.equals(Fyyyy))) {
                tdbDate = Sdd + " - " + Fdd + " " + ConvertMonth(Smm) + " " + Syyyy;
            } else {
                tdbDate = Sdate + " - " + Fdate;
            }

            File reportFile = new File(getServletConfig().getServletContext()
                    .getRealPath("/resources/jasper/WMS621.jasper"));
            byte[] bytes;

            HashMap<String, Object> map = new HashMap<String, Object>();
            map.put("SELECTDATE", tdbDate);

            try {
                bytes = JasperRunManager.runReportToPdf(reportFile.getPath(), map, new JRResultSetDataSource(result));

                ServletContext servletContext2 = getServletContext();
                String realPath2 = servletContext2.getRealPath(FILE_DEST);
                String saperate2 = realPath2.contains(":") ? "\\" : "/";
                String path2 = realPath2 + saperate2 + "WMS621_Detail.pdf";

                FileOutputStream out = new FileOutputStream(path2);
                out.write(bytes, 0, bytes.length);
//                out.flush();
//                out.close();
            } catch (JRException e) {
                StringWriter stringWriter = new StringWriter();
                PrintWriter printWriter = new PrintWriter(stringWriter);
                e.printStackTrace(printWriter);
//                response.setContentType("text/plain");
//                response.getOutputStream().print(stringWriter.toString());

            }

        } catch (Exception e) {
            System.err.println(e.toString());
        }

// ************************************** DETAIL ****************************************************************
        String forward = PAGE_VIEW;
        String showPDF = "";

        showPDF = "<script type=\"text/javascript\">\n"
                + "                 window.onload = function (e) {\n"
                + "                     window.open(\"/TABLE2/WMS620/display\");\n"
                + "                 };\n"
                + "        </script>";

        request.setAttribute("showPDF", showPDF);

        WarehouseDao dao1 = new WarehouseDao();
        WarehouseDao dao3 = new WarehouseDao();

        request.setAttribute("Swh", Swh);
        request.setAttribute("Fwh", Fwh);
        request.setAttribute("Swhn", dao1.findWHname(Swh));
        request.setAttribute("Fwhn", dao3.findWHname(Fwh));
        request.setAttribute("StranDate", StranDate);
        request.setAttribute("FtranDate", FtranDate);
        request.setAttribute("Splant", Splant);
        request.setAttribute("Fplant", Fplant);
        request.setAttribute("Spo", Spo);
        request.setAttribute("Fpo", Fpo);
        request.setAttribute("Stax", Stax);
        request.setAttribute("Ftax", Ftax);
        request.setAttribute("SmatCode", SmatCode);
        request.setAttribute("FmatCode", FmatCode);
        request.setAttribute("Suser", Suser);
        request.setAttribute("Fuser", Fuser);

        WarehouseDao dao = new WarehouseDao();
        List<Warehouse> pList = dao.selectWarehouse();

        QRMTRA_3Dao dao2 = new QRMTRA_3Dao();
        List<QRMTRA_3> qmList = dao2.selectPO();

        QRMTRA_3Dao dao7 = new QRMTRA_3Dao();
        List<QRMTRA_3> taxList = dao7.selectTax();

        QRMTRA_3Dao dao4 = new QRMTRA_3Dao();
        List<QRMTRA_3> plantList = dao4.selectPlant();

        QRMTRA_3Dao dao5 = new QRMTRA_3Dao();
        List<QRMTRA_3> matList = dao5.selectMatCode();

        QRMTRA_3Dao dao6 = new QRMTRA_3Dao();
        List<QRMTRA_3> usList = dao6.selectUser();

        RequestDispatcher view = request.getRequestDispatcher(forward);
        request.setAttribute("WHList", pList);
        request.setAttribute("QMList", qmList);
        request.setAttribute("taxList", taxList);
        request.setAttribute("plantList", plantList);
        request.setAttribute("matList", matList);
        request.setAttribute("usList", usList);
        view.forward(request, response);
    }

    public PdfPCell getCell(String text, int alignment, String srcFont, boolean underLine) throws IOException, DocumentException {
        Font fontThai = new Font(BaseFont.createFont(srcFont,
                BaseFont.IDENTITY_H, BaseFont.EMBEDDED), 16);
        PdfPCell cell = new PdfPCell(new Phrase(text, fontThai));
        cell.setPadding(0);
        cell.setHorizontalAlignment(alignment);
        if (underLine) {
            cell.setBorder(PdfPCell.BOTTOM);
            cell.setPaddingBottom(2);
        } else {
            cell.setBorder(PdfPCell.NO_BORDER);
        }
        return cell;
    }

    public PdfPCell DETgetCell(String text, int alignment, String srcFont, boolean underLine) throws IOException, DocumentException {
        Font fontThai = new Font(BaseFont.createFont(srcFont,
                BaseFont.IDENTITY_H, BaseFont.EMBEDDED), 13);
        PdfPCell cell = new PdfPCell(new Phrase(text, fontThai));
        cell.setPadding(0);
        cell.setHorizontalAlignment(alignment);
        if (underLine) {
            cell.setBorder(PdfPCell.BOTTOM);
            cell.setPaddingBottom(2);
        } else {
            cell.setBorder(PdfPCell.NO_BORDER);
        }
        return cell;
    }

    public void manipulatePdf(String src, String dest, String srcFont, String dateTime, String tdbDate, List<Warehouse> WHpList) throws IOException, DocumentException {
        Font fontThai = new Font(BaseFont.createFont(srcFont,
                BaseFont.IDENTITY_H, BaseFont.EMBEDDED), 18);
        PdfReader reader = new PdfReader(src);
        PdfStamper stamper = new PdfStamper(reader, new FileOutputStream(dest));
        PdfContentByte pagecontent;
        int n = 0;
        String fm = "";
        int t = 1;
        for (int i = 0; i < WHpList.size(); i++) {
            n = Integer.parseInt(WHpList.get(i).getCode());
            fm = "";
            if (n < 10) {
                fm = "%s\u00a0\u00a0";
            } else {
                fm = "%s";
            }
            for (int j = 0; j < n; j++) {
                pagecontent = stamper.getOverContent(t++);
                ColumnText.showTextAligned(pagecontent, Element.ALIGN_RIGHT,
                        new Phrase(String.format(fm, n), fontThai), 827, 571, 0);
                ColumnText.showTextAligned(pagecontent, Element.ALIGN_LEFT,
                        new Phrase(String.format(dateTime), fontThai), 620, 570, 0);
                ColumnText.showTextAligned(pagecontent, Element.ALIGN_CENTER,
                        new Phrase(String.format("RM Issue. Summary Report"), fontThai), 422, 570, 0);
                ColumnText.showTextAligned(pagecontent, Element.ALIGN_LEFT,
                        new Phrase(String.format("   WMS620"), fontThai), 5, 570, 0);
                ColumnText.showTextAligned(pagecontent, Element.ALIGN_CENTER,
                        new Phrase(String.format(tdbDate), fontThai), 422, 553, 0);
                ColumnText.showTextAligned(pagecontent, Element.ALIGN_LEFT,
                        new Phrase(String.format("   Warehouse : "), fontThai), 5, 530, 0);
                ColumnText.showTextAligned(pagecontent, Element.ALIGN_LEFT,
                        new Phrase(String.format("   Plant : "), fontThai), 5, 507, 0);
                ColumnText.showTextAligned(pagecontent, Element.ALIGN_LEFT,
                        new Phrase(String.format("Trans Date      "
                                + "Doc no.          "
                                + "Invoice no.            "
                                + "Material Code   "
                                + "Name                           "
                                + "                               "
                                + "        "
                                + "Quantity   "
                                + "U/M    "
                                + "Box   "
                                + "Bag   "
                                + "Roll"), fontThai), 15, 484, 0);
                ColumnText.showTextAligned(pagecontent, Element.ALIGN_CENTER,
                        new Phrase(String.format("_______________________"
                                + "______________________________________"
                                + "______________________________________"
                                + "______________________________________"
                                + "________"), fontThai), 419, 477, 0);
                ColumnText.showTextAligned(pagecontent, Element.ALIGN_CENTER,
                        new Phrase(String.format("_______________________"
                                + "______________________________________"
                                + "______________________________________"
                                + "______________________________________"
                                + "________"), fontThai), 419, 500, 0);

                ColumnText.showTextAligned(pagecontent, Element.ALIGN_CENTER,
                        new Phrase(String.format("_______________________"
                                + "______________________________________"
                                + "______________________________________"
                                + "______________________________________"
                                + "________"), fontThai), 423, 477, 0);
                ColumnText.showTextAligned(pagecontent, Element.ALIGN_CENTER,
                        new Phrase(String.format("_______________________"
                                + "______________________________________"
                                + "______________________________________"
                                + "______________________________________"
                                + "________"), fontThai), 423, 500, 0);
            }

        }
        stamper.close();
        reader.close();
    }

    public void DETmanipulatePdf(String src, String dest, String srcFont, String dateTime, String tdbDate, List<Warehouse> WHpList) throws IOException, DocumentException {
        Font fontThai = new Font(BaseFont.createFont(srcFont,
                BaseFont.IDENTITY_H, BaseFont.EMBEDDED), 14);
        PdfReader reader = new PdfReader(src);
        PdfStamper stamper = new PdfStamper(reader, new FileOutputStream(dest));
        PdfContentByte pagecontent;
        int n = 0;
        String fm = "";
        int t = 1;
        for (int i = 0; i < WHpList.size(); i++) {
            n = Integer.parseInt(WHpList.get(i).getCode());
            fm = "";
            if (n < 10) {
                fm = "%s\u00a0\u00a0";
            } else {
                fm = "%s";
            }
            for (int j = 0; j < n; j++) {
                pagecontent = stamper.getOverContent(t++);
                ColumnText.showTextAligned(pagecontent, Element.ALIGN_RIGHT,
                        new Phrase(String.format(fm, n), fontThai), 827, 575.5f, 0);
                ColumnText.showTextAligned(pagecontent, Element.ALIGN_LEFT,
                        new Phrase(String.format(dateTime), fontThai), 610, 575.5f, 0);
                ColumnText.showTextAligned(pagecontent, Element.ALIGN_CENTER,
                        new Phrase(String.format("RM Issue. Detail Report"), fontThai), 422, 575.5f, 0);
                ColumnText.showTextAligned(pagecontent, Element.ALIGN_LEFT,
                        new Phrase(String.format("   WMS621"), fontThai), 5, 575.5f, 0);
                ColumnText.showTextAligned(pagecontent, Element.ALIGN_CENTER,
                        new Phrase(String.format(tdbDate), fontThai), 422, 558.5f, 0);
                ColumnText.showTextAligned(pagecontent, Element.ALIGN_LEFT,
                        new Phrase(String.format("   Warehouse : "), fontThai), 5, 538, 0);
                ColumnText.showTextAligned(pagecontent, Element.ALIGN_LEFT,
                        new Phrase(String.format("   Plant : "), fontThai), 5, 521, 0);
                ColumnText.showTextAligned(pagecontent, Element.ALIGN_LEFT,
                        new Phrase(String.format("Trans Date       "
                                + "Doc no.            "
                                + "Invoice no.             "
                                + "Material Code   "
                                + "Name                           "
                                + "                                "
                                + "ID                             "
                                + "      "
                                + "Quantity  "
                                + "U/M  "
                                + "Package  "
                                + "No.  "
                                + "User"), fontThai), 15, 502, 0);
                ColumnText.showTextAligned(pagecontent, Element.ALIGN_CENTER,
                        new Phrase(String.format("_______________________"
                                + "______________________________________"
                                + "______________________________________"
                                + "______________________________________"
                                + "______________________________________"
                                + "___________"), fontThai), 419, 495, 0);
                ColumnText.showTextAligned(pagecontent, Element.ALIGN_CENTER,
                        new Phrase(String.format("_______________________"
                                + "______________________________________"
                                + "______________________________________"
                                + "______________________________________"
                                + "______________________________________"
                                + "___________"), fontThai), 419, 517, 0);

                ColumnText.showTextAligned(pagecontent, Element.ALIGN_CENTER,
                        new Phrase(String.format("_______________________"
                                + "______________________________________"
                                + "______________________________________"
                                + "______________________________________"
                                + "______________________________________"
                                + "___________"), fontThai), 423, 495, 0);
                ColumnText.showTextAligned(pagecontent, Element.ALIGN_CENTER,
                        new Phrase(String.format("_______________________"
                                + "______________________________________"
                                + "______________________________________"
                                + "______________________________________"
                                + "______________________________________"
                                + "___________"), fontThai), 423, 517, 0);
            }

        }
        stamper.close();
        reader.close();
    }

    public String ConvertMonth(int mm) {
        String Smonth = "";
        switch (mm) {
            case 1:
                Smonth = "January";
                break;
            case 2:
                Smonth = "February";
                break;
            case 3:
                Smonth = "March";
                break;
            case 4:
                Smonth = "April";
                break;
            case 5:
                Smonth = "May";
                break;
            case 6:
                Smonth = "June";
                break;
            case 7:
                Smonth = "July";
                break;
            case 8:
                Smonth = "August";
                break;
            case 9:
                Smonth = "September";
                break;
            case 10:
                Smonth = "October";
                break;
            case 11:
                Smonth = "November";
                break;
            case 12:
                Smonth = "December";
                break;
            default:
                Smonth = "Invalid month";
                break;
        }
        return Smonth;
    }

}
