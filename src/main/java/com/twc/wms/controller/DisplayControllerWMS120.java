/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.controller;

import com.twc.wms.dao.PALMOVEDao;
import com.twc.wms.dao.QIDETAILDao;
import com.twc.wms.dao.WHDao;
import com.twc.wms.entity.WH;
import com.twc.wms.dao.QIHEADDao;
import com.twc.wms.dao.QRMMASDao;
import com.twc.wms.dao.QdestDao;
import com.twc.wms.entity.MSSMATN;
import com.twc.wms.entity.PALMOVE;
import com.twc.wms.entity.QIDETAIL;
import com.twc.wms.entity.Qdest;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author wien
 */
public class DisplayControllerWMS120 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static final String PAGE_VIEW = "../views/displayWMS120.jsp";

    public DisplayControllerWMS120() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        request.setAttribute("PROGRAMNAME", "WMS120");
        String forward = "";
        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "Pallet Movement. Entry");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
//    ********
        String userid = request.getParameter("userid");

        PALMOVEDao daoP = new PALMOVEDao();
        List<PALMOVE> PALList = daoP.findAll(userid);

        RequestDispatcher view = request.getRequestDispatcher(forward);
        request.setAttribute("PALList", PALList);
        view.forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("PROGRAMNAME", "WMS120");
        String forward = "";
        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "Pallet Movement. Entry");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
//    ********
        String userid = request.getParameter("userid");

        String from = request.getParameter("from");
        String to = request.getParameter("to");
        String[] ID = request.getParameterValues("ID");

        String sendMessage = "";

        int suc = 0;
        int fail = 0;
        int dup = 0;
        if (!from.equals("")) {
            QRMMASDao dao = new QRMMASDao();
            List<PALMOVE> palList = dao.findPallet(from);
            for (int i = 0; i < palList.size(); i++) {
                PALMOVEDao daock = new PALMOVEDao();
                if (daock.check(palList.get(i).getNo()).equals("t")) {
                    PALMOVEDao daoa = new PALMOVEDao();
                    if (daoa.add(palList.get(i).getNo(), palList.get(i).getId(), palList.get(i).getCode(), palList.get(i).getDesc(), userid)) {
                        suc += 1;
                    } else {
                        fail += 1;
                    }
                } else {
                    dup += 1;
                }
            }

            sendMessage = "<script type=\"text/javascript\">\n"
                    + "            var show2 = function () {\n"
                    + "                $('#myModal2').modal('show');\n"
                    + "            };\n"
                    + "\n"
                    + "            window.setTimeout(show2, 0);\n"
                    + "\n"
                    + "        </script>";
            request.setAttribute("sendMessage", sendMessage);

            String s = "<h4 align=\"center\">Success : " + suc + "</h4>";
            String f = "<h4 align=\"center\">Fail : " + fail + "</h4>";
            String d = "<h4 align=\"center\">Duplicate : " + dup + "</h4>";
            String n = "<h4 align=\"center\">No Data !</h4>";

            if (suc != 0) {
                request.setAttribute("Success", s);
            }
            if (fail != 0) {
                request.setAttribute("Fail", f);
            }
            if (dup != 0) {
                request.setAttribute("Duplicate", d);
            }
            if ((suc + fail + dup) == 0) {
                request.setAttribute("nodata", n);
            }

        } else if (!to.equals("")) {
            if (ID != null) {
                for (int i = 0; i < ID.length; i++) {
                    PALMOVEDao daoa = new PALMOVEDao();
                    if (daoa.editTO(ID[i], to)) {
                        suc += 1;
                    } else {
                        fail += 1;
                    }
                }

                sendMessage = "<script type=\"text/javascript\">\n"
                        + "            var show3 = function () {\n"
                        + "                $('#myModal3').modal('show');\n"
                        + "            };\n"
                        + "\n"
                        + "            window.setTimeout(show3, 0);\n"
                        + "\n"
                        + "        </script>";
                request.setAttribute("sendMessage", sendMessage);
                String s = "<h4 align=\"center\">Success : " + suc + "</h4>";
                String f = "<h4 align=\"center\">Fail : " + fail + "</h4>";

                if (suc != 0) {
                    request.setAttribute("Success", s);
                }
                if (fail != 0) {
                    request.setAttribute("Fail", f);
                }
            }
        }

        PALMOVEDao daoP = new PALMOVEDao();
        List<PALMOVE> PALList = daoP.findAll(userid);

        RequestDispatcher view = request.getRequestDispatcher(forward);
        request.setAttribute("PALList", PALList);
        view.forward(request, response);
    }

}
