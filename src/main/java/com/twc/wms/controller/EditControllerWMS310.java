/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.controller;

import com.twc.wms.dao.QIDETAILDao;
import com.twc.wms.entity.QIDETAIL;
import com.twc.wms.dao.QIHEADDao;
import com.twc.wms.dao.QMVTDao;
import com.twc.wms.dao.QdestDao;
import com.twc.wms.entity.QIHEAD;
import com.twc.wms.entity.Qdest;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author wien
 */
public class EditControllerWMS310 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static final String PAGE_VIEW = "../views/editWMS310.jsp";

    public EditControllerWMS310() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("PROGRAMNAME", "WMS310");
        String forward = "";
        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "RM Issue. Approval");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
//    ********
        String qno = request.getParameter("qno");
        String wh = request.getParameter("wh");
        String show = request.getParameter("show");

//        QIDETAILDao dao4 = new QIDETAILDao();
//        String[] mnx = dao4.getSTS(wh.trim(), qno.trim());
//
//        QIDETAILDao dao5 = new QIDETAILDao();
//        String tqty = dao5.getTQY(wh.trim(), qno.trim());
//
//        DecimalFormat formatDou = new DecimalFormat("#,##0.00");
//        if (tqty == null) {
//            double tot = Double.parseDouble("0.000");
//            tqty = formatDou.format(tot);
//        } else {
//            double tot = Double.parseDouble(tqty);
//            tqty = formatDou.format(tot);
//        }
//
//        QIHEADDao dao3 = new QIHEADDao();
//        dao3.SETSTS(mnx[0], mnx[1], wh.trim(), qno.trim(), tqty.trim());
        QIHEADDao dao = new QIHEADDao();
        QIHEAD hd = dao.findByQno(wh, qno);

        String[] part = hd.getMatCtrl().split(" : ");

        request.setAttribute("wh", hd.getWh());
        request.setAttribute("whn", hd.getWhn());
        request.setAttribute("dest", hd.getDest());
        request.setAttribute("pgroup", hd.getProductGroup());
        request.setAttribute("mvt", hd.getMvt());
        request.setAttribute("mvtn", hd.getMvtn());
        request.setAttribute("mc", hd.getMatCtrl());
        request.setAttribute("mc1", part[0]);
        request.setAttribute("transDate", hd.getTransDate());
        request.setAttribute("set", hd.getSet());
        request.setAttribute("setTotal", hd.getSetTotal());
        request.setAttribute("user", hd.getUser());
        request.setAttribute("qno", hd.getQueueNo());
        request.setAttribute("totalQty", hd.getTotalQty());

        QIDETAILDao daoP = new QIDETAILDao();
        String plant = daoP.findPLANT(qno);
        if (plant != null) {
            if (plant.trim().equals("1000")) {
                request.setAttribute("plantF", plant.trim());
                request.setAttribute("plantT", "1050");
            } else if (plant.trim().equals("1050")) {
                request.setAttribute("plantF", plant.trim());
                request.setAttribute("plantT", "1000");
            }
        }

        QIDETAILDao dao2 = new QIDETAILDao();
        List<QIDETAIL> deList = dao2.findAll(wh, qno);
        request.setAttribute("deList", deList);

        request.setAttribute("size", deList.size());

        String sendM = "";
        if (show != null) {
            if (show.equals("show")) {
                sendM = "<script type=\"text/javascript\">\n"
                        + "            var show = function () {\n"
                        + "                $('#myModal').modal('show');\n"
                        + "            };\n"
                        + "\n"
                        + "            window.setTimeout(show, 0);\n"
                        + "\n"
                        + "        </script>";
            }
        }
        request.setAttribute("sendM", sendM);

        QdestDao dao4 = new QdestDao();
        List<Qdest> destList = dao4.findByWhs(wh);
        request.setAttribute("destList", destList);

        QMVTDao dao5 = new QMVTDao();
        List<Qdest> mvtList = dao5.findAll();
        request.setAttribute("mvtList", mvtList);

        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

}
