/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.controller;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.twc.wms.dao.QPGMAS_2Dao;
import com.twc.wms.entity.QPGMAS_2;
import com.twc.wms.dao.QPGTYPEDao;
import com.twc.wms.entity.QPGTYPE;
import java.util.List;

/**
 *
 * @author wien
 */
public class DisplayControllerWMS011 extends HttpServlet {

  private static final long serialVersionUID = 4707490878358448870L;
  private static final String PAGE_VIEW = "../views/displayWMS011.jsp";

  public DisplayControllerWMS011() {
  }

  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    request.setAttribute("PROGRAMNAME", "WMS011");
    String forward = "";
    try {
      String action = request.getParameter("action");
      if (action == null) {
        forward = PAGE_VIEW;
        request.setAttribute("PROGRAMDESC", "Program Master. Display");
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
//    ********
    QPGMAS_2Dao dao = new QPGMAS_2Dao();
    List<QPGMAS_2> pList = dao.findAll();
    
    RequestDispatcher view = request.getRequestDispatcher(forward);
    request.setAttribute("QPGMASList", pList);
    view.forward(request, response);
  }

  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    
  }

}
