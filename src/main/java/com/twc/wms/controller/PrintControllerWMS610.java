/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.controller;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.twc.wms.dao.WarehouseDao;
import com.twc.wms.entity.Warehouse;
import com.twc.wms.dao.QRMTRA_2Dao;
import com.twc.wms.entity.QRMTRA_2;
import java.util.List;

import com.itextpdf.text.*;
import com.itextpdf.*;
import com.itextpdf.text.pdf.*;
import java.io.FileOutputStream;
import com.itextpdf.text.Font.FontFamily;
import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.ServletContext;
import com.itextpdf.text.pdf.PRStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.ResultSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.ArrayList;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.ServletOutputStream;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.j2ee.servlets.*;

/**
 *
 * @author wien
 */
public class PrintControllerWMS610 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static final String PAGE_VIEW = "../views/printWMS610.jsp";
    private static final String FILE_PATH = "/report/";
    private static final String FILE_DEST = "/report/";
    private static final String FILE_FONT = "/resources/vendors/fonts/";
    private static final DecimalFormat formatDou = new DecimalFormat("#,###.000");
    private static final DecimalFormat formatInt = new DecimalFormat("#,###");

    public PrintControllerWMS610() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("PROGRAMNAME", "WMS610");
        String forward = "";
        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "RM Receipt. Report");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
//    ********
        WarehouseDao dao = new WarehouseDao();
        List<Warehouse> pList = dao.selectWarehouse();

        QRMTRA_2Dao dao2 = new QRMTRA_2Dao();
        List<QRMTRA_2> qmList = dao2.selectPO();

        QRMTRA_2Dao dao3 = new QRMTRA_2Dao();
        List<QRMTRA_2> taxList = dao3.selectTax();

        QRMTRA_2Dao dao4 = new QRMTRA_2Dao();
        List<QRMTRA_2> plantList = dao4.selectPlant();

        QRMTRA_2Dao dao5 = new QRMTRA_2Dao();
        List<QRMTRA_2> matList = dao5.selectMatCode();

        QRMTRA_2Dao dao6 = new QRMTRA_2Dao();
        List<QRMTRA_2> usList = dao6.selectUser();

        RequestDispatcher view = request.getRequestDispatcher(forward);
        request.setAttribute("WHList", pList);
        request.setAttribute("QMList", qmList);
        request.setAttribute("taxList", taxList);
        request.setAttribute("plantList", plantList);
        request.setAttribute("matList", matList);
        request.setAttribute("usList", usList);
        view.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        request.setCharacterEncoding("utf-8");

        request.setAttribute("PROGRAMNAME", "WMS610");
        request.setAttribute("PROGRAMDESC", "RM Receipt. Report");

        String Swh = request.getParameter("Swh");
        String Fwh = request.getParameter("Fwh");
        String StranDate = request.getParameter("StranDate");
        String FtranDate = request.getParameter("FtranDate");
        String Splant = request.getParameter("Splant");
        String Fplant = request.getParameter("Fplant");
        String Spo = request.getParameter("Spo");
        String Fpo = request.getParameter("Fpo");
        String Stax = request.getParameter("Stax");
        String Ftax = request.getParameter("Ftax");
        String SmatCode = request.getParameter("SmatCode");
        String FmatCode = request.getParameter("FmatCode");
        String Suser = request.getParameter("Suser");
        String Fuser = request.getParameter("Fuser");

        if (!Swh.trim().equals("") && Fwh.trim().equals("")) {
            Fwh = Swh;
        } else if (Swh.trim().equals("") && !Fwh.trim().equals("")) {
            Swh = Fwh;
        }

        if (FtranDate.trim().equals("")) {
            FtranDate = StranDate;
        }

        if (!Splant.trim().equals("") && Fplant.trim().equals("")) {
            Fplant = Splant;
        } else if (Splant.trim().equals("") && !Fplant.trim().equals("")) {
            Splant = Fplant;
        }

        if (!Spo.trim().equals("") && Fpo.trim().equals("")) {
            Fpo = Spo;
        } else if (Spo.trim().equals("") && !Fpo.trim().equals("")) {
            Spo = Fpo;
        }

        if (!Stax.trim().equals("") && Ftax.trim().equals("")) {
            Ftax = Stax;
        } else if (Stax.trim().equals("") && !Ftax.trim().equals("")) {
            Stax = Ftax;
        }

        if (!SmatCode.trim().equals("") && FmatCode.trim().equals("")) {
            FmatCode = SmatCode;
        } else if (SmatCode.trim().equals("") && !FmatCode.trim().equals("")) {
            SmatCode = FmatCode;
        }

        if (!Suser.trim().equals("") && Fuser.trim().equals("")) {
            Fuser = Suser;
        } else if (Suser.trim().equals("") && !Fuser.trim().equals("")) {
            Suser = Fuser;
        }

// ************************************** SUMMARY ****************************************************************
        try {

            String Sdd = StranDate.substring(8, 10);
            int Smm = Integer.parseInt(StranDate.substring(5, 7));
            String Syyyy = StranDate.substring(0, 4);
            String Sdate = Sdd + " " + ConvertMonth(Smm) + " " + Syyyy;

            String Fdd = FtranDate.substring(8, 10);
            int Fmm = Integer.parseInt(FtranDate.substring(5, 7));
            String Fyyyy = FtranDate.substring(0, 4);
            String Fdate = Fdd + " " + ConvertMonth(Fmm) + " " + Fyyyy;

            QRMTRA_2Dao dao5 = new QRMTRA_2Dao();
            ResultSet result = dao5.findTranDate(StranDate, FtranDate, Swh, Fwh, Splant, Fplant,
                    Spo, Fpo, Stax, Ftax, SmatCode, FmatCode, Suser, Fuser);

            String tdbDate = "";
            if (Sdate.equals(Fdate)) {
                tdbDate = Sdate;
            } else if ((Smm == Fmm) && (Syyyy.equals(Fyyyy))) {
                tdbDate = Sdd + " - " + Fdd + " " + ConvertMonth(Smm) + " " + Syyyy;
            } else {
                tdbDate = Sdate + " - " + Fdate;
            }

            File reportFile = new File(getServletConfig().getServletContext()
                    .getRealPath("/resources/jasper/WMS610.jasper"));
            byte[] bytes;

            HashMap<String, Object> map = new HashMap<String, Object>();
            map.put("SELECTDATE", tdbDate);

            try {
                bytes = JasperRunManager.runReportToPdf(reportFile.getPath(), map, new JRResultSetDataSource(result));

                ServletContext servletContext2 = getServletContext();
                String realPath2 = servletContext2.getRealPath(FILE_DEST);
                String saperate2 = realPath2.contains(":") ? "\\" : "/";
                String path2 = realPath2 + saperate2 + "WMS610_Summary.pdf";

                FileOutputStream out = new FileOutputStream(path2);
                out.write(bytes, 0, bytes.length);
//                out.flush();
//                out.close();
            } catch (JRException e) {
                StringWriter stringWriter = new StringWriter();
                PrintWriter printWriter = new PrintWriter(stringWriter);
                e.printStackTrace(printWriter);
//                response.setContentType("text/plain");
//                response.getOutputStream().print(stringWriter.toString());

            }

        } catch (Exception e) {
            System.err.println(e.toString());
        }
// ************************************** SUMMARY ****************************************************************
// ************************************** DETAIL ****************************************************************
        try {
            String Sdd = StranDate.substring(8, 10);
            int Smm = Integer.parseInt(StranDate.substring(5, 7));
            String Syyyy = StranDate.substring(0, 4);
            String Sdate = Sdd + " " + ConvertMonth(Smm) + " " + Syyyy;

            String Fdd = FtranDate.substring(8, 10);
            int Fmm = Integer.parseInt(FtranDate.substring(5, 7));
            String Fyyyy = FtranDate.substring(0, 4);
            String Fdate = Fdd + " " + ConvertMonth(Fmm) + " " + Fyyyy;

            QRMTRA_2Dao dao5 = new QRMTRA_2Dao();
            ResultSet result = dao5.findDetail(StranDate, FtranDate, Swh, Fwh, Splant, Fplant,
                    Spo, Fpo, Stax, Ftax, SmatCode, FmatCode, Suser, Fuser);

            String tdbDate = "";
            if (Sdate.equals(Fdate)) {
                tdbDate = Sdate;
            } else if ((Smm == Fmm) && (Syyyy.equals(Fyyyy))) {
                tdbDate = Sdd + " - " + Fdd + " " + ConvertMonth(Smm) + " " + Syyyy;
            } else {
                tdbDate = Sdate + " - " + Fdate;
            }

            File reportFile = new File(getServletConfig().getServletContext()
                    .getRealPath("/resources/jasper/WMS611.jasper"));
            byte[] bytes;

            HashMap<String, Object> map = new HashMap<String, Object>();
            map.put("SELECTDATE", tdbDate);

            try {
                bytes = JasperRunManager.runReportToPdf(reportFile.getPath(), map, new JRResultSetDataSource(result));

                ServletContext servletContext2 = getServletContext();
                String realPath2 = servletContext2.getRealPath(FILE_DEST);
                String saperate2 = realPath2.contains(":") ? "\\" : "/";
                String path2 = realPath2 + saperate2 + "WMS611_Detail.pdf";

                FileOutputStream out = new FileOutputStream(path2);
                out.write(bytes, 0, bytes.length);
//                out.flush();
//                out.close();
            } catch (JRException e) {
                StringWriter stringWriter = new StringWriter();
                PrintWriter printWriter = new PrintWriter(stringWriter);
                e.printStackTrace(printWriter);
//                response.setContentType("text/plain");
//                response.getOutputStream().print(stringWriter.toString());

            }

        } catch (Exception e) {
            System.err.println(e.toString());
        }
// ************************************** DETAIL ****************************************************************
        String forward = PAGE_VIEW;
        String showPDF = "";

        showPDF = "<script type=\"text/javascript\">\n"
                + "                 window.onload = function (e) {\n"
                + "                     window.open(\"/TABLE2/WMS610/display\");\n"
                + "                 };\n"
                + "        </script>";

        request.setAttribute("showPDF", showPDF);

        WarehouseDao dao1 = new WarehouseDao();
        WarehouseDao dao3 = new WarehouseDao();

        request.setAttribute("Swh", Swh);
        request.setAttribute("Fwh", Fwh);
        request.setAttribute("Swhn", dao1.findWHname(Swh));
        request.setAttribute("Fwhn", dao3.findWHname(Fwh));
        request.setAttribute("StranDate", StranDate);
        request.setAttribute("FtranDate", FtranDate);
        request.setAttribute("Splant", Splant);
        request.setAttribute("Fplant", Fplant);
        request.setAttribute("Spo", Spo);
        request.setAttribute("Fpo", Fpo);
        request.setAttribute("Stax", Stax);
        request.setAttribute("Ftax", Ftax);
        request.setAttribute("SmatCode", SmatCode);
        request.setAttribute("FmatCode", FmatCode);
        request.setAttribute("Suser", Suser);
        request.setAttribute("Fuser", Fuser);

        WarehouseDao dao = new WarehouseDao();
        List<Warehouse> pList = dao.selectWarehouse();

        QRMTRA_2Dao dao2 = new QRMTRA_2Dao();
        List<QRMTRA_2> qmList = dao2.selectPO();

        QRMTRA_2Dao dao7 = new QRMTRA_2Dao();
        List<QRMTRA_2> taxList = dao7.selectTax();

        QRMTRA_2Dao dao4 = new QRMTRA_2Dao();
        List<QRMTRA_2> plantList = dao4.selectPlant();

        QRMTRA_2Dao dao5 = new QRMTRA_2Dao();
        List<QRMTRA_2> matList = dao5.selectMatCode();

        QRMTRA_2Dao dao6 = new QRMTRA_2Dao();
        List<QRMTRA_2> usList = dao6.selectUser();

        RequestDispatcher view = request.getRequestDispatcher(forward);
        request.setAttribute("WHList", pList);
        request.setAttribute("QMList", qmList);
        request.setAttribute("taxList", taxList);
        request.setAttribute("plantList", plantList);
        request.setAttribute("matList", matList);
        request.setAttribute("usList", usList);
        view.forward(request, response);
    }

    public String ConvertMonth(int mm) {
        String Smonth = "";
        switch (mm) {
            case 1:
                Smonth = "January";
                break;
            case 2:
                Smonth = "February";
                break;
            case 3:
                Smonth = "March";
                break;
            case 4:
                Smonth = "April";
                break;
            case 5:
                Smonth = "May";
                break;
            case 6:
                Smonth = "June";
                break;
            case 7:
                Smonth = "July";
                break;
            case 8:
                Smonth = "August";
                break;
            case 9:
                Smonth = "September";
                break;
            case 10:
                Smonth = "October";
                break;
            case 11:
                Smonth = "November";
                break;
            case 12:
                Smonth = "December";
                break;
            default:
                Smonth = "Invalid month";
                break;
        }
        return Smonth;
    }

}
