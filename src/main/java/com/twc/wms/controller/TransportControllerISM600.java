package com.twc.wms.controller;

import com.twc.wms.dao.ISM600Dao;
import com.twc.wms.dao.QdestDao;
import com.twc.wms.dao.WHDao;
import com.twc.wms.entity.Qdest;
import com.twc.wms.entity.WH;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.ResultSet;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRResultSetDataSource;
import net.sf.jasperreports.engine.JasperRunManager;

/**
 *
 * @author 93176
 */
public class TransportControllerISM600 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;

    public TransportControllerISM600() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        request.setCharacterEncoding("utf-8");
        boolean check = false;
        String[] id = request.getParameterValues("selectCk");
//        String shipDate = request.getParameter("shipDate");
        String wh = request.getParameter("wh");
        String dest = request.getParameter("dest");
        String round = request.getParameter("round");
        String shipDateX2 = request.getParameter("shipDateX2");

        WHDao daofwhn = new WHDao();
        WH uawh = daofwhn.findByUid(wh);

        QdestDao daodes = new QdestDao();
        Qdest uade = daodes.findByCod(dest);

        String userid = request.getParameter("userid");

        if (id != null) {
            for (int i = 0; i < id.length; i++) {
                String mvt = new ISM600Dao().findMVT(wh.trim(), id[i].trim().split("-")[1]);
                if (id[i].trim().split("-")[0].length() < 17) {
                    ISM600Dao daoq = new ISM600Dao();
                    List<String> idList = daoq.UngroupQRID(id[i].trim());
                    for (int j = 0; j < idList.size(); j++) {
//                        ISM600Dao daoid = new ISM600Dao();
//                        if (daoid.CheckApprove(idList.get(j).trim()).equals("5")) {
                        ISM600Dao dao2 = new ISM600Dao();
                        if (dao2.transport(idList.get(j).trim(), userid)) {
                            ISM600Dao up = new ISM600Dao();
                            if (!mvt.trim().equals("601")) {
                                up.UpdateStatus(idList.get(j).split("-")[0], "6");
                                ISM600Dao adTRA = new ISM600Dao();
                                adTRA.AddQRMTRA(idList.get(j).split("-")[0], userid);
                            }

                        }

                        ISM600Dao dao = new ISM600Dao();
                        String[] mnx = dao.getSTS(wh.trim(), id[i].split("-")[1].trim());

                        ISM600Dao dao4 = new ISM600Dao();
                        String tqty = dao4.getTQY(wh.trim(), id[i].split("-")[1].trim());

                        DecimalFormat formatDou = new DecimalFormat("#,##0.00");
                        if (tqty == null) {
                            double tot = Double.parseDouble("0.000");
                            tqty = formatDou.format(tot);
                        } else {
                            double tot = Double.parseDouble(tqty);
                            tqty = formatDou.format(tot);
                        }

                        ISM600Dao dao3 = new ISM600Dao();
                        dao3.SETSTS(mnx[0], mnx[1], wh.trim(), id[i].split("-")[1].trim(), tqty.trim().replace(",", ""));
//                        }
                    }
                } else if (id[i].trim().split("-")[0].length() == 17) {
//                    ISM600Dao daoid = new ISM600Dao();
//                    if (daoid.CheckApprove(id[i].trim()).equals("5")) {
                    ISM600Dao dao2 = new ISM600Dao();
                    if (dao2.transport(id[i].trim(), userid)) {
                        ISM600Dao up = new ISM600Dao();
                        if (!mvt.trim().equals("601")) {
                            up.UpdateStatus(id[i].split("-")[0], "6");
                            ISM600Dao adTRA = new ISM600Dao();
                            adTRA.AddQRMTRA(id[i].split("-")[0], userid);
                        }

                    }

                    ISM600Dao dao = new ISM600Dao();
                    String[] mnx = dao.getSTS(wh.trim(), id[i].split("-")[1].trim());

                    ISM600Dao dao4 = new ISM600Dao();
                    String tqty = dao4.getTQY(wh.trim(), id[i].split("-")[1].trim());

                    DecimalFormat formatDou = new DecimalFormat("#,##0.00");
                    if (tqty == null) {
                        double tot = Double.parseDouble("0.000");
                        tqty = formatDou.format(tot);
                    } else {
                        double tot = Double.parseDouble(tqty);
                        tqty = formatDou.format(tot);
                    }

                    ISM600Dao dao3 = new ISM600Dao();
                    dao3.SETSTS(mnx[0], mnx[1], wh.trim(), id[i].split("-")[1].trim(), tqty.trim().replace(",", ""));
//                    }
                }
//                ****************ROUND*************************
                ISM600Dao dao2 = new ISM600Dao();
                check = dao2.roundX(id[i].trim(), userid, round, shipDateX2);
            }
//            ********************PRINT******************
            if (check) {
                try {

                    ISM600Dao dao = new ISM600Dao();
                    ResultSet result = dao.findForPrintISM600X(id, wh, dest);

                    ServletOutputStream servletOutputStream = response.getOutputStream();
                    File reportFile = new File(getServletConfig().getServletContext()
                            .getRealPath("/resources/jasper/XMS350.jasper"));
                    byte[] bytes;

                    HashMap<String, Object> map = new HashMap<String, Object>();
                    map.put("WH", wh);
                    map.put("DEST", dest);
                    map.put("WHN", uawh.getName());
                    map.put("DESTN", uade.getDesc());
//                map.put("SHIPDATE", shipDate);

                    try {
                        bytes = JasperRunManager.runReportToPdf(reportFile.getPath(), map, new JRResultSetDataSource(result));
                        response.setContentType("application/pdf");
                        response.setContentLength(bytes.length);

                        servletOutputStream.write(bytes, 0, bytes.length);
                        servletOutputStream.flush();
                        servletOutputStream.close();

                    } catch (JRException e) {
                        StringWriter stringWriter = new StringWriter();
                        PrintWriter printWriter = new PrintWriter(stringWriter);
                        e.printStackTrace(printWriter);
                        response.setContentType("text/plain");
                        response.getOutputStream().print(stringWriter.toString());

                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
//        response.setHeader("Refresh", "0;/TABLE2/XMS350/display?wh=" + wh + "&dest=" + dest + "&sts=6");
        response.setHeader("Refresh", "0;/TABLE2/ISM600/print?wh=" + wh + "&dest=" + dest + "&shipDate=" + shipDateX2 + "&round=" + round);

    }

}
