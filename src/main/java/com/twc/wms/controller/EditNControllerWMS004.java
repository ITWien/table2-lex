/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.controller;

import com.twc.wms.dao.MSSMATNDao;
import com.twc.wms.dao.SAPMASDao;
import com.twc.wms.dao.WarehouseDao;
import com.twc.wms.entity.MSSMATN;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.twc.wms.entity.Warehouse;
import com.twc.wms.entity.SAPMAS;
import java.util.List;
import javax.servlet.http.HttpSession;

/**
 *
 * @author wien
 */
public class EditNControllerWMS004 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static final String PAGE_VIEW = "../views/editNWMS004.jsp";

    public EditNControllerWMS004() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("PROGRAMNAME", "WMS004/NE");
        String forward = "";
        String mat = request.getParameter("uid");
        String vt = request.getParameter("vt");
        String plant = request.getParameter("plant");
        SAPMASDao dao = new SAPMASDao();
        SAPMAS ua = dao.findAll(mat, vt, plant);
        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "Mat Ctrl Connect Mat Code. Display");
                request.setAttribute("MatCode", ua.getMatcode());
                request.setAttribute("Desc", ua.getDesc());
                request.setAttribute("Plant", ua.getPlant());
                request.setAttribute("Vtype", ua.getVtype());
                request.setAttribute("Mtype", ua.getMtype());
                request.setAttribute("UM", ua.getUm());
                request.setAttribute("Mgroup", ua.getMgroup());
                request.setAttribute("MgroupDesc", ua.getMgdesc());
                request.setAttribute("MatCtrl", ua.getMatCtrl());
                request.setAttribute("MatCtrlDesc", ua.getMatCtrlName());
                request.setAttribute("Pgroup", ua.getPgroup());
                request.setAttribute("PgroupName", ua.getPgroupName());
                request.setAttribute("Location", ua.getLocation());

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        MSSMATNDao dao2 = new MSSMATNDao();
        List<MSSMATN> pList2 = dao2.findAll();

        RequestDispatcher view = request.getRequestDispatcher(forward);
        request.setAttribute("MCList", pList2);
        view.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");

        String matcode = request.getParameter("materialCode");
        String desc = request.getParameter("description");
        String plant = request.getParameter("plant");
        String vtype = request.getParameter("Vtype");
        String mtype = request.getParameter("Mtype");
        String um = request.getParameter("um");
        String mgroup = request.getParameter("Mgroup");
        String mgdesc = request.getParameter("MgroupDesc");

        String matctrl = request.getParameter("matCtrl");
        String[] parts = matctrl.split(" : ");
        String part1 = parts[0];
        String[] ds = part1.split(" ");
        String ds1 = ds[0];
        matctrl = ds1;

        String matctrldesc = request.getParameter("MatCtrlDesc");
        String pgroup = request.getParameter("Pgroup");
        String pgname = request.getParameter("PgroupName");
        String location = request.getParameter("location");

        String userid = request.getParameter("userid");

        String sendMessage = "";
        String forward = "";

        request.setAttribute("PROGRAMNAME", "WMS004/NE");
        request.setAttribute("PROGRAMDESC", "Mat Ctrl Connect Mat Code. Display");

        SAPMASDao dao = new SAPMASDao();
        SAPMAS p = new SAPMAS(matcode, desc, plant, vtype, mtype, um, mgroup, mgdesc, matctrl, matctrldesc, pgroup, pgname, location);

        if (dao.edit(p, userid)) {
            response.setHeader("Refresh", "0;/TABLE2/WMS004/ndisplay?uid=" + matctrl);
        } else {
            sendMessage = "<script type=\"text/javascript\">\n"
                    + "            var show2 = function () {\n"
                    + "                $('#myModal2').modal('show');\n"
                    + "            };\n"
                    + "\n"
                    + "            window.setTimeout(show2, 0);\n"
                    + "\n"
                    + "        </script>";
            forward = PAGE_VIEW;
            request.setAttribute("sendMessage", sendMessage);

            RequestDispatcher view = request.getRequestDispatcher(forward);
            view.forward(request, response);
        }
    }
}
