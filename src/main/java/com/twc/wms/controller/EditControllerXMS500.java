/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.controller;

import com.twc.wms.dao.XMSRMRDao;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author wien
 */
public class EditControllerXMS500 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;

    public EditControllerXMS500() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");

        String uid = request.getParameter("uid");
        String[] qrid = request.getParameterValues("qrid");
        String[] qty = request.getParameterValues("qty");
        String[] pack = request.getParameterValues("pack");
        String[] plant = request.getParameterValues("plant");
        String[] user = request.getParameterValues("user");

        for (int i = 0; i < qrid.length; i++) {
            if (new XMSRMRDao().edit(qrid[i], qty[i], pack[i], plant[i])) {
                new XMSRMRDao().addTRA(qrid[i], user[i]);
                new XMSRMRDao().delete(qrid[i], user[i]);
            }
        }

        response.setHeader("Refresh", "0;/TABLE2/XMS500/display?uid=" + uid);

    }
}
