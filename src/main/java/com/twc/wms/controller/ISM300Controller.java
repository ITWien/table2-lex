/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.controller;

import com.twc.wms.dao.MASMDAO;
import com.twc.wms.dao.PACKDAO;
import com.twc.wms.dao.TOPKDAO;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author wien
 */
public class ISM300Controller extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static String PACKING = "../views/packingISM300.jsp";
    private static String ISSUE = "../views/issueISM300.jsp";
    private static String REQUEST_APPROVAL = "../views/requestApprovalISM300.jsp";
    private MASMDAO MASMDAO;
    private TOPKDAO TOPKDAO;
    private PACKDAO PACKDAO;

    public ISM300Controller() {
        super();
        MASMDAO = new MASMDAO();
        TOPKDAO = new TOPKDAO();
        PACKDAO = new PACKDAO();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        request.setAttribute("PROGRAMNAME", "ISM300");
        request.setAttribute("PROGRAMDESC", "Sales Order Pick-up. Process");

        String forward = "";
        String action = "";
        String page = "";
        String POFG = "";
        String LGPBE = "";
        String GRPNO = "";
        String ISSUENO = "";
        String TOPREQ = "";

        if (request.getParameter("a") != null) {
            action = request.getParameter("a");
        }

        if (action.equalsIgnoreCase("list")) {

            POFG = request.getParameter("POFG");
            LGPBE = request.getParameter("LGPBE");
            GRPNO = request.getParameter("GRPNO");
            ISSUENO = request.getParameter("ISSUENO");

            TOPREQ = request.getParameter("Topreq");  //new mod
            if (TOPREQ == null) {
                TOPREQ = "0.00";
            }

            request.setAttribute("TOPREQ", TOPREQ);
            request.setAttribute("POFG", POFG);
            request.setAttribute("LGPBE", LGPBE);
            request.setAttribute("GRPNO", GRPNO);
            request.setAttribute("ISSUENO", ISSUENO);

        }

        if (request.getParameter("p") != null) {
            page = request.getParameter("p");
        }

        if (page.equalsIgnoreCase("2")) {

            String MATNR = request.getParameter("MATNR");

            String modal = "";

            if (request.getParameter("m") != null) {
                modal = request.getParameter("m");
            }

            if (modal.equalsIgnoreCase("show")) {
                request.setAttribute("modal", "show");
                request.setAttribute("listDetail", PACKDAO.getListDetail(GRPNO, Integer.parseInt(ISSUENO), MATNR));
            }

            float ISSUETOPICK = TOPKDAO.getISSUETOPICK(GRPNO, Integer.parseInt(ISSUENO), MATNR);
            float ISSUETOPACK = PACKDAO.getISSUETOPACK(GRPNO, Integer.parseInt(ISSUENO), MATNR);

            request.setAttribute("MATNR", MATNR);
            request.setAttribute("ISSUETOPICK", "" + ISSUETOPICK);
            request.setAttribute("ISSUETOPACK", "" + ISSUETOPACK);
            request.setAttribute("ISSUETODIFF", "" + (Float.parseFloat(TOPREQ) - ISSUETOPACK));
            request.setAttribute("list", PACKDAO.getList(GRPNO, Integer.parseInt(ISSUENO), MATNR));

            forward = PACKING;

        } else if (page.equalsIgnoreCase("3")) {
            request.setAttribute("listIssue", MASMDAO.getIssue(POFG, LGPBE, GRPNO, ISSUENO));
            forward = REQUEST_APPROVAL;
        } else {

            if (!POFG.equals("") && !LGPBE.equals("") && !GRPNO.equals("") && !ISSUENO.equals("")) {
                request.setAttribute("listIssue", MASMDAO.getIssue(POFG, LGPBE, GRPNO, ISSUENO));
            }

            forward = ISSUE;

        }

        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String action = "";

        // RequestDispatcher view = request.getRequestDispatcher(ISSUE);
        response.sendRedirect("/MSSX/MSS001");

    }

}
