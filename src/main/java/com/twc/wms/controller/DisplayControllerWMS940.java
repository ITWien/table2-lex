/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.controller;

import com.twc.wms.dao.QIDETAILDao;
import com.twc.wms.dao.WHDao;
import com.twc.wms.entity.WH;
import com.twc.wms.dao.QIHEADDao;
import com.twc.wms.dao.QdestDao;
import com.twc.wms.entity.MSSMATN;
import com.twc.wms.entity.QIDETAIL;
import com.twc.wms.entity.Qdest;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author wien
 */
public class DisplayControllerWMS940 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static final String PAGE_VIEW = "../views/displayWMS940.jsp";

    public DisplayControllerWMS940() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        request.setAttribute("PROGRAMNAME", "WMS940");
        String forward = "";
        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "RM Issue. Tracking");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
//    ********

        Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH) + 1;

        String y = Integer.toString(year);
        String m = Integer.toString(month);

        if (m.length() < 2) {
            m = "0" + m;
        }

        String cd = request.getParameter("CD");
        String ym = "";

        if (cd != null) {
            ym = cd;
            request.setAttribute("ym", ym);
        } else {
            ym = y + m;
            request.setAttribute("ym", ym);
        }

        String wh = request.getParameter("wh");
        if (wh == null) {
            wh = "WH2";
        }

        QIDETAILDao daoMain = new QIDETAILDao();
        List<QIDETAIL> mainList = daoMain.findUser940(ym, wh);
        request.setAttribute("mainList", mainList);

        QIDETAILDao daoUser = new QIDETAILDao();
        List<QIDETAIL> userList = daoUser.findUser940(ym, wh);
        request.setAttribute("userList", userList);

        QIDETAILDao daoMat = new QIDETAILDao();
        List<QIDETAIL> matList = daoMat.findMat940(ym, wh);
        request.setAttribute("matList", matList);

        QIDETAILDao daoDest = new QIDETAILDao();
        List<QIDETAIL> destList = daoDest.findDest940(ym, wh);
        request.setAttribute("destList", destList);

        QIDETAILDao daoApp = new QIDETAILDao();
        List<QIDETAIL> appList = daoApp.findApp940(ym, wh);
        request.setAttribute("appList", appList);

        QIDETAILDao daoDoc = new QIDETAILDao();
        List<QIDETAIL> docList = daoDoc.findDoc940(ym, wh);
        request.setAttribute("docList", docList);

        QIDETAILDao daoMvt = new QIDETAILDao();
        List<QIDETAIL> mvtList = daoMvt.findMvt940(ym, wh);
        request.setAttribute("mvtList", mvtList);

        WHDao dao2 = new WHDao();
        List<WH> pList2 = dao2.findAllnoTWC();
        request.setAttribute("MCList", pList2);

        WHDao dao1 = new WHDao();
        WH uaF = dao1.findByUid(wh);

        request.setAttribute("mcF", wh);
        request.setAttribute("nameF", uaF.getName());

        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("PROGRAMNAME", "WMS940");
        String forward = "";
        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "RM Issue. Tracking");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
//    ********
        String ym = request.getParameter("ym");
        String wh = request.getParameter("wh");
        String sortBy = request.getParameter("sortBy");
        String data = request.getParameter("data");
        String fullData = data;

        String sts = request.getParameter("sts");

        if (data != null) {
            data = data.split(" ")[0];
        }

        request.setAttribute("ym", ym);

        if (sortBy.equals("user")) {
            request.setAttribute("sortBy", "<option value=\"user\" hidden>1 : User</option>");

            QIDETAILDao daoMain = new QIDETAILDao();
            List<QIDETAIL> mainList = daoMain.findUser940(ym, wh);
            request.setAttribute("mainList", mainList);

        } else if (sortBy.equals("mat")) {
            request.setAttribute("sortBy", "<option value=\"mat\" hidden>2 : Material Control</option>");

            QIDETAILDao daoMain = new QIDETAILDao();
            List<QIDETAIL> mainList = daoMain.findMat940(ym, wh);
            request.setAttribute("mainList", mainList);

        } else if (sortBy.equals("dest")) {
            request.setAttribute("sortBy", "<option value=\"dest\" hidden>3 : Destination</option>");

            QIDETAILDao daoMain = new QIDETAILDao();
            List<QIDETAIL> mainList = daoMain.findDest940(ym, wh);
            request.setAttribute("mainList", mainList);

        } else if (sortBy.equals("app")) {
            request.setAttribute("sortBy", "<option value=\"app\" hidden>4 : Approve User</option>");

            QIDETAILDao daoMain = new QIDETAILDao();
            List<QIDETAIL> mainList = daoMain.findApp940(ym, wh);
            request.setAttribute("mainList", mainList);

        } else if (sortBy.equals("doc")) {
            request.setAttribute("sortBy", "<option value=\"doc\" hidden>5 : Document No.</option>");

            QIDETAILDao daoMain = new QIDETAILDao();
            List<QIDETAIL> mainList = daoMain.findDoc940(ym, wh);
            request.setAttribute("mainList", mainList);

        } else if (sortBy.equals("mvt")) {
            request.setAttribute("sortBy", "<option value=\"mvt\" hidden>6 : Movement Type</option>");

            QIDETAILDao daoMain = new QIDETAILDao();
            List<QIDETAIL> mainList = daoMain.findMvt940(ym, wh);
            request.setAttribute("mainList", mainList);

        }

        request.setAttribute("data", new String(fullData.getBytes("iso-8859-1"), "UTF-8"));
//        request.setAttribute("dataID", fullData.split(" ")[0]);

        QIDETAILDao daoUser = new QIDETAILDao();
        List<QIDETAIL> userList = daoUser.findUser940(ym, wh);
        request.setAttribute("userList", userList);

        QIDETAILDao daoMat = new QIDETAILDao();
        List<QIDETAIL> matList = daoMat.findMat940(ym, wh);
        request.setAttribute("matList", matList);

        QIDETAILDao daoDest = new QIDETAILDao();
        List<QIDETAIL> destList = daoDest.findDest940(ym, wh);
        request.setAttribute("destList", destList);

        QIDETAILDao daoApp = new QIDETAILDao();
        List<QIDETAIL> appList = daoApp.findApp940(ym, wh);
        request.setAttribute("appList", appList);

        QIDETAILDao daoDoc = new QIDETAILDao();
        List<QIDETAIL> docList = daoDoc.findDoc940(ym, wh);
        request.setAttribute("docList", docList);

        QIDETAILDao daoMvt = new QIDETAILDao();
        List<QIDETAIL> mvtList = daoMvt.findMvt940(ym, wh);
        request.setAttribute("mvtList", mvtList);

        QIDETAILDao daodt = new QIDETAILDao();
        List<QIDETAIL> detList = daodt.findAll940(ym, sortBy, data, wh, sts);
        request.setAttribute("detList", detList);

        WHDao dao2 = new WHDao();
        List<WH> pList2 = dao2.findAllnoTWC();
        request.setAttribute("MCList", pList2);

        WHDao dao1 = new WHDao();
        WH uaF = dao1.findByUid(wh);

        request.setAttribute("mcF", wh);
        request.setAttribute("nameF", uaF.getName());

        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);
    }

}
