/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.controller;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author wien
 */
public class SumByDestControllerISM200 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static final String PAGE_VIEW = "../views/sumByDestISM200.jsp";

    public SumByDestControllerISM200() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

//        String[] order = request.getParameterValues("oorder");
//        request.setAttribute("order", order);
//        String[] seq = request.getParameterValues("sseq");
//        request.setAttribute("seq", seq);
        String[] oANDs = request.getParameterValues("oANDs");
        request.setAttribute("oANDs", oANDs);

        request.setAttribute("PROGRAMNAME", "ISM200");
        String forward = "";
        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "Sales Order Simulation. Display");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
//    ********
        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

//        String[] order = request.getParameterValues("order");
//        String[] seq = request.getParameterValues("seq");
//        request.setAttribute("order", order);
//        request.setAttribute("seq", seq);
        String[] oANDs = request.getParameterValues("oANDs");
        request.setAttribute("oANDs", oANDs);

        request.setAttribute("PROGRAMNAME", "ISM200");
        String forward = "";
        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "Sales Order Simulation. Display");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
//    ********
        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);

    }

}
