/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.controller;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.twc.wms.dao.WarehouseDao;
import com.twc.wms.entity.Warehouse;
import java.util.List;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;
import com.twc.wms.dao.QRKDETAILDao;
import com.twc.wms.entity.QRKDETAIL;
import java.io.FileOutputStream;
import com.itextpdf.text.Font.FontFamily;
import javax.servlet.ServletContext;

/**
 *
 * @author wien
 */
public class PrintGControllerWMS008 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static final String PAGE_VIEW = "../views/printGWMS008.jsp";
    private static final String FILE_PATH = "/report/QR/";

    public PrintGControllerWMS008() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");

        String wh = request.getParameter("wh");
        String loc = request.getParameter("loc");

        String PDFfilename = "";
        String path = "";
        try {

            Rectangle pageSize = new Rectangle(164, 116);
            Document document = new Document(pageSize, 0, 0, 0, -100);
            String QRcode = loc;
            PDFfilename = QRcode;
            // Edit by ji
            ServletContext servletContext = getServletContext();    // new by ji
            String realPath = servletContext.getRealPath(FILE_PATH);        // new by ji
            String saperate = realPath.contains(":") ? "\\" : "/";
            path = realPath + saperate + QRcode + ".pdf";        // new by ji
//            String path = "C:/Users/nutthawoot.noo/Desktop/" + QRcode + ".pdf";       // old by lek
//            String path = "C:\\Users\\nutthawoot.noo\\Documents\\NetBeansProjects\\TWC-WMS-WMS009\\TWC-WMS-WMS008\\src\\main\\webapp\\report\\QR\\" + QRcode + ".pdf";   // new path by ji

            PdfWriter.getInstance(document, new FileOutputStream(path));

            document.open();

            int QRsize = 130;
            BarcodeQRCode barcodeQRCode = new BarcodeQRCode(QRcode, 1000, 1000, null);
            Image codeQrImage = barcodeQRCode.getImage();
            codeQrImage.scaleAbsolute(QRsize, QRsize);
            codeQrImage.setAbsolutePosition(16, -3);
            document.add(codeQrImage);

            Paragraph p2 = new Paragraph(QRcode, new Font(FontFamily.HELVETICA, 12));
            p2.setAlignment(Element.ALIGN_CENTER);
            p2.setSpacingBefore(95);
            document.add(p2);

            document.newPage();
            document.close();

        } catch (Exception e) {
            System.err.println(e.toString());
        }

        String showPDF = "";

        showPDF = "<script type=\"text/javascript\">\n"
                + "                 window.onload = function (e) {\n"
                + "                     window.open(\"../report/QR/" + PDFfilename + ".pdf\");\n"
                + "                     window.close();\n"
                + "                 };\n"
                + "        </script>";
//        request.setAttribute("pathGenerate", path);
        request.setAttribute("showPDF", showPDF);
        String forward = PAGE_VIEW;
        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

}
