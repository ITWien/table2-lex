/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.controller;

import com.twc.wms.dao.QUSMTCTRLDao;
import com.twc.wms.dao.WMS935Dao;
import com.twc.wms.dao.WMSPRTDao;
import com.twc.wms.dao.WMSTOT930Dao;
import com.twc.wms.entity.TOT930;
import com.twc.wms.entity.TOT930TB;
import com.twc.wms.entity.WMS910PRT;
import com.twc.wms.entity.WMS960;
import com.twc.wms.entity.WMSTOT930;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import javax.servlet.ServletContext;
import javax.servlet.ServletOutputStream;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFFont;

/**
 *
 * @author wien
 */
public class PrintControllerWMS935 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static final String FILE_PATH = "/report/";
    private static final int BUFSIZE = 4096;
    DecimalFormat formatDou0 = new DecimalFormat("#,###,###,##0");
    DecimalFormat formatDou = new DecimalFormat("#,###,###,##0.00");
    DecimalFormat formatDou3 = new DecimalFormat("#,###,###,##0.000");

    public PrintControllerWMS935() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");

        String wh = request.getParameter("wh");
        String oh1000 = request.getParameter("oh1000");
        String oh1050 = request.getParameter("oh1050");
        String whText = request.getParameter("whText");
        String oh1000Text = request.getParameter("oh1000Text");
        String oh1050Text = request.getParameter("oh1050Text");

        String pt1000 = "";
        String pt1050 = "";

        if (!oh1000.equals("none")) {
            pt1000 = "1000";
        }

        if (!oh1050.equals("none")) {
            pt1050 = "1050";
        }

        //        ***************EXCEL*************************************
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet("Summary Stock Checking");

        Map<String, Object[]> data = new TreeMap<String, Object[]>();

        int ni = 100001;
        int exLine = 0;
        List<Integer> mergeList = new ArrayList<Integer>();

        data.put(Integer.toString(ni++), new Object[]{"", "", "", "", "", "Summary Stock Checking"});
        data.put(Integer.toString(ni++), new Object[]{""});
        exLine++;
        data.put(Integer.toString(ni++), new Object[]{"", "Warehouse", new String(whText.getBytes("iso-8859-1"), "UTF-8")});
        exLine++;
        data.put(Integer.toString(ni++), new Object[]{"", "Plant 1000", oh1000Text});
        exLine++;
        data.put(Integer.toString(ni++), new Object[]{"", "Plant 1050", oh1050Text});
        exLine++;
        data.put(Integer.toString(ni++), new Object[]{""});
        exLine++;

        List<TOT930TB> resList = new WMS935Dao().findWHTOT930(oh1000, oh1050, pt1000, pt1050, wh);

        for (int i = 0; i < resList.size(); i++) {

            List<TOT930> dataList = new WMS935Dao().findTOT930(oh1000, oh1050, pt1000, pt1050, resList.get(i).getWH(), wh);

            data.put(Integer.toString(ni++), new Object[]{"",
                dataList.get(0).getTWHSE() + "<BOLD>",
                "1000<BOLD><CENTER>", "<BOLD>",
                "1050<BOLD><CENTER>", "<BOLD>",
                "TOTAL<BOLD><CENTER>", "<BOLD>"
            });
            exLine++;
            mergeList.add(exLine);

            data.put(Integer.toString(ni++), new Object[]{"",
                "Mat Ctrl<BOLD>",
                "SKU<BOLD><RIGHT>",
                "Amount<BOLD><RIGHT>",
                "SKU<BOLD><RIGHT>",
                "Amount<BOLD><RIGHT>",
                "SKU<BOLD><RIGHT>",
                "Amount<BOLD><RIGHT>"
            });
            exLine++;

            for (int j = 0; j < dataList.size(); j++) {

                data.put(Integer.toString(ni++), new Object[]{"",
                    dataList.get(j).getTMTCL(),
                    dataList.get(j).getSKU_1000() + "<RIGHT>",
                    dataList.get(j).getAMT_1000() + "<RIGHT>",
                    dataList.get(j).getSKU_1050() + "<RIGHT>",
                    dataList.get(j).getAMT_1050() + "<RIGHT>",
                    dataList.get(j).getSKU_TOT() + "<RIGHT>",
                    dataList.get(j).getAMT_TOT() + "<RIGHT>"
                });
                exLine++;
            }

            data.put(Integer.toString(ni++), new Object[]{"",
                "TOTAL<BOLD>",
                dataList.get(0).getT1000_SKU() + "<BOLD><RIGHT>",
                dataList.get(0).getT1000_AMT() + "<BOLD><RIGHT>",
                dataList.get(0).getT1050_SKU() + "<BOLD><RIGHT>",
                dataList.get(0).getT1050_AMT() + "<BOLD><RIGHT>",
                dataList.get(0).getTTOT_SKU() + "<BOLD><RIGHT>",
                dataList.get(0).getTTOT_AMT() + "<BOLD><RIGHT>"
            });
            exLine++;

            if (i == resList.size() - 1) {
                data.put(Integer.toString(ni++), new Object[]{"",
                    "GRAND TOTAL<BOLD>",
                    dataList.get(0).getGT1000_SKU() + "<BOLD><RIGHT>",
                    dataList.get(0).getGT1000_AMT() + "<BOLD><RIGHT>",
                    dataList.get(0).getGT1050_SKU() + "<BOLD><RIGHT>",
                    dataList.get(0).getGT1050_AMT() + "<BOLD><RIGHT>",
                    dataList.get(0).getGTTOT_SKU() + "<BOLD><RIGHT>",
                    dataList.get(0).getGTTOT_AMT() + "<BOLD><RIGHT>"
                });
                exLine++;
            }

            data.put(Integer.toString(ni++), new Object[]{""});
            exLine++;
            data.put(Integer.toString(ni++), new Object[]{""});
            exLine++;
        }

        //Set Column Width
        sheet.setColumnWidth(0, 1000);
        sheet.setColumnWidth(1, 4000);
        sheet.setColumnWidth(2, 4000);
        sheet.setColumnWidth(3, 4000);
        sheet.setColumnWidth(4, 4000);
        sheet.setColumnWidth(5, 4000);
        sheet.setColumnWidth(6, 4000);
        sheet.setColumnWidth(7, 4000);

        for (int i = 0; i < mergeList.size(); i++) {

            int row = mergeList.get(i);
            sheet.addMergedRegion(new CellRangeAddress(row, row, 2, 3));
            sheet.addMergedRegion(new CellRangeAddress(row, row, 4, 5));
            sheet.addMergedRegion(new CellRangeAddress(row, row, 6, 7));

        }

        XSSFFont my_font = workbook.createFont();
//        my_font.setBold(true);
        my_font.setFontHeight(11);

        XSSFFont my_font_bold = workbook.createFont();
        my_font_bold.setBold(true);
        my_font_bold.setFontHeight(12);

        // Iterate over data and write to sheet 
        Set<String> keyset = data.keySet();
        int rownum = 0;
        for (String key : keyset) {
            // this creates a new row in the sheet 
            Row row = sheet.createRow(rownum++);
            Object[] objArr = data.get(key);
            int cellnum = 0;
            for (Object obj : objArr) {
                // this line creates a cell in the next column of that row 
                Cell cell = row.createCell(cellnum++);
                cell.setCellValue((String) obj);

                CellStyle style_tmp = workbook.createCellStyle();

                if (rownum <= 5) {
                    style_tmp.setFont(my_font_bold);
                }

                if (cell.getStringCellValue().contains("<BOLD>")) {
                    style_tmp.setFont(my_font_bold);
                    style_tmp.setBorderTop(BorderStyle.THIN);
                    style_tmp.setBorderBottom(BorderStyle.THIN);
                    cell.setCellValue(cell.getStringCellValue().replace("<BOLD>", ""));
                }

                if (cell.getStringCellValue().contains("<CENTER>")) {
                    style_tmp.setAlignment(HorizontalAlignment.CENTER);
                    cell.setCellValue(cell.getStringCellValue().replace("<CENTER>", ""));
                }

                if (cell.getStringCellValue().contains("<RIGHT>")) {
                    style_tmp.setAlignment(HorizontalAlignment.RIGHT);
                    cell.setCellValue(cell.getStringCellValue().replace("<RIGHT>", ""));
                }

                cell.setCellStyle(style_tmp);
                cell.setCellValue(cell.getStringCellValue().replace("", ""));
            }
        }

        try {
            ServletContext servletContextEX = getServletContext();    // new by ji
            String realPathEX = servletContextEX.getRealPath(FILE_PATH);        // new by ji
            String saperateEX = realPathEX.contains(":") ? "\\" : "/";
            String pathEX = realPathEX + saperateEX + "Summary_Stock_Checking.xlsx";

            FileOutputStream outputStream = new FileOutputStream(pathEX);
            workbook.write(outputStream);
            workbook.close();

//            ******************************************************
            File fileEX = new File(pathEX);
            int length = 0;
            ServletOutputStream outStream = response.getOutputStream();
            ServletContext context = getServletConfig().getServletContext();
            String mimetype = context.getMimeType(pathEX);

            // sets response content type
            if (mimetype == null) {
                mimetype = "application/octet-stream";
            }
            response.setContentType(mimetype);
            response.setContentLength((int) fileEX.length());
            String fileNameEX = (new File(pathEX)).getName();

            // sets HTTP header
            response.setHeader("Content-Disposition", "attachment; filename=\"" + fileNameEX + "\"");

            byte[] byteBuffer = new byte[BUFSIZE];
            DataInputStream in = new DataInputStream(new FileInputStream(fileEX));

            // reads the file's bytes and writes them to the response stream
            while ((in != null) && ((length = in.read(byteBuffer)) != -1)) {
                outStream.write(byteBuffer, 0, length);
            }

            in.close();
            outStream.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

}
