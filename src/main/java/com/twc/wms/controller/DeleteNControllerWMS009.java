/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.controller;

import com.twc.wms.dao.PGMUDao;
import com.twc.wms.dao.QPGMAS_2Dao;
import com.twc.wms.entity.PGMU;
import com.twc.wms.entity.QPGMAS_2;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 *
 * @author wien
 */
public class DeleteNControllerWMS009 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static final String PAGE_VIEW = "../views/deleteNWMS009.jsp";

    public DeleteNControllerWMS009() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        request.setAttribute("PROGRAMNAME", "WMS009/CD");
        String forward = "";
        String uid = request.getParameter("id");
        String pid = request.getParameter("uid");

        QPGMAS_2Dao dao1 = new QPGMAS_2Dao();
        QPGMAS_2 ua = dao1.findByUid(pid);

        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "User Authorization Connect Program. Display");
                request.setAttribute("uid", uid);
                request.setAttribute("pid", pid);
                request.setAttribute("pname", ua.getName());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");

        String uid = request.getParameter("userid");
        String pid = request.getParameter("pid");

        PGMUDao dao = new PGMUDao();

        dao.delete(uid, pid);

        response.setHeader("Refresh", "0;/TABLE2/WMS009/ndisplay?uid=" + uid);
    }
}
