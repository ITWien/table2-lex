/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.controller;

import com.twc.wms.dao.ISM800Dao;
import com.twc.wms.entity.ISMMASH;
import com.twc.wms.entity.ISMSDWD;
import com.twc.wms.entity.ISMSDWH;
import java.io.IOException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author 93176
 */
public class EditControllerISM800 extends HttpServlet {

    private static final String PAGE_VIEW = "../views/editISM800.jsp";

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        request.setAttribute("PROGRAMNAME", "ISM800/E");
        request.setAttribute("PROGRAMDESC", "Sales Order Download. Process");

        String date = request.getParameter("dt");
        String customer = request.getParameter("cus");
        String no = request.getParameter("no");

        System.out.println("Edit800");

        String cusnam = new ISM800Dao().getCUSName(customer);

//        request.setAttribute("dt", date.substring(6, 8) + "/" + date.substring(4, 6) + "/" + date.substring(0, 4));
        request.setAttribute("date", date);
        request.setAttribute("cus", customer);
        request.setAttribute("cusname", cusnam);
        request.setAttribute("no", no);
        RequestDispatcher view = request.getRequestDispatcher(PAGE_VIEW);
        view.forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String date = request.getParameter("date");
        String customer = request.getParameter("cus");
        String no = request.getParameter("no");

        String[] ccbox = request.getParameterValues("CCBOX");
        String uid = request.getParameter("userid");

        String alrt = "";
        boolean passHead = false;
        boolean passDetail = false;

        for (int b = 0; b < ccbox.length; b++) {
            String number = ccbox[b].split(":")[0];
            String order = ccbox[b].split(":")[1];

            String seq_current = new ISM800Dao().SelectSEQbyORDERNo(order);

//            if (seq_current.equals("0")) {
//                seq_current = LineDown.get(i).getISDSEQ();
//            }
            String stsck = new ISM800Dao().selectSTSMasHead(order, seq_current);

            if (stsck.equals("0") || stsck.equals("")) { // Status Master Head = 0 or Master Head Empty

                System.out.println("sts 0");

                List<ISMSDWH> HeadDistinct = new ISM800Dao().GetNotDupHMasnDwn(order, date, no);
                List<ISMSDWD> DetailDistinct = new ISM800Dao().GetNotDupMasnDwn(order, date, no); //Line Mas

                if (!HeadDistinct.isEmpty()) {

                    boolean allowHead = true;

                    for (int i = 0; i < HeadDistinct.size(); i++) {

//                        if (HeadDistinct.get(i).getISHLSTS().equals("0")) { //STS in Mas = 0
                        boolean delMasH = new ISM800Dao().deleteISMMASHbySEQ(HeadDistinct.get(i).getISHORD(), seq_current); //Delete By Order No
//                        boolean delMasH = new ISM800Dao().deleteISMMASHbySEQ(HeadDistinct.get(i).getISHORD(), HeadDistinct.get(i).getISHSEQ()); //Delete By Order No

                        System.out.println("delHead " + delMasH);
//                        if (delMasH) {
                        boolean insertISMMASH = new ISM800Dao().addISMMASH(HeadDistinct.get(i).getISHORD(), seq_current, HeadDistinct.get(i).getISHTRDT(), HeadDistinct.get(i).getISHUART(), HeadDistinct.get(i).getISHTWEG(),
                                HeadDistinct.get(i).getISHMVT(), HeadDistinct.get(i).getISHCUNO(), HeadDistinct.get(i).getISHCUNM1(), HeadDistinct.get(i).getISHCUNM2(), HeadDistinct.get(i).getISHCUAD1(), HeadDistinct.get(i).getISHCUAD2(),
                                HeadDistinct.get(i).getISHBSTKD(), HeadDistinct.get(i).getISHSUBMI(), HeadDistinct.get(i).getISHMATLOT(), HeadDistinct.get(i).getISHTAXNO(), HeadDistinct.get(i).getISHBRANCH01(), HeadDistinct.get(i).getISHDATUM(),
                                HeadDistinct.get(i).getISHSTYLE(), HeadDistinct.get(i).getISHCOLOR(), HeadDistinct.get(i).getISHLOT(), HeadDistinct.get(i).getISHAMTFG(), HeadDistinct.get(i).getISHSTYLE2(), Integer.toString(DetailDistinct.size()),
                                HeadDistinct.get(i).getISHPOFG(), HeadDistinct.get(i).getISHGRPNO(), HeadDistinct.get(i).getISHWHNO(), HeadDistinct.get(i).getISHPGRP(), HeadDistinct.get(i).getISHDEST(), HeadDistinct.get(i).getISHREASON(), uid, "0", "0");

                        if (insertISMMASH == false) {
                            allowHead = insertISMMASH;
                        }
//                        } else {
//                            System.out.println("Delete Master Head Order " + HeadDistinct.get(i).getISHORD() + " Seq " + HeadDistinct.get(i).getISHSEQ() + " Failed.");
//                        }

//                        } else { //STS in Mas > 0
//
//                        }
                    }

                    passHead = allowHead;

                    if (allowHead) {
                        alrt += "alertify.success('Insert To Master Head Success.');";
                    } else {
                        alrt += "alertify.error('Insert To Master Head Failed.');";
                    }

                } else {
                    alrt += "alertify.error('Not Found New Sale. (Head)');";
                }

                if (!DetailDistinct.isEmpty()) {

                    boolean allowDetail = true;

                    for (int i = 0; i < DetailDistinct.size(); i++) {

//                        if (DetailDistinct.get(i).getISDSTS().equals("0")) { // Sts Detail Master = 0
                        boolean delMasD = new ISM800Dao().deleteISMMASDBySEQnLINE(DetailDistinct.get(i).getISDORD(), seq_current, DetailDistinct.get(i).getISDLINO());

                        System.out.println("delMasD " + delMasD);

                        String vtNull = "";

                        if (DetailDistinct.get(i).getISDVT() == null) {
                            vtNull = DetailDistinct.get(i).getISDVT();
                        } else {
                            vtNull = DetailDistinct.get(i).getISDVT().trim();
                        }

//                        if (delMasD) {
                        boolean insertISMMASD = new ISM800Dao().addISMMASD(DetailDistinct.get(i).getISDORD(), seq_current, DetailDistinct.get(i).getISDLINO(), "0", DetailDistinct.get(i).getISDITNO().trim(),
                                DetailDistinct.get(i).getISDPLANT(), vtNull, DetailDistinct.get(i).getISDSTRG(), DetailDistinct.get(i).getISDRQQTY(), DetailDistinct.get(i).getISDUNIT(),
                                DetailDistinct.get(i).getISDPRICE(), DetailDistinct.get(i).getISDUNR01(), DetailDistinct.get(i).getISDUNR03(), DetailDistinct.get(i).getISDMTCTRL(),
                                DetailDistinct.get(i).getISDDEST(), DetailDistinct.get(i).getISDPURG(), uid, "0");

                        if (insertISMMASD == false) {
                            allowDetail = insertISMMASD;
                        }
//                        } else {
//                            alrt += "alertify.error('Delete Failed.');";
//                            System.out.println("Delete Detail Failed " + DetailDistinct.get(i).getISDORD() + " Seq " + DetailDistinct.get(i).getISDSEQ() + " Line " + DetailDistinct.get(i).getISDLINO());
//                        }
//                        } else { // Sts Detail Master > 0
//
//                        }

                    }

                    passDetail = allowDetail;

                    if (allowDetail) {
                        alrt += "alertify.success('Insert To Master Detail Success.');";
                    } else {
                        alrt += "alertify.error('Insert To Master Detail Failed.');";
                    }

                } else {
                    alrt += "alertify.error('Not Found New Sale. (DETAIL)');";
                }

            } else { // Status Master Head = 1,2,3,...

                System.out.println("1 2 3");

                String sseq = Integer.toString(Integer.parseInt(seq_current) + 1);

                System.out.println("new seq " + sseq);

                List<ISMSDWH> HeadDistinct = new ISM800Dao().GetNotDupHMasnDwn(order, date, no);
                List<ISMSDWD> DetailDistinct = new ISM800Dao().GetNotDupMasnDwn(order, date, no); //Line Mas

                if (!HeadDistinct.isEmpty()) {

                    boolean allowHead = true;

                    for (int i = 0; i < HeadDistinct.size(); i++) {

//                        sseq = Integer.toString(Integer.parseInt(HeadDistinct.get(i).getISHSEQ()) + 1);
//                        boolean delMasH = new ISM800Dao().deleteISMMASHbySEQ(HeadDistinct.get(i).getISHORD(), HeadDistinct.get(i).getISHSEQ()); //Delete By Order No
                        boolean insertISMMASH = new ISM800Dao().addISMMASH(HeadDistinct.get(i).getISHORD(), sseq, HeadDistinct.get(i).getISHTRDT(), HeadDistinct.get(i).getISHUART(), HeadDistinct.get(i).getISHTWEG(),
                                HeadDistinct.get(i).getISHMVT(), HeadDistinct.get(i).getISHCUNO(), HeadDistinct.get(i).getISHCUNM1(), HeadDistinct.get(i).getISHCUNM2(), HeadDistinct.get(i).getISHCUAD1(), HeadDistinct.get(i).getISHCUAD2(),
                                HeadDistinct.get(i).getISHBSTKD(), HeadDistinct.get(i).getISHSUBMI(), HeadDistinct.get(i).getISHMATLOT(), HeadDistinct.get(i).getISHTAXNO(), HeadDistinct.get(i).getISHBRANCH01(), HeadDistinct.get(i).getISHDATUM(),
                                HeadDistinct.get(i).getISHSTYLE(), HeadDistinct.get(i).getISHCOLOR(), HeadDistinct.get(i).getISHLOT(), HeadDistinct.get(i).getISHAMTFG(), HeadDistinct.get(i).getISHSTYLE2(), Integer.toString(DetailDistinct.size()),
                                HeadDistinct.get(i).getISHPOFG(), HeadDistinct.get(i).getISHGRPNO(), HeadDistinct.get(i).getISHWHNO(), HeadDistinct.get(i).getISHPGRP(), HeadDistinct.get(i).getISHDEST(), HeadDistinct.get(i).getISHREASON(), uid, "0", "0");
//
//                        if (insertISMMASH == false) {
//                            allowHead = insertISMMASH;
//                        }
//                            System.out.println("Delete Master Head Order " + HeadDistinct.get(i).getISHORD() + " Seq " + HeadDistinct.get(i).getISHSEQ() + " Failed.");
                    }

                    passHead = allowHead;

                    if (allowHead) {
                        alrt += "alertify.success('Insert To Master Head Success.');";
                    } else {
                        alrt += "alertify.error('Insert To Master Head Failed.');";
                    }

                } else {
                    alrt += "alertify.error('Not Found New Sale. (Head)');";
                }

                if (!DetailDistinct.isEmpty()) {

                    boolean allowDetail = true;

                    for (int i = 0; i < DetailDistinct.size(); i++) {

                        boolean delMasD = new ISM800Dao().deleteISMMASDBySEQnLINE(DetailDistinct.get(i).getISDORD(), DetailDistinct.get(i).getISDSEQ(), DetailDistinct.get(i).getISDLINO());
//                        System.out.println("delMasD " + delMasD);
                        String vtNull = "";

                        if (DetailDistinct.get(i).getISDVT() == null) {
                            vtNull = DetailDistinct.get(i).getISDVT();
                        } else {
                            vtNull = DetailDistinct.get(i).getISDVT().trim();
                        }

                        boolean insertISMMASD = new ISM800Dao().addISMMASD(DetailDistinct.get(i).getISDORD(), sseq, DetailDistinct.get(i).getISDLINO(), "0", DetailDistinct.get(i).getISDITNO().trim(),
                                DetailDistinct.get(i).getISDPLANT(), vtNull, DetailDistinct.get(i).getISDSTRG(), DetailDistinct.get(i).getISDRQQTY(), DetailDistinct.get(i).getISDUNIT(),
                                DetailDistinct.get(i).getISDPRICE(), DetailDistinct.get(i).getISDUNR01(), DetailDistinct.get(i).getISDUNR03(), DetailDistinct.get(i).getISDMTCTRL(),
                                DetailDistinct.get(i).getISDDEST(), DetailDistinct.get(i).getISDPURG(), uid, "0");

                        if (insertISMMASD == false) {
                            allowDetail = insertISMMASD;
                        }
//                            alrt += "alertify.error('Delete Failed.');";
//                            System.out.println("Delete Detail Failed " + DetailDistinct.get(i).getISDORD() + " Seq " + DetailDistinct.get(i).getISDSEQ() + " Line " + DetailDistinct.get(i).getISDLINO());

                    }

                    passDetail = allowDetail;

                    if (allowDetail) {
                        alrt += "alertify.success('Insert To Master Detail Success.');";
                    } else {
                        alrt += "alertify.error('Insert To Master Detail Failed.');";
                    }

                } else {
                    alrt += "alertify.error('Not Found New Sale. (DETAIL)');";
                }

            }
//            System.out.println("order " + order);
//        List<ISMMASD> LineMas = new ISM800Dao().GetMASByORD(order, number); //Line Mas
//        List<ISMSDWD> LineDown = new ISM800Dao().GetDWNByORD(order, number); //Line Dwn
//        List<ISMSDWH> DWNHead = new ISM800Dao().GetDWNHeadByORD(order, number); //Download Head
//        List<ISMMASH> MASHead = new ISM800Dao().GetMASHEADByORD(order, number);
//
//        System.out.println("" + LineMas.size());
//        System.out.println("" + LineDown.size());

//            System.out.println(HeadDistinct.size());
//            System.out.println(DetailDistinct.size());
//        if (LineMas.size() == LineDown.size()) {
//
//            System.out.println("this");
//
////            if (!LineMas.get(0).getISDSTS().equals("0")) {
////                alrt = "alertify.error('Please Return Status To 0');";
////            } else {
//            boolean delMasH = new ISM800Dao().deleteISMMASH(order); //Delete By Order No
//            boolean delMasD = new ISM800Dao().deleteISMMASD(order); //Delete By Order No
//
////                String seq = new ISM800Dao().SelectMAXSEQbyORDERNo(number); //seq + 1
//            for (int i = 0; i < DWNHead.size(); i++) {
//                boolean insertISMMASH = new ISM800Dao().addISMMASH(DWNHead.get(i).getISHORD(), DWNHead.get(i).getISHSEQ(), DWNHead.get(i).getISHTRDT(), DWNHead.get(i).getISHUART(), DWNHead.get(i).getISHTWEG(),
//                        DWNHead.get(i).getISHMVT(), DWNHead.get(i).getISHCUNO(), DWNHead.get(i).getISHCUNM1(), DWNHead.get(i).getISHCUNM2(), DWNHead.get(i).getISHCUAD1(), DWNHead.get(i).getISHCUAD2(),
//                        DWNHead.get(i).getISHBSTKD(), DWNHead.get(i).getISHSUBMI(), DWNHead.get(i).getISHMATLOT(), DWNHead.get(i).getISHTAXNO(), DWNHead.get(i).getISHBRANCH01(), DWNHead.get(i).getISHDATUM(),
//                        DWNHead.get(i).getISHSTYLE(), DWNHead.get(i).getISHCOLOR(), DWNHead.get(i).getISHLOT(), DWNHead.get(i).getISHAMTFG(), DWNHead.get(i).getISHSTYLE2(), DWNHead.get(i).getISHNOR(),
//                        DWNHead.get(i).getISHPOFG(), DWNHead.get(i).getISHGRPNO(), DWNHead.get(i).getISHWHNO(), DWNHead.get(i).getISHPGRP(), DWNHead.get(i).getISHDEST(), uid, "0", "0");
//            }
//
////                List<ISMMASH> GET = new ISM800Dao().GetMASHEAD(number); // Old Head
////
////                boolean insertISMMASH = new ISM800Dao().addISMMASH(GET.get(0).getISHORD(), seq, GET.get(0).getISHTRDT(), GET.get(0).getISHUART(), GET.get(0).getISHTWEG(),
////                        GET.get(0).getISHMVT(), GET.get(0).getISHCUNO(), GET.get(0).getISHCUNM1(), GET.get(0).getISHCUNM2(), GET.get(0).getISHCUAD1(), GET.get(0).getISHCUAD2(),
////                        GET.get(0).getISHBSTKD(), GET.get(0).getISHSUBMI(), GET.get(0).getISHMATLOT(), GET.get(0).getISHTAXNO(), GET.get(0).getISHBRANCH01(), GET.get(0).getISHDATUM(),
////                        GET.get(0).getISHSTYLE(), GET.get(0).getISHCOLOR(), GET.get(0).getISHLOT(), GET.get(0).getISHAMTFG(), GET.get(0).getISHSTYLE2(), GET.get(0).getISHNOR(),
////                        GET.get(0).getISHPOFG(), GET.get(0).getISHGRPNO(), GET.get(0).getISHWHNO(), GET.get(0).getISHPGRP(), GET.get(0).getISHDEST(), uid, "0", "0");
//            for (int i = 0; i < LineDown.size(); i++) {
////                for (int i = LineMas.size(); i < LineDown.size(); i++) {
//
//                String vtNull = "";
//
//                if (LineDown.get(i).getISDVT() == null) {
//                    vtNull = LineDown.get(i).getISDVT();
//                } else {
//                    vtNull = LineDown.get(i).getISDVT().trim();
//                }
//
//                boolean insertISMMASD = new ISM800Dao().addISMMASD(LineDown.get(i).getISDORD(), LineDown.get(i).getISDSEQ(), LineDown.get(i).getISDLINO(), "0", LineDown.get(i).getISDITNO().trim(),
//                        LineDown.get(i).getISDPLANT(), vtNull, LineDown.get(i).getISDSTRG(), LineDown.get(i).getISDRQQTY(), LineDown.get(i).getISDUNIT(),
//                        LineDown.get(i).getISDPRICE(), LineDown.get(i).getISDUNR01(), LineDown.get(i).getISDUNR03(), LineDown.get(i).getISDMTCTRL(),
//                        LineDown.get(i).getISDDEST(), uid, "0");
//
//            }
////            }
//
//        } else {
//
//            if (LineMas.isEmpty()) { //Master Empty || Master Size = 0
//
//                System.out.println("this2");
//
////                String seq = new ISM800Dao().SelectMAXSEQbyORDERNo(number); //seq + 1
////                System.out.println("seq " + seq);
////                List<ISMMASH> GET = new ISM800Dao().GetMASHEAD(number); // Old Head
////
//                for (int i = 0; i < DWNHead.size(); i++) {
//                    boolean insertISMMASH = new ISM800Dao().addISMMASH(DWNHead.get(i).getISHORD(), DWNHead.get(i).getISHSEQ(), DWNHead.get(i).getISHTRDT(), DWNHead.get(i).getISHUART(), DWNHead.get(i).getISHTWEG(),
//                            DWNHead.get(i).getISHMVT(), DWNHead.get(i).getISHCUNO(), DWNHead.get(i).getISHCUNM1(), DWNHead.get(i).getISHCUNM2(), DWNHead.get(i).getISHCUAD1(), DWNHead.get(i).getISHCUAD2(),
//                            DWNHead.get(i).getISHBSTKD(), DWNHead.get(i).getISHSUBMI(), DWNHead.get(i).getISHMATLOT(), DWNHead.get(i).getISHTAXNO(), DWNHead.get(i).getISHBRANCH01(), DWNHead.get(i).getISHDATUM(),
//                            DWNHead.get(i).getISHSTYLE(), DWNHead.get(i).getISHCOLOR(), DWNHead.get(i).getISHLOT(), DWNHead.get(i).getISHAMTFG(), DWNHead.get(i).getISHSTYLE2(), DWNHead.get(i).getISHNOR(),
//                            DWNHead.get(i).getISHPOFG(), DWNHead.get(i).getISHGRPNO(), DWNHead.get(i).getISHWHNO(), DWNHead.get(i).getISHPGRP(), DWNHead.get(i).getISHDEST(), uid, "0", "0");
//                }
//
//                for (int i = LineMas.size(); i < LineDown.size(); i++) {
//
//                    String vtNull = "";
//
//                    if (LineDown.get(i).getISDVT() == null) {
//                        vtNull = LineDown.get(i).getISDVT();
//                    } else {
//                        vtNull = LineDown.get(i).getISDVT().trim();
//                    }
//
//                    boolean insertISMMASD = new ISM800Dao().addISMMASD(LineDown.get(i).getISDORD(), LineDown.get(i).getISDSEQ(), LineDown.get(i).getISDLINO(), "0", LineDown.get(i).getISDITNO().trim(),
//                            LineDown.get(i).getISDPLANT(), vtNull, LineDown.get(i).getISDSTRG(), LineDown.get(i).getISDRQQTY(), LineDown.get(i).getISDUNIT(),
//                            LineDown.get(i).getISDPRICE(), LineDown.get(i).getISDUNR01(), LineDown.get(i).getISDUNR03(), LineDown.get(i).getISDMTCTRL(),
//                            LineDown.get(i).getISDDEST(), uid, "0");
//
//                }
//
//            } else { //Master Not Empty || Master Size != 0
//
//                System.out.println("this3");
////                if (!LineMas.get(0).getISDSTS().equals("0")) {
////                    alrt = "alertify.error('Please Return Status To 0');";
////                } else {
//
////                    List<ISMMASH> GET = new ISM800Dao().GetMASHEAD(number); // Old Head
//                boolean delMasH = new ISM800Dao().deleteISMMASH(order); //Delete By Order No
//                boolean delMasD = new ISM800Dao().deleteISMMASD(order); //Delete By Order No
////                    String seq = new ISM800Dao().SelectMAXSEQbyORDERNo(number); //seq + 1
//
//                for (int i = 0; i < DWNHead.size(); i++) {
//                    boolean insertISMMASH = new ISM800Dao().addISMMASH(DWNHead.get(i).getISHORD(), DWNHead.get(i).getISHSEQ(), DWNHead.get(i).getISHTRDT(), DWNHead.get(i).getISHUART(), DWNHead.get(i).getISHTWEG(),
//                            DWNHead.get(i).getISHMVT(), DWNHead.get(i).getISHCUNO(), DWNHead.get(i).getISHCUNM1(), DWNHead.get(i).getISHCUNM2(), DWNHead.get(i).getISHCUAD1(), DWNHead.get(i).getISHCUAD2(),
//                            DWNHead.get(i).getISHBSTKD(), DWNHead.get(i).getISHSUBMI(), DWNHead.get(i).getISHMATLOT(), DWNHead.get(i).getISHTAXNO(), DWNHead.get(i).getISHBRANCH01(), DWNHead.get(i).getISHDATUM(),
//                            DWNHead.get(i).getISHSTYLE(), DWNHead.get(i).getISHCOLOR(), DWNHead.get(i).getISHLOT(), DWNHead.get(i).getISHAMTFG(), DWNHead.get(i).getISHSTYLE2(), DWNHead.get(i).getISHNOR(),
//                            DWNHead.get(i).getISHPOFG(), DWNHead.get(i).getISHGRPNO(), DWNHead.get(i).getISHWHNO(), DWNHead.get(i).getISHPGRP(), DWNHead.get(i).getISHDEST(), uid, "0", "0");
//                }
//
//                for (int i = 0; i < LineDown.size(); i++) {
//
//                    String vtNull = "";
//
//                    if (LineDown.get(i).getISDVT() == null) {
//                        vtNull = LineDown.get(i).getISDVT();
//                    } else {
//                        vtNull = LineDown.get(i).getISDVT().trim();
//                    }
//
//                    boolean insertISMMASD = new ISM800Dao().addISMMASD(LineDown.get(i).getISDORD(), LineDown.get(i).getISDSEQ(), LineDown.get(i).getISDLINO(), "0", LineDown.get(i).getISDITNO().trim(),
//                            LineDown.get(i).getISDPLANT(), vtNull, LineDown.get(i).getISDSTRG(), LineDown.get(i).getISDRQQTY(), LineDown.get(i).getISDUNIT(),
//                            LineDown.get(i).getISDPRICE(), LineDown.get(i).getISDUNR01(), LineDown.get(i).getISDUNR03(), LineDown.get(i).getISDMTCTRL(),
//                            LineDown.get(i).getISDDEST(), uid, "0");
//
//                }
////                }
//            }
//        }
        }
        alrt = "";

        if (passHead) {
            alrt += "alertify.success('Insert To Master Head Success.');";
        } else {
            alrt += "alertify.error('Insert To Master Head Failed.');";
        }
        if (passDetail) {
            alrt += "alertify.success('Insert To Master Detail Success.');";
        } else {
            alrt += "alertify.error('Insert To Master Head Failed.');";
        }

        HttpSession sess = request.getSession(false);
        sess.setAttribute("ALRT", alrt);
        sess.setAttribute("dt", date);
        sess.setAttribute("cus", customer);
        sess.setAttribute("no", no);
        response.sendRedirect(request.getContextPath() + "/ISM800/edit?dt=" + date + "&cus=" + customer + "&ord=&no=" + no + "");
    }

}
