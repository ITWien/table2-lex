/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.controller;

import com.twc.wms.dao.QIDETAILDao;
import com.twc.wms.entity.QIDETAIL;
import com.twc.wms.dao.QIHEADDao;
import com.twc.wms.dao.QRMMASDao;
import com.twc.wms.dao.QRMTRADao;
import com.twc.wms.entity.QIHEAD;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author wien
 */
public class ApproveAllControllerWMS310 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;

    public ApproveAllControllerWMS310() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String[] id = request.getParameterValues("selectCk");//contain QNO-WH-MVT
        String wh = request.getParameter("WHS");
        String matCtrl = request.getParameter("MTCTRL");
        String plantTO = "";

        String userid = request.getParameter("userid");

        if (id != null) {
            for (int i = 0; i < id.length; i++) {
                QIDETAILDao daoP = new QIDETAILDao();
                String plant = daoP.findPLANT(id[i].split("-")[0]);
                if (plant != null) {
                    if (plant.trim().equals("1000")) {
                        plantTO = "1050";
                    } else if (plant.trim().equals("1050")) {
                        plantTO = "1000";
                    }
                }

                if (id[i].split("-")[2].equals("301")) {
                    QIDETAILDao dao22 = new QIDETAILDao();
                    List<QIDETAIL> deList = dao22.findAll(id[i].split("-")[1], id[i].split("-")[0]);

                    for (int j = 0; j < deList.size(); j++) {
                        String dat = "";
                        if (deList.get(j).getGID() == null || deList.get(j).getGID().equals("")) {
                            dat = deList.get(j).getId() + "-" + deList.get(j).getSts() + "-" + deList.get(j).getNo();
                        } else {
                            dat = deList.get(j).getGID() + "-" + deList.get(j).getSts() + "-" + deList.get(j).getNo();
                        }
//                        System.out.println(dat);
                        if (dat.trim().split("-")[0].length() < 17) {
                            QIDETAILDao daoq = new QIDETAILDao();
                            List<String> idList = daoq.UngroupQRID(dat.trim());
                            for (int k = 0; k < idList.size(); k++) {
//                                QRMMASDao daoid = new QRMMASDao();
//                                if (daoid.CheckApprove(idList.get(k).trim()).equals("3")) {
                                QIDETAILDao dao2 = new QIDETAILDao();
                                if (dao2.approveMVT301and311(idList.get(k).trim(), userid, id[i].split("-")[0])) {
                                    QRMMASDao up = new QRMMASDao();
                                    up.UpdateStatusMVT301(idList.get(k).split("-")[0], "2", plantTO);

                                    QRMTRADao adTRA = new QRMTRADao();
                                    adTRA.AddQRMTRA(idList.get(k).split("-")[0], userid);
                                }
//                                }
                            }
                        } else if (dat.trim().split("-")[0].length() == 17) {
//                            QRMMASDao daoid = new QRMMASDao();
//                            if (daoid.CheckApprove(dat.trim()).equals("3")) {
                            QIDETAILDao dao2 = new QIDETAILDao();
                            if (dao2.approveMVT301and311(dat.trim(), userid, id[i].split("-")[0])) {
                                QRMMASDao up = new QRMMASDao();
                                up.UpdateStatusMVT301(dat.split("-")[0], "2", plantTO);

                                QRMTRADao adTRA = new QRMTRADao();
                                adTRA.AddQRMTRA(dat.split("-")[0], userid);
                            }
//                            }
                        }
                    }

                    QIDETAILDao dao = new QIDETAILDao();
                    String[] mnx = dao.getSTS(id[i].split("-")[1].trim(), id[i].split("-")[0].trim());

                    QIDETAILDao dao2 = new QIDETAILDao();
                    String tqty = dao2.getTQY(id[i].split("-")[1].trim(), id[i].split("-")[0].trim());

                    DecimalFormat formatDou = new DecimalFormat("#,##0.00");
                    if (tqty == null) {
                        double tot = Double.parseDouble("0.000");
                        tqty = formatDou.format(tot);
                    } else {
                        double tot = Double.parseDouble(tqty);
                        tqty = formatDou.format(tot);
                    }

                    QIHEADDao dao3 = new QIHEADDao();
                    dao3.SETSTS(mnx[0], mnx[1], id[i].split("-")[1].trim(), id[i].split("-")[0].trim(), tqty.trim().replace(",", ""));
                } else if (id[i].split("-")[2].equals("311")) {
                    QIDETAILDao dao22 = new QIDETAILDao();
                    List<QIDETAIL> deList = dao22.findAll(id[i].split("-")[1], id[i].split("-")[0]);

                    for (int j = 0; j < deList.size(); j++) {
                        String dat = "";
                        if (deList.get(j).getGID() == null || deList.get(j).getGID().equals("")) {
                            dat = deList.get(j).getId() + "-" + deList.get(j).getSts() + "-" + deList.get(j).getNo();
                        } else {
                            dat = deList.get(j).getGID() + "-" + deList.get(j).getSts() + "-" + deList.get(j).getNo();
                        }
//                        System.out.println(dat);
                        if (dat.trim().split("-")[0].length() < 17) {
                            QIDETAILDao daoq = new QIDETAILDao();
                            List<String> idList = daoq.UngroupQRID(dat.trim());
                            for (int k = 0; k < idList.size(); k++) {
//                                QRMMASDao daoid = new QRMMASDao();
//                                if (daoid.CheckApprove(idList.get(k).trim()).equals("3")) {
                                QIDETAILDao dao2 = new QIDETAILDao();
                                if (dao2.approveMVT301and311(idList.get(k).trim(), userid, id[i].split("-")[0])) {
                                    QRMMASDao up = new QRMMASDao();
                                    up.UpdateStatusMVT311(idList.get(k).split("-")[0], "2");

                                    QRMTRADao adTRA = new QRMTRADao();
                                    adTRA.AddQRMTRA(idList.get(k).split("-")[0], userid);
                                }
//                                }
                            }
                        } else if (dat.trim().split("-")[0].length() == 17) {
//                            QRMMASDao daoid = new QRMMASDao();
//                            if (daoid.CheckApprove(dat.trim()).equals("3")) {
                            QIDETAILDao dao2 = new QIDETAILDao();
                            if (dao2.approveMVT301and311(dat.trim(), userid, id[i].split("-")[0])) {
                                QRMMASDao up = new QRMMASDao();
                                up.UpdateStatusMVT311(dat.split("-")[0], "2");

                                QRMTRADao adTRA = new QRMTRADao();
                                adTRA.AddQRMTRA(dat.split("-")[0], userid);
                            }
//                            }
                        }
                    }

                    QIDETAILDao dao = new QIDETAILDao();
                    String[] mnx = dao.getSTS(id[i].split("-")[1].trim(), id[i].split("-")[0].trim());

                    QIDETAILDao dao2 = new QIDETAILDao();
                    String tqty = dao2.getTQY(id[i].split("-")[1].trim(), id[i].split("-")[0].trim());

                    DecimalFormat formatDou = new DecimalFormat("#,##0.00");
                    if (tqty == null) {
                        double tot = Double.parseDouble("0.000");
                        tqty = formatDou.format(tot);
                    } else {
                        double tot = Double.parseDouble(tqty);
                        tqty = formatDou.format(tot);
                    }

                    QIHEADDao dao3 = new QIHEADDao();
                    dao3.SETSTS(mnx[0], mnx[1], id[i].split("-")[1].trim(), id[i].split("-")[0].trim(), tqty.trim().replace(",", ""));
                } else if (id[i].split("-")[2].charAt(2) == 'A') {
                    QIDETAILDao dao22 = new QIDETAILDao();
                    List<QIDETAIL> deList = dao22.findAll(id[i].split("-")[1], id[i].split("-")[0]);

                    for (int j = 0; j < deList.size(); j++) {
                        String dat = "";
                        if (deList.get(j).getGID() == null || deList.get(j).getGID().equals("")) {
                            dat = deList.get(j).getId() + "-" + deList.get(j).getSts() + "-" + deList.get(j).getNo();
                        } else {
                            dat = deList.get(j).getGID() + "-" + deList.get(j).getSts() + "-" + deList.get(j).getNo();
                        }
//                        System.out.println(dat);
                        if (dat.trim().split("-")[0].length() < 17) {
                            QIDETAILDao daoq = new QIDETAILDao();
                            List<String> idList = daoq.UngroupQRID(dat.trim());
                            for (int k = 0; k < idList.size(); k++) {
//                                QRMMASDao daoid = new QRMMASDao();
//                                if (daoid.CheckApprove(idList.get(k).trim()).equals("3")) {
                                QIDETAILDao dao2 = new QIDETAILDao();
                                if (dao2.approveMVT301and311(idList.get(k).trim(), userid, id[i].split("-")[0])) {
                                    QRMMASDao up = new QRMMASDao();
                                    up.UpdateStatusMVT541(idList.get(k).split("-")[0], "P");

                                    QRMTRADao adTRA = new QRMTRADao();
                                    adTRA.AddQRMTRA(idList.get(k).split("-")[0], userid);
                                }
//                                }
                            }
                        } else if (dat.trim().split("-")[0].length() == 17) {
//                            QRMMASDao daoid = new QRMMASDao();
//                            if (daoid.CheckApprove(dat.trim()).equals("3")) {
                            QIDETAILDao dao2 = new QIDETAILDao();
                            if (dao2.approveMVT301and311(dat.trim(), userid, id[i].split("-")[0])) {
                                QRMMASDao up = new QRMMASDao();
                                up.UpdateStatusMVT541(dat.split("-")[0], "P");

                                QRMTRADao adTRA = new QRMTRADao();
                                adTRA.AddQRMTRA(dat.split("-")[0], userid);
                            }
//                            }
                        }
                    }

                    QIDETAILDao dao = new QIDETAILDao();
                    String[] mnx = dao.getSTS(id[i].split("-")[1].trim(), id[i].split("-")[0].trim());

                    QIDETAILDao dao2 = new QIDETAILDao();
                    String tqty = dao2.getTQY(id[i].split("-")[1].trim(), id[i].split("-")[0].trim());

                    DecimalFormat formatDou = new DecimalFormat("#,##0.00");
                    if (tqty == null) {
                        double tot = Double.parseDouble("0.000");
                        tqty = formatDou.format(tot);
                    } else {
                        double tot = Double.parseDouble(tqty);
                        tqty = formatDou.format(tot);
                    }

                    QIHEADDao dao3 = new QIHEADDao();
                    dao3.SETSTS(mnx[0], mnx[1], id[i].split("-")[1].trim(), id[i].split("-")[0].trim(), tqty.trim().replace(",", ""));
                } else {
                    QIDETAILDao dao22 = new QIDETAILDao();
                    List<QIDETAIL> deList = dao22.findAll(id[i].split("-")[1], id[i].split("-")[0]);

                    for (int j = 0; j < deList.size(); j++) {
                        String dat = "";
                        if (deList.get(j).getGID() == null || deList.get(j).getGID().equals("")) {
                            dat = deList.get(j).getId() + "-" + deList.get(j).getSts() + "-" + deList.get(j).getNo();
                        } else {
                            dat = deList.get(j).getGID() + "-" + deList.get(j).getSts() + "-" + deList.get(j).getNo();
                        }
//                        System.out.println(dat);
                        if (dat.split("-")[0].length() < 17) {
                            QIDETAILDao daoq = new QIDETAILDao();
                            List<String> idList = daoq.UngroupQRID(dat);
                            for (int k = 0; k < idList.size(); k++) {
//                                QRMMASDao daoid = new QRMMASDao();
//                                if (daoid.CheckApprove(idList.get(k).trim()).equals("3")) {
                                QIDETAILDao dao2 = new QIDETAILDao();
                                if (dao2.approve(idList.get(k).trim(), userid, id[i].split("-")[0])) {
                                    QRMMASDao up = new QRMMASDao();
                                    up.UpdateStatus(idList.get(k).split("-")[0], "4");
                                }
//                                }
                            }
                        } else if (dat.split("-")[0].length() == 17) {
//                            QRMMASDao daoid = new QRMMASDao();
//                            if (daoid.CheckApprove(dat).equals("3")) {
                            QIDETAILDao dao2 = new QIDETAILDao();
                            if (dao2.approve(dat, userid, id[i].split("-")[0])) {
                                QRMMASDao up = new QRMMASDao();
                                up.UpdateStatus(dat.split("-")[0], "4");
                            }
//                            } 
                        }
                    }

                    QIDETAILDao dao = new QIDETAILDao();
                    String[] mnx = dao.getSTS(id[i].split("-")[1].trim(), id[i].split("-")[0].trim());

                    QIDETAILDao dao2 = new QIDETAILDao();
                    String tqty = dao2.getTQY(id[i].split("-")[1].trim(), id[i].split("-")[0].trim());

                    DecimalFormat formatDou = new DecimalFormat("#,##0.00");
                    if (tqty == null) {
                        double tot = Double.parseDouble("0.000");
                        tqty = formatDou.format(tot);
                    } else {
                        double tot = Double.parseDouble(tqty);
                        tqty = formatDou.format(tot);
                    }

                    QIHEADDao dao3 = new QIHEADDao();
                    dao3.SETSTS(mnx[0], mnx[1], id[i].split("-")[1].trim(), id[i].split("-")[0].trim(), tqty.trim().replace(",", ""));
                }
            }
        }

        response.setHeader("Refresh", "0;/TABLE2/WMS310/Fdisplay?WHS=" + wh + "&MTCTRL=" + matCtrl + "");

    }

}
