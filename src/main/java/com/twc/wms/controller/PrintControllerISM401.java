package com.twc.wms.controller;

import com.twc.wms.database.connectiondb;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRResultSetDataSource;
import net.sf.jasperreports.engine.JRRuntimeException;
import net.sf.jasperreports.engine.JasperRunManager;

/**
 *
 * @author 93176
 */
public class PrintControllerISM401 extends HttpServlet {

    private static final String FILE_DEST = "/report/output/";

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        Connection connection = null;
        ResultSet rs = null;
        ResultSet rs2 = null;
        PreparedStatement ps = null;
        PreparedStatement ps2 = null;

        String cust = request.getParameter("cust");
        String salesFrom = request.getParameter("salesf");
        String salesTo = request.getParameter("salest");
        String seqFrom = request.getParameter("seqf");
        String seqTo = request.getParameter("seqt");

        String pathPDF = "";

        String sql = "SELECT * , CASE WHEN VALUE IS NULL THEN 0.00 ELSE VALUE END AS VALUE2,ROW_NUMBER() OVER(ORDER BY QR1.SaleNo,QR1.SqcNo,No) AS Row# FROM ( \n"
                + "	SELECT MASH.ISHSTYLE + ' #' + MASH.ISHLOT + ' ' + MASH.ISHCOLOR + ' ' + MASH.ISHAMTFG AS LOT\n"
                + "			,' ' + MASH.ISHPOFG AS RUNNO\n"
                + "			,RIGHT(MASH.ISHCUNO, 6) AS CUSTNO\n"
                + "			,MASH.ISHMVT AS DOCNO\n"
                + "			,MASD.ISDPLANT AS PLANT\n"
                + "			,ISNULL(MASH.ISHCUNM1,'') + ISNULL(MASH.ISHCUNM2,'') AS NAME\n"
                + "			, MASD.ISDSTRG AS LOCA\n"
                + "			,FORMAT(CONVERT(DATE, CONVERT(nvarchar, MASH.ISHTRDT)), 'dd.MM.yyyy', 'en-us') AS DATE\n"
                + "			,RTRIM(ISNULL(MASH.ISHCUAD1,'')) + ' ' + LTRIM(ISNULL(MASH.ISHCUAD2,'')) AS ADDR\n"
                + "			,ISNULL(MASD.ISDVT,'00') AS VT \n"
                + "			,MASD.ISDPRICE AS PRICE\n"
                + "			,MASH.ISHORD AS [SaleNo]\n"
                + "			,MASH.ISHBSTKD AS [PO]\n"
                + "			,MASD.ISDLINO AS [No]\n"
                + "			,MASD.ISDITNO AS [Material]\n"
                + "			,SMAS.[SAPDESC] AS [Description]\n"
                + "			,MASD.[ISDUNIT] AS [U/M]\n"
                + "			,SUM(ISNULL(MASD.ISDRQQTY,0)) AS REQ\n"
                + "			,MAX(ISNULL(MASD.ISDUNR01,0)) AS onHand1\n"
                + "			,MAX(ISNULL(MASD.ISDUNR03,0)) AS onHand2\n"
                + "			,CASE WHEN MAX(ISNULL(MASD.ISDUNR01,0))+ MAX(ISNULL(MASD.ISDUNR03,0)) - SUM(ISNULL(MASD.ISDRQQTY,0)) < 0 THEN '-' END AS onHandDiff\n"
                + "			,MAX(MASH.ISHPOFG) AS CusNo\n"
                + "			,MAX(MASH.ISHCUNM1) AS CusName\n"
                + "			,' ' + MASH.ISHPOFG AS GrpSNo\n"
                + "			,MAX(MASD.ISDMTCTRL) AS MatCtrlNo\n"
                + "			,(SELECT TOP 1 MATCNAME FROM MSSMATN MASN WHERE MASN.LGPBE = MAX(MASD.ISDMTCTRL)) AS MatCtrlName\n"
                + "			,MAX(CHAR(MASD.ISDSEQ + 64)) AS SqcNo, MAX(MASD.ISDSEQ) AS ISSUENO\n"
                + "			,SUM(MASD.ISDPKQTY) AS VALUE\n"
                + "                     ,ISNULL(MASH.ISHREASON,'') AS ISHREASON\n"
                + "\n"
                + "	FROM [RMShipment].[dbo].[ISMMASD] MASD\n"
                + "	FULL JOIN [RMShipment].[dbo].[ISMMASH] MASH ON MASH.ISHORD = MASD.ISDORD\n"
                + "	FULL JOIN [SAPMAS] SMAS ON [SAPMAT] = MASD.[ISDITNO] collate SQL_Latin1_General_CP1_CI_AS AND [SAPVAL] = ISNULL(MASD.ISDVT,'00') collate SQL_Latin1_General_CP1_CI_AS\n"
                + "	WHERE (MASD.ISDORD BETWEEN '" + salesFrom.trim() + "' AND '" + salesTo.trim() + "') AND (MASD.ISDSEQ BETWEEN '" + seqFrom.trim() + "' AND '" + seqTo.trim() + "') AND MASH.ISHCUNO = '0000" + cust.trim() + "'\n"
                + "\n"
                + "	GROUP BY MASH.ISHSTYLE,MASH.ISHLOT,MASH.ISHCOLOR,MASH.ISHAMTFG,MASH.ISHPOFG,MASH.ISHCUNO,MASH.ISHMVT,MASD.ISDPLANT,MASH.ISHCUNM1\n"
                + "	,MASH.ISHCUNM2,MASD.ISDSTRG,MASH.ISHTRDT,MASH.ISHCUAD1,MASH.ISHCUAD2,MASD.ISDVT,MASD.ISDPRICE,MASH.ISHORD,MASH.ISHBSTKD\n"
                + "	,MASD.ISDLINO,MASD.ISDITNO,SMAS.[SAPDESC],MASD.[ISDUNIT],MASD.ISDORD,MASD.ISDSEQ,MASH.ISHREASON\n"
                + "\n"
                + ") QR1\n"
                + "\n"
                + "ORDER BY QR1.SaleNo,QR1.SqcNo,No";

        try {

//            System.out.println(sql);
            connectiondb mssqlc = new connectiondb();
            connection = mssqlc.getConnection();
            ps = connection.prepareStatement(sql);
            ps2 = connection.prepareStatement(sql);
            rs = ps.executeQuery();
            rs2 = ps2.executeQuery();

            //find reason then select from 2 jasper files
            String reason = "";

            while (rs2.next()) {
                reason = rs2.getString("ISHREASON");
            }

            String pathJ;

            if (reason.equals("ขายเพิ่ม")) {
                pathJ = "/resources/jasper/ISM401_RP_Sales.jasper";
            } else {
                pathJ = "/resources/jasper/ISM401_RP.jasper";
            }

            File reportFile = new File(getServletConfig().getServletContext()
                    .getRealPath(pathJ));

            if (!reportFile.exists()) {
                throw new JRRuntimeException("File *.jasper not found. The report design must be compiled first.");
            } else {
                System.out.println("Found File Jasper");
            }

            byte[] bytes;

            File pdffile = new File(getServletConfig().getServletContext()
                    .getRealPath("/report/output/ISM401_RP.pdf"));

            if (pdffile.exists()) { //Delete PDF File Before Create New
                if (pdffile.delete()) {
                    System.out.println("DELETE PDF File Successful");
                } else {
                    System.out.println("DELETE PDF File Failed");
                }
            } else {
                System.out.println("Not Found PDF Files");
            }

            System.out.println("generate PDF File...... ISM401 ");

            bytes = JasperRunManager.runReportToPdf(reportFile.getPath(), null, new JRResultSetDataSource(rs));

            ServletContext servletContext = getServletContext();
            String realPath = servletContext.getRealPath(FILE_DEST);
            String saperate = realPath.contains(":") ? "\\" : "/";
            String path = realPath + saperate + "ISM401_RP.pdf";

            FileOutputStream out = new FileOutputStream(path);
            out.write(bytes, 0, bytes.length);

            pathPDF = "http://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath() + "/report/output/ISM401_RP.pdf";

            rs.close();
            connection.close();
            ps.close();

        } catch (SQLException ex) {
            Logger.getLogger(PrintControllerISM401.class.getName()).log(Level.SEVERE, null, ex);
        } catch (JRException ex) {
            Logger.getLogger(PrintControllerISM401.class.getName()).log(Level.SEVERE, null, ex);
        }

        response.setStatus(response.SC_MOVED_TEMPORARILY);
        response.setHeader("Location", pathPDF);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

}
