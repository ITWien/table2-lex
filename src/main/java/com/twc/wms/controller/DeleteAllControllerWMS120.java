/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.controller;

import com.twc.wms.dao.PALMOVEDao;
import com.twc.wms.dao.QIDETAILDao;
import com.twc.wms.dao.WHDao;
import com.twc.wms.entity.WH;
import com.twc.wms.dao.QIHEADDao;
import com.twc.wms.dao.QRMMASDao;
import com.twc.wms.dao.QRMTRADao;
import com.twc.wms.dao.QdestDao;
import com.twc.wms.entity.MSSMATN;
import com.twc.wms.entity.PALMOVE;
import com.twc.wms.entity.QIDETAIL;
import com.twc.wms.entity.Qdest;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author wien
 */
public class DeleteAllControllerWMS120 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static final String PAGE_VIEW = "../views/displayWMS120.jsp";

    public DeleteAllControllerWMS120() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("PROGRAMNAME", "WMS120");
        String forward = "";
        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "Pallet Movement. Entry");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
//    ********
        String[] ID = request.getParameterValues("ID");

        if (ID != null) {
            for (int i = 0; i < ID.length; i++) {
                PALMOVEDao daoa = new PALMOVEDao();
                daoa.delete(ID[i]);
            }
        }

        response.setHeader("Refresh", "0;/TABLE2/WMS120/display");
    }

}
