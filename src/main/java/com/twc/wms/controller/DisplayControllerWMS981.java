/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.controller;

import com.twc.wms.dao.QIDETAILDao;
import com.twc.wms.dao.WHDao;
import com.twc.wms.entity.WH;
import com.twc.wms.dao.QIHEADDao;
import com.twc.wms.dao.QdestDao;
import com.twc.wms.dao.UserDao;
import com.twc.wms.dao.WMS981Dao;
import com.twc.wms.dao.XMSWIHEADDao;
import com.twc.wms.entity.MSSMATN;
import com.twc.wms.entity.QIDETAIL;
import com.twc.wms.entity.Qdest;
import com.twc.wms.entity.UserAuth;
import com.twc.wms.entity.WMS981;
import com.twc.wms.entity.XMSWIHEAD;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.poi.sl.draw.geom.Path;

/**
 *
 * @author wien
 */
public class DisplayControllerWMS981 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static final String PAGE_VIEW = "../views/displayWMS981.jsp";

    public DisplayControllerWMS981() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        request.setAttribute("PROGRAMNAME", "WMS981");
        String forward = "";
        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "Summary Check Stock Yearly by Product and Warehouse. Display");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
//    ********
        request.setAttribute("whd", "All");
        request.setAttribute("detail", "checked");
        request.setAttribute("aList", "");
        request.setAttribute("plant", "ALL");

        List<WMS981> ohdList = new WMS981Dao().findOHDate();
        request.setAttribute("ohdList", ohdList);

        List<WMS981> whList = new WMS981Dao().findWH();
        request.setAttribute("whList", whList);

        List<WMS981> deptList = new WMS981Dao().findDept();
        request.setAttribute("deptList", deptList);

        for (int i = 0; i < 8; i++) {
            request.setAttribute("checked" + (i + 1), "checked=\"true\"");
        }

        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        request.setAttribute("PROGRAMNAME", "WMS981");
        String forward = "";
        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "Summary Check Stock Yearly by Product and Warehouse. Display");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
//    ********
        String[] selectCheck = request.getParameterValues("selectCheck");
        String whd = request.getParameter("whd");
        String detail = request.getParameter("detail");
        String plant = request.getParameter("plant");

        request.setAttribute("whd", whd);
        request.setAttribute("detail", detail);
        request.setAttribute("plant", plant);

        String sinv = "";
        String sinv2 = "";
        String aList = "";

        if (selectCheck != null) {
            for (int i = 0; i < selectCheck.length; i++) {
                sinv += "<label style=\"cursor: pointer;\"><input type=\"checkbox\" name=\"selectCheck\" value=\"" + selectCheck[i] + "\" style=\"width: 15px; height: 15px;\">  " + selectCheck[i] + "</label><br>";
                sinv2 += "<label style=\"cursor: pointer;\"><input type=\"checkbox\" name=\"selectCheck\" value=\"" + selectCheck[i] + "\" style=\"width: 15px; height: 15px;\">  " + selectCheck[i] + "</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
                if (i == 0) {
                    aList += "'" + selectCheck[i] + "'";
                } else {
                    aList += ", '" + selectCheck[i] + "'";
                }
            }
        }

        request.setAttribute("sinv", sinv);
        request.setAttribute("sinv2", sinv2);
        request.setAttribute("aList", aList);

        List<WMS981> detailList = new WMS981Dao().findDetail(selectCheck, whd, detail, plant);
        request.setAttribute("detailList", detailList);

        double ohSKU = 0.0;
        double ckSKU = 0.0;
        double colSKU = 0.0;
        double diffSKU = 0.0;
        double amount = 0.0;
        double packpcs = 0.0;
        double amount2 = 0.0;
        double packpcs2 = 0.0;

        for (int i = 0; i < detailList.size(); i++) {
            ohSKU += Double.parseDouble(detailList.get(i).getOH_SKU());
            ckSKU += Double.parseDouble(detailList.get(i).getCK_SKU());
            colSKU += Double.parseDouble(detailList.get(i).getCOL_SKU());
            diffSKU += Double.parseDouble(detailList.get(i).getDIFF_SKU());
            amount += Double.parseDouble(detailList.get(i).getAMOUNT());
            packpcs += Double.parseDouble(detailList.get(i).getPACKPCS());
            amount2 += Double.parseDouble(detailList.get(i).getAMOUNT2());
            packpcs2 += Double.parseDouble(detailList.get(i).getPACKPCS2());
        }

        WMS981 sum = new WMS981();
        sum.setOH_SKU(Double.toString(ohSKU));
        sum.setCK_SKU(Double.toString(ckSKU));
        sum.setCK_SKU_PER(Double.toString((ckSKU / ohSKU) * 100));
        sum.setCOL_SKU(Double.toString(colSKU));
        sum.setCOL_SKU_PER(Double.toString((colSKU / ohSKU) * 100));
        sum.setDIFF_SKU(Double.toString(diffSKU));
        sum.setDIFF_SKU_PER(Double.toString((diffSKU / ohSKU) * 100));
        sum.setAMOUNT(Double.toString(amount));
        sum.setPACKPCS(Double.toString(packpcs));
        sum.setAMOUNT2(Double.toString(amount2));
        sum.setPACKPCS2(Double.toString(packpcs2));
        if (ohSKU == 0) {
            sum.setCK_SKU_PER(Double.toString(0));
            sum.setCOL_SKU_PER(Double.toString(0));
            sum.setDIFF_SKU_PER(Double.toString(0));
        }
        request.setAttribute("sum", sum);

        List<WMS981> ohdList = new WMS981Dao().findOHDate();
        request.setAttribute("ohdList", ohdList);

        List<WMS981> whList = new WMS981Dao().findWH();
        request.setAttribute("whList", whList);

        List<WMS981> deptList = new WMS981Dao().findDept();
        request.setAttribute("deptList", deptList);

        String[] lab = request.getParameterValues("labN");

        for (int i = 0; i < 8; i++) {
            boolean tt = false;
            if (lab != null) {
                for (int j = 0; j < lab.length; j++) {
                    if ((i + 1) == Integer.parseInt(lab[j])) {
                        tt = true;
                    }
                }
            }
            if (tt) {
                request.setAttribute("checked" + (i + 1), "checked=\"true\"");
            }
        }

        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);
    }

}
