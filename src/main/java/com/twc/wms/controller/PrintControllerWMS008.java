/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.controller;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.twc.wms.dao.WarehouseDao;
import com.twc.wms.entity.Warehouse;
import java.util.List;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;
import com.twc.wms.dao.QRKDETAILDao;
import com.twc.wms.entity.QRKDETAIL;
import java.io.FileOutputStream;
import com.itextpdf.text.Font.FontFamily;
import javax.servlet.ServletContext;

/**
 *
 * @author wien
 */
public class PrintControllerWMS008 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static final String PAGE_VIEW = "../views/printWMS008.jsp";
    private static final String FILE_PATH = "/report/QR/";

    public PrintControllerWMS008() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("PROGRAMNAME", "WMS008/P");
        String forward = "";
        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "Rack Master. Display");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
//    ********
        WarehouseDao dao = new WarehouseDao();
        List<Warehouse> pList = dao.selectWarehouse();

        RequestDispatcher view = request.getRequestDispatcher(forward);
        request.setAttribute("WHList", pList);
        view.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        request.setCharacterEncoding("utf-8");

        String wh = request.getParameter("wh");

        request.setAttribute("PROGRAMNAME", "WMS008/P");
        request.setAttribute("PROGRAMDESC", "Rack Master. Display");

        String Srkno = request.getParameter("Srkno");
        String Frkno = request.getParameter("Frkno");

        if (Srkno.trim().length() == 1) {
            Srkno = "0" + Srkno;
        }
        if (Frkno.trim().length() == 1) {
            Frkno = "0" + Frkno;
        }

        String Sside = request.getParameter("Sside");
        String Fside = request.getParameter("Fside");
        String Scolumn = request.getParameter("Scolumn");
        String Fcolumn = request.getParameter("Fcolumn");
        String Srow = request.getParameter("Srow");
        String Frow = request.getParameter("Frow");

        QRKDETAILDao dao = new QRKDETAILDao();
        List<QRKDETAIL> loc = dao.findLoc(wh, Srkno, Frkno, Sside, Fside, Scolumn, Fcolumn, Srow, Frow);

        String PDFfilename = "";
        String path = "";

        try {

            Rectangle pageSize = new Rectangle(164, 116);
            Document document = new Document(pageSize, 0, 0, 0, -100);
            String QRcode = "Print_Rack_QRcode";
            PDFfilename = QRcode;
            // Edit by ji
            ServletContext servletContext = getServletContext();    // new by ji
            String realPath = servletContext.getRealPath(FILE_PATH);        // new by ji
            String saperate = realPath.contains(":") ? "\\" : "/";
            path = realPath + saperate + QRcode + ".pdf";        // new by ji
//            String path = "C:/Users/nutthawoot.noo/Desktop/" + QRcode + ".pdf";       // old by lek
//            String path = "C:\\Users\\nutthawoot.noo\\Documents\\NetBeansProjects\\TWC-WMS-WMS009\\TWC-WMS-WMS008\\src\\main\\webapp\\report\\QR\\" + QRcode + ".pdf";   // new path by ji

            PdfWriter.getInstance(document, new FileOutputStream(path));

            document.open();

            for (int i = 0; i < loc.size(); i++) {
                int QRsize = 130;
                BarcodeQRCode barcodeQRCode = new BarcodeQRCode(loc.get(i).getRkno(), 1000, 1000, null);
                Image codeQrImage = barcodeQRCode.getImage();
                codeQrImage.scaleAbsolute(QRsize, QRsize);
                codeQrImage.setAbsolutePosition(16, -3);
                document.add(codeQrImage);

                Paragraph p2 = new Paragraph(loc.get(i).getRkno(), new Font(FontFamily.HELVETICA, 12));
                p2.setAlignment(Element.ALIGN_CENTER);
                p2.setSpacingBefore(95);
                document.add(p2);

                document.newPage();
            }

            document.close();

        } catch (Exception e) {
            System.err.println(e.toString());
        }

        String forward = PAGE_VIEW;

        WarehouseDao dao1 = new WarehouseDao();
        List<Warehouse> pList = dao1.selectWarehouse();

        WarehouseDao dao2 = new WarehouseDao();

        request.setAttribute("wh", wh);
        request.setAttribute("whn", dao2.findWHname(wh));
        request.setAttribute("Srkno", Srkno);
        request.setAttribute("Frkno", Frkno);
        request.setAttribute("Sside", Sside);
        request.setAttribute("Fside", Fside);
        request.setAttribute("Scolumn", Scolumn);
        request.setAttribute("Fcolumn", Fcolumn);
        request.setAttribute("Srow", Srow);
        request.setAttribute("Frow", Frow);

        String showPDF = "";

        showPDF = "<script type=\"text/javascript\">\n"
                + "                 window.onload = function (e) {\n"
                + "                     window.open(\"../report/QR/" + PDFfilename + ".pdf\");\n"
                + "                 };\n"
                + "        </script>";

        request.setAttribute("showPDF", showPDF);

        RequestDispatcher view = request.getRequestDispatcher(forward);
        request.setAttribute("WHList", pList);
        view.forward(request, response);
    }

}
