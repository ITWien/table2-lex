/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.controller;

import com.twc.wms.dao.ISM800Dao;
import com.twc.wms.entity.ISMSDWD;
import com.twc.wms.entity.ISMSDWH;
import com.twc.wms.entity.ISMSDWS;
import com.twc.wms.service.BAPIRET2;
import com.twc.wms.service.TABLEOFBAPIRET2;
import com.twc.wms.service.TABLEOFZBAPICHARG;
import com.twc.wms.service.TABLEOFZBAPICREATEBY;
import com.twc.wms.service.TABLEOFZBAPICUSTOMER;
import com.twc.wms.service.TABLEOFZBAPILGORT;
import com.twc.wms.service.TABLEOFZBAPIMATNR;
import com.twc.wms.service.TABLEOFZBAPIPLANT;
import com.twc.wms.service.TABLEOFZBAPIRMHEAD01;
import com.twc.wms.service.TABLEOFZBAPIRMITEM01;
import com.twc.wms.service.TABLEOFZBAPISALESDATE;
import com.twc.wms.service.TABLEOFZBAPISALESNO;
import com.twc.wms.service.TABLEOFZBAPISTOCKRM01;
import com.twc.wms.service.ZBAPICHARG;
import com.twc.wms.service.ZBAPICREATEBY;
import com.twc.wms.service.ZBAPICUSTOMER;
import com.twc.wms.service.ZBAPILGORT;
import com.twc.wms.service.ZBAPIMATNR;
import com.twc.wms.service.ZBAPIPLANT;
import com.twc.wms.service.ZBAPIRMHEAD01;
import com.twc.wms.service.ZBAPIRMITEM01;
import com.twc.wms.service.ZBAPISALESDATE;
import com.twc.wms.service.ZBAPISALESNO;
import com.twc.wms.service.ZBAPISTOCKRM01;
import com.twc.wms.service.ZBAPITWCMMRMSTOCKLISTResponse;
import com.twc.wms.service.ZBAPITWCSDRMGETSALES01Response;
import com.twc.wms.service.ZWSTWCMMRMSTOCKLIST;
import com.twc.wms.service.ZWSTWCMMRMSTOCKLISTService;
import com.twc.wms.service.ZWSTWCSDRMGETSALES01;
import com.twc.wms.service.ZWSTWCSDRMGETSALES01Service;
//import com.twc.wms.service.ZWSTWCSDRMGETSALES01
import java.io.IOException;
import java.net.Authenticator;
import java.net.PasswordAuthentication;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.Future;
import java.util.regex.Pattern;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.json.JSONObject;

/**
 *
 * @author 93176
 */
public class CreateControllerISM800 extends HttpServlet {

    private static final String PAGE_VIEW = "../views/createISM800.jsp";
    static String GG = "";

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        request.setAttribute("PROGRAMNAME", "ISM800/C");
        String forward = "";
        try {

            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "Sales Order Download. Process");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
//    ********
        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);

    }

    public static String toAscii(String s) {
        StringBuffer output = new StringBuffer();
        int size = s.length();
        for (int i = 0; i < size; i++) {
            char c = s.charAt(i);
            if (c >= 161 + 3424 && c <= 251 + 3424) {
                c = (char) (c - 3424);
            }
            output.append(c);
        }
        //System.out.println(output);
        return output.toString();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        try {

            String cusSelect = request.getParameter("cusSelect"); //210026 : บริษัท วาโก้ลำพูน จำกัด
            String saledatef = request.getParameter("saledatef");
            String saledatet = request.getParameter("saledatet");
            String delidatef = request.getParameter("delidatef");
            String delidatet = request.getParameter("delidatet");
            String salelist = request.getParameter("saleOrdList"); //saleOrdList  labelte
            String createbf = request.getParameter("createbf");
            String createbt = request.getParameter("createbt");

            if (!delidatef.equals("")) {
                delidatef = delidatef.split("/")[2] + "-" + delidatef.split("/")[1] + "-" + delidatef.split("/")[0];
            }
            if (!delidatet.equals("")) {
                delidatet = delidatet.split("/")[2] + "-" + delidatet.split("/")[1] + "-" + delidatet.split("/")[0];
            }

            String optionSaleDate = "";
            String optionSaleOrder = "";
            String optionCreateBy = "";

            if (!saledatef.equals("")) {
                saledatef = saledatef.split("/")[2] + "-" + saledatef.split("/")[1] + "-" + saledatef.split("/")[0];
            }
            if (!saledatet.equals("")) {
                saledatet = saledatet.split("/")[2] + "-" + saledatet.split("/")[1] + "-" + saledatet.split("/")[0];
            }

            if (saledatef.equals("") && !saledatet.equals("")) {
                saledatef = saledatet;
                optionSaleDate = "EQ";
            } else if (!saledatef.equals("") && saledatet.equals("")) {
                optionSaleDate = "EQ";
            } else if (!saledatef.equals("") && !saledatet.equals("")) {
                optionSaleDate = "BT";
            }

            if (createbf.contains("*")) { //case => TBUS*
                optionCreateBy = "CP";
            } else {
                if (createbf.equals("") && !createbt.equals("")) {
                    createbf = createbt;
                    optionCreateBy = "EQ";
                } else if (!createbf.equals("") && createbt.equals("")) {
                    optionCreateBy = "EQ";
                } else if (!createbf.equals("") && !createbt.equals("")) {
                    optionCreateBy = "BT";
                }
            }

            String saleorderf = request.getParameter("saleorderf");
            String saleordert = request.getParameter("saleordert");
            String procurf = request.getParameter("procurf");
            String procurt = request.getParameter("procurt");
            String proCurList = request.getParameter("proCurList");
            String radio = request.getParameter("radio");

//            System.out.println(cusSelect);
//            System.out.println("sdate " + saledatef + " " + saledatet);
//            System.out.println("ddate " + delidatef + " " + delidatet);
//            System.out.println(saleorderf + " " + saleordert);
//            System.out.println("salelist " + salelist);
//            System.out.println(procurf + " " + procurt);
//            System.out.println("proCurList " + proCurList);
//            System.out.println("radio " + new String(radio.getBytes("iso-8859-1"), "UTF-8")); //to reason
            List<String> listPurG = new ArrayList<String>();

            if (!proCurList.equals("")) { //PurList
                String[] splitedPur = proCurList.split("\\s+");

                for (int i = 0; i < splitedPur.length; i++) {
                    listPurG.add(splitedPur[i]);
                }

            } else { //Pur From-To

                if (!procurf.equals("") && !procurt.equals("")) {
                    char fchar = procurf.charAt(0); //Char From
                    char tchar = procurt.charAt(0); //Char To

                    int asciiFrom = (int) fchar;
                    int asciiTo = (int) tchar;

                    int f = Integer.parseInt(procurf.replace(fchar, '0'));
                    int t = Integer.parseInt(procurt.replace(tchar, '0'));

                    String StrFrom = String.valueOf(asciiFrom) + procurf.replace(fchar, ' ');
                    String StrTo = String.valueOf(asciiTo) + procurt.replace(tchar, ' ');

                    int loopChar = asciiTo - asciiFrom;

                    if (loopChar == 0) {
                        loopChar = 1;
                    }

                    int condiFrom = Integer.parseInt(StrFrom.replace(" ", ""));
                    int condiTo = Integer.parseInt(StrTo.replace(" ", ""));

                    for (int i = 0; i <= loopChar; i++) {

                        char CurrentChar = (char) asciiFrom;

                        while (condiFrom <= condiTo) {

                            if (f > 99) {
                                f = 1;
                                asciiFrom++;
                                CurrentChar = (char) asciiFrom;
                            }

                            String ftos = Integer.toString(f);

                            if (ftos.length() < 2) {
                                ftos = "0" + ftos;
                            }

                            String merge = CurrentChar + "" + ftos;
                            listPurG.add(merge);

                            f++;
                            condiFrom++;
                        }
                    }
                }
            }
            if (true) {

//            if ((!optionSaleDate.equals("") && !optionSaleOrder.equals("")) || (optionSaleDate.equals("") && !optionSaleOrder.equals("")) || (!optionSaleDate.equals("") && optionSaleOrder.equals(""))) {
                String SelectProd = request.getParameter("SelectProd"); //1F : FOUNDATION

                String uid = request.getParameter("userid");

                String alrt = "";

                String again = request.getParameter("again");

                String forward = "";

                String user = uid;
                String seq = null;

                try {

                    //ต้องแบ่งเป็นแบบปกติ กับ แบบ list เพราะ dwnno ใช้ไม่เหมือนกัน
                    if (!salelist.equals("")) {

                        System.out.println("this New");

                        //------------------------------- Order List -----------------------------
                        String[] splited = salelist.split("\\s+");

                        String maxdwnno = new ISM800Dao().SelectMax();

                        List<String> listOrdTrue = new ArrayList<String>();
                        List<String> listOrdFalse = new ArrayList<String>();
                        boolean permis = false;

                        for (int z = 0; z < splited.length; z++) { //loop all order no

                            List<ISMSDWH> Headlist = new ArrayList<ISMSDWH>();
                            List<ISMSDWD> invDetailList = new ArrayList<ISMSDWD>();
                            List<ISMSDWS> sldetail = new ArrayList<ISMSDWS>();

                            String order = splited[z];

                            if (order.length() == 10) {

                                Authenticator.setDefault(new Authenticator() {
                                    @Override
                                    protected PasswordAuthentication getPasswordAuthentication() {

                                        Calendar c = Calendar.getInstance();
                                        int month = c.get(Calendar.MONTH) + 1;

                                        String m = Integer.toString(month);
                                        if (m.length() < 2) {
                                            m = "0" + m;
                                        }

                                        String pass = "display" + m;

                                        return new PasswordAuthentication("tviewwien", pass.toCharArray());
                                    }
                                });

                                String DISTR_CHAN = "20";
                                String DIVISION = "00";
                                String DOC_TYPE = "Z1O2";
                                String SALES_ORG = "1000";

                                ZWSTWCSDRMGETSALES01Service service = new ZWSTWCSDRMGETSALES01Service();
                                ZWSTWCSDRMGETSALES01 getsale01 = service.getZWSTWCSDRMGETSALES01SoapBinding();

                                TABLEOFZBAPIPLANT tableplant = new TABLEOFZBAPIPLANT();
                                TABLEOFZBAPISALESDATE tablesaledate = new TABLEOFZBAPISALESDATE();
                                TABLEOFZBAPISALESNO tablesaleno = new TABLEOFZBAPISALESNO();
                                TABLEOFZBAPICUSTOMER tablesaleto = new TABLEOFZBAPICUSTOMER();
                                TABLEOFBAPIRET2 _return = new TABLEOFBAPIRET2();
                                TABLEOFZBAPIRMHEAD01 tablehead01 = new TABLEOFZBAPIRMHEAD01();
                                TABLEOFZBAPIRMITEM01 tableitem01 = new TABLEOFZBAPIRMITEM01();
                                TABLEOFZBAPICREATEBY tablecreateby = new TABLEOFZBAPICREATEBY();

                                ZBAPIPLANT plant = new ZBAPIPLANT();
                                plant.setSIGN("I");
                                plant.setOPTION("EQ");
                                plant.setWERKSLOW("1000");
                                plant.setWERKSHIGH("");
//                tableplant.getItem().add(plant);

                                ZBAPISALESDATE saledate = new ZBAPISALESDATE();
                                saledate.setSIGN("I");
                                saledate.setOPTION(optionSaleDate); //<--- optionSaleDate
                                saledate.setDATELOW(saledatef); //Format Date yyyy-MM-dd "2021-02-29"
                                saledate.setDATEHIGH(saledatet);

                                ZBAPICREATEBY createby = new ZBAPICREATEBY();
                                createby.setSIGN("I");
                                createby.setOPTION(optionCreateBy);
                                createby.setERNAMLOW(createbf);
                                createby.setERNAMHIGH(createbt);

                                if (!createbf.equals("") || !createbt.equals("")) {
                                    tablecreateby.getItem().add(createby);
                                }

                                if (!saledatef.equals("") || !saledatet.equals("")) {
                                    tablesaledate.getItem().add(saledate);
                                }

                                String dwnno = new ISM800Dao().SelectNo(order);

                                optionSaleOrder = "EQ";
                                saleorderf = order;
                                saleordert = order;

                                ZBAPISALESNO saleno = new ZBAPISALESNO();
                                saleno.setSIGN("I");
                                saleno.setOPTION(optionSaleOrder);  //<--- optionSaleOrder
                                saleno.setVBELNLOW(saleorderf); //1420054631
                                saleno.setVBELNHIGH(saleordert);

                                if (!saleorderf.equals("") || !saleordert.equals("")) {
                                    tablesaleno.getItem().add(saleno);
                                }

                                ZBAPICUSTOMER saleto = new ZBAPICUSTOMER(); //210026
                                saleto.setSIGN("I");
                                saleto.setOPTION("EQ");
                                saleto.setKUNNRLOW("0000" + cusSelect.trim()); //0000210026
                                saleto.setKUNNRHIGH("");

                                if (!cusSelect.equals("")) {
                                    tablesaleto.getItem().add(saleto);
                                }
                                Future<?> getsaleAsync = getsale01.zBAPITWCSDRMGETSALES01Async(tablecreateby, DISTR_CHAN, DIVISION, DOC_TYPE, tablehead01, tableitem01, tableplant, _return, tablesaledate, tablesaleno, SALES_ORG, tablesaleto); //Execute --> Can Check Parameter From This Line
                                ZBAPITWCSDRMGETSALES01Response getsale01Response = null;

                                getsale01Response = (ZBAPITWCSDRMGETSALES01Response) getsaleAsync.get();
                                TABLEOFBAPIRET2 sreturn = getsale01Response.getRETURN();

                                for (BAPIRET2 ret : sreturn.getItem()) {
                                    System.out.println("GETSALE_FIELD : " + ret.getFIELD());
                                    System.out.println("ID : " + ret.getID());
                                    System.out.println("LOGNO : " + ret.getLOGNO());
                                    System.out.println("MESSAGE : " + ret.getMESSAGE());
                                    System.out.println("MESSAGEV1 : " + ret.getMESSAGEV1());
                                    System.out.println("MESSAGEV2 : " + ret.getMESSAGEV2());
                                    System.out.println("MESSAGEV3 : " + ret.getMESSAGEV3());
                                    System.out.println("MESSAGEV4 : " + ret.getMESSAGEV4());
                                    System.out.println("NUMBER : " + ret.getNUMBER());
                                    System.out.println("PARAMETER : " + ret.getPARAMETER());
                                    System.out.println("SYSTEM : " + ret.getSYSTEM());
                                    System.out.println("TYPE : " + ret.getTYPE());
                                }

                                TABLEOFZBAPIRMHEAD01 tablegetsale01 = getsale01Response.getORDERHEADERSOUT();

                                String destCC = "";

                                for (ZBAPIRMHEAD01 getsaleHead : tablegetsale01.getItem()) {

                                    ISMSDWH head = new ISMSDWH();
                                    head.setISHCONO("TWC");
                                    head.setISHDWQNO("1"); //(SELECT ISNULL(MAX(EMSDWQNO)+1, 1) FROM EMSDWT)
                                    head.setISHORD(getsaleHead.getSALESNO().trim());
                                    head.setISHSEQ("1"); //
                                    head.setISHTRDT(""); //CURRENT_DATE
                                    head.setISHUART(getsaleHead.getDOCTYPE().trim());
                                    head.setISHSORG(getsaleHead.getSALESORG().trim());
                                    head.setISHTWEG(getsaleHead.getDISTRCHAN().trim());
                                    head.setISHDIVI(getsaleHead.getDIVISION().trim());
                                    head.setISHSGRP(getsaleHead.getSALESGRP().trim());
                                    head.setISHSOFF(getsaleHead.getSALESOFF().trim());
                                    head.setISHMVT(getsaleHead.getMMTYPE().trim());
                                    head.setISHCUNO(getsaleHead.getSOLDTO().trim());
                                    head.setISHCUNM1(getsaleHead.getNAME1().trim());
                                    head.setISHCUNM2(getsaleHead.getNAME2().trim());
                                    head.setISHCUAD1(getsaleHead.getADDR1().trim());
                                    head.setISHCUAD2(getsaleHead.getADDR2().trim());
                                    head.setISHBSTKD(getsaleHead.getPONUMBER().trim());

                                    String poNumber = getsaleHead.getPONUMBER().trim();

                                    //------------------- PO -----------------
                                    //1122007293(WO3114#2205/NN/100/2A) -> normal Type
                                    String poNumberCle = poNumber.replaceAll("  ", " ").replaceAll(" = ", "=").replaceAll("= ", "=").replaceAll(" =", "=").replaceAll(" \\(", "(").replaceAll(" #", "#").trim();

                                    String matlot;
                                    String style;
                                    String color = "null";
                                    String lot;
                                    String amt = "null";

                                    String firChar = poNumberCle.substring(0, 1);

                                    int count = 0;
                                    int countfir = 0;
                                    int countlas = 0;
                                    int countspa = 0;
                                    for (int i = 0; i < poNumberCle.length(); i++) {
                                        switch (poNumberCle.charAt(i)) {
                                            case '/':
                                                count++;
                                                break;
                                            case '(':
                                                countfir++;
                                                break;
                                            case ')':
                                                countlas++;
                                                break;
                                            case ' ':
                                                countspa++;
                                                break;
                                            default:
                                                break;
                                        }
                                    }

                                    if (isNumeric(firChar)) { //true -> num
//                    //substring
                                        if (poNumberCle.contains("(")) {

                                            if (countfir != countlas) {
                                                if (!poNumberCle.substring(poNumberCle.length() - 1).equals(")")) { //last Char is not ')'
                                                    poNumberCle = poNumberCle + ")";
                                                }
                                            }

                                            if (poNumberCle.contains("#")) {

                                                matlot = poNumberCle.substring(poNumberCle.indexOf("(") + 1, poNumberCle.lastIndexOf(")"));

                                                if (matlot.contains("/")) {
                                                    style = matlot.split("#")[0];
                                                    lot = matlot.split("#")[1];

                                                    if (lot.contains("/")) {
                                                        amt = lot.split("/")[2];
                                                        color = lot.split("/")[1];
                                                        lot = lot.split("/")[0];
                                                    } else if (lot.contains(" ")) {
                                                        color = lot.split(" ")[1];

                                                        if (color.contains("=")) {
                                                            amt = color.split("=")[1];
                                                            color = color.split("=")[0];
                                                        } else {
                                                            if (lot.split(" ").length > 2) {
                                                                amt = lot.split(" ")[2];
                                                            }
                                                        }
                                                        lot = lot.split(" ")[0];
                                                    }
                                                } else { //1122035367(WH6K16X1-X5WH03A 1,850#) <- case found at (27/04/2023) 
                                                    style = "";
                                                    color = "";
                                                    lot = "";
                                                    amt = "";
                                                }

                                            } else {

                                                if (poNumberCle.contains("/")) {
                                                    matlot = poNumberCle.substring(poNumberCle.indexOf("(") + 1, poNumberCle.lastIndexOf(")"));
                                                    style = matlot.split("/")[0];
                                                    lot = matlot.split("/")[1];
                                                    color = matlot.split("/")[2];
                                                    amt = matlot.split("/")[3];
                                                } else { //<- this case found at (27/04/2023)
                                                    //เรียงค่าภายใน () มั่ว ไม่สามารถ split ค่าที่ถูกต้องหรือ แยกได้
                                                    //1122033326(ME1E84 E4, E5 WR 40)
                                                    //1122038624(WH6H06 WHE 2,000 PC)
                                                    matlot = poNumberCle.substring(poNumberCle.indexOf("(") + 1, poNumberCle.lastIndexOf(")"));
                                                    style = matlot.split(" ")[0];
                                                    lot = "";
                                                    color = "";
                                                    amt = "";
                                                }

                                            }

                                        } else { //6122000605,  <- this case
                                            matlot = "";
                                            style = "";
                                            color = "";
                                            lot = "";
                                            amt = "";
                                        }
                                    } else { //false -> string
                                        matlot = "";
                                        style = "";
                                        color = "";
                                        lot = "";
                                        amt = "";
                                    }

//                System.out.println("i " + c + " poNumberCle " + poNumberCle + " matlot " + matlot + " style " + style + " color " + color + " lot " + lot + " amt " + amt);
                                    //------------------- END PO -----------------
                                    head.setISHSUBMI(getsaleHead.getSDCOLNUM().trim()); //collective number
                                    head.setISHMATLOT(matlot);
                                    head.setISHDDATE(getsaleHead.getDOCDATE().trim());
                                    head.setISHDTIME(getsaleHead.getDOCTIME().trim());
                                    head.setISHTAXNO("");
                                    head.setISHBRANCH01("");
                                    head.setISHSTYLE(style);
                                    head.setISHCOLOR(color);
                                    head.setISHLOT(lot);
                                    head.setISHAMTFG(amt);  //Ex. 750)
                                    head.setISHSTYLE2("");
                                    head.setISHNOR(""); // จำนวนบรรทัดรวม

                                    String CusCode;

                                    if (cusSelect.equals("210026")) {
                                        CusCode = "WLC";
                                    } else if (cusSelect.equals("210027")) {
                                        CusCode = "PKC";
                                    } else if (cusSelect.equals("210284")) {
                                        CusCode = "WSC";
                                    } else if (cusSelect.equals("210025")) {
                                        CusCode = "WKC";
                                    } else if (cusSelect.equals("211289")) {
                                        CusCode = "MSB";
                                    } else {
                                        CusCode = "";
                                    }

                                    destCC = CusCode;

                                    head.setISHPOFG(CusCode); //รหัสบริษัทย่อของลูกค้า
                                    head.setISHWHNO(""); //รหัสคลังสินค้า
                                    head.setISHPGRP(getsaleHead.getBUSGRP().trim());
                                    head.setISHLSTS("0");
                                    head.setISHHSTS("0");
                                    head.setISHDEST(destCC); //รหัสสถานที่ส่งปลายทาง
                                    head.setISHREMK(""); //หมายเหตุ 
                                    head.setISHCUNO2(getsaleHead.getSHIPTO().trim());
                                    head.setISHDLDT(getsaleHead.getREQDATEH());
                                    head.setISHDLS(getsaleHead.getDELIVSTAT());
                                    head.setISHODLS(getsaleHead.getDLVSTATH());
                                    head.setISHCRDT(getsaleHead.getRECDATE());
                                    head.setISHCRTM(getsaleHead.getRECTIME());
                                    head.setISHSUSER(getsaleHead.getCREATEDBY());
                                    head.setISHEUSR("");
                                    head.setISHUSER(uid);
                                    head.setISHREASON(new String(radio.getBytes("iso-8859-1"), "UTF-8"));
//                    head.setISHDATUM(getsaleHead.getDOCDATE().trim() + " " + getsaleHead.getDOCTIME().trim());
//                    head.setISHGRPNO("");

                                    Headlist.add(head);

                                }

                                TABLEOFZBAPIRMITEM01 tablegetsale01item = getsale01Response.getORDERITEMSOUT();

                                if (!listPurG.isEmpty()) {

                                    DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd");
                                    LocalDate datef = null;
                                    LocalDate datet = null;
                                    LocalDate reqdate = null;

                                    for (ZBAPIRMITEM01 getsaleItem : tablegetsale01item.getItem()) {

                                        boolean checkDate = false;

                                        if (!delidatef.equals("") && !delidatet.equals("")) {
                                            datef = LocalDate.parse(delidatef, formatter);
                                            datet = LocalDate.parse(delidatet, formatter);
                                            reqdate = LocalDate.parse(getsaleItem.getREQDATEI());
                                            checkDate = isWithinRange(reqdate, datef, datet);
                                        } else {
                                            if (delidatef.equals("") || delidatet.equals("")) {
                                                checkDate = true;
                                            }
                                        }

                                        permis = checkDate;

                                        if (checkDate) {

                                            for (int i = 0; i < listPurG.size(); i++) {

                                                if (getsaleItem.getPURGROUP().trim().equals(listPurG.get(i))) {
                                                    ISMSDWD detail = new ISMSDWD();
                                                    detail.setISDCONO("TWC");
                                                    detail.setISDDWQNO("1");
                                                    detail.setISDORD(getsaleItem.getDOCNUMBER().trim());
                                                    detail.setISDSEQ("1");
                                                    detail.setISDLINO(getsaleItem.getITMNUMBER().trim());
//                    detail.setISDSINO("0");
                                                    detail.setISDITNO(getsaleItem.getMATERIAL().trim());
                                                    detail.setISDBAT(getsaleItem.getBATCH());
                                                    detail.setISDMGRP(getsaleItem.getMATLGROUP());
                                                    detail.setISDSTEXT(getsaleItem.getSHORTTEXT());
                                                    detail.setISDITCAT(getsaleItem.getITEMCATEG());
                                                    detail.setISDRSRJ(getsaleItem.getREAFORRE());
                                                    detail.setISDRSRJT(getsaleItem.getREAFORRETXT());
                                                    detail.setISDPLANT(getsaleItem.getPLANT());
                                                    detail.setISDVT(getsaleItem.getVALTYPE());
                                                    detail.setISDSTRG(getsaleItem.getSTGELOC());
                                                    detail.setISDRQQTY(getsaleItem.getREQQTY().toString());
                                                    detail.setISDDLQTY(getsaleItem.getDLVQTY().toString());
                                                    detail.setISDUNIT(getsaleItem.getSALESUNIT());
                                                    detail.setISDBUNIT(getsaleItem.getBASEUOM());
                                                    detail.setISDCON1(getsaleItem.getSALESQTY1().toString());
                                                    detail.setISDCON2(getsaleItem.getSALESQTY2().toString());
                                                    detail.setISDPRICE(getsaleItem.getNETPRICE().toString());
                                                    detail.setISDNETV(getsaleItem.getNETVALUE().toString());
                                                    detail.setISDCUR(getsaleItem.getCURRENCY());
                                                    detail.setISDEXC(getsaleItem.getEXCHRATE().toString());
                                                    detail.setISDMTCTRL(getsaleItem.getMATCONTROL());
                                                    detail.setISDPFCT(getsaleItem.getPROFITCTR());
                                                    detail.setISDACGP(getsaleItem.getACCTASSGT());
                                                    detail.setISDODT(getsaleItem.getOVERDLVT().toString());
                                                    detail.setISDUDT(getsaleItem.getUNDERDLV().toString());
                                                    detail.setISDRQDT(getsaleItem.getREQDATEI());
                                                    detail.setISDDLS(getsaleItem.getDELIVSTAT());
                                                    detail.setISDODLS(getsaleItem.getDLVSTATI());
                                                    detail.setISDCRDT(getsaleItem.getCREATEDATE());
                                                    detail.setISDCRTM(getsaleItem.getRECTIME());
                                                    detail.setISDSUSER(getsaleItem.getCREATEDBY());
                                                    detail.setISDDEST(destCC);
                                                    detail.setISDSTS("0");
                                                    detail.setISDREMK("");
                                                    detail.setISDEUSR("");
                                                    detail.setISDPURG(getsaleItem.getPURGROUP().trim());

                                                    //-------------------------- STOCKLIST ---------------------------------------
                                                    ZWSTWCMMRMSTOCKLISTService service2 = new ZWSTWCMMRMSTOCKLISTService();
                                                    ZWSTWCMMRMSTOCKLIST getstocklist = service2.getZWSTWCMMRMSTOCKLISTSoapBinding();

                                                    TABLEOFZBAPIMATNR tablematnum = new TABLEOFZBAPIMATNR();
                                                    TABLEOFZBAPIPLANT tableplantSL = new TABLEOFZBAPIPLANT();
                                                    TABLEOFZBAPILGORT tablestgeloc = new TABLEOFZBAPILGORT();
                                                    TABLEOFZBAPICHARG tablebatch = new TABLEOFZBAPICHARG();
                                                    TABLEOFBAPIRET2 _returnSL = new TABLEOFBAPIRET2();
                                                    TABLEOFZBAPISTOCKRM01 stockRMOUT = new TABLEOFZBAPISTOCKRM01();

                                                    ZBAPIMATNR matnum = new ZBAPIMATNR();
                                                    matnum.setSIGN("I");
                                                    matnum.setOPTION("EQ");
                                                    matnum.setMATNRLOW(getsaleItem.getMATERIAL().trim());
                                                    matnum.setMATNRHIGH("");
                                                    tablematnum.getItem().add(matnum);

                                                    ZBAPIPLANT plantSL = new ZBAPIPLANT();
                                                    plantSL.setSIGN("I");
                                                    plantSL.setOPTION("EQ");
                                                    plantSL.setWERKSLOW(getsaleItem.getPLANT());
                                                    plantSL.setWERKSHIGH("");
                                                    tableplantSL.getItem().add(plantSL);

                                                    ZBAPILGORT stgeloc = new ZBAPILGORT();
                                                    stgeloc.setSIGN("I");
                                                    stgeloc.setOPTION("EQ");
                                                    stgeloc.setLGORTLOW(getsaleItem.getSTGELOC().trim());
                                                    stgeloc.setLGORTHIGH("");
                                                    tablestgeloc.getItem().add(stgeloc);

                                                    ZBAPICHARG batch = new ZBAPICHARG();
                                                    batch.setSIGN("");
                                                    batch.setOPTION("");
                                                    batch.setCHARGLOW("");
                                                    batch.setCHARGHIGH("");
//            tablebatch.getItem().add(batch);

                                                    Future<?> getSLAsync = getstocklist.zBAPITWCMMRMSTOCKLISTAsync(tablebatch, tablematnum, tableplantSL, _returnSL, tablestgeloc, stockRMOUT);
                                                    ZBAPITWCMMRMSTOCKLISTResponse getSLAsyncResponse = null;
                                                    getSLAsyncResponse = (ZBAPITWCMMRMSTOCKLISTResponse) getSLAsync.get();

                                                    TABLEOFBAPIRET2 resReturn = getSLAsyncResponse.getRETURN();
                                                    TABLEOFZBAPISTOCKRM01 stockrm01 = getSLAsyncResponse.getSTOCKRMOUT();

                                                    for (BAPIRET2 ret : resReturn.getItem()) {
                                                        System.out.println("STOCKLIST_FIELD : " + ret.getFIELD());
                                                        System.out.println("ID : " + ret.getID());
                                                        System.out.println("LOGNO : " + ret.getLOGNO());
                                                        System.out.println("MESSAGE : " + ret.getMESSAGE());
                                                        System.out.println("MESSAGEV1 : " + ret.getMESSAGEV1());
                                                        System.out.println("MESSAGEV2 : " + ret.getMESSAGEV2());
                                                        System.out.println("MESSAGEV3 : " + ret.getMESSAGEV3());
                                                        System.out.println("MESSAGEV4 : " + ret.getMESSAGEV4());
                                                        System.out.println("NUMBER : " + ret.getNUMBER());
                                                        System.out.println("PARAMETER : " + ret.getPARAMETER());
                                                        System.out.println("SYSTEM : " + ret.getSYSTEM());
                                                        System.out.println("TYPE : " + ret.getTYPE());
                                                    }

                                                    for (ZBAPISTOCKRM01 sl01 : stockrm01.getItem()) {

                                                        ISMSDWS stocklist = new ISMSDWS();
                                                        stocklist.setISSCONO("TWC");
                                                        stocklist.setISSMTNO(sl01.getMATERIAL()); //Material Number
                                                        stocklist.setISSPLANT(sl01.getPLANT()); //Plant
                                                        stocklist.setISSSTORE(sl01.getLGORT()); //Storage Location 
                                                        stocklist.setISSBTNO(sl01.getVALTYPE()); //Batch Number --
                                                        stocklist.setISSMTGP(sl01.getMATGRP()); //Material Group
                                                        stocklist.setISSSBIN(sl01.getMATCTRL()); //Storage Bin
                                                        stocklist.setISSMDES(sl01.getMATCTRLNAME()); //Name
                                                        stocklist.setISSBUOM(sl01.getBOMUNIT()); //Base Unit of Measure
                                                        stocklist.setISSUNR(sl01.getUNRQTY().toString()); //Valuated Unrestricted-Use Stock
                                                        stocklist.setISSQI(sl01.getQIQTY().toString()); //Stock in Quality Inspection
                                                        stocklist.setISSBLOCK(sl01.getBLOCKQTY().toString()); //Blocked Stock
                                                        stocklist.setISSTOTQ(sl01.getTOTALQTY().toString()); //Total Valuated Stock
                                                        stocklist.setISSMAVG(sl01.getMOVAVG().toString()); //Moving Average Price/Periodic Unit Price
                                                        stocklist.setISSTOTA(sl01.getTOTALAMT().toString()); //Value of Total Valuated Stock
                                                        stocklist.setISSEDT("");
                                                        stocklist.setISSETM("");
                                                        stocklist.setISSEUSR("");
                                                        stocklist.setISSCDT("");
                                                        stocklist.setISSCTM("");
                                                        stocklist.setISSUSER("");
                                                        sldetail.add(stocklist);

                                                        if (sl01.getVALTYPE().equals("01")) {
                                                            detail.setISDUNR01(sl01.getTOTALQTY().toString());
                                                        } else if (sl01.getVALTYPE().equals("03")) {
                                                            detail.setISDUNR03(sl01.getTOTALQTY().toString());
                                                        }

                                                    }

                                                    //-------------------------- STOCKLIST ---------------------------------------
                                                    invDetailList.add(detail);
                                                }
                                            }
                                        }
                                    }
                                } else {

                                    DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd");
                                    LocalDate datef = null;
                                    LocalDate datet = null;
                                    LocalDate reqdate = null;

                                    for (ZBAPIRMITEM01 getsaleItem : tablegetsale01item.getItem()) {

                                        boolean checkDate = false;

                                        if (!delidatef.equals("") && !delidatet.equals("")) {
                                            datef = LocalDate.parse(delidatef, formatter);
                                            datet = LocalDate.parse(delidatet, formatter);
                                            reqdate = LocalDate.parse(getsaleItem.getREQDATEI());
                                            checkDate = isWithinRange(reqdate, datef, datet);
                                        } else {
                                            if (delidatef.equals("") || delidatet.equals("")) {
                                                checkDate = true;
                                            }
                                        }

                                        permis = checkDate;

                                        if (checkDate) {

                                            ISMSDWD detail = new ISMSDWD();
                                            detail.setISDCONO("TWC");
                                            detail.setISDDWQNO("1");
                                            detail.setISDORD(getsaleItem.getDOCNUMBER().trim());
                                            detail.setISDSEQ("1");
                                            detail.setISDLINO(getsaleItem.getITMNUMBER().trim());
//                    detail.setISDSINO("0");
                                            detail.setISDITNO(getsaleItem.getMATERIAL().trim());
                                            detail.setISDBAT(getsaleItem.getBATCH());
                                            detail.setISDMGRP(getsaleItem.getMATLGROUP());
                                            detail.setISDSTEXT(getsaleItem.getSHORTTEXT());
                                            detail.setISDITCAT(getsaleItem.getITEMCATEG());
                                            detail.setISDRSRJ(getsaleItem.getREAFORRE());
                                            detail.setISDRSRJT(getsaleItem.getREAFORRETXT());
                                            detail.setISDPLANT(getsaleItem.getPLANT());
                                            detail.setISDVT(getsaleItem.getVALTYPE());
                                            detail.setISDSTRG(getsaleItem.getSTGELOC());
                                            detail.setISDRQQTY(getsaleItem.getREQQTY().toString());
                                            detail.setISDDLQTY(getsaleItem.getDLVQTY().toString());
                                            detail.setISDUNIT(getsaleItem.getSALESUNIT());
                                            detail.setISDBUNIT(getsaleItem.getBASEUOM());
                                            detail.setISDCON1(getsaleItem.getSALESQTY1().toString());
                                            detail.setISDCON2(getsaleItem.getSALESQTY2().toString());
                                            detail.setISDPRICE(getsaleItem.getNETPRICE().toString());
                                            detail.setISDNETV(getsaleItem.getNETVALUE().toString());
                                            detail.setISDCUR(getsaleItem.getCURRENCY());
                                            detail.setISDEXC(getsaleItem.getEXCHRATE().toString());
                                            detail.setISDMTCTRL(getsaleItem.getMATCONTROL());
                                            detail.setISDPFCT(getsaleItem.getPROFITCTR());
                                            detail.setISDACGP(getsaleItem.getACCTASSGT());
                                            detail.setISDODT(getsaleItem.getOVERDLVT().toString());
                                            detail.setISDUDT(getsaleItem.getUNDERDLV().toString());
                                            detail.setISDRQDT(getsaleItem.getREQDATEI());
                                            detail.setISDDLS(getsaleItem.getDELIVSTAT());
                                            detail.setISDODLS(getsaleItem.getDLVSTATI());
                                            detail.setISDCRDT(getsaleItem.getCREATEDATE());
                                            detail.setISDCRTM(getsaleItem.getRECTIME());
                                            detail.setISDSUSER(getsaleItem.getCREATEDBY());
                                            detail.setISDDEST(destCC);
                                            detail.setISDSTS("0");
                                            detail.setISDREMK("");
                                            detail.setISDEUSR("");
                                            detail.setISDPURG(getsaleItem.getPURGROUP().trim());

                                            //-------------------------- STOCKLIST ---------------------------------------
                                            ZWSTWCMMRMSTOCKLISTService service2 = new ZWSTWCMMRMSTOCKLISTService();
                                            ZWSTWCMMRMSTOCKLIST getstocklist = service2.getZWSTWCMMRMSTOCKLISTSoapBinding();

                                            TABLEOFZBAPIMATNR tablematnum = new TABLEOFZBAPIMATNR();
                                            TABLEOFZBAPIPLANT tableplantSL = new TABLEOFZBAPIPLANT();
                                            TABLEOFZBAPILGORT tablestgeloc = new TABLEOFZBAPILGORT();
                                            TABLEOFZBAPICHARG tablebatch = new TABLEOFZBAPICHARG();
                                            TABLEOFBAPIRET2 _returnSL = new TABLEOFBAPIRET2();
                                            TABLEOFZBAPISTOCKRM01 stockRMOUT = new TABLEOFZBAPISTOCKRM01();

                                            ZBAPIMATNR matnum = new ZBAPIMATNR();
                                            matnum.setSIGN("I");
                                            matnum.setOPTION("EQ");
                                            matnum.setMATNRLOW(getsaleItem.getMATERIAL().trim());
                                            matnum.setMATNRHIGH("");
                                            tablematnum.getItem().add(matnum);

                                            ZBAPIPLANT plantSL = new ZBAPIPLANT();
                                            plantSL.setSIGN("I");
                                            plantSL.setOPTION("EQ");
                                            plantSL.setWERKSLOW(getsaleItem.getPLANT());
                                            plantSL.setWERKSHIGH("");
                                            tableplantSL.getItem().add(plantSL);

                                            ZBAPILGORT stgeloc = new ZBAPILGORT();
                                            stgeloc.setSIGN("I");
                                            stgeloc.setOPTION("EQ");
                                            stgeloc.setLGORTLOW(getsaleItem.getSTGELOC().trim());
                                            stgeloc.setLGORTHIGH("");
                                            tablestgeloc.getItem().add(stgeloc);

                                            ZBAPICHARG batch = new ZBAPICHARG();
                                            batch.setSIGN("");
                                            batch.setOPTION("");
                                            batch.setCHARGLOW("");
                                            batch.setCHARGHIGH("");
//            tablebatch.getItem().add(batch);

                                            Future<?> getSLAsync = getstocklist.zBAPITWCMMRMSTOCKLISTAsync(tablebatch, tablematnum, tableplantSL, _returnSL, tablestgeloc, stockRMOUT);
                                            ZBAPITWCMMRMSTOCKLISTResponse getSLAsyncResponse = null;
                                            getSLAsyncResponse = (ZBAPITWCMMRMSTOCKLISTResponse) getSLAsync.get();

                                            TABLEOFBAPIRET2 resReturn = getSLAsyncResponse.getRETURN();
                                            TABLEOFZBAPISTOCKRM01 stockrm01 = getSLAsyncResponse.getSTOCKRMOUT();

                                            for (BAPIRET2 ret : resReturn.getItem()) {
                                                System.out.println("STOCKLIST_FIELD : " + ret.getFIELD());
                                                System.out.println("ID : " + ret.getID());
                                                System.out.println("LOGNO : " + ret.getLOGNO());
                                                System.out.println("MESSAGE : " + ret.getMESSAGE());
                                                System.out.println("MESSAGEV1 : " + ret.getMESSAGEV1());
                                                System.out.println("MESSAGEV2 : " + ret.getMESSAGEV2());
                                                System.out.println("MESSAGEV3 : " + ret.getMESSAGEV3());
                                                System.out.println("MESSAGEV4 : " + ret.getMESSAGEV4());
                                                System.out.println("NUMBER : " + ret.getNUMBER());
                                                System.out.println("PARAMETER : " + ret.getPARAMETER());
                                                System.out.println("SYSTEM : " + ret.getSYSTEM());
                                                System.out.println("TYPE : " + ret.getTYPE());
                                            }

                                            for (ZBAPISTOCKRM01 sl01 : stockrm01.getItem()) {

                                                ISMSDWS stocklist = new ISMSDWS();
                                                stocklist.setISSCONO("TWC");
                                                stocklist.setISSMTNO(sl01.getMATERIAL()); //Material Number
                                                stocklist.setISSPLANT(sl01.getPLANT()); //Plant
                                                stocklist.setISSSTORE(sl01.getLGORT()); //Storage Location 
                                                stocklist.setISSBTNO(sl01.getVALTYPE()); //Batch Number --
                                                stocklist.setISSMTGP(sl01.getMATGRP()); //Material Group
                                                stocklist.setISSSBIN(sl01.getMATCTRL()); //Storage Bin
                                                stocklist.setISSMDES(sl01.getMATCTRLNAME()); //Name
                                                stocklist.setISSBUOM(sl01.getBOMUNIT()); //Base Unit of Measure
                                                stocklist.setISSUNR(sl01.getUNRQTY().toString()); //Valuated Unrestricted-Use Stock
                                                stocklist.setISSQI(sl01.getQIQTY().toString()); //Stock in Quality Inspection
                                                stocklist.setISSBLOCK(sl01.getBLOCKQTY().toString()); //Blocked Stock
                                                stocklist.setISSTOTQ(sl01.getTOTALQTY().toString()); //Total Valuated Stock
                                                stocklist.setISSMAVG(sl01.getMOVAVG().toString()); //Moving Average Price/Periodic Unit Price
                                                stocklist.setISSTOTA(sl01.getTOTALAMT().toString()); //Value of Total Valuated Stock
                                                stocklist.setISSEDT("");
                                                stocklist.setISSETM("");
                                                stocklist.setISSEUSR("");
                                                stocklist.setISSCDT("");
                                                stocklist.setISSCTM("");
                                                stocklist.setISSUSER("");
                                                sldetail.add(stocklist);

                                                if (sl01.getVALTYPE().equals("01")) {
                                                    detail.setISDUNR01(sl01.getTOTALQTY().toString());
                                                } else if (sl01.getVALTYPE().equals("03")) {
                                                    detail.setISDUNR03(sl01.getTOTALQTY().toString());
                                                }

                                            }

                                            //-------------------------- STOCKLIST ---------------------------------------
                                            invDetailList.add(detail);
                                        }
                                    }
                                }

                                boolean stsMas = false;
                                boolean delSL = false;

                                if (!Headlist.isEmpty()) { //get Head Data From Web Service

                                    for (int i = 0; i < Headlist.size(); i++) { //Many Order In Head

                                        JSONObject resCKHeadDwn = new ISM800Dao().CKDWHbyORD(Headlist.get(i).getISHORD()); //Check Head Data in Dwn DB

//                                    System.out.println("resCKHeadDwn " + resCKHeadDwn);
                                        if (resCKHeadDwn.optBoolean("DTDwnHead")) { //Found this order no in Dwn DB
                                            System.out.println("Found Head in DB 1");
                                            //get Seq
                                            String seq_current = new ISM800Dao().SelectSEQbyORDERNo(Headlist.get(i).getISHORD());
                                            //get Status
                                            String stsck = new ISM800Dao().selectSTSMasHead(Headlist.get(i).getISHORD(), seq_current);

                                            int LineDet = 0; //for Count Line

                                            if (!invDetailList.isEmpty()) { //get Detail Data From Web Service

                                                String max_seq = new ISM800Dao().SelectMaxSEQbyORDERNo(Headlist.get(i).getISHORD());

                                                for (int j = 0; j < invDetailList.size(); j++) {
                                                    if (Headlist.get(i).getISHORD().equals(invDetailList.get(j).getISDORD())) {

                                                        boolean CkbyORDnLINE = new ISM800Dao().CheckDWDbyORDnLine(invDetailList.get(j).getISDORD(), invDetailList.get(j).getISDLINO());

                                                        if (CkbyORDnLINE) { // Not Found this Line

                                                            if (!stsck.equals("0")) {

                                                                boolean insertDTDwn = new ISM800Dao().insertDetailLineNotFound(maxdwnno, "1", invDetailList.get(j), user, proCurList, procurf, procurt);
                                                                if (insertDTDwn) {
                                                                    LineDet++;
                                                                }

                                                            } else { //insert

                                                                boolean insertDTDwn = new ISM800Dao().insertDetailLineNotFound(dwnno, "1", invDetailList.get(j), user, proCurList, procurf, procurt);
                                                                if (insertDTDwn) {
                                                                    LineDet++;
                                                                }
                                                            }
                                                        }

                                                    }
                                                }

                                                if (new ISM800Dao().CKDWHbyORD(Headlist.get(i).getISHORD()).optBoolean("DTDwnHead")) { // Found Head
                                                    if (LineDet != 0) {
                                                        if (!stsck.equals("0")) {
                                                            boolean insertHead = new ISM800Dao().add(maxdwnno, Headlist.get(i), user, "1", LineDet, delidatef, delidatet); //Download Head
                                                            if (insertHead) {
                                                                listOrdTrue.add(Headlist.get(i).getISHORD());
                                                                System.out.println("insertHead 1 " + insertHead);
                                                                JSONObject del = new ISM800Dao().deleteDownloadS(Headlist.get(i).getISHORD()); //Delete StockList
                                                                delSL = del.optBoolean("recDelS"); // Delete StockList Success => true | Failed => False
                                                            } else {
                                                                stsMas = true;
                                                                if (permis) {
                                                                    listOrdFalse.add(Headlist.get(i).getISHORD());
                                                                }
                                                            }
                                                        } else {
                                                            System.out.println("Do Not");
                                                            if (permis) {
                                                                listOrdFalse.add(Headlist.get(i).getISHORD());
                                                            }
//                                                boolean insertHead = new ISM800Dao().add(dwnno, Headlist.get(i), user, "1", LineDet); //Download Head
//                                                if (insertHead) {
//                                                    System.out.println("insertHead 1 " + insertHead);
//                                                    JSONObject del = new ISM800Dao().deleteDownloadS(Headlist.get(i).getISHORD()); //Delete StockList
//                                                    delSL = del.optBoolean("recDelS"); // Delete StockList Success => true | Failed => False
//                                                } else {
//                                                    stsMas = true;
//                                                }
                                                        }
                                                    }
                                                } else { //Not Found Head
                                                    if (LineDet != 0) {
                                                        if (!stsck.equals("0")) {
                                                            boolean insertHead = new ISM800Dao().add(maxdwnno, Headlist.get(i), user, max_seq, LineDet, delidatef, delidatet); //Download Head
                                                            if (insertHead) {
                                                                listOrdTrue.add(Headlist.get(i).getISHORD());
                                                                System.out.println("insertHead 2 " + insertHead);
                                                                JSONObject del = new ISM800Dao().deleteDownloadS(Headlist.get(i).getISHORD()); //Delete StockList
                                                                delSL = del.optBoolean("recDelS"); // Delete StockList Success => true | Failed => False
                                                            } else {
                                                                if (permis) {
                                                                    listOrdFalse.add(Headlist.get(i).getISHORD());
                                                                }
                                                                stsMas = true;
                                                            }
                                                        } else {
                                                            boolean insertHead = new ISM800Dao().add(dwnno, Headlist.get(i), user, max_seq, LineDet, delidatef, delidatet); //Download Head
                                                            if (insertHead) {
                                                                listOrdTrue.add(Headlist.get(i).getISHORD());
                                                                System.out.println("insertHead 2 " + insertHead);
                                                                JSONObject del = new ISM800Dao().deleteDownloadS(Headlist.get(i).getISHORD()); //Delete StockList
                                                                delSL = del.optBoolean("recDelS"); // Delete StockList Success => true | Failed => False
                                                            } else {
                                                                if (permis) {
                                                                    listOrdFalse.add(Headlist.get(i).getISHORD());
                                                                }
                                                                stsMas = true;
                                                            }
                                                        }
                                                    }
                                                }

                                            }

                                            if (permis) {
                                                listOrdFalse.add(Headlist.get(i).getISHORD());
                                            }

                                        } else { //Not Found this order no in Dwn DB

                                            int LineDet = 0;

                                            if (!invDetailList.isEmpty()) {
                                                for (int j = 0; j < invDetailList.size(); j++) {
                                                    if (Headlist.get(i).getISHORD().equals(invDetailList.get(j).getISDORD())) {

                                                        boolean CkbyORDnLINE = new ISM800Dao().CheckDWDbyORDnLine(invDetailList.get(j).getISDORD(), invDetailList.get(j).getISDLINO());

                                                        if (CkbyORDnLINE) { // Not Found this Line

                                                            //condition for ISDRQQTY ,ISDDLQTY
                                                            //if sts (ISDODLS) eq 'C' then [ISDRQQTY = 0]
                                                            //else [ISDRQQTY = ISDRQQTY - ISDDLQTY] if value has - (nagative value) then [ISDRQQTY = 0]
                                                            boolean insertDTDwn = new ISM800Dao().insertDetailLineNotFound(maxdwnno, "1", invDetailList.get(j), user, proCurList, procurf, procurt);
                                                            if (insertDTDwn) {
                                                                LineDet++;
                                                            }

                                                        } else { //Found this Line
                                                            System.out.println("Found This Line Not Insert Detail This Line");
                                                        }

                                                    }
                                                }

                                                if (LineDet != 0) {
                                                    boolean insertHead = new ISM800Dao().add(maxdwnno, Headlist.get(i), user, "1", LineDet, delidatef, delidatet); //Download Head
                                                    if (insertHead) {
                                                        System.out.println("----------------->" + Headlist.get(i).getISHORD());
                                                        listOrdTrue.add(Headlist.get(i).getISHORD());
                                                        JSONObject del = new ISM800Dao().deleteDownloadS(Headlist.get(i).getISHORD()); //Delete StockList
                                                        delSL = del.optBoolean("recDelS"); // Delete StockList Success => true | Failed => False
                                                    } else {
                                                        stsMas = true;
                                                        if (permis) {
                                                            listOrdFalse.add(Headlist.get(i).getISHORD());
                                                        }
                                                    }
                                                }
                                            }

                                        }

                                    }

                                    String text = "";

                                    System.out.println("head size " + Headlist.size());
                                    System.out.println("detail size " + invDetailList.size());

                                    if (invDetailList.isEmpty() || Headlist.isEmpty()) {
                                        stsMas = true;
                                        text = "alertify.error(\"NO DATA.\",0);";
                                    }

                                    if (stsMas) {
                                        if (text.equals("")) {
                                            request.setAttribute("ALRT", "alertify.error(\"Download NOT Complete.\",0);");
                                        } else {
                                            request.setAttribute("ALRT", text);
                                        }
                                    } else {
                                        request.setAttribute("ALRT", "alertify.success(\"Download Complete.\",0);");
                                    }

                                } else {
                                    System.out.println("Error Failed Not Found Head Data From Download");
                                    request.setAttribute("ALRT", "alertify.error(\"Download NOT Complete. (Not Found Data In SAP)\",0);");
                                }

                                if (delSL) { //Delete_Success_StockList
                                    if (!sldetail.isEmpty()) {
                                        System.out.println("Del SL Done.");
                                        boolean slsts = new ISM800Dao().addStockList(sldetail, user); //ISMSDWS
                                        if (slsts) {
                                            System.out.println("Insert Dwn SL Success!");
                                        } else {
                                            System.out.println("Insert Dwn SL Failed!");
                                        }
                                    } else {
                                        System.out.println("Error SL Empty");
                                    }
                                } else {
                                    if (!sldetail.isEmpty()) {
                                        boolean slsts = new ISM800Dao().addStockList(sldetail, user); //ISMSDWS
                                        if (slsts) {
                                            System.out.println("Insert Dwn SL Success!");
                                        } else {
                                            System.out.println("Insert Dwn SL Failed!");
                                        }
                                    } else {
                                        System.out.println("Error SL Empty");
                                    }
                                }
                            }
                        }

                        if (listOrdTrue.isEmpty()) {
                            request.setAttribute("truetotal", "");
                            request.setAttribute("listtrue", "");
                        } else {
                            request.setAttribute("truetotal", "Total " + listOrdTrue.size() + " Entried");
                            request.setAttribute("listtrue", listOrdTrue);
                        }

                        if (listOrdFalse.isEmpty()) {
                            request.setAttribute("falsetotal", "");
                            request.setAttribute("listfalse", "");
                        } else {
                            request.setAttribute("falsetotal", "Total " + listOrdFalse.size() + " Entried");
                            request.setAttribute("listfalse", listOrdFalse);
                        }

//                        request.setAttribute("myModal_alert_list", "$('#myModal_alert_list').modal('show');");
                        //------------------------------- End Order List -----------------------------
                    } else {

                        System.out.println("this Old");

                        boolean permis = false;

                        Authenticator.setDefault(new Authenticator() {
                            @Override
                            protected PasswordAuthentication getPasswordAuthentication() {

                                Calendar c = Calendar.getInstance();
                                int month = c.get(Calendar.MONTH) + 1;

                                String m = Integer.toString(month);
                                if (m.length() < 2) {
                                    m = "0" + m;
                                }

                                String pass = "display" + m;

                                return new PasswordAuthentication("tviewwien", pass.toCharArray());
                            }
                        });

                        String DISTR_CHAN = "20";
                        String DIVISION = "00";
                        String DOC_TYPE = "Z1O2";
                        String SALES_ORG = "1000";

                        ZWSTWCSDRMGETSALES01Service service = new ZWSTWCSDRMGETSALES01Service();
                        ZWSTWCSDRMGETSALES01 getsale01 = service.getZWSTWCSDRMGETSALES01SoapBinding();

                        TABLEOFZBAPIPLANT tableplant = new TABLEOFZBAPIPLANT();
                        TABLEOFZBAPISALESDATE tablesaledate = new TABLEOFZBAPISALESDATE();
                        TABLEOFZBAPISALESNO tablesaleno = new TABLEOFZBAPISALESNO();
                        TABLEOFZBAPICUSTOMER tablesaleto = new TABLEOFZBAPICUSTOMER();
                        TABLEOFBAPIRET2 _return = new TABLEOFBAPIRET2();
                        TABLEOFZBAPIRMHEAD01 tablehead01 = new TABLEOFZBAPIRMHEAD01();
                        TABLEOFZBAPIRMITEM01 tableitem01 = new TABLEOFZBAPIRMITEM01();
                        TABLEOFZBAPICREATEBY tablecreateby = new TABLEOFZBAPICREATEBY();

                        ZBAPIPLANT plant = new ZBAPIPLANT();
                        plant.setSIGN("I");
                        plant.setOPTION("EQ");
                        plant.setWERKSLOW("1000");
                        plant.setWERKSHIGH("");
//                tableplant.getItem().add(plant);

                        ZBAPISALESDATE saledate = new ZBAPISALESDATE();
                        saledate.setSIGN("I");
                        saledate.setOPTION(optionSaleDate); //<--- optionSaleDate
                        saledate.setDATELOW(saledatef); //Format Date yyyy-MM-dd "2021-02-29"
                        saledate.setDATEHIGH(saledatet);

                        ZBAPICREATEBY createby = new ZBAPICREATEBY();
                        createby.setSIGN("I");
                        createby.setOPTION(optionCreateBy);
                        createby.setERNAMLOW(createbf);
                        createby.setERNAMHIGH(createbt);

                        if (!createbf.equals("") || !createbt.equals("")) {
                            tablecreateby.getItem().add(createby);
                        }

                        if (!saledatef.equals("") || !saledatet.equals("")) {
                            tablesaledate.getItem().add(saledate);
                        }

                        if (saleorderf.equals("") && !saleordert.equals("")) {
                            saleorderf = saleordert;
                            optionSaleOrder = "EQ";
                        } else if (!saleorderf.equals("") && saleordert.equals("")) {
                            optionSaleOrder = "EQ";
                        } else if (!saleorderf.equals("") && !saleordert.equals("")) {
                            optionSaleOrder = "BT";
                        }

                        ZBAPISALESNO saleno = new ZBAPISALESNO();
                        saleno.setSIGN("I");
                        saleno.setOPTION(optionSaleOrder);  //<--- optionSaleOrder
                        saleno.setVBELNLOW(saleorderf); //1420054631
                        saleno.setVBELNHIGH(saleordert);

                        if (!saleorderf.equals("") || !saleordert.equals("")) {
                            tablesaleno.getItem().add(saleno);
                        }

                        ZBAPICUSTOMER saleto = new ZBAPICUSTOMER(); //210026
                        saleto.setSIGN("I");
                        saleto.setOPTION("EQ");
                        saleto.setKUNNRLOW("0000" + cusSelect.trim()); //0000210026
                        saleto.setKUNNRHIGH("");

                        if (!cusSelect.equals("")) {
                            tablesaleto.getItem().add(saleto);
                        }

                        Future<?> getsaleAsync = getsale01.zBAPITWCSDRMGETSALES01Async(tablecreateby, DISTR_CHAN, DIVISION, DOC_TYPE, tablehead01, tableitem01, tableplant, _return, tablesaledate, tablesaleno, SALES_ORG, tablesaleto); //Execute --> Can Check Parameter From This Line
                        ZBAPITWCSDRMGETSALES01Response getsale01Response = null;

                        getsale01Response = (ZBAPITWCSDRMGETSALES01Response) getsaleAsync.get();
                        TABLEOFBAPIRET2 sreturn = getsale01Response.getRETURN();

//                    System.out.println("Return ");
                        for (BAPIRET2 ret : sreturn.getItem()) {
                            System.out.println("GETSALE_FIELD : " + ret.getFIELD());
                            System.out.println("ID : " + ret.getID());
                            System.out.println("LOGNO : " + ret.getLOGNO());
                            System.out.println("MESSAGE : " + ret.getMESSAGE());
                            System.out.println("MESSAGEV1 : " + ret.getMESSAGEV1());
                            System.out.println("MESSAGEV2 : " + ret.getMESSAGEV2());
                            System.out.println("MESSAGEV3 : " + ret.getMESSAGEV3());
                            System.out.println("MESSAGEV4 : " + ret.getMESSAGEV4());
                            System.out.println("NUMBER : " + ret.getNUMBER());
                            System.out.println("PARAMETER : " + ret.getPARAMETER());
                            System.out.println("SYSTEM : " + ret.getSYSTEM());
                            System.out.println("TYPE : " + ret.getTYPE());
                        }

                        TABLEOFZBAPIRMHEAD01 tablegetsale01 = getsale01Response.getORDERHEADERSOUT();

//                        System.out.println("Response ORDERHEADERSOUT");
//                        System.out.println("Head getItem " + tablegetsale01.getItem());
                        List<ISMSDWH> Headlist = new ArrayList<ISMSDWH>();

                        String destCC = "";

                        for (ZBAPIRMHEAD01 getsaleHead : tablegetsale01.getItem()) {

                            ISMSDWH head = new ISMSDWH();
                            head.setISHCONO("TWC");
                            head.setISHDWQNO("1"); //(SELECT ISNULL(MAX(EMSDWQNO)+1, 1) FROM EMSDWT)
                            head.setISHORD(getsaleHead.getSALESNO().trim());
                            head.setISHSEQ("1"); //
                            head.setISHTRDT(""); //CURRENT_DATE
                            head.setISHUART(getsaleHead.getDOCTYPE().trim());
                            head.setISHSORG(getsaleHead.getSALESORG().trim());
                            head.setISHTWEG(getsaleHead.getDISTRCHAN().trim());
                            head.setISHDIVI(getsaleHead.getDIVISION().trim());
                            head.setISHSGRP(getsaleHead.getSALESGRP().trim());
                            head.setISHSOFF(getsaleHead.getSALESOFF().trim());
                            head.setISHMVT(getsaleHead.getMMTYPE().trim());
                            head.setISHCUNO(getsaleHead.getSOLDTO().trim());
                            head.setISHCUNM1(getsaleHead.getNAME1().trim());
                            head.setISHCUNM2(getsaleHead.getNAME2().trim());
                            head.setISHCUAD1(getsaleHead.getADDR1().trim());
                            head.setISHCUAD2(getsaleHead.getADDR2().trim());
                            head.setISHBSTKD(getsaleHead.getPONUMBER().trim());

                            String poNumber = getsaleHead.getPONUMBER().trim();

                            //------------------- PO -----------------
                            //1122007293(WO3114#2205/NN/100/2A) -> normal Type
                            String poNumberCle = poNumber.replaceAll("  ", " ").replaceAll(" = ", "=").replaceAll("= ", "=").replaceAll(" =", "=").replaceAll(" \\(", "(").replaceAll(" #", "#").trim();

                            String matlot;
                            String style;
                            String color = "null";
                            String lot;
                            String amt = "null";

                            String firChar = poNumberCle.substring(0, 1);

                            int count = 0;
                            int countfir = 0;
                            int countlas = 0;
                            int countspa = 0;
                            for (int i = 0; i < poNumberCle.length(); i++) {
                                switch (poNumberCle.charAt(i)) {
                                    case '/':
                                        count++;
                                        break;
                                    case '(':
                                        countfir++;
                                        break;
                                    case ')':
                                        countlas++;
                                        break;
                                    case ' ':
                                        countspa++;
                                        break;
                                    default:
                                        break;
                                }
                            }

                            if (isNumeric(firChar)) { //true -> num
//                    //substring
                                if (poNumberCle.contains("(")) {

                                    if (countfir != countlas) {
                                        if (!poNumberCle.substring(poNumberCle.length() - 1).equals(")")) { //last Char is not ')'
                                            poNumberCle = poNumberCle + ")";
                                        }
                                    }

                                    if (poNumberCle.contains("#")) {

                                        matlot = poNumberCle.substring(poNumberCle.indexOf("(") + 1, poNumberCle.lastIndexOf(")"));

                                        if (matlot.contains("/")) {
                                            style = matlot.split("#")[0];
                                            lot = matlot.split("#")[1];

                                            if (lot.contains("/")) {
                                                amt = lot.split("/")[2];
                                                color = lot.split("/")[1];
                                                lot = lot.split("/")[0];
                                            } else if (lot.contains(" ")) {
                                                color = lot.split(" ")[1];

                                                if (color.contains("=")) {
                                                    amt = color.split("=")[1];
                                                    color = color.split("=")[0];
                                                } else {
                                                    if (lot.split(" ").length > 2) {
                                                        amt = lot.split(" ")[2];
                                                    }
                                                }
                                                lot = lot.split(" ")[0];
                                            }
                                        } else { //1122035367(WH6K16X1-X5WH03A 1,850#) <- case found at (27/04/2023) 
                                            style = "";
                                            color = "";
                                            lot = "";
                                            amt = "";
                                        }

                                    } else {

                                        if (poNumberCle.contains("/")) {
                                            matlot = poNumberCle.substring(poNumberCle.indexOf("(") + 1, poNumberCle.lastIndexOf(")"));
                                            style = matlot.split("/")[0];
                                            lot = matlot.split("/")[1];
                                            color = matlot.split("/")[2];
                                            amt = matlot.split("/")[3];
                                        } else { //<- this case found at (27/04/2023)
                                            //เรียงค่าภายใน () มั่ว ไม่สามารถ split ค่าที่ถูกต้องหรือ แยกได้
                                            //1122033326(ME1E84 E4, E5 WR 40)
                                            //1122038624(WH6H06 WHE 2,000 PC)
                                            matlot = poNumberCle.substring(poNumberCle.indexOf("(") + 1, poNumberCle.lastIndexOf(")"));
                                            style = matlot.split(" ")[0];
                                            lot = "";
                                            color = "";
                                            amt = "";
                                        }

                                    }

                                } else { //6122000605,  <- this case
                                    matlot = "";
                                    style = "";
                                    color = "";
                                    lot = "";
                                    amt = "";
                                }
                            } else { //false -> string
                                matlot = "";
                                style = "";
                                color = "";
                                lot = "";
                                amt = "";
                            }

//                System.out.println("i " + c + " poNumberCle " + poNumberCle + " matlot " + matlot + " style " + style + " color " + color + " lot " + lot + " amt " + amt);
                            //------------------- END PO -----------------
                            head.setISHSUBMI(getsaleHead.getSDCOLNUM().trim());
                            head.setISHMATLOT(matlot);
                            head.setISHDDATE(getsaleHead.getDOCDATE().trim());
                            head.setISHDTIME(getsaleHead.getDOCTIME().trim());
                            head.setISHTAXNO("");
                            head.setISHBRANCH01("");
                            head.setISHSTYLE(style);
                            head.setISHCOLOR(color);
                            head.setISHLOT(lot);
                            head.setISHAMTFG(amt);  //Ex. 750)
                            head.setISHSTYLE2("");
                            head.setISHNOR(""); // จำนวนบรรทัดรวม

                            String CusCode;

                            if (cusSelect.equals("210026")) {
                                CusCode = "WLC";
                            } else if (cusSelect.equals("210027")) {
                                CusCode = "PKC";
                            } else if (cusSelect.equals("210284")) {
                                CusCode = "WSC";
                            } else if (cusSelect.equals("210025")) {
                                CusCode = "WKC";
                            } else if (cusSelect.equals("211289")) {
                                CusCode = "MSB";
                            } else {
                                CusCode = "";
                            }

                            destCC = CusCode;

                            head.setISHPOFG(CusCode); //รหัสบริษัทย่อของลูกค้า
                            head.setISHWHNO(""); //รหัสคลังสินค้า
                            head.setISHPGRP(getsaleHead.getBUSGRP().trim());
                            head.setISHLSTS("0");
                            head.setISHHSTS("0");
                            head.setISHDEST(""); //รหัสสถานที่ส่งปลายทาง
                            head.setISHREMK(""); //หมายเหตุ
                            head.setISHCUNO2(getsaleHead.getSHIPTO().trim());
                            head.setISHDLDT(getsaleHead.getREQDATEH());
                            head.setISHDLS(getsaleHead.getDELIVSTAT());
                            head.setISHODLS(getsaleHead.getDLVSTATH());
                            head.setISHCRDT(getsaleHead.getRECDATE());
                            head.setISHCRTM(getsaleHead.getRECTIME());
                            head.setISHSUSER(getsaleHead.getCREATEDBY());
                            head.setISHEUSR("");
                            head.setISHUSER(uid);
                            head.setISHREASON(new String(radio.getBytes("iso-8859-1"), "UTF-8"));
//                    head.setISHDATUM(getsaleHead.getDOCDATE().trim() + " " + getsaleHead.getDOCTIME().trim());
//                    head.setISHGRPNO("");

                            Headlist.add(head);

                        }

                        List<ISMSDWD> invDetailList = new ArrayList<ISMSDWD>();

                        TABLEOFZBAPIRMITEM01 tablegetsale01item = getsale01Response.getORDERITEMSOUT();

                        List<ISMSDWS> sldetail = new ArrayList<ISMSDWS>();

                        if (!listPurG.isEmpty()) {

                            DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd");
                            LocalDate datef = null;
                            LocalDate datet = null;
                            LocalDate reqdate = null;

                            for (ZBAPIRMITEM01 getsaleItem : tablegetsale01item.getItem()) {

                                boolean checkDate = false;

                                if (!delidatef.equals("") && !delidatet.equals("")) {
                                    datef = LocalDate.parse(delidatef, formatter);
                                    datet = LocalDate.parse(delidatet, formatter);
                                    reqdate = LocalDate.parse(getsaleItem.getREQDATEI());
                                    checkDate = isWithinRange(reqdate, datef, datet);
                                } else {
                                    if (delidatef.equals("") || delidatet.equals("")) {
                                        checkDate = true;
                                    }
                                }

                                permis = checkDate;

                                if (checkDate) {

                                    for (int i = 0; i < listPurG.size(); i++) {

                                        if (getsaleItem.getPURGROUP().trim().equals(listPurG.get(i))) {

                                            ISMSDWD detail = new ISMSDWD();
                                            detail.setISDCONO("TWC");
                                            detail.setISDDWQNO("1");
                                            detail.setISDORD(getsaleItem.getDOCNUMBER().trim());
                                            detail.setISDSEQ("1");
                                            detail.setISDLINO(getsaleItem.getITMNUMBER().trim());
//                    detail.setISDSINO("0");
                                            detail.setISDITNO(getsaleItem.getMATERIAL().trim());
                                            detail.setISDBAT(getsaleItem.getBATCH());
                                            detail.setISDMGRP(getsaleItem.getMATLGROUP());
                                            detail.setISDSTEXT(getsaleItem.getSHORTTEXT());
                                            detail.setISDITCAT(getsaleItem.getITEMCATEG());
                                            detail.setISDRSRJ(getsaleItem.getREAFORRE());
                                            detail.setISDRSRJT(getsaleItem.getREAFORRETXT());
                                            detail.setISDPLANT(getsaleItem.getPLANT());
                                            detail.setISDVT(getsaleItem.getVALTYPE());
                                            detail.setISDSTRG(getsaleItem.getSTGELOC());
                                            detail.setISDRQQTY(getsaleItem.getREQQTY().toString());
                                            detail.setISDDLQTY(getsaleItem.getDLVQTY().toString());
                                            detail.setISDUNIT(getsaleItem.getSALESUNIT());
                                            detail.setISDBUNIT(getsaleItem.getBASEUOM());
                                            detail.setISDCON1(getsaleItem.getSALESQTY1().toString());
                                            detail.setISDCON2(getsaleItem.getSALESQTY2().toString());
                                            detail.setISDPRICE(getsaleItem.getNETPRICE().toString());
                                            detail.setISDNETV(getsaleItem.getNETVALUE().toString());
                                            detail.setISDCUR(getsaleItem.getCURRENCY());
                                            detail.setISDEXC(getsaleItem.getEXCHRATE().toString());
                                            detail.setISDMTCTRL(getsaleItem.getMATCONTROL());
                                            detail.setISDPFCT(getsaleItem.getPROFITCTR());
                                            detail.setISDACGP(getsaleItem.getACCTASSGT());
                                            detail.setISDODT(getsaleItem.getOVERDLVT().toString());
                                            detail.setISDUDT(getsaleItem.getUNDERDLV().toString());
                                            detail.setISDRQDT(getsaleItem.getREQDATEI());
                                            detail.setISDDLS(getsaleItem.getDELIVSTAT());
                                            detail.setISDODLS(getsaleItem.getDLVSTATI());
                                            detail.setISDCRDT(getsaleItem.getCREATEDATE());
                                            detail.setISDCRTM(getsaleItem.getRECTIME());
                                            detail.setISDSUSER(getsaleItem.getCREATEDBY());
                                            detail.setISDDEST(destCC);
                                            detail.setISDSTS("0");
                                            detail.setISDREMK("");
                                            detail.setISDEUSR("");
                                            detail.setISDPURG(getsaleItem.getPURGROUP().trim());

                                            //-------------------------- STOCKLIST ---------------------------------------
                                            ZWSTWCMMRMSTOCKLISTService service2 = new ZWSTWCMMRMSTOCKLISTService();
                                            ZWSTWCMMRMSTOCKLIST getstocklist = service2.getZWSTWCMMRMSTOCKLISTSoapBinding();

                                            TABLEOFZBAPIMATNR tablematnum = new TABLEOFZBAPIMATNR();
                                            TABLEOFZBAPIPLANT tableplantSL = new TABLEOFZBAPIPLANT();
                                            TABLEOFZBAPILGORT tablestgeloc = new TABLEOFZBAPILGORT();
                                            TABLEOFZBAPICHARG tablebatch = new TABLEOFZBAPICHARG();
                                            TABLEOFBAPIRET2 _returnSL = new TABLEOFBAPIRET2();
                                            TABLEOFZBAPISTOCKRM01 stockRMOUT = new TABLEOFZBAPISTOCKRM01();

                                            ZBAPIMATNR matnum = new ZBAPIMATNR();
                                            matnum.setSIGN("I");
                                            matnum.setOPTION("EQ");
                                            matnum.setMATNRLOW(getsaleItem.getMATERIAL().trim());
                                            matnum.setMATNRHIGH("");
                                            tablematnum.getItem().add(matnum);

                                            ZBAPIPLANT plantSL = new ZBAPIPLANT();
                                            plantSL.setSIGN("I");
                                            plantSL.setOPTION("EQ");
                                            plantSL.setWERKSLOW(getsaleItem.getPLANT());
                                            plantSL.setWERKSHIGH("");
                                            tableplantSL.getItem().add(plantSL);

                                            ZBAPILGORT stgeloc = new ZBAPILGORT();
                                            stgeloc.setSIGN("I");
                                            stgeloc.setOPTION("EQ");
                                            stgeloc.setLGORTLOW(getsaleItem.getSTGELOC().trim());
                                            stgeloc.setLGORTHIGH("");
                                            tablestgeloc.getItem().add(stgeloc);

                                            ZBAPICHARG batch = new ZBAPICHARG();
                                            batch.setSIGN("");
                                            batch.setOPTION("");
                                            batch.setCHARGLOW("");
                                            batch.setCHARGHIGH("");
//            tablebatch.getItem().add(batch);

                                            Future<?> getSLAsync = getstocklist.zBAPITWCMMRMSTOCKLISTAsync(tablebatch, tablematnum, tableplantSL, _returnSL, tablestgeloc, stockRMOUT);
                                            ZBAPITWCMMRMSTOCKLISTResponse getSLAsyncResponse = null;
                                            getSLAsyncResponse = (ZBAPITWCMMRMSTOCKLISTResponse) getSLAsync.get();

                                            TABLEOFBAPIRET2 resReturn = getSLAsyncResponse.getRETURN();
                                            TABLEOFZBAPISTOCKRM01 stockrm01 = getSLAsyncResponse.getSTOCKRMOUT();

                                            for (BAPIRET2 ret : resReturn.getItem()) {
                                                System.out.println("STOCKLIST_FIELD : " + ret.getFIELD());
                                                System.out.println("ID : " + ret.getID());
                                                System.out.println("LOGNO : " + ret.getLOGNO());
                                                System.out.println("MESSAGE : " + ret.getMESSAGE());
                                                System.out.println("MESSAGEV1 : " + ret.getMESSAGEV1());
                                                System.out.println("MESSAGEV2 : " + ret.getMESSAGEV2());
                                                System.out.println("MESSAGEV3 : " + ret.getMESSAGEV3());
                                                System.out.println("MESSAGEV4 : " + ret.getMESSAGEV4());
                                                System.out.println("NUMBER : " + ret.getNUMBER());
                                                System.out.println("PARAMETER : " + ret.getPARAMETER());
                                                System.out.println("SYSTEM : " + ret.getSYSTEM());
                                                System.out.println("TYPE : " + ret.getTYPE());
                                            }

                                            for (ZBAPISTOCKRM01 sl01 : stockrm01.getItem()) {

                                                ISMSDWS stocklist = new ISMSDWS();
                                                stocklist.setISSCONO("TWC");
                                                stocklist.setISSMTNO(sl01.getMATERIAL()); //Material Number
                                                stocklist.setISSPLANT(sl01.getPLANT()); //Plant
                                                stocklist.setISSSTORE(sl01.getLGORT()); //Storage Location 
                                                stocklist.setISSBTNO(sl01.getVALTYPE()); //Batch Number --
                                                stocklist.setISSMTGP(sl01.getMATGRP()); //Material Group
                                                stocklist.setISSSBIN(sl01.getMATCTRL()); //Storage Bin
                                                stocklist.setISSMDES(sl01.getMATCTRLNAME()); //Name
                                                stocklist.setISSBUOM(sl01.getBOMUNIT()); //Base Unit of Measure
                                                stocklist.setISSUNR(sl01.getUNRQTY().toString()); //Valuated Unrestricted-Use Stock
                                                stocklist.setISSQI(sl01.getQIQTY().toString()); //Stock in Quality Inspection
                                                stocklist.setISSBLOCK(sl01.getBLOCKQTY().toString()); //Blocked Stock
                                                stocklist.setISSTOTQ(sl01.getTOTALQTY().toString()); //Total Valuated Stock
                                                stocklist.setISSMAVG(sl01.getMOVAVG().toString()); //Moving Average Price/Periodic Unit Price
                                                stocklist.setISSTOTA(sl01.getTOTALAMT().toString()); //Value of Total Valuated Stock
                                                stocklist.setISSEDT("");
                                                stocklist.setISSETM("");
                                                stocklist.setISSEUSR("");
                                                stocklist.setISSCDT("");
                                                stocklist.setISSCTM("");
                                                stocklist.setISSUSER("");
                                                sldetail.add(stocklist);

                                                if (sl01.getVALTYPE().equals("01")) {
                                                    detail.setISDUNR01(sl01.getTOTALQTY().toString());
                                                } else if (sl01.getVALTYPE().equals("03")) {
                                                    detail.setISDUNR03(sl01.getTOTALQTY().toString());
                                                }

                                            }

                                            //-------------------------- STOCKLIST ---------------------------------------
                                            invDetailList.add(detail);
                                        }
                                    }
                                }
                            }
                        } else {

                            DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd");
                            LocalDate datef = null;
                            LocalDate datet = null;
                            LocalDate reqdate = null;

                            for (ZBAPIRMITEM01 getsaleItem : tablegetsale01item.getItem()) {

                                boolean checkDate = false;

                                if (!delidatef.equals("") && !delidatet.equals("")) {
                                    datef = LocalDate.parse(delidatef, formatter);
                                    datet = LocalDate.parse(delidatet, formatter);
                                    reqdate = LocalDate.parse(getsaleItem.getREQDATEI());
                                    checkDate = isWithinRange(reqdate, datef, datet);
                                } else {
                                    if (delidatef.equals("") || delidatet.equals("")) {
                                        checkDate = true;
                                    }
                                }

                                permis = checkDate;

                                if (checkDate) {

                                    ISMSDWD detail = new ISMSDWD();
                                    detail.setISDCONO("TWC");
                                    detail.setISDDWQNO("1");
                                    detail.setISDORD(getsaleItem.getDOCNUMBER().trim());
                                    detail.setISDSEQ("1");
                                    detail.setISDLINO(getsaleItem.getITMNUMBER().trim());
//                    detail.setISDSINO("0");
                                    detail.setISDITNO(getsaleItem.getMATERIAL().trim());
                                    detail.setISDBAT(getsaleItem.getBATCH());
                                    detail.setISDMGRP(getsaleItem.getMATLGROUP());
                                    detail.setISDSTEXT(getsaleItem.getSHORTTEXT());
                                    detail.setISDITCAT(getsaleItem.getITEMCATEG());
                                    detail.setISDRSRJ(getsaleItem.getREAFORRE());
                                    detail.setISDRSRJT(getsaleItem.getREAFORRETXT());
                                    detail.setISDPLANT(getsaleItem.getPLANT());
                                    detail.setISDVT(getsaleItem.getVALTYPE());
                                    detail.setISDSTRG(getsaleItem.getSTGELOC());
                                    detail.setISDRQQTY(getsaleItem.getREQQTY().toString());
                                    detail.setISDDLQTY(getsaleItem.getDLVQTY().toString());
                                    detail.setISDUNIT(getsaleItem.getSALESUNIT());
                                    detail.setISDBUNIT(getsaleItem.getBASEUOM());
                                    detail.setISDCON1(getsaleItem.getSALESQTY1().toString());
                                    detail.setISDCON2(getsaleItem.getSALESQTY2().toString());
                                    detail.setISDPRICE(getsaleItem.getNETPRICE().toString());
                                    detail.setISDNETV(getsaleItem.getNETVALUE().toString());
                                    detail.setISDCUR(getsaleItem.getCURRENCY());
                                    detail.setISDEXC(getsaleItem.getEXCHRATE().toString());
                                    detail.setISDMTCTRL(getsaleItem.getMATCONTROL());
                                    detail.setISDPFCT(getsaleItem.getPROFITCTR());
                                    detail.setISDACGP(getsaleItem.getACCTASSGT());
                                    detail.setISDODT(getsaleItem.getOVERDLVT().toString());
                                    detail.setISDUDT(getsaleItem.getUNDERDLV().toString());
                                    detail.setISDRQDT(getsaleItem.getREQDATEI());
                                    detail.setISDDLS(getsaleItem.getDELIVSTAT());
                                    detail.setISDODLS(getsaleItem.getDLVSTATI());
                                    detail.setISDCRDT(getsaleItem.getCREATEDATE());
                                    detail.setISDCRTM(getsaleItem.getRECTIME());
                                    detail.setISDSUSER(getsaleItem.getCREATEDBY());
                                    detail.setISDDEST(destCC);
                                    detail.setISDSTS("0");
                                    detail.setISDREMK("");
                                    detail.setISDEUSR("");
                                    detail.setISDPURG(getsaleItem.getPURGROUP().trim());

                                    //-------------------------- STOCKLIST ---------------------------------------
                                    ZWSTWCMMRMSTOCKLISTService service2 = new ZWSTWCMMRMSTOCKLISTService();
                                    ZWSTWCMMRMSTOCKLIST getstocklist = service2.getZWSTWCMMRMSTOCKLISTSoapBinding();

                                    TABLEOFZBAPIMATNR tablematnum = new TABLEOFZBAPIMATNR();
                                    TABLEOFZBAPIPLANT tableplantSL = new TABLEOFZBAPIPLANT();
                                    TABLEOFZBAPILGORT tablestgeloc = new TABLEOFZBAPILGORT();
                                    TABLEOFZBAPICHARG tablebatch = new TABLEOFZBAPICHARG();
                                    TABLEOFBAPIRET2 _returnSL = new TABLEOFBAPIRET2();
                                    TABLEOFZBAPISTOCKRM01 stockRMOUT = new TABLEOFZBAPISTOCKRM01();

                                    ZBAPIMATNR matnum = new ZBAPIMATNR();
                                    matnum.setSIGN("I");
                                    matnum.setOPTION("EQ");
                                    matnum.setMATNRLOW(getsaleItem.getMATERIAL().trim());
                                    matnum.setMATNRHIGH("");
                                    tablematnum.getItem().add(matnum);

                                    ZBAPIPLANT plantSL = new ZBAPIPLANT();
                                    plantSL.setSIGN("I");
                                    plantSL.setOPTION("EQ");
                                    plantSL.setWERKSLOW(getsaleItem.getPLANT());
                                    plantSL.setWERKSHIGH("");
                                    tableplantSL.getItem().add(plantSL);

                                    ZBAPILGORT stgeloc = new ZBAPILGORT();
                                    stgeloc.setSIGN("I");
                                    stgeloc.setOPTION("EQ");
                                    stgeloc.setLGORTLOW(getsaleItem.getSTGELOC().trim());
                                    stgeloc.setLGORTHIGH("");
                                    tablestgeloc.getItem().add(stgeloc);

                                    ZBAPICHARG batch = new ZBAPICHARG();
                                    batch.setSIGN("");
                                    batch.setOPTION("");
                                    batch.setCHARGLOW("");
                                    batch.setCHARGHIGH("");
//            tablebatch.getItem().add(batch);

                                    Future<?> getSLAsync = getstocklist.zBAPITWCMMRMSTOCKLISTAsync(tablebatch, tablematnum, tableplantSL, _returnSL, tablestgeloc, stockRMOUT);
                                    ZBAPITWCMMRMSTOCKLISTResponse getSLAsyncResponse = null;
                                    getSLAsyncResponse = (ZBAPITWCMMRMSTOCKLISTResponse) getSLAsync.get();

                                    TABLEOFBAPIRET2 resReturn = getSLAsyncResponse.getRETURN();
                                    TABLEOFZBAPISTOCKRM01 stockrm01 = getSLAsyncResponse.getSTOCKRMOUT();

                                    for (BAPIRET2 ret : resReturn.getItem()) {
                                        System.out.println("STOCKLIST_FIELD : " + ret.getFIELD());
                                        System.out.println("ID : " + ret.getID());
                                        System.out.println("LOGNO : " + ret.getLOGNO());
                                        System.out.println("MESSAGE : " + ret.getMESSAGE());
                                        System.out.println("MESSAGEV1 : " + ret.getMESSAGEV1());
                                        System.out.println("MESSAGEV2 : " + ret.getMESSAGEV2());
                                        System.out.println("MESSAGEV3 : " + ret.getMESSAGEV3());
                                        System.out.println("MESSAGEV4 : " + ret.getMESSAGEV4());
                                        System.out.println("NUMBER : " + ret.getNUMBER());
                                        System.out.println("PARAMETER : " + ret.getPARAMETER());
                                        System.out.println("SYSTEM : " + ret.getSYSTEM());
                                        System.out.println("TYPE : " + ret.getTYPE());
                                    }

//                    System.out.println("----------------------");
                                    for (ZBAPISTOCKRM01 sl01 : stockrm01.getItem()) {

                                        ISMSDWS stocklist = new ISMSDWS();
                                        stocklist.setISSCONO("TWC");
                                        stocklist.setISSMTNO(sl01.getMATERIAL()); //Material Number
                                        stocklist.setISSPLANT(sl01.getPLANT()); //Plant
                                        stocklist.setISSSTORE(sl01.getLGORT()); //Storage Location 
                                        stocklist.setISSBTNO(sl01.getVALTYPE()); //Batch Number --
                                        stocklist.setISSMTGP(sl01.getMATGRP()); //Material Group
                                        stocklist.setISSSBIN(sl01.getMATCTRL()); //Storage Bin
                                        stocklist.setISSMDES(sl01.getMATCTRLNAME()); //Name
                                        stocklist.setISSBUOM(sl01.getBOMUNIT()); //Base Unit of Measure
                                        stocklist.setISSUNR(sl01.getUNRQTY().toString()); //Valuated Unrestricted-Use Stock
                                        stocklist.setISSQI(sl01.getQIQTY().toString()); //Stock in Quality Inspection
                                        stocklist.setISSBLOCK(sl01.getBLOCKQTY().toString()); //Blocked Stock
                                        stocklist.setISSTOTQ(sl01.getTOTALQTY().toString()); //Total Valuated Stock
                                        stocklist.setISSMAVG(sl01.getMOVAVG().toString()); //Moving Average Price/Periodic Unit Price
                                        stocklist.setISSTOTA(sl01.getTOTALAMT().toString()); //Value of Total Valuated Stock
                                        stocklist.setISSEDT("");
                                        stocklist.setISSETM("");
                                        stocklist.setISSEUSR("");
                                        stocklist.setISSCDT("");
                                        stocklist.setISSCTM("");
                                        stocklist.setISSUSER("");
                                        sldetail.add(stocklist);

                                        if (sl01.getVALTYPE().equals("01")) {
                                            detail.setISDUNR01(sl01.getTOTALQTY().toString());
                                        } else if (sl01.getVALTYPE().equals("03")) {
                                            detail.setISDUNR03(sl01.getTOTALQTY().toString());
                                        }

                                    }

                                    //-------------------------- STOCKLIST ---------------------------------------
                                    invDetailList.add(detail);
                                }
                            }
                        }

                        System.out.println("invDetailList " + invDetailList.size());
                        System.out.println("head " + Headlist.size());

                        String maxdwnno = new ISM800Dao().SelectMax();

                        boolean stsMas = false;
                        boolean delSL = false;

                        List<String> listOrdTrue = new ArrayList<String>();
                        List<String> listOrdFalse = new ArrayList<String>();

                        if (!Headlist.isEmpty()) { //get Head Data From Web Service

                            for (int i = 0; i < Headlist.size(); i++) { //Many Order In Head

                                String dwnno = new ISM800Dao().SelectNo(Headlist.get(i).getISHORD());

                                JSONObject resCKHeadDwn = new ISM800Dao().CKDWHbyORD(Headlist.get(i).getISHORD()); //Check Head Data in Dwn DB

                                if (resCKHeadDwn.optBoolean("DTDwnHead")) { //Found this order no in Dwn DB
                                    System.out.println("Found Head in DB 2");
                                    //get Seq
                                    String seq_current = new ISM800Dao().SelectSEQbyORDERNo(Headlist.get(i).getISHORD());
                                    //get Status
                                    String stsck = new ISM800Dao().selectSTSMasHead(Headlist.get(i).getISHORD(), seq_current);

                                    int LineDet = 0; //for Count Line

                                    if (!invDetailList.isEmpty()) { //get Detail Data From Web Service

                                        System.out.println("DB in");

                                        String max_seq = new ISM800Dao().SelectMaxSEQbyORDERNo(Headlist.get(i).getISHORD());

                                        for (int j = 0; j < invDetailList.size(); j++) {
                                            if (Headlist.get(i).getISHORD().equals(invDetailList.get(j).getISDORD())) {

                                                boolean CkbyORDnLINE = new ISM800Dao().CheckDWDbyORDnLine(invDetailList.get(j).getISDORD(), invDetailList.get(j).getISDLINO());

                                                if (CkbyORDnLINE) { // Not Found this Line

                                                    if (!stsck.equals("0")) {
                                                        boolean insertDTDwn = new ISM800Dao().insertDetailLineNotFound(maxdwnno, "1", invDetailList.get(j), user, proCurList, procurf, procurt);
                                                        if (insertDTDwn) {
                                                            LineDet++;
                                                        }
                                                    } else { //insert
                                                        boolean insertDTDwn = new ISM800Dao().insertDetailLineNotFound(dwnno, "1", invDetailList.get(j), user, proCurList, procurf, procurt);
                                                        if (insertDTDwn) {
                                                            LineDet++;
                                                        }
                                                    }

                                                }

                                            }
                                        }

                                        if (new ISM800Dao().CKDWHbyORD(Headlist.get(i).getISHORD()).optBoolean("DTDwnHead")) { // Found Head
                                            System.out.println("LineDet " + LineDet);
                                            if (LineDet != 0) {
                                                if (!stsck.equals("0")) {
                                                    boolean insertHead = new ISM800Dao().add(maxdwnno, Headlist.get(i), user, "1", LineDet, delidatef, delidatet); //Download Head
                                                    if (insertHead) {
                                                        listOrdTrue.add(Headlist.get(i).getISHORD());
                                                        System.out.println("insertHead 1 " + insertHead);
                                                        JSONObject del = new ISM800Dao().deleteDownloadS(Headlist.get(i).getISHORD()); //Delete StockList
                                                        delSL = del.optBoolean("recDelS"); // Delete StockList Success => true | Failed => False
                                                    } else {
                                                        if (permis) {
                                                            listOrdFalse.add(Headlist.get(i).getISHORD());
                                                        }
                                                        stsMas = true;
                                                    }
                                                } else {
                                                    System.out.println("Do Not");
                                                    if (permis) {
                                                        listOrdFalse.add(Headlist.get(i).getISHORD());
                                                    }
//                                                boolean insertHead = new ISM800Dao().add(dwnno, Headlist.get(i), user, "1", LineDet); //Download Head
//                                                if (insertHead) {
//                                                    System.out.println("insertHead 1 " + insertHead);
//                                                    JSONObject del = new ISM800Dao().deleteDownloadS(Headlist.get(i).getISHORD()); //Delete StockList
//                                                    delSL = del.optBoolean("recDelS"); // Delete StockList Success => true | Failed => False
//                                                } else {
//                                                    stsMas = true;
//                                                }
                                                }
                                            }
                                        } else { //Not Found Head
                                            if (LineDet != 0) {
                                                if (!stsck.equals("0")) {
                                                    boolean insertHead = new ISM800Dao().add(maxdwnno, Headlist.get(i), user, max_seq, LineDet, delidatef, delidatet); //Download Head
                                                    if (insertHead) {
                                                        listOrdTrue.add(Headlist.get(i).getISHORD());
                                                        System.out.println("insertHead 2 " + insertHead);
                                                        JSONObject del = new ISM800Dao().deleteDownloadS(Headlist.get(i).getISHORD()); //Delete StockList
                                                        delSL = del.optBoolean("recDelS"); // Delete StockList Success => true | Failed => False
                                                    } else {
                                                        if (permis) {
                                                            listOrdFalse.add(Headlist.get(i).getISHORD());
                                                        }
                                                        stsMas = true;
                                                    }
                                                } else {
                                                    boolean insertHead = new ISM800Dao().add(dwnno, Headlist.get(i), user, max_seq, LineDet, delidatef, delidatet); //Download Head
                                                    if (insertHead) {
                                                        listOrdTrue.add(Headlist.get(i).getISHORD());
                                                        System.out.println("insertHead 2 " + insertHead);
                                                        JSONObject del = new ISM800Dao().deleteDownloadS(Headlist.get(i).getISHORD()); //Delete StockList
                                                        delSL = del.optBoolean("recDelS"); // Delete StockList Success => true | Failed => False
                                                    } else {
                                                        if (permis) {
                                                            listOrdFalse.add(Headlist.get(i).getISHORD());
                                                        }
                                                        stsMas = true;
                                                    }
                                                }
                                            }
                                        }

                                    }

                                    if (permis) {
                                        listOrdFalse.add(Headlist.get(i).getISHORD());
                                    }

                                } else { //Not Found this order no in Dwn DB

                                    int LineDet = 0;

                                    if (!invDetailList.isEmpty()) {
                                        for (int j = 0; j < invDetailList.size(); j++) {
                                            if (Headlist.get(i).getISHORD().equals(invDetailList.get(j).getISDORD())) {

                                                boolean CkbyORDnLINE = new ISM800Dao().CheckDWDbyORDnLine(invDetailList.get(j).getISDORD(), invDetailList.get(j).getISDLINO());

                                                if (CkbyORDnLINE) { // Not Found this Line

                                                    //condition for ISDRQQTY ,ISDDLQTY
                                                    //if sts (ISDODLS) eq 'C' then [ISDRQQTY = 0]
                                                    //else [ISDRQQTY = ISDRQQTY - ISDDLQTY] if value has - (nagative value) then [ISDRQQTY = 0]
                                                    boolean insertDTDwn = new ISM800Dao().insertDetailLineNotFound(maxdwnno, "1", invDetailList.get(j), user, proCurList, procurf, procurt);
                                                    if (insertDTDwn) {
                                                        LineDet++;
                                                    }

                                                } else { //Found this Line
                                                    System.out.println("Found This Line Not Insert Detail This Line");
                                                }

                                            }
                                        }

                                        if (LineDet != 0) {
                                            boolean insertHead = new ISM800Dao().add(maxdwnno, Headlist.get(i), user, "1", LineDet, delidatef, delidatet); //Download Head
                                            if (insertHead) {
                                                listOrdTrue.add(Headlist.get(i).getISHORD());
                                                JSONObject del = new ISM800Dao().deleteDownloadS(Headlist.get(i).getISHORD()); //Delete StockList
                                                delSL = del.optBoolean("recDelS"); // Delete StockList Success => true | Failed => False
                                            } else {
                                                if (permis) {
                                                    listOrdFalse.add(Headlist.get(i).getISHORD());
                                                }
                                                stsMas = true;
                                            }
                                        }
                                    }

                                }

                            }

                            System.out.println("head size " + Headlist.size());
                            System.out.println("detail size " + invDetailList.size());

                            String text = "";

                            if (invDetailList.isEmpty() || Headlist.isEmpty()) {
                                stsMas = true;
                                text = "alertify.error(\"NO DATA.\",0);";
                            }

                            if (stsMas) {
                                if (text.equals("")) {
                                    request.setAttribute("ALRT", "alertify.error(\"Download NOT Complete.\",0);");
                                } else {
                                    request.setAttribute("ALRT", text);
                                }
                            } else {

                                request.setAttribute("ALRT", "alertify.success(\"Download Complete.\",0);");

                                if (listOrdTrue.isEmpty()) {
                                    request.setAttribute("truetotal", "");
                                    request.setAttribute("listtrue", "");
                                } else {
                                    request.setAttribute("truetotal", "Total " + listOrdTrue.size() + " Entried");
                                    request.setAttribute("listtrue", listOrdTrue);
                                }

                                if (listOrdFalse.isEmpty()) {
                                    request.setAttribute("falsetotal", "");
                                    request.setAttribute("listfalse", "");
                                } else {
                                    request.setAttribute("falsetotal", "Total " + listOrdFalse.size() + " Entried");
                                    request.setAttribute("listfalse", listOrdFalse);
                                }

                                request.setAttribute("myModal_alert_list", "$('#myModal_alert_list').modal('show');");

                            }

                        } else {
                            System.out.println("Error Failed Not Found Head Data From Download");
                            request.setAttribute("ALRT", "alertify.error(\"Download NOT Complete. (Not Found Data In SAP)\",0);");
                        }

                        if (delSL) { //Delete_Success_StockList
                            if (!sldetail.isEmpty()) {
                                System.out.println("Del SL Done.");
                                boolean slsts = new ISM800Dao().addStockList(sldetail, user); //ISMSDWS
                                if (slsts) {
                                    System.out.println("Insert Dwn SL Success!");
                                } else {
                                    System.out.println("Insert Dwn SL Failed!");
                                }
                            } else {
                                System.out.println("Error SL Empty");
                            }
                        } else {
                            if (!sldetail.isEmpty()) {
                                boolean slsts = new ISM800Dao().addStockList(sldetail, user); //ISMSDWS
                                if (slsts) {
                                    System.out.println("Insert Dwn SL Success!");
                                } else {
                                    System.out.println("Insert Dwn SL Failed!");
                                }
                            } else {
                                System.out.println("Error SL Empty");
                            }
                        }
                    }

                    request.setAttribute("PROGRAMNAME", "ISM800/C");
                    request.setAttribute("PROGRAMDESC", "Sales Order Download. Process");
                    RequestDispatcher view = request.getRequestDispatcher(PAGE_VIEW);
                    view.forward(request, response);

                } catch (Exception ex) {

                    System.out.println("Catch : " + ex + " " + ex.getMessage());

                    String headModal = "No Data !";

                    if (ex.toString().contains("ArrayIndexOutOfBounds")) {
                        request.setAttribute("ALRT", "$('#myModal_po_error').modal('show');");
                        headModal = "";
                    } else if (ex.toString().contains("401")) {
                        headModal = "401: Unauthorized";
                    }

                    ex.printStackTrace();

                    if (!headModal.equals("")) {
                        request.setAttribute("headModal", headModal);
                        request.setAttribute("sendMessage", "window.setTimeout(show2, 0);");
                    }

                    RequestDispatcher view = request.getRequestDispatcher(PAGE_VIEW);
                    view.forward(request, response);

                }
            } else { //Not Choose Parameter

                request.setAttribute("sendMessage", "window.setTimeout(show2, 0);");
                request.setAttribute("headModal", "Download Failed !");
                RequestDispatcher view = request.getRequestDispatcher(PAGE_VIEW);
                view.forward(request, response);

            }

        } catch (Exception e) {

            e.printStackTrace();
            request.setAttribute("sendMessage", "window.setTimeout(show2, 0);");
            request.setAttribute("headModal", "Download Failed !");
            request.setAttribute("headModalDet", GG + " : " + e.getMessage());
            RequestDispatcher view = request.getRequestDispatcher(PAGE_VIEW);
            view.forward(request, response);

        }

    }

    boolean isWithinRange(LocalDate testDate, LocalDate startDate, LocalDate endDate) {
        if (testDate != null && startDate != null && endDate != null) {
            return !(testDate.isBefore(startDate) || testDate.isAfter(endDate));
        } else {
            return false;
        }
    }

    private Pattern pattern = Pattern.compile("-?\\d+(\\.\\d+)?");

    public boolean isNumeric(String strNum) {
        if (strNum == null) {
            return false;
        }
        return pattern.matcher(strNum).matches();
    }

}
