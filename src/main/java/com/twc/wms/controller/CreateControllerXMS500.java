/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.controller;

import com.twc.wms.dao.XMSRMRDao;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author wien
 */
public class CreateControllerXMS500 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;

    public CreateControllerXMS500() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");

        String qr = request.getParameter("qr");
        String uid = request.getParameter("userid");

        qr = qr.split("\\|")[0];

        if (uid.trim().equals("")) {
            uid = request.getParameter("uid");
        }

        if (qr.trim().length() == 10) {
            List<String> UAList = new XMSRMRDao().findQRIDByGPQR(qr);
            for (int i = 0; i < UAList.size(); i++) {
                new XMSRMRDao().add(UAList.get(i), uid);
            }

        } else if (qr.trim().length() == 17) {
            new XMSRMRDao().add(qr, uid);

        }

        response.setHeader("Refresh", "0;/TABLE2/XMS500/display?uid=" + uid);

    }
}
