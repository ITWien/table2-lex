/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.controller;

import com.twc.wms.dao.QIDETAILDao;
import com.twc.wms.dao.WHDao;
import com.twc.wms.entity.WH;
import com.twc.wms.dao.QIHEADDao;
import com.twc.wms.dao.QdestDao;
import com.twc.wms.dao.UserDao;
import com.twc.wms.dao.WMS971Dao;
import com.twc.wms.dao.XMSWIHEADDao;
import com.twc.wms.entity.MSSMATN;
import com.twc.wms.entity.QIDETAIL;
import com.twc.wms.entity.Qdest;
import com.twc.wms.entity.UserAuth;
import com.twc.wms.entity.WMS971;
import com.twc.wms.entity.XMSWIHEAD;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.poi.sl.draw.geom.Path;

/**
 *
 * @author wien
 */
public class DisplayControllerWMS971 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static final String PAGE_VIEW = "../views/displayWMS971.jsp";
    private static final String FILE_PATH = "WMS/images/raw-materials/";

    public DisplayControllerWMS971() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        request.setAttribute("PROGRAMNAME", "WMS971");
        String forward = "";
        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "Onhand by Location. Display");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
//    ********
        request.setAttribute("plant", "1000");

        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        request.setAttribute("PROGRAMNAME", "WMS971");
        String forward = "";
        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "Onhand by Location. Display");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
//    ********
        String code = request.getParameter("code");
        String plant = request.getParameter("plant");

        if (code.length() >= 8) {
            if (code.contains("|")) {
                code = code.split("\\|")[3];
//                code = code.substring(0, 8);
            } else {
//                code = code.substring(0, 8);
            }
        }

        request.setAttribute("code", code);
        request.setAttribute("plant", plant);

        List<WMS971> headList = new WMS971Dao().getOnhand(code, plant);
        request.setAttribute("headList", headList);

        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);
    }

}
