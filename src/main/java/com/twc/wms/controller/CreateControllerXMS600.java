/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.controller;

import com.twc.wms.dao.QSELLERDao;
import com.twc.wms.dao.QSETDao;
import com.twc.wms.dao.QdestDao;
import com.twc.wms.dao.QdestXMSDao;
import com.twc.wms.dao.WHDao;
import com.twc.wms.dao.XMSBRDETAILDao;
//import com.twc.wms.dao.XMSBRDETAILDao;
import com.twc.wms.dao.XMSBRHEADDao;
import com.twc.wms.entity.QSELLER;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.twc.wms.entity.QSET;
import com.twc.wms.entity.Qdest;
import com.twc.wms.entity.WH;
import com.twc.wms.entity.XMSBRDETAIL;
//import com.twc.wms.entity.XMSBRDETAIL;
import com.twc.wms.entity.XMSBRHEAD;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpSession;

/**
 *
 * @author wien
 */
public class CreateControllerXMS600 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static final String PAGE_VIEW = "../views/createXMS600.jsp";

    public CreateControllerXMS600() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("PROGRAMNAME", "XMS600/C");
        String forward = "";
        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "Summary Truck by Branch. Display");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        String shipDate = request.getParameter("shipDate");

        String dest = request.getParameter("dest");
        String round = request.getParameter("round");
        String lino = request.getParameter("lino");
        String driver = request.getParameter("driver");
        String follower = request.getParameter("follower");
        if (lino == null) {
            lino = "";
        }
        if (driver == null) {
            driver = "";
        }
        if (follower == null) {
            follower = "";
        }
        Qdest qd = new QdestDao().findByCod(dest);
        request.setAttribute("dest", dest);
        request.setAttribute("destn", qd.getDesc());
        request.setAttribute("round", round == null ? "1" : round);
        request.setAttribute("lino", lino);
        request.setAttribute("driver", driver);
        request.setAttribute("follower", follower);

        List<Qdest> destList = new QdestXMSDao().findByType(shipDate);
        request.setAttribute("destList", destList);

        List<Qdest> linoList = new XMSBRHEADDao().findLino(shipDate);
        request.setAttribute("linoList", linoList);

        List<Qdest> roundList = new XMSBRHEADDao().findRound(shipDate);
        request.setAttribute("roundList", roundList);

        List<Qdest> shipDateList = new XMSBRHEADDao().findShipDate();
        request.setAttribute("shipDateList", shipDateList);

        request.setAttribute("shipDate", shipDate);

        String shipDate2 = shipDate;
        if (shipDate != null) {
            shipDate2 = shipDate.split("-")[2] + "/" + shipDate.split("-")[1] + "/" + shipDate.split("-")[0];
        }
        request.setAttribute("shipDate2", shipDate2);

        request.setAttribute("rowIDX", "0");
        request.setAttribute("added", "display: none;");

        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        request.setAttribute("PROGRAMNAME", "XMS600/C");
        String forward = "";
        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "Summary Truck by Branch. Display");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        String shipDate = request.getParameter("shipDate");
        String dest = request.getParameter("dest");
        String round = request.getParameter("round");
        String lino = request.getParameter("lino");

        List<Qdest> linoList = new XMSBRHEADDao().findLino(shipDate);
        request.setAttribute("linoList", linoList);

        List<Qdest> roundList = new XMSBRHEADDao().findRound(shipDate);
        request.setAttribute("roundList", roundList);

        List<Qdest> shipDateList = new XMSBRHEADDao().findShipDate();
        request.setAttribute("shipDateList", shipDateList);

        Qdest qd = new QdestDao().findByCod(dest);
        request.setAttribute("dest", dest);

        request.setAttribute("round", round);
        request.setAttribute("lino", lino);
        String userid = request.getParameter("userid");

        List<Qdest> destList = new QdestXMSDao().findByType(shipDate);

        request.setAttribute("destList", destList);
        request.setAttribute("shipDate", shipDate);
        String shipDate2 = shipDate;
        if (shipDate != null) {
            shipDate2 = shipDate.split("-")[2] + "/" + shipDate.split("-")[1] + "/" + shipDate.split("-")[0];
        }
        request.setAttribute("shipDate2", shipDate2);

        List<XMSBRDETAIL> detList = new ArrayList<XMSBRDETAIL>();

        if (dest.trim().equals("KPC")) {
            request.setAttribute("destn", "��Թ������ (E01,E02,B20,B21)");
            detList = new XMSBRDETAILDao().findFromOT(lino, shipDate, "E01", "B20", round);
            detList.addAll(new XMSBRDETAILDao().findFromOT(lino, shipDate, "E02", "B21", round));

        } else if (dest.trim().equals("WSC")) {
            request.setAttribute("destn", "��������Ҫ� (E03,B22)");
            detList = new XMSBRDETAILDao().findFromOT(lino, shipDate, "E03", "B22", round);

        } else if (dest.trim().equals("WLC")) {
            request.setAttribute("destn", "�����Ӿٹ (E04,B23)");
            detList = new XMSBRDETAILDao().findFromOT(lino, shipDate, "E04", "B23", round);

        } else if (dest.trim().equals("MWC")) {
            request.setAttribute("destn", "���¹�������� (E05,B24)");
            detList = new XMSBRDETAILDao().findFromOT(lino, shipDate, "E05", "B24", round);

        } else {
            request.setAttribute("destn", qd.getDesc());
            detList = new XMSBRDETAILDao().findFromOT(lino, shipDate, dest, "", round);
        }

        int XMSBRDBAG1sum = 0;
        int XMSBRDROLL1sum = 0;
        int XMSBRDBOX1sum = 0;
        int XMSBRDPCS1sum = 0;
        int XMSBRDTOT1sum = 0;
        int XMSBRDBAG2sum = 0;
        int XMSBRDROLL2sum = 0;
        int XMSBRDBOX2sum = 0;
        int XMSBRDPCS2sum = 0;
        int XMSBRDTOT2sum = 0;
        int XMSBRDBAG3sum = 0;
        int XMSBRDROLL3sum = 0;
        int XMSBRDBOX3sum = 0;
        int XMSBRDPCS3sum = 0;
        int XMSBRDTOT3sum = 0;
        int XMSBRDBAGsum = 0;
        int XMSBRDROLLsum = 0;
        int XMSBRDBOXsum = 0;
        int XMSBRDPCSsum = 0;
        int XMSBRDTOTsum = 0;

        String qno = new XMSBRHEADDao().findMaxQno();

        if (!detList.isEmpty()) {
            request.setAttribute("driver", detList.get(0).getXMSBRDDRIVER());
            request.setAttribute("follower", detList.get(0).getXMSBRDFOLLOWER());

            for (int i = 0; i < detList.size(); i++) {

                XMSBRDETAIL detail = new XMSBRDETAIL();

                detail.setXMSBRDQNO(qno);
                detail.setXMSBRDLINE(Integer.toString(i + 1));
                detail.setXMSBRDDEST(detList.get(i).getXMSBRDDEST());
                detail.setXMSBRDDESTN(detList.get(i).getXMSBRDDESTN().replace("<b>", "").replace("</b>", "").replace("��� ", ""));
                detail.setXMSBRDDRIVER(detList.get(i).getXMSBRDDRIVER());
                detail.setXMSBRDFOLLOWER(detList.get(i).getXMSBRDFOLLOWER());
                detail.setXMSBRDBAG1(detList.get(i).getXMSBRDBAG1().replace("-", "0"));
                detail.setXMSBRDROLL1(detList.get(i).getXMSBRDROLL1().replace("-", "0"));
                detail.setXMSBRDBOX1(detList.get(i).getXMSBRDBOX1().replace("-", "0"));
                detail.setXMSBRDPCS1(detList.get(i).getXMSBRDPCS1().replace("-", "0"));
                detail.setXMSBRDTOT1(detList.get(i).getXMSBRDTOT1().replace("-", "0"));
                detail.setXMSBRDBAG2(detList.get(i).getXMSBRDBAG2().replace("-", "0"));
                detail.setXMSBRDROLL2(detList.get(i).getXMSBRDROLL2().replace("-", "0"));
                detail.setXMSBRDBOX2(detList.get(i).getXMSBRDBOX2().replace("-", "0"));
                detail.setXMSBRDPCS2(detList.get(i).getXMSBRDPCS2().replace("-", "0"));
                detail.setXMSBRDTOT2(detList.get(i).getXMSBRDTOT2().replace("-", "0"));
                detail.setXMSBRDBAG3(detList.get(i).getXMSBRDBAG3().replace("-", "0"));
                detail.setXMSBRDROLL3(detList.get(i).getXMSBRDROLL3().replace("-", "0"));
                detail.setXMSBRDBOX3(detList.get(i).getXMSBRDBOX3().replace("-", "0"));
                detail.setXMSBRDPCS3(detList.get(i).getXMSBRDPCS3().replace("-", "0"));
                detail.setXMSBRDTOT3(detList.get(i).getXMSBRDTOT3().replace("-", "0"));
                detail.setXMSBRDBAG(detList.get(i).getXMSBRDBAG().replace("-", "0"));
                detail.setXMSBRDROLL(detList.get(i).getXMSBRDROLL().replace("-", "0"));
                detail.setXMSBRDBOX(detList.get(i).getXMSBRDBOX().replace("-", "0"));
                detail.setXMSBRDPCS(detList.get(i).getXMSBRDPCS().replace("-", "0"));
                detail.setXMSBRDTOT(detList.get(i).getXMSBRDTOT().replace("-", "0"));
                detail.setXMSBRDUSER(userid);

                new XMSBRDETAILDao().add(detail);

                if (!detList.get(i)
                        .getXMSBRDDESTN().equals("���")) {
                    if (!detList.get(i).getXMSBRDDESTN().equals("��ż�ҹ")) {
                        if (!detList.get(i).getXMSBRDDESTN().equals("����")) {

                            XMSBRDBAG1sum += Integer.parseInt(detList.get(i).getXMSBRDBAG1().trim().equals("-") ? "0" : detList.get(i).getXMSBRDBAG1().replace(",", ""));
                            XMSBRDROLL1sum += Integer.parseInt(detList.get(i).getXMSBRDROLL1().trim().equals("-") ? "0" : detList.get(i).getXMSBRDROLL1().replace(",", ""));
                            XMSBRDBOX1sum += Integer.parseInt(detList.get(i).getXMSBRDBOX1().trim().equals("-") ? "0" : detList.get(i).getXMSBRDBOX1().replace(",", ""));
                            XMSBRDPCS1sum += Integer.parseInt(detList.get(i).getXMSBRDPCS1().trim().equals("-") ? "0" : detList.get(i).getXMSBRDPCS1().replace(",", ""));
                            XMSBRDTOT1sum += Integer.parseInt(detList.get(i).getXMSBRDTOT1().trim().equals("-") ? "0" : detList.get(i).getXMSBRDTOT1().replace(",", ""));
                            XMSBRDBAG2sum += Integer.parseInt(detList.get(i).getXMSBRDBAG2().trim().equals("-") ? "0" : detList.get(i).getXMSBRDBAG2().replace(",", ""));
                            XMSBRDROLL2sum += Integer.parseInt(detList.get(i).getXMSBRDROLL2().trim().equals("-") ? "0" : detList.get(i).getXMSBRDROLL2().replace(",", ""));
                            XMSBRDBOX2sum += Integer.parseInt(detList.get(i).getXMSBRDBOX2().trim().equals("-") ? "0" : detList.get(i).getXMSBRDBOX2().replace(",", ""));
                            XMSBRDPCS2sum += Integer.parseInt(detList.get(i).getXMSBRDPCS2().trim().equals("-") ? "0" : detList.get(i).getXMSBRDPCS2().replace(",", ""));
                            XMSBRDTOT2sum += Integer.parseInt(detList.get(i).getXMSBRDTOT2().trim().equals("-") ? "0" : detList.get(i).getXMSBRDTOT2().replace(",", ""));
                            XMSBRDBAG3sum += Integer.parseInt(detList.get(i).getXMSBRDBAG3().trim().equals("-") ? "0" : detList.get(i).getXMSBRDBAG3().replace(",", ""));
                            XMSBRDROLL3sum += Integer.parseInt(detList.get(i).getXMSBRDROLL3().trim().equals("-") ? "0" : detList.get(i).getXMSBRDROLL3().replace(",", ""));
                            XMSBRDBOX3sum += Integer.parseInt(detList.get(i).getXMSBRDBOX3().trim().equals("-") ? "0" : detList.get(i).getXMSBRDBOX3().replace(",", ""));
                            XMSBRDPCS3sum += Integer.parseInt(detList.get(i).getXMSBRDPCS3().trim().equals("-") ? "0" : detList.get(i).getXMSBRDPCS3().replace(",", ""));
                            XMSBRDTOT3sum += Integer.parseInt(detList.get(i).getXMSBRDTOT3().trim().equals("-") ? "0" : detList.get(i).getXMSBRDTOT3().replace(",", ""));
                            XMSBRDBAGsum += Integer.parseInt(detList.get(i).getXMSBRDBAG().trim().equals("-") ? "0" : detList.get(i).getXMSBRDBAG().replace(",", ""));
                            XMSBRDROLLsum += Integer.parseInt(detList.get(i).getXMSBRDROLL().trim().equals("-") ? "0" : detList.get(i).getXMSBRDROLL().replace(",", ""));
                            XMSBRDBOXsum += Integer.parseInt(detList.get(i).getXMSBRDBOX().trim().equals("-") ? "0" : detList.get(i).getXMSBRDBOX().replace(",", ""));
                            XMSBRDPCSsum += Integer.parseInt(detList.get(i).getXMSBRDPCS().trim().equals("-") ? "0" : detList.get(i).getXMSBRDPCS().replace(",", ""));
                            XMSBRDTOTsum += Integer.parseInt(detList.get(i).getXMSBRDTOT().trim().equals("-") ? "0" : detList.get(i).getXMSBRDTOT().replace(",", ""));

                        }
                    }
                }
            }
        }

        request.setAttribute("XMSBRDBAG1sum", XMSBRDBAG1sum);
        request.setAttribute("XMSBRDROLL1sum", XMSBRDROLL1sum);
        request.setAttribute("XMSBRDBOX1sum", XMSBRDBOX1sum);
        request.setAttribute("XMSBRDPCS1sum", XMSBRDPCS1sum);
        request.setAttribute("XMSBRDTOT1sum", XMSBRDTOT1sum);
        request.setAttribute("XMSBRDBAG2sum", XMSBRDBAG2sum);
        request.setAttribute("XMSBRDROLL2sum", XMSBRDROLL2sum);
        request.setAttribute("XMSBRDBOX2sum", XMSBRDBOX2sum);
        request.setAttribute("XMSBRDPCS2sum", XMSBRDPCS2sum);
        request.setAttribute("XMSBRDTOT2sum", XMSBRDTOT2sum);
        request.setAttribute("XMSBRDBAG3sum", XMSBRDBAG3sum);
        request.setAttribute("XMSBRDROLL3sum", XMSBRDROLL3sum);
        request.setAttribute("XMSBRDBOX3sum", XMSBRDBOX3sum);
        request.setAttribute("XMSBRDPCS3sum", XMSBRDPCS3sum);
        request.setAttribute("XMSBRDTOT3sum", XMSBRDTOT3sum);
        request.setAttribute("XMSBRDBAGsum", XMSBRDBAGsum);
        request.setAttribute("XMSBRDROLLsum", XMSBRDROLLsum);
        request.setAttribute("XMSBRDBOXsum", XMSBRDBOXsum);
        request.setAttribute("XMSBRDPCSsum", XMSBRDPCSsum);
        request.setAttribute("XMSBRDTOTsum", XMSBRDTOTsum);

        boolean added = false;

        if (XMSBRDTOTsum > 0) {

            XMSBRHEAD head = new XMSBRHEAD(qno, shipDate, dest, round, lino, detList.get(0).getXMSBRDDRIVER(), detList.get(0).getXMSBRDFOLLOWER(), Integer.toString(XMSBRDTOT1sum), Integer.toString(XMSBRDTOT2sum), Integer.toString(XMSBRDTOT3sum), Integer.toString(XMSBRDTOTsum), userid);
            if (new XMSBRHEADDao().add(head)) {
                added = true;
                request.setAttribute("qno", qno);

                List<String> qnoList = new XMSBRHEADDao().findDupQno(head, qno);
                for (int i = 0; i < qnoList.size(); i++) {
                    new XMSBRHEADDao().delete(qnoList.get(i));
                    new XMSBRDETAILDao().delete(qnoList.get(i));
                }
            } else {
                new XMSBRDETAILDao().delete(qno);
            }
        }

        request.setAttribute("detList", detList);
        if (added) {
            request.setAttribute("added", "");
        } else {
            request.setAttribute("added", "display: none;");
        }

        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);

    }
}
