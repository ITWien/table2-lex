/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.controller;

import com.twc.wms.dao.PALMOVEDao;
import com.twc.wms.dao.QIDETAILDao;
import com.twc.wms.dao.WHDao;
import com.twc.wms.entity.WH;
import com.twc.wms.dao.QIHEADDao;
import com.twc.wms.dao.QRMMASDao;
import com.twc.wms.dao.QdestDao;
import com.twc.wms.entity.MSSMATN;
import com.twc.wms.entity.PALMOVE;
import com.twc.wms.entity.QIDETAIL;
import com.twc.wms.entity.Qdest;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author wien
 */
public class DeleteControllerWMS120 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static final String PAGE_VIEW = "../views/displayWMS120.jsp";

    public DeleteControllerWMS120() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String no = request.getParameter("no");
        PALMOVEDao daoa = new PALMOVEDao();
        daoa.delete(no);

        response.setHeader("Refresh", "0;/TABLE2/WMS120/display");

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

}
