/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.controller;

import com.twc.wms.dao.MSS210Dao;
import com.twc.wms.dao.XMSWIDETAILDao;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.ResultSet;
import java.util.HashMap;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRResultSetDataSource;
import net.sf.jasperreports.engine.JasperRunManager;

/**
 *
 * @author wien
 */
public class PrintControllerMSS210 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static final String FILE_PATH = "/report/";
    private static final String FILE_DEST = "/report/";
    private static final String FILE_FONT = "/resources/vendors/fonts/";

    public PrintControllerMSS210() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");

        String POFG = request.getParameter("POFG");
        String GRPNO = request.getParameter("GRPNO");
        String dateToReport = request.getParameter("dateToReport");

        try {

            ResultSet result = new MSS210Dao().SQLGetData(POFG, GRPNO);

            ServletOutputStream servletOutputStream = response.getOutputStream();
            File reportFile = new File(getServletConfig().getServletContext()
                    .getRealPath("/resources/jasper/MSS210_Report.jasper"));
            byte[] bytes;

            HashMap<String, Object> map = new HashMap<String, Object>();
            map.put("DATE_", dateToReport);
            map.put("LOGO_WC", getServletConfig().getServletContext()
                    .getRealPath("/resources/images/wc.png"));

            try {
                bytes = JasperRunManager.runReportToPdf(reportFile.getPath(), map, new JRResultSetDataSource(result));
                response.setContentType("application/pdf");
                response.setContentLength(bytes.length);

                servletOutputStream.write(bytes, 0, bytes.length);
                servletOutputStream.flush();
                servletOutputStream.close();

            } catch (JRException e) {
                StringWriter stringWriter = new StringWriter();
                PrintWriter printWriter = new PrintWriter(stringWriter);
                e.printStackTrace(printWriter);
                response.setContentType("text/plain");
                response.getOutputStream().print(stringWriter.toString());

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

}
