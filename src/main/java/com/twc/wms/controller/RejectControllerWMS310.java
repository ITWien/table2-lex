/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.controller;

import com.twc.wms.dao.QIDETAILDao;
import com.twc.wms.entity.QIDETAIL;
import com.twc.wms.dao.QIHEADDao;
import com.twc.wms.dao.QRMMASDao;
import com.twc.wms.entity.QIHEAD;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author wien
 */
public class RejectControllerWMS310 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;

    public RejectControllerWMS310() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String[] id = request.getParameterValues("selectCk");
        String qno = request.getParameter("qnoH");
        String wh = request.getParameter("whH");
        String size = request.getParameter("size");

        String userid = request.getParameter("userid");

        if (id != null) {
            for (int i = 0; i < id.length; i++) {
                if (id[i].trim().split("-")[0].length() < 17) {
                    QIDETAILDao daoq = new QIDETAILDao();
                    List<String> idList = daoq.UngroupQRID(id[i].trim());
                    for (int j = 0; j < idList.size(); j++) {
                        QIDETAILDao dao2 = new QIDETAILDao();
                        if (dao2.reject(idList.get(j).trim(), qno, userid)) {
                            QRMMASDao up = new QRMMASDao();
                            up.UpdateStatus(idList.get(j).split("-")[0], "3");
                        }
                    }
                } else if (id[i].trim().split("-")[0].length() == 17) {
                    QIDETAILDao dao2 = new QIDETAILDao();
                    if (dao2.reject(id[i].trim(), qno, userid)) {
                        QRMMASDao up = new QRMMASDao();
                        up.UpdateStatus(id[i].split("-")[0], "3");
                    }
                }
            }

            QIDETAILDao dao = new QIDETAILDao();
            String[] mnx = dao.getSTS(wh.trim(), qno.trim());

            QIDETAILDao dao2 = new QIDETAILDao();
            String tqty = dao2.getTQY(wh.trim(), qno.trim());

            DecimalFormat formatDou = new DecimalFormat("#,##0.00");
            if (tqty == null) {
                double tot = Double.parseDouble("0.000");
                tqty = formatDou.format(tot);
            } else {
                double tot = Double.parseDouble(tqty);
                tqty = formatDou.format(tot);
            }

            QIHEADDao dao3 = new QIHEADDao();
            dao3.SETSTS(mnx[0], mnx[1], wh.trim(), qno.trim(), tqty.trim().replace(",", ""));
        } else {
            if (size.trim().equals("0")) {
                QIHEADDao dao3 = new QIHEADDao();
                dao3.SETSTS2(wh.trim(), qno.trim());
            }
        }

        response.setHeader("Refresh", "0;/TABLE2/WMS310/edit?qno=" + qno + "&wh=" + wh + "");

    }

}
