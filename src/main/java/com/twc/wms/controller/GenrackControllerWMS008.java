/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.controller;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.twc.wms.dao.QRKDETAILDao;
import com.twc.wms.dao.QRKFEATUREDao;
import com.twc.wms.dao.QRKHEADDao;
import com.twc.wms.entity.QRKDETAIL;
import com.twc.wms.entity.QRKFEATURE;
import com.twc.wms.entity.QRKHEAD;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author wien
 */
public class GenrackControllerWMS008 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static final String PAGE_VIEW = "../views/genrackWMS008.jsp";

    public GenrackControllerWMS008() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("PROGRAMNAME", "WMS008/G");
        String forward = "";
        String wh = request.getParameter("wh");
        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "Generate Rack Matrix. Display");
                request.setAttribute("wh", wh);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        QRKDETAILDao dao = new QRKDETAILDao();
        List<QRKDETAIL> pList = dao.findAll(wh);

        RequestDispatcher view = request.getRequestDispatcher(forward);
        request.setAttribute("QdestList", pList);
        view.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

}
