/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.controller;

import com.twc.wms.dao.QSETDao;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.twc.wms.entity.QSET;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpSession;

/**
 *
 * @author wien
 */
public class EditControllerWMS012 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static final String PAGE_VIEW = "../views/editWMS012.jsp";

    public EditControllerWMS012() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("PROGRAMNAME", "WMS012/E");
        String forward = "";

        String cod = request.getParameter("cod");
        QSETDao dao = new QSETDao();
        QSET se = dao.findByCod(cod);

        QSETDao dao2 = new QSETDao();
        List<QSET> sq = dao2.findAllSeq(cod);

        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "Setting Code. Display");
                request.setAttribute("cod", cod);
                request.setAttribute("total", se.getTotal());
                request.setAttribute("seq", sq.get(0).getSq1());
                sq.remove(0);

                for (int i = 1; i < sq.size(); i++) {
                    String ii = Integer.toString(i);
                    request.setAttribute("seq" + ii, sq.get(i));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        RequestDispatcher view = request.getRequestDispatcher(forward);
        request.setAttribute("sqList", sq);
        view.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");

        request.setAttribute("PROGRAMNAME", "WMS012/E");
        request.setAttribute("PROGRAMDESC", "Setting Code. Display");

        String userid = request.getParameter("userid");

        String cod = request.getParameter("cod");
        String total = request.getParameter("total");
        String seq = request.getParameter("seq");
        String[] allseq = request.getParameterValues("sq");
        List<String> seqs = new ArrayList<String>();

        try {
            for (int i = 0; i < allseq.length; i++) {
                if (!allseq[i].trim().equals("")) {
                    seqs.add(allseq[i]);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        QSET ua = new QSET(cod, total);
        QSETDao dao = new QSETDao();

        String sendMessage = "";
        String forward = "";

        if (dao.edit(ua, userid)) {
            QSETDao dao3 = new QSETDao();
            dao3.deleteSeq(cod);

            QSET ua1 = new QSET(cod, 1, seq);
            QSETDao dao1 = new QSETDao();
            dao1.addSeq(ua1, userid);

            for (int i = 0; i < seqs.size(); i++) {
                QSET ua2 = new QSET(cod, i + 2, seqs.get(i));
                QSETDao dao2 = new QSETDao();
                dao2.addSeq(ua2, userid);
            }

            response.setHeader("Refresh", "0;/TABLE2/WMS012/display");
        } else {
            sendMessage = "<script type=\"text/javascript\">\n"
                    + "            var show2 = function () {\n"
                    + "                $('#myModal2').modal('show');\n"
                    + "            };\n"
                    + "\n"
                    + "            window.setTimeout(show2, 0);\n"
                    + "\n"
                    + "        </script>";
            forward = PAGE_VIEW;
            request.setAttribute("sendMessage", sendMessage);

            RequestDispatcher view = request.getRequestDispatcher(forward);
            view.forward(request, response);
        }
    }
}
