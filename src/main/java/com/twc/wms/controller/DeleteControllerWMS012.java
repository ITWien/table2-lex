/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.controller;

import com.twc.wms.dao.QSETDao;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.twc.wms.entity.QSET;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author wien
 */
public class DeleteControllerWMS012 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static final String PAGE_VIEW = "../views/deleteWMS012.jsp";

    public DeleteControllerWMS012() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("PROGRAMNAME", "WMS012/D");
        String forward = "";

        String cod = request.getParameter("cod");
        QSETDao dao = new QSETDao();
        QSET se = dao.findByCod(cod);

        QSETDao dao2 = new QSETDao();
        List<QSET> sq = dao2.findAllSeq(cod);

        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "Setting Code. Display");
                request.setAttribute("cod", cod);
                request.setAttribute("total", se.getTotal());
                request.setAttribute("seq", sq.get(0).getSq1());
                sq.remove(0);

                for (int i = 1; i < sq.size(); i++) {
                    String ii = Integer.toString(i);
                    request.setAttribute("seq" + ii, sq.get(i));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        RequestDispatcher view = request.getRequestDispatcher(forward);
        request.setAttribute("sqList", sq);
        view.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");

        String cod = request.getParameter("cod");

        QSETDao dao = new QSETDao();
        dao.delete(cod); 
        
        QSETDao dao1 = new QSETDao();
        dao1.deleteSeq(cod);

        response.setHeader("Refresh", "0;/TABLE2/WMS012/display");
    }
}
