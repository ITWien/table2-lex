/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.controller;

import com.twc.wms.dao.QRKDETAILDao;
import com.twc.wms.dao.WarehouseDao;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.twc.wms.entity.QRKDETAIL;
import com.twc.wms.entity.Warehouse;
import java.util.List;
import javax.servlet.http.HttpSession;

/**
 *
 * @author wien
 */
public class CreateGControllerWMS008 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static final String PAGE_VIEW = "../views/createGWMS008.jsp";

    public CreateGControllerWMS008() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("PROGRAMNAME", "WMS008/GC");
        String forward = "";
        String wh = request.getParameter("wh");
        WarehouseDao dao2 = new WarehouseDao();
        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "Rack Matrix. Display");
                request.setAttribute("wh", wh);

                String whname = dao2.findWHname(wh);
                request.setAttribute("whn", whname);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");

        String sendMessage = "";
        String forward = "";

        String wh = request.getParameter("wh");
        String zone = request.getParameter("zone");
        String rkno = request.getParameter("rkno");
        String side = request.getParameter("side");
        String col = request.getParameter("col");
        String row = request.getParameter("row");

        String userid = request.getParameter("userid");

        if (rkno.trim().length() == 1) {
            rkno = "0" + rkno;
        }

        request.setAttribute("PROGRAMNAME", "WMS008/GC");
        request.setAttribute("PROGRAMDESC", "Rack Matrix. Display");

        QRKDETAIL ua = new QRKDETAIL(zone, wh, rkno, side, col, row);
        QRKDETAILDao dao = new QRKDETAILDao();

        String loc = rkno + side + col + row;

        if (dao.check(wh, zone, loc).equals("f")) {
            sendMessage = "<script type=\"text/javascript\">\n"
                    + "            var show = function () {\n"
                    + "                $('#myModal').modal('show');\n"
                    + "            };\n"
                    + "\n"
                    + "            window.setTimeout(show, 0);\n"
                    + "\n"
                    + "        </script>";
            forward = PAGE_VIEW;
            request.setAttribute("sendMessage", sendMessage);

            RequestDispatcher view = request.getRequestDispatcher(forward);
            view.forward(request, response);
        } else {
            if (dao.add(ua, userid)) {
                response.setHeader("Refresh", "0;/TABLE2/WMS008/genrack?wh=" + wh);
            } else {
                sendMessage = "<script type=\"text/javascript\">\n"
                        + "            var show2 = function () {\n"
                        + "                $('#myModal2').modal('show');\n"
                        + "            };\n"
                        + "\n"
                        + "            window.setTimeout(show2, 0);\n"
                        + "\n"
                        + "        </script>";
                forward = PAGE_VIEW;
                request.setAttribute("sendMessage", sendMessage);

                RequestDispatcher view = request.getRequestDispatcher(forward);
                view.forward(request, response);
            }
        }
    }
}
