/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.controller;

import com.twc.wms.dao.MSSMATNDao;
import com.twc.wms.dao.WarehouseDao;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.twc.wms.entity.Warehouse;
import com.twc.wms.entity.MSSMATN;
import java.util.List;

/**
 *
 * @author wien
 */
public class CreateControllerWMS004 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static final String PAGE_VIEW = "../views/createWMS004.jsp";

    public CreateControllerWMS004() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("PROGRAMNAME", "WMS004/C");
        String forward = "";
        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "Material Control Master. Display");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        WarehouseDao dao = new WarehouseDao();
        List<Warehouse> pList = dao.selectWarehouse();

        RequestDispatcher view = request.getRequestDispatcher(forward);
        request.setAttribute("WHList", pList);
        view.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");

        String mat = request.getParameter("materialControl");
        String des = request.getParameter("description");
        String wh = request.getParameter("warehouse");

        request.setAttribute("PROGRAMNAME", "WMS004/C");
        request.setAttribute("PROGRAMDESC", "Material Control Master. Display");

        MSSMATN ua = new MSSMATN(mat, des, wh);
        MSSMATNDao dao = new MSSMATNDao();

        String sendMessage = "";
        String forward = "";

        if (dao.check(mat).equals("f")) {
            sendMessage = "<script type=\"text/javascript\">\n"
                    + "            var show = function () {\n"
                    + "                $('#myModal').modal('show');\n"
                    + "            };\n"
                    + "\n"
                    + "            window.setTimeout(show, 0);\n"
                    + "\n"
                    + "        </script>";
            forward = PAGE_VIEW;
            request.setAttribute("sendMessage", sendMessage);

            RequestDispatcher view = request.getRequestDispatcher(forward);
            view.forward(request, response);
        } else {
            MSSMATNDao daoadd = new MSSMATNDao();
            if (daoadd.add(ua)) {
                response.setHeader("Refresh", "0;/TABLE2/WMS004/display");
            } else {
                sendMessage = "<script type=\"text/javascript\">\n"
                        + "            var show2 = function () {\n"
                        + "                $('#myModal2').modal('show');\n"
                        + "            };\n"
                        + "\n"
                        + "            window.setTimeout(show2, 0);\n"
                        + "\n"
                        + "        </script>";
                forward = PAGE_VIEW;
                request.setAttribute("sendMessage", sendMessage);

                RequestDispatcher view = request.getRequestDispatcher(forward);
                view.forward(request, response);
            }

        }
    }
}
