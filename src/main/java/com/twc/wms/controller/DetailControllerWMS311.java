/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.controller;

import com.twc.wms.dao.QIDETAILDao;
import com.twc.wms.entity.QIDETAIL;
import com.twc.wms.dao.QIHEADDao;
import com.twc.wms.entity.QIHEAD;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author wien
 */
public class DetailControllerWMS311 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static final String PAGE_VIEW = "../views/detailWMS311.jsp";

    public DetailControllerWMS311() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("PROGRAMNAME", "WMS311");
        String forward = "";
        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "RM Issue. Logistics Returned");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
//    ********
        String qno = request.getParameter("qno");
        String wh = request.getParameter("wh");

//        QIDETAILDao dao4 = new QIDETAILDao();
//        String[] mnx = dao4.getSTS(wh.trim(), qno.trim());
//
//        QIDETAILDao dao5 = new QIDETAILDao();
//        String tqty = dao5.getTQY(wh.trim(), qno.trim());
//
//        DecimalFormat formatDou = new DecimalFormat("#,##0.00");
//        if (tqty == null) {
//            double tot = Double.parseDouble("0.000");
//            tqty = formatDou.format(tot);
//        } else {
//            double tot = Double.parseDouble(tqty);
//            tqty = formatDou.format(tot);
//        }
//
//        QIHEADDao dao3 = new QIHEADDao();
//        dao3.SETSTS(mnx[0], mnx[1], wh.trim(), qno.trim(), tqty.trim());
        QIHEADDao dao = new QIHEADDao();
        QIHEAD hd = dao.findByQno(wh, qno);

        String[] part = hd.getMatCtrl().split(" : ");

        request.setAttribute("wh", hd.getWh());
        request.setAttribute("whn", hd.getWhn());
        request.setAttribute("dest", hd.getDest());
        request.setAttribute("pgroup", hd.getProductGroup());
        request.setAttribute("mvt", hd.getMvt());
        request.setAttribute("mvtn", hd.getMvtn());
        request.setAttribute("mc", hd.getMatCtrl());
        request.setAttribute("mc1", part[0]);
        request.setAttribute("transDate", hd.getTransDate());
        request.setAttribute("set", hd.getSet());
        request.setAttribute("setTotal", hd.getSetTotal());
        request.setAttribute("user", hd.getUser());
        request.setAttribute("qno", hd.getQueueNo());
        request.setAttribute("totalQty", hd.getTotalQty());

        QIDETAILDao dao2 = new QIDETAILDao();
        List<QIDETAIL> deList = dao2.findAllWMS311(wh, qno);

        request.setAttribute("deList", deList);

        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

}
