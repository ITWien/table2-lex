/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.controller;

import com.twc.wms.dao.QSELLERDao;
import com.twc.wms.dao.QSETDao;
import com.twc.wms.dao.QdestDao;
import com.twc.wms.dao.WHDao;
import com.twc.wms.dao.XMSOTDETAILDao;
import com.twc.wms.dao.XMSOTHEADDao;
import com.twc.wms.entity.QSELLER;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.twc.wms.entity.QSET;
import com.twc.wms.entity.Qdest;
import com.twc.wms.entity.WH;
import com.twc.wms.entity.XMSOTDETAIL;
import com.twc.wms.entity.XMSOTHEAD;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpSession;

/**
 *
 * @author wien
 */
public class CreateControllerXMS400 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static final String PAGE_VIEW = "../views/createXMS400.jsp";

    public CreateControllerXMS400() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("PROGRAMNAME", "XMS400/C");
        String forward = "";
        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "RM Issue Other. Entry");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        String wh = request.getParameter("wh");
        String shipDate = request.getParameter("shipDate");

        String dest = request.getParameter("dest");
        String round = request.getParameter("round");
        String lino = request.getParameter("lino");
        String driver = request.getParameter("driver");
        String follower = request.getParameter("follower");
        if (lino == null) {
            lino = "";
        }
        if (driver == null) {
            driver = "";
        }
        if (follower == null) {
            follower = "";
        }
        Qdest qd = new QdestDao().findByCod(dest);
        request.setAttribute("dest", dest);
        request.setAttribute("destn", qd.getDesc());
        request.setAttribute("round", round);
        request.setAttribute("lino", new String(lino.getBytes("iso-8859-1"), "UTF-8"));
        request.setAttribute("driver", new String(driver.getBytes("iso-8859-1"), "UTF-8"));
        request.setAttribute("follower", new String(follower.getBytes("iso-8859-1"), "UTF-8"));

        WH uawh = new WHDao().findByUid(wh);
        List<WH> pList2 = new WHDao().findAll();
        List<Qdest> destList = new QdestDao().findByWhs(wh);
        List<QSELLER> sellerList = new QSELLERDao().findByWHS(wh);

        request.setAttribute("MCList", pList2);
        request.setAttribute("destList", destList);
        request.setAttribute("sellerList", sellerList);
        request.setAttribute("wh", wh);
        request.setAttribute("whn", uawh.getName());
        request.setAttribute("shipDate", shipDate);
        request.setAttribute("rowIDX", "0");

        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");

        String wh = request.getParameter("wh");
        String shipDate = request.getParameter("shipDate");
        String dest = request.getParameter("dest");
        String round = request.getParameter("round");
        String lino = request.getParameter("lino");
        String driver = request.getParameter("driver");
        String follower = request.getParameter("follower");
        String type = request.getParameter("type");

        if (round.trim().equals("")) {
            round = "1";
        }

        String inbag = request.getParameter("in-bag");
        String inroll = request.getParameter("in-roll");
        String inbox = request.getParameter("in-box");
        String inpcs = request.getParameter("in-pcs");
        String intotdoc = request.getParameter("in-totdoc");

        String userid = request.getParameter("userid");

        String qno = new XMSOTHEADDao().findMaxQno();

        XMSOTHEAD head = new XMSOTHEAD(qno, wh, shipDate, dest, round, type, lino, driver, follower, inbag, inroll, inbox, inpcs, intotdoc, userid);

        new XMSOTHEADDao().add(head);

        String[] sender = request.getParameterValues("sender");
        String[] dept = request.getParameterValues("dept");
        String[] desc = request.getParameterValues("desc");
        String[] bag = request.getParameterValues("bag");
        String[] roll = request.getParameterValues("roll");
        String[] box = request.getParameterValues("box");
        String[] pcs = request.getParameterValues("pcs");
        String[] tot = request.getParameterValues("tot");
        String[] totdoc = request.getParameterValues("totdoc");
        String[] pono = request.getParameterValues("pono");
        String[] invno = request.getParameterValues("invno");
        String[] delno = request.getParameterValues("delno");
        String[] amt = request.getParameterValues("amt");

        if (sender != null) {
            for (int i = 0; i < sender.length; i++) {

                if (bag[i].trim().equals("")) {
                    bag[i] = "0";
                }
                if (roll[i].trim().equals("")) {
                    roll[i] = "0";
                }
                if (box[i].trim().equals("")) {
                    box[i] = "0";
                }
                if (pcs[i].trim().equals("")) {
                    pcs[i] = "0";
                }
                if (tot[i].trim().equals("")) {
                    tot[i] = "0";
                }
                if (totdoc[i].trim().equals("")) {
                    totdoc[i] = "0";
                }
                if (amt[i].trim().equals("")) {
                    amt[i] = "0";
                }

                XMSOTDETAIL detail = new XMSOTDETAIL(qno, Integer.toString(i + 1), sender[i], dept[i],
                        desc[i], bag[i].replace(",", ""), roll[i].replace(",", ""), box[i].replace(",", ""),
                        pcs[i].replace(",", ""), tot[i].replace(",", ""), totdoc[i].replace(",", ""),
                        pono[i], invno[i], delno[i], amt[i].replace(",", ""), userid, "", "", "");

                new XMSOTDETAILDao().add(detail);
            }
        }

        response.setHeader("Refresh", "0;/TABLE2/XMS400/edit?qno=" + qno);

    }
}
