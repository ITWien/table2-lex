/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.controller;

import com.twc.wms.dao.QIDETAILDao;
import com.twc.wms.entity.QIDETAIL;
import com.twc.wms.dao.QIHEADDao;
import com.twc.wms.dao.QRMMASDao;
import com.twc.wms.entity.QIHEAD;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author wien
 */
public class HoldControllerXMS350 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;

    public HoldControllerXMS350() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");

        String[] id = request.getParameterValues("selectCk");
        String shipDate = request.getParameter("shipDate");
        String wh = request.getParameter("wh");
        String dest = request.getParameter("dest");

        String userid = request.getParameter("userid");

        if (id != null) {
            for (int i = 0; i < id.length; i++) {
                String mvt = new QIHEADDao().findMVT(wh.trim(), id[i].trim().split("-")[1]);
                if (id[i].trim().split("-")[0].length() < 17) {
                    QIDETAILDao daoq = new QIDETAILDao();
                    List<String> idList = daoq.UngroupQRID(id[i].trim());
                    for (int j = 0; j < idList.size(); j++) {
                        QIDETAILDao dao2 = new QIDETAILDao();
                        if (dao2.HoldWMS350(idList.get(j).trim(), userid)) {
                            QRMMASDao up = new QRMMASDao();
                            if (!mvt.trim().equals("601")) {
                                up.UpdateStatus(idList.get(j).split("-")[0], "5");
                            }
                        }

                        QIDETAILDao dao = new QIDETAILDao();
                        String[] mnx = dao.getSTS(wh.trim(), id[i].split("-")[1].trim());

                        QIDETAILDao dao4 = new QIDETAILDao();
                        String tqty = dao4.getTQY(wh.trim(), id[i].split("-")[1].trim());

                        DecimalFormat formatDou = new DecimalFormat("#,##0.00");
                        if (tqty == null) {
                            double tot = Double.parseDouble("0.000");
                            tqty = formatDou.format(tot);
                        } else {
                            double tot = Double.parseDouble(tqty);
                            tqty = formatDou.format(tot);
                        }

                        QIHEADDao dao3 = new QIHEADDao();
                        dao3.SETSTS(mnx[0], mnx[1], wh.trim(), id[i].split("-")[1].trim(), tqty.trim().replace(",", ""));

                    }
                } else if (id[i].trim().split("-")[0].length() == 17) {
                    QIDETAILDao dao2 = new QIDETAILDao();
                    if (dao2.HoldWMS350(id[i].trim(), userid)) {
                        QRMMASDao up = new QRMMASDao();
                        if (!mvt.trim().equals("601")) {
                            up.UpdateStatus(id[i].split("-")[0], "5");
                        }
                    }

                    QIDETAILDao dao = new QIDETAILDao();
                    String[] mnx = dao.getSTS(wh.trim(), id[i].split("-")[1].trim());

                    QIDETAILDao dao4 = new QIDETAILDao();
                    String tqty = dao4.getTQY(wh.trim(), id[i].split("-")[1].trim());

                    DecimalFormat formatDou = new DecimalFormat("#,##0.00");
                    if (tqty == null) {
                        double tot = Double.parseDouble("0.000");
                        tqty = formatDou.format(tot);
                    } else {
                        double tot = Double.parseDouble(tqty);
                        tqty = formatDou.format(tot);
                    }

                    QIHEADDao dao3 = new QIHEADDao();
                    dao3.SETSTS(mnx[0], mnx[1], wh.trim(), id[i].split("-")[1].trim(), tqty.trim().replace(",", ""));

                }
            }
        }

        response.setHeader("Refresh", "0;/TABLE2/XMS350/display?shipDate=" + shipDate + "&wh=" + wh + "&dest=" + dest);

    }

}
