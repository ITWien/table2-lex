/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.controller;

import com.twc.wms.dao.UserDao;
import com.twc.wms.dao.WarehouseDao;
import com.twc.wms.entity.UserAuth;
import com.twc.wms.entity.Warehouse;
import java.io.IOException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author wien
 */
public class LoginControllerWMS009 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static final String PAGE_VIEW = "../views/LoginUserWMS009.jsp";

    public LoginControllerWMS009() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("PROGRAMNAME", "WMS009");
        String forward = "";
        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "User Authorization. Display");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        
        String id = request.getParameter("userid");
        String pass = request.getParameter("password");
        
        request.setAttribute("PROGRAMNAME", "WMS009/C");
        request.setAttribute("PROGRAMDESC", "User Authorization. Display");
        
        UserAuth ua = new UserAuth(id, pass);
        UserDao dao = new UserDao();
        
        String sendMessage = "";
        String forward = "";
        
        if (dao.checkLogin(id, pass)) {
            
            response.setHeader("Refresh", "0;/TABLE2/WMS009/display");
            
        } else {
            sendMessage = "<script type=\"text/javascript\">\n"
                    + "            var show = function () {\n"
                    + "                $('#myModal').modal('show');\n"
                    + "            };\n"
                    + "\n"
                    + "            window.setTimeout(show, 0);\n"
                    + "\n"
                    + "        </script>";
            forward = PAGE_VIEW;
            request.setAttribute("sendMessage", sendMessage);

            RequestDispatcher view = request.getRequestDispatcher(forward);
            view.forward(request, response);
            
        }
    }
}
