/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.controller;

import com.twc.wms.dao.QUSPDGRPDao;
import com.twc.wms.dao.UserDao;
import com.twc.wms.entity.QUSPDGRP;
import com.twc.wms.entity.UserAuth;
import java.io.IOException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author 93176
 */
public class CreateProdGrpControllerWMS009 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static final String PAGE_VIEW = "../views/createProdGrpWMS009.jsp";

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        request.setAttribute("PROGRAMNAME", "WMS009/PGC");
        request.setAttribute("PROGRAMDESC", "User Authorization Connect Prod Grp. Display");
        String id = request.getParameter("id");

        UserDao dao = new UserDao();
        UserAuth ua = dao.findByUid(id);

        request.setAttribute("id", id);
        request.setAttribute("name", ua.getName());

        QUSPDGRPDao dao2 = new QUSPDGRPDao();
        List<QUSPDGRP> pList2 = dao2.findProdGrp();

        RequestDispatcher view = request.getRequestDispatcher(PAGE_VIEW);
        request.setAttribute("MCList", pList2);
        view.forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        request.setCharacterEncoding("utf-8");

        String uid = request.getParameter("userid");
        String QUSPDGRP = request.getParameter("pid");

        String userid = request.getParameter("userid22");

        String[] pts = uid.split(" : ");
        String pt1 = pts[0];
        uid = pt1;

        String[] parts = QUSPDGRP.split(" + ");
        String part1 = parts[0];
        String[] ds = part1.split(" ");
        String ds1 = ds[0];
        QUSPDGRP = ds1;

        request.setAttribute("PROGRAMNAME", "WMS009/PGC");
        request.setAttribute("PROGRAMDESC", "User Authorization Connect Prod Grp. Display");

        request.setAttribute("id", uid);

        QUSPDGRP ua = new QUSPDGRP("", "", uid, QUSPDGRP, "", "", "", userid);
        QUSPDGRPDao dao = new QUSPDGRPDao();

        String sendMessage = "";
        String forward = "";

        if (dao.checkPrdGrp(uid, QUSPDGRP).equals("f")) {
            sendMessage = "<script type=\"text/javascript\">\n"
                    + "            var show = function () {\n"
                    + "                $('#myModal').modal('show');\n"
                    + "            };\n"
                    + "\n"
                    + "            window.setTimeout(show, 0);\n"
                    + "\n"
                    + "        </script>";
            forward = PAGE_VIEW;
            request.setAttribute("sendMessage", sendMessage);
//
            RequestDispatcher view = request.getRequestDispatcher(forward);
            view.forward(request, response);
        } else {
            QUSPDGRPDao daoadd = new QUSPDGRPDao();
            if (daoadd.addProdGrp(ua, userid)) {
                response.setHeader("Refresh", "0;/TABLE2/WMS009/prdgrp?uid=" + uid);
            } else {
                sendMessage = "<script type=\"text/javascript\">\n"
                        + "            var show2 = function () {\n"
                        + "                $('#myModal2').modal('show');\n"
                        + "            };\n"
                        + "\n"
                        + "            window.setTimeout(show2, 0);\n"
                        + "\n"
                        + "        </script>";
                forward = PAGE_VIEW;
                request.setAttribute("sendMessage", sendMessage);
//
                RequestDispatcher view = request.getRequestDispatcher(forward);
                view.forward(request, response);
            }
        }

    }

}
