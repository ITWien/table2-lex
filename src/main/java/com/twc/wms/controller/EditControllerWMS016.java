/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.controller;

import com.twc.wms.dao.QRMMATSTSDao;
import com.twc.wms.dao.QRMSTSDao;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.twc.wms.dao.QdestDao;
import com.twc.wms.entity.QDESTYP;
import com.twc.wms.entity.Qdest;
import java.util.List;
import javax.servlet.http.HttpSession;

/**
 *
 * @author wien
 */
public class EditControllerWMS016 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static final String PAGE_VIEW = "../views/editWMS016.jsp";

    public EditControllerWMS016() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("PROGRAMNAME", "WMS016/E");
        String forward = "";
        String code = request.getParameter("code");
        String desc = request.getParameter("desc");

        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "System Queue Status. Display");
                request.setAttribute("code", code);
                request.setAttribute("desc", new String(desc.getBytes("iso-8859-1"), "UTF-8"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");

        String code = request.getParameter("code");
        String desc = request.getParameter("desc");

        String userid = request.getParameter("userid");

        request.setAttribute("PROGRAMNAME", "WMS016/E");
        request.setAttribute("PROGRAMDESC", "System Queue Status. Display");

        QRMSTSDao dao = new QRMSTSDao();

        dao.edit(code, desc, userid);

        response.setHeader("Refresh", "0;/TABLE2/WMS016/display");
    }

}
