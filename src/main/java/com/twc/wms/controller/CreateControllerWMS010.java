/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.controller;

import com.twc.wms.dao.QdestDao;
import com.twc.wms.entity.QDESTYP;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.twc.wms.entity.Qdest;
import java.util.List;
import javax.servlet.http.HttpSession;

/**
 *
 * @author wien
 */
public class CreateControllerWMS010 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static final String PAGE_VIEW = "../views/createWMS010.jsp";

    public CreateControllerWMS010() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("PROGRAMNAME", "WMS010/C");
        String forward = "";
        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "Destination Master. Display");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        QdestDao dao = new QdestDao();
        QdestDao dao2 = new QdestDao();
        List<Qdest> pList = dao.findAll();
        List<QDESTYP> tList = dao2.Selecttype();

        RequestDispatcher view = request.getRequestDispatcher(forward);
        request.setAttribute("QdestList", pList);
        request.setAttribute("QdesttypeList", tList);
        view.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");

        String code = request.getParameter("destinationCode");
        String desc = request.getParameter("description");
        String name = request.getParameter("name");
        String adrs1 = request.getParameter("address_1");
        String adrs2 = request.getParameter("address_2");
        String adrs3 = request.getParameter("address_3");
        String adrs4 = request.getParameter("address_4");
        String detype = request.getParameter("DESTYPE");

        String userid = request.getParameter("userid");

        request.setAttribute("PROGRAMNAME", "WMS010/C");
        request.setAttribute("PROGRAMDESC", "Destination Master. Display");

        Qdest qd = new Qdest(code, desc, name, adrs1, adrs2, adrs3, adrs4);
        QdestDao dao = new QdestDao();

        String sendMessage = "";
        String forward = "";

        if (dao.check(code).equals("f")) {
            sendMessage = "<script type=\"text/javascript\">\n"
                    + "            var show = function () {\n"
                    + "                $('#myModal').modal('show');\n"
                    + "            };\n"
                    + "\n"
                    + "            window.setTimeout(show, 0);\n"
                    + "\n"
                    + "        </script>";
            forward = PAGE_VIEW;
            request.setAttribute("sendMessage", sendMessage);

            RequestDispatcher view = request.getRequestDispatcher(forward);

            view.forward(request, response);
        } else {
            dao.add(qd, userid, detype);

            response.setHeader("Refresh", "0;/TABLE2/WMS010/display");
        }

    }
}
