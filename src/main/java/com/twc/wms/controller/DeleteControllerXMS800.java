/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.controller;

import com.twc.wms.dao.XMSWIDETAILDao;
import com.twc.wms.dao.XMSWIHEADDao;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author wien
 */
public class DeleteControllerXMS800 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;

    public DeleteControllerXMS800() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String qno = request.getParameter("qno");
        String wh = request.getParameter("wh");
        String shipDate = request.getParameter("shipDate");

        new XMSWIHEADDao().delete(qno);
        new XMSWIDETAILDao().delete(qno);

        response.setHeader("Refresh", "0;/TABLE2/XMS800/display?shipDate=" + shipDate + "&wh=" + wh);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

}
