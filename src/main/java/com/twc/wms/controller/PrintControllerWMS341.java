/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.controller;

import com.twc.wms.dao.QIDETAILDao;
import com.twc.wms.entity.QIDETAIL;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRResultSetDataSource;
import net.sf.jasperreports.engine.JasperRunManager;

/**
 *
 * @author wien
 */
public class PrintControllerWMS341 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static final String FILE_PATH = "/report/";
    private static final String FILE_DEST = "/report/";
    private static final String FILE_FONT = "/resources/vendors/fonts/";

    public PrintControllerWMS341() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");

        String matCtrl = request.getParameter("matCtrl");
        String[] id = request.getParameterValues("selectCk-" + matCtrl);
        String mat = request.getParameter("mat");
        String wh = request.getParameter("wh");
        String dest = request.getParameter("dest");
        String start = request.getParameter("StartFrom-" + matCtrl);

        HttpSession session = request.getSession(true);
        String userid = (String) session.getAttribute("uid");

        List<QIDETAIL> FL = new ArrayList<QIDETAIL>();
        List<String> TMP = new ArrayList<String>();

        if (id != null) {
//            for (int i = 0; i < id.length; i++) {
//                System.out.println(id[i].split("-")[2] + "====" + id[i].split("-")[3]);
//                QIDETAILDao dao2 = new QIDETAILDao();
//                List<QIDETAIL> UAList = dao2.findRUNNO(id[i].trim());
//
//                for (int j = 0; j < UAList.size(); j++) {
//                    if (!TMP.contains(UAList.get(j).getRunno())) {
//                        TMP.add(UAList.get(j).getRunno());
//                        FL.add(UAList.get(j));
//                    }
//                }
//            }

            try {

                QIDETAILDao dao = new QIDETAILDao();
                ResultSet result = dao.findForPrintWMS340(id, start);

                ServletOutputStream servletOutputStream = response.getOutputStream();
                File reportFile = new File(getServletConfig().getServletContext()
                        .getRealPath("/resources/jasper/WMS340.jasper"));
                byte[] bytes;

//        HashMap<String, Object> map = new HashMap<String, Object>();
//        map.put("STARTFROM", 1);
                try {
                    bytes = JasperRunManager.runReportToPdf(reportFile.getPath(), null, new JRResultSetDataSource(result));
                    response.setContentType("application/pdf");
                    response.setContentLength(bytes.length);

                    servletOutputStream.write(bytes, 0, bytes.length);
                    servletOutputStream.flush();
                    servletOutputStream.close();

                } catch (JRException e) {
                    StringWriter stringWriter = new StringWriter();
                    PrintWriter printWriter = new PrintWriter(stringWriter);
                    e.printStackTrace(printWriter);
                    response.setContentType("text/plain");
                    response.getOutputStream().print(stringWriter.toString());

                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

}
