/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.controller;

import com.twc.wms.dao.QIDETAILDao;
import com.twc.wms.dao.WHDao;
import com.twc.wms.entity.WH;
import com.twc.wms.dao.QIHEADDao;
import com.twc.wms.dao.QdestDao;
import com.twc.wms.dao.UserDao;
import com.twc.wms.dao.XMSWIHEADDao;
import com.twc.wms.entity.MSSMATN;
import com.twc.wms.entity.QIDETAIL;
import com.twc.wms.entity.Qdest;
import com.twc.wms.entity.UserAuth;
import com.twc.wms.entity.XMSWIHEAD;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author wien
 */
public class DisplayControllerXMS800 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static final String PAGE_VIEW = "../views/displayXMS800.jsp";

    public DisplayControllerXMS800() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        request.setAttribute("PROGRAMNAME", "XMS800");
        String forward = "";
        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "Summary Truck by Internal(Production). Display");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
//    ********
        String wh = request.getParameter("wh");
        String shipDate = request.getParameter("shipDate");
        String uid = request.getParameter("uid");

        if (wh == null) {
            UserAuth ua = new UserDao().findByUid(uid);
            wh = ua.getWarehouse();
        }

        if (shipDate == null) {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            Date date = new Date();
            shipDate = formatter.format(date);
        }

        List<XMSWIHEAD> HEADList = new XMSWIHEADDao().findAll(wh, shipDate);

        WH uawh = new WHDao().findByUid(wh);
        List<WH> pList2 = new WHDao().findAll();

        request.setAttribute("MCList", pList2);
        request.setAttribute("wh", wh);
        request.setAttribute("whn", uawh.getName());

        request.setAttribute("HEADList", HEADList);
        request.setAttribute("shipDate", shipDate);

        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        request.setAttribute("PROGRAMNAME", "XMS800");
        String forward = "";
        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "Summary Truck by Internal(Production). Display");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
//    ********
        String wh = request.getParameter("wh");
        String shipDate = request.getParameter("shipDate");
        String uid = request.getParameter("uid");

        if (wh == null) {
            UserAuth ua = new UserDao().findByUid(uid);
            wh = ua.getWarehouse();
        }

        if (shipDate == null) {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            Date date = new Date();
            shipDate = formatter.format(date);
        }

        List<XMSWIHEAD> HEADList = new XMSWIHEADDao().findAll(wh, shipDate);

        WH uawh = new WHDao().findByUid(wh);
        List<WH> pList2 = new WHDao().findAll();

        request.setAttribute("MCList", pList2);
        request.setAttribute("wh", wh);
        request.setAttribute("whn", uawh.getName());

        request.setAttribute("HEADList", HEADList);
        request.setAttribute("shipDate", shipDate);

        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);
    }

}
