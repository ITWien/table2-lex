/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.controller;

import com.twc.wms.dao.QUSPDGRPDao;
import com.twc.wms.entity.QUSPDGRP;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author 93176
 */
public class DeleteProdGrpControllerWMS009 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static final String PAGE_VIEW = "../views/deleteProdGrpWMS009.jsp";

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        request.setCharacterEncoding("utf-8");
        request.setAttribute("PROGRAMNAME", "WMS009/CD");
        String forward = "";
        String uid = request.getParameter("uid");
        String pid = request.getParameter("pid");

        QUSPDGRPDao dao = new QUSPDGRPDao();
        QUSPDGRP pList = dao.findProdGrpByUidObj(uid, pid);

        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "User Authorization Connect Prod Grp. Display");
                request.setAttribute("uid", uid);
                request.setAttribute("pid", pid);
                request.setAttribute("pname", pList.getQWHBGRP());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        request.setCharacterEncoding("utf-8");

        String uid = request.getParameter("userid");
        String pid = request.getParameter("pid");

        QUSPDGRPDao dao = new QUSPDGRPDao();

        dao.deleteProdGrp(uid, pid);

        response.setHeader("Refresh", "0;/TABLE2/WMS009/prdgrp?uid=" + uid);

    }

}
