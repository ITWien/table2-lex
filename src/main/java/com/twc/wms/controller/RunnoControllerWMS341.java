/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.controller;

import com.twc.wms.dao.QIDETAILDao;
import com.twc.wms.entity.QIDETAIL;
import com.twc.wms.dao.QIHEADDao;
import com.twc.wms.dao.QNBSERDao;
import com.twc.wms.entity.QIHEAD;
import java.io.IOException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author wien
 */
public class RunnoControllerWMS341 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;

    public RunnoControllerWMS341() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");

        String matCtrl = request.getParameter("matCtrl");
        String[] id = request.getParameterValues("selectCk-" + matCtrl);
        String mat = request.getParameter("mat");
        String wh = request.getParameter("wh");
        String dest = request.getParameter("dest");
        String shdt = request.getParameter("shdt-" + matCtrl);

        String userid = request.getParameter("userid");

        if (id != null) {
            for (int i = 0; i < id.length; i++) {
                QIDETAILDao daons = new QIDETAILDao();
                String[] obs = daons.findNumberSerie(id[i].trim());
                if (obs[2] != null) {
                    int current = Integer.parseInt(obs[2]);
                    current += 1;
                    String runno = Integer.toString(current);

                    QIDETAILDao daoCheck = new QIDETAILDao();
                    String check = daoCheck.check(id[i].trim());

                    if (check == null) {
                        QIDETAILDao dao2 = new QIDETAILDao();
                        if (dao2.runno(id[i].trim(), userid, runno, shdt)) {
                            QNBSERDao daoed = new QNBSERDao();
                            daoed.editCurrent(runno, obs[1], obs[0]);
                        }
                    }
                }
            }
        }

        String[] m1 = matCtrl.split(" ");
        String[] m2 = mat.split(" ");

        String[] destcod = dest.split(" ");
        String[] whcod = wh.split(" ");
        String[] matcod = mat.split(" ");

        if (m1[0].equals(m2[0]) || m2[0].equals("TOTAL")) {
            response.setHeader("Refresh", "0;/TABLE2/WMS341/detailDest?mat=" + matcod[0] + "&wh=" + whcod[0] + "&dest=" + destcod[0]);
        } else {
            response.setHeader("Refresh", "0;/TABLE2/WMS341/detailMat?mat=" + matcod[0] + "&wh=" + whcod[0] + "&dest=" + destcod[0]);
        }
    }

}
