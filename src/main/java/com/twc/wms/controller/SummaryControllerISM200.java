/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.controller;

import com.twc.wms.dao.WHDao;
import com.twc.wms.entity.WH;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author wien
 */
public class SummaryControllerISM200 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static final String PAGE_VIEW = "../views/summaryISM200.jsp";

    public SummaryControllerISM200() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        request.setAttribute("PROGRAMNAME", "ISM200");
        String forward = "";
        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "Sales Order Simulation. Display");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
//    ********
        String dest = request.getParameter("dest");
        request.setAttribute("dest", dest);
        String seq = request.getParameter("sseq");
        request.setAttribute("seq", seq);

        String oANDs = request.getParameter("oANDs");
        request.setAttribute("oANDs", oANDs);

        String wh = request.getParameter("wh");
        WH whn = new WHDao().findByUid(wh);
        request.setAttribute("wh", wh);
        request.setAttribute("whn", wh + " " + whn.getName());

//        String styleLotList = request.getParameter("styleLotList");
//        request.setAttribute("styleLotList", styleLotList);
//
        String jobNo = request.getParameter("jobNo");
        request.setAttribute("jobNo", jobNo);

        String goBack = request.getParameter("goBack");
        if (goBack == null) {
            goBack = "prepare?dest=" + dest + oANDs;
        } else {
            goBack = "display?wh=" + wh;
        }
        request.setAttribute("goBack", goBack);

        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

}
