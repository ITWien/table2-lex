/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.controller;

import com.twc.wms.dao.QSELLERDao;
import com.twc.wms.dao.QSETDao;
import com.twc.wms.dao.QdestDao;
import com.twc.wms.dao.WHDao;
import com.twc.wms.dao.XMSOTDETAILDao;
import com.twc.wms.dao.XMSOTHEADDao;
import com.twc.wms.entity.QSELLER;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.twc.wms.entity.QSET;
import com.twc.wms.entity.Qdest;
import com.twc.wms.entity.WH;
import com.twc.wms.entity.XMSOTDETAIL;
import com.twc.wms.entity.XMSOTHEAD;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpSession;

/**
 *
 * @author wien
 */
public class PushMATControllerXMS400 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static final String PAGE_VIEW = "../views/createMATXMS400.jsp";

    public PushMATControllerXMS400() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("PROGRAMNAME", "XMS400/C");
        String forward = "";
        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "RM Issue Other. Entry");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        String wh = request.getParameter("wh");
        String shipDate = request.getParameter("shipDate");

        WH uawh = new WHDao().findByUid(wh);
        List<WH> pList2 = new WHDao().findAll();
        List<Qdest> destList = new QdestDao().findByWhs(wh);
        List<QSELLER> sellerList = new QSELLERDao().findByWHS(wh);

        request.setAttribute("MCList", pList2);
        request.setAttribute("destList", destList);
        request.setAttribute("sellerList", sellerList);
        request.setAttribute("wh", wh);
        request.setAttribute("whn", uawh.getName());
        request.setAttribute("shipDate", shipDate);
        request.setAttribute("rowIDX", "0");

        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");

        request.setAttribute("PROGRAMNAME", "XMS400/C");
        String forward = "";
        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "RM Issue Other. Entry");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        String wh = request.getParameter("wh");
        String shipDate = request.getParameter("shipDate");
        String dest = request.getParameter("dest");
        String round = request.getParameter("round");
        String lino = request.getParameter("lino");
        String driver = request.getParameter("driver");
        String follower = request.getParameter("follower");
        String type = request.getParameter("type");

        WH ua = new WHDao().findByUid(wh);
        Qdest qd = new QdestDao().findByCod(dest);

        request.setAttribute("wh", wh);
        request.setAttribute("whn", ua.getName());
        request.setAttribute("shipDate", shipDate);
        request.setAttribute("dest", dest);
        request.setAttribute("destn", qd.getDesc());
        request.setAttribute("round", round);
        request.setAttribute("lino", lino);
        request.setAttribute("driver", driver);
        request.setAttribute("follower", follower);
        request.setAttribute("type", type);

        List<WH> pList2 = new WHDao().findAll();
        List<Qdest> destList = new QdestDao().findByWhs(wh);
        List<QSELLER> sellerList = new QSELLERDao().findByWHS(wh);

        request.setAttribute("MCList", pList2);
        request.setAttribute("destList", destList);
        request.setAttribute("sellerList", sellerList);

        List<XMSOTDETAIL> detList = new XMSOTDETAILDao().findFromQI(wh, shipDate, dest, round);

        int rowIDX = 0;
        if (!detList.isEmpty()) {
            rowIDX = detList.size();
        }

        List<XMSOTDETAIL> detList2 = new ArrayList<XMSOTDETAIL>();

        String[] sender = request.getParameterValues("sender");
        String[] desc = request.getParameterValues("desc");
        String[] bag = request.getParameterValues("bag");
        String[] roll = request.getParameterValues("roll");
        String[] box = request.getParameterValues("box");
        String[] pcs = request.getParameterValues("pcs");
        String[] tot = request.getParameterValues("tot");
        String[] totdoc = request.getParameterValues("totdoc");
        String[] docno = request.getParameterValues("docno");

        int ite = detList.size();

        if (sender != null) {
            for (int i = 0; i < sender.length; i++) {
                int ab = 0;
                for (int i1 = 0; i1 < detList.size(); i1++) {
                    int aa = 0;
                    if (detList.get(i1).getXMSOTDSENDER().trim().equals(sender[i].trim())) {
                        aa += 1;
                    }
                    if (detList.get(i1).getXMSOTDDESC().trim().equals(desc[i].trim())) {
                        aa += 1;
                    }
                    if (detList.get(i1).getXMSOTDBAG().trim().equals(bag[i].trim())) {
                        aa += 1;
                    }
                    if (detList.get(i1).getXMSOTDROLL().trim().equals(roll[i].trim())) {
                        aa += 1;
                    }
                    if (detList.get(i1).getXMSOTDBOX().trim().equals(box[i].trim())) {
                        aa += 1;
                    }
                    if (detList.get(i1).getXMSOTDTOT().trim().equals(tot[i].trim())) {
                        aa += 1;
                    }
                    if (detList.get(i1).getXMSOTDTOTDOC().trim().equals(totdoc[i].trim())) {
                        aa += 1;
                    }
                    if (detList.get(i1).getXMSOTDDOCNO().trim().equals(docno[i].trim())) {
                        aa += 1;
                    }
                    if (aa == 8) {
                        ab += 1;
                    }
                }

                XMSOTDETAIL p = new XMSOTDETAIL();

                p.setXMSOTDLINE(Integer.toString(++ite));
                p.setXMSOTDSENDER(sender[i]);
                p.setXMSOTDDESC(desc[i]);
                p.setXMSOTDBAG(bag[i]);
                p.setXMSOTDROLL(roll[i]);
                p.setXMSOTDBOX(box[i]);
                p.setXMSOTDPCS(pcs[i]);
                p.setXMSOTDTOT(tot[i]);
                p.setXMSOTDTOTDOC(totdoc[i]);
                p.setXMSOTDDOCNO(docno[i]);

                if (ab == 0) {
                    detList2.add(p);
                }
            }
        }

        if (!detList2.isEmpty()) {
            rowIDX += detList2.size();
        }

        request.setAttribute("rowIDX", Integer.toString(rowIDX + 1));
        request.setAttribute("detList", detList);
        request.setAttribute("detList2", detList2);

        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);

    }
}
