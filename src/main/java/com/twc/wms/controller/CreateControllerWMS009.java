/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.controller;

import com.twc.wms.dao.UserDao;
import com.twc.wms.dao.WarehouseDao;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.twc.wms.entity.Warehouse;
import com.twc.wms.entity.UserAuth;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;

/**
 *
 * @author wien
 */
public class CreateControllerWMS009 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static final String PAGE_VIEW = "../views/createWMS009.jsp";

    public CreateControllerWMS009() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("PROGRAMNAME", "WMS009/C");
        String forward = "";
        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "User Authorization. Display");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        WarehouseDao dao = new WarehouseDao();
        List<Warehouse> pList = dao.selectWarehouse();

        String sendMessage = "";
        String mod = request.getParameter("mod");

        if (mod != null) {
            if (mod.equals("dup")) {
                sendMessage = "<script type=\"text/javascript\">\n"
                        + "            var show = function () {\n"
                        + "                $('#myModal').modal('show');\n"
                        + "            };\n"
                        + "\n"
                        + "            window.setTimeout(show, 0);\n"
                        + "\n"
                        + "        </script>";
                request.setAttribute("sendMessage", sendMessage);
            } else if (mod.equals("fail")) {
                sendMessage = "<script type=\"text/javascript\">\n"
                        + "            var show2 = function () {\n"
                        + "                $('#myModal2').modal('show');\n"
                        + "            };\n"
                        + "\n"
                        + "            window.setTimeout(show2, 0);\n"
                        + "\n"
                        + "        </script>";
                request.setAttribute("sendMessage", sendMessage);
            }
        }

        RequestDispatcher view = request.getRequestDispatcher(forward);
        request.setAttribute("WHList", pList);
        view.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        FileItemFactory factory = new DiskFileItemFactory();
        ServletFileUpload upload = new ServletFileUpload(factory);

        request.setCharacterEncoding("utf-8");

        String id = "";
        String pass = "";
        String name = "";
        String level = "";
        String wh = "";
        String dpart = "";
        String menu = "";
        String path = "";
        String userid = "";

        try {

            List<FileItem> uploadItems = upload.parseRequest(request);

            for (FileItem uploadItem : uploadItems) {
                if (uploadItem.isFormField()) {
                    String fieldName = uploadItem.getFieldName();
                    String value = uploadItem.getString();

                    if (fieldName.equals("userid22")) {
                        userid = value;
                    }
                    if (fieldName.equals("userid")) {
                        id = value;
                    }
                    if (fieldName.equals("password")) {
                        pass = value;
                    }
                    if (fieldName.equals("name")) {
                        name = new String(value.getBytes("iso-8859-1"), "UTF-8");
                    }
                    if (fieldName.equals("level")) {
                        level = value;
                    }
                    if (fieldName.equals("warehouse")) {
                        wh = value;
                    }
                    if (fieldName.equals("department")) {
                        dpart = new String(value.getBytes("iso-8859-1"), "UTF-8");
                    }
                    if (fieldName.equals("menuset")) {
                        menu = new String(value.getBytes("iso-8859-1"), "UTF-8");
                    }

                } else {
                    String fileNameP = FilenameUtils.getName(uploadItem.getName());
//                    String fieldNameP = uploadItem.getFieldName();
                    InputStream fileContentP = uploadItem.getInputStream();

                    try {
                        if (fileNameP.equals("")) {
                            path = "NO IMAGE";
                        } else {
                            path = fileNameP;
//                            File reportimage = new File(getServletConfig().getServletContext().getRealPath("/resources/images/user/user"));

                            ServletContext servletContext2 = getServletContext();
                            String realPath2 = servletContext2.getRealPath("/resources/images/");
                            String saperate2 = realPath2.contains(":") ? "\\" : "/";
                            String path2 = realPath2 + saperate2 + fileNameP;

                            OutputStream outputStream = new FileOutputStream(path2);
                            IOUtils.copy(fileContentP, outputStream);
                            outputStream.close();

//                            FileOutputStream output = new FileOutputStream(path2);
//                            byte[] buf = new byte[1024];
//                            int bytesRead;
//                            while ((bytesRead = fileContentP.read(buf)) > 0) {
//                                output.write(buf, 0, bytesRead);
//                            }
//                            fileContentP.close();
//                            output.close();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            }

            request.setAttribute("PROGRAMNAME", "WMS009/C");
            request.setAttribute("PROGRAMDESC", "User Authorization. Display");

            UserAuth ua = new UserAuth(id, pass, name, level, wh, dpart, menu);
            UserDao dao = new UserDao();

            if (dao.check(id).equals("f")) {
                response.setHeader("Refresh", "0;/TABLE2/WMS009/create?mod=dup");
            } else {
                if (dao.add(ua, userid, path)) {
                    response.setHeader("Refresh", "0;/TABLE2/WMS009/display");
                } else {
                    response.setHeader("Refresh", "0;/TABLE2/WMS009/create?mod=fail");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
