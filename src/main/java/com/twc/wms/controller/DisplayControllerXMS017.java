/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.controller;

import com.twc.wms.dao.WHDao;
import com.twc.wms.dao.XMS017Dao;
import com.twc.wms.dao.XMSBRHEADDao;
import com.twc.wms.entity.Qdest;
import com.twc.wms.entity.WH;
import com.twc.wms.entity.XMSBRHEAD;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author wien
 */
public class DisplayControllerXMS017 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static final String PAGE_CREATE = "../views/createXMS017.jsp";
    private static final String PAGE_DISPLAY = "../views/displayXMS017.jsp";
    private static DecimalFormat df4 = new DecimalFormat("#.####");

    public DisplayControllerXMS017() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String forward = "";
        try {
            String action = request.getParameter("action");
            request.setAttribute("PROGRAMDESC", "Material Master. Separate RM to GROUP QR ID (S)");
            if (action == null) {
                forward = PAGE_DISPLAY;
                request.setAttribute("PROGRAMNAME", "XMS017");

                WHDao dao2 = new WHDao();
                List<WH> pList2 = dao2.findAll();
                request.setAttribute("MCList", pList2);

                String date = new XMS017Dao().getDateByFormat("yyyy-MM-dd");
                request.setAttribute("date", date);

            } else if (action.equals("create")) {
                String wh = request.getParameter("wh");
                String whn = new WHDao().findByUid(wh).getName();
                request.setAttribute("wh", wh);
                request.setAttribute("whn", whn);

                forward = PAGE_CREATE;
                request.setAttribute("PROGRAMNAME", "XMS017/C");

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
//    ********

        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

}
