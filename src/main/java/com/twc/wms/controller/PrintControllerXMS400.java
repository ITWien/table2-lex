/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.controller;

import com.twc.wms.dao.QIDETAILDao;
import com.twc.wms.dao.QNBSERDao;
import com.twc.wms.dao.QdestDao;
import com.twc.wms.dao.WHDao;
import com.twc.wms.dao.XMSOTDETAILDao;
import com.twc.wms.dao.XMSOTHEADDao;
import com.twc.wms.entity.QIDETAIL;
import com.twc.wms.entity.Qdest;
import com.twc.wms.entity.WH;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRResultSetDataSource;
import net.sf.jasperreports.engine.JasperRunManager;

/**
 *
 * @author wien
 */
public class PrintControllerXMS400 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static final String FILE_PATH = "/report/";
    private static final String FILE_DEST = "/report/";
    private static final String FILE_FONT = "/resources/vendors/fonts/";

    public PrintControllerXMS400() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");

        String printBy = request.getParameter("printBy");

        if (printBy.equals("qno")) {
            String qno = request.getParameter("qno");
            String wh = request.getParameter("wh");
            String type = request.getParameter("type");
            String size = request.getParameter("size");

            try {
                if (new XMSOTHEADDao().addDocno(qno)) {
                    new QNBSERDao().addLastXMS400(wh);
                }

                String JasperName = "";

                if (type.equals("bill")) {
                    JasperName = "XMS400_QNO";

                } else if (type.equals("mat")) {
                    JasperName = "XMS400_QNO_MAT";

                } else if (type.equals("ot")) {
                    JasperName = "XMS400_QNO_OT";

                }

                if (size.equals("a4")) {
                    JasperName += "_A4";
                } else if (size.equals("a5")) {
                    JasperName += "_A5";
                }

                ResultSet result = new XMSOTDETAILDao().findForPrintXMS400QNO(qno);

                ServletOutputStream servletOutputStream = response.getOutputStream();
                File reportFile = new File(getServletConfig().getServletContext()
                        .getRealPath("/resources/jasper/" + JasperName + ".jasper"));
                byte[] bytes;

                HashMap<String, Object> map = new HashMap<String, Object>();

                try {
                    bytes = JasperRunManager.runReportToPdf(reportFile.getPath(), map, new JRResultSetDataSource(result));
                    response.setContentType("application/pdf");
                    response.setContentLength(bytes.length);

                    servletOutputStream.write(bytes, 0, bytes.length);
                    servletOutputStream.flush();
                    servletOutputStream.close();

                } catch (JRException e) {
                    StringWriter stringWriter = new StringWriter();
                    PrintWriter printWriter = new PrintWriter(stringWriter);
                    e.printStackTrace(printWriter);
                    response.setContentType("text/plain");
                    response.getOutputStream().print(stringWriter.toString());

                }

            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if (printBy.equals("dest")) {
            String wh = request.getParameter("wh");
            String shipDate = request.getParameter("shipDate");
            String dest = request.getParameter("dest");

        } else if (printBy.equals("whdt")) {
            String wh = request.getParameter("wh");
            String shipDate = request.getParameter("shipDate");

        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

}
