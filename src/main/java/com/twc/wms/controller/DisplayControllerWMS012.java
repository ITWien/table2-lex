/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.controller;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.twc.wms.dao.QSETDao;
import com.twc.wms.entity.QSET;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author wien
 */
public class DisplayControllerWMS012 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static final String PAGE_VIEW = "../views/displayWMS012.jsp";

    public DisplayControllerWMS012() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("PROGRAMNAME", "WMS012");
        String forward = "";
        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "Setting Code. Display");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
//    ********
        QSETDao dao = new QSETDao();
        List<QSET> set = dao.findAll();

        for (int i = 0; i < set.size(); i++) {
            QSETDao dao2 = new QSETDao();
            List<QSET> sq = dao2.findAllSeq(set.get(i).getCode());

            if (sq.size() >= 5) {
                set.get(i).setSq1(sq.get(0).getSq1());
                set.get(i).setSq2(sq.get(1).getSq1());
                set.get(i).setSq3(sq.get(2).getSq1());
                set.get(i).setSq4(sq.get(3).getSq1());
                set.get(i).setSq5(sq.get(4).getSq1());
            } else if (sq.size() == 4){
                set.get(i).setSq1(sq.get(0).getSq1());
                set.get(i).setSq2(sq.get(1).getSq1());
                set.get(i).setSq3(sq.get(2).getSq1());
                set.get(i).setSq4(sq.get(3).getSq1());
            } else if (sq.size() == 3){
                set.get(i).setSq1(sq.get(0).getSq1());
                set.get(i).setSq2(sq.get(1).getSq1());
                set.get(i).setSq3(sq.get(2).getSq1());
            } else if (sq.size() == 2){
                set.get(i).setSq1(sq.get(0).getSq1());
                set.get(i).setSq2(sq.get(1).getSq1());
            } else if (sq.size() == 1){
                set.get(i).setSq1(sq.get(0).getSq1());
            }

        }

        RequestDispatcher view = request.getRequestDispatcher(forward);
        request.setAttribute("setHLList", set);
        view.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

}
