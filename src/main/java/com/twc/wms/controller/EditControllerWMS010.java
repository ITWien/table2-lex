/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.controller;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.twc.wms.dao.QdestDao;
import com.twc.wms.entity.QDESTYP;
import com.twc.wms.entity.Qdest;
import java.util.List;
import javax.servlet.http.HttpSession;

/**
 *
 * @author wien
 */
public class EditControllerWMS010 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static final String PAGE_VIEW = "../views/editWMS010.jsp";

    public EditControllerWMS010() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("PROGRAMNAME", "WMS010/E");
        String forward = "";
        String cod = request.getParameter("cod");
        String type = request.getParameter("type");
        QdestDao dao = new QdestDao();
        QdestDao dao2 = new QdestDao();
        Qdest qd = dao.findByCod(cod);
        List<QDESTYP> tList = dao2.Selecttype();

        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "Destination Master. Display");
                request.setAttribute("EQDcod", qd.getCode());
                request.setAttribute("EQDdes", qd.getDesc());
                request.setAttribute("EQDname", qd.getName());
                request.setAttribute("EQDaddr1", qd.getAdrs1());
                request.setAttribute("EQDaddr2", qd.getAdrs2());
                request.setAttribute("EQDaddr3", qd.getAdrs3());
                request.setAttribute("EQDaddr4", qd.getAdrs4());
                request.setAttribute("DESTYPE", tList);
                request.setAttribute("type", type);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        RequestDispatcher view = request.getRequestDispatcher(forward);
        request.setAttribute("QdesttypeList", tList);
        view.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");

        String code = request.getParameter("EDITdestinationCode");
        String desc = request.getParameter("EDITdescription");
        String name = request.getParameter("name");
        String addr1 = request.getParameter("address_1");
        String addr2 = request.getParameter("address_2");
        String addr3 = request.getParameter("address_3");
        String addr4 = request.getParameter("address_4");
        String detype = request.getParameter("DESTYPE");
        detype = detype.substring(0, 1);
        String userid = request.getParameter("userid");

        request.setAttribute("PROGRAMNAME", "WMS010/E");
        request.setAttribute("PROGRAMDESC", "Destination Master. Display");

        QdestDao dao = new QdestDao();

        Qdest p = new Qdest(code, desc, name, addr1, addr2, addr3, addr4);

        dao.edit(p, userid, detype);

        response.setHeader("Refresh", "0;/TABLE2/WMS010/display");
    }

}
