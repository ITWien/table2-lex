/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.controller;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import com.twc.wms.entity.QRMTRA;
import com.twc.wms.dao.QRMTRADao;
import com.twc.wms.entity.WH;
import com.twc.wms.dao.WHDao;

/**
 *
 * @author wien
 */
public class DetailControllerWMS070 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static final String PAGE_VIEW = "../views/detailWMS070.jsp";

    public DetailControllerWMS070() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("PROGRAMNAME", "WMS070");
        String forward = "";

        String rn = request.getParameter("rn");
        String wh = request.getParameter("wh");
        String sd = request.getParameter("sd");
        String ed = request.getParameter("ed");

        WHDao dao2 = new WHDao();
        WH ua2 = dao2.findByUid(wh);

        QRMTRADao dao = new QRMTRADao();
        QRMTRA ua = dao.findByRunno(rn);

        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "Material History Transaction. Display");
                request.setAttribute("wh", wh);
                request.setAttribute("whn", ua2.getName());
                request.setAttribute("qcod", ua.getVtype());
                request.setAttribute("sdesc", ua.getMtype());
                request.setAttribute("qtdt", ua.getMatcode());
                request.setAttribute("qid", ua.getUm());
                request.setAttribute("qdn", ua.getMgroup());
                request.setAttribute("qsn", ua.getMgdesc());
                request.setAttribute("qqty", ua.getMatcode2());
                request.setAttribute("qbun", ua.getPgroup());
                request.setAttribute("qroll", ua.getMatcode5());
                request.setAttribute("qcdt", ua.getMatcode3());
                request.setAttribute("qplt", ua.getMatcode7());
                request.setAttribute("qval", ua.getMatcode8());
                request.setAttribute("qstrg", ua.getMatcode6());
                request.setAttribute("qmvt", ua.getDesc());
                request.setAttribute("qtrt", ua.getPlant());
                request.setAttribute("mc", ua.getMatcode1());
                request.setAttribute("mcn", ua.getMatcode9());
                request.setAttribute("mg", ua.getMatcode10());
                request.setAttribute("mgn", ua.getMatcode11());
                request.setAttribute("quser", ua.getLocation());
                
                request.setAttribute("sd", sd);
                request.setAttribute("ed", ed);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

}
