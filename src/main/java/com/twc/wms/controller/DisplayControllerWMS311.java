/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.controller;

import com.twc.wms.dao.WHDao;
import com.twc.wms.entity.WH;
import com.twc.wms.dao.QIHEADDao;
import com.twc.wms.entity.MSSMATN;
import java.io.IOException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author wien
 */
public class DisplayControllerWMS311 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static final String PAGE_VIEW = "../views/displayWMS311.jsp";

    public DisplayControllerWMS311() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("PROGRAMNAME", "WMS311");
        String forward = "";
        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "RM Issue. Logistics Returned");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
//    ********
        String whF = request.getParameter("whF");
        String whT = request.getParameter("whT");

        if (whF == null) {
            WHDao dao2 = new WHDao();
            List<WH> pList2 = dao2.findAll();

            request.setAttribute("numMat", 0);

            RequestDispatcher view = request.getRequestDispatcher(forward);
            request.setAttribute("MCList", pList2);
            view.forward(request, response);
        } else {
            if (whF.equals("") && !whT.equals("")) {
                whF = whT;
            } else if (whT.equals("") && !whF.equals("")) {
                whT = whF;
            }

            WHDao dao2 = new WHDao();
            List<WH> pList2 = dao2.findAll();

            WHDao dao1 = new WHDao();
            WH uaF = dao1.findByUid(whF);

            WHDao dao3 = new WHDao();
            WH uaT = dao3.findByUid(whT);

            request.setAttribute("mcF", whF);
            request.setAttribute("nameF", uaF.getName());
            request.setAttribute("mcT", whT);
            request.setAttribute("nameT", uaT.getName());
//        request.setAttribute("sd", sd);
//        request.setAttribute("ed", ed);

            QIHEADDao dao4 = new QIHEADDao();
            List<MSSMATN> MTCList = dao4.findByUidWMS311(whF, whT);

            request.setAttribute("numMat", MTCList.size());

            RequestDispatcher view = request.getRequestDispatcher(forward);
            request.setAttribute("MCList", pList2);
            request.setAttribute("MTCList", MTCList);
            view.forward(request, response);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");

        String whF = request.getParameter("warehouseFrom");
        String whT = request.getParameter("warehouseTo");
//        String sd = request.getParameter("startdate");
//        String ed = request.getParameter("enddate");

        if (whF.equals("") && !whT.equals("")) {
            whF = whT;
        } else if (whT.equals("") && !whF.equals("")) {
            whT = whF;
        }

//        if (sd.equals("") && !ed.equals("")) {
//            sd = ed;
//        } else if (ed.equals("") && !sd.equals("")) {
//            ed = sd;
//        }
        request.setAttribute("PROGRAMNAME", "WMS311");
        String forward = "";
        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "RM Issue. Logistics Returned");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
//    ********
        WHDao dao2 = new WHDao();
        List<WH> pList2 = dao2.findAll();

        WHDao dao1 = new WHDao();
        WH uaF = dao1.findByUid(whF);

        WHDao dao3 = new WHDao();
        WH uaT = dao3.findByUid(whT);

        request.setAttribute("mcF", whF);
        request.setAttribute("nameF", uaF.getName());
        request.setAttribute("mcT", whT);
        request.setAttribute("nameT", uaT.getName());
//        request.setAttribute("sd", sd);
//        request.setAttribute("ed", ed);

        QIHEADDao dao4 = new QIHEADDao();
        List<MSSMATN> MTCList = dao4.findByUidWMS311(whF, whT);

        request.setAttribute("numMat", MTCList.size());

        RequestDispatcher view = request.getRequestDispatcher(forward);
        request.setAttribute("MCList", pList2);
        request.setAttribute("MTCList", MTCList);
        view.forward(request, response);
    }

}
