/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.controller;

import com.twc.wms.dao.QRKDETAILDao;
import com.twc.wms.dao.WarehouseDao;
import com.twc.wms.entity.QRKDETAIL;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author wien
 */
public class DeleteGControllerWMS008 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static final String PAGE_VIEW = "../views/deleteGWMS008.jsp";

    public DeleteGControllerWMS008() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("PROGRAMNAME", "WMS008/GD");
        String forward = "";

        String wh = request.getParameter("wh");
        String zone = request.getParameter("zone");
        String loc = request.getParameter("loc");

        QRKDETAILDao dao = new QRKDETAILDao();
        QRKDETAIL ua = dao.findByCod(wh, zone, loc);
        WarehouseDao dao2 = new WarehouseDao();
        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "Rack Matrix. Display");
                request.setAttribute("wh", wh);
                String whname = dao2.findWHname(wh);
                request.setAttribute("whn", whname);
                request.setAttribute("zone", zone);
                request.setAttribute("rkno", ua.getRkno());
                request.setAttribute("side", ua.getSide());
                request.setAttribute("col", ua.getColumn());
                request.setAttribute("row", ua.getRow());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");

        String wh = request.getParameter("wh");
        String zone = request.getParameter("zone");
        String rkno = request.getParameter("rkno");
        String side = request.getParameter("side");
        String col = request.getParameter("col");
        String row = request.getParameter("row");
        String loc = rkno + side + col + row;

        QRKDETAILDao dao = new QRKDETAILDao();

        dao.delete(wh, zone, loc);

        response.setHeader("Refresh", "0;/TABLE2/WMS008/genrack?wh=" + wh);
    }

}
