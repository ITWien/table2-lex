/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.controller;

import com.twc.wms.dao.MSSMATNDao;
import com.twc.wms.dao.QPGMASDao;
import com.twc.wms.dao.PGMUDao;
import com.twc.wms.dao.QUSMTCTRLDao;
import com.twc.wms.dao.UserDao;
import com.twc.wms.entity.MSSMATN;
import com.twc.wms.entity.QPGMAS;
import com.twc.wms.entity.PGMU;
import com.twc.wms.entity.UserAuth;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import javax.servlet.http.HttpSession;

/**
 *
 * @author wien
 */
public class CreateNmatControllerWMS009 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static final String PAGE_VIEW = "../views/createNmatWMS009.jsp";

    public CreateNmatControllerWMS009() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("PROGRAMNAME", "WMS009/CC");
        String forward = "";
        String id = request.getParameter("id");

        UserDao dao = new UserDao();
        UserAuth ua = dao.findByUid(id);

        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "User Authorization Connect Mat Control. Display");
                request.setAttribute("id", id);
                request.setAttribute("name", ua.getName());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        MSSMATNDao dao2 = new MSSMATNDao();
        List<MSSMATN> pList2 = dao2.findAll();

        RequestDispatcher view = request.getRequestDispatcher(forward);
        request.setAttribute("MCList", pList2);
        view.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");

        String uid = request.getParameter("userid");
        String pid = request.getParameter("pid");

        String userid = request.getParameter("userid22");

        String[] pts = uid.split(" : ");
        String pt1 = pts[0];
        uid = pt1;

        String[] parts = pid.split(" + ");
        String part1 = parts[0];
        String[] ds = part1.split(" ");
        String ds1 = ds[0];
        pid = ds1;

        request.setAttribute("PROGRAMNAME", "WMS009/CC");
        request.setAttribute("PROGRAMDESC", "User Authorization Connect Mat Control. Display");

        request.setAttribute("id", uid);

        PGMU ua = new PGMU(uid, pid);
        QUSMTCTRLDao dao = new QUSMTCTRLDao();

        String sendMessage = "";
        String forward = "";

        if (dao.check(uid, pid).equals("f")) {
            sendMessage = "<script type=\"text/javascript\">\n"
                    + "            var show = function () {\n"
                    + "                $('#myModal').modal('show');\n"
                    + "            };\n"
                    + "\n"
                    + "            window.setTimeout(show, 0);\n"
                    + "\n"
                    + "        </script>";
            forward = PAGE_VIEW;
            request.setAttribute("sendMessage", sendMessage);

            RequestDispatcher view = request.getRequestDispatcher(forward);
            view.forward(request, response);
        } else {
            QUSMTCTRLDao daoadd = new QUSMTCTRLDao();
            if (daoadd.add(ua, userid)) {
                response.setHeader("Refresh", "0;/TABLE2/WMS009/nmat?uid=" + uid);
            } else {
                sendMessage = "<script type=\"text/javascript\">\n"
                        + "            var show2 = function () {\n"
                        + "                $('#myModal2').modal('show');\n"
                        + "            };\n"
                        + "\n"
                        + "            window.setTimeout(show2, 0);\n"
                        + "\n"
                        + "        </script>";
                forward = PAGE_VIEW;
                request.setAttribute("sendMessage", sendMessage);

                RequestDispatcher view = request.getRequestDispatcher(forward);
                view.forward(request, response);
            }
        }
    }
}
