/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.controller;

import com.twc.wms.dao.WHDao;
import com.twc.wms.entity.WH;
import com.twc.wms.dao.QIHEADDao;
import com.twc.wms.dao.UserDao;
import com.twc.wms.entity.MSSMATN;
import com.twc.wms.entity.UserAuth;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author wien
 */
public class DisplayMatControllerXMS340 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static final String PAGE_MAT = "../views/displayMatXMS340.jsp";
    private static final String PAGE_DEST = "../views/displayDestXMS340.jsp";

    public DisplayMatControllerXMS340() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        request.setCharacterEncoding("utf-8");

        String sb = request.getParameter("sb");
        String whF = request.getParameter("wh");

        String uid = request.getParameter("uid");
//        uid = "92416";
        UserAuth ua = new UserDao().findByUid(uid);
        if (ua.getWarehouse() != null) {
            whF = ua.getWarehouse();
            sb = "mat";
        }

        if (whF == null || sb == null) {
            request.setAttribute("PROGRAMNAME", "XMS340");
            String forward = "";
            try {
                String action = request.getParameter("action");
                if (action == null) {
                    forward = PAGE_MAT;
                    request.setAttribute("PROGRAMDESC", "RM Issue. Logistics Receipt");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
//    ********
            WHDao dao2 = new WHDao();
            List<WH> pList2 = dao2.findAllnoTWC();

            request.setAttribute("numMat", 0);

            RequestDispatcher view = request.getRequestDispatcher(forward);
            request.setAttribute("MCList", pList2);
            view.forward(request, response);
        } else {
            String whT = whF;
//        String sd = request.getParameter("startdate");
//        String ed = request.getParameter("enddate");

            if (whF.equals("") && !whT.equals("")) {
                whF = whT;
            } else if (whT.equals("") && !whF.equals("")) {
                whT = whF;
            }

//        if (sd.equals("") && !ed.equals("")) {
//            sd = ed;
//        } else if (ed.equals("") && !sd.equals("")) {
//            ed = sd;
//        }
            request.setAttribute("PROGRAMNAME", "XMS340");
            String forward = "";
            try {
                String action = request.getParameter("action");
                if (action == null) {
                    if (sb.equals("mat") || sb.equals("")) {
                        forward = PAGE_MAT;
                    } else if (sb.equals("dest")) {
                        forward = PAGE_DEST;
                    }
                    request.setAttribute("PROGRAMDESC", "RM Issue. Logistics Receipt");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
//    ********
            WHDao dao2 = new WHDao();
            List<WH> pList2 = dao2.findAllnoTWC();

            WHDao dao1 = new WHDao();
            WH uaF = dao1.findByUid(whF);

            WHDao dao3 = new WHDao();
            WH uaT = dao3.findByUid(whT);

            request.setAttribute("mcF", whF);
            request.setAttribute("nameF", uaF.getName());
            request.setAttribute("mcT", whT);
            request.setAttribute("nameT", uaT.getName());
//        request.setAttribute("sd", sd);
//        request.setAttribute("ed", ed);

            QIHEADDao dao4 = new QIHEADDao();
            List<MSSMATN> MTCList = new ArrayList<MSSMATN>();
            if (sb.equals("mat") || sb.equals("")) {
                MTCList = dao4.findByUidWMS320X(whF, whT);
                request.setAttribute("numMat", MTCList.size());
            } else if (sb.equals("dest")) {
                MTCList = dao4.findByUidWMS340DestX(whF, whT);
                request.setAttribute("numDest", MTCList.size());
            }

            RequestDispatcher view = request.getRequestDispatcher(forward);
            request.setAttribute("MCList", pList2);
            request.setAttribute("MTCList", MTCList);
            view.forward(request, response);
        }

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");

        String sb = request.getParameter("selectBy");
        String whF = request.getParameter("warehouseFrom");
        String whT = whF;
//        String sd = request.getParameter("startdate");
//        String ed = request.getParameter("enddate");

        if (whF.equals("") && !whT.equals("")) {
            whF = whT;
        } else if (whT.equals("") && !whF.equals("")) {
            whT = whF;
        }

//        if (sd.equals("") && !ed.equals("")) {
//            sd = ed;
//        } else if (ed.equals("") && !sd.equals("")) {
//            ed = sd;
//        }
        request.setAttribute("PROGRAMNAME", "XMS340");
        String forward = "";
        try {
            String action = request.getParameter("action");
            if (action == null) {
                if (sb.equals("mat") || sb.equals("")) {
                    forward = PAGE_MAT;
                } else if (sb.equals("dest")) {
                    forward = PAGE_DEST;
                }
                request.setAttribute("PROGRAMDESC", "RM Issue. Logistics Receipt");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
//    ********
        WHDao dao2 = new WHDao();
        List<WH> pList2 = dao2.findAllnoTWC();

        WHDao dao1 = new WHDao();
        WH uaF = dao1.findByUid(whF);

        WHDao dao3 = new WHDao();
        WH uaT = dao3.findByUid(whT);

        request.setAttribute("mcF", whF);
        request.setAttribute("nameF", uaF.getName());
        request.setAttribute("mcT", whT);
        request.setAttribute("nameT", uaT.getName());
//        request.setAttribute("sd", sd);
//        request.setAttribute("ed", ed);

        QIHEADDao dao4 = new QIHEADDao();
        List<MSSMATN> MTCList = new ArrayList<MSSMATN>();
        if (sb.equals("mat") || sb.equals("")) {
            MTCList = dao4.findByUidWMS320X(whF, whT);
            request.setAttribute("numMat", MTCList.size());
        } else if (sb.equals("dest")) {
            MTCList = dao4.findByUidWMS340DestX(whF, whT);
            request.setAttribute("numDest", MTCList.size());
        }

        RequestDispatcher view = request.getRequestDispatcher(forward);
        request.setAttribute("MCList", pList2);
        request.setAttribute("MTCList", MTCList);
        view.forward(request, response);
    }

}
