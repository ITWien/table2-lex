/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.controller;

import com.twc.wms.dao.QIDETAILDao;
import com.twc.wms.dao.WHDao;
import com.twc.wms.entity.WH;
import com.twc.wms.dao.QIHEADDao;
import com.twc.wms.dao.QdestDao;
import com.twc.wms.entity.MSSMATN;
import com.twc.wms.entity.QIDETAIL;
import com.twc.wms.entity.Qdest;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author wien
 */
public class DisplayControllerXMS350 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static final String PAGE_VIEW = "../views/displayXMS350.jsp";

    public DisplayControllerXMS350() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        request.setAttribute("PROGRAMNAME", "XMS350");
        String forward = "";
        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "RM Issue. Logistics Transportation");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
//    ********
        String wh = request.getParameter("wh");
        String dest = request.getParameter("dest");
        String mat = request.getParameter("mat");
        String shipDate = request.getParameter("shipDate");
        String sts = request.getParameter("sts");
        String MY = request.getParameter("MY");

        if (MY == null) {
            Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH) + 1;

            String y = Integer.toString(year);

            String m = Integer.toString(month);
            if (m.length() < 2) {
                m = "0" + m;
            }
            MY = y + "-" + m;
        }

        if (mat == null) {
            mat = "All";
        } else {
            if (mat.trim().equals("")) {
                mat = "All";
            }
        }

        if (dest != null) {
            dest = dest.split(" : ")[0];
        }

//        System.out.println(wh);
//        System.out.println(dest);
//        System.out.println(shipDate);
        WHDao dao2 = new WHDao();
        List<WH> pList2 = dao2.findAll();

        WHDao daofwhn = new WHDao();
        WH uawh = daofwhn.findByUid(wh);

        QdestDao dao = new QdestDao();
        List<Qdest> destList = dao.findByWhs(wh);

        List<QIDETAIL> matList = new QIDETAILDao().findMat350X(wh, dest);

        QdestDao daodes = new QdestDao();
        Qdest uade = daodes.findByCod(dest);

        QIDETAILDao daodt = new QIDETAILDao();

        if (sts == null || sts.trim().equals("")) {
            sts = "2";
        }

        if (sts.equals("2")) {
            request.setAttribute("disno6", "display:none;");
//            request.setAttribute("disno5", "display:none;");
        } else if (sts.equals("5")) {
            request.setAttribute("disno5", "display:none;");
        } else if (sts.equals("6")) {
//            response.setHeader("Refresh", "0;/TABLE2/XMS350/display");
            request.setAttribute("disno6", "display:none;");
        }

        if ((wh != null && dest != null) && (!wh.equals("") && !dest.equals(""))) {
            List<QIDETAIL> detList = daodt.findDetail350X(wh, dest, shipDate, sts, mat.split(" ")[0], MY);
            request.setAttribute("detList", detList);
            if (!detList.isEmpty()) {
                QIDETAIL detListSum = new QIDETAIL();
                detListSum.setBox(detList.get(detList.size() - 1).getBox());
                detListSum.setBag(detList.get(detList.size() - 1).getBag());
                detListSum.setRoll(detList.get(detList.size() - 1).getRoll());
                detListSum.setM3(detList.get(detList.size() - 1).getM3());
                detListSum.setQty(detList.get(detList.size() - 1).getQty());
                request.setAttribute("detListSum", detListSum);
                detList.remove(detList.size() - 1);
            }
        }

        request.setAttribute("wh", wh);
        request.setAttribute("whn", uawh.getName());
        request.setAttribute("dest", dest);
        request.setAttribute("sts", sts);
        request.setAttribute("mat", new String(mat.getBytes("iso-8859-1"), "UTF-8"));
        request.setAttribute("destn", uade.getDesc());
        request.setAttribute("shipDate", shipDate);
        request.setAttribute("MY", MY);
        RequestDispatcher view = request.getRequestDispatcher(forward);
        request.setAttribute("MCList", pList2);
        request.setAttribute("destList", destList);
        request.setAttribute("matList", matList);

        view.forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("PROGRAMNAME", "XMS350");
        String forward = "";
        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "RM Issue. Logistics Transportation");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
//    ********
        String wh = request.getParameter("wh");
        String dest = request.getParameter("dest");
        String mat = request.getParameter("mat");
        String shipDate = request.getParameter("shipDate");
        String sts = request.getParameter("sts");
        String MY = request.getParameter("MY");

        if (MY == null) {
            Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH) + 1;

            String y = Integer.toString(year);

            String m = Integer.toString(month);
            if (m.length() < 2) {
                m = "0" + m;
            }
            MY = y + "-" + m;
        }

        if (dest != null) {
            dest = dest.split(" : ")[0];
        }

        if (mat == null) {
            mat = "All";
        } else {
            if (mat.trim().equals("")) {
                mat = "All";
            }
        }

//        System.out.println(wh);
//        System.out.println(dest);
//        System.out.println(shipDate);
        WHDao dao2 = new WHDao();
        List<WH> pList2 = dao2.findAll();

        WHDao daofwhn = new WHDao();
        WH uawh = daofwhn.findByUid(wh);

        QdestDao dao = new QdestDao();
        List<Qdest> destList = dao.findByWhs(wh);

        List<QIDETAIL> matList = new QIDETAILDao().findMat350X(wh, dest);

        QdestDao daodes = new QdestDao();
        Qdest uade = daodes.findByCod(dest);

        if (sts == null || sts.trim().equals("")) {
            sts = "2";
        }

        String ck[] = request.getParameterValues("selectCk");

        if (ck != null) {
            sts = "5";

            for (int i = 0; i < ck.length; i++) {
                new QIDETAILDao().rcQI2To5("5", ck[i].split("-")[0], ck[i].split("-")[1], ck[i].split("-")[4]);
            }

            request.setAttribute("disno5", "display:none;");
        } else {
            if (sts.equals("2")) {
                request.setAttribute("disno6", "display:none;");
//                request.setAttribute("disno5", "display:none;");
            } else if (sts.equals("6")) {
                request.setAttribute("disno6", "display:none;");
            }
        }

        QIDETAILDao daodt = new QIDETAILDao();
        if ((wh != null && dest != null) && (!wh.equals("") && !dest.equals(""))) {
            List<QIDETAIL> detList = daodt.findDetail350X(wh, dest, shipDate, sts, mat.split(" ")[0], MY);
            request.setAttribute("detList", detList);
            if (!detList.isEmpty()) {
                QIDETAIL detListSum = new QIDETAIL();
                detListSum.setBox(detList.get(detList.size() - 1).getBox());
                detListSum.setBag(detList.get(detList.size() - 1).getBag());
                detListSum.setRoll(detList.get(detList.size() - 1).getRoll());
                detListSum.setM3(detList.get(detList.size() - 1).getM3());
                detListSum.setQty(detList.get(detList.size() - 1).getQty());
                request.setAttribute("detListSum", detListSum);
                detList.remove(detList.size() - 1);
            }
        }

        request.setAttribute("wh", wh);
        request.setAttribute("whn", uawh.getName());
        request.setAttribute("dest", dest);
        request.setAttribute("sts", sts);
        request.setAttribute("mat", new String(mat.getBytes("iso-8859-1"), "UTF-8"));
        request.setAttribute("destn", uade.getDesc());
        request.setAttribute("shipDate", shipDate);
        request.setAttribute("MY", MY);
        RequestDispatcher view = request.getRequestDispatcher(forward);
        request.setAttribute("MCList", pList2);
        request.setAttribute("destList", destList);
        request.setAttribute("matList", matList);

        view.forward(request, response);
    }

}
