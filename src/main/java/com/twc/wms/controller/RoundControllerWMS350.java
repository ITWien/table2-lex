/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.controller;

import com.twc.wms.dao.QIDETAILDao;
import com.twc.wms.entity.QIDETAIL;
import com.twc.wms.dao.QIHEADDao;
import com.twc.wms.entity.QIHEAD;
import java.io.IOException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author wien
 */
public class RoundControllerWMS350 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;

    public RoundControllerWMS350() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");

        String[] id = request.getParameterValues("selectCk");
        String shipDate = request.getParameter("shipDate");
        String wh = request.getParameter("wh");
        String dest = request.getParameter("dest");
        String round = request.getParameter("round");

        String userid = request.getParameter("userid");

        if (id != null) {
            for (int i = 0; i < id.length; i++) {
                QIDETAILDao dao2 = new QIDETAILDao();
                dao2.round(id[i].trim(), userid, round);
            }
        }

        response.setHeader("Refresh", "0;/TABLE2/WMS350/display?shipDate=" + shipDate + "&wh=" + wh + "&dest=" + dest);

    }

}
