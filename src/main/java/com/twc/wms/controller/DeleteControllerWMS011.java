/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.controller;

import com.twc.wms.dao.QPGTYPEDao;
import com.twc.wms.dao.QPGMAS_2Dao;
import com.twc.wms.dao.UserDao;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.twc.wms.entity.QPGTYPE;
import com.twc.wms.entity.QPGMAS_2;
import com.twc.wms.entity.UserAuth;
import java.util.List;

/**
 *
 * @author wien
 */
public class DeleteControllerWMS011 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static final String PAGE_VIEW = "../views/deleteWMS011.jsp";

    public DeleteControllerWMS011() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        request.setAttribute("PROGRAMNAME", "WMS011/D");
        String forward = "";

        String pid = request.getParameter("pid");

        QPGMAS_2Dao dao1 = new QPGMAS_2Dao();
        QPGMAS_2 ua = dao1.findByUid(pid);

        String[] parts = ua.getType().split(" : ");
        String typeid = parts[0];
        String typedesc = parts[1];

        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "Program Master. Display");
                request.setAttribute("pid", pid);
                request.setAttribute("name", ua.getName());
                request.setAttribute("typeid", typeid);
                request.setAttribute("typedesc", typedesc);
                request.setAttribute("resuid", ua.getRespon());

                UserDao dao3 = new UserDao();
                UserAuth us = dao3.findByUid(ua.getRespon());

                request.setAttribute("resname", us.getName());

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");

        String pid = request.getParameter("pid");

        QPGMAS_2Dao dao = new QPGMAS_2Dao();

        dao.delete(pid);

        response.setHeader("Refresh", "0;/TABLE2/WMS011/display");
    }

}
