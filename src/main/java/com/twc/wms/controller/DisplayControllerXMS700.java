/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.controller;

import com.twc.wms.dao.XMSBRHEADDao;
import com.twc.wms.entity.Qdest;
import com.twc.wms.entity.XMSBRHEAD;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author wien
 */
public class DisplayControllerXMS700 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static final String PAGE_VIEW = "../views/displayXMS700.jsp";
    private static DecimalFormat df4 = new DecimalFormat("#.####");

    public DisplayControllerXMS700() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        request.setAttribute("PROGRAMNAME", "XMS700");
        String forward = "";
        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "Linear Equation. Display");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
//    ********
        String body = "<input type=\"text\" onpaste=\"getPastedValue(this,event);\" name=\"inputX\" style=\"text-align: right; width: 45%;\" onkeypress=\"return isNumberKey(event)\">\n"
                + "<input type=\"text\" onpaste=\"getPastedValue(this,event);\" name=\"inputY\" style=\"text-align: right; width: 45%;\" onkeypress=\"return isNumberKey(event)\">";

        request.setAttribute("body", body);

        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        request.setAttribute("PROGRAMNAME", "XMS700");
        String forward = "";
        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "Linear Equation. Display");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
//    ********
        String[] inputX = request.getParameterValues("inputX");
        String[] inputY = request.getParameterValues("inputY");

        String body = "<input type=\"text\" onpaste=\"getPastedValue(this,event);\" name=\"inputX\" style=\"text-align: right; width: 45%;\" onkeypress=\"return isNumberKey(event)\">\n"
                + "<input type=\"text\" onpaste=\"getPastedValue(this,event);\" name=\"inputY\" style=\"text-align: right; width: 45%;\" onkeypress=\"return isNumberKey(event)\">";

        if (inputX != null) {
            double[] x = new double[inputX.length];
            double[] y = new double[inputY.length];

            List<Qdest> inputXYList = new ArrayList<Qdest>();

            for (int i = 0; i < inputX.length; i++) {

                if (!(inputX[i].trim().equals("") && inputY[i].trim().equals(""))) {
                    String tmpX = inputX[i];
                    String tmpY = inputY[i];
                    if (inputX[i].trim().equals("") || inputX[i].trim().equals(".")) {
                        tmpX = "0";
                    }
                    if (inputY[i].trim().equals("") || inputY[i].trim().equals(".")) {
                        tmpY = "0";
                    }

                    x[i] = Double.parseDouble(tmpX);
                    y[i] = Double.parseDouble(tmpY);

                    Qdest p = new Qdest();
                    p.setCode(inputX[i]);
                    p.setDesc(inputY[i]);
                    inputXYList.add(p);
                }
            }

            double m = slope(x, y);
            double b = intercept(x, y);

            String result = "y = " + df4.format(m) + "x + " + df4.format(b);
            request.setAttribute("result", result);

            request.setAttribute("inputXYList", inputXYList);

            if (inputXYList.isEmpty()) {
                request.setAttribute("body", body);
            }
        } else {
            request.setAttribute("body", body);
        }

        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);
    }

    static double slope(double[] x, double[] y) {
        double xsum = 0;
        double ysum = 0;

        for (int i = 0; i < x.length; i++) {
            xsum += x[i];
            ysum += y[i];
        }

        double xbar = xsum / x.length;
        double ybar = ysum / y.length;

        double xbsum = 0;
        double ybsum = 0;

        for (int i = 0; i < x.length; i++) {
            xbsum += (x[i] - xbar) * (y[i] - ybar);
            ybsum += (x[i] - xbar) * (x[i] - xbar);
        }

        double m = xbsum / ybsum;

        return m;
    }

    static double intercept(double[] x, double[] y) {
        double xsum = 0;
        double ysum = 0;

        for (int i = 0; i < x.length; i++) {
            xsum += x[i];
            ysum += y[i];
        }

        double xbar = xsum / x.length;
        double ybar = ysum / y.length;

        double xbsum = 0;
        double ybsum = 0;

        for (int i = 0; i < x.length; i++) {
            xbsum += (x[i] - xbar) * (y[i] - ybar);
            ybsum += (x[i] - xbar) * (x[i] - xbar);
        }

        double m = xbsum / ybsum;
        double b = ybar - (m * xbar);

        return b;
    }

}
