/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.controller;

import com.twc.wms.dao.QPGTYPEDao;
import com.twc.wms.dao.UserDao;
import com.twc.wms.dao.QPGMAS_2Dao;
import com.twc.wms.dao.QPGSYSDao;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.twc.wms.entity.QPGTYPE;
import com.twc.wms.entity.UserAuth;
import com.twc.wms.entity.QPGMAS_2;
import java.util.List;
import javax.servlet.http.HttpSession;

/**
 *
 * @author wien
 */
public class CreateControllerWMS011 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static final String PAGE_VIEW = "../views/createWMS011.jsp";

    public CreateControllerWMS011() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("PROGRAMNAME", "WMS011");
        String forward = "";
        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "Program Master. Display");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        QPGTYPEDao dao = new QPGTYPEDao();
        List<QPGTYPE> pList = dao.selectPGType();

        QPGSYSDao daopg = new QPGSYSDao();
        List<QPGTYPE> pgList = daopg.selectPGSYS();

        UserDao dao1 = new UserDao();
        List<UserAuth> uList = dao1.findAllIT();

        RequestDispatcher view = request.getRequestDispatcher(forward);
        request.setAttribute("QPGTYPEList", pList);
        request.setAttribute("QPGSYSList", pgList);
        request.setAttribute("UserList", uList);
        view.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");

        String pid = request.getParameter("pid");
        String name = request.getParameter("name");
        String sys = request.getParameter("sys");
        String type = request.getParameter("type");
        String respon = request.getParameter("respon");

//        HttpSession session = request.getSession(true);
//        String userid = (String) session.getAttribute("uid");
        request.setAttribute("PROGRAMNAME", "WMS011");
        request.setAttribute("PROGRAMDESC", "Program Master. Display");

        QPGMAS_2 ua = new QPGMAS_2(pid, name, type, respon);
        QPGMAS_2Dao dao = new QPGMAS_2Dao();

        String sendMessage = "";
        String forward = "";

        if (dao.check(pid).equals("f")) {
            sendMessage = "<script type=\"text/javascript\">\n"
                    + "            var show = function () {\n"
                    + "                $('#myModal').modal('show');\n"
                    + "            };\n"
                    + "\n"
                    + "            window.setTimeout(show, 0);\n"
                    + "\n"
                    + "        </script>";
            forward = PAGE_VIEW;
            request.setAttribute("sendMessage", sendMessage);

            RequestDispatcher view = request.getRequestDispatcher(forward);
            view.forward(request, response);
        } else {

            if (dao.add(ua, respon, sys)) {
                response.setHeader("Refresh", "0;/TABLE2/WMS011/display");
            } else {
                sendMessage = "<script type=\"text/javascript\">\n"
                        + "            var show2 = function () {\n"
                        + "                $('#myModal2').modal('show');\n"
                        + "            };\n"
                        + "\n"
                        + "            window.setTimeout(show2, 0);\n"
                        + "\n"
                        + "        </script>";
                forward = PAGE_VIEW;
                request.setAttribute("sendMessage", sendMessage);

                RequestDispatcher view = request.getRequestDispatcher(forward);
                view.forward(request, response);
            }
        }
    }
}
