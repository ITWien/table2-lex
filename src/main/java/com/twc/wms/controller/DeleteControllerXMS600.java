/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.controller;

import com.twc.wms.dao.XMSBRDETAILDao;
import com.twc.wms.dao.XMSBRHEADDao;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author wien
 */
public class DeleteControllerXMS600 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;

    public DeleteControllerXMS600() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String qno = request.getParameter("qno");
        String shipDate = request.getParameter("shipDate");

        new XMSBRHEADDao().delete(qno);
        new XMSBRDETAILDao().delete(qno);

        response.setHeader("Refresh", "0;/TABLE2/XMS600/display?shipDate=" + shipDate);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

}
