/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.controller;

import com.twc.wms.dao.UserDao;
import com.twc.wms.dao.WarehouseDao;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.twc.wms.entity.Warehouse;
import com.twc.wms.entity.UserAuth;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;

/**
 *
 * @author wien
 */
public class EditControllerWMS009 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static final String PAGE_VIEW = "../views/editWMS009.jsp";

    public EditControllerWMS009() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("PROGRAMNAME", "WMS009/E");
        String forward = "";
        String id = request.getParameter("uid");
        UserDao dao = new UserDao();
        WarehouseDao dao2 = new WarehouseDao();
        UserAuth ua = dao.findByUid(id);
        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "User Authorization. Display");
                request.setAttribute("Eid", ua.getUid());
                request.setAttribute("Epass", ua.getPassword());
                request.setAttribute("Ename", ua.getName());
                request.setAttribute("Elevel", ua.getLevel());

                request.setAttribute("Ewh", ua.getWarehouse());
                String whname = dao2.findWHname(ua.getWarehouse());
                request.setAttribute("Ewhn", whname);

                request.setAttribute("Edpart", ua.getDepartment());
                request.setAttribute("Emenu", ua.getMenuset());
                request.setAttribute("path", ua.getPath());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        WarehouseDao dao1 = new WarehouseDao();
        List<Warehouse> pList = dao1.selectWarehouse();

        RequestDispatcher view = request.getRequestDispatcher(forward);
        request.setAttribute("WHList", pList);
        view.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        FileItemFactory factory = new DiskFileItemFactory();
        ServletFileUpload upload = new ServletFileUpload(factory);

        request.setCharacterEncoding("utf-8");

        String id = "";
        String pass = "";
        String name = "";
        String level = "";
        String wh = "";
        String dpart = "";
        String menu = "";
        String path = "";
        String pthChk = "";

        try {

            List<FileItem> uploadItems = upload.parseRequest(request);

            for (FileItem uploadItem : uploadItems) {
                if (uploadItem.isFormField()) {
                    String fieldName = uploadItem.getFieldName();
                    String value = uploadItem.getString();

                    if (fieldName.equals("path")) {
                        pthChk = value;
                    }
                    if (fieldName.equals("userid")) {
                        id = value;
                    }
                    if (fieldName.equals("password")) {
                        pass = value;
                    }
                    if (fieldName.equals("name")) {
                        name = new String(value.getBytes("iso-8859-1"), "UTF-8");
                    }
                    if (fieldName.equals("level")) {
                        level = value;
                    }
                    if (fieldName.equals("warehouse")) {
                        wh = value;
                    }
                    if (fieldName.equals("department")) {
                        dpart = new String(value.getBytes("iso-8859-1"), "UTF-8");
                    }
                    if (fieldName.equals("menuset")) {
                        menu = new String(value.getBytes("iso-8859-1"), "UTF-8");
                    }

                } else {
                    String fileNameP = FilenameUtils.getName(uploadItem.getName());
//                    String fieldNameP = uploadItem.getFieldName();
                    InputStream fileContentP = uploadItem.getInputStream();

                    try {
                        if (fileNameP.equals("")) {
                            if (pthChk.equals("male.jpg")) {
                                path = "NO IMAGE";
                            } else {
                                path = pthChk;
                            }
                        } else {
                            path = fileNameP;
//                            File reportimage = new File(getServletConfig().getServletContext().getRealPath("/resources/images/user/user"));

                            ServletContext servletContext2 = getServletContext();
                            String realPath2 = servletContext2.getRealPath("/resources/images/");
                            String saperate2 = realPath2.contains(":") ? "\\" : "/";
                            String path2 = realPath2 + saperate2 + fileNameP;

                            OutputStream outputStream = new FileOutputStream(path2);
                            IOUtils.copy(fileContentP, outputStream);
                            outputStream.close();

//                            FileOutputStream output = new FileOutputStream(path2);
//                            byte[] buf = new byte[1024];
//                            int bytesRead;
//                            while ((bytesRead = fileContentP.read(buf)) > 0) {
//                                output.write(buf, 0, bytesRead);
//                            }
//                            fileContentP.close();
//                            output.close();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            }

            String userid = request.getParameter("userid");

            request.setAttribute("PROGRAMNAME", "WMS009/E");
            request.setAttribute("PROGRAMDESC", "User Authorization. Display");

            String sendMessage = "";
            String forward = "";

            UserDao dao = new UserDao();
            UserAuth p = new UserAuth(id, pass, name, level, wh, dpart, menu);

            if (dao.edit(p, userid, path)) {
                response.setHeader("Refresh", "0;/TABLE2/WMS009/display");
            } else {
                sendMessage = "<script type=\"text/javascript\">\n"
                        + "            var show2 = function () {\n"
                        + "                $('#myModal2').modal('show');\n"
                        + "            };\n"
                        + "\n"
                        + "            window.setTimeout(show2, 0);\n"
                        + "\n"
                        + "        </script>";
                forward = PAGE_VIEW;
                request.setAttribute("sendMessage", sendMessage);

                RequestDispatcher view = request.getRequestDispatcher(forward);
                view.forward(request, response);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
