/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.controller;

import com.twc.wms.dao.WHDao;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import com.twc.wms.entity.QRMTRA;
import com.twc.wms.dao.QRMTRADao;
import com.twc.wms.entity.WH;

/**
 *
 * @author wien
 */
public class DisplayControllerWMS070 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static final String PAGE_VIEW = "../views/displayWMS070.jsp";

    public DisplayControllerWMS070() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("PROGRAMNAME", "WMS070");
        String forward = "";

        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "Material History Transaction. Display");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        WHDao dao2 = new WHDao();
        List<WH> pList2 = dao2.findAll();

        RequestDispatcher view = request.getRequestDispatcher(forward);
        request.setAttribute("MCList", pList2);
        view.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");

        String wh = request.getParameter("warehouse");
        String sd = request.getParameter("startdate");
        String ed = request.getParameter("enddate");

        request.setAttribute("PROGRAMNAME", "WMS070");
        String forward = "";
        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "Material History Transaction. Display");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        QRMTRADao dao = new QRMTRADao();
        List<QRMTRA> QList = dao.findByMc(wh, sd, ed);

        WHDao dao1 = new WHDao();
        WH ua = dao1.findByUid(wh);

        request.setAttribute("mc", wh);
        request.setAttribute("name", ua.getName());
        request.setAttribute("sd", sd);
        request.setAttribute("ed", ed);

        WHDao dao2 = new WHDao();
        List<WH> pList2 = dao2.findAll();

        RequestDispatcher view = request.getRequestDispatcher(forward);
        request.setAttribute("MCNList", QList);
        request.setAttribute("MCList", pList2);
        view.forward(request, response);
    }

}
