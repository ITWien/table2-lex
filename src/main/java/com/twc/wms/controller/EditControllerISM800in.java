/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.controller;

import com.twc.wms.dao.ISM800Dao;
import com.twc.wms.entity.ISMSDWH;
import java.io.IOException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author 93176
 */
public class EditControllerISM800in extends HttpServlet {

    private static final String PAGE_VIEW = "../views/editISM800in.jsp";

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        request.setAttribute("PROGRAMNAME", "ISM800/E");
        request.setAttribute("PROGRAMDESC", "Sales Order Download. Process");

        String saleorder = request.getParameter("slord");
        String no = request.getParameter("no");
        String dt = request.getParameter("dt");

//        System.out.println("saleorder " + saleorder + " " + " dt " + dt);
        List<ISMSDWH> disp = new ISM800Dao().findEditinBy(saleorder, dt);

        request.setAttribute("dtpara", disp.get(0).getISHTRDT());
        request.setAttribute("dtshow", disp.get(0).getISHTRDT());
//        request.setAttribute("dtshow", disp.get(0).getISHTRDT().substring(6, 8) + "/" + disp.get(0).getISHTRDT().substring(4, 6) + "/" + disp.get(0).getISHTRDT().substring(0, 4));
        request.setAttribute("cus", disp.get(0).getISHCUNO());
        request.setAttribute("cusnam", disp.get(0).getISHCUNM1());
        request.setAttribute("no", no);
        request.setAttribute("ord", disp.get(0).getISHORD());
        request.setAttribute("grp", disp.get(0).getISHPGRP());
        request.setAttribute("rson", disp.get(0).getReason());
        request.setAttribute("slord", saleorder);
        request.setAttribute("dldate", disp.get(0).getISHDLDT());

        RequestDispatcher view = request.getRequestDispatcher(PAGE_VIEW);
        view.forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String saleorder = request.getParameter("slord");
        String no = request.getParameter("no");
        String[] ccbox = request.getParameterValues("CCBOX");

        String number = ccbox[0].split(":")[0];
        String order = ccbox[0].split(":")[1];

        String uid = request.getParameter("userid");

        String dt = request.getParameter("dt");

//        for (int i = 0; i < ccbox.length; i++) {
//            
//            System.out.println(ccbox[i]);
//
//            String number = ccbox[i].split(":")[0];
//            String order = ccbox[i].split(":")[1];
//            String line = ccbox[i].split(":")[2];
//
//            List<ISMSDWD> LineD = new ISM800Dao().GetDWNByORDByLine(order, number, line); //Line Mas
//
//            System.out.println("number " + number);
//            System.out.println("order " + order);
//            System.out.println("line " + line);
//        }
//        List<ISMMASD> LineMas = new ISM800Dao().GetMASByORD(order, number); //Line Mas
//        List<ISMSDWD> LineDown = new ISM800Dao().GetDWNByORD(order, number); //Line Mas
//        String alrt = "";
//
//        if (LineMas.size() == LineDown.size()) {
//
//            if (!LineMas.get(0).getISDSTS().equals("0")) {
//                alrt = "alertify.error('Please Return Status To 0');";
//            }
//
//        } else {
//
//            if (!LineMas.get(0).getISDSTS().equals("0")) {
//                alrt = "alertify.error('Please Return Status To 0');";
//            } else {
//                String seq = new ISM800Dao().SelectMAXSEQbyORD(order); //seq + 1
//
//                List<ISMMASH> GET = new ISM800Dao().GetMASHEADByORD(order, number); // Old Head
////                boolean insertISMMASH = new ISM800Dao().addISMMASH(GET.get(0).getISHORD(), seq, GET.get(0).getISHTRDT(), GET.get(0).getISHUART(), GET.get(0).getISHTWEG(),
////                        GET.get(0).getISHMVT(), GET.get(0).getISHCUNO(), GET.get(0).getISHCUNM1(), GET.get(0).getISHCUNM2(), GET.get(0).getISHCUAD1(), GET.get(0).getISHCUAD2(),
////                        GET.get(0).getISHBSTKD(), GET.get(0).getISHSUBMI(), GET.get(0).getISHMATLOT(), GET.get(0).getISHTAXNO(), GET.get(0).getISHBRANCH01(), GET.get(0).getISHDATUM(),
////                        GET.get(0).getISHSTYLE(), GET.get(0).getISHCOLOR(), GET.get(0).getISHLOT(), GET.get(0).getISHAMTFG(), GET.get(0).getISHSTYLE2(), GET.get(0).getISHNOR(),
////                        GET.get(0).getISHPOFG(), GET.get(0).getISHGRPNO(), GET.get(0).getISHWHNO(), GET.get(0).getISHPGRP(), GET.get(0).getISHDEST(), uid, "0", "0");
//                for (int i = LineMas.size(); i < LineDown.size(); i++) {
////                    boolean insertISMMASD = new ISM800Dao().addISMMASD(LineDown.get(i).getISDORD(), seq, LineDown.get(i).getISDLINO(), "0", LineDown.get(i).getISDITNO().trim(),
////                            LineDown.get(i).getISDPLANT(), LineDown.get(i).getISDVT().trim(), LineDown.get(i).getISDSTRG(), LineDown.get(i).getISDRQQTY(), LineDown.get(i).getISDUNIT(),
////                            LineDown.get(i).getISDPRICE(), LineDown.get(i).getISDUNR01(), LineDown.get(i).getISDUNR03(), LineDown.get(i).getISDMTCTRL(),
////                            LineDown.get(i).getISDDEST(), uid, "0");
//                }
//            }
//        }
//        request.setAttribute("ALRT", alrt);
//        request.setAttribute("dt", date);
//        request.setAttribute("cus", customer);
        request.setAttribute("no", no);

        List<ISMSDWH> disp = new ISM800Dao().findEditinBy(saleorder, dt);

        request.setAttribute("dtpara", disp.get(0).getISHTRDT());
        request.setAttribute("dtshow", disp.get(0).getISHTRDT().substring(6, 8) + "/" + disp.get(0).getISHTRDT().substring(4, 6) + "/" + disp.get(0).getISHTRDT().substring(0, 4));
        request.setAttribute("cus", disp.get(0).getISHCUNO());
        request.setAttribute("cusnam", disp.get(0).getISHCUNM1());
        request.setAttribute("no", no);
        request.setAttribute("ord", disp.get(0).getISHORD());
        request.setAttribute("grp", disp.get(0).getISHPGRP());
        request.setAttribute("rson", disp.get(0).getReason());

        RequestDispatcher view = request.getRequestDispatcher(PAGE_VIEW);
        view.forward(request, response);

    }

}
