/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.utils;

import com.google.gson.Gson;
import com.twc.wms.dao.QDMBAGDao;
import com.twc.wms.dao.WHDao;
import com.twc.wms.dao.XMS017Dao;
import com.twc.wms.entity.QDMBAG;
import com.twc.wms.entity.QRMMAS;
import com.twc.wms.entity.QSHEAD;
import com.twc.wms.entity.WMS950;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author nutthawoot.noo
 */
public class ShowDataTablesXMS017 extends HttpServlet {

    private Gson gson;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        PrintWriter out = response.getWriter();
        gson = new Gson();
        String json = null;

        String mode = request.getParameter("mode");
        String type = request.getParameter("type");
        String idcode = request.getParameter("idcode");
        String id = request.getParameter("id");
        String code = request.getParameter("code");
        String len = request.getParameter("len");

        String wh = request.getParameter("wh");
        String gid = request.getParameter("gid");
        String mat = request.getParameter("mat");
        String tranDateFrom = request.getParameter("tranDateFrom");
        String tranDateTo = request.getParameter("tranDateTo");

        try {
            if (mode != null) {
                if (mode.equals("display")) {
                    List<QSHEAD> QSHEADList = new XMS017Dao().findByWhTranDate(wh, tranDateFrom, tranDateTo);
                    json = gson.toJson(QSHEADList);
                    response.getWriter().write(json);

                } else if (mode.equals("findMaster")) {
                    idcode = idcode.replace(" ", "+");
                    QRMMAS mas = new XMS017Dao().findMaster(idcode, type);
                    json = gson.toJson(mas);
                    response.getWriter().write(json);

                } else if (mode.equals("findQSHEAD")) {
                    QRMMAS mas = new XMS017Dao().findQSHEAD(gid);
                    json = gson.toJson(mas);
                    response.getWriter().write(json);

                } else if (mode.equals("checkLastQr")) {
                    boolean checkLast = new XMS017Dao().checkLastQr(id);
                    json = gson.toJson(checkLast);
                    response.getWriter().write(json);

                } else if (mode.equals("insertGenQr")) {
                    boolean insert = new XMS017Dao().insertGenQr(id);
                    json = gson.toJson(insert);
                    response.getWriter().write(json);

                } else if (mode.equals("insertQSHEAD")) {
                    String uid = request.getParameter("uid");
                    boolean insert = new XMS017Dao().insertQSHEAD(gid, wh, mat, uid);
                    json = gson.toJson(insert);
                    response.getWriter().write(json);

                } else if (mode.equals("insertQSDETAIL")) {

                    String lino = request.getParameter("lino");
                    String bfQty = request.getParameter("bfQty");
                    String afQty = request.getParameter("afQty");
                    String qty = request.getParameter("qty");
                    String unit = request.getParameter("unit");
                    String bfPack = request.getParameter("bfPack");
                    String afPack = request.getParameter("afPack");
                    String uid = request.getParameter("uid");

                    boolean insert = new XMS017Dao().insertQSDETAIL(wh, lino, mat, gid, id, bfQty, afQty, qty, unit, bfPack, afPack, uid);
                    json = gson.toJson(insert);
                    response.getWriter().write(json);

                } else if (mode.equals("updateGenQr")) {
                    boolean update = new XMS017Dao().updateGenQr(id, len);
                    json = gson.toJson(update);
                    response.getWriter().write(json);

                } else if (mode.equals("updateQNBSER")) {
                    boolean update = new XMS017Dao().updateQNBSER(wh, id);
                    json = gson.toJson(update);
                    response.getWriter().write(json);

                } else if (mode.equals("getGenQr")) {
                    String last = new XMS017Dao().getGenQr(id);
                    json = gson.toJson(last);
                    response.getWriter().write(json);

                } else if (mode.equals("getLastQNBSER")) {
                    String last = new XMS017Dao().getLastQNBSER(wh, id);
                    json = gson.toJson(last);
                    response.getWriter().write(json);

                } else if (mode.equals("getMatCtrl")) {
                    String matCtrl = new XMS017Dao().getMatCtrl(code);
                    json = gson.toJson(matCtrl);
                    response.getWriter().write(json);

                } else if (mode.equals("getDate")) {
                    String date = new XMS017Dao().getDate();
                    json = gson.toJson(date);
                    response.getWriter().write(json);

                } else if (mode.equals("getGenRoll")) {
                    String lastRoll = new XMS017Dao().getGenRoll(code);
                    json = gson.toJson(lastRoll);
                    response.getWriter().write(json);

                } else if (mode.equals("updateGenRoll")) {
                    boolean update = new XMS017Dao().updateGenRoll(code, len);
                    json = gson.toJson(update);
                    response.getWriter().write(json);

                } else if (mode.equals("insertQRMMAS")) {

                    String cdt = request.getParameter("cdt");
                    String qrmroll = request.getParameter("qrmroll");
                    String qrmqty = request.getParameter("qrmqty");
                    String qrmpacktype = request.getParameter("qrmpacktype");
                    String qrmid = request.getParameter("qrmid");
                    String uid = request.getParameter("uid");

                    boolean insert = new XMS017Dao().insertQRMMAS(id, cdt, qrmroll, qrmqty, qrmpacktype, qrmid, uid);
                    json = gson.toJson(insert);
                    response.getWriter().write(json);

                } else if (mode.equals("insertQRMTRA")) {

                    String cdt = request.getParameter("cdt");
                    String qrmroll = request.getParameter("qrmroll");
                    String qrmqty = request.getParameter("qrmqty");
                    String qrmpacktype = request.getParameter("qrmpacktype");
                    String qrmid = request.getParameter("qrmid");
                    String uid = request.getParameter("uid");

                    boolean insert = new XMS017Dao().insertQRMTRA(id, cdt, qrmroll, qrmqty, qrmpacktype, qrmid, uid);
                    json = gson.toJson(insert);
                    response.getWriter().write(json);

                } else if (mode.equals("updateParentQRMMAS")) {

                    String cdt = request.getParameter("cdt");
                    String remainQty = request.getParameter("remainQty");
                    String parentPackType = request.getParameter("parentPackType");
                    String uid = request.getParameter("uid");

                    boolean update = new XMS017Dao().updateParentQRMMAS(id, cdt, remainQty, parentPackType, uid);
                    json = gson.toJson(update);
                    response.getWriter().write(json);

                } else if (mode.equals("insertParentQRMTRA")) {

                    String cdt = request.getParameter("cdt");
                    String remainQty = request.getParameter("remainQty");
                    String parentPackType = request.getParameter("parentPackType");
                    String uid = request.getParameter("uid");

                    boolean insert = new XMS017Dao().insertParentQRMTRA(id, cdt, remainQty, parentPackType, uid);
                    json = gson.toJson(insert);
                    response.getWriter().write(json);

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
