package com.twc.wms.utils;

import com.google.gson.Gson;
import com.twc.wms.dao.ISM500Dao;
import com.twc.wms.entity.ISMBILLD;
import com.twc.wms.entity.ISMBILLH;
import com.twc.wms.entity.ISMBILLP;
import com.twc.wms.service.BAPIRET2;
import com.twc.wms.service.TABLEOFBAPIRET2;
import com.twc.wms.service.TABLEOFZBAPIBILLDATE;
import com.twc.wms.service.TABLEOFZBAPIBILLHEAD01;
import com.twc.wms.service.TABLEOFZBAPIBILLITEM01;
import com.twc.wms.service.TABLEOFZBAPIBILLNO;
import com.twc.wms.service.TABLEOFZBAPICUSTOMER;
import com.twc.wms.service.TABLEOFZBAPIPARTNERS;
import com.twc.wms.service.ZBAPIBILLHEAD01;
import com.twc.wms.service.ZBAPIBILLITEM01;
import com.twc.wms.service.ZBAPIBILLNO;
import com.twc.wms.service.ZBAPIGTMSDGETBILL01Response;
import com.twc.wms.service.ZBAPIPARTNERS;
import com.twc.wms.service.ZWSGTMSDGETBILL01;
import com.twc.wms.service.ZWSGTMSDGETBILL01Service;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.Authenticator;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author 93176
 */
public class ShowDataTablesISM500 extends HttpServlet {

    private Gson gson;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        PrintWriter out = response.getWriter();
        gson = new Gson();
        String json = null;
        Map<String, Object> m = new HashMap<String, Object>();

        String mode = request.getParameter("mode");
        String invno = request.getParameter("invno");
        String uid = request.getParameter("uid");

        if (mode != null) {
            if (mode.equals("dwnload500")) {

                System.out.println("invno " + invno);
                System.out.println("uid " + uid);

                JSONObject res = new JSONObject();

                Authenticator.setDefault(new Authenticator() {
                    @Override
                    protected java.net.PasswordAuthentication getPasswordAuthentication() {

                        Calendar c = Calendar.getInstance();
                        int month = c.get(Calendar.MONTH) + 1;

                        String m = Integer.toString(month);
                        if (m.length() < 2) {
                            m = "0" + m;
                        }

                        String pass = "display" + m;

                        return new java.net.PasswordAuthentication("tviewwien", pass.toCharArray());
                    }
                });

                //----------------- GETBILL01 -----------------
                try {

                    ZWSGTMSDGETBILL01Service service = new ZWSGTMSDGETBILL01Service();
                    ZWSGTMSDGETBILL01 getbill01 = service.getZWSGTMSDGETBILL01SoapBinding();

                    TABLEOFZBAPIBILLDATE billdate = new TABLEOFZBAPIBILLDATE();

                    TABLEOFZBAPIBILLNO billno = new TABLEOFZBAPIBILLNO();
                    ZBAPIBILLNO bill = new ZBAPIBILLNO();
                    bill.setSIGN("I");
                    bill.setOPTION("EQ");
                    bill.setVBELNLOW(invno.trim()); // invoice number from FRONT_END
                    bill.setVBELNHIGH("");
                    billno.getItem().add(bill);

                    TABLEOFZBAPIBILLHEAD01 BILL_HEADERS_OUT = new TABLEOFZBAPIBILLHEAD01();
                    TABLEOFZBAPIBILLITEM01 BILL_ITEMS_OUT = new TABLEOFZBAPIBILLITEM01();
                    TABLEOFZBAPIPARTNERS partnersOUT = new TABLEOFZBAPIPARTNERS();
                    TABLEOFBAPIRET2 _return = new TABLEOFBAPIRET2();
                    TABLEOFZBAPICUSTOMER soldto = new TABLEOFZBAPICUSTOMER();

                    String distrCHAN = "20";
                    String division = "00";
                    String docTYPE = "Z1O2";
                    String salesORG = "1000";

                    Future<?> getBILLAsync = getbill01.zBAPIGTMSDGETBILL01Async(billdate, billno, BILL_HEADERS_OUT, BILL_ITEMS_OUT, distrCHAN, division, docTYPE, partnersOUT, _return, salesORG, soldto);
                    ZBAPIGTMSDGETBILL01Response getBILLAsyncResponse = null;
                    getBILLAsyncResponse = (ZBAPIGTMSDGETBILL01Response) getBILLAsync.get();

                    TABLEOFBAPIRET2 resReturn = getBILLAsyncResponse.getRETURN();
                    for (BAPIRET2 ret : resReturn.getItem()) {
                        System.out.println("STOCKLIST_FIELD : " + ret.getFIELD());
                        System.out.println("ID : " + ret.getID());
                        System.out.println("LOGNO : " + ret.getLOGNO());
                        System.out.println("MESSAGE : " + ret.getMESSAGE());
                        System.out.println("MESSAGEV1 : " + ret.getMESSAGEV1());
                        System.out.println("MESSAGEV2 : " + ret.getMESSAGEV2());
                        System.out.println("MESSAGEV3 : " + ret.getMESSAGEV3());
                        System.out.println("MESSAGEV4 : " + ret.getMESSAGEV4());
                        System.out.println("NUMBER : " + ret.getNUMBER());
                        System.out.println("PARAMETER : " + ret.getPARAMETER());
                        System.out.println("SYSTEM : " + ret.getSYSTEM());
                        System.out.println("TYPE : " + ret.getTYPE());
                    }

                    TABLEOFZBAPIBILLHEAD01 respHeadOut01 = getBILLAsyncResponse.getBILLHEADERSOUT(); //Get Header
                    TABLEOFZBAPIBILLITEM01 respItemOut01 = getBILLAsyncResponse.getBILLITEMSOUT(); //Get Detail
                    TABLEOFZBAPIPARTNERS respPartners = getBILLAsyncResponse.getPARTNERSOUT(); //Get Partners

                    List<ISMBILLH> billHeadList = new ArrayList<ISMBILLH>();
                    List<ISMBILLD> billDetailList = new ArrayList<ISMBILLD>();
                    List<ISMBILLP> billPartnerList = new ArrayList<ISMBILLP>();

                    //insert Database
                    for (ZBAPIBILLHEAD01 h01 : respHeadOut01.getItem()) {
                        ISMBILLH Head_OBJ = new ISMBILLH();

                        Head_OBJ.setVBELN(h01.getVBELN());
                        Head_OBJ.setFKART(h01.getFKART());
                        Head_OBJ.setWAERK(h01.getWAERK());
                        Head_OBJ.setVKORG(h01.getVKORG());
                        Head_OBJ.setVTWEG(h01.getVTWEG());
                        Head_OBJ.setFKDAT(h01.getFKDAT());
                        Head_OBJ.setINCO1(h01.getINCO1());
                        Head_OBJ.setINCO2(h01.getINCO2());
                        Head_OBJ.setKURRF(h01.getKURRF().doubleValue());
                        Head_OBJ.setZTERM(h01.getZTERM());
                        Head_OBJ.setLAND1(h01.getLAND1());
                        Head_OBJ.setBUKRS(h01.getBUKRS());
                        Head_OBJ.setTAXK1(h01.getTAXK1());
                        Head_OBJ.setNETWR(h01.getNETWR().doubleValue());
                        Head_OBJ.setERNAM(h01.getERNAM());
                        Head_OBJ.setERZET(h01.getERZET());
                        Head_OBJ.setERDAT(h01.getERDAT());
                        Head_OBJ.setKUNRG(h01.getKUNRG());
                        Head_OBJ.setKUNAG(h01.getKUNAG());
                        Head_OBJ.setSPART(h01.getSPART());
                        Head_OBJ.setXBLNR(h01.getXBLNR());
                        Head_OBJ.setZUONR(h01.getZUONR());
                        Head_OBJ.setMWSBK(h01.getMWSBK().doubleValue());
                        Head_OBJ.setFKSTO(h01.getFKSTO());
                        Head_OBJ.setBUPLA(h01.getBUPLA());
                        Head_OBJ.setNAMRG(h01.getNAMRG());
                        Head_OBJ.setADRNR(h01.getADRNR());

                        billHeadList.add(Head_OBJ);

                    }

                    for (ZBAPIBILLITEM01 d01 : respItemOut01.getItem()) {
                        ISMBILLD Detail_OBJ = new ISMBILLD();

                        Detail_OBJ.setVBELN(d01.getVBELN());
                        Detail_OBJ.setPOSNR(d01.getPOSNR());
                        Detail_OBJ.setUEPOS(d01.getUEPOS());
                        Detail_OBJ.setFKIMG(d01.getFKIMG().doubleValue());
                        Detail_OBJ.setVRKME(d01.getVRKME());
                        Detail_OBJ.setUMVKZ(d01.getUMVKZ().doubleValue());
                        Detail_OBJ.setUMVKN(d01.getUMVKN().doubleValue());
                        Detail_OBJ.setMEINS(d01.getMEINS());
                        Detail_OBJ.setSMENG(d01.getSMENG().doubleValue());
                        Detail_OBJ.setFKLMG(d01.getFKLMG().doubleValue());
                        Detail_OBJ.setLMENG(d01.getLMENG().doubleValue());
                        Detail_OBJ.setNTGEW(d01.getNTGEW().doubleValue());
                        Detail_OBJ.setBRGEW(d01.getBRGEW().doubleValue());
                        Detail_OBJ.setGEWEI(d01.getGEWEI());
                        Detail_OBJ.setPRSDT(d01.getPRSDT());
                        Detail_OBJ.setFBUDA(d01.getFBUDA());
                        Detail_OBJ.setKURSK(d01.getKURSK().doubleValue());
                        Detail_OBJ.setNETWR(d01.getNETWR().doubleValue());
                        Detail_OBJ.setVBELV(d01.getVBELV());
                        Detail_OBJ.setPOSNV(d01.getPOSNV());
                        Detail_OBJ.setVGBEL(d01.getVGBEL());
                        Detail_OBJ.setVGPOS(d01.getVGPOS());
                        Detail_OBJ.setVGTYP(d01.getVGTYP());
                        Detail_OBJ.setAUBEL(d01.getAUBEL());
                        Detail_OBJ.setAUPOS(d01.getAUPOS());
                        Detail_OBJ.setAUREF(d01.getAUREF());
                        Detail_OBJ.setMATNR(d01.getMATNR());
                        Detail_OBJ.setARKTX(d01.getARKTX());
                        Detail_OBJ.setPMATN(d01.getPMATN());
                        Detail_OBJ.setCHARG(d01.getCHARG());
                        Detail_OBJ.setMATKL(d01.getMATKL());
                        Detail_OBJ.setPSTYV(d01.getPSTYV());
                        Detail_OBJ.setPOSAR(d01.getPOSAR());
                        Detail_OBJ.setPRODH(d01.getPRODH());
                        Detail_OBJ.setVSTEL(d01.getVSTEL());
                        Detail_OBJ.setSPART(d01.getSPART());
                        Detail_OBJ.setPOSPA(d01.getPOSPA());
                        Detail_OBJ.setWERKS(d01.getWERKS());
                        Detail_OBJ.setALAND(d01.getALAND());
                        Detail_OBJ.setTAXM1(d01.getTAXM1());
                        Detail_OBJ.setKTGRM(d01.getKTGRM());
                        Detail_OBJ.setVKGRP(d01.getVKGRP());
                        Detail_OBJ.setVKBUR(d01.getVKBUR());
                        Detail_OBJ.setSPARA(d01.getSPARA());
                        Detail_OBJ.setERNAM(d01.getERNAM());
                        Detail_OBJ.setERDAT(d01.getERDAT());
                        Detail_OBJ.setERZET(d01.getERZET());
                        Detail_OBJ.setBWTAR(d01.getBWTAR());
                        Detail_OBJ.setLGORT(d01.getLGORT());
                        Detail_OBJ.setWAVWR(d01.getWAVWR().doubleValue());
                        Detail_OBJ.setSTCUR(d01.getSTCUR().doubleValue());
                        Detail_OBJ.setPRCTR(d01.getPRCTR());
                        Detail_OBJ.setKVGR1(d01.getKVGR1());
                        Detail_OBJ.setKVGR2(d01.getKVGR2());
                        Detail_OBJ.setKVGR3(d01.getKVGR3());
                        Detail_OBJ.setKVGR4(d01.getKVGR4());
                        Detail_OBJ.setKVGR5(d01.getKVGR5());
                        Detail_OBJ.setBONBA(d01.getBONBA().doubleValue());
                        Detail_OBJ.setKOKRS(d01.getKOKRS());
                        Detail_OBJ.setMWSBP(d01.getMWSBP().doubleValue());
                        Detail_OBJ.setPO_NUMBER(d01.getPONUMBER());
                        Detail_OBJ.setPO_DATE(d01.getPODATE());
                        Detail_OBJ.setYOUR_REFERENCE(d01.getYOURREFERENCE());
                        Detail_OBJ.setPO_ITEM(d01.getPOITEM());
                        Detail_OBJ.setCUST_MAT(d01.getCUSTMAT());
                        Detail_OBJ.setKBETR(d01.getKBETR().doubleValue());

                        billDetailList.add(Detail_OBJ);

                    }

                    for (ZBAPIPARTNERS p01 : respPartners.getItem()) {
                        ISMBILLP Partners_OBJ = new ISMBILLP();

                        Partners_OBJ.setVBELN(p01.getVBELN());
                        Partners_OBJ.setPOSNR(p01.getPOSNR());
                        Partners_OBJ.setPARVW(p01.getPARVW());
                        Partners_OBJ.setKUNNR(p01.getKUNNR());
                        Partners_OBJ.setADRNR(p01.getADRNR());
                        Partners_OBJ.setNAME1(p01.getNAME1());
                        Partners_OBJ.setNAME2(p01.getNAME2());
                        Partners_OBJ.setNAME3(p01.getNAME3());
                        Partners_OBJ.setNAME4(p01.getNAME4());
                        Partners_OBJ.setCITY1(p01.getCITY1());
                        Partners_OBJ.setCITY2(p01.getCITY2());
                        Partners_OBJ.setPOST_CODE1(p01.getPOSTCODE1());
                        Partners_OBJ.setSTREET(p01.getSTREET());
                        Partners_OBJ.setCOUNTRY(p01.getCOUNTRY());
                        Partners_OBJ.setLANGU(p01.getLANGU());
                        Partners_OBJ.setREGION(p01.getREGION());
                        Partners_OBJ.setSORT1(p01.getSORT1());
                        Partners_OBJ.setTEL_NUMBER(p01.getTELNUMBER());
                        Partners_OBJ.setTEL_EXTENS(p01.getTELEXTENS());
                        Partners_OBJ.setFAX_NUMBER(p01.getFAXNUMBER());
                        Partners_OBJ.setFAX_EXTENS(p01.getFAXEXTENS());
                        Partners_OBJ.setEXTENSION1(p01.getEXTENSION1());
                        Partners_OBJ.setEXTENSION2(p01.getEXTENSION2());

                        billPartnerList.add(Partners_OBJ);

                    }

                    System.out.println("size " + billHeadList.size());
                    System.out.println("size " + billDetailList.size());
                    System.out.println("size " + billPartnerList.size());

                    boolean resInsH = new ISM500Dao().insBillHead(billHeadList, uid);
                    boolean resInsD = new ISM500Dao().insBillDetail(billDetailList, uid);
                    boolean resInsP = new ISM500Dao().insBillPartners(billPartnerList, uid);
//                    System.out.println("resInsH " + resInsH);
//                    System.out.println("resInsD " + resInsD);
//                    System.out.println("resInsP " + resInsP);

                    res.put("resInsH", resInsH);
                    res.put("resInsD", resInsD);
                    res.put("resInsP", resInsP);

                } catch (InterruptedException ex) {
                    Logger.getLogger(ShowDataTablesISM500.class
                            .getName()).log(Level.SEVERE, null, ex);

                } catch (ExecutionException ex) {
                    Logger.getLogger(ShowDataTablesISM500.class
                            .getName()).log(Level.SEVERE, null, ex);
                }
                //----------------- GETBILL01 ----------------- END
//                List<ISMMASH> headList = new ISM100Dao().getHead(vel, pdg);
                json = gson.toJson(res);

            } else if (mode.equals("dwnload510")) {

                JSONObject res = new JSONObject();

                String invto = request.getParameter("invto");

                //get invoice list
                List<ISMBILLD> invList = new ISM500Dao().CheckDetailData510(invno, invto);

                if (!invList.isEmpty()) {

                    Authenticator.setDefault(new Authenticator() {
                        @Override
                        protected java.net.PasswordAuthentication getPasswordAuthentication() {

                            Calendar c = Calendar.getInstance();
                            int month = c.get(Calendar.MONTH) + 1;

                            String m = Integer.toString(month);
                            if (m.length() < 2) {
                                m = "0" + m;
                            }

                            String pass = "display" + m;

                            return new java.net.PasswordAuthentication("tviewwien", pass.toCharArray());
                        }
                    });

                    //----------------- GETBILL01 -----------------
                    try {

                        List<ISMBILLH> billHeadList = new ArrayList<ISMBILLH>();
                        List<ISMBILLD> billDetailList = new ArrayList<ISMBILLD>();
                        List<ISMBILLP> billPartnerList = new ArrayList<ISMBILLP>();

                        for (int i = 0; i < invList.size(); i++) {

//                            System.out.println(invList.get(i).getVBELN());
                            ZWSGTMSDGETBILL01Service service = new ZWSGTMSDGETBILL01Service();
                            ZWSGTMSDGETBILL01 getbill01 = service.getZWSGTMSDGETBILL01SoapBinding();

                            TABLEOFZBAPIBILLDATE billdate = new TABLEOFZBAPIBILLDATE();

                            TABLEOFZBAPIBILLNO billno = new TABLEOFZBAPIBILLNO();
                            ZBAPIBILLNO bill = new ZBAPIBILLNO();
                            bill.setSIGN("I");
                            bill.setOPTION("EQ");
                            bill.setVBELNLOW(invList.get(i).getVBELN().trim()); // invoice number from FRONT_END
                            bill.setVBELNHIGH("");
                            billno.getItem().add(bill);

                            TABLEOFZBAPIBILLHEAD01 BILL_HEADERS_OUT = new TABLEOFZBAPIBILLHEAD01();
                            TABLEOFZBAPIBILLITEM01 BILL_ITEMS_OUT = new TABLEOFZBAPIBILLITEM01();
                            TABLEOFZBAPIPARTNERS partnersOUT = new TABLEOFZBAPIPARTNERS();
                            TABLEOFBAPIRET2 _return = new TABLEOFBAPIRET2();
                            TABLEOFZBAPICUSTOMER soldto = new TABLEOFZBAPICUSTOMER();

                            String distrCHAN = "20";
                            String division = "00";
                            String docTYPE = "Z1O2";
                            String salesORG = "1000";

                            Future<?> getBILLAsync = getbill01.zBAPIGTMSDGETBILL01Async(billdate, billno, BILL_HEADERS_OUT, BILL_ITEMS_OUT, distrCHAN, division, docTYPE, partnersOUT, _return, salesORG, soldto);
                            ZBAPIGTMSDGETBILL01Response getBILLAsyncResponse = null;
                            getBILLAsyncResponse = (ZBAPIGTMSDGETBILL01Response) getBILLAsync.get();

                            TABLEOFBAPIRET2 resReturn = getBILLAsyncResponse.getRETURN();
                            for (BAPIRET2 ret : resReturn.getItem()) {
                                System.out.println("STOCKLIST_FIELD : " + ret.getFIELD());
                                System.out.println("ID : " + ret.getID());
                                System.out.println("LOGNO : " + ret.getLOGNO());
                                System.out.println("MESSAGE : " + ret.getMESSAGE());
                                System.out.println("MESSAGEV1 : " + ret.getMESSAGEV1());
                                System.out.println("MESSAGEV2 : " + ret.getMESSAGEV2());
                                System.out.println("MESSAGEV3 : " + ret.getMESSAGEV3());
                                System.out.println("MESSAGEV4 : " + ret.getMESSAGEV4());
                                System.out.println("NUMBER : " + ret.getNUMBER());
                                System.out.println("PARAMETER : " + ret.getPARAMETER());
                                System.out.println("SYSTEM : " + ret.getSYSTEM());
                                System.out.println("TYPE : " + ret.getTYPE());
                            }

                            TABLEOFZBAPIBILLHEAD01 respHeadOut01 = getBILLAsyncResponse.getBILLHEADERSOUT(); //Get Header
                            TABLEOFZBAPIBILLITEM01 respItemOut01 = getBILLAsyncResponse.getBILLITEMSOUT(); //Get Detail
                            TABLEOFZBAPIPARTNERS respPartners = getBILLAsyncResponse.getPARTNERSOUT(); //Get Partners

                            //insert Database
                            for (ZBAPIBILLHEAD01 h01 : respHeadOut01.getItem()) {
                                ISMBILLH Head_OBJ = new ISMBILLH();

                                Head_OBJ.setVBELN(h01.getVBELN());
                                Head_OBJ.setFKART(h01.getFKART());
                                Head_OBJ.setWAERK(h01.getWAERK());
                                Head_OBJ.setVKORG(h01.getVKORG());
                                Head_OBJ.setVTWEG(h01.getVTWEG());
                                Head_OBJ.setFKDAT(h01.getFKDAT());
                                Head_OBJ.setINCO1(h01.getINCO1());
                                Head_OBJ.setINCO2(h01.getINCO2());
                                Head_OBJ.setKURRF(h01.getKURRF().doubleValue());
                                Head_OBJ.setZTERM(h01.getZTERM());
                                Head_OBJ.setLAND1(h01.getLAND1());
                                Head_OBJ.setBUKRS(h01.getBUKRS());
                                Head_OBJ.setTAXK1(h01.getTAXK1());
                                Head_OBJ.setNETWR(h01.getNETWR().doubleValue());
                                Head_OBJ.setERNAM(h01.getERNAM());
                                Head_OBJ.setERZET(h01.getERZET());
                                Head_OBJ.setERDAT(h01.getERDAT());
                                Head_OBJ.setKUNRG(h01.getKUNRG());
                                Head_OBJ.setKUNAG(h01.getKUNAG());
                                Head_OBJ.setSPART(h01.getSPART());
                                Head_OBJ.setXBLNR(h01.getXBLNR());
                                Head_OBJ.setZUONR(h01.getZUONR());
                                Head_OBJ.setMWSBK(h01.getMWSBK().doubleValue());
                                Head_OBJ.setFKSTO(h01.getFKSTO());
                                Head_OBJ.setBUPLA(h01.getBUPLA());
                                Head_OBJ.setNAMRG(h01.getNAMRG());
                                Head_OBJ.setADRNR(h01.getADRNR());

                                billHeadList.add(Head_OBJ);

                            }

                            for (ZBAPIBILLITEM01 d01 : respItemOut01.getItem()) {
                                ISMBILLD Detail_OBJ = new ISMBILLD();

                                Detail_OBJ.setVBELN(d01.getVBELN());
                                Detail_OBJ.setPOSNR(d01.getPOSNR());
                                Detail_OBJ.setUEPOS(d01.getUEPOS());
                                Detail_OBJ.setFKIMG(d01.getFKIMG().doubleValue());
                                Detail_OBJ.setVRKME(d01.getVRKME());
                                Detail_OBJ.setUMVKZ(d01.getUMVKZ().doubleValue());
                                Detail_OBJ.setUMVKN(d01.getUMVKN().doubleValue());
                                Detail_OBJ.setMEINS(d01.getMEINS());
                                Detail_OBJ.setSMENG(d01.getSMENG().doubleValue());
                                Detail_OBJ.setFKLMG(d01.getFKLMG().doubleValue());
                                Detail_OBJ.setLMENG(d01.getLMENG().doubleValue());
                                Detail_OBJ.setNTGEW(d01.getNTGEW().doubleValue());
                                Detail_OBJ.setBRGEW(d01.getBRGEW().doubleValue());
                                Detail_OBJ.setGEWEI(d01.getGEWEI());
                                Detail_OBJ.setPRSDT(d01.getPRSDT());
                                Detail_OBJ.setFBUDA(d01.getFBUDA());
                                Detail_OBJ.setKURSK(d01.getKURSK().doubleValue());
                                Detail_OBJ.setNETWR(d01.getNETWR().doubleValue());
                                Detail_OBJ.setVBELV(d01.getVBELV());
                                Detail_OBJ.setPOSNV(d01.getPOSNV());
                                Detail_OBJ.setVGBEL(d01.getVGBEL());
                                Detail_OBJ.setVGPOS(d01.getVGPOS());
                                Detail_OBJ.setVGTYP(d01.getVGTYP());
                                Detail_OBJ.setAUBEL(d01.getAUBEL());
                                Detail_OBJ.setAUPOS(d01.getAUPOS());
                                Detail_OBJ.setAUREF(d01.getAUREF());
                                Detail_OBJ.setMATNR(d01.getMATNR());
                                Detail_OBJ.setARKTX(d01.getARKTX());
                                Detail_OBJ.setPMATN(d01.getPMATN());
                                Detail_OBJ.setCHARG(d01.getCHARG());
                                Detail_OBJ.setMATKL(d01.getMATKL());
                                Detail_OBJ.setPSTYV(d01.getPSTYV());
                                Detail_OBJ.setPOSAR(d01.getPOSAR());
                                Detail_OBJ.setPRODH(d01.getPRODH());
                                Detail_OBJ.setVSTEL(d01.getVSTEL());
                                Detail_OBJ.setSPART(d01.getSPART());
                                Detail_OBJ.setPOSPA(d01.getPOSPA());
                                Detail_OBJ.setWERKS(d01.getWERKS());
                                Detail_OBJ.setALAND(d01.getALAND());
                                Detail_OBJ.setTAXM1(d01.getTAXM1());
                                Detail_OBJ.setKTGRM(d01.getKTGRM());
                                Detail_OBJ.setVKGRP(d01.getVKGRP());
                                Detail_OBJ.setVKBUR(d01.getVKBUR());
                                Detail_OBJ.setSPARA(d01.getSPARA());
                                Detail_OBJ.setERNAM(d01.getERNAM());
                                Detail_OBJ.setERDAT(d01.getERDAT());
                                Detail_OBJ.setERZET(d01.getERZET());
                                Detail_OBJ.setBWTAR(d01.getBWTAR());
                                Detail_OBJ.setLGORT(d01.getLGORT());
                                Detail_OBJ.setWAVWR(d01.getWAVWR().doubleValue());
                                Detail_OBJ.setSTCUR(d01.getSTCUR().doubleValue());
                                Detail_OBJ.setPRCTR(d01.getPRCTR());
                                Detail_OBJ.setKVGR1(d01.getKVGR1());
                                Detail_OBJ.setKVGR2(d01.getKVGR2());
                                Detail_OBJ.setKVGR3(d01.getKVGR3());
                                Detail_OBJ.setKVGR4(d01.getKVGR4());
                                Detail_OBJ.setKVGR5(d01.getKVGR5());
                                Detail_OBJ.setBONBA(d01.getBONBA().doubleValue());
                                Detail_OBJ.setKOKRS(d01.getKOKRS());
                                Detail_OBJ.setMWSBP(d01.getMWSBP().doubleValue());
                                Detail_OBJ.setPO_NUMBER(d01.getPONUMBER());
                                Detail_OBJ.setPO_DATE(d01.getPODATE());
                                Detail_OBJ.setYOUR_REFERENCE(d01.getYOURREFERENCE());
                                Detail_OBJ.setPO_ITEM(d01.getPOITEM());
                                Detail_OBJ.setCUST_MAT(d01.getCUSTMAT());
                                Detail_OBJ.setKBETR(d01.getKBETR().doubleValue());

                                billDetailList.add(Detail_OBJ);

                            }

                            for (ZBAPIPARTNERS p01 : respPartners.getItem()) {
                                ISMBILLP Partners_OBJ = new ISMBILLP();

                                Partners_OBJ.setVBELN(p01.getVBELN());
                                Partners_OBJ.setPOSNR(p01.getPOSNR());
                                Partners_OBJ.setPARVW(p01.getPARVW());
                                Partners_OBJ.setKUNNR(p01.getKUNNR());
                                Partners_OBJ.setADRNR(p01.getADRNR());
                                Partners_OBJ.setNAME1(p01.getNAME1());
                                Partners_OBJ.setNAME2(p01.getNAME2());
                                Partners_OBJ.setNAME3(p01.getNAME3());
                                Partners_OBJ.setNAME4(p01.getNAME4());
                                Partners_OBJ.setCITY1(p01.getCITY1());
                                Partners_OBJ.setCITY2(p01.getCITY2());
                                Partners_OBJ.setPOST_CODE1(p01.getPOSTCODE1());
                                Partners_OBJ.setSTREET(p01.getSTREET());
                                Partners_OBJ.setCOUNTRY(p01.getCOUNTRY());
                                Partners_OBJ.setLANGU(p01.getLANGU());
                                Partners_OBJ.setREGION(p01.getREGION());
                                Partners_OBJ.setSORT1(p01.getSORT1());
                                Partners_OBJ.setTEL_NUMBER(p01.getTELNUMBER());
                                Partners_OBJ.setTEL_EXTENS(p01.getTELEXTENS());
                                Partners_OBJ.setFAX_NUMBER(p01.getFAXNUMBER());
                                Partners_OBJ.setFAX_EXTENS(p01.getFAXEXTENS());
                                Partners_OBJ.setEXTENSION1(p01.getEXTENSION1());
                                Partners_OBJ.setEXTENSION2(p01.getEXTENSION2());

                                billPartnerList.add(Partners_OBJ);

                            }

                            System.out.println("size " + billHeadList.size());
                            System.out.println("size " + billDetailList.size());
                            System.out.println("size " + billPartnerList.size());

                        }

                        boolean resInsH = new ISM500Dao().insBillHead(billHeadList, uid);
                        boolean resInsD = new ISM500Dao().insBillDetail(billDetailList, uid);
                        boolean resInsP = new ISM500Dao().insBillPartners(billPartnerList, uid);

                        res.put("resInsH", resInsH);
                        res.put("resInsD", resInsD);
                        res.put("resInsP", resInsP);

                    } catch (InterruptedException ex) {
                        Logger.getLogger(ShowDataTablesISM500.class
                                .getName()).log(Level.SEVERE, null, ex);

                    } catch (ExecutionException ex) {
                        Logger.getLogger(ShowDataTablesISM500.class
                                .getName()).log(Level.SEVERE, null, ex);
                    }
                    //----------------- GETBILL01 ----------------- END

                } else {

                    res.put("resInsH", false);
                    res.put("resInsD", false);
                    res.put("resInsP", false);

                }

                json = gson.toJson(res);

            } else if (mode.equals("getdataHead")) {

                List<ISMBILLH> resData = new ISM500Dao().HeadData(invno);
                m.put("resData", resData);
                json = this.gson.toJson(m);

            } else if (mode.equals("getdataHead510")) {

                String invto = request.getParameter("invto");

                List<ISMBILLH> resData = new ISM500Dao().HeadData510(invno, invto);
                m.put("resData", resData);
                json = this.gson.toJson(m);

            } else if (mode.equals("getdataDetail")) {

                List<ISMBILLD> toViewData = new ArrayList<ISMBILLD>();

                List<ISMBILLD> resData = new ISM500Dao().DetailData(invno);

                Double sumReal = 0.00;
                Double sumFKLMG = 0.00;
                Double sumKBETR = 0.00;
                Double sumBONBA = 0.00;
                Double sumMWSBP = 0.00;

                DecimalFormat formatter = new DecimalFormat("#,###.00");

                for (int i = 0; i < resData.size(); i++) {

                    String VBELN = resData.get(i).getVBELN();
                    String MATNR = resData.get(i).getMATNR();
                    String ARKTX = resData.get(i).getARKTX();
                    Double LMENG = resData.get(i).getLMENG();
                    String ISDETM = resData.get(i).getISDETM();
                    String VGBEL = resData.get(i).getVGBEL();
                    Double FKLMG = resData.get(i).getFKLMG();
                    Double KBETR = resData.get(i).getKBETR();
                    Double BONBA = resData.get(i).getBONBA();
                    Double MWSBP = resData.get(i).getMWSBP();

                    ISMBILLD billDObj1 = new ISMBILLD();
                    ISMBILLD billDObj2 = new ISMBILLD();
                    ISMBILLD billDObj3 = new ISMBILLD();
                    ISMBILLD billDObjVat = new ISMBILLD();
                    ISMBILLD billDObjTotal = new ISMBILLD();

                    sumFKLMG += FKLMG;
                    sumKBETR += KBETR;
                    sumBONBA += BONBA;
                    sumMWSBP += MWSBP;
                    sumReal += Double.parseDouble(ISDETM);

                    if (i == 0) { //first time

                        billDObj1.setVBELN(VBELN);
                        billDObj1.setMATNR(MATNR);
                        billDObj1.setARKTX(ARKTX);
                        billDObj1.setLMENG(LMENG);
                        billDObj1.setISDETM(ISDETM);
                        billDObj1.setVGBEL(VGBEL);
                        billDObj1.setFKLMG(FKLMG);
                        billDObj1.setKBETR(KBETR);
                        billDObj1.setBONBA(BONBA);

                        toViewData.add(billDObj1);

                    } else { // next time

                        if (i == resData.size() - 1) { //last time

                            billDObj1.setVBELN(VBELN);
                            billDObj1.setMATNR(MATNR);
                            billDObj1.setARKTX(ARKTX);
                            billDObj1.setLMENG(LMENG);
                            billDObj1.setISDETM(ISDETM);
                            billDObj1.setVGBEL(VGBEL);
                            billDObj1.setFKLMG(FKLMG);
                            billDObj1.setKBETR(KBETR);
                            billDObj1.setBONBA(BONBA);

                            toViewData.add(billDObj1);

//                            if (!VGBEL.equals(resData.get(i - 1).getVGBEL())) {
                            billDObj2.setVBELN("");
                            billDObj2.setMATNR("");
                            billDObj2.setARKTX("Delivery No. : " + VGBEL);
                            billDObj2.setLMENG(0.00);
                            billDObj2.setISDETM("");
                            billDObj2.setVGBEL("");
                            billDObj2.setFKLMG(0.00);
                            billDObj2.setKBETR(0.00);
                            billDObj2.setBONBA(0.00);

                            toViewData.add(billDObj2);
//                            }

                            billDObj3.setVBELN("");
                            billDObj3.setMATNR("");
                            billDObj3.setARKTX("รวม ");
                            billDObj3.setLMENG(0.00);
                            billDObj3.setISDETM(sumReal.toString());
                            billDObj3.setVGBEL("");
                            billDObj3.setFKLMG(sumFKLMG);
                            billDObj3.setKBETR(sumKBETR);
                            billDObj3.setBONBA(sumBONBA);

                            toViewData.add(billDObj3);

                            billDObjVat.setVBELN("");
                            billDObjVat.setMATNR("");
                            billDObjVat.setARKTX("ภาษีมูลค่าเพิ่ม ");
                            billDObjVat.setLMENG(0.00);
                            billDObjVat.setISDETM("");
                            billDObjVat.setVGBEL("");
                            billDObjVat.setFKLMG(0.00);
                            billDObjVat.setKBETR(0.00);
                            billDObjVat.setBONBA(sumMWSBP);

                            toViewData.add(billDObjVat);

                            billDObjTotal.setVBELN("");
                            billDObjTotal.setMATNR("");
                            billDObjTotal.setARKTX("รวมทั้งสิ้น ");
                            billDObjTotal.setLMENG(0.00);
                            billDObjTotal.setISDETM("");
                            billDObjTotal.setVGBEL("");
                            billDObjTotal.setFKLMG(0.00);
                            billDObjTotal.setKBETR(0.00);
                            billDObjTotal.setBONBA(sumMWSBP + sumBONBA);

                            toViewData.add(billDObjTotal);

                        } else { //

                            if (!VGBEL.equals(resData.get(i - 1).getVGBEL())) {

                                billDObj2.setVBELN("");
                                billDObj2.setMATNR("");
                                billDObj2.setARKTX("Delivery No. : " + VGBEL);
                                billDObj2.setLMENG(0.00);
                                billDObj2.setISDETM("");
                                billDObj2.setVGBEL("");
                                billDObj2.setFKLMG(sumFKLMG);
                                billDObj2.setKBETR(sumKBETR);
                                billDObj2.setBONBA(sumBONBA);

                                toViewData.add(billDObj2);
                            }

                            billDObj1.setVBELN(VBELN);
                            billDObj1.setMATNR(MATNR);
                            billDObj1.setARKTX(ARKTX);
                            billDObj1.setLMENG(LMENG);
                            billDObj1.setISDETM(ISDETM);
                            billDObj1.setVGBEL(VGBEL);
                            billDObj1.setFKLMG(FKLMG);
                            billDObj1.setKBETR(KBETR);
                            billDObj1.setBONBA(BONBA);

                            toViewData.add(billDObj1);

                        }
                    }
                }

//                for (int i = 0; i < toViewData.size(); i++) {
//                    System.out.println(toViewData.get(i).getMATNR());
//                    System.out.println(toViewData.get(i).getARKTX());
//                    System.out.println(toViewData.get(i).getLMENG());
//                    System.out.println(toViewData.get(i).getISDETM());
//                    System.out.println(toViewData.get(i).getVGBEL());
//                    System.out.println(toViewData.get(i).getFKLMG());
//                    System.out.println(toViewData.get(i).getKBETR());
//                    System.out.println(toViewData.get(i).getBONBA());
//                }
                m.put("resData", toViewData);
                json = this.gson.toJson(m);

            } else if (mode.equals("getdataDetail510")) {

                String invto = request.getParameter("invto");

                List<ISMBILLD> toViewData = new ArrayList<ISMBILLD>();

                List<ISMBILLD> resData = new ISM500Dao().DetailData510(invno, invto);

                Double sumReal = 0.00;
                Double sumFKLMG = 0.00;
                Double sumQTY = 0.00;
                Double sumLMENG = 0.00;
                Double sumBERGJING = 0.00;
                Double sumTotalQTYby = 0.00;
                Double sumTotalReal = 0.00;

//                Double sumBONBA = 0.00;
//                Double sumMWSBP = 0.00;
                DecimalFormat formatter = new DecimalFormat("#,###.00");

                for (int i = 0; i < resData.size(); i++) {

                    ISMBILLD billDObjH = new ISMBILLD();
                    ISMBILLD billDObjHT = new ISMBILLD();
                    ISMBILLD billDObjD = new ISMBILLD();
                    ISMBILLD billDObj3 = new ISMBILLD();
                    ISMBILLD billDObjLast = new ISMBILLD();

                    String CUST_MAT = resData.get(i).getCUST_MAT();

                    int cus_code = Integer.parseInt(resData.get(i).getCUST_MAT());

                    if (CUST_MAT.equals("0000210026")) {
                        CUST_MAT = "WLC";
                    } else if (CUST_MAT.equals("0000210027")) {
                        CUST_MAT = "PKC";
                    } else if (CUST_MAT.equals("0000210012")) {
                        CUST_MAT = "ICC";
                    }

                    String VBELN = resData.get(i).getVBELN();
                    String MATNR = resData.get(i).getMATNR();
                    String ARKTX = resData.get(i).getARKTX();
                    Double LMENG = resData.get(i).getLMENG();
                    String ISDETM = resData.get(i).getISDETM();
                    String VGBEL = resData.get(i).getVGBEL();
                    Double FKLMG = resData.get(i).getFKLMG();
                    Double KBETR = resData.get(i).getKBETR();
                    Double BONBA = resData.get(i).getBONBA();
                    String NAMRG = resData.get(i).getCUST_CODE();
                    String SALESORDER = resData.get(i).getSALESORDER();

                    sumFKLMG += FKLMG;
                    sumReal += FKLMG;
                    sumTotalQTYby += LMENG;
//                    sumTotalReal += LMENG;
                    sumTotalReal += Double.parseDouble(ISDETM);

                    if (i == 0) { //first

                        sumQTY += FKLMG;
                        sumLMENG += LMENG;
                        sumBERGJING += Double.parseDouble(ISDETM);

                        //Head
                        billDObjH.setCUST_MAT(cus_code + " : " + CUST_MAT + " : " + NAMRG);
                        billDObjH.setVBELN(VBELN);
                        billDObjH.setMATNR(MATNR);
                        billDObjH.setARKTX(ARKTX);
                        billDObjH.setLMENG(KBETR);
                        billDObjH.setISDETM(ISDETM);
                        billDObjH.setVGBEL(VGBEL);
                        billDObjH.setFKLMG(BONBA);
                        toViewData.add(billDObjH);

                        //Head Table
                        billDObjHT.setCUST_MAT(MATNR);
                        billDObjHT.setVBELN("");
                        billDObjHT.setMATNR("");
                        billDObjHT.setARKTX("inv no.");
                        billDObjHT.setLMENG(0.00);
                        billDObjHT.setISDETM("");
                        billDObjHT.setVGBEL("");
                        billDObjHT.setFKLMG(0.00);
                        toViewData.add(billDObjHT);

                        //Detail
                        billDObjD.setCUST_MAT(MATNR);
                        billDObjD.setVBELN("");
                        billDObjD.setMATNR("detail");
                        billDObjD.setARKTX(VBELN);
                        billDObjD.setLMENG(LMENG);
                        billDObjD.setISDETM("");
                        billDObjD.setVGBEL(VGBEL);
                        billDObjD.setFKLMG(FKLMG);
                        billDObjD.setSALESORDER(SALESORDER);
                        toViewData.add(billDObjD);

                    } else if (i == resData.size() - 1) { //last

                        //---------------------------
                        if (resData.get(i).getCUST_MAT().equals(resData.get(i - 1).getCUST_MAT())) { //same customer

                            sumQTY += FKLMG;
                            sumLMENG += LMENG;
                            sumBERGJING += Double.parseDouble(ISDETM);

                            if (MATNR.equals(resData.get(i - 1).getMATNR())) { //same matcode

                                //Detail
                                billDObjD.setCUST_MAT(MATNR);
                                billDObjD.setVBELN("");
                                billDObjD.setMATNR("detail");
                                billDObjD.setARKTX(VBELN);
                                billDObjD.setLMENG(LMENG);
                                billDObjD.setISDETM("");
                                billDObjD.setVGBEL(VGBEL);
                                billDObjD.setFKLMG(FKLMG);
                                billDObjD.setSALESORDER(SALESORDER);
                                toViewData.add(billDObjD);

                            } else { //not same matcode

                                sumFKLMG = 0.0;
                                sumFKLMG += FKLMG;

                                //Head
                                billDObjH.setCUST_MAT("");
//                                billDObjH.setCUST_MAT(cus_code + " : " + CUST_MAT + " : " + NAMRG);
                                billDObjH.setVBELN(VBELN);
                                billDObjH.setMATNR(MATNR);
                                billDObjH.setARKTX(ARKTX);
                                billDObjH.setLMENG(KBETR);
                                billDObjH.setISDETM(ISDETM);
                                billDObjH.setVGBEL(VGBEL);
                                billDObjH.setFKLMG(BONBA);
                                toViewData.add(billDObjH);

                                //Head Table
                                billDObjHT.setCUST_MAT(MATNR);
                                billDObjHT.setVBELN("");
                                billDObjHT.setMATNR("");
                                billDObjHT.setARKTX("inv no.");
                                billDObjHT.setLMENG(0.00);
                                billDObjHT.setISDETM("");
                                billDObjHT.setVGBEL("");
                                billDObjHT.setFKLMG(0.00);
                                toViewData.add(billDObjHT);

                                //Detail
                                billDObjD.setCUST_MAT(MATNR);
                                billDObjD.setVBELN("");
                                billDObjD.setMATNR("detail");
                                billDObjD.setARKTX(VBELN);
                                billDObjD.setLMENG(LMENG);
                                billDObjD.setISDETM("");
                                billDObjD.setVGBEL(VGBEL);
                                billDObjD.setFKLMG(sumFKLMG);
                                billDObjD.setSALESORDER(SALESORDER);
                                toViewData.add(billDObjD);
                            }
                        } else { //not same
                            sumFKLMG = 0.0;
                            sumFKLMG += FKLMG;

                            //Head
                            billDObjH.setCUST_MAT(cus_code + " : " + CUST_MAT + " : " + NAMRG);
                            billDObjH.setVBELN(VBELN);
                            billDObjH.setMATNR(MATNR);
                            billDObjH.setARKTX(ARKTX);
                            billDObjH.setLMENG(KBETR);
                            billDObjH.setISDETM(ISDETM);
                            billDObjH.setVGBEL(VGBEL);
                            billDObjH.setFKLMG(BONBA);
                            toViewData.add(billDObjH);

                            //Head Table
                            billDObjHT.setCUST_MAT(MATNR);
                            billDObjHT.setVBELN("");
                            billDObjHT.setMATNR("");
                            billDObjHT.setARKTX("inv no.");
                            billDObjHT.setLMENG(0.00);
                            billDObjHT.setISDETM("");
                            billDObjHT.setVGBEL("");
                            billDObjHT.setFKLMG(0.00);
                            toViewData.add(billDObjHT);

                            //Detail
                            billDObjD.setCUST_MAT(MATNR);
                            billDObjD.setVBELN("");
                            billDObjD.setMATNR("detail");
                            billDObjD.setARKTX(VBELN);
                            billDObjD.setLMENG(LMENG);
                            billDObjD.setISDETM("");
                            billDObjD.setVGBEL(VGBEL);
                            billDObjD.setFKLMG(sumFKLMG);
                            billDObjD.setSALESORDER(SALESORDER);
                            toViewData.add(billDObjD);
                        }
                        //---------------------------

                        //sum last line
                        billDObj3.setCUST_MAT("");
                        billDObj3.setVBELN("");
                        billDObj3.setMATNR("");
                        billDObj3.setARKTX("รวม ");
                        billDObj3.setLMENG(sumLMENG);
                        billDObj3.setISDETM(sumBERGJING.toString());
                        billDObj3.setVGBEL("");
                        billDObj3.setFKLMG(sumQTY);
                        toViewData.add(billDObj3);

                        //Sum total
                        billDObjLast.setCUST_MAT("");
                        billDObjLast.setVBELN("");
                        billDObjLast.setMATNR("");
                        billDObjLast.setARKTX("รวมทั้งหมด ");
                        billDObjLast.setLMENG(sumTotalQTYby);
                        billDObjLast.setISDETM(sumTotalReal.toString());
                        billDObjLast.setVGBEL("");
                        billDObjLast.setFKLMG(sumReal);
                        toViewData.add(billDObjLast);

                    } else { //next

                        if (resData.get(i).getCUST_MAT().equals(resData.get(i - 1).getCUST_MAT())) { //same customer

                            sumQTY += FKLMG;
                            sumLMENG += LMENG;
                            sumBERGJING += Double.parseDouble(ISDETM);

                            if (MATNR.equals(resData.get(i - 1).getMATNR())) { //same matcode

                                //Detail
                                billDObjD.setCUST_MAT(MATNR);
                                billDObjD.setVBELN("");
                                billDObjD.setMATNR("detail");
                                billDObjD.setARKTX(VBELN);
                                billDObjD.setLMENG(LMENG);
                                billDObjD.setISDETM("");
                                billDObjD.setVGBEL(VGBEL);
                                billDObjD.setFKLMG(FKLMG);
                                billDObjD.setSALESORDER(SALESORDER);
                                toViewData.add(billDObjD);

                            } else { //not same matcode

                                sumFKLMG = 0.0;
                                sumFKLMG += FKLMG;

                                //Head
//                                billDObjH.setCUST_MAT(cus_code + " : " + CUST_MAT + " : " + NAMRG);
                                billDObjH.setCUST_MAT("");
                                billDObjH.setVBELN(VBELN);
                                billDObjH.setMATNR(MATNR);
                                billDObjH.setARKTX(ARKTX);
                                billDObjH.setLMENG(KBETR);
                                billDObjH.setISDETM(ISDETM);
                                billDObjH.setVGBEL(VGBEL);
                                billDObjH.setFKLMG(BONBA);
                                toViewData.add(billDObjH);

                                //Head Table
                                billDObjHT.setCUST_MAT(MATNR);
                                billDObjHT.setVBELN("");
                                billDObjHT.setMATNR("");
                                billDObjHT.setARKTX("inv no.");
                                billDObjHT.setLMENG(0.00);
                                billDObjHT.setISDETM("");
                                billDObjHT.setVGBEL("");
                                billDObjHT.setFKLMG(0.00);
                                toViewData.add(billDObjHT);

                                //Detail
                                billDObjD.setCUST_MAT(MATNR);
                                billDObjD.setVBELN("");
                                billDObjD.setMATNR("detail");
                                billDObjD.setARKTX(VBELN);
                                billDObjD.setLMENG(LMENG);
                                billDObjD.setISDETM("");
                                billDObjD.setVGBEL(VGBEL);
                                billDObjD.setFKLMG(sumFKLMG);
                                billDObjD.setSALESORDER(SALESORDER);
                                toViewData.add(billDObjD);
                            }
                        } else { //not same

                            //sum by customer                  
                            billDObj3.setCUST_MAT("");
                            billDObj3.setVBELN("");
                            billDObj3.setMATNR("");
                            billDObj3.setARKTX("รวม "); //if need customer code use i - 1
                            billDObj3.setLMENG(sumLMENG);
                            billDObj3.setISDETM(sumBERGJING.toString());
                            billDObj3.setVGBEL("");
                            billDObj3.setFKLMG(sumQTY);
                            toViewData.add(billDObj3);

                            sumQTY = 0.0;
                            sumQTY += FKLMG;

                            sumLMENG = 0.0;
                            sumLMENG += LMENG;

                            sumBERGJING = 0.0;
                            sumBERGJING += Double.parseDouble(ISDETM);

                            sumFKLMG = 0.0;
                            sumFKLMG += FKLMG;

                            //Head
                            billDObjH.setCUST_MAT(cus_code + " : " + CUST_MAT + " : " + NAMRG);
                            billDObjH.setVBELN(VBELN);
                            billDObjH.setMATNR(MATNR);
                            billDObjH.setARKTX(ARKTX);
                            billDObjH.setLMENG(KBETR);
                            billDObjH.setISDETM(ISDETM);
                            billDObjH.setVGBEL(VGBEL);
                            billDObjH.setFKLMG(BONBA);
                            toViewData.add(billDObjH);

                            //Head Table
                            billDObjHT.setCUST_MAT(MATNR);
                            billDObjHT.setVBELN("");
                            billDObjHT.setMATNR("");
                            billDObjHT.setARKTX("inv no.");
                            billDObjHT.setLMENG(0.00);
                            billDObjHT.setISDETM("");
                            billDObjHT.setVGBEL("");
                            billDObjHT.setFKLMG(0.00);
                            toViewData.add(billDObjHT);

                            //Detail
                            billDObjD.setCUST_MAT(MATNR);
                            billDObjD.setVBELN("");
                            billDObjD.setMATNR("detail");
                            billDObjD.setARKTX(VBELN);
                            billDObjD.setLMENG(LMENG);
                            billDObjD.setISDETM("");
                            billDObjD.setVGBEL(VGBEL);
                            billDObjD.setFKLMG(sumFKLMG);
                            billDObjD.setSALESORDER(SALESORDER);
                            toViewData.add(billDObjD);
                        }

                    }

                }

                m.put("resData", toViewData);
                json = this.gson.toJson(m);

            } else if (mode.equals("CheckDetail")) {

                List<ISMBILLD> resData = new ISM500Dao().CheckDetailData(invno);

                if (!resData.isEmpty()) { //found
                    json = this.gson.toJson(true);
                } else { //not found
                    json = this.gson.toJson(false);
                }

//                m.put("resData", toViewData);
            } else if (mode.equals("CheckDetailData510")) {

                String invto = request.getParameter("invto");

                List<ISMBILLD> resData = new ISM500Dao().CheckDetailData510(invno, invto);

                if (!resData.isEmpty()) { //found
                    json = this.gson.toJson(true);
                } else { //not found
                    json = this.gson.toJson(false);
                }

//                m.put("resData", toViewData);
            } else if (mode.equals("DelHDP")) {

                JSONObject resJson = new ISM500Dao().DelHDP(invno);
                json = this.gson.toJson(resJson);

            } else if (mode.equals("DelHDP510")) {

                String invto = request.getParameter("invto");

                JSONObject res = new JSONObject();
                boolean Hbool = true;
                boolean Dbool = true;
                boolean Pbool = true;

                //get invoice list
                List<ISMBILLD> invList = new ISM500Dao().CheckDetailData510(invno, invto);

                if (!invList.isEmpty()) {
                    for (int i = 0; i < invList.size(); i++) {
                        JSONObject resJson = new ISM500Dao().DelHDP(invList.get(i).getVBELN().trim());

                        if (resJson.getBoolean("resDelH") == false) {
                            Hbool = false;
                        }
                        if (resJson.getBoolean("resDelD") == false) {
                            Dbool = false;
                        }
                        if (resJson.getBoolean("resDelP") == false) {
                            Pbool = false;
                        }
                    }
                } else {
                    Hbool = false;
                    Dbool = false;
                    Pbool = false;
                }

                res.put("resDelH", Hbool);
                res.put("resDelD", Dbool);
                res.put("resDelP", Pbool);

                json = this.gson.toJson(res);

            }

            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
            response.getWriter().write(json);
        }

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }

}
