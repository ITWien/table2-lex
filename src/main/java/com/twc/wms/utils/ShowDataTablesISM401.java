/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package com.twc.wms.utils;

import com.google.gson.Gson;
import com.twc.wms.dao.ISM401Dao;
import com.twc.wms.entity.ISMMASH;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author 93176
 */
public class ShowDataTablesISM401 extends HttpServlet {

    private Gson gson;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        PrintWriter out = response.getWriter();
        gson = new Gson();
        String json = null;
        Map<String, Object> m = new HashMap<String, Object>();

        String mode = request.getParameter("mode");
        String cusno = request.getParameter("cusno");
        String sales = request.getParameter("sales"); //Sales Order

        if (mode != null) {
            if (mode.equals("getCustomer")) {
                List<ISMMASH> resList = new ISM401Dao().CustomerData();
                json = this.gson.toJson(resList);
            } else if (mode.equals("getSalesNumbers")) {
                List<ISMMASH> resList = new ISM401Dao().SalesOrderDataByCus(cusno);
                json = this.gson.toJson(resList);
            } else if (mode.equals("getSeq")) {

                String salesf = request.getParameter("salesf");
                String salest = request.getParameter("salest");

                List<ISMMASH> resList = new ISM401Dao().SeqDataByCusnSales(cusno, salesf, salest);
                json = this.gson.toJson(resList);
            }
        }

        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(json);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

}
