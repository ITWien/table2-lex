/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package com.twc.wms.utils;

import com.google.gson.Gson;
import com.twc.wms.dao.ISM910Dao;
import com.twc.wms.entity.ISMMASD;
import com.twc.wms.entity.ISMMASH;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author 93176
 */
public class ShowDataTablesISM910 extends HttpServlet {

    private Gson gson;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");

        PrintWriter out = response.getWriter();
        gson = new Gson();
        String json = null;

        String mode = request.getParameter("mode");
        String orderNo = request.getParameter("ord");
        String param = request.getParameter("param");
        String sorter = request.getParameter("sorter");

        try {

            if (mode != null) {
                if (mode.equals("getDet")) {

                    String toDeep = "";

                    if (sorter.equals("ord")) {
                        toDeep = " WHERE ISDORD = ? ";
                    } else if (sorter.equals("cus")) {
                        toDeep = " WHERE ISDORD IN (SELECT DISTINCT ISHORD FROM [RMShipment].[dbo].[ISMMASH] WHERE ISHCUNO = ?) ";
                    } else if (sorter.equals("doc")) {
                        toDeep = " WHERE ISDORD IN (SELECT DISTINCT ISHORD FROM [RMShipment].[dbo].[ISMMASH] WHERE ISHSUBMI = ?) ";
                    } else if (sorter.equals("sty")) {
                        toDeep = " WHERE ISDORD IN (SELECT DISTINCT ISHORD FROM [RMShipment].[dbo].[ISMMASH] WHERE ISHSTYLE+ISHCOLOR = ?) ";
                    } else if (sorter.equals("lot")) {
                        toDeep = " WHERE ISDORD IN (SELECT DISTINCT ISHORD FROM [RMShipment].[dbo].[ISMMASH] WHERE ISHLOT = ?) ";
                    }

                    List<ISMMASD> detailList = new ISM910Dao().GetMasDet(param, toDeep);

                    List<ISMMASD> outlist = new ArrayList<ISMMASD>();

                    for (int i = 0; i < detailList.size(); i++) {

                        ISMMASD p1 = new ISMMASD();

                        if (i == 0) {

                            p1.setISDCONO(detailList.get(i).getISDCONO());
                            p1.setISDORD(detailList.get(i).getISDORD());
                            p1.setISDSEQ(detailList.get(i).getISDSEQ());
                            p1.setISDLINO(detailList.get(i).getISDLINO());
                            p1.setISDSINO(detailList.get(i).getISDSINO());
                            p1.setISDITNO(detailList.get(i).getISDITNO());
                            p1.setISDPLANT(detailList.get(i).getISDPLANT());
                            p1.setISDVT(detailList.get(i).getISDVT());
                            p1.setISDSTRG(detailList.get(i).getISDSTRG());
                            p1.setISDRQQTY(detailList.get(i).getISDRQQTY());
                            p1.setISDUNIT(detailList.get(i).getISDUNIT());
                            p1.setISDPRICE(detailList.get(i).getISDPRICE());
                            p1.setISDUNR01(detailList.get(i).getISDUNR01());
                            p1.setISDUNR03(detailList.get(i).getISDUNR03());
                            p1.setISDMTCTRL(detailList.get(i).getISDMTCTRL());
                            p1.setISDDEST(detailList.get(i).getISDDEST());
                            p1.setISDRQDT(detailList.get(i).getISDRQDT());
                            p1.setISDRQTM(detailList.get(i).getISDRQTM());
                            p1.setISDAPDT(detailList.get(i).getISDAPDT());
                            p1.setISDAPTM(detailList.get(i).getISDAPTM());
                            p1.setISDAPUSER(detailList.get(i).getISDAPUSER());
                            p1.setISDRCDT(detailList.get(i).getISDRCDT());
                            p1.setISDPKQTY(detailList.get(i).getISDPKQTY());
                            p1.setISDPKDT(detailList.get(i).getISDPKDT());
                            p1.setISDPKTM(detailList.get(i).getISDPKTM());
                            p1.setISDPKUSER(detailList.get(i).getISDPKUSER());
                            p1.setISDTPQTY(detailList.get(i).getISDTPQTY());
                            p1.setISDTPDT(detailList.get(i).getISDTPDT());
                            p1.setISDTPTM(detailList.get(i).getISDTPTM());
                            p1.setISDTPUSER(detailList.get(i).getISDTPUSER());
                            p1.setISDSTS(detailList.get(i).getISDSTS());
                            p1.setISDJBNO(detailList.get(i).getISDJBNO());
                            p1.setISDQNO(detailList.get(i).getISDQNO());
                            p1.setISDREMARK(detailList.get(i).getISDREMARK());
                            p1.setISDISUNO(detailList.get(i).getISDISUNO());
                            p1.setISDTEMP(detailList.get(i).getISDTEMP());
                            p1.setISDEDT(detailList.get(i).getISDEDT());
                            p1.setISDETM(detailList.get(i).getISDETM());
                            p1.setISDEUSR(detailList.get(i).getISDEUSR());
                            p1.setISDCDT(detailList.get(i).getISDCDT());
                            p1.setISDCTM(detailList.get(i).getISDCTM());
                            p1.setISDUSER(detailList.get(i).getISDUSER());
                            p1.setISDSTAD(detailList.get(i).getISDSTAD());
                            p1.setISDSTSTMP(detailList.get(i).getISDSTSTMP());
                            p1.setISDREMAIN(detailList.get(i).getISDREMAIN());
                            p1.setISDDESC(detailList.get(i).getISDDESC());
                            p1.setFAM(detailList.get(i).getFAM());
                            p1.setSTSI(detailList.get(i).getSTSI());

                            outlist.add(p1);

                        } else {

                            p1.setISDCONO(detailList.get(i).getISDCONO());

                            if (detailList.get(i).getISDORD().equals(detailList.get(i - 1).getISDORD())) {
                                p1.setISDORD("");
                            } else {
                                p1.setISDORD(detailList.get(i).getISDORD());
                            }

                            p1.setISDSEQ(detailList.get(i).getISDSEQ());
                            p1.setISDLINO(detailList.get(i).getISDLINO());
                            p1.setISDSINO(detailList.get(i).getISDSINO());
                            p1.setISDITNO(detailList.get(i).getISDITNO());
                            p1.setISDPLANT(detailList.get(i).getISDPLANT());
                            p1.setISDVT(detailList.get(i).getISDVT());
                            p1.setISDSTRG(detailList.get(i).getISDSTRG());
                            p1.setISDRQQTY(detailList.get(i).getISDRQQTY());
                            p1.setISDUNIT(detailList.get(i).getISDUNIT());
                            p1.setISDPRICE(detailList.get(i).getISDPRICE());
                            p1.setISDUNR01(detailList.get(i).getISDUNR01());
                            p1.setISDUNR03(detailList.get(i).getISDUNR03());
                            p1.setISDMTCTRL(detailList.get(i).getISDMTCTRL());
                            p1.setISDDEST(detailList.get(i).getISDDEST());
                            p1.setISDRQDT(detailList.get(i).getISDRQDT());
                            p1.setISDRQTM(detailList.get(i).getISDRQTM());
                            p1.setISDAPDT(detailList.get(i).getISDAPDT());
                            p1.setISDAPTM(detailList.get(i).getISDAPTM());
                            p1.setISDAPUSER(detailList.get(i).getISDAPUSER());
                            p1.setISDRCDT(detailList.get(i).getISDRCDT());
                            p1.setISDPKQTY(detailList.get(i).getISDPKQTY());
                            p1.setISDPKDT(detailList.get(i).getISDPKDT());
                            p1.setISDPKTM(detailList.get(i).getISDPKTM());
                            p1.setISDPKUSER(detailList.get(i).getISDPKUSER());
                            p1.setISDTPQTY(detailList.get(i).getISDTPQTY());
                            p1.setISDTPDT(detailList.get(i).getISDTPDT());
                            p1.setISDTPTM(detailList.get(i).getISDTPTM());
                            p1.setISDTPUSER(detailList.get(i).getISDTPUSER());
                            p1.setISDSTS(detailList.get(i).getISDSTS());
                            p1.setISDJBNO(detailList.get(i).getISDJBNO());
                            p1.setISDQNO(detailList.get(i).getISDQNO());
                            p1.setISDREMARK(detailList.get(i).getISDREMARK());
                            p1.setISDISUNO(detailList.get(i).getISDISUNO());
                            p1.setISDTEMP(detailList.get(i).getISDTEMP());
                            p1.setISDEDT(detailList.get(i).getISDEDT());
                            p1.setISDETM(detailList.get(i).getISDETM());
                            p1.setISDEUSR(detailList.get(i).getISDEUSR());
                            p1.setISDCDT(detailList.get(i).getISDCDT());
                            p1.setISDCTM(detailList.get(i).getISDCTM());
                            p1.setISDUSER(detailList.get(i).getISDUSER());
                            p1.setISDSTAD(detailList.get(i).getISDSTAD());
                            p1.setISDSTSTMP(detailList.get(i).getISDSTSTMP());
                            p1.setISDREMAIN(detailList.get(i).getISDREMAIN());
                            p1.setISDDESC(detailList.get(i).getISDDESC());
                            p1.setFAM(detailList.get(i).getFAM());
                            p1.setSTSI(detailList.get(i).getSTSI());

                            outlist.add(p1);

                        }
                    }

                    json = gson.toJson(outlist);
                    response.getWriter().write(json);

                } else if (mode.equals("getHead")) {

                    String toDeep = "";

                    if (sorter.equals("ord")) {
                        toDeep = " WHERE ISHORD = ? ";
                    } else if (sorter.equals("cus")) {
                        toDeep = " WHERE ISHORD IN (SELECT DISTINCT ISHORD FROM [RMShipment].[dbo].[ISMMASH] WHERE ISHCUNO = ?) ";
                    } else if (sorter.equals("doc")) {
                        toDeep = " WHERE ISHORD IN (SELECT DISTINCT ISHORD FROM [RMShipment].[dbo].[ISMMASH] WHERE ISHSUBMI = ?) ";
                    } else if (sorter.equals("sty")) {
                        toDeep = " WHERE ISHORD IN (SELECT DISTINCT ISHORD FROM [RMShipment].[dbo].[ISMMASH] WHERE ISHSTYLE+ISHCOLOR = ?) ";
                    } else if (sorter.equals("lot")) {
                        toDeep = " WHERE ISHORD IN (SELECT DISTINCT ISHORD FROM [RMShipment].[dbo].[ISMMASH] WHERE ISHLOT = ?) ";
                    }

                    List<ISMMASH> headList = new ISM910Dao().getHead(param, toDeep);

                    json = gson.toJson(headList);
                    response.getWriter().write(json);
                } else if (mode.equals("getDistinctSalesH")) {

                    List<ISMMASH> headList = new ISM910Dao().getDisSalesH();

                    json = gson.toJson(headList);
                    response.getWriter().write(json);

                } else if (mode.equals("getDistinctCusNoH")) {

                    List<ISMMASH> headList = new ISM910Dao().getDisCusNoH();

                    json = gson.toJson(headList);
                    response.getWriter().write(json);

                } else if (mode.equals("getDistinctDocNoH")) {

                    List<ISMMASH> headList = new ISM910Dao().getDisDocNoH();

                    json = gson.toJson(headList);
                    response.getWriter().write(json);

                } else if (mode.equals("getDistinctStyFGH")) {

                    List<ISMMASH> headList = new ISM910Dao().getDisStyFGH();

                    json = gson.toJson(headList);
                    response.getWriter().write(json);

                } else if (mode.equals("getDistinctLotH")) {

                    List<ISMMASH> headList = new ISM910Dao().getDisLotH();

                    json = gson.toJson(headList);
                    response.getWriter().write(json);

                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
