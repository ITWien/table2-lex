/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.utils;

import com.twc.wms.dao.ISM300Dao;
import com.twc.wms.dao.MASDDAO;
import com.twc.wms.dao.MASHDAO;
import com.twc.wms.dao.MASMDAO;
import com.twc.wms.dao.MATNDAO;
import com.twc.wms.dao.PACKDAO;
import com.twc.wms.dao.TOPKDAO;
import com.twc.wms.entity.MPDATAD;
import com.twc.wms.entity.MPDATAP;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author nutthawoot.noo
 */
public class ISM300AJAX extends HttpServlet {

    private static final long serialVersionUID = 102831973239L;
    private MASHDAO MASHDAO;
    private MASDDAO MASDDAO;
    private MASMDAO MASMDAO;
    private MATNDAO MATNDAO;
    private TOPKDAO TOPKDAO;
    private PACKDAO PACKDAO;

    public ISM300AJAX() {
        super();
        MASHDAO = new MASHDAO();
        MASDDAO = new MASDDAO();
        MASMDAO = new MASMDAO();
        MATNDAO = new MATNDAO();
        TOPKDAO = new TOPKDAO();
        PACKDAO = new PACKDAO();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String action = "";
        String text = "";

        if (request.getParameter("a") != null) {
            action = request.getParameter("a");
        }

        if (action.equalsIgnoreCase("update")) {

            String GRPNO = request.getParameter("GRPNO");
            int ISSUENO = Integer.parseInt(request.getParameter("ISSUENO"));
            String MATNR = request.getParameter("MATNR");
            float ISSUEPICK = Float.parseFloat(request.getParameter("ISSUEPICK"));
            float KWMENG = Float.parseFloat(request.getParameter("KWMENG"));

            Calendar cal = Calendar.getInstance();

            TOPKDAO.addMSSTAPK(GRPNO, ISSUENO, MATNR, ISSUEPICK, cal.getTime());

            if (TOPKDAO.checkDuplicate(GRPNO, ISSUENO, MATNR)) {
                float ISSUETOPICK = TOPKDAO.getISSUETOPICK(GRPNO, ISSUENO, MATNR);
                TOPKDAO.update(GRPNO, ISSUENO, MATNR, ISSUETOPICK + ISSUEPICK);
            } else {
                TOPKDAO.addMSSTOPK(GRPNO, ISSUENO, MATNR, ISSUEPICK);
            }

            text = new DecimalFormat("#,###,##0.000").format(TOPKDAO.getISSUETOPICK(GRPNO, ISSUENO, MATNR)) + "|" + new DecimalFormat("#,###,##0.000").format(KWMENG - TOPKDAO.getISSUETOPICK(GRPNO, ISSUENO, MATNR));

        } else if (action.equalsIgnoreCase("add")) {      // add packing by scan mod --new

            String GRPNO = request.getParameter("GRPNO");
            int ISSUENO = Integer.parseInt(request.getParameter("ISSUENO"));
            String MATNR = request.getParameter("MATNR");

            //float ISSUEPICK = Float.parseFloat(request.getParameter("ISSUEPICK"));
            float ISSUEPICK = Float.parseFloat(request.getParameter("VALUE"));
            float KWMENG = Float.parseFloat(request.getParameter("VALUE"));
            //float KWMENG = Float.parseFloat(request.getParameter("KWMENG"));

            String TYPE = request.getParameter("TYPE");
            int UNIT = Integer.parseInt(request.getParameter("UNIT"));
            float VALUE = Float.parseFloat(request.getParameter("VALUE"));
            String MTCTRL = request.getParameter("MTCTRL");
            String IDITEM = request.getParameter("IDITEM");
            String TYPBOX = request.getParameter("TYPBOX");
            String Statusqr = "";

            if ((IDITEM.substring(0, 1)).equals("G")) {
                IDITEM = PACKDAO.getqriditem(IDITEM.trim());   // get iditem with barcode
            }

            Statusqr = PACKDAO.getqrSts(IDITEM);  	 // check status qrmas equasl=2

            //for (int i = 0; i < UNIT; i++) {
            if (Statusqr.equals("2")) {

                // add update first
                Calendar cal = Calendar.getInstance();

                TOPKDAO.addMSSTAPK(GRPNO, ISSUENO, MATNR, ISSUEPICK, cal.getTime());

                if (TOPKDAO.checkDuplicate(GRPNO, ISSUENO, MATNR)) {
                    float ISSUETOPICK = TOPKDAO.getISSUETOPICK(GRPNO, ISSUENO, MATNR);
                    TOPKDAO.update(GRPNO, ISSUENO, MATNR, ISSUETOPICK + ISSUEPICK);
                } else {
                    TOPKDAO.addMSSTOPK(GRPNO, ISSUENO, MATNR, ISSUEPICK);
                }

                text = new DecimalFormat("#,###,##0.000").format(TOPKDAO.getISSUETOPICK(GRPNO, ISSUENO, MATNR)) + "|" + new DecimalFormat("#,###,##0.000").format(KWMENG - TOPKDAO.getISSUETOPICK(GRPNO, ISSUENO, MATNR));
                /// add update new

                if (TYPBOX.equals("") || TYPBOX == null) {  //check box
                    TYPBOX = PACKDAO.getTypepack(IDITEM, MATNR);
                }

                new ISM300Dao().UPDPick(GRPNO, ISSUENO, MATNR, MTCTRL, VALUE); //update PKQTY in ISMMASD

                PACKDAO.addMSSPACK(GRPNO, ISSUENO, MATNR, PACKDAO.getNo(GRPNO, ISSUENO, MATNR) + 1, TYPBOX, VALUE, MTCTRL, IDITEM);  //new mod
                //PACKDAO.updateHeadpack(GRPNO, ISSUENO, MATNR,VALUE);  // update head  new mod

                //update status  new
//                PACKDAO.upqrstatusID(IDITEM, "3");lexcom
                text = "null";
            } else {
                text = "ไม่พบรหัสใน Plant 1000 กรุณาตรวจสอบ หรือสถานะไม่พร้อมใช้งาน";

            }
            //}

        } else if (action.equalsIgnoreCase("delete")) {
            String GRPNO = request.getParameter("GRPNO");
            int ISSUENO = Integer.parseInt(request.getParameter("ISSUENO"));
            String MATNR = request.getParameter("MATNR");
            String LGPBE = request.getParameter("LGPBE");

            new ISM300Dao().DELAllPick(GRPNO, ISSUENO, MATNR, LGPBE); //update PKQTY to 0 in ISMMASD
            //  UP SATUS QRCODE
//            PACKDAO.upqrstsGroup(GRPNO, ISSUENO, MATNR, "2");lexcom
            PACKDAO.updateHeadpackGroup(GRPNO, ISSUENO, MATNR);
            //  UP SATUS QRCODE

            PACKDAO.deleteMSSPACK(GRPNO, ISSUENO, MATNR);
            PACKDAO.deleteMSSPACD(GRPNO, ISSUENO, MATNR);

            text = "success";
        } else if (action.equalsIgnoreCase("deleteline")) {
            String GRPNO = request.getParameter("GRPNO");

            String LGPBE = request.getParameter("LGPBE");

            int ISSUENO = Integer.parseInt(request.getParameter("ISSUENO"));
            int LINENO = Integer.parseInt(request.getParameter("LINENO"));

            //	System.out.println(request.getParameter("ISSUENO")+" line="+request.getParameter("LINENO"));
            String MATNRid = request.getParameter("MATNRid");
            String MATNR = request.getParameter("MATNR");
            float QTYID = Float.parseFloat(request.getParameter("QTYID"));

            new ISM300Dao().DELLinePick(GRPNO, ISSUENO, MATNR, LGPBE, QTYID); //update PKQTY to 0 in ISMMASD

            //	System.out.println(request.getParameter("QTYID"));
            //  UP SATUS QRCODE
//            PACKDAO.upqrstsLine(GRPNO, ISSUENO, MATNRid, "2");lexcom
            PACKDAO.updateHeadpackLine(GRPNO, ISSUENO, QTYID, MATNR);
            //  UP SATUS QRCODE

            PACKDAO.deleteMSSPACKLine(GRPNO, ISSUENO, MATNRid);
            PACKDAO.deleteMSSPACDLine(GRPNO, ISSUENO, MATNR, LINENO);

            text = "success";
        } else if (action.equalsIgnoreCase("packdetail")) {
            String GRPNO = request.getParameter("GRPNO");
            int ISSUENO = Integer.parseInt(request.getParameter("ISSUENO"));
            String MATNR = request.getParameter("MATNR");

            MPDATAP MPDATAP = new MPDATAP();
            MPDATAD MPDATAD = new MPDATAD();

            PACKDAO.deleteMSSPACD(GRPNO, ISSUENO, MATNR);

            List<MPDATAD> listDetailForClear = MASDDAO.getDetailForClear(GRPNO, ISSUENO, MATNR);
            for (int a = 0; a < listDetailForClear.size(); a++) {
                MPDATAD = listDetailForClear.get(a);
                MASDDAO.updateTemp(0, MPDATAD.getVBELN(), MPDATAD.getPOSNR(), MPDATAD.getMATNR());
            }

            List<MPDATAP> listPack = PACKDAO.getList(GRPNO, ISSUENO, MATNR);
            for (int i = 0; i < listPack.size(); i++) {
                MPDATAP = listPack.get(i);
                float tempVALUE = MPDATAP.getVALUE();
                List<MPDATAD> listDetailForPack = MASDDAO.getDetailForPack(GRPNO, ISSUENO, MATNR);
                for (int j = 0; j < listDetailForPack.size(); j++) {
                    MPDATAD = listDetailForPack.get(j);
                    float calTotal = MPDATAD.getKWMENG() - MPDATAD.getTEMP();

                    if (tempVALUE <= calTotal) {
                        MASDDAO.updateTemp(tempVALUE + MPDATAD.getTEMP(), MPDATAD.getVBELN(), MPDATAD.getPOSNR(), MPDATAD.getMATNR());
                        PACKDAO.addMSSPACD(GRPNO, ISSUENO, MPDATAD.getVBELN(), MPDATAD.getPOSNR(), MPDATAD.getMATNR(), MPDATAP.getNO(), MPDATAP.getTYPE(), tempVALUE);
                        break;
                    } else {

                        if (j == listDetailForPack.size() - 1 && i == listPack.size() - 1) {
                            MASDDAO.updateTemp(tempVALUE + MPDATAD.getTEMP(), MPDATAD.getVBELN(), MPDATAD.getPOSNR(), MPDATAD.getMATNR());
                            PACKDAO.addMSSPACD(GRPNO, ISSUENO, MPDATAD.getVBELN(), MPDATAD.getPOSNR(), MPDATAD.getMATNR(), MPDATAP.getNO(), MPDATAP.getTYPE(), tempVALUE);
                        } else {
                            MASDDAO.updateTemp(calTotal + MPDATAD.getTEMP(), MPDATAD.getVBELN(), MPDATAD.getPOSNR(), MPDATAD.getMATNR());
                            PACKDAO.addMSSPACD(GRPNO, ISSUENO, MPDATAD.getVBELN(), MPDATAD.getPOSNR(), MPDATAD.getMATNR(), MPDATAP.getNO(), MPDATAP.getTYPE(), calTotal);
                        }

                        tempVALUE = tempVALUE - calTotal;
                    }
                }
            }
        } else if (action.equalsIgnoreCase("getCustomerName")) {
            String POFG = request.getParameter("POFG");
            text = MASHDAO.getCustomerName(POFG);
        } else if (action.equalsIgnoreCase("getMatName")) {
            String LGPBE = request.getParameter("LGPBE");
            text = MATNDAO.getMatName(LGPBE);
        } else if (action.equalsIgnoreCase("getDropDownGroup")) {
            String POFG = request.getParameter("POFG");
            String[] textArr = MASHDAO.getDropDownGroup(POFG);
            if (textArr != null) {
                text = "<select id=\"sGRPNO\" class=\"form-control\">";
                for (int i = 0; i < textArr.length; i++) {
                    text = text + "<option value=\"" + textArr[i] + "\">" + textArr[i] + "</option>";
                }
                text = text + "</select>";
            } else {
                text = "null";
            }
        } else if (action.equalsIgnoreCase("getDropDownSQ")) {
            String POFG = request.getParameter("POFG");
            String[] textArr = MASDDAO.getDropDownSQ(POFG);
            if (textArr != null) {
                text = "<select id=\"sISSUENO\" class=\"form-control\">";
                for (int i = 0; i < textArr.length; i++) {
                    String[] textsplit = textArr[i].split("\\|");
                    text = text + "<option value=\"" + textsplit[0] + "\">" + textsplit[1] + "</option>";
                }
                text = text + "</select>";
            } else {
                text = "null";
            }
        } else if (action.equalsIgnoreCase("getwarehouse")) {
            String POFG = request.getParameter("POFG");
            String[] textArr = MASDDAO.getWareHouse(POFG);

            HttpSession session = request.getSession();
            String warehouse = (String) session.getAttribute("MSWHSE");
            if (warehouse == null) {
                warehouse = "";
            }
            //System.out.println("ware="+warehouse);

            if (textArr != null) {
                String chkwarehouse = "";
                text = "<select id=\"sWAREHOUSE\" class=\"form-control\">";
                text = text + "<option value=''></option>";
                for (int i = 0; i < textArr.length; i++) {
                    String[] textsplit = textArr[i].split("\\|");
                    if (warehouse.equals("" + textsplit[0].trim() + "")) {
                        chkwarehouse = " selected ";
                        //System.out.println("ware="+warehouse);
                    } else {
                        chkwarehouse = " ";
                    }

                    text = text + "<option value=\"" + textsplit[0] + "\"" + chkwarehouse + ">" + textsplit[0] + ":" + textsplit[1] + "</option>";
                }
                text = text + "</select>";
            } else {
                text = "null";
            }

        } else if (action.equalsIgnoreCase("300BackWard")) {

            String jsonText = request.getParameter("jsonText");
            String jsonText2 = request.getParameter("jsonText2");

            JSONObject resC = null;
            JSONObject resD = null;

            boolean delPack = true;
            boolean delStat = true;
            boolean delTapk = true;
            boolean delPacd = true;
            boolean delTopk = true;

            JSONArray jsonArr = new JSONArray(jsonText);
            JSONArray jsonArr2 = new JSONArray(jsonText2);

            text = "alertify.success('Backward Success.')";

            if (jsonArr.length() > 0) {

                String customer = jsonArr.get(0).toString();
                String matctrl = jsonArr.get(1).toString();
                String jobno = jsonArr.get(2).toString();
                String seq = jsonArr.get(3).toString();

                if (jsonArr2.length() > 0) {
                    for (int j = 0; j < jsonArr2.length(); j++) {

                        //delete matcode
                        resC = new ISM300Dao().changeSTSForBackward(customer, matctrl, jobno, seq, jsonArr2.get(j).toString());
                        resD = new ISM300Dao().delForBackward(customer, matctrl, jobno, seq, jsonArr2.get(j).toString());

                        if (resC == null) { //null
                            text = "alertify.error('Backward Failed.')";
                        } else {
                            if (resC.getBoolean("upD") == false) { //false
                                text = "alertify.error('Backward Failed.')";
                            }
                        }

                        if (resD.getBoolean("delPack") == false) {
                            delPack = false;
                        }
                        if (resD.getBoolean("delStat") == false) {
                            delStat = false;
                        }
                        if (resD.getBoolean("delTapk") == false) {
                            delTapk = false;
                        }
                        if (resD.getBoolean("delPacd") == false) {
                            delPacd = false;
                        }
                        if (resD.getBoolean("delTopk") == false) {
                            delTopk = false;
                        }

                    }
                }
            }

            //delPack
            //delStat
            //delTapk
            //delPacd
            //delTopk 
            if (resD != null) {
                if (delPack) {
                    if (delStat) {
                        if (delTapk) {
                            if (delPacd) {
                                if (delTopk) {
                                    text += " alertify.success('Delete Success.') ";
                                } else {
                                    text += " alertify.error('Delete Failed.')";
                                }
                            } else {
                                text += " alertify.error('Delete Failed.')";
                            }
                        } else {
                            text += " alertify.error('Delete Failed.')";
                        }
                    } else {
                        text += " alertify.error('Delete Failed.')";
                    }
                } else {
                    text += " alertify.error('Delete Failed.')";
                }
            } else {
                text += " alertify.error('Delete Failed.')";
            }

        }

        response.setContentType("text/plain");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(text);

    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String select[] = request.getParameterValues("nCheck");

        for (int i = 0; i < select.length; i++) {

            if (select[i].length() > 1) {

                int status;

                if (select[i].substring(0, 1).equals("0") || select[i].substring(0, 1).equals("1")) { // ?????? ???
                    status = 1;
                } else if (select[i].substring(0, 1).equals("2")) {
                    status = 3;
                } else {
                    status = 0;
                }

                String[] data = select[i].split("\\|");   /// 2|MWC16/18|1|1FCV0028CR}

                MASDDAO.updateStatusTmp(data[1], data[2], data[3], 1);

                if (MASMDAO.checkDuplicateStatus(data[1], data[2], data[3]) == false) {
                    MASMDAO.addStatus(data[1], data[2], data[3], status);
                } else {
                    int STATUS = MASMDAO.checkStatus(data[1], data[2], data[3]);
                    if ((STATUS == 0 || STATUS == 9) && status == 1) {
                        MASMDAO.updateStatus(data[1], data[2], data[3], status);
                    } else if (STATUS == 2 && status == 3) {
                        MASMDAO.updateStatus(data[1], data[2], data[3], status);
                    }
                }
            }

        }

        response.setContentType("text/plain");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write("0");

    }

}
