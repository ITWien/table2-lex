/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.utils;

import com.google.gson.Gson;
import com.twc.wms.dao.ISM100Dao;
import com.twc.wms.entity.ISMMASD;
import com.twc.wms.entity.ISMMASH;
import com.twc.wms.entity.WH;
import java.io.IOException;
import java.io.PrintWriter;
//import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author nutthawoot.noo
 */
public class ShowDataTablesISM100 extends HttpServlet {

    private Gson gson;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        PrintWriter out = response.getWriter();
        gson = new Gson();
        String json = null;

        String mode = request.getParameter("mode");
        String orderNo = request.getParameter("orderNo");
        String regDate = request.getParameter("regDate");
        String dest = request.getParameter("dest");
        String wh = request.getParameter("wh");
        String remark = request.getParameter("remark");
        String lino = request.getParameter("lino");
        String sino = request.getParameter("sino");
        String qtyOld = request.getParameter("qtyOld");
        String qtyNew = request.getParameter("qtyNew");
        String qtyRem = request.getParameter("qtyRem");
        String uid = request.getParameter("uid");
        String level = request.getParameter("level");
        String pdg = request.getParameter("pdg");
        String seq = request.getParameter("seq");

        try {
            if (mode != null) {
                if (mode.equals("getHead")) {

                    String vel = new ISM100Dao().getLevel(uid);
                    List<ISMMASH> headList = new ISM100Dao().getHead(vel, pdg);

                    json = gson.toJson(headList);
                    response.getWriter().write(json);

                } else if (mode.equals("getDetail")) {

                    List<ISMMASD> detailList = new ISM100Dao().getDetail(orderNo, seq);

                    json = gson.toJson(detailList);
                    response.getWriter().write(json);

                } else if (mode.equals("updateReqDate")) {

//                    System.out.println("seq " + seq);
                    boolean update = new ISM100Dao().updateReqDate(orderNo, regDate, uid, seq);
                    boolean updateAllDet = new ISM100Dao().updateReqDateAllDet(orderNo, regDate, uid, seq);

                    json = gson.toJson(update);
                    response.getWriter().write(json);

                } else if (mode.equals("updateReqDateDetail")) {

                    boolean updateHead = new ISM100Dao().updateReqDate(orderNo, regDate, uid, seq); //Copy updateReqDate function
                    boolean update = new ISM100Dao().updateReqDateDetail(orderNo, lino, regDate, uid, seq);

                    json = gson.toJson(update);
                    response.getWriter().write(json);

                } else if (mode.equals("updateReqApp")) {

                    boolean update = new ISM100Dao().updateReqApp(orderNo, uid, seq);
                    boolean updateAllDet = new ISM100Dao().updateReqAppAllDet(orderNo, uid, seq);

                    json = gson.toJson(update);
                    response.getWriter().write(json);

                } else if (mode.equals("updateReqAppDetail")) {

//                    System.out.println("app" + seq);
                    boolean update = new ISM100Dao().updateReqAppDetail(orderNo, lino, uid, seq);
                    boolean updateSts = new ISM100Dao().updateSTS(orderNo, seq);

                    json = gson.toJson(update);
                    response.getWriter().write(json);

                } else if (mode.equals("updateRemarkCancel")) {

                    boolean update = new ISM100Dao().updateRemarkCancel(orderNo, new String(remark.getBytes("iso-8859-1"), "UTF-8"), uid, seq);
                    boolean updateAllDet = new ISM100Dao().updateRemarkCancelAllDet(orderNo, new String(remark.getBytes("iso-8859-1"), "UTF-8"), uid, seq);

                    json = gson.toJson(update);
                    response.getWriter().write(json);

                } else if (mode.equals("updateRemarkCancelDetail")) {

                    boolean update = new ISM100Dao().updateRemarkCancelDetail(orderNo, lino, new String(remark.getBytes("iso-8859-1"), "UTF-8"));
                    boolean updateSts = new ISM100Dao().updateSTS(orderNo, seq);

                    json = gson.toJson(update);
                    response.getWriter().write(json);

                } else if (mode.equals("updateDest")) {

                    boolean update = new ISM100Dao().updateDest(orderNo, dest, uid, seq);
                    boolean updateAllDet = new ISM100Dao().updateDestAllDet(orderNo, dest, uid, seq);

                    json = gson.toJson(updateAllDet);
                    response.getWriter().write(json);

                } else if (mode.equals("updateDestDetail")) {

//                    boolean updateH = new ISM100Dao().updateDest(orderNo, dest, uid, seq);
                    boolean update = new ISM100Dao().updateDestDetail(orderNo, lino, dest, seq, uid);

                    json = gson.toJson(update);
                    response.getWriter().write(json);

                } else if (mode.equals("getWH")) {

                    List<WH> whList = new ISM100Dao().findWH(uid);

                    json = gson.toJson(whList);
                    response.getWriter().write(json);

                } else if (mode.equals("getDest")) {

                    List<WH> whList = new ISM100Dao().findDest(wh);

                    json = gson.toJson(whList);
                    response.getWriter().write(json);

                } else if (mode.equals("getProductGroup")) {

                    List<WH> pdList = new ISM100Dao().findProductGroup(uid);

                    json = gson.toJson(pdList);
                    response.getWriter().write(json);

                } else if (mode.equals("splitDetail")) {

                    sino = lino.split("-")[1];

                    boolean addRem = new ISM100Dao().addSplitDetail(orderNo, lino, qtyRem, "NULL", uid, seq);
                    boolean addNew = new ISM100Dao().addSplitDetail(orderNo, lino, qtyNew, dest, uid, seq);

                    if (!sino.trim().equals("0")) {
                        new ISM100Dao().deleteDetail(orderNo, lino);
                    }

                    boolean runNo = new ISM100Dao().runNoDetailChild(orderNo, lino);

                    json = gson.toJson(addNew);
                    response.getWriter().write(json);

                } else if (mode.equals("deleteDetailChild")) {

                    boolean delete = new ISM100Dao().deleteDetailChild(orderNo, lino);

                    json = gson.toJson(delete);
                    response.getWriter().write(json);

                } else if (mode.equals("getLevel")) {

                    String vel = new ISM100Dao().getLevel(uid);

                    json = gson.toJson(vel);
                    response.getWriter().write(json);

                } else if (mode.equals("backward")) {

                    boolean update = new ISM100Dao().updateBackwardH(orderNo, uid, seq);
                    boolean updateAllDet = new ISM100Dao().updateBackWardAllDet(orderNo, uid, seq);

                    json = gson.toJson(update);
                    response.getWriter().write(json);

                } else if (mode.equals("updateMngApp")) {

                    boolean update = new ISM100Dao().updateMngApp(orderNo, uid, seq);
                    boolean updateAllDet = new ISM100Dao().updateMngAppAllDet(orderNo, uid, seq);

                    json = gson.toJson(update);
                    response.getWriter().write(json);

                } else if (mode.equals("getAppHead")) {

                    ISMMASH appHead = new ISM100Dao().getAppHead(orderNo, seq);

                    json = gson.toJson(appHead);
                    response.getWriter().write(json);

                } else if (mode.equals("getEditUser")) {

                    String euid = new ISM100Dao().getEditUser(orderNo);

                    json = gson.toJson(euid);
                    response.getWriter().write(json);

                } else if (mode.equals("getRemainData")) {

                    Map<String, Object> m = new HashMap<String, Object>();

                    List<ISMMASD> detailList = new ISM100Dao().getreMain(pdg);

                    m.put("datalist", detailList);
                    json = gson.toJson(m);
                    response.getWriter().write(json);

                } else if (mode.equals("insRemainLine")) {

                    String data = request.getParameter("data"); //json string format <= array of json object
                    JSONArray jsonArr = new JSONArray(data);
                    String child = "";

                    JSONObject res = new JSONObject();

                    String orderMem = "";
//                    String lineMem = "";
//                    String FamTreeMem = "";
//                    String childMem = "";

                    double sumtoMom = 0.00;

                    for (int i = 0; i < jsonArr.length(); i++) {

                        JSONObject jsonObject = new JSONObject(jsonArr.get(i).toString());
                        orderNo = jsonObject.getString("order");
                        seq = jsonObject.getString("seq");
                        lino = jsonObject.getString("line");
                        sino = jsonObject.getString("sino");
                        child = jsonObject.getString("child");

                        String linoBf = "";

                        if (i > 0) {
                            linoBf = new JSONObject(jsonArr.get(i - 1).toString()).getString("line");
                        }

                        //what Seq Should use
                        String CurSeq = new ISM100Dao().SelectMaxCurSEQbyORDERNo(orderNo);
                        String seqNew = new ISM100Dao().SelectMaxSEQbyORDERNo(orderNo);

                        //Head
                        if (orderMem.equals("")) { // first
                            boolean insRMHeadRes = new ISM100Dao().addRemainHead(orderNo, seq, seqNew, uid);
                            res.put("insRMHeadRes", insRMHeadRes);
                        } else {
                            if (!orderMem.equals(orderNo)) {
                                boolean insRMHeadRes = new ISM100Dao().addRemainHead(orderNo, seq, seqNew, uid);
                                res.put("insRMHeadRes", insRMHeadRes);
                            }
                        }

                        //Detail
                        if (child.equals("0")) { // Single
                            if (orderMem.equals("")) { // first
                                boolean insRMDetRes = new ISM100Dao().addRemainLine(orderNo, lino, seq, seqNew, uid, sino);
                                boolean updRMRes = new ISM100Dao().updateDetRemainLine(orderNo, lino, seq, uid, sino);
                                res.put("insRMDetRes", insRMDetRes);
                                res.put("updRMRes", updRMRes);
                            } else {
                                if (!orderMem.equals(orderNo)) {
                                    sumtoMom = 0.00; // reset sum
                                    boolean insRMDetRes = new ISM100Dao().addRemainLine(orderNo, lino, seq, seqNew, uid, sino);
                                    boolean updRMRes = new ISM100Dao().updateDetRemainLine(orderNo, lino, seq, uid, sino);
                                    res.put("insRMDetRes", insRMDetRes);
                                    res.put("updRMRes", updRMRes);
                                } else {
                                    boolean insRMDetRes = new ISM100Dao().addRemainLine(orderNo, lino, seq, CurSeq, uid, sino);
                                    boolean updRMRes = new ISM100Dao().updateDetRemainLine(orderNo, lino, seq, uid, sino);
                                    res.put("insRMDetRes", insRMDetRes);
                                    res.put("updRMRes", updRMRes);
                                }
                            }

                        } else { //Mom and Child
                            //update rq qty parent by child's rq qty
                            if (sino.equals("0")) { //Mom
                                if (!orderMem.equals(orderNo)) {
                                    sumtoMom = 0.00; // reset sum
                                    boolean insRMDetRes = new ISM100Dao().addRemainLineMom(orderNo, lino, seq, seqNew, uid, sino); //insert Mom ไม่ต้องมี RQ_QTY
                                    res.put("insRMDetRes", insRMDetRes);
                                } else {
                                    boolean insRMDetRes = new ISM100Dao().addRemainLineMom(orderNo, lino, seq, CurSeq, uid, sino); //insert Mom ไม่ต้องมี RQ_QTY
                                    res.put("insRMDetRes", insRMDetRes);
                                }
                            } else { //Child
                                String rq_qty = new ISM100Dao().SelectRQ_QTYFromChild(orderNo, lino, seq, sino);

                                //1 -- 2 -- 3 -- 4 -- 5
                                sumtoMom += Double.parseDouble(rq_qty); //sum RQ QTY

                                if (orderMem.equals("")) { // first
                                    boolean resp = new ISM100Dao().updateReqQTYToMom(orderNo, seqNew, linoBf, sumtoMom); //update mom line defore
                                    res.put("updateQTYtoMOM", resp);
                                } else {
                                    if (!orderMem.equals(orderNo)) {
                                        sumtoMom = 0.00; // reset sum
                                        sumtoMom += Double.parseDouble(rq_qty); //sum RQ QTY
                                        boolean resp = new ISM100Dao().updateReqQTYToMom(orderNo, seqNew, linoBf, sumtoMom); //update mom line defore
                                        res.put("updateQTYtoMOM", resp);
                                    } else {
                                        boolean insRMDetRes = new ISM100Dao().addRemainLine(orderNo, lino, seq, CurSeq, uid, sino); // insert child line
                                        boolean resp = new ISM100Dao().updateReqQTYToMom(orderNo, CurSeq, linoBf, sumtoMom); //update mom line defore
                                        res.put("insRMDetRes", insRMDetRes);
                                        res.put("updateQTYtoMOM", resp);
                                    }

                                    if (i == (jsonArr.length() - 1)) {
                                        boolean resp = new ISM100Dao().updateReqQTYToMom(orderNo, seqNew, linoBf, sumtoMom); //update mom line defore
                                        res.put("updateQTYtoMOM", resp);
                                    }
                                }
                            }

                            boolean updRMRes = new ISM100Dao().updateDetRemainLine(orderNo, lino, seq, uid, sino); //update 'R' all line

                        }

                        orderMem = orderNo;
//                        lineMem = lino;
//                        FamTreeMem = sino;
//                        childMem = child;

                    }

                    json = gson.toJson(res);
                    response.getWriter().write(json);

                } else if (mode.equals("checkRMsts")) {

                    boolean resSTS = new ISM100Dao().ckStsRemain(orderNo, seq);

                    json = gson.toJson(resSTS);
                    response.getWriter().write(json);

                } else if (mode.equals("updateMngAppPerLine")) {

                    String data = request.getParameter("data"); //json string format <= array of json object
                    JSONArray jsonArr = new JSONArray(data);

                    JSONArray resArr = new JSONArray();
                    JSONObject res = new JSONObject();
                    JSONObject res2 = new JSONObject();

                    boolean update = new ISM100Dao().updateMngAppAgain(orderNo, uid, seq); //Update Status 2 to Head
                    res2.put("upHead", update);
                    resArr.put(res2);

                    for (int i = 0; i < jsonArr.length(); i++) {
                        JSONObject jsonObject = new JSONObject(jsonArr.get(i).toString());
                        lino = jsonObject.getString("line");  //CONCAT([ISDLINO],'-',[ISDSINO]) = ?

                        JSONObject resJsonObj = new ISM100Dao().updateMngAppAllDetLine(orderNo, uid, seq, lino); //Update New Detail , Old Detail

                        res.put("line", lino);
                        res.put("upSts", resJsonObj.getBoolean("upSts"));
                        res.put("upRM", resJsonObj.getBoolean("upRM"));

                        resArr.put(res);

                    }

                    json = gson.toJson(resArr);
                    response.getWriter().write(json);

                } else if (mode.equals("deletePerLine")) {

                    String data = request.getParameter("data"); //json string format <= array of json object
                    JSONArray jsonArr = new JSONArray(data);
                    JSONArray resArr = new JSONArray();
                    JSONObject res = new JSONObject();
                    JSONObject res2 = new JSONObject();

                    for (int i = 0; i < jsonArr.length(); i++) {
                        JSONObject jsonObject = new JSONObject(jsonArr.get(i).toString());
                        lino = jsonObject.getString("line");  //CONCAT([ISDLINO],'-',[ISDSINO]) = ?
                        JSONObject resJsonObj = new ISM100Dao().deleteAllDetLine(orderNo, uid, seq, lino); //Delete New Detail , Clear Remain Old Detail

                        res.put("line", lino);
                        res.put("delOld", resJsonObj.getBoolean("delOld"));
                        res.put("upClear", resJsonObj.getBoolean("upClear"));

                        resArr.put(res);
                    }

                    if (!new ISM100Dao().ckStsRemain(orderNo, seq)) { //false access in
                        boolean deleteHead = new ISM100Dao().deleteHeadAgain(orderNo, seq); //delete Head
                        res2.put("delHead", deleteHead);
                        resArr.put(res2);
                    }

                    json = gson.toJson(resArr);
                    response.getWriter().write(json);

                } else if (mode.equals("closePerLine")) {

                    String data = request.getParameter("data"); //json string format <= array of json object
                    JSONArray jsonArr = new JSONArray(data);
                    JSONArray resArr = new JSONArray();
                    JSONObject res = new JSONObject();

                    for (int i = 0; i < jsonArr.length(); i++) {
                        JSONObject jsonObject = new JSONObject(jsonArr.get(i).toString());
                        lino = jsonObject.getString("line");  //CONCAT([ISDLINO],'-',[ISDSINO]) = ?
                        JSONObject resJsonObj = new ISM100Dao().closeAllDetLine(orderNo, uid, seq, lino); //Update New Detail => C , Update Old Detail => C

                        res.put("line", lino);
                        res.put("closeOld", resJsonObj.getBoolean("closeOld"));
                        res.put("closeNew", resJsonObj.getBoolean("closeNew"));

                        resArr.put(res);

                    }

                    json = gson.toJson(resArr);
                    response.getWriter().write(json);
                } else if (mode.equals("updateRespStyHead")) {

                    String resinput = request.getParameter("resinput");

                    boolean update = new ISM100Dao().updateRespStyHead(orderNo, seq, uid, new String(resinput.getBytes("iso-8859-1"), "UTF-8"));

                    json = gson.toJson(update);
                    response.getWriter().write(json);

                } else if (mode.equals("deleteSO")) {

                    JSONObject res = new JSONObject();

                    //get status from detail
                    String sts = new ISM100Dao().getStatus(orderNo, seq);
                    System.out.println("sts " + sts);

                    if (sts.equals("0")) {
                        boolean delHead = new ISM100Dao().deleteSOHead(orderNo, seq);
                        boolean delDetail = new ISM100Dao().deleteSODetail(orderNo, seq);

                        res.put("delH", delHead);
                        res.put("delD", delDetail);
                    } else {
                        res.put("delH", false);
                        res.put("delD", false);
                    }

                    res.put("getSTS", sts);

                    json = gson.toJson(res);
                    response.getWriter().write(json);

                }

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
