/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.utils;

import com.google.gson.Gson;
import com.twc.wms.dao.QDMBAGDao;
import com.twc.wms.dao.WHDao;
import com.twc.wms.dao.XMS007Dao;
import com.twc.wms.entity.QDMBAG;
import com.twc.wms.entity.QRMMAS;
import com.twc.wms.entity.WMS950;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author nutthawoot.noo
 */
public class ShowDataTablesWMS001 extends HttpServlet {

    private Gson gson;
    private static final String FILE_PATH = "WMS/images/raw-materials/";

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        PrintWriter out = response.getWriter();
        gson = new Gson();
        String json = null;

        String qrmcode = request.getParameter("qrmcode");

        try {
            String path = "";
            ServletContext servletContext = getServletContext();    // new by ji
            String realPath = servletContext.getRealPath(FILE_PATH);        // new by ji
            String saperate = realPath.contains(":") ? "\\" : "/";
            path = realPath + saperate + "noimages.jpg";        // new by ji

            realPath = realPath.replace("TABLE2/", "");
            request.setAttribute("realPath", realPath);

            File folder = new File(realPath);
            File[] listOfFiles = folder.listFiles();

            List<String> imageList = new ArrayList<String>();

            int noi = 0;

            if (listOfFiles != null) {
                for (int j = 0; j < listOfFiles.length; j++) {
                    if (listOfFiles[j].isFile()) {
                        if (listOfFiles[j].getName().contains(qrmcode)) {
                            imageList.add(listOfFiles[j].getName());
                        }
                    }
                }

                noi = imageList.size();
            }

            json = gson.toJson(noi);
            response.getWriter().write(json);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
