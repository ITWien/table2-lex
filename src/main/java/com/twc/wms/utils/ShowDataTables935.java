/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.utils;

import com.google.gson.Gson;
import com.twc.wms.dao.QUSMTCTRLDao;
import com.twc.wms.dao.WHDao;
import com.twc.wms.dao.WMS935Dao;
import com.twc.wms.entity.TOT930;
import com.twc.wms.entity.TOT930TB;
import com.twc.wms.entity.WMS950;
import com.twc.wms.entity.WMS960;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author nutthawoot.noo
 */
public class ShowDataTables935 extends HttpServlet {

    private Gson gson;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        PrintWriter out = response.getWriter();
        gson = new Gson();
        String json = null;

        String mode = request.getParameter("mode");
        String wh = request.getParameter("wh");
        String oh1000 = request.getParameter("oh1000");
        String oh1050 = request.getParameter("oh1050");

        try {
            if (mode != null) {
                if (mode.equals("getTOT930")) {

                    String pt1000 = "";
                    String pt1050 = "";

                    if (!oh1000.equals("none")) {
                        pt1000 = "1000";
                    }

                    if (!oh1050.equals("none")) {
                        pt1050 = "1050";
                    }

                    List<TOT930TB> resList = new WMS935Dao().findWHTOT930(oh1000, oh1050, pt1000, pt1050, wh);

                    for (int i = 0; i < resList.size(); i++) {
                        List<TOT930> dataList = new WMS935Dao().findTOT930(oh1000, oh1050, pt1000, pt1050, resList.get(i).getWH(), wh);
                        resList.get(i).setDATALIST(dataList);
                    }

                    json = gson.toJson(resList);
                    response.getWriter().write(json);

                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
