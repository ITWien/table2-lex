/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.utils;

import com.google.gson.Gson;
import com.twc.wms.dao.ISM100Dao;
import com.twc.wms.dao.ISM200Dao;
import com.twc.wms.entity.ISMMASD;
import com.twc.wms.entity.ISMMASH;
import com.twc.wms.entity.WH;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javax.xml.crypto.Data;

/**
 *
 * @author nutthawoot.noo
 */
public class ShowDataTablesISM200 extends HttpServlet {

    private Gson gson;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        PrintWriter out = response.getWriter();
        gson = new Gson();
        String json = null;

        String mode = request.getParameter("mode");
        String orderNo = request.getParameter("orderNo");
        String regDate = request.getParameter("regDate");
        String dest = request.getParameter("dest");
        String wh = request.getParameter("wh");
        String remark = request.getParameter("remark");
        String lino = request.getParameter("lino");
        String sino = request.getParameter("sino");
        String qtyOld = request.getParameter("qtyOld");
        String qtyNew = request.getParameter("qtyNew");
        String qtyRem = request.getParameter("qtyRem");
        String uid = request.getParameter("uid");
        String level = request.getParameter("level");
        String styleLotList = request.getParameter("styleLotList");
        String matCode = request.getParameter("matCode");
        String cnt = request.getParameter("cnt");
        String jobCreateDate = request.getParameter("jobCreateDate");
        String jobDebitDate = request.getParameter("jobDebitDate");
        String jobTransferDate = request.getParameter("jobTransferDate");
        String pdg = request.getParameter("pdg");
        String seq = request.getParameter("seq");

        try {
            if (mode != null) {
                if (mode.equals("getHead")) {
                    List<ISMMASH> headList = new ISM200Dao().getHead(wh, pdg);

                    json = gson.toJson(headList);
                    response.getWriter().write(json);

                } else if (mode.equals("updateReceived")) {

                    boolean update = new ISM200Dao().updateReceived(orderNo, uid, seq);
                    boolean updateAllDet = new ISM200Dao().updateReceivedAllDet(orderNo, uid, seq);

                    json = gson.toJson(update);
                    response.getWriter().write(json);

                } else if (mode.equals("updateCancel")) {

                    boolean update = new ISM200Dao().updateCancel(orderNo);
                    boolean updateAllDet = new ISM200Dao().updateCancelAllDet(orderNo);

                    json = gson.toJson(update);
                    response.getWriter().write(json);

                } else if (mode.equals("setJobNo")) {

                    String sseq = request.getParameter("sseq");

                    String jobNo = new ISM200Dao().getJobNo(wh);
                    if (!jobNo.equals("")) {
                        boolean updateJobNo = new ISM200Dao().updateJobNo(wh);
                    }
                    boolean setJobNo = new ISM200Dao().setJobNo(styleLotList.replace("HashTag", "#"), jobNo, sseq);

                    json = gson.toJson(jobNo);
                    response.getWriter().write(json);

                } else if (mode.equals("getCardData")) {

//                    String[] order = request.getParameterValues("order");
//                    String[] seq2 = request.getParameterValues("seq");
                    String[] oANDs = request.getParameterValues("oANDs");
//                    List<ISMMASH> headList = new ISM200Dao().getCardData(order, seq2, oANDs);
                    List<ISMMASH> headList = new ISM200Dao().getCardData(oANDs);

                    json = gson.toJson(headList);
                    response.getWriter().write(json);

                } else if (mode.equals("getCardStyleLotData")) {

                    String[] oANDs = request.getParameterValues("oANDs");
                    List<ISMMASH> headList = new ISM200Dao().getCardStyleLotData(dest, oANDs);

                    json = gson.toJson(headList);
                    response.getWriter().write(json);

                } else if (mode.equals("getDetailPrepare")) {

                    String sseq = request.getParameter("sseq");
                    List<ISMMASD> detList = new ISM200Dao().getDetailPrepare(styleLotList.replace("HashTag", "#"), sseq);

                    json = gson.toJson(detList);
                    response.getWriter().write(json);

                } else if (mode.equals("getHeadByMatcode")) {

                    List<ISMMASH> headList = new ISM200Dao().getHeadByMatcode(matCode, styleLotList.replace("HashTag", "#"));

                    json = gson.toJson(headList);
                    response.getWriter().write(json);

                } else if (mode.equals("getHeadSummary")) {

                    String jobNo = request.getParameter("jobNo");
                    ISMMASH head = new ISM200Dao().getHeadSummary(jobNo);

                    json = gson.toJson(head);
                    response.getWriter().write(json);

                } else if (mode.equals("getDetailSummary")) { //Mod Display STS=3 To Display STS=5

                    String jobNo = request.getParameter("jobNo");
                    List<ISMMASD> detList = new ISM200Dao().getDetailSummary(jobNo, wh);

                    json = gson.toJson(detList);
                    response.getWriter().write(json);

                } else if (mode.equals("getHeadByMatcodeJobNo")) {

                    String jobNo = request.getParameter("jobNo");
                    List<ISMMASH> headList = new ISM200Dao().getHeadByMatcodeJobNo(matCode, jobNo);

                    json = gson.toJson(headList);
                    response.getWriter().write(json);

                } else if (mode.equals("getWH")) {

                    List<WH> whList = new ISM100Dao().findWH(uid);

                    json = gson.toJson(whList);
                    response.getWriter().write(json);

                } else if (mode.equals("updateStatus4To5")) {

                    String jbno = request.getParameter("jbno");
                    String sts = request.getParameter("sts"); // Status Update Head and Detail

                    boolean updateSTSH = new ISM200Dao().updateSTS4To5Head(jbno, sts); // update Status Head
                    boolean updateSTSD = new ISM200Dao().updateSTS4To5Detail(jbno, sts); //update Ststus Detail

                    json = gson.toJson(updateSTSH);
                    response.getWriter().write(json);

                } else if (mode.equals("getQnoList")) {

                    List<String> qnoList = new ISM200Dao().getQno(Integer.parseInt(cnt), wh);
                    new ISM200Dao().updateQno(cnt, wh);

                    json = gson.toJson(qnoList);
                    response.getWriter().write(json);

                } else if (mode.equals("updateQnoList")) {

                    String payloadRequest = getBody(request);
                    String jobNo = request.getParameter("jobNo");

                    ISMMASH hd = new ISMMASH();
                    hd.setISHP1DT(jobCreateDate.replace("-", ""));
                    hd.setISHA1DT(jobDebitDate.replace("-", ""));
                    hd.setISHT1DT(jobTransferDate.replace("-", ""));
                    new ISM200Dao().updateHeadDate(jobNo, hd);

                    JSONArray jsonArr = new JSONArray(payloadRequest);
                    List<ISMMASD> qnoList = new ArrayList<ISMMASD>();
                    for (int i = 0; i < jsonArr.length(); i++) {
                        JSONObject jsonObj = jsonArr.getJSONObject(i);
                        ISMMASD data = new ISMMASD();
                        data.setISDORD(jsonObj.getString("ORD"));
                        data.setISDLINO(jsonObj.getString("LINO"));
                        data.setISDSINO(jsonObj.getString("SINO"));
                        data.setISDQNO(jsonObj.getString("QNO"));
                        qnoList.add(data);
                    }
//
                    boolean updateQnoList = new ISM200Dao().updateQnoList(qnoList);
//
                    json = gson.toJson(updateQnoList);
                    response.getWriter().write(json);

                } else if (mode.equals("backward")) { //clear everything  2 -> 0 || 4 -> 0

                    boolean update = new ISM200Dao().updateBackwardH(orderNo, uid, seq);
                    boolean updateAllDet = new ISM200Dao().updateBackWardAllDet(orderNo, uid, seq);

                    json = gson.toJson(update);
                    response.getWriter().write(json);

                } else if (mode.equals("getRPStyle")) {

                    List<ISMMASH> RPStyle = new ISM200Dao().getISMMASHDataTop1(orderNo, seq);

                    json = gson.toJson(RPStyle);
                    response.getWriter().write(json);

                } else if (mode.equals("closeOrderData")) {

                    Map<String, Object> m = new HashMap<String, Object>();

                    String appDate = request.getParameter("appDate");

                    List<ISMMASD> resData = new ISM200Dao().getCloseOD(appDate);

                    m.put("datalist", resData);
                    json = gson.toJson(m);
                    response.getWriter().write(json);

                } else if (mode.equals("updateClose")) {

                    List<JSONObject> listOrder = new ArrayList<JSONObject>();

                    String data = request.getParameter("data"); //json string format <= array of json object
                    JSONArray jsonArr = new JSONArray(data);

                    JSONObject res = new JSONObject();

                    String orderMem = "";
                    for (int i = 0; i < jsonArr.length(); i++) {

                        JSONObject jsonObject = new JSONObject(jsonArr.get(i).toString());
                        orderNo = jsonObject.getString("order");
                        seq = jsonObject.getString("seq");
                        lino = jsonObject.getString("line");
                        sino = jsonObject.getString("sino");

                        //update all row
                        boolean updRes = new ISM200Dao().updateCloseOrderSts(orderNo, seq, lino, sino);
                        res.put("updRes", updRes);

                        if (!orderMem.equals(orderNo)) {
                            JSONObject resD = new JSONObject();
                            resD.put("order", orderNo);
                            resD.put("seq", seq);
                            listOrder.add(resD);
                            //update Head Low Status
                            boolean updHLow = new ISM200Dao().updateCloseOrderStsHeadLow(orderNo, seq);
                        }

                        orderMem = orderNo;
                    }

                    for (int i = 0; i < listOrder.size(); i++) {
                        //check all detail status is C then
                        List<ISMMASD> listD = new ISM200Dao().getMasD_C_Sts(listOrder.get(i).get("order").toString(), listOrder.get(i).get("seq").toString());

                        if (listD.isEmpty()) { //All in C Status
                            //update high status to C
                            boolean updHHigh = new ISM200Dao().updateCloseOrderStsHeadHigh(listOrder.get(i).get("order").toString(), listOrder.get(i).get("seq").toString());
                        }
                    }

                    json = gson.toJson(res);
                    response.getWriter().write(json);

                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    public static String getBody(HttpServletRequest request) throws IOException {

        String body = null;
        StringBuilder stringBuilder = new StringBuilder();
        BufferedReader bufferedReader = null;

        try {
            InputStream inputStream = request.getInputStream();
            if (inputStream != null) {
                bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                char[] charBuffer = new char[128];
                int bytesRead = -1;
                while ((bytesRead = bufferedReader.read(charBuffer)) > 0) {
                    stringBuilder.append(charBuffer, 0, bytesRead);
                }
            } else {
                stringBuilder.append("");
            }
        } catch (IOException ex) {
            throw ex;
        } finally {
            if (bufferedReader != null) {
                try {
                    bufferedReader.close();
                } catch (IOException ex) {
                    throw ex;
                }
            }
        }

        body = stringBuilder.toString();
        return body;
    }
}
