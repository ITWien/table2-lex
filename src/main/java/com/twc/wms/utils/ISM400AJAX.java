/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.utils;

import com.twc.wms.dao.MASDDAO;
import com.twc.wms.dao.MASMDAO;
import com.twc.wms.dao.MSSUSERDAO;
import com.twc.wms.dao.PACKDAO;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author nutthawoot.noo
 */
public class ISM400AJAX extends HttpServlet {

    private static final long serialVersionUID = 102831973239L;
    private MSSUSERDAO USERDAO;
    private MASMDAO MASMDAO;
    private MASDDAO MASDDAO;
    private PACKDAO PACKDAO;

    public ISM400AJAX() {
        super();
        USERDAO = new MSSUSERDAO();
        MASMDAO = new MASMDAO();
        MASDDAO = new MASDDAO();
        PACKDAO = new PACKDAO();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String action = "";
        String text = "";

        if (request.getParameter("a") != null) {
            action = request.getParameter("a");
        }

        if (action.equalsIgnoreCase("login")) {

            String username = request.getParameter("user");  //new mod
            String password = request.getParameter("password");

            if (USERDAO.getUser(username, password) != null) {
                String user = USERDAO.getUser(username, password);  //new mod
                //String user = USERDAO.getUser(password);
                String[] USERGRP = USERDAO.getUserAll(username, password);
                HttpSession session = request.getSession();
                session.setAttribute("user", user);
                session.setAttribute("userid", username);

                //System.out.println("testuser="+USERGRP[0]);
                if (USERGRP[0] != null) {
                    String[] USERGRPtxt = USERGRP[0].split("@");

                    session.setAttribute("MSWHSE", USERGRPtxt[0]);
                    session.setAttribute("MSLEVEL", USERGRPtxt[1]);
                    session.setAttribute("MSPICTURE", USERGRPtxt[2]);

                    //
                }

                text = user;
            } else {
                text = "0";
            }

        } else if (action.equalsIgnoreCase("logout")) {
            HttpSession session = request.getSession();
            session.invalidate();
            text = "1";
        }

        response.setContentType("text/plain");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(text);

    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String select[] = request.getParameterValues("nCheck");
        String stsCK[] = request.getParameterValues("stsforCK");
        String mtCK[] = request.getParameterValues("mtforCK");
        String pickCK[] = request.getParameterValues("pickforCK");
        String type = request.getParameter("fType");
        String name = request.getParameter("fName");
        String text = "";
        String userid = request.getParameter("uidbyAip");

        //	MASMDAO.checkStatus(data[1], data[2], data[3]);  //check user approve
        for (int i = 0; i < select.length; i++) {

            if (select[i].length() > 1) {

                String[] data = select[i].split("\\|");

                int sts = 1;
                int stsQi = 2;
//                int stsQi = 5;
                int stsTmp = 0;

                if (type.equals("1")) { //Approve Btn
                    sts = 3;
                    stsTmp = 1;
                    stsQi = 2;
//                    stsQi = 5;
//                    MASDDAO.updateStatusQRMMAS(data[1], data[2], data[3], 5);lexcom

                    if (stsCK[i].equals("1")) { // update ISMSTAT 1 To 3 Only
                        new MASDDAO().updateSTATTo3(data[1], data[2], data[3], 3);
                    }

                    String STSHead = new MASDDAO().getSTSHead(data[1], data[3]); //HIGH STATUS
                    String STSDetail = new MASDDAO().getSTSDetail(data[1], data[3]); //DETAIL STATUS

                    if (STSHead.equals("5")) { // Eq 5 Then Update
                        //update head
                        new MASDDAO().updateStatusHead(data[1], data[3], mtCK[i], 6, "ISHHSTS");
                    }

                    if (STSDetail.equals("5")) { // Eq 5 Then Update
                        //update detail
                        new MASDDAO().updateStatusDetail(data[1], data[3], mtCK[i], 6, userid, pickCK[i]); //update PICK
                    }

                    boolean ckk = new MASDDAO().getCKSTS(data[1], data[3], mtCK[i]); //LOW STATUS

                    if (ckk) {
                        //update Low STS To Head => 6
                        new MASDDAO().updateStatusHead(data[1], data[3], mtCK[i], 6, "ISHLSTS");
                    }

                } else if (type.equals("2")) { // Rejected Btn
                    sts = 9;
                    stsTmp = 0;
                    stsQi = 9;

//                    MASDDAO.updateStatusQRMMAS(data[1], data[2], data[3], 4);lexcom
                    boolean R1 = new MASDDAO().updateBackwardH(data[1], data[2], data[3]); //UPDATE Clear Status To 0
                    boolean R2 = new MASDDAO().updateBackWardAllDet(data[1], data[2], data[3]); //UPDATE Clear Status To 0

                }

                if (i == 0) {
                    String sqlDeleteHeadDetail = new MASDDAO().getSQLdeleteHeadDetail(data[1], data[2], data[3]); //Change To XIHEAD, XIDETAIL
                    new MASDDAO().execSQL(sqlDeleteHeadDetail);
                }

                new MASDDAO().updateStatus(data[1], data[2], data[3], sts, stsTmp);
                int cntDup = new MASDDAO().checkDupQIHEAD(data[1], data[2], data[3]); //Change To XIHEAD

                if (cntDup > 0) {
                    String sqlUpdateHead = new MASDDAO().getSQLupdateHead(data[1], data[2], data[3]);
                    new MASDDAO().execSQL(sqlUpdateHead);
                } else {
                    new MASDDAO().addQIHEAD(data[1], data[2], data[3], stsQi); //Change To XIHEAD
                }

                new MASDDAO().addQIDETAIL(data[1], data[2], data[3], stsQi); //Change To XIDETAIL

//                if (type.equals("2")) {
//                    String sqlDeleteHeadDetail = MASDDAO.getSQLdeleteHeadDetail(data[1], data[2], data[3]);
//                    MASDDAO.execSQL(sqlDeleteHeadDetail);
//                }
//
//                int STATUS = MASMDAO.checkStatus(data[1], data[2], data[3]);
//
//                if ((STATUS == 1 || STATUS == 9) && type.equals("1")) {  // approved button
//                    MASMDAO.updateStatus(data[1], data[2], data[3], 2);
//
//                    //new mod update allocate
//                    MASMDAO.deleteAllocate(data[1], data[2], data[3]);  //DELETE ALLOCAT BY GROP,IS,ITEM
//                    MASMDAO.InsertAllocate(data[1], data[2], data[3], 2);   // insert allocat table GROP,IS,ITEM
//                    MASMDAO.updateAllocate(data[1], data[2], data[3], 2);  // UPDATE MASTER BY ITEM
//
//                    //update status qrmaster with group //  new mod   // 1=group,2=sequen,3=item
//                    PACKDAO.upqrstsGroup(data[1], Integer.parseInt(data[2]), data[3], "4");
//
//                } else if ((STATUS == 1 || STATUS == 2) && type.equals("2")) {   // rejected button
//                    MASMDAO.updateStatus(data[1], data[2], data[3], 9);
//
//                    //new mod update allocate
//                    MASMDAO.deleteAllocate(data[1], data[2], data[3]);  //DELETE ALLOCAT BY GROP,IS,ITEM
//                    MASMDAO.InsertAllocate(data[1], data[2], data[3], 9);   // insert allocat table GROP,IS,ITEM
//                    MASMDAO.updateAllocateGrop(data[1], data[2], data[3]);  //update ALLOCAT BY GROP,ISsue,ITEM
//                    //MASMDAO.InsertAllocate(data[1], data[2], data[3], 2);   // insert allocat table GROP,IS,ITEM
//                    MASMDAO.updateAllocate(data[1], data[2], data[3], 2);  // UPDATE MASTER BY ITEM
//
//                    //update status qrmaster with group //  new mod   // 1=group,2=sequen,3=item
//                    PACKDAO.upqrstsGroup(data[1], Integer.parseInt(data[2]), data[3], "3");
//
//                }
//
//                if (MASMDAO.checkDuplicateStatusName(data[1], data[2], data[3]) == false) {
//                    MASMDAO.addStatusName(data[1], data[2], data[3], name);
//                    text = "0";
//                } else {
//                    MASMDAO.updateStatusAName(data[1], data[2], data[3], name);
//                    text = "1";
//                }
            }

        }

        response.setContentType("text/plain");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(text);

    }

}
