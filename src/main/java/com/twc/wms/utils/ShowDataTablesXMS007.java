/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.utils;

import com.google.gson.Gson;
import com.twc.wms.dao.QDMBAGDao;
import com.twc.wms.dao.WHDao;
import com.twc.wms.dao.XMS007Dao;
import com.twc.wms.entity.QDMBAG;
import com.twc.wms.entity.QRMMAS;
import com.twc.wms.entity.WMS950;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author nutthawoot.noo
 */
public class ShowDataTablesXMS007 extends HttpServlet {

    private Gson gson;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        PrintWriter out = response.getWriter();
        gson = new Gson();
        String json = null;

        String mode = request.getParameter("mode");
        String type = request.getParameter("type");
        String idcode = request.getParameter("idcode");
        String id = request.getParameter("id");
        String code = request.getParameter("code");
        String len = request.getParameter("len");

        try {
            if (mode != null) {
                if (mode.equals("findMaster")) {
                    idcode = idcode.replace(" ", "+");
                    QRMMAS mas = new XMS007Dao().findMaster(idcode, type);
                    json = gson.toJson(mas);
                    response.getWriter().write(json);

                } else if (mode.equals("checkLastQr")) {
                    boolean checkLast = new XMS007Dao().checkLastQr(id);
                    json = gson.toJson(checkLast);
                    response.getWriter().write(json);

                } else if (mode.equals("insertGenQr")) {
                    boolean insert = new XMS007Dao().insertGenQr(id);
                    json = gson.toJson(insert);
                    response.getWriter().write(json);

                } else if (mode.equals("updateGenQr")) {
                    boolean update = new XMS007Dao().updateGenQr(id, len);
                    json = gson.toJson(update);
                    response.getWriter().write(json);

                } else if (mode.equals("getGenQr")) {
                    String last = new XMS007Dao().getGenQr(id);
                    json = gson.toJson(last);
                    response.getWriter().write(json);

                } else if (mode.equals("getDate")) {
                    String date = new XMS007Dao().getDate();
                    json = gson.toJson(date);
                    response.getWriter().write(json);

                } else if (mode.equals("getGenRoll")) {
                    String lastRoll = new XMS007Dao().getGenRoll(code);
                    json = gson.toJson(lastRoll);
                    response.getWriter().write(json);

                } else if (mode.equals("updateGenRoll")) {
                    boolean update = new XMS007Dao().updateGenRoll(code, len);
                    json = gson.toJson(update);
                    response.getWriter().write(json);

                } else if (mode.equals("insertQRMMAS")) {

                    String cdt = request.getParameter("cdt");
                    String qrmroll = request.getParameter("qrmroll");
                    String qrmqty = request.getParameter("qrmqty");
                    String qrmpacktype = request.getParameter("qrmpacktype");
                    String qrmid = request.getParameter("qrmid");
                    String uid = request.getParameter("uid");

                    boolean insert = new XMS007Dao().insertQRMMAS(id, cdt, qrmroll, qrmqty, qrmpacktype, qrmid, uid);
                    json = gson.toJson(insert);
                    response.getWriter().write(json);

                } else if (mode.equals("insertQRMTRA")) {

                    String cdt = request.getParameter("cdt");
                    String qrmroll = request.getParameter("qrmroll");
                    String qrmqty = request.getParameter("qrmqty");
                    String qrmpacktype = request.getParameter("qrmpacktype");
                    String qrmid = request.getParameter("qrmid");
                    String uid = request.getParameter("uid");

                    boolean insert = new XMS007Dao().insertQRMTRA(id, cdt, qrmroll, qrmqty, qrmpacktype, qrmid, uid);
                    json = gson.toJson(insert);
                    response.getWriter().write(json);

                } else if (mode.equals("updateParentQRMMAS")) {

                    String cdt = request.getParameter("cdt");
                    String remainQty = request.getParameter("remainQty");
                    String parentPackType = request.getParameter("parentPackType");
                    String uid = request.getParameter("uid");

                    boolean update = new XMS007Dao().updateParentQRMMAS(id, cdt, remainQty, parentPackType, uid);
                    json = gson.toJson(update);
                    response.getWriter().write(json);

                } else if (mode.equals("insertParentQRMTRA")) {

                    String cdt = request.getParameter("cdt");
                    String remainQty = request.getParameter("remainQty");
                    String parentPackType = request.getParameter("parentPackType");
                    String uid = request.getParameter("uid");

                    boolean insert = new XMS007Dao().insertParentQRMTRA(id, cdt, remainQty, parentPackType, uid);
                    json = gson.toJson(insert);
                    response.getWriter().write(json);

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
