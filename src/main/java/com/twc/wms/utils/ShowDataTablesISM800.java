/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.utils;

import com.google.gson.Gson;
import com.twc.wms.dao.ISM800Dao;
import com.twc.wms.entity.ISMMASD;
import com.twc.wms.entity.ISMMASH;
import com.twc.wms.entity.ISMSDWD;
import com.twc.wms.entity.ISMSDWH;
import com.twc.wms.entity.ISMSDWS;
import com.twc.wms.entity.QWHSTR;
import com.twc.wms.service.BAPIRET2;
import com.twc.wms.service.TABLEOFBAPIRET2;
import com.twc.wms.service.TABLEOFZBAPICHARG;
import com.twc.wms.service.TABLEOFZBAPILGORT;
import com.twc.wms.service.TABLEOFZBAPIMATNR;
import com.twc.wms.service.TABLEOFZBAPIPLANT;
import com.twc.wms.service.TABLEOFZBAPISTOCKRM01;
import com.twc.wms.service.ZBAPICHARG;
import com.twc.wms.service.ZBAPILGORT;
import com.twc.wms.service.ZBAPIMATNR;
import com.twc.wms.service.ZBAPIPLANT;
import com.twc.wms.service.ZBAPISTOCKRM01;
import com.twc.wms.service.ZBAPITWCMMRMSTOCKLISTResponse;
import com.twc.wms.service.ZWSTWCMMRMSTOCKLIST;
import com.twc.wms.service.ZWSTWCMMRMSTOCKLISTService;
import java.io.IOException;
import java.net.Authenticator;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.Future;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author 93176
 */
public class ShowDataTablesISM800 extends HttpServlet {

    private final Gson gson;

    public ShowDataTablesISM800() {
        this.gson = new Gson();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String mode = request.getParameter("mode");
        String json = new String();
        Map<String, Object> m = new HashMap<String, Object>();

        if (mode.equals("getProd")) {

            ISM800Dao dao = new ISM800Dao();

            List<QWHSTR> ValDao = null;

            ValDao = dao.findProd();

            m.put("Prod", ValDao);
            json = this.gson.toJson(m);

            /**
             * ****************************
             * Return JSON to Client ****************************
             */
            // SET Response header
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
            // SET List data to Json
            response.getWriter().write(json);

        } else if (mode.equals("getNameCusbyID")) {

            ISM800Dao dao = new ISM800Dao();

            List<QWHSTR> ValDao = null;

            ValDao = dao.findProd();

            m.put("Prod", ValDao);
            json = this.gson.toJson(m);

            /**
             * ****************************
             * Return JSON to Client ****************************
             */
            // SET Response header
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
            // SET List data to Json
            response.getWriter().write(json);

        } else if (mode.equals("CKbfInsert")) {

            String whSelect = request.getParameter("whSelect");
            String saledatef = request.getParameter("saledatef");
            String saledatet = request.getParameter("saledatet");
            String saleorderf = request.getParameter("saleorderf");
            String saleordert = request.getParameter("saleordert");
            String SelectProd = request.getParameter("SelectProd");

            boolean cc = new ISM800Dao().check(whSelect, saledatef, saledatet, saleorderf, saleordert, SelectProd);

            m.put("Val", cc);
            json = this.gson.toJson(m);

            /**
             * ****************************
             * Return JSON to Client ****************************
             */
            // SET Response header
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
            // SET List data to Json
            response.getWriter().write(json);
        } else if (mode.equals("EditDetailDisplay")) {

            String number = request.getParameter("no");
            String date = request.getParameter("dt");

            date = date.split("/")[2] + date.split("/")[1] + date.split("/")[0];

            List<ISMSDWD> ValDao = new ISM800Dao().findEditDetailBy(number, date);

            m.put("EditDet", ValDao);
            json = this.gson.toJson(m);

            /**
             * ****************************
             * Return JSON to Client ****************************
             */
            // SET Response header
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
            // SET List data to Json
            response.getWriter().write(json);
        } else if (mode.equals("DelDownload")) {

            String number = request.getParameter("no");
            boolean delRes = true;

//            String ordNo = new ISM800Dao().GetORDByDWNno(number);
            JSONObject dd = new ISM800Dao().deleteDownload(number);
            Iterator<String> keys = dd.keys();
            while (keys.hasNext()) {
                String key = keys.next();
//                System.out.println(dd.getJSONArray(key).getJSONObject(0).optBoolean("recDelS" + key));
//                System.out.println(dd.getJSONArray(key).getJSONObject(0).optBoolean("recDelD" + key));

                if (dd.getJSONArray(key).getJSONObject(0).optBoolean("recDelS" + key) == false) {
                    delRes = dd.getJSONArray(key).getJSONObject(0).optBoolean("recDelS" + key);
                }
            }

            m.put("DelVal", delRes);
            json = this.gson.toJson(m);

            /**
             * ****************************
             * Return JSON to Client ****************************
             */
            // SET Response header
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
            // SET List data to Json
            response.getWriter().write(json);
        } else if (mode.equals("DelDownloadLine")) {

            String number = request.getParameter("no");
            String line = request.getParameter("line");

            boolean dd = new ISM800Dao().deleteDownloadLine(number, line);

            m.put("DelVal", dd);
            json = this.gson.toJson(m);
            /**
             * ****************************
             * Return JSON to Client ****************************
             */
            // SET Response header
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
            // SET List data to Json
            response.getWriter().write(json);
        } else if (mode.equals("diffDetail")) {

//            String number = request.getParameter("no");
//
//            List<ISMMASD> LineMas = new ISM800Dao().GetMAS(number); //Line Mas
//            List<ISMSDWD> LineDown = new ISM800Dao().GetDWN(number); //Line Mas
//
//            int diff = LineDown.size() - LineMas.size();
//
////            boolean dd = new ISM800Dao().deleteDownloadLine(number, line);
//            m.put("Diff", diff);
//            json = this.gson.toJson(m);
//            /**
//             * ****************************
//             * Return JSON to Client ****************************
//             */
//            // SET Response header
//            response.setContentType("application/json");
//            response.setCharacterEncoding("UTF-8");
//            // SET List data to Json
//            response.getWriter().write(json);
        } else if (mode.equals("EditDetailDisplayByORD")) {

            String ord = request.getParameter("ord");
            String no = request.getParameter("no");
            String dt = request.getParameter("dt");

            dt = dt.split("/")[2] + dt.split("/")[1] + dt.split("/")[0];

            List<ISMSDWD> ValDao = new ISM800Dao().findEditDetailByORD(ord, no, dt);

            m.put("EditDet", ValDao);
            json = this.gson.toJson(m);

            /**
             * ****************************
             * Return JSON to Client ****************************
             */
            // SET Response header
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
            // SET List data to Json
            response.getWriter().write(json);
        } else if (mode.equals("diffDetailByORD")) {

            String order = request.getParameter("ord");
            String no = request.getParameter("no");

            //this Edit
            List<ISMMASD> LineMas = new ISM800Dao().GetMASByORD(order); //Line Mas
            List<ISMSDWD> LineDown = new ISM800Dao().GetDWNByORD(order, no); //Line Mas

            int diff = LineDown.size() - LineMas.size();

//            boolean dd = new ISM800Dao().deleteDownloadLine(number, line);
            m.put("Diff", diff);
            json = this.gson.toJson(m);
            /**
             * ****************************
             * Return JSON to Client ****************************
             */
            // SET Response header
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
            // SET List data to Json
            response.getWriter().write(json);
        } else if (mode.equals("DelDownloadLineIN")) {

            String nb = request.getParameter("no");
            String order = request.getParameter("order");
            String line = request.getParameter("line");
            String seq = request.getParameter("seq");

            boolean dd = new ISM800Dao().deleteDownloadLineIN2(nb, order, line, seq);

            m.put("DelVal", dd);
            json = this.gson.toJson(m);
            /**
             * ****************************
             * Return JSON to Client ****************************
             */
            // SET Response header
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
            // SET List data to Json
            response.getWriter().write(json);
        } else if (mode.equals("DelDownloadIN")) {

            String no = request.getParameter("no");
            String order = request.getParameter("order");

            JSONObject dd = new ISM800Dao().deleteDownloadIN(no, order);

//            m.put("DelVal", true);
            m.put("DelVal", dd.optBoolean("DelDwn"));
            json = this.gson.toJson(m);
            /**
             * ****************************
             * Return JSON to Client ****************************
             */
            // SET Response header
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
            // SET List data to Json
            response.getWriter().write(json);
        } else if (mode.equals("InsertINByLine")) {

            boolean ticket = true;

//            String no = request.getParameter("no");
//            String order = request.getParameter("order");
//            String line = request.getParameter("line");
            String pOrder = request.getParameter("pOrder");
            String linedata = request.getParameter("linedata");
            String uid = request.getParameter("uid");

//            System.out.println("pOrder " + pOrder);
            JSONArray jsonArr = new JSONArray(linedata);

            //delete Head Detail By Order and Sts = 0 Then Insert Not Duplicate 
            //deleteISMMASHbySEQSts1
//            //getMaster Head
//            List<ISMMASH> listHead = new ISM800Dao().GetMASHEADByORD(pOrder);
//            //getMasterDetail
//            List<ISMMASD> listDetail = new ISM800Dao().GetMASByORD(pOrder);
//
//            if (listHead.size() > 0) {
//                //Clear Master Head
//                boolean resDelH = new ISM800Dao().deleteISMMASH(pOrder);
//            }
//
//            if (listDetail.size() > 0) {
//                //Clear Master Detail
//                boolean resDelD = new ISM800Dao().deleteISMMASD(pOrder);
//            }
//            System.out.println("insert line ");
            String order = "";
            String no = "";
            String seq = "";
            int cntRowDet = 0;

            if (jsonArr.length() > 0) {

                System.out.println("this");

                //Clear Master Sts = 0
//                boolean resDelH = new ISM800Dao().deleteISMMASHSTS0(pOrder);
//                boolean resDelD = new ISM800Dao().deleteISMMASDSTS0(pOrder);
                for (int i = 0; i < jsonArr.length(); i++) {

                    no = jsonArr.getString(i).split("\\|")[0];
                    order = jsonArr.getString(i).split("\\|")[1];
                    String line = jsonArr.getString(i).split("\\|")[2];
                    seq = jsonArr.getString(i).split("\\|")[3];

//                    boolean resDelH = new ISM800Dao().deleteISMMASHSTS0(pOrder, no);
                    boolean resDelD = new ISM800Dao().deleteISMMASDSTS0(pOrder, line);

                    if (!no.equals("") && !order.equals("") && !line.equals("") && !seq.equals("")) {

                        //get sts by order no
                        String seq_current = new ISM800Dao().SelectSEQbyORDERNo(order);
                        System.out.println("seq_current " + seq_current);
                        String stsck = new ISM800Dao().selectSTSMasHead(order, seq_current);
                        System.out.println("stsck " + stsck);

                        if (stsck.equals("0") || stsck.equals("")) { // Status Master Head = 0 or Master Head Empty

                            System.out.println("0 or Empty");

                            List<ISMSDWD> LineDown = new ISM800Dao().GetNotDupMasnDwnPerLine(order, line, seq);

                            for (int j = 0; j < LineDown.size(); j++) {

                                String vtNull = "";

                                if (LineDown.get(j).getISDVT() == null) {
                                    vtNull = LineDown.get(j).getISDVT();
                                } else {
                                    vtNull = LineDown.get(j).getISDVT().trim();
                                }

                                if (seq_current.equals("0")) {
                                    seq_current = LineDown.get(i).getISDSEQ();
                                }

                                boolean insertISMMASD = new ISM800Dao().addISMMASD(LineDown.get(j).getISDORD(), seq_current, LineDown.get(j).getISDLINO(), "0", LineDown.get(j).getISDITNO().trim(),
                                        LineDown.get(j).getISDPLANT(), vtNull, LineDown.get(j).getISDSTRG(), LineDown.get(j).getISDRQQTY(), LineDown.get(j).getISDUNIT(),
                                        LineDown.get(j).getISDPRICE(), LineDown.get(j).getISDUNR01(), LineDown.get(j).getISDUNR03(), LineDown.get(j).getISDMTCTRL(),
                                        LineDown.get(j).getISDDEST(), LineDown.get(j).getISDPURG(), uid, "0");

                                if (insertISMMASD == false) {
                                    ticket = false;
                                } else {
                                    cntRowDet++;
                                }
                            }

                            List<ISMMASH> getHeadMas = new ISM800Dao().GetMASHEADByORDWithSEQ(order, no, seq); //Should Check With Seq Edit This
                            if (getHeadMas.isEmpty()) { //Not Found Master Head

                                List<ISMSDWH> DWNHead = new ISM800Dao().GetDWNHeadByORDWithSEQ(order, no, seq); //Download Head

                                boolean insertISMMASH = new ISM800Dao().addISMMASH(DWNHead.get(0).getISHORD(), seq_current, DWNHead.get(0).getISHTRDT(), DWNHead.get(0).getISHUART(), DWNHead.get(0).getISHTWEG(),
                                        DWNHead.get(0).getISHMVT(), DWNHead.get(0).getISHCUNO(), DWNHead.get(0).getISHCUNM1(), DWNHead.get(0).getISHCUNM2(), DWNHead.get(0).getISHCUAD1(), DWNHead.get(0).getISHCUAD2(),
                                        DWNHead.get(0).getISHBSTKD(), DWNHead.get(0).getISHSUBMI(), DWNHead.get(0).getISHMATLOT(), DWNHead.get(0).getISHTAXNO(), DWNHead.get(0).getISHBRANCH01(), DWNHead.get(0).getISHDATUM(),
                                        DWNHead.get(0).getISHSTYLE(), DWNHead.get(0).getISHCOLOR(), DWNHead.get(0).getISHLOT(), DWNHead.get(0).getISHAMTFG(), DWNHead.get(0).getISHSTYLE2(), Integer.toString(cntRowDet),
                                        DWNHead.get(0).getISHPOFG(), DWNHead.get(0).getISHGRPNO(), DWNHead.get(0).getISHWHNO(), DWNHead.get(0).getISHPGRP(), DWNHead.get(0).getISHDEST(), DWNHead.get(0).getISHREASON(), uid, "0", "0");
                            }

                        } else { // Status Master Head = 1,2,3,...

                            System.out.println("Status Master Head = 1,2,3,... " + order + " " + line);

                            String sseq = "";

                            List<ISMSDWD> LineDown = new ISM800Dao().GetNotDupMasnDwnPerLine(order, line, seq);

                            for (int j = 0; j < LineDown.size(); j++) {

                                String vtNull = "";

                                if (LineDown.get(j).getISDVT() == null) {
                                    vtNull = LineDown.get(j).getISDVT();
                                } else {
                                    vtNull = LineDown.get(j).getISDVT().trim();
                                }

                                sseq = Integer.toString(Integer.parseInt(seq_current) + 1);
//                                sseq = Integer.toString(Integer.parseInt(LineDown.get(j).getISDSEQ()) + 1);

                                boolean insertISMMASD = new ISM800Dao().addISMMASD(LineDown.get(j).getISDORD(), sseq, LineDown.get(j).getISDLINO(), "0", LineDown.get(j).getISDITNO().trim(),
                                        LineDown.get(j).getISDPLANT(), vtNull, LineDown.get(j).getISDSTRG(), LineDown.get(j).getISDRQQTY(), LineDown.get(j).getISDUNIT(),
                                        LineDown.get(j).getISDPRICE(), LineDown.get(j).getISDUNR01(), LineDown.get(j).getISDUNR03(), LineDown.get(j).getISDMTCTRL(),
                                        LineDown.get(j).getISDDEST(), LineDown.get(j).getISDPURG(), uid, "0");

                                if (insertISMMASD == false) {
                                    ticket = false;
                                } else {
                                    cntRowDet++;
                                }
                            }

                            List<ISMMASH> getHeadMas = new ISM800Dao().GetMASHEADByORDWithSEQ(order, no, sseq); //Should Check With Seq Edit This

                            if (getHeadMas.isEmpty()) { //Not Found Master Head

                                List<ISMSDWH> DWNHead = new ISM800Dao().GetDWNHeadByORDWithSEQ(order, no, seq); //Download Head

                                boolean insertISMMASH = new ISM800Dao().addISMMASH(DWNHead.get(0).getISHORD(), sseq, DWNHead.get(0).getISHTRDT(), DWNHead.get(0).getISHUART(), DWNHead.get(0).getISHTWEG(),
                                        DWNHead.get(0).getISHMVT(), DWNHead.get(0).getISHCUNO(), DWNHead.get(0).getISHCUNM1(), DWNHead.get(0).getISHCUNM2(), DWNHead.get(0).getISHCUAD1(), DWNHead.get(0).getISHCUAD2(),
                                        DWNHead.get(0).getISHBSTKD(), DWNHead.get(0).getISHSUBMI(), DWNHead.get(0).getISHMATLOT(), DWNHead.get(0).getISHTAXNO(), DWNHead.get(0).getISHBRANCH01(), DWNHead.get(0).getISHDATUM(),
                                        DWNHead.get(0).getISHSTYLE(), DWNHead.get(0).getISHCOLOR(), DWNHead.get(0).getISHLOT(), DWNHead.get(0).getISHAMTFG(), DWNHead.get(0).getISHSTYLE2(), Integer.toString(cntRowDet),
                                        DWNHead.get(0).getISHPOFG(), DWNHead.get(0).getISHGRPNO(), DWNHead.get(0).getISHWHNO(), DWNHead.get(0).getISHPGRP(), DWNHead.get(0).getISHDEST(), DWNHead.get(0).getISHREASON(), uid, "0", "0");
                            }

                        }

                    }

                }

            }

            m.put("DelVal", ticket);
//            m.put("DelVal", dd);
            json = this.gson.toJson(m);
            /**
             * ****************************
             * Return JSON to Client ****************************
             */
            // SET Response header
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
            // SET List data to Json
            response.getWriter().write(json);

        } else if (mode.equals("sendMail")) {

            final String username = "sandwind1@gmail.com";
            final String password = "joyjapma5678";

//            System.out.println("this in Java Send Mail");
            Properties prop = new Properties();
            prop.put("mail.smtp.host", "smtp.gmail.com");
            prop.put("mail.smtp.port", "465");
            prop.put("mail.smtp.auth", "true");
            prop.put("mail.smtp.starttls.enable", "true"); //TLS

//            System.out.println(username);
//            System.out.println(password);
//            Session session = Session.getInstance(prop,
//                    new javax.mail.Authenticator() {
//                @Override
//                protected PasswordAuthentication getPasswordAuthentication() {
//                    return new PasswordAuthentication(username, password);
//                }
//            });
//            try {
//
//                Message message = new MimeMessage(session);
//                message.setFrom(new InternetAddress("sandwind1@gmail.com"));
//                message.setRecipients(
//                        Message.RecipientType.TO,
//                        InternetAddress.parse("aip2539@gmail.com")
//                );
//                message.setSubject("Testing Gmail TLS");
//                message.setText("Dear Mail Crawler,"
//                        + "\n\n Please do not spam my email!");
//
//                Transport.send(message);
//
//                System.out.println("Done");
//
//            } catch (MessagingException e) {
//                e.printStackTrace();
//            }
            m.put("DelVal", true);
//            m.put("DelVal", dd);
            json = this.gson.toJson(m);
            /**
             * ****************************
             * Return JSON to Client ****************************
             */
            // SET Response header
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
            // SET List data to Json
            response.getWriter().write(json);

        } else if (mode.equals("redownloadSL")) {

            JSONArray resArr = new JSONArray();

            try {

//                List<ISMSDWH> HeadDistinct = new ISM800Dao().GetNotDupHMasnDwn(order);
                String orderdata = request.getParameter("orderdata");
                String uid = request.getParameter("uid");
//                String date = request.getParameter("date");
                String maxdwnno = new ISM800Dao().SelectMax();

                JSONArray jsonArr = new JSONArray(orderdata);

                if (jsonArr.length() > 0) {

                    for (int i = 0; i < jsonArr.length(); i++) {

                        JSONObject res = new JSONObject();

                        String dwnno = jsonArr.getString(i).split(":")[0];
                        String orderno = jsonArr.getString(i).split(":")[1];

                        String seq_current = new ISM800Dao().SelectSEQbyORDERNo(orderno);

                        //select matcode by order --> sts = 0 || sts = 9
                        List<ISMMASD> masData = new ISM800Dao().GetMASbySTS0n9(orderno, dwnno);

                        res.put("resOrder", orderno);
                        res.put("resDwnno", dwnno);

                        boolean InsertHead = false;

                        for (int j = 0; j < masData.size(); j++) {

                            List<ISMSDWS> sldetail = new ArrayList<ISMSDWS>();

                            String matcode = masData.get(j).getISDITNO();
                            String plant = masData.get(j).getISDPLANT();
                            String stock = masData.get(j).getISDSTRG();
                            String seqOld = masData.get(j).getISDSEQ();
                            String vt = masData.get(j).getISDVT();

                            if (vt == null) {
                                vt = "01";
                            }

                            Authenticator.setDefault(new Authenticator() {
                                @Override
                                protected java.net.PasswordAuthentication getPasswordAuthentication() {

                                    Calendar c = Calendar.getInstance();
                                    int month = c.get(Calendar.MONTH) + 1;

                                    String m = Integer.toString(month);
                                    if (m.length() < 2) {
                                        m = "0" + m;
                                    }

                                    String pass = "display" + m;

                                    return new java.net.PasswordAuthentication("tviewwien", pass.toCharArray());
                                }
                            });

//-------------------------- STOCKLIST ---------------------------------------
                            ZWSTWCMMRMSTOCKLISTService service2 = new ZWSTWCMMRMSTOCKLISTService();
                            ZWSTWCMMRMSTOCKLIST getstocklist = service2.getZWSTWCMMRMSTOCKLISTSoapBinding();

                            TABLEOFZBAPIMATNR tablematnum = new TABLEOFZBAPIMATNR();
                            TABLEOFZBAPIPLANT tableplantSL = new TABLEOFZBAPIPLANT();
                            TABLEOFZBAPILGORT tablestgeloc = new TABLEOFZBAPILGORT();
                            TABLEOFZBAPICHARG tablebatch = new TABLEOFZBAPICHARG();
                            TABLEOFBAPIRET2 _returnSL = new TABLEOFBAPIRET2();
                            TABLEOFZBAPISTOCKRM01 stockRMOUT = new TABLEOFZBAPISTOCKRM01();

                            ZBAPIMATNR matnum = new ZBAPIMATNR();
                            matnum.setSIGN("I");
                            matnum.setOPTION("EQ");
                            matnum.setMATNRLOW(matcode.trim());
                            matnum.setMATNRHIGH("");
                            tablematnum.getItem().add(matnum);

                            ZBAPIPLANT plantSL = new ZBAPIPLANT();
                            plantSL.setSIGN("I");
                            plantSL.setOPTION("EQ");
                            plantSL.setWERKSLOW(plant.trim());
                            plantSL.setWERKSHIGH("");
                            tableplantSL.getItem().add(plantSL);

                            ZBAPILGORT stgeloc = new ZBAPILGORT();
                            stgeloc.setSIGN("I");
                            stgeloc.setOPTION("EQ");
                            stgeloc.setLGORTLOW(stock.trim());
                            stgeloc.setLGORTHIGH("");
                            tablestgeloc.getItem().add(stgeloc);

                            ZBAPICHARG batch = new ZBAPICHARG();
                            batch.setSIGN("");
                            batch.setOPTION("");
                            batch.setCHARGLOW("");
                            batch.setCHARGHIGH("");
//            tablebatch.getItem().add(batch);

                            Future<?> getSLAsync = getstocklist.zBAPITWCMMRMSTOCKLISTAsync(tablebatch, tablematnum, tableplantSL, _returnSL, tablestgeloc, stockRMOUT);
                            ZBAPITWCMMRMSTOCKLISTResponse getSLAsyncResponse = null;
                            getSLAsyncResponse = (ZBAPITWCMMRMSTOCKLISTResponse) getSLAsync.get();

                            TABLEOFBAPIRET2 resReturn = getSLAsyncResponse.getRETURN();
                            TABLEOFZBAPISTOCKRM01 stockrm01 = getSLAsyncResponse.getSTOCKRMOUT();

                            for (BAPIRET2 ret : resReturn.getItem()) {
                                System.out.println("STOCKLIST_FIELD : " + ret.getFIELD());
                                System.out.println("ID : " + ret.getID());
                                System.out.println("LOGNO : " + ret.getLOGNO());
                                System.out.println("MESSAGE : " + ret.getMESSAGE());
                                System.out.println("MESSAGEV1 : " + ret.getMESSAGEV1());
                                System.out.println("MESSAGEV2 : " + ret.getMESSAGEV2());
                                System.out.println("MESSAGEV3 : " + ret.getMESSAGEV3());
                                System.out.println("MESSAGEV4 : " + ret.getMESSAGEV4());
                                System.out.println("NUMBER : " + ret.getNUMBER());
                                System.out.println("PARAMETER : " + ret.getPARAMETER());
                                System.out.println("SYSTEM : " + ret.getSYSTEM());
                                System.out.println("TYPE : " + ret.getTYPE());
                            }

//                            System.out.println("----------------------");
                            for (ZBAPISTOCKRM01 sl01 : stockrm01.getItem()) {

                                ISMSDWS stocklist = new ISMSDWS();
                                stocklist.setISSCONO("TWC");
                                stocklist.setISSMTNO(sl01.getMATERIAL()); //Material Number
                                stocklist.setISSPLANT(sl01.getPLANT()); //Plant
                                stocklist.setISSSTORE(sl01.getLGORT()); //Storage Location 
                                stocklist.setISSBTNO(sl01.getVALTYPE()); //Batch Number --
                                stocklist.setISSMTGP(sl01.getMATGRP()); //Material Group
                                stocklist.setISSSBIN(sl01.getMATCTRL()); //Storage Bin
                                stocklist.setISSMDES(sl01.getMATCTRLNAME()); //Name
                                stocklist.setISSBUOM(sl01.getBOMUNIT()); //Base Unit of Measure
                                stocklist.setISSUNR(sl01.getUNRQTY().toString()); //Valuated Unrestricted-Use Stock
                                stocklist.setISSQI(sl01.getQIQTY().toString()); //Stock in Quality Inspection
                                stocklist.setISSBLOCK(sl01.getBLOCKQTY().toString()); //Blocked Stock
                                stocklist.setISSTOTQ(sl01.getTOTALQTY().toString()); //Total Valuated Stock
                                stocklist.setISSMAVG(sl01.getMOVAVG().toString()); //Moving Average Price/Periodic Unit Price
                                stocklist.setISSTOTA(sl01.getTOTALAMT().toString()); //Value of Total Valuated Stock
                                stocklist.setISSEDT("");
                                stocklist.setISSETM("");
                                stocklist.setISSEUSR("");
                                stocklist.setISSCDT("");
                                stocklist.setISSCTM("");
                                stocklist.setISSUSER("");
                                sldetail.add(stocklist);

                            }

                            String seqNew = Integer.toString(Integer.parseInt(seqOld) + 1); //SEQ + 1

                            for (int k = 0; k < sldetail.size(); k++) {
                                if (masData.get(j).getISDSTS().equals("9")) {

//                                    res.put("resSTS", "9");
                                    //----------- Status 9 Not Do anything
//                                    String vtNull = "";
//
//                                    if (masData.get(j).getISDVT() == null) {
//                                        vtNull = masData.get(j).getISDVT();
//                                    } else {
//                                        vtNull = masData.get(j).getISDVT().trim();
//                                    }
//
//                                    boolean insertISMMASD = new ISM800Dao().addISMMASD(masData.get(j).getISDORD(), seqNew, masData.get(j).getISDLINO(), "0", masData.get(j).getISDITNO().trim(),
//                                            masData.get(j).getISDPLANT(), vtNull, masData.get(j).getISDSTRG(), masData.get(j).getISDRQQTY(), masData.get(j).getISDUNIT(),
//                                            masData.get(j).getISDPRICE(), masData.get(j).getISDUNR01(), masData.get(j).getISDUNR03(), masData.get(j).getISDMTCTRL(),
//                                            masData.get(j).getISDDEST(), uid, "0");
                                    //เปลี่ยนเป็น sts 0 Seq +1 => เป็น insert detail ที่ sts เป็น 0 แทน
//                                    boolean updateQTYSL = new ISM800Dao().updateSL(orderno, matcode, vt, sldetail.get(k).getISSTOTQ(), seqNew, seqOld);
//                                    if (insertISMMASD) {
//                                        InsertHead = insertISMMASD;
//                                    }
                                } else if (masData.get(j).getISDSTS().equals("0")) {

                                    if (vt.equals(sldetail.get(k).getISSBTNO())) {
                                        res.put("resSTS0", "0");

                                        //เปลี่ยนแค่ข้อมูล
                                        boolean updateQTYSL = new ISM800Dao().updateSL2(orderno, matcode, vt, sldetail.get(k).getISSTOTQ());

                                        if (updateQTYSL) {
                                            res.put("resUpdDwnSts0", true);
                                        } else {
                                            res.put("resUpdDwnSts0", false);
                                        }
                                    }

                                }
                            }

                            //-------------------------- STOCKLIST ---------------------------------------
//                            boolean insertDTDwn = new ISM800Dao().insertDetailLineNotFound(maxdwnno, max_seq, invDetailList.get(j), user);
                        }

                        String maxseq = new ISM800Dao().SelectMaxSEQbyORDERNo(orderno);
                        String stsck = new ISM800Dao().selectSTSMasHead(orderno, seq_current);

                        if (stsck.equals("0") || stsck.equals("")) { // Status Master Head = 0 or Master Head Empty
//                            System.out.println("this sts 0 or Empty Head");
                        } else { // Status Master Head = 1,2,3,...
                            //Insert Head Per Order
//                        String nnSEQ = Integer.toString(Integer.parseInt(masData.get(0).getISDSEQ()) + 1);
                            boolean insertISMSDWD = new ISM800Dao().addISMSDWD(orderno, maxseq, maxdwnno, dwnno); //Add new Dwn Detail By 

                            if (insertISMSDWD) {
                                boolean updateQTYSL = new ISM800Dao().updateSL3(orderno, maxseq, masData.get(0).getISDSEQ(), maxdwnno, dwnno);
                                if (updateQTYSL) {
                                    res.put("resInsDwnHSts9", true);
                                    res.put("resInsDwnDSts9", true);
                                    res.put("resSTS9", "9");
                                } else {
                                    res.put("resInsDwnHSts9", false);
                                    res.put("resInsDwnDSts9", false);
//                                res.put("resSTS9", "9");
                                }
                            } else {
                                res.put("resInsDwnDSts9", false);
                            }
                        }

                        resArr.put(res);
                    }
                }

            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println("Catch : " + ex.getMessage());
            }

            m.put("resJson", resArr);
            json = this.gson.toJson(m);

            /**
             * ****************************
             * Return JSON to Client ****************************
             */
            // SET Response header
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
            // SET List data to Json
            response.getWriter().write(json);

        } else if (mode.equals("EditDetailDisplayByORDForEdit")) {

            String ord = request.getParameter("ord");
            String no = request.getParameter("no");
            String dt = request.getParameter("dt");

            dt = dt.split("/")[2] + dt.split("/")[1] + dt.split("/")[0];

            String sts = new ISM800Dao().findEditDetailByORDForEdit(ord, no, dt);

            m.put("sts", sts);
            json = this.gson.toJson(m);

            /**
             * ****************************
             * Return JSON to Client ****************************
             */
            // SET Response header
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
            // SET List data to Json
            response.getWriter().write(json);
        }

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

}
