/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.utils;

/**
 *
 * @author nutthawoot.noo
 */
public final class ThaiConverter {

    public String toUnicode(String s) {

        StringBuffer output = new StringBuffer();
        int size = s.length();

        for (int i = 0; i < size; i++) {
            char c = s.charAt(i);
            if (c >= 161 && c <= 251) {
                c = (char) (c + 3424);
            }
            output.append(c);
        }

        return output.toString();

    }

    public String toAscii(String s) {

        StringBuffer output = new StringBuffer();
        int size = s.length();

        for (int i = 0; i < size; i++) {
            char c = s.charAt(i);
            if (c >= 161 + 3424 && c <= 251 + 3424) {
                c = (char) (c - 3424);
            }
            output.append(c);
        }

        return output.toString();
    }

}
