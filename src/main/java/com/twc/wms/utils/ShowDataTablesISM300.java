/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.utils;

import com.google.gson.Gson;
import com.twc.wms.dao.ISM300Dao;
import com.twc.wms.entity.ISMMASD;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author nutthawoot.noo
 */
public class ShowDataTablesISM300 extends HttpServlet {

    private Gson gson;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        PrintWriter out = response.getWriter();
        gson = new Gson();
        String json = null;

        String mode = request.getParameter("mode");
        String cus = request.getParameter("cus");
        String mat = request.getParameter("mat");
        String jobNo = request.getParameter("jobNo");
        String seqNo = request.getParameter("seqNo");

        try {
            if (mode != null) {
                if (mode.equals("getCustomerName")) {

                    String cusName = new ISM300Dao().getCustomerName(cus);

                    json = gson.toJson(cusName);
                    response.getWriter().write(json);

                } else if (mode.equals("getMatName")) {

                    String matName = new ISM300Dao().getMatName(mat);

                    json = gson.toJson(matName);
                    response.getWriter().write(json);

                } else if (mode.equals("getJobNoList")) {

                    List<String> jobNoList = new ISM300Dao().getJobNoList(cus);

                    json = gson.toJson(jobNoList);
                    response.getWriter().write(json);

                } else if (mode.equals("getSeqNoList")) {

                    List<String> seqNoList = new ISM300Dao().getSeqNoList(cus);

                    json = gson.toJson(seqNoList);
                    response.getWriter().write(json);

                } else if (mode.equals("getDetailList")) {

                    List<ISMMASD> detailList = new ISM300Dao().getDetailList(cus, mat, jobNo, seqNo);

                    json = gson.toJson(detailList);
                    response.getWriter().write(json);

                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
