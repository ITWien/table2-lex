/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.utils;

import com.google.gson.Gson;
import com.twc.wms.dao.QDMROLLDETDao;
import com.twc.wms.dao.QDMROLLHEDDao;
import com.twc.wms.dao.WHDao;
import com.twc.wms.entity.QDMROLLDET;
import com.twc.wms.entity.QDMROLLHED;
import com.twc.wms.entity.WMS950;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author nutthawoot.noo
 */
public class ShowDataTablesWMS170 extends HttpServlet {

    private Gson gson;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        PrintWriter out = response.getWriter();
        gson = new Gson();
        String json = null;

        String mode = request.getParameter("mode");
        String style = request.getParameter("style");
        String qrmid = request.getParameter("qrmid");

        String code = request.getParameter("code");
        String desc = request.getParameter("desc");
        String unit = request.getParameter("unit");
        String equa = request.getParameter("equa");
        String x = request.getParameter("x");
        String y = request.getParameter("y");
        String uid = request.getParameter("uid");

        String line = request.getParameter("line");
        String qty = request.getParameter("qty");
        String height = request.getParameter("height");
        String length = request.getParameter("length");

        try {
            if (mode != null) {
                if (mode.equals("getHed")) {
                    QDMROLLHED ROLLHED = new QDMROLLHEDDao().findByStyle(style);
                    json = gson.toJson(ROLLHED);
                    response.getWriter().write(json);

                } else if (mode.equals("getDet")) {
                    List<QDMROLLDET> ROLLDETList = new QDMROLLDETDao().findByStyle(style);
                    json = gson.toJson(ROLLDETList);
                    response.getWriter().write(json);

                } else if (mode.equals("getQRMMAS")) {
                    QDMROLLHED qrmmas = new QDMROLLHEDDao().findQRMMAS(qrmid);
                    json = gson.toJson(qrmmas);
                    response.getWriter().write(json);

                } else if (mode.equals("checkHed")) {
                    String hed = new QDMROLLHEDDao().checkHed(style);
                    json = gson.toJson(hed);
                    response.getWriter().write(json);

                } else if (mode.equals("updateRollHed")) {
                    equa = equa.replace("[plus]", "+");
//                    desc = desc.replace("[sharp]", "#");
                    boolean updateRollHed = new QDMROLLHEDDao().updateRollHed(code, desc, unit, equa, x, y, uid);
                    json = gson.toJson(updateRollHed);
                    response.getWriter().write(json);

                } else if (mode.equals("updateRollDet")) {
                    boolean updateRollDet = new QDMROLLDETDao().updateRollDet(code, line, length, height, uid);
                    json = gson.toJson(updateRollDet);
                    response.getWriter().write(json);

                } else if (mode.equals("deleteDet")) {
                    new QDMROLLDETDao().deleteDet(code, line);

                } else if (mode.equals("addDet")) {
                    boolean addDet = new QDMROLLDETDao().addDet(code, length, uid, qrmid);
                    json = gson.toJson(addDet);
                    response.getWriter().write(json);

                } else if (mode.equals("checkDet")) {
                    String det = new QDMROLLDETDao().checkDet(qrmid);
                    json = gson.toJson(det);
                    response.getWriter().write(json);

                } else if (mode.equals("addHed")) {
//                    desc = desc.replace("[sharp]", "#");
                    boolean addHed = new QDMROLLHEDDao().addHed(code, desc, unit, uid);
                    json = gson.toJson(addHed);
                    response.getWriter().write(json);

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
