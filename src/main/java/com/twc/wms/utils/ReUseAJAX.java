/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package com.twc.wms.utils;

import com.google.gson.Gson;
import com.twc.wms.dao.WMS009Dao;
import com.twc.wms.dao.WMS009Dao;
import com.twc.wms.entity.PGMU;
import com.twc.wms.entity.UserAuth;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author 93176
 */
public class ReUseAJAX extends HttpServlet {

    private Gson gson;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        PrintWriter out = response.getWriter();
        gson = new Gson();
        String json = null;

        String mode = request.getParameter("mode");
        String uid = request.getParameter("uid");
        String uidto = request.getParameter("uidto");
        String whoEdit = request.getParameter("whoEdit");

        try {

            if (mode != null) {
                if (!mode.equals("")) {
                    if (mode.equals("getUserList")) {

                        WMS009Dao dao = new WMS009Dao();
                        List<UserAuth> pList1 = dao.findAll();

                        json = gson.toJson(pList1);
                        response.getWriter().write(json);

                    } else if (mode.equals("getMenuByUID")) {

                        WMS009Dao dao = new WMS009Dao();
                        List<PGMU> mnu = dao.CheckMenuByuid(uid);

                        json = gson.toJson(mnu);
                        response.getWriter().write(json);

                    } else if (mode.equals("copyMenuTo")) {
                        WMS009Dao dao = new WMS009Dao();
                        List<PGMU> mnu = dao.findAll(uid); //MENU USER From

                        boolean resp = true;

                        for (int i = 0; i < mnu.size(); i++) {
                            boolean insertMenu = new WMS009Dao().add(uidto, mnu.get(i).getUid(), whoEdit);
                            if (insertMenu == false) {
                                resp = insertMenu;
                            }
                        }

                        json = gson.toJson(resp);
                        response.getWriter().write(json);

                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
