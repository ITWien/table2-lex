/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.utils;

import com.google.gson.Gson;
import com.twc.wms.dao.QUSMTCTRLDao;
import com.twc.wms.dao.WHDao;
import com.twc.wms.entity.WMS950;
import com.twc.wms.entity.WMS960;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author nutthawoot.noo
 */
public class ShowDataTables960 extends HttpServlet {

    private Gson gson;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        PrintWriter out = response.getWriter();
        gson = new Gson();
        String json = null;

        String mode = request.getParameter("mode");
        String user = request.getParameter("user");
        String wh = request.getParameter("wh");
        String matct = request.getParameter("matct");
        String plant = request.getParameter("plant");
        String rm = request.getParameter("rm");
        String id = request.getParameter("id");
        String uid = request.getParameter("uid");

        uid = "91333";
        try {
            if (mode != null) {
//                System.out.println("mode : " + mode);
//                System.out.println("user : " + user);
//                System.out.println("wh : " + wh);
//                System.out.println("matct : " + matct);
//                System.out.println("plant : " + plant);
//                System.out.println("rm : " + rm);
//                System.out.println("id : " + id);
//                System.out.println("-----End-----");

                if (mode.equals("Plant")) {
                    List<WMS960> AllList = new ArrayList<WMS960>();

                    QUSMTCTRLDao dao = new QUSMTCTRLDao();
                    List<WMS960> WHList = dao.findAll960Plant(user, wh, matct, plant, id);

                    for (int i = 0; i < WHList.size(); i++) {
                        String ID = "DET-" + user + "-" + wh + "-" + matct + "-" + plant + "-" + WHList.get(i).getRmN();

                        QUSMTCTRLDao dao2 = new QUSMTCTRLDao();
                        List<WMS960> WHList2 = dao2.findAll960RM(user, wh, matct, plant, WHList.get(i).getRmN(), ID);

                        AllList.addAll(WHList2);
                    }

                    AllList.addAll(WHList);

                    json = gson.toJson(AllList);
                    response.getWriter().write(json);

                } else if (mode.equals("RM Style")) {
                    QUSMTCTRLDao dao = new QUSMTCTRLDao();
                    List<WMS960> WHList = dao.findAll960RM(user, wh, matct, plant, rm, id);
                    json = gson.toJson(WHList);
                    response.getWriter().write(json);

                }
            } else {
                QUSMTCTRLDao dao = new QUSMTCTRLDao();
                List<WMS960> WHList = dao.findAll960(uid);
                json = gson.toJson(WHList);
                response.getWriter().write(json);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
