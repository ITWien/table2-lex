/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.database;

import java.sql.Connection;
import java.sql.SQLException;
import javax.naming.InitialContext;
import javax.sql.DataSource;

/**
 *
 * @author nutthawoot.noo
 */
public class MSSQLDbConnectionPool {

    public MSSQLDbConnectionPool() {
    }

    public static Connection getConnection() throws Exception {

        Connection connection = null;
        try {

            InitialContext cxt = new InitialContext();
            if (cxt == null) {
                throw new Exception("Uh oh -- no context!");
            }

            DataSource ds = (DataSource) cxt.lookup("java:/comp/env/jdbc/MSSQLDBCP");
            connection = ds.getConnection();
            if (ds == null) {
                throw new Exception("Data source not found!");
            }
        } catch (Exception e) {
            System.err.println("Error Poolling getConnection() ==> " + e.toString());
            e.printStackTrace();
        }

        return connection;
    }

}
