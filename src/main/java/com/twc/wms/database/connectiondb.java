/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author 93176
 */
public class connectiondb {

//    private String driver = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
//    private static String uri = "jdbc:sqlserver://10.11.9.174\\TWCSQL;DatabaseName=RMShipment";
//    private static String user = "shipment";
//    private static String pass = "sh!pment";
    protected Connection connect;

    public connectiondb() {
        try {
            Class.forName(databaseInfo.DRIVER);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void close() {
        try {
            System.out.println("closed");
            if (connect != null) {
                connect.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Connection getConnection() {
        try {
            System.out.println("connected");
            connect = DriverManager.getConnection(databaseInfo.URI, databaseInfo.USERNAME, databaseInfo.PASSWORD);
        } catch (SQLException e) {
            System.err.println("getConnection ==> \n\t" + e);
        }
        return connect;
    }

}
