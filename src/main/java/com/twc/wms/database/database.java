/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.database;

import java.sql.*;

/**
 *
 * @author nutthawoot.noo
 */
public class database {

    protected Connection connect;

    // define database driver and connect connection database
    public database() {

        try {

            Class.forName(databaseInfo.DRIVER);

            connect = DriverManager.getConnection(databaseInfo.URI, databaseInfo.USERNAME, databaseInfo.PASSWORD);

        } catch (Exception e) {

            e.printStackTrace();

            throw new RuntimeException(e);

        }

    }

    // close connection database
    public void close() {

        try {

            if (connect != null) {

                connect.close();

            }

        } catch (Exception e) {

            throw new RuntimeException(e);

        }

    }
}
