/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.dao;

import com.twc.wms.database.database;

import java.sql.*;

import java.util.ArrayList;

import java.util.List;

import com.twc.wms.entity.XMSWIHEAD;
import com.twc.wms.entity.SAPMAS;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import com.twc.wms.database.MSSQLDbConnectionPool;
import com.twc.wms.entity.Qdest;

/**
 *
 * @author nutthawoot.noo
 */
public class XMSWIHEADDao extends database {

    public List<Qdest> findByType(String wh, String shipDate) {

        List<Qdest> QdestList = new ArrayList<Qdest>();

        String sql = "SELECT *\n"
                + "FROM(\n"
                + "SELECT A.QDECOD, A.QDEDESC, A.QDETYPE, B.QDEDESC as QDEDESC2 \n"
                + "  ,(SELECT COUNT([XMSOTHDEST]) FROM [XMSOTHEAD] WHERE [XMSOTHDEST] = A.QDECOD AND FORMAT(XMSOTHSPDT,'yyyy-MM-dd') = '" + shipDate + "' AND XMSOTHWHS = '" + wh + "') AS CNT\n"
                + "  FROM QDEST as A\n"
                + "  LEFT JOIN QDESTYP as B \n"
                + "  ON A.QDETYPE = B.QDETYPE\n"
                + "  WHERE A.QDETYPE = '1'\n"
                + ")TMP \n"
                + "WHERE CNT > 0\n"
                + "ORDER BY QDECOD";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                Qdest p = new Qdest();

                p.setCode(result.getString("QDECOD"));

                p.setDesc(result.getString("QDEDESC"));

                p.setType(result.getString("QDETYPE") + " : " + result.getString("QDEDESC2"));

                QdestList.add(p);

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return QdestList;

    }

    public List<String> findDupQno(XMSWIHEAD head, String qno) {

        List<String> qnoList = new ArrayList<String>();

        String sql = "SELECT [XMSWIHQNO]\n"
                + "  FROM [RMShipment].[dbo].[XMSWIHEAD]\n"
                + "  WHERE FORMAT([XMSWIHSPDT],'yyyy-MM-dd') = '" + head.getXMSWIHSPDT() + "'\n"
                + "  AND [XMSWIHWHS] = '" + head.getXMSWIHWHS() + "'\n"
                + "  AND [XMSWIHROUND] = '" + head.getXMSWIHROUND() + "'\n"
                + "  AND [XMSWIHLICENNO] = '" + head.getXMSWIHLICENNO() + "'\n"
                + "  AND XMSWIHQNO != '" + qno + "'";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                String p = "";
                p = result.getString("XMSWIHQNO");
                qnoList.add(p);

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return qnoList;

    }

    public List<Qdest> findShipDate() {

        List<Qdest> objList = new ArrayList<Qdest>();

        String sql = "SELECT DISTINCT FORMAT([XMSOTHSPDT],'yyyy-MM-dd') AS CODE\n"
                + "  ,FORMAT([XMSOTHSPDT],'dd/MM/yyyy') AS DESCS\n"
                + "  FROM [RMShipment].[dbo].[XMSOTHEAD]\n"
                + "  ORDER BY CODE DESC";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                Qdest p = new Qdest();

                p.setCode(result.getString("CODE"));
                p.setDesc(result.getString("DESCS"));

                objList.add(p);

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return objList;

    }

    public List<Qdest> findRound(String wh, String shipDate) {

        List<Qdest> objList = new ArrayList<Qdest>();

        String sql = "SELECT DISTINCT [XMSOTHROUND]\n"
                + "  FROM [RMShipment].[dbo].[XMSOTHEAD]\n"
                + "  WHERE FORMAT(XMSOTHSPDT,'yyyy-MM-dd') = '" + shipDate + "' AND XMSOTHWHS = '" + wh + "'\n"
                + "  ORDER BY [XMSOTHROUND]";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                Qdest p = new Qdest();

                p.setCode(result.getString("XMSOTHROUND"));
//                p.setDesc(result.getString("QDEDESC"));

                objList.add(p);

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return objList;

    }

    public List<Qdest> findLino(String wh, String shipDate) {

        List<Qdest> objList = new ArrayList<Qdest>();

        String sql = "SELECT DISTINCT [XMSOTHLICENNO]\n"
                + "  FROM [RMShipment].[dbo].[XMSOTHEAD]\n"
                + "  LEFT JOIN [QDEST] ON [QDEST].[QDECOD] = [XMSOTHEAD].[XMSOTHDEST]\n"
                + "  WHERE FORMAT(XMSOTHSPDT,'yyyy-MM-dd') = '" + shipDate + "' AND XMSOTHWHS = '" + wh + "'\n"
                + "  AND [QDETYPE] = '1'\n"
                + "  ORDER BY [XMSOTHLICENNO]";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                Qdest p = new Qdest();

                p.setCode(result.getString("XMSOTHLICENNO"));
//                p.setDesc(result.getString("QDEDESC"));

                objList.add(p);

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return objList;

    }

    public boolean addDocno(String qno) {

        boolean result = false;

        String sql = "UPDATE [RMShipment].[dbo].[XMSWIHEAD]\n"
                + "SET [XMSWIHDOCNO] = (\n"
                + "                    SELECT FORMAT(CURRENT_TIMESTAMP,'yy')+'-'+FORMAT(CURRENT_TIMESTAMP,'MM')+'-'+FORMAT([QNBLAST]+1,'0000')\n"
                + "  FROM [QNBSER]\n"
                + "  WHERE [QNBTYPE] = REPLACE([XMSWIHWHS],'WH','W')\n"
                + "  AND [QNBGROUP] = FORMAT(CURRENT_TIMESTAMP,'MM')\n"
                + "  AND [QNBYEAR] = FORMAT(CURRENT_TIMESTAMP,'yyyy')\n"
                + "					)\n"
                + "  WHERE [XMSWIHQNO] = '" + qno + "' AND XMSWIHDOCNO IS NULL ";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public XMSWIHEAD findByQno(String qno) {

        XMSWIHEAD p = new XMSWIHEAD();

        String sql = "SELECT FORMAT([XMSWIHSPDT],'yyyy-MM-dd') AS [XMSWIHSPDT]\n"
                + "      ,[XMSWIHDEST]\n"
                + "      ,[XMSWIHROUND]\n"
                + "      ,[XMSWIHLICENNO]\n"
                + "      ,[XMSWIHWHS]\n"
                + "  FROM [RMShipment].[dbo].[XMSWIHEAD]\n"
                + "  WHERE [XMSWIHQNO] = '" + qno + "'";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                p.setXMSWIHSPDT(result.getString("XMSWIHSPDT"));
                p.setXMSWIHDEST(result.getString("XMSWIHDEST"));
                p.setXMSWIHROUND(result.getString("XMSWIHROUND"));
                p.setXMSWIHLICENNO(result.getString("XMSWIHLICENNO"));
                p.setXMSWIHWHS(result.getString("XMSWIHWHS"));

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return p;

    }

    public String findMaxQno() {

        String p = "";

        String sql = "SELECT ISNULL(MAX(XMSWIHQNO),0)+1 AS MAXQNO FROM XMSWIHEAD ";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                p = result.getString("MAXQNO");

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return p;

    }

    public List<XMSWIHEAD> findAll(String wh, String shipDate) {

        List<XMSWIHEAD> UAList = new ArrayList<XMSWIHEAD>();

        String sql = "SELECT [XMSWIHQNO]\n"
                + "      ,[XMSWIHWHS]\n"
                + "      ,[XMSWIHSPDT]\n"
                + "      ,[XMSWIHDEST] AS DEST\n"
                + "      ,[XMSWIHDEST]+' : '+[QDEDESC] AS [XMSWIHDEST]\n"
                + "      ,[XMSWIHROUND]\n"
                + "      ,[XMSWIHLICENNO]\n"
                + "      ,[XMSWIHDRIVER]\n"
                + "      ,[XMSWIHFOLLOWER]\n"
                + "      ,format([XMSWIHBAG],'#,#0') as [XMSWIHBAG]\n"
                + "      ,format([XMSWIHROLL],'#,#0') as [XMSWIHROLL]\n"
                + "      ,format([XMSWIHBOX],'#,#0') as [XMSWIHBOX]\n"
                + "      ,format([XMSWIHPCS],'#,#0') as [XMSWIHPCS]\n"
                + "      ,format([XMSWIHTOT],'#,#0') as [XMSWIHTOT]\n"
                + "      ,[XMSWIHUSER]+' : '+[USERS] as [XMSWIHUSER]\n"
                + "      ,[XMSWIHDOCNO]\n"
                + "  FROM [RMShipment].[dbo].[XMSWIHEAD]\n"
                + "  LEFT JOIN [QDEST] ON [QDEST].[QDECOD] = [XMSWIHEAD].[XMSWIHDEST]\n"
                + "  LEFT JOIN [MSSUSER] ON [MSSUSER].[USERID] = [XMSWIHEAD].[XMSWIHUSER]\n"
                + "  WHERE FORMAT([XMSWIHSPDT],'yyyy-MM-dd') = '" + shipDate + "'\n"
                + "  AND [XMSWIHWHS] = '" + wh + "'\n"
                + "UNION ALL\n"
                + "SELECT NULL,NULL,NULL,NULL,NULL,NULL,[XMSWIHLICENNO],'TOTAL : '+[XMSWIHLICENNO],NULL\n"
                + "	  ,[XMSWIHBAG]\n"
                + "      ,[XMSWIHROLL]\n"
                + "      ,[XMSWIHBOX]\n"
                + "      ,[XMSWIHPCS]\n"
                + "      ,[XMSWIHTOT]\n"
                + "	  ,NULL,NULL\n"
                + "FROM(\n"
                + "SELECT [XMSWIHLICENNO]\n"
                + "	  ,format(SUM(isnull([XMSWIHBAG],0)),'#,#0') as [XMSWIHBAG]\n"
                + "      ,format(SUM(isnull([XMSWIHROLL],0)),'#,#0') as [XMSWIHROLL]\n"
                + "      ,format(SUM(isnull([XMSWIHBOX],0)),'#,#0') as [XMSWIHBOX]\n"
                + "      ,format(SUM(isnull([XMSWIHPCS],0)),'#,#0') as [XMSWIHPCS]\n"
                + "      ,format(SUM(isnull([XMSWIHTOT],0)),'#,#0') as [XMSWIHTOT]\n"
                + "  FROM [RMShipment].[dbo].[XMSWIHEAD]\n"
                + "  LEFT JOIN [QDEST] ON [QDEST].[QDECOD] = [XMSWIHEAD].[XMSWIHDEST]\n"
                + "  LEFT JOIN [MSSUSER] ON [MSSUSER].[USERID] = [XMSWIHEAD].[XMSWIHUSER]\n"
                + "  WHERE FORMAT([XMSWIHSPDT],'yyyy-MM-dd') = '" + shipDate + "'\n"
                + "  AND [XMSWIHWHS] = '" + wh + "'\n"
                + "  GROUP BY [XMSWIHLICENNO]\n"
                + ")TMPTOT\n"
                + "ORDER BY [XMSWIHLICENNO],[XMSWIHQNO]";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                if (result.getString("XMSWIHDRIVER").contains("TOTAL")) {
                    XMSWIHEAD p = new XMSWIHEAD();

                    p.setXMSWIHQNO(result.getString("XMSWIHQNO"));
                    p.setXMSWIHWHS(result.getString("XMSWIHWHS"));
                    p.setXMSWIHSPDT(result.getString("XMSWIHSPDT"));
                    p.setXMSWIHDEST(result.getString("XMSWIHDEST"));
                    p.setXMSWIHROUND(result.getString("XMSWIHROUND"));
                    p.setXMSWIHLICENNO("<b style=\"opacity: 0;\">" + result.getString("XMSWIHLICENNO") + "</b>");
                    p.setXMSWIHDRIVER("<b style=\"color: #00399b;\">" + result.getString("XMSWIHDRIVER") + "</b>");
                    p.setXMSWIHFOLLOWER(result.getString("XMSWIHFOLLOWER"));
                    p.setXMSWIHBAG("<b style=\"color: #00399b;\">" + result.getString("XMSWIHBAG") + "</b>");
                    p.setXMSWIHROLL("<b style=\"color: #00399b;\">" + result.getString("XMSWIHROLL") + "</b>");
                    p.setXMSWIHBOX("<b style=\"color: #00399b;\">" + result.getString("XMSWIHBOX") + "</b>");
                    p.setXMSWIHPCS("<b style=\"color: #00399b;\">" + result.getString("XMSWIHPCS") + "</b>");
                    p.setXMSWIHTOT("<b style=\"color: #00399b;\">" + result.getString("XMSWIHTOT") + "</b>");
                    p.setXMSWIHUSER(result.getString("XMSWIHUSER"));

//                    p.setOptions("<a href=\"print?printBy=dest&wh=" + wh + "&shipDate=" + shipDate + "&dest=" + result.getString("DEST") + "\" target=\"_blank\"><i class=\"fa fa-print\" style=\"font-size:28px; padding-left: 10px; color: #c21b1b;\"></i></a>");
                    UAList.add(p);
                } else {
                    XMSWIHEAD p = new XMSWIHEAD();
                    p.setXMSWIHQNO(result.getString("XMSWIHQNO"));
                    p.setXMSWIHWHS(result.getString("XMSWIHWHS"));
                    p.setXMSWIHSPDT(result.getString("XMSWIHSPDT"));
                    p.setXMSWIHDEST(result.getString("XMSWIHDEST"));
                    p.setXMSWIHROUND(result.getString("XMSWIHROUND"));
                    p.setXMSWIHLICENNO(result.getString("XMSWIHLICENNO"));
                    p.setXMSWIHDRIVER(result.getString("XMSWIHDRIVER"));
                    p.setXMSWIHFOLLOWER(result.getString("XMSWIHFOLLOWER"));
                    p.setXMSWIHBAG(result.getString("XMSWIHBAG"));
                    p.setXMSWIHROLL(result.getString("XMSWIHROLL"));
                    p.setXMSWIHBOX(result.getString("XMSWIHBOX"));
                    p.setXMSWIHPCS(result.getString("XMSWIHPCS"));
                    p.setXMSWIHTOT(result.getString("XMSWIHTOT"));
                    p.setXMSWIHUSER(result.getString("XMSWIHUSER"));
                    p.setXMSWIHDOCNO(result.getString("XMSWIHDOCNO"));

                    p.setOptions("<a href=\"edit?qno=" + result.getString("XMSWIHQNO") + "\"><i class=\"fa fa-edit\" style=\"font-size:28px; padding-left: 10px\"></i></a>\n"
                            + "<a onclick=\"document.getElementById('myModal-del-" + result.getString("XMSWIHQNO") + "').style.display = 'block';\" style=\"cursor: pointer;\"><i class=\"fa fa-trash\" style=\"font-size:28px; padding-left: 10px\"></i></a>\n"
                            + "<a href=\"print?qno=" + result.getString("XMSWIHQNO") + "&wh=" + result.getString("XMSWIHWHS") + "\" target=\"_blank\"><i class=\"fa fa-print\" style=\"font-size:28px; padding-left: 10px\"></i></a>\n");

                    UAList.add(p);
                }

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public boolean edit(XMSWIHEAD head) {

        boolean result = false;

        String sql = "UPDATE XMSWIHEAD "
                + "SET [XMSWIHSPDT] = '" + head.getXMSWIHSPDT() + "'\n"
                + "      ,[XMSWIHDEST] = '" + head.getXMSWIHDEST() + "'\n"
                + "      ,[XMSWIHROUND] = '" + head.getXMSWIHROUND() + "'\n"
                + "      ,[XMSWIHLICENNO] = '" + head.getXMSWIHLICENNO() + "'\n"
                + "      ,[XMSWIHDRIVER] = '" + head.getXMSWIHDRIVER() + "'\n"
                + "      ,[XMSWIHFOLLOWER] = '" + head.getXMSWIHFOLLOWER() + "'\n"
                + "      ,[XMSWIHBAG] = '" + head.getXMSWIHBAG() + "'\n"
                + "      ,[XMSWIHROLL] = '" + head.getXMSWIHROLL() + "'\n"
                + "      ,[XMSWIHBOX] = '" + head.getXMSWIHBOX() + "'\n"
                + "      ,[XMSWIHTOT] = '" + head.getXMSWIHTOT() + "'\n"
                + "      ,[XMSWIHEDT] = CURRENT_TIMESTAMP\n"
                + "      ,[XMSWIHUSER] = '" + head.getXMSWIHUSER() + "'\n"
                + "WHERE [XMSWIHQNO] = '" + head.getXMSWIHQNO() + "'";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean add(XMSWIHEAD head) {

        boolean result = false;

        String sql = "INSERT INTO XMSWIHEAD "
                + "([XMSWIHCOM]\n"
                + "      ,[XMSWIHQNO]\n"
                + "      ,[XMSWIHWHS]\n"
                + "      ,[XMSWIHSPDT]\n"
                + "      ,[XMSWIHROUND]\n"
                + "      ,[XMSWIHLICENNO]\n"
                + "      ,[XMSWIHDRIVER]\n"
                + "      ,[XMSWIHFOLLOWER]\n"
                + "      ,[XMSWIHBAG]\n"
                + "      ,[XMSWIHROLL]\n"
                + "      ,[XMSWIHBOX]\n"
                + "      ,[XMSWIHPCS]\n"
                + "      ,[XMSWIHTOT]\n"
                + "      ,[XMSWIHEDT]\n"
                + "      ,[XMSWIHCDT]\n"
                + "      ,[XMSWIHUSER]) "
                + "VALUES('TWC'"
                + ",'" + head.getXMSWIHQNO() + "'"
                + ",'" + head.getXMSWIHWHS() + "'"
                + ",'" + head.getXMSWIHSPDT() + "'"
                + ",'" + head.getXMSWIHROUND() + "'"
                + ",'" + head.getXMSWIHLICENNO() + "'"
                + ",'" + head.getXMSWIHDRIVER() + "'"
                + ",'" + head.getXMSWIHFOLLOWER() + "'"
                + ",'" + head.getXMSWIHBAG() + "'"
                + ",'" + head.getXMSWIHROLL() + "'"
                + ",'" + head.getXMSWIHBOX() + "'"
                + ",'" + head.getXMSWIHPCS() + "'"
                + ",'" + head.getXMSWIHTOT() + "'"
                + ",CURRENT_TIMESTAMP"
                + ",CURRENT_TIMESTAMP"
                + ",'" + head.getXMSWIHUSER() + "') ";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public void delete(String qno) {

        String sql = "DELETE FROM XMSWIHEAD WHERE XMSWIHQNO = '" + qno + "'";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ps.executeUpdate();

        } catch (Exception e) {

            e.printStackTrace();

        }

    }
}
