package com.twc.wms.dao;

import com.twc.wms.database.database;
import com.twc.wms.entity.ISMBILLD;
import com.twc.wms.entity.ISMBILLH;
import com.twc.wms.entity.ISMBILLP;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONObject;

/**
 *
 * @author 93176
 */
public class ISM500Dao extends database {

    public boolean insBillHead(List<ISMBILLH> list, String uid) {

        boolean result = true;

        try {

            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO [ISMBILLH] ([ISHCONO],[VBELN],[FKART],[WAERK],[VKORG],[VTWEG],[FKDAT],[INCO1],[INCO2],[KURRF],[ZTERM],[LAND1],[BUKRS],[TAXK1],[NETWR],[ERNAM],[ERZET],[ERDAT],[KUNRG],[KUNAG],[SPART],[XBLNR],[ZUONR],[MWSBK],[FKSTO],[BUPLA],[NAMRG],[ADRNR],[ISHEDT],[ISHETM],[ISHCDT],[ISHCTM],[ISHUSER]) VALUES \n");

            for (int i = 0; i < list.size(); i++) {

                String VBELN = list.get(i).getVBELN();
                String FKART = list.get(i).getFKART();
                String WAERK = list.get(i).getWAERK();
                String VKORG = list.get(i).getVKORG();
                String VTWEG = list.get(i).getVTWEG();
                String FKDAT = list.get(i).getFKDAT();
                String INCO1 = list.get(i).getINCO1();
                String INCO2 = list.get(i).getINCO2();
                Double KURRF = list.get(i).getKURRF();
                String ZTERM = list.get(i).getZTERM();
                String LAND1 = list.get(i).getLAND1();
                String BUKRS = list.get(i).getBUKRS();
                String TAXK1 = list.get(i).getTAXK1();
                Double NETWR = list.get(i).getNETWR();
                String ERNAM = list.get(i).getERNAM();
                String ERZET = list.get(i).getERZET();
                String ERDAT = list.get(i).getERDAT();
                String KUNRG = list.get(i).getKUNRG();
                String KUNAG = list.get(i).getKUNAG();
                String SPART = list.get(i).getSPART();
                String XBLNR = list.get(i).getXBLNR();
                String ZUONR = list.get(i).getZUONR();
                Double MWSBK = list.get(i).getMWSBK();
                String FKSTO = list.get(i).getFKSTO();
                String BUPLA = list.get(i).getBUPLA();
                String NAMRG = list.get(i).getNAMRG();
                String ADRNR = list.get(i).getADRNR();

                sql.append("('TWC','").append(VBELN).append("','").append(FKART).append("','").append(WAERK).append("','").append(VKORG).append("','").append(VTWEG).append("','")
                        .append(FKDAT).append("','").append(INCO1).append("','").append(INCO2).append("',").append(KURRF).append(",'").append(ZTERM).append("','")
                        .append(LAND1).append("','").append(BUKRS).append("','").append(TAXK1).append("',").append(NETWR).append(",'").append(ERNAM).append("','")
                        .append(ERZET).append("','").append(ERDAT).append("','").append(KUNRG).append("','").append(KUNAG).append("','").append(SPART).append("','")
                        .append(XBLNR).append("','").append(ZUONR).append("',").append(MWSBK).append(",'").append(FKSTO).append("','").append(BUPLA).append("','")
                        .append(NAMRG).append("','").append(ADRNR).append("',FORMAT(CURRENT_TIMESTAMP,'yyyyMMdd'),FORMAT(CURRENT_TIMESTAMP,'HHmmss'),FORMAT(CURRENT_TIMESTAMP,'yyyyMMdd'),FORMAT(CURRENT_TIMESTAMP,'HHmmss'),")
                        .append("'").append(uid).append("' )");

                if (i != (list.size() - 1)) {
                    sql.append(",\n");
                }
            }

//            System.out.println(sql.toString());
            PreparedStatement ps = connect.prepareStatement(sql.toString());

            int record = ps.executeUpdate();

            if (record < 1) {
                result = false;
            }

            ps.close();
            connect.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;

    }

    public boolean insBillDetail(List<ISMBILLD> list, String uid) {

        boolean result = true;

        try {

            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO [ISMBILLD] ([ISDCONO],[VBELN],[POSNR],[UEPOS],[FKIMG],[VRKME],[UMVKZ],[UMVKN],[MEINS],[SMENG],[FKLMG],[LMENG],[NTGEW],[BRGEW],[GEWEI],[PRSDT],[FBUDA],[KURSK]\n"
                    + "      ,[NETWR],[VBELV],[POSNV],[VGBEL],[VGPOS],[VGTYP],[AUBEL],[AUPOS],[AUREF],[MATNR],[ARKTX],[PMATN],[CHARG],[MATKL],[PSTYV],[POSAR],[PRODH],[VSTEL],[SPART]\n"
                    + "      ,[POSPA],[WERKS],[ALAND],[TAXM1],[KTGRM],[VKGRP],[VKBUR],[SPARA],[ERNAM],[ERDAT],[ERZET],[BWTAR],[LGORT],[WAVWR],[STCUR],[PRCTR],[KVGR1],[KVGR2],[KVGR3],[KVGR4]\n"
                    + "      ,[KVGR5],[BONBA],[KOKRS],[MWSBP],[PO_NUMBER],[PO_DATE],[YOUR_REFERENCE],[PO_ITEM],[CUST_MAT],[KBETR],[ISDEDT],[ISDETM],[ISDCDT],[ISDCTM],[ISDUSER]) VALUES \n");

            for (int i = 0; i < list.size(); i++) {

                String VBELN = list.get(i).getVBELN();
                String POSNR = list.get(i).getPOSNR();
                String UEPOS = list.get(i).getUEPOS();
                Double FKIMG = list.get(i).getFKIMG();
                String VRKME = list.get(i).getVRKME();
                Double UMVKZ = list.get(i).getUMVKZ();
                Double UMVKN = list.get(i).getUMVKN();
                String MEINS = list.get(i).getMEINS();
                Double SMENG = list.get(i).getSMENG();
                Double FKLMG = list.get(i).getFKLMG();
                Double LMENG = list.get(i).getLMENG();
                Double NTGEW = list.get(i).getNTGEW();
                Double BRGEW = list.get(i).getBRGEW();
                String GEWEI = list.get(i).getGEWEI();
                String PRSDT = list.get(i).getPRSDT();
                String FBUDA = list.get(i).getFBUDA();
                Double KURSK = list.get(i).getKURSK();
                Double NETWR = list.get(i).getNETWR();
                String VBELV = list.get(i).getVBELV();
                String POSNV = list.get(i).getPOSNV();
                String VGBEL = list.get(i).getVGBEL();
                String VGPOS = list.get(i).getVGPOS();
                String VGTYP = list.get(i).getVGTYP();
                String AUBEL = list.get(i).getAUBEL();
                String AUPOS = list.get(i).getAUPOS();
                String AUREF = list.get(i).getAUREF();
                String MATNR = list.get(i).getMATNR();
                String ARKTX = list.get(i).getARKTX();
                String PMATN = list.get(i).getPMATN();
                String CHARG = list.get(i).getCHARG();
                String MATKL = list.get(i).getMATKL();
                String PSTYV = list.get(i).getPSTYV();
                String POSAR = list.get(i).getPOSAR();
                String PRODH = list.get(i).getPRODH();
                String VSTEL = list.get(i).getVSTEL();
                String SPART = list.get(i).getSPART();
                String POSPA = list.get(i).getPOSPA();
                String WERKS = list.get(i).getWERKS();
                String ALAND = list.get(i).getALAND();
                String TAXM1 = list.get(i).getTAXM1();
                String KTGRM = list.get(i).getKTGRM();
                String VKGRP = list.get(i).getVKGRP();
                String VKBUR = list.get(i).getVKBUR();
                String SPARA = list.get(i).getSPARA();
                String ERNAM = list.get(i).getERNAM();
                String ERDAT = list.get(i).getERDAT();
                String ERZET = list.get(i).getERZET();
                String BWTAR = list.get(i).getBWTAR();
                String LGORT = list.get(i).getLGORT();
                Double WAVWR = list.get(i).getWAVWR();
                Double STCUR = list.get(i).getSTCUR();
                String PRCTR = list.get(i).getPRCTR();
                String KVGR1 = list.get(i).getKVGR1();
                String KVGR2 = list.get(i).getKVGR2();
                String KVGR3 = list.get(i).getKVGR3();
                String KVGR4 = list.get(i).getKVGR4();
                String KVGR5 = list.get(i).getKVGR5();
                Double BONBA = list.get(i).getBONBA();
                String KOKRS = list.get(i).getKOKRS();
                Double MWSBP = list.get(i).getMWSBP();
                String PO_NUMBER = list.get(i).getPO_NUMBER();
                String PO_DATE = list.get(i).getPO_DATE();
                String YOUR_REFERENCE = list.get(i).getYOUR_REFERENCE();
                String PO_ITEM = list.get(i).getPO_ITEM();
                String CUST_MAT = list.get(i).getCUST_MAT();
                Double KBETR = list.get(i).getKBETR();

                sql.append("('TWC','").append(VBELN).append("','").append(POSNR).append("','").append(UEPOS).append("',").append(FKIMG).append(",'").append(VRKME).append("',")
                        .append(UMVKZ).append(",").append(UMVKN).append(",'").append(MEINS).append("',").append(SMENG).append(",").append(FKLMG).append(",").append(LMENG).append(",")
                        .append(NTGEW).append(",").append(BRGEW).append(",'").append(GEWEI).append("','").append(PRSDT).append("','").append(FBUDA).append("',").append(KURSK).append(",")
                        .append(NETWR).append(",'").append(VBELV).append("','").append(POSNV).append("','").append(VGBEL).append("','").append(VGPOS).append("','").append(VGTYP).append("','")
                        .append(AUBEL).append("','").append(AUPOS).append("','").append(AUREF).append("','").append(MATNR).append("','").append(ARKTX.replaceAll("'", "''")).append("','").append(PMATN).append("','")
                        .append(CHARG).append("','").append(MATKL).append("','").append(PSTYV).append("','").append(POSAR).append("','").append(PRODH).append("','").append(VSTEL).append("','")
                        .append(SPART).append("','").append(POSPA).append("','").append(WERKS).append("','").append(ALAND).append("','").append(TAXM1).append("','").append(KTGRM).append("','")
                        .append(VKGRP).append("','").append(VKBUR).append("','").append(SPARA).append("','").append(ERNAM).append("','").append(ERDAT).append("','").append(ERZET).append("','")
                        .append(BWTAR).append("','").append(LGORT).append("',").append(WAVWR).append(",").append(STCUR).append(",'").append(PRCTR).append("','").append(KVGR1).append("','")
                        .append(KVGR2).append("','").append(KVGR3).append("','").append(KVGR4).append("','").append(KVGR5).append("',").append(BONBA).append(",'").append(KOKRS).append("',")
                        .append(MWSBP).append(",'").append(PO_NUMBER).append("','").append(PO_DATE).append("','").append(YOUR_REFERENCE).append("','").append(PO_ITEM).append("','").append(CUST_MAT).append("',")
                        .append(KBETR).append(",FORMAT(CURRENT_TIMESTAMP,'yyyyMMdd'),FORMAT(CURRENT_TIMESTAMP,'HHmmss'),FORMAT(CURRENT_TIMESTAMP,'yyyyMMdd'),FORMAT(CURRENT_TIMESTAMP,'HHmmss'),").append("'").append(uid).append("' )");

                if (i != (list.size() - 1)) {
                    sql.append(",\n");
                }
            }

//            System.out.println(sql.toString());
            PreparedStatement ps = connect.prepareStatement(sql.toString());

            int record = ps.executeUpdate();

            if (record < 1) {
                result = false;
            }

            ps.close();
            connect.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;

    }

    public boolean insBillPartners(List<ISMBILLP> list, String uid) {

        boolean result = true;

        try {

            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO [ISMBILLP] ([ISPCONO],[VBELN],[POSNR],[PARVW],[KUNNR],[ADRNR],[NAME1],[NAME2],[NAME3],[NAME4],[CITY1],[CITY2],[POST_CODE1],[STREET],[COUNTRY],[LANGU],[REGION],[SORT1],[TEL_NUMBER]\n"
                    + "      ,[TEL_EXTENS],[FAX_NUMBER],[FAX_EXTENS],[EXTENSION1],[EXTENSION2],[ISPEDT],[ISPETM],[ISPCDT],[ISPCTM],[ISPUSER]) VALUES \n");

            for (int i = 0; i < list.size(); i++) {

                String VBELN = list.get(i).getVBELN();
                String POSNR = list.get(i).getPOSNR();
                String PARVW = list.get(i).getPARVW();
                String KUNNR = list.get(i).getKUNNR();
                String ADRNR = list.get(i).getADRNR();
                String NAME1 = list.get(i).getNAME1();
                String NAME2 = list.get(i).getNAME2();
                String NAME3 = list.get(i).getNAME3();
                String NAME4 = list.get(i).getNAME4();
                String CITY1 = list.get(i).getCITY1();
                String CITY2 = list.get(i).getCITY2();
                String POST_CODE1 = list.get(i).getPOST_CODE1();
                String STREET = list.get(i).getSTREET();
                String COUNTRY = list.get(i).getCOUNTRY();
                String LANGU = list.get(i).getLANGU();
                String REGION = list.get(i).getREGION();
                String SORT1 = list.get(i).getSORT1();
                String TEL_NUMBER = list.get(i).getTEL_NUMBER();
                String TEL_EXTENS = list.get(i).getTEL_EXTENS();
                String FAX_NUMBER = list.get(i).getFAX_NUMBER();
                String FAX_EXTENS = list.get(i).getFAX_EXTENS();
                String EXTENSION1 = list.get(i).getEXTENSION1();
                String EXTENSION2 = list.get(i).getEXTENSION2();

                sql.append("('TWC','").append(VBELN).append("','").append(POSNR).append("','").append(PARVW).append("','").append(KUNNR).append("','").append(ADRNR).append("','")
                        .append(NAME1).append("','").append(NAME2).append("','").append(NAME3).append("','").append(NAME4).append("','").append(CITY1).append("','")
                        .append(CITY2).append("','").append(POST_CODE1).append("','").append(STREET).append("','").append(COUNTRY).append("','").append(LANGU).append("','")
                        .append(REGION).append("','").append(SORT1).append("','").append(TEL_NUMBER).append("','").append(TEL_EXTENS).append("','").append(FAX_NUMBER).append("','")
                        .append(FAX_EXTENS).append("','").append(EXTENSION1).append("','").append(EXTENSION2)
                        .append("',FORMAT(CURRENT_TIMESTAMP,'yyyyMMdd'),FORMAT(CURRENT_TIMESTAMP,'HHmmss'),FORMAT(CURRENT_TIMESTAMP,'yyyyMMdd'),FORMAT(CURRENT_TIMESTAMP,'HHmmss'),")
                        .append("'").append(uid).append("' )");

                if (i != (list.size() - 1)) {
                    sql.append(",\n");
                }
            }

//            System.out.println(sql.toString());
            PreparedStatement ps = connect.prepareStatement(sql.toString());

            int record = ps.executeUpdate();

            if (record < 1) {
                result = false;
            }

            ps.close();
            connect.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;

    }

    public List<ISMBILLH> HeadData(String invno) {

        List<ISMBILLH> UAList = new ArrayList<ISMBILLH>();

        String sql = "SELECT  H.VBELN ,(SELECT TOP 1 AUBEL FROM [RMShipment].[dbo].[ISMBILLD] D WHERE D.VBELN = H.VBELN) AS SALESORDER,FKDAT,NAMRG,KUNRG\n"
                + ",(SELECT TOP 1 STREET FROM [RMShipment].[dbo].[ISMBILLP] P WHERE P.VBELN = H.VBELN) AS STREET\n"
                + ",(SELECT TOP 1 CITY2 FROM [RMShipment].[dbo].[ISMBILLP] P WHERE P.VBELN = H.VBELN) AS CITY2\n"
                + ",(SELECT TOP 1 CITY1 FROM [RMShipment].[dbo].[ISMBILLP] P WHERE P.VBELN = H.VBELN) AS CITY1\n"
                + ",(SELECT TOP 1 POST_CODE1 FROM [RMShipment].[dbo].[ISMBILLP] P WHERE P.VBELN = H.VBELN) AS POSTC\n"
                + "FROM [RMShipment].[dbo].[ISMBILLH] H WHERE H.VBELN = '" + invno.trim() + "'";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                ISMBILLH p = new ISMBILLH();
                p.setVBELN(result.getString("VBELN"));
                p.setISHEDT(result.getString("SALESORDER"));
                String fkdate = result.getString("FKDAT");

                if (!fkdate.equals("")) {
                    p.setFKDAT(fkdate.split("-")[2] + "." + fkdate.split("-")[1] + "." + fkdate.split("-")[0]);
                } else {
                    p.setFKDAT("Not Found FKDATE");
                }

                p.setNAMRG(result.getString("NAMRG"));
                p.setKUNRG(result.getString("KUNRG"));
                p.setISHETM(result.getString("STREET"));
                p.setISHCDT(result.getString("CITY2"));
                p.setISHCTM(result.getString("CITY1"));
                p.setISHUSER(result.getString("POSTC"));

                UAList.add(p);

            }
            ps.close();
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public List<ISMBILLH> HeadData510(String invno, String invto) {

        List<ISMBILLH> UAList = new ArrayList<ISMBILLH>();

        String sql = "SELECT  H.VBELN ,(SELECT TOP 1 AUBEL FROM [RMShipment].[dbo].[ISMBILLD] D WHERE D.VBELN = H.VBELN) AS SALESORDER,FKDAT,NAMRG,KUNRG\n"
                + ",(SELECT TOP 1 STREET FROM [RMShipment].[dbo].[ISMBILLP] P WHERE P.VBELN = H.VBELN) AS STREET\n"
                + ",(SELECT TOP 1 CITY2 FROM [RMShipment].[dbo].[ISMBILLP] P WHERE P.VBELN = H.VBELN) AS CITY2\n"
                + ",(SELECT TOP 1 CITY1 FROM [RMShipment].[dbo].[ISMBILLP] P WHERE P.VBELN = H.VBELN) AS CITY1\n"
                + ",(SELECT TOP 1 POST_CODE1 FROM [RMShipment].[dbo].[ISMBILLP] P WHERE P.VBELN = H.VBELN) AS POSTC\n"
                + "FROM [RMShipment].[dbo].[ISMBILLH] H WHERE H.VBELN BETWEEN '" + invno.trim() + "' AND '" + invto.trim() + "'";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                ISMBILLH p = new ISMBILLH();
                p.setVBELN(result.getString("VBELN"));
                p.setISHEDT(result.getString("SALESORDER"));
                String fkdate = result.getString("FKDAT");

                if (!fkdate.equals("")) {
                    p.setFKDAT(fkdate.split("-")[2] + "." + fkdate.split("-")[1] + "." + fkdate.split("-")[0]);
                } else {
                    p.setFKDAT("Not Found FKDATE");
                }

                p.setNAMRG(result.getString("NAMRG"));
                p.setKUNRG(result.getString("KUNRG"));
                p.setISHETM(result.getString("STREET"));
                p.setISHCDT(result.getString("CITY2"));
                p.setISHCTM(result.getString("CITY1"));
                p.setISHUSER(result.getString("POSTC"));

                UAList.add(p);

            }
            ps.close();
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public List<ISMBILLD> DetailData(String invno) {

        List<ISMBILLD> UAList = new ArrayList<ISMBILLD>();

        String sql = "SELECT D.VBELN,D.MATNR,D.ARKTX,D.LMENG,0.00 AS REALL,D.VGBEL AS DELI,D.FKLMG,D.KBETR,D.BONBA,D.MWSBP\n"
                + "FROM [RMShipment].[dbo].[ISMBILLD] D WHERE D.VBELN = '" + invno.trim() + "'";

        try {

//            System.out.println(sql);
            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                ISMBILLD p = new ISMBILLD();
                p.setVBELN(result.getString("VBELN"));
                p.setMATNR(result.getString("MATNR"));
                p.setARKTX(result.getString("ARKTX"));
                p.setLMENG(result.getDouble("LMENG"));
                p.setISDETM(result.getString("REALL"));
                p.setVGBEL(result.getString("DELI"));
                p.setFKLMG(result.getDouble("FKLMG"));
                p.setKBETR(result.getDouble("KBETR"));
                p.setBONBA(result.getDouble("BONBA"));
                p.setMWSBP(result.getDouble("MWSBP"));

                UAList.add(p);

            }
            ps.close();
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public List<ISMBILLD> DetailData510(String invno, String invto) {

        List<ISMBILLD> UAList = new ArrayList<ISMBILLD>();

        String sql = "SELECT H.KUNRG,D.VBELN,D.MATNR,D.ARKTX,SUM(D.LMENG) AS LMENG,0.00 AS REALL,D.VGBEL AS DELI,SUM(D.FKLMG) AS FKLMG--,D.KBETR,D.BONBA,D.MWSBP\n"
                + ",(SELECT SUM(LMENG) FROM [RMShipment].[dbo].[ISMBILLD] D2 WHERE D2.MATNR = D.MATNR AND D2.VBELN BETWEEN '" + invno.trim() + "' AND '" + invto.trim() + "') AS SUMLMENG\n"
                + ",(SELECT SUM(FKLMG) FROM [RMShipment].[dbo].[ISMBILLD] D2 WHERE D2.MATNR = D.MATNR AND D2.VBELN BETWEEN '" + invno.trim() + "' AND '" + invto.trim() + "') AS SUMFKLMG\n"
                + ",H.NAMRG\n"
                + ",D.AUBEL AS SALESORDER\n"
                + "FROM [RMShipment].[dbo].[ISMBILLD] D \n"
                + "LEFT JOIN [RMShipment].[dbo].[ISMBILLH] H ON H.VBELN = D.VBELN\n"
                + "WHERE (D.VBELN BETWEEN '" + invno.trim() + "' AND '" + invto.trim() + "') /*AND MATNR = 'WU2986X5NN'*/\n"
                + "GROUP BY H.KUNRG,H.NAMRG,D.VBELN,D.AUBEL,D.MATNR,D.ARKTX,D.VGBEL\n"
                + "ORDER BY H.KUNRG,D.MATNR,D.AUBEL,D.VBELN"; //order by customer,matcode,saleorder,invoice
//        String sql = "SELECT D.VBELN,D.MATNR,D.ARKTX,D.LMENG,0.00 AS REALL,D.VGBEL AS DELI,D.FKLMG,D.KBETR,D.BONBA,D.MWSBP\n"
//                + ",(SELECT TOP 1 KUNRG FROM [RMShipment].[dbo].[ISMBILLH] WHERE VBELN = D.VBELN) AS KUNRG\n"
//                + "FROM [RMShipment].[dbo].[ISMBILLD] D WHERE D.VBELN BETWEEN '" + invno.trim() + "' AND '" + invto.trim() + "' "
//                + "ORDER BY D.VBELN,MATNR ";

        try {

//            System.out.println(sql);
            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                ISMBILLD p = new ISMBILLD();
                p.setVBELN(result.getString("VBELN"));
                p.setMATNR(result.getString("MATNR"));
                p.setARKTX(result.getString("ARKTX"));
                p.setLMENG(result.getDouble("LMENG"));
                p.setISDETM(result.getString("REALL"));
                p.setVGBEL(result.getString("DELI"));
                p.setFKLMG(result.getDouble("FKLMG"));
                p.setKBETR(result.getDouble("SUMLMENG")); //SUMLMENG
                p.setBONBA(result.getDouble("SUMFKLMG")); //SUMFKLMG
//                p.setMWSBP(result.getDouble("MWSBP"));
                p.setCUST_MAT(result.getString("KUNRG")); // customer code
                p.setCUST_CODE(result.getString("NAMRG"));
                p.setSALESORDER(result.getString("SALESORDER"));

                UAList.add(p);

            }
            ps.close();
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public List<ISMBILLD> CheckDetailData(String invno) {

        List<ISMBILLD> UAList = new ArrayList<ISMBILLD>();

        String sql = "SELECT TOP 1 VBELN FROM [RMShipment].[dbo].[ISMBILLD] WHERE VBELN = '" + invno.trim() + "'";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);
            ResultSet result = ps.executeQuery();

            while (result.next()) {

                ISMBILLD p = new ISMBILLD();
                p.setVBELN(result.getString("VBELN"));

                UAList.add(p);

            }
            ps.close();
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public List<ISMBILLD> CheckDetailData510(String invnoFrom, String invnoTo) {

        List<ISMBILLD> UAList = new ArrayList<ISMBILLD>();

        String sql = "SELECT DISTINCT VBELN FROM [RMShipment].[dbo].[ISMBILLD] WHERE VBELN BETWEEN '" + invnoFrom.trim() + "' AND '" + invnoTo.trim() + "'";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);
            ResultSet result = ps.executeQuery();

            while (result.next()) {

                ISMBILLD p = new ISMBILLD();
                p.setVBELN(result.getString("VBELN"));

                UAList.add(p);

            }
            ps.close();
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public JSONObject DelHDP(String invno) { //delete all by invno

        JSONObject res = new JSONObject();

        String sqlHead = "DELETE [RMShipment].[dbo].[ISMBILLH] WHERE VBELN = '" + invno + "'";
        String sqlDetail = "DELETE [RMShipment].[dbo].[ISMBILLD] WHERE VBELN = '" + invno + "'";
        String sqlPartner = "DELETE [RMShipment].[dbo].[ISMBILLP] WHERE VBELN = '" + invno + "'";

        try {

            Statement stmt = connect.createStatement();

            int recordH = stmt.executeUpdate(sqlHead);
            int recordD = stmt.executeUpdate(sqlDetail);
            int recordP = stmt.executeUpdate(sqlPartner);

            if (recordH > 0) {
                res.put("resDelH", true);
            } else {
                res.put("resDelH", false);
            }

            if (recordD > 0) {
                res.put("resDelD", true);
            } else {
                res.put("resDelD", false);
            }

            if (recordP > 0) {
                res.put("resDelP", true);
            } else {
                res.put("resDelP", false);
            }

            stmt.close();
            connect.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return res;

    }

}
