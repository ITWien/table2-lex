/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.dao;

import com.twc.wms.database.database;
import java.sql.*;

import java.util.ArrayList;

import java.util.List;

import com.twc.wms.entity.Warehouse;

/**
 *
 * @author nutthawoot.noo
 */
public class WarehouseDao extends database {

    public List<Warehouse> selectWarehouse() {

        List<Warehouse> WHList = new ArrayList<Warehouse>();

        String sql = "SELECT * FROM QWHMAS";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                Warehouse p = new Warehouse();

                p.setName(result.getString("QWHNAME"));

                p.setCode(result.getString("QWHCOD"));

                WHList.add(p);

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return WHList;

    }

    public String findWHname(String whcod) {

        String whname = "";

        String sql = "SELECT * FROM QWHMAS WHERE QWHCOD = ?";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ps.setString(1, whcod);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                whname = result.getString("QWHNAME");

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return whname;

    }

}
