/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.dao;

import com.twc.wms.database.database;
import com.twc.wms.entity.QSET;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 *
 * @author nutthawoot.noo
 */
public class QSETDao extends database {

    public List<QSET> findAll() {

        List<QSET> QsetList = new ArrayList<QSET>();

        String sql = "SELECT * FROM QSETHD ";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                QSET p = new QSET();

                p.setCode(result.getString("QSECOD"));
                p.setTotal(result.getString("QSETOTSQ"));

                QsetList.add(p);

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return QsetList;

    }

    public List<QSET> findAllSeq(String cod) {

        List<QSET> QsetList = new ArrayList<QSET>();

        String sql = "SELECT * FROM QSETLN "
                + "WHERE QSECOD = '" + cod + "'";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                QSET p = new QSET();

                p.setSq1(result.getString("QSESEQ"));

                QsetList.add(p);

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return QsetList;

    }

    public String check(String code) {
        PreparedStatement ps = null;
        ResultSet rs = null;
        String STRRETURN = null;
        try {
            String check = "Select * FROM [RMShipment].[dbo].[QSETHD] where [QSECOD] = '" + code + "'";
            ps = connect.prepareStatement(check);
            rs = ps.executeQuery();
            if (rs.next()) {
                STRRETURN = "f";
            } else {
                STRRETURN = "t";
            }
        } catch (Exception exp) {
            exp.printStackTrace();
        }
        return STRRETURN;
    }

    public String checkSeq(String code, int lno, String seq) {
        PreparedStatement ps = null;
        ResultSet rs = null;
        String STRRETURN = null;
        try {
            String check = "Select * FROM [RMShipment].[dbo].[QSETLN] where [QSECOD] = '" + code + "' "
                    + "AND [QSELNNO] = " + lno + " ";
            ps = connect.prepareStatement(check);
            rs = ps.executeQuery();
            if (rs.next()) {
                STRRETURN = "f";
            } else {
                STRRETURN = "t";
            }
        } catch (Exception exp) {
            exp.printStackTrace();
        }
        return STRRETURN;
    }

    public boolean add(QSET qset, String uid) {

        boolean result = false;

        String sql = "INSERT INTO QSETHD"
                + " (QSECOM, QSECOD, QSETOTSQ, QSEEDT, QSECDT, QSEUSER)"
                + " VALUES(?, ?, ?, ?, ?, ?)";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ps.setString(1, "TWC");

            ps.setString(2, qset.getCode());

            ps.setString(3, qset.getTotal());

            Calendar cal = Calendar.getInstance();
            Timestamp timestamp = new Timestamp(cal.getTimeInMillis());

            ps.setTimestamp(4, timestamp);

            ps.setTimestamp(5, timestamp);

            ps.setString(6, uid);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean addSeq(QSET qset, String uid) {

        boolean result = false;

        String sql = "INSERT INTO QSETLN"
                + " (QSECOM, QSECOD, QSELNNO, QSESEQ, QSEEDT, QSECDT, QSEUSER)"
                + " VALUES(?, ?, ?, ?, ?, ?, ?)";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ps.setString(1, "TWC");

            ps.setString(2, qset.getCode());

            ps.setInt(3, qset.getLno());

            ps.setString(4, qset.getSq1());

            Calendar cal = Calendar.getInstance();
            Timestamp timestamp = new Timestamp(cal.getTimeInMillis());

            ps.setTimestamp(5, timestamp);

            ps.setTimestamp(6, timestamp);

            ps.setString(7, uid);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public QSET findByCod(String cod) {

        QSET qd = new QSET();

        String sql = "SELECT * FROM QSETHD WHERE QSECOD = ? ";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ps.setString(1, cod);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                qd.setCode(result.getString("QSECOD"));

                qd.setTotal(result.getString("QSETOTSQ"));

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return qd;

    }

    public boolean edit(QSET qset, String uid) {

        boolean result = false;

        String sql = "UPDATE QSETHD SET QSETOTSQ = ?, QSEEDT = ?, QSEUSER = ? "
                + "WHERE QSECOD = ? ";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ps.setString(4, qset.getCode());

            ps.setString(1, qset.getTotal());

            Calendar cal = Calendar.getInstance();
            Timestamp timestamp = new Timestamp(cal.getTimeInMillis());

            ps.setTimestamp(2, timestamp);

            ps.setString(3, uid);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean editSeq(QSET qset) {

        boolean result = false;

        String sql = "UPDATE QSETLN SET QSESEQ = ?, QSEEDT = ? "
                + "WHERE QSECOD = ? AND QSELNNO = ?";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ps.setString(3, qset.getCode());

            ps.setInt(4, qset.getLno());

            ps.setString(1, qset.getSq1());

            Calendar cal = Calendar.getInstance();
            Timestamp timestamp = new Timestamp(cal.getTimeInMillis());

            ps.setTimestamp(2, timestamp);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public void delete(String cod) {

        String sql = "DELETE FROM QSETHD "
                + "WHERE QSECOD = ? ";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ps.setString(1, cod);

            ps.executeUpdate();

        } catch (Exception e) {

            e.printStackTrace();

        }

    }

    public void deleteSeq(String cod) {

        String sql = "DELETE FROM QSETLN "
                + "WHERE QSECOD = ? ";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ps.setString(1, cod);

            ps.executeUpdate();

        } catch (Exception e) {

            e.printStackTrace();

        }

    }
}
