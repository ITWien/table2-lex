/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.twc.wms.dao;

import com.twc.wms.database.database;
import com.twc.wms.entity.PGMU;
import com.twc.wms.entity.UserAuth;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 *
 * @author 93176
 */
public class WMS009Dao extends database {

    public List<UserAuth> findAll() {

        List<UserAuth> UAList = new ArrayList<UserAuth>();

        String sql = "SELECT * FROM MSSUSER "
                + "INNER JOIN QWHMAS ON MSSUSER.MSWHSE=QWHMAS.QWHCOD WHERE MSCONO = 'TWC' "
                + "ORDER BY USERID";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                UserAuth p = new UserAuth();

                p.setUid(result.getString("USERID"));

                p.setName(result.getString("USERS"));

                p.setLevel(Integer.toString(result.getInt("MSLEVEL")));

                p.setWarehouse(result.getString("MSWHSE") + " : " + result.getString("QWHNAME"));

                p.setDepartment(result.getString("MSDPARTNO"));

                p.setMenuset(result.getString("MSMENUSET"));

                UAList.add(p);

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public List<PGMU> findAll(String uid) {

        List<PGMU> UAList = new ArrayList<PGMU>();

        String sql = "SELECT * FROM QPGMNU WHERE QPGUSERID = ?";

        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            ps.setString(1, uid);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                PGMU p = new PGMU();

                p.setUid(result.getString("QPGPGID"));

                UAList.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public List<PGMU> CheckMenuByuid(String uid) {

        List<PGMU> UAList = new ArrayList<PGMU>();

        String sql = "SELECT TOP 1 * FROM QPGMNU WHERE QPGUSERID = ?";

        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            ps.setString(1, uid);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                PGMU p = new PGMU();

                p.setUid(result.getString("QPGPGID"));

                UAList.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public boolean add(String Nuid, String pgId, String whoEdit) {

        boolean result = false;

        String sql = "INSERT INTO QPGMNU (QPGUSERID, QPGPGID, QPGEDT, QPGCDT, QPGUSER) VALUES(?, ?, ?, ?, ?)";

        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            ps.setString(1, Nuid);

            ps.setString(2, pgId);

            Calendar cal = Calendar.getInstance();
            Timestamp timestamp = new Timestamp(cal.getTimeInMillis());

            ps.setTimestamp(3, timestamp);

            ps.setTimestamp(4, timestamp);

            ps.setString(5, whoEdit);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

}
