/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.dao;

import com.twc.wms.database.database;
import com.twc.wms.entity.QRKFEATURE;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 *
 * @author nutthawoot.noo
 */
public class QRKFEATUREDao extends database {

    public String check(String wh) {
        PreparedStatement ps = null;
        ResultSet rs = null;
        String STRRETURN = null;
        try {
            String check = "Select * FROM [RMShipment].[dbo].[QRKFEATURE] where [QRKWHS] = '" + wh + "'";
            ps = connect.prepareStatement(check);
            rs = ps.executeQuery();
            if (rs.next()) {
                STRRETURN = "f";
            } else {
                STRRETURN = "t";
            }
        } catch (Exception exp) {
            exp.printStackTrace();
        }
        return STRRETURN;
    }

    public boolean add(QRKFEATURE qdest, String uid) {

        boolean result = false;

        String sql = "INSERT INTO QRKFEATURE "
                + "(QRKCOM, QRKWHS, QRKFRACK, QRKTRACK, QRKFSIDE, QRKTSIDE, "
                + "QRKFCOL, QRKTCOL, QRKFRAW, QRKTRAW, QRKEDT, QRKCDT, QRKUSER)"
                + " VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ps.setString(1, "TWC");

            ps.setString(2, qdest.getWH());

            ps.setString(3, qdest.getSrkno());

            ps.setString(4, qdest.getFrkno());

            ps.setString(5, qdest.getSside());

            ps.setString(6, qdest.getFside());

            ps.setString(7, qdest.getScolumn());

            ps.setString(8, qdest.getFcolumn());

            ps.setString(9, qdest.getSrow());

            ps.setString(10, qdest.getFrow());

            Calendar cal = Calendar.getInstance();
            Timestamp timestamp = new Timestamp(cal.getTimeInMillis());

            ps.setTimestamp(11, timestamp);

            ps.setTimestamp(12, timestamp);

            ps.setString(13, uid);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public QRKFEATURE findByCod(String wh) {

        QRKFEATURE qd = new QRKFEATURE();

        String sql = "SELECT * FROM QRKFEATURE WHERE QRKWHS = ? ";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ps.setString(1, wh);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                qd.setWH(result.getString("QRKWHS"));

                qd.setSrkno(result.getString("QRKFRACK"));

                qd.setFrkno(result.getString("QRKTRACK"));

                qd.setSside(result.getString("QRKFSIDE"));

                qd.setFside(result.getString("QRKTSIDE"));

                qd.setScolumn(result.getString("QRKFCOL"));

                qd.setFcolumn(result.getString("QRKTCOL"));

                qd.setSrow(result.getString("QRKFRAW"));

                qd.setFrow(result.getString("QRKTRAW"));

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return qd;

    }

    public boolean edit(QRKFEATURE qdest, String uid) {

        boolean result = false;

        String sql = "UPDATE QRKFEATURE SET QRKFRACK = ?, QRKTRACK = ?, "
                + "QRKFSIDE = ?, QRKTSIDE = ?, QRKFCOL = ?, QRKTCOL = ?, QRKFRAW = ?, QRKTRAW = ?, QRKEDT = ?, QRKUSER = ? "
                + "WHERE QRKWHS = ?";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ps.setString(11, qdest.getWH());

            ps.setString(1, qdest.getSrkno());

            ps.setString(2, qdest.getFrkno());

            ps.setString(3, qdest.getSside());

            ps.setString(4, qdest.getFside());

            ps.setString(5, qdest.getScolumn());

            ps.setString(6, qdest.getFcolumn());

            ps.setString(7, qdest.getSrow());

            ps.setString(8, qdest.getFrow());

            Calendar cal = Calendar.getInstance();
            Timestamp timestamp = new Timestamp(cal.getTimeInMillis());
            ps.setTimestamp(9, timestamp);

            ps.setString(10, uid);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }
}
