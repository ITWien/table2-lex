package com.twc.wms.dao;

import com.twc.wms.database.database;
import com.twc.wms.entity.ISMMASD;
import com.twc.wms.entity.ISMMASH;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author 93176
 */
public class ISM910Dao extends database {

    public List<ISMMASD> GetMasDet(String param, String toDeep) {

        List<ISMMASD> UAList = new ArrayList<ISMMASD>();

        String sql = "SELECT *"
                + ",isnull((SELECT TOP 1 LEFT([SAPDESC],10) from [SAPMAS] WHERE [SAPMAT]=[ISDITNO] collate SQL_Latin1_General_CP1_CI_AS),'') as [ISDDESC]\n"
                + ",isnull((SELECT TOP 1 ISHTRDT from [ISMMASH] WHERE ISHORD = ISDORD),0) as trancDate \n"
                + ",ISSDESC,isnull(case when [ISDSTS] = 0 then '-o' else '' end,'') as [LSTSI]\n"
                + "FROM [RMShipment].[dbo].[ISMMASD] LEFT JOIN [RMShipment].[dbo].[ISMSTS] ON ISDSTS = ISSSTS " + toDeep + " ORDER BY ISDORD,ISDLINO";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);
            ps.setString(1, param);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                ISMMASD p = new ISMMASD();

                p.setISDCONO(result.getString("ISDCONO"));
                p.setISDORD(result.getString("ISDORD"));
                p.setISDSEQ(result.getString("ISDSEQ"));

                String lineStr = result.getString("ISDLINO");

                if (lineStr.length() > 3) {
                    lineStr = lineStr.substring(lineStr.length() - 3);
                }

                p.setISDLINO(lineStr);
                p.setISDSINO(result.getString("ISDSINO"));
                p.setISDITNO(result.getString("ISDITNO"));
                p.setISDPLANT(result.getString("ISDPLANT"));

                if (result.getString("ISDVT") == null) {
                    p.setISDVT("");
                } else {
                    if (result.getString("ISDVT").equals("null")) {
                        p.setISDVT("");
                    } else {
                        p.setISDVT(result.getString("ISDVT"));
                    }
                }

                p.setISDSTRG(result.getString("ISDSTRG"));
                p.setISDRQQTY(result.getString("ISDRQQTY"));
                p.setISDUNIT(result.getString("ISDUNIT"));
                p.setISDPRICE(result.getString("ISDPRICE"));
                p.setISDUNR01(result.getString("ISDUNR01"));
                p.setISDUNR03(result.getString("ISDUNR03"));
                p.setISDMTCTRL(result.getString("ISDMTCTRL"));
                p.setISDDEST(result.getString("ISDDEST"));
                p.setISDRQDT(result.getString("ISDRQDT"));
                p.setISDRQTM(result.getString("ISDRQTM"));
                p.setISDAPDT(result.getString("ISDAPDT"));
                p.setISDAPTM(result.getString("ISDAPTM"));
                p.setISDAPUSER(result.getString("ISDAPUSER"));
                p.setISDRCDT(result.getString("ISDRCDT"));

                if (result.getString("ISDPKQTY") == null) {
                    p.setISDPKQTY("0.00");
                } else {
                    if (result.getString("ISDPKQTY").equals("null")) {
                        p.setISDPKQTY("0.00");
                    } else {
                        p.setISDPKQTY(result.getString("ISDPKQTY"));
                    }
                }

                p.setISDPKDT(result.getString("ISDPKDT"));
                p.setISDPKTM(result.getString("ISDPKTM"));
                p.setISDPKUSER(result.getString("ISDPKUSER"));
                p.setISDTPQTY(result.getString("ISDTPQTY"));
                p.setISDTPDT(result.getString("ISDTPDT"));
                p.setISDTPTM(result.getString("ISDTPTM"));
                p.setISDTPUSER(result.getString("ISDTPUSER"));
                p.setISDSTS(result.getString("ISDSTS"));

                if (result.getString("ISDJBNO") == null) {
                    p.setISDJBNO("");
                } else {
                    if (result.getString("ISDJBNO").equals("null")) {
                        p.setISDJBNO("");
                    } else {
                        p.setISDJBNO(result.getString("ISDJBNO"));
                    }
                }

                p.setISDQNO(result.getString("ISDQNO"));
                p.setISDREMARK(result.getString("ISDREMARK"));
                p.setISDISUNO(result.getString("ISDISUNO"));
                p.setISDTEMP(result.getString("ISDTEMP"));
                p.setISDEDT(result.getString("ISDEDT"));
                p.setISDETM(result.getString("trancDate"));
                p.setISDEUSR(result.getString("ISDEUSR"));
                p.setISDCDT(result.getString("ISDCDT"));
                p.setISDCTM(result.getString("ISDCTM"));
                p.setISDUSER(result.getString("ISDUSER"));
                p.setISDSTAD(result.getString("ISDSTAD"));
                p.setISDSTSTMP(result.getString("ISDSTSTMP"));
                p.setISDREMAIN(result.getString("ISDREMAIN"));
                p.setISDDESC(result.getString("ISDDESC"));
                p.setFAM(result.getString("ISSDESC"));
                p.setSTSI(result.getString("LSTSI"));

                UAList.add(p);

            }
            ps.close();
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public List<ISMMASH> getHead(String param, String toDeep) {

        List<ISMMASH> UAList = new ArrayList<ISMMASH>();
        String sql = "SELECT TOP 1 * FROM [RMShipment].[dbo].[ISMMASH] " + toDeep;

        try {

            PreparedStatement ps = connect.prepareStatement(sql);
            ps.setString(1, param);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                ISMMASH p = new ISMMASH();
                p.setISHCONO(result.getString("ISHCONO"));
                p.setISHORD(result.getString("ISHORD"));
                p.setISHSEQ(result.getString("ISHSEQ"));
                p.setISHTRDT(result.getString("ISHTRDT"));
                p.setISHUART(result.getString("ISHUART"));
                p.setISHTWEG(result.getString("ISHTWEG"));
                p.setISHMVT(result.getString("ISHMVT"));
                p.setISHCUNO(result.getString("ISHCUNO"));
                p.setISHCUNM1(result.getString("ISHCUNM1"));
                p.setISHCUNM2(result.getString("ISHCUNM2"));
                p.setISHCUAD1(result.getString("ISHCUAD1"));
                p.setISHCUAD2(result.getString("ISHCUAD2"));
                p.setISHBSTKD(result.getString("ISHBSTKD"));
                p.setISHSUBMI(result.getString("ISHSUBMI"));
                p.setISHMATLOT(result.getString("ISHMATLOT"));
                p.setISHTAXNO(result.getString("ISHTAXNO"));
                p.setISHBRANCH01(result.getString("ISHBRANCH01"));
                p.setISHDATUM(result.getString("ISHDATUM"));
                p.setISHSTYLE(result.getString("ISHSTYLE"));
                p.setISHCOLOR(result.getString("ISHCOLOR"));
                p.setISHLOT(result.getString("ISHLOT"));
                p.setISHAMTFG(result.getString("ISHAMTFG"));
                p.setISHSTYLE2(result.getString("ISHSTYLE2"));
                p.setISHNOR(result.getString("ISHNOR"));
                p.setISHPOFG(result.getString("ISHPOFG"));
                p.setISHGRPNO(result.getString("ISHGRPNO"));
                p.setISHWHNO(result.getString("ISHWHNO"));
                p.setISHPGRP(result.getString("ISHPGRP"));
                p.setISHLSTS(result.getString("ISHLSTS"));
                p.setISHHSTS(result.getString("ISHHSTS"));
                p.setISHDEST(result.getString("ISHDEST"));
                p.setISHRQDT(result.getString("ISHRQDT"));
                p.setISHRQTM(result.getString("ISHRQTM"));
                p.setISHAPDT(result.getString("ISHAPDT"));
                p.setISHAPTM(result.getString("ISHAPTM"));
                p.setISHAPUSR(result.getString("ISHAPUSR"));
                p.setISHRCDT(result.getString("ISHRCDT"));
                p.setISHRCTM(result.getString("ISHRCTM"));
                p.setISHRCUSR(result.getString("ISHRCUSR"));
                p.setISHPKDT(result.getString("ISHPKDT"));
                p.setISHPKTM(result.getString("ISHPKTM"));
                p.setISHPKUSR(result.getString("ISHPKUSR"));
                p.setISHTPDT(result.getString("ISHTPDT"));
                p.setISHTPTM(result.getString("ISHTPTM"));
                p.setISHTPUSR(result.getString("ISHTPUSR"));
                p.setISHP1DT(result.getString("ISHP1DT"));
                p.setISHA1DT(result.getString("ISHA1DT"));
                p.setISHT1DT(result.getString("ISHT1DT"));
                p.setISHJBNO(result.getString("ISHJBNO"));
                p.setISHREMARK(result.getString("ISHREMARK"));
                p.setISHEDT(result.getString("ISHEDT"));
                p.setISHETM(result.getString("ISHETM"));
                p.setISHEUSR(result.getString("ISHEUSR"));
                p.setISHCDT(result.getString("ISHCDT"));
                p.setISHCTM(result.getString("ISHCTM"));
                p.setISHUSER(result.getString("ISHUSER"));

                UAList.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public List<ISMMASH> getDisSalesH() {

        List<ISMMASH> UAList = new ArrayList<ISMMASH>();
        String sql = "SELECT DISTINCT ISHORD FROM [RMShipment].[dbo].[ISMMASH] ORDER BY ISHORD";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);
            ResultSet result = ps.executeQuery();

            while (result.next()) {

                ISMMASH p = new ISMMASH();
                p.setISHORD(result.getString("ISHORD"));

                UAList.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public List<ISMMASH> getDisCusNoH() {

        List<ISMMASH> UAList = new ArrayList<ISMMASH>();
        String sql = "SELECT DISTINCT ISHCUNO FROM [RMShipment].[dbo].[ISMMASH] ORDER BY ISHCUNO";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);
            ResultSet result = ps.executeQuery();

            while (result.next()) {

                ISMMASH p = new ISMMASH();
                p.setISHCUNO(result.getString("ISHCUNO"));

                UAList.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public List<ISMMASH> getDisDocNoH() {

        List<ISMMASH> UAList = new ArrayList<ISMMASH>();
        String sql = "SELECT DISTINCT ISHSUBMI FROM [RMShipment].[dbo].[ISMMASH] ORDER BY ISHSUBMI";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);
            ResultSet result = ps.executeQuery();

            while (result.next()) {

                ISMMASH p = new ISMMASH();
                p.setISHSUBMI(result.getString("ISHSUBMI"));

                UAList.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public List<ISMMASH> getDisStyFGH() {

        List<ISMMASH> UAList = new ArrayList<ISMMASH>();
        String sql = "SELECT DISTINCT ISHSTYLE,ISHCOLOR FROM [RMShipment].[dbo].[ISMMASH] ORDER BY ISHSTYLE,ISHCOLOR";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);
            ResultSet result = ps.executeQuery();

            while (result.next()) {

                ISMMASH p = new ISMMASH();
                p.setISHSTYLE(result.getString("ISHSTYLE"));
                p.setISHCOLOR(result.getString("ISHCOLOR"));

                UAList.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public List<ISMMASH> getDisLotH() {

        List<ISMMASH> UAList = new ArrayList<ISMMASH>();
        String sql = "SELECT DISTINCT ISHLOT FROM [RMShipment].[dbo].[ISMMASH] ORDER BY ISHLOT";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);
            ResultSet result = ps.executeQuery();

            while (result.next()) {

                ISMMASH p = new ISMMASH();
                p.setISHLOT(result.getString("ISHLOT"));

                UAList.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

}
