/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.dao;

import com.twc.wms.database.database;

import java.sql.*;

import java.util.ArrayList;

import java.util.List;

import com.twc.wms.entity.XMSOTDETAIL;
import com.twc.wms.entity.SAPMAS;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import com.twc.wms.database.MSSQLDbConnectionPool;
import com.twc.wms.entity.XMSOTDETAIL;

/**
 *
 * @author nutthawoot.noo
 */
public class XMSOTDETAILDao extends database {

    public ResultSet findForPrintXMS400QNO(String qno) throws SQLException {

        String sql = "SELECT [XMSOTDQNO]\n"
                + "      ,[XMSOTHDOCNO]\n"
                + "	  ,[XMSOTHDEST]\n"
                + "	  ,[QDEDESC]\n"
                + "	  ,FORMAT([XMSOTHSPDT],'d/MM/yyyy') AS [XMSOTHSPDT]\n"
                + "	  ,[XMSOTHWHS]\n"
                + "	  ,[QWHNAME]\n"
                + "	  ,[XMSOTHROUND]\n"
                + "	  ,[XMSOTHTYPE]\n"
                + "      ,[XMSOTHLICENNO]\n"
                + "      ,[XMSOTHDRIVER]\n"
                + "      ,[XMSOTHFOLLOWER]\n"
                + "      ,[XMSOTDLINE]\n"
                + "      ,[XMSOTDSENDER]\n"
                + "      ,[XMSOTDDEPT]\n"
                + "	  ,[QSNAME] AS DEPTNAME\n"
                + "      ,[XMSOTDDESC]\n"
                + "      ,[XMSOTDBAG]\n"
                + "      ,[XMSOTDROLL]\n"
                + "      ,[XMSOTDBOX]\n"
                + "      ,[XMSOTDPCS]\n"
                + "      ,[XMSOTDTOT]\n"
                + "      ,[XMSOTDTOTDOC]\n"
                + "      ,[XMSOTDPONO]\n"
                + "      ,[XMSOTDINVNO]\n"
                + "      ,[XMSOTDDELNO]\n"
                + "      ,[XMSOTDAMT]\n"
                + "      ,[XMSOTDREMARK]\n"
                + "      ,[XMSOTDDOCNO]\n"
                + "      ,[XMSOTDRECEIVER]\n"
                + "  FROM [RMShipment].[dbo].[XMSOTDETAIL]\n"
                + "  LEFT JOIN [XMSOTHEAD] ON [XMSOTHEAD].[XMSOTHQNO] = [XMSOTDETAIL].[XMSOTDQNO]\n"
                + "  LEFT JOIN [QSELLER] ON [QSELLER].[QSCODE] = [XMSOTDETAIL].[XMSOTDDEPT] AND [QSELLER].[QSWHSE] = [XMSOTHEAD].[XMSOTHWHS]\n"
                + "  LEFT JOIN [QDEST] ON [QDEST].[QDECOD] = [XMSOTHEAD].[XMSOTHDEST]\n"
                + "  LEFT JOIN [QWHMAS] ON [QWHMAS].[QWHCOD] = [XMSOTHEAD].[XMSOTHWHS]\n"
                + "  WHERE [XMSOTDQNO] = '" + qno + "'\n"
                + "  --ORDER BY [XMSOTDSENDER], [XMSOTDDOCNO] ";
//        System.out.println(sql);

        ResultSet result = null;

        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            result = ps.executeQuery();
        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public List<XMSOTDETAIL> findFromQI(String wh, String shipDate, String dest, String round) {

        List<XMSOTDETAIL> UAList = new ArrayList<XMSOTDETAIL>();

        String sql = "SELECT [QIDPDGP],[QIDPDTYPE],[QPTDESC],BAG,ROLL,BOX,ISNULL((BAG+ROLL+BOX),0) AS TOT,(CASE WHEN ([QIDOCNO] is null) then 0 else (case when (ROW_NUMBER() OVER(PARTITION BY [QIDOCNO] ORDER BY [QIDPDGP],[QIDPDTYPE])) = 1 then TOTDOC else 0 end) end) as TOTDOC,ISNULL([QIDOCNO],'') as [QIDOCNO]\n"
                + ", QIDQNO\n"
                + "FROM(\n"
                + "SELECT [QIDPDGP],[QIDPDTYPE],[QPTDESC],\n"
                + "SUM(BAG) AS BAG,SUM(ROLL) AS ROLL,SUM(BOX) AS BOX,TOTD AS TOTDOC, QIDOCNO, QIDQNO\n"
                + "FROM(\n"
                + "SELECT DTRO, QIDSHDT, QIDROUND, MATCTRL,[QIDPDGP],[QIDPDTYPE], QIDMAT, QIDDESC, \n"
                + "CASE WHEN CNT = 1 THEN (CASE WHEN QIDGPQR IS NULL THEN SUM(BAG) ELSE (CASE WHEN SUM(BAG) > 1 THEN 1 ELSE SUM(BAG) END) END) WHEN CNT != 1 AND QIDGPQR IS NULL THEN SUM(BAG) ELSE 0 END AS BAG, \n"
                + "CASE WHEN CNT = 1 THEN (CASE WHEN QIDGPQR IS NULL THEN SUM(ROLL) ELSE (CASE WHEN SUM(ROLL) > 1 THEN 1 ELSE SUM(ROLL) END) END) WHEN CNT != 1 AND QIDGPQR IS NULL THEN SUM(ROLL) ELSE 0 END AS ROLL, \n"
                + "CASE WHEN CNT = 1 THEN (CASE WHEN QIDGPQR IS NULL THEN SUM(BOX) ELSE (CASE WHEN SUM(BOX) > 1 THEN 1 ELSE SUM(BOX) END) END) WHEN CNT != 1 AND QIDGPQR IS NULL THEN SUM(BOX) ELSE 0 END AS BOX,\n"
                + "QIDQNO, QIDGPQR, MVT, TOTD, QIDOCNO, DOCLEN, QIDCDT, QIDID, CNT\n"
                + "FROM(\n"
                + "SELECT QIDSHDT +'-'+ QIDROUND AS DTRO, QIDSHDT, QIDROUND, MATCTRL,[QIDPDGP],[QIDPDTYPE], QIDMAT, QIDDESC, \n"
                + "BAG, ROLL, BOX, QIDQNO, QIDGPQR, MVT, TOTD, QIDOCNO, DOCLEN, QIDCDT, QIDID, CNT\n"
                + "FROM(\n"
                + "SELECT TOP 100 PERCENT FORMAT(QIDSHDT,'yyyy-MM-dd') AS QIDSHDT, QIDROUND,\n"
                + "       CASE WHEN QIDMTCTRL = '' OR QIDMTCTRL IS NULL THEN (select top 1 [SAPMCTRL] from [SAPMAS] where [SAPMAT] = QIDMAT) ELSE QIDMTCTRL END AS MATCTRL,\n"
                + "       [QIDPDGP],[QIDPDTYPE],QIDMAT, (select top 1 QRMDESC from QRMMAS where QRMCODE = QIDMAT) AS QIDDESC,\n"
                + "	   CASE WHEN QIDPACKTYPE = 'BAG' THEN 1 WHEN QIDPACKTYPE = 'PACK' THEN 1 ELSE 0 END AS BAG,\n"
                + "	   CASE WHEN QIDPACKTYPE = 'ROLL' THEN 1 ELSE 0 END AS ROLL,\n"
                + "	   CASE WHEN QIDPACKTYPE = 'BOX' THEN 1 ELSE 0 END AS BOX,\n"
                + "       QIDQNO, QIDGPQR, \n"
                + "	   (select top 1 QIHMVT from QIHEAD where QIHQNO = QIDQNO) AS MVT,\n"
                + "	   (select top 1 QIHTOTD from QIHEAD where QIHQNO = QIDQNO) AS TOTD,\n"
                + "	   QIDOCNO,LEN(QIDOCNO) AS DOCLEN, QIDCDT, CASE WHEN QIDGPQR IS NULL THEN QIDID ELSE QIDGPQR END as QIDID\n"
                + "       ,ROW_NUMBER() \n"
                + "	   OVER(PARTITION BY (CASE WHEN QIDGPQR IS NULL THEN QIDID ELSE QIDGPQR END) ORDER BY QIDSHDT, QIDROUND, QIDQNO, QIDCDT, (select top 1 QIHMTCTRL from QIHEAD where QIHQNO = QIDQNO), QIDGPQR, QIDMAT, (select top 1 QIHMVT from QIHEAD where QIHQNO = QIDQNO), QIDOCNO, LEN(QIDOCNO)) as CNT\n"
                + "FROM QIDETAIL\n"
                + "WHERE (select top 1 QIHDEST from QIHEAD where QIHQNO = QIDQNO) = '" + dest + "'\n"
                + "AND QIDWHS = '" + wh + "'\n"
                + "AND QIDSHDT = '" + shipDate + "'\n"
                + "AND QIDROUND = '" + round + "'\n"
                + "ORDER BY QIDSHDT, QIDROUND, QIDQNO, QIDCDT, MATCTRL, QIDGPQR, QIDMAT, MVT, QIDOCNO, DOCLEN\n"
                + ") TMP\n"
                + ")TMP2\n"
                + "WHERE DTRO is not null\n"
                + "GROUP BY DTRO, QIDSHDT, QIDROUND, MATCTRL, [QIDPDGP],[QIDPDTYPE],QIDMAT, QIDDESC, QIDQNO, QIDGPQR, MVT, TOTD, QIDOCNO, DOCLEN, QIDCDT, QIDID, CNT\n"
                + ")TMPXMS400\n"
                + "LEFT JOIN [QPDTYPE] ON [QPDTYPE].[QPTPDTYPE] = TMPXMS400.[QIDPDTYPE]\n"
                + "GROUP BY [QIDPDGP],[QIDPDTYPE],[QPTDESC],TOTD, QIDOCNO, QIDQNO\n"
                + ")TMPX2\n"
                + "ORDER BY QIDPDGP, QIDQNO";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            int ite = 0;

            while (result.next()) {

                XMSOTDETAIL p = new XMSOTDETAIL();

                p.setXMSOTDLINE(Integer.toString(++ite));
                p.setXMSOTDSENDER(result.getString("QIDPDGP") == null ? "" : result.getString("QIDPDGP"));
                p.setXMSOTDDESC(result.getString("QPTDESC") == null ? "" : result.getString("QPTDESC"));
                p.setXMSOTDBAG(result.getString("BAG"));
                p.setXMSOTDROLL(result.getString("ROLL"));
                p.setXMSOTDBOX(result.getString("BOX"));
                p.setXMSOTDTOT(result.getString("TOT"));
                p.setXMSOTDTOTDOC(result.getString("TOTDOC"));
                p.setXMSOTDDOCNO(result.getString("QIDOCNO") == null ? "" : result.getString("QIDOCNO"));

                UAList.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public List<XMSOTDETAIL> findByQno(String qno) {

        List<XMSOTDETAIL> UAList = new ArrayList<XMSOTDETAIL>();

        String sql = "SELECT [XMSOTDQNO]\n"
                + "      ,[XMSOTDLINE]\n"
                + "      ,[XMSOTDSENDER]\n"
                + "      ,[XMSOTDDEPT]\n"
                + "	  ,[QSNAME] AS DEPTNAME\n"
                + "      ,[XMSOTDDESC]\n"
                + "      ,FORMAT([XMSOTDBAG],'#,#0') AS [XMSOTDBAG]\n"
                + "      ,FORMAT([XMSOTDROLL],'#,#0') AS [XMSOTDROLL]\n"
                + "      ,FORMAT([XMSOTDBOX],'#,#0') AS [XMSOTDBOX]\n"
                + "      ,FORMAT([XMSOTDPCS],'#,#0') AS [XMSOTDPCS]\n"
                + "      ,FORMAT([XMSOTDTOT],'#,#0') AS [XMSOTDTOT]\n"
                + "      ,FORMAT([XMSOTDTOTDOC],'#,#0') AS [XMSOTDTOTDOC]\n"
                + "      ,[XMSOTDPONO]\n"
                + "      ,[XMSOTDINVNO]\n"
                + "      ,[XMSOTDDELNO]\n"
                + "      ,FORMAT([XMSOTDAMT],'#,#0.00') AS [XMSOTDAMT]\n"
                + "      ,[XMSOTDREMARK]\n"
                + "      ,[XMSOTDDOCNO]\n"
                + "      ,[XMSOTDRECEIVER]\n"
                + "  FROM [RMShipment].[dbo].[XMSOTDETAIL]\n"
                + "  LEFT JOIN [XMSOTHEAD] ON [XMSOTHEAD].[XMSOTHQNO] = [XMSOTDETAIL].[XMSOTDQNO]\n"
                + "  LEFT JOIN [QSELLER] ON [QSELLER].[QSCODE] = [XMSOTDETAIL].[XMSOTDDEPT] AND [QSELLER].[QSWHSE] = [XMSOTHEAD].[XMSOTHWHS]\n"
                + "  WHERE [XMSOTDQNO] = '" + qno + "'";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                XMSOTDETAIL p = new XMSOTDETAIL();

                p.setXMSOTDQNO(result.getString("XMSOTDQNO"));
                p.setXMSOTDLINE(result.getString("XMSOTDLINE"));
                p.setXMSOTDSENDER(result.getString("XMSOTDSENDER"));
                p.setXMSOTDDEPT(result.getString("XMSOTDDEPT"));
                p.setDeptn(result.getString("DEPTNAME"));

                p.setXMSOTDDESC(result.getString("XMSOTDDESC"));
                p.setXMSOTDBAG(result.getString("XMSOTDBAG"));
                p.setXMSOTDROLL(result.getString("XMSOTDROLL"));
                p.setXMSOTDBOX(result.getString("XMSOTDBOX"));
                p.setXMSOTDPCS(result.getString("XMSOTDPCS"));
                p.setXMSOTDTOT(result.getString("XMSOTDTOT"));
                p.setXMSOTDTOTDOC(result.getString("XMSOTDTOTDOC"));
                p.setXMSOTDPONO(result.getString("XMSOTDPONO"));
                p.setXMSOTDINVNO(result.getString("XMSOTDINVNO"));
                p.setXMSOTDDELNO(result.getString("XMSOTDDELNO"));
                p.setXMSOTDAMT(result.getString("XMSOTDAMT"));

                p.setXMSOTDREMARK(result.getString("XMSOTDREMARK"));
                p.setXMSOTDDOCNO(result.getString("XMSOTDDOCNO"));
                p.setXMSOTDRECEIVER(result.getString("XMSOTDRECEIVER"));

                UAList.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public boolean add(XMSOTDETAIL detail) {

        boolean result = false;

        String sql = "INSERT INTO XMSOTDETAIL "
                + "([XMSOTDCOM]\n"
                + "      ,[XMSOTDQNO]\n"
                + "      ,[XMSOTDLINE]\n"
                + "      ,[XMSOTDSENDER]\n"
                + "      ,[XMSOTDDEPT]\n"
                + "      ,[XMSOTDDESC]\n"
                + "      ,[XMSOTDBAG]\n"
                + "      ,[XMSOTDROLL]\n"
                + "      ,[XMSOTDBOX]\n"
                + "      ,[XMSOTDPCS]\n"
                + "      ,[XMSOTDTOT]\n"
                + "      ,[XMSOTDTOTDOC]\n"
                + "      ,[XMSOTDPONO]\n"
                + "      ,[XMSOTDINVNO]\n"
                + "      ,[XMSOTDDELNO]\n"
                + "      ,[XMSOTDAMT]\n"
                + "      ,[XMSOTDEDT]\n"
                + "      ,[XMSOTDCDT]\n"
                + "      ,[XMSOTDREMARK]\n"
                + "      ,[XMSOTDDOCNO]\n"
                + "      ,[XMSOTDRECEIVER]\n"
                + "      ,[XMSOTDUSER]) "
                + "VALUES('TWC'"
                + ",'" + detail.getXMSOTDQNO() + "'"
                + ",'" + detail.getXMSOTDLINE() + "'"
                + ",'" + detail.getXMSOTDSENDER() + "'"
                + ",'" + detail.getXMSOTDDEPT() + "'"
                + ",'" + detail.getXMSOTDDESC() + "'"
                + ",'" + detail.getXMSOTDBAG() + "'"
                + ",'" + detail.getXMSOTDROLL() + "'"
                + ",'" + detail.getXMSOTDBOX() + "'"
                + ",'" + detail.getXMSOTDPCS() + "'"
                + ",'" + detail.getXMSOTDTOT() + "'"
                + ",'" + detail.getXMSOTDTOTDOC() + "'"
                + ",'" + detail.getXMSOTDPONO() + "'"
                + ",'" + detail.getXMSOTDINVNO() + "'"
                + ",'" + detail.getXMSOTDDELNO() + "'"
                + ",'" + detail.getXMSOTDAMT() + "'"
                + ",CURRENT_TIMESTAMP"
                + ",CURRENT_TIMESTAMP"
                + ",'" + detail.getXMSOTDREMARK() + "'"
                + ",'" + detail.getXMSOTDDOCNO() + "'"
                + ",'" + detail.getXMSOTDRECEIVER() + "'"
                + ",'" + detail.getXMSOTDUSER() + "') ";
//        System.out.println(sql);
        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public void delete(String qno) {

        String sql = "DELETE FROM XMSOTDETAIL WHERE XMSOTDQNO = '" + qno + "'";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ps.executeUpdate();

        } catch (Exception e) {

            e.printStackTrace();

        }

    }
}
