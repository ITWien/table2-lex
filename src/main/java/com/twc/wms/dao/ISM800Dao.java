/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.dao;

import com.twc.wms.database.database;
import com.twc.wms.entity.ISMMASD;
import com.twc.wms.entity.ISMMASH;
import com.twc.wms.entity.ISMSDWD;
import com.twc.wms.entity.ISMSDWH;
import com.twc.wms.entity.ISMSDWS;
import com.twc.wms.entity.QWHSTR;
import java.io.UnsupportedEncodingException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author 93176
 */
public class ISM800Dao extends database {

    public List<ISMSDWH> findAll() {

        List<ISMSDWH> UAList = new ArrayList<ISMSDWH>();

        String sql = "SELECT\n"
                + "	  [ISHDWQNO] AS [NUM]\n"
                //                + "      ,[ISHTRDT]\n"
                + "      ,isnull(right([ISHTRDT],2)+'/'+left(right([ISHTRDT],4),2)+'/'+left([ISHTRDT],4),'') as [ISHTRDT]\n"
                //                + "      , '' AS [ISHTRDT]\n"
                + "      ,[ISHCUNO]\n"
                + "      ,[ISHCUNM1]\n"
                + "	  ,'' AS SODATE\n"
                + "	  ,'' AS SODATETO\n"
                + "	  ,'' AS SOFROM\n"
                + "	  ,'' AS SOTO\n"
                + "	  ,[ISHPGRP]\n"
                //                + "	  ,(CASE WHEN ISHODLS = 'C' THEN 'Complete' ELSE 'Not Complete' END) AS [STATUS]\n"
                //                + "	  ,[ISHSUSER] AS [STATUS]\n"
                + "      ,[ISHUSER]\n"
                + "      ,(SELECT TOP 1 [USERS] FROM [RMShipment].[dbo].[MSSUSER] WHERE USERID = [ISHUSER]) AS UNAME \n"
                + "      ,'' AS [ISHORD]\n"
                + "  FROM [RMShipment].[dbo].[ISMSDWH] GROUP BY [ISHDWQNO],[ISHCUNO],[ISHCUNM1],[ISHPGRP],[ISHUSER],[ISHTRDT]--,[ISHSUSER]";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                ISMSDWH p = new ISMSDWH();
//
                p.setNUM(result.getString("NUM"));
                p.setISHTRDT(result.getString("ISHTRDT"));
                p.setISHCUNO(result.getString("ISHCUNO").replace("0000", ""));
                p.setISHCUNM1(result.getString("ISHCUNM1"));
                p.setSODATE(result.getString("SODATE"));
                p.setSODATETO(result.getString("SODATETO"));
                p.setSOFROM(result.getString("SOFROM"));
                p.setSOTO(result.getString("SOTO"));
                p.setISHPGRP(result.getString("ISHPGRP"));
//                p.setSTATUS(result.getString("STATUS"));
                p.setISHUSER(result.getString("ISHUSER") + " : " + result.getString("UNAME").trim().split(" ")[0]);
                p.setISHORD(result.getString("ISHORD"));

//
                UAList.add(p);

            }
            ps.close();
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public List<QWHSTR> findProd() {

        List<QWHSTR> UAList = new ArrayList<QWHSTR>();

        String sql = "SELECT MIN(QWHMGRP) AS CODE,QWHSUBPROD\n"
                + "FROM QWHSTR \n"
                + "GROUP BY QWHSUBPROD\n"
                + "ORDER BY MIN(QWHMGRP) ASC,QWHSUBPROD";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                QWHSTR p = new QWHSTR();
//
                p.setCode(result.getString("CODE"));
                p.setQWHSUBPROD(result.getString("QWHSUBPROD"));

//
                UAList.add(p);

            }
            ps.close();
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public boolean check(String cusno, String trdtf, String trdtt, String ordf, String ordt, String prgp) {
        PreparedStatement ps = null;
        ResultSet rs = null;
        boolean STRRETURN = false;
        try {

            String ord = "";
            String grp = "";

            if (!ordf.equals("") && !ordt.equals("")) {
                ord = "AND ISHORD BETWEEN '" + ordf + "' AND '" + ordt + "'\n";
            }

            if (!prgp.equals("")) {
                grp = "AND ISHPGRP = '" + prgp + "'";
            }

            String check = "SELECT *\n"
                    + "FROM [RMShipment].[dbo].[ISMMASH]\n"
                    + "WHERE ISHCUNO = '0000" + cusno + "'\n"
                    //+ "AND ISHTRDT BETWEEN '" + trdtf.replaceAll("-", "") + "' AND '" + trdtt.replaceAll("-", "") + "'\n"
                    + ord + grp;

//            System.out.println("" + check);
            ps = connect.prepareStatement(check);
            rs = ps.executeQuery();
            if (rs.next()) {
                STRRETURN = true;
            }
            ps.close();
            connect.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return STRRETURN;
    }

//    public boolean checkDwn(String cusno, String trdtf, String trdtt, String ordf, String ordt, String prgp) {
//        PreparedStatement ps = null;
//        ResultSet rs = null;
//        boolean STRRETURN = false;
//        try {
//
//            String ord = "";
//            String grp = "";
//
//            if (!ordf.equals("") && !ordt.equals("")) {
//                ord = "AND ISHORD BETWEEN '" + ordf + "' AND '" + ordt + "'\n";
//            }
//
//            if (!prgp.equals("")) {
//                grp = "AND ISHPGRP = '" + prgp + "'";
//            }
//
//            String check = "SELECT *\n"
//                    + "FROM [RMShipment].[dbo].[ISMMASH]\n"
//                    + "WHERE ISHCUNO = '0000" + cusno + "'\n"
//                    //+ "AND ISHTRDT BETWEEN '" + trdtf.replaceAll("-", "") + "' AND '" + trdtt.replaceAll("-", "") + "'\n"
//                    + ord + grp;
//
////            System.out.println("" + check);
//            ps = connect.prepareStatement(check);
//            rs = ps.executeQuery();
//            if (rs.next()) {
//                STRRETURN = true;
//            }
//            ps.close();
//            connect.close();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return STRRETURN;
//    }
//    public static String toAscii(String s) {
//        StringBuffer output = new StringBuffer();
//        int size = s.length();
//        for (int i = 0; i < size; i++) {
//            char c = s.charAt(i);
//            if (c >= 161 + 3424 && c <= 251 + 3424) {
//                c = (char) (c - 3424);
//            }
//            output.append(c);
//        }
//        //System.out.println(output);
//        return output.toString();
//    }
//
    public String checkNull(String a) {

        if (a == null) {
            a = "NULL";
        } else {
            if (a.equals("")) {
                a = "NULL";
            } else {
                a = "'" + a + "'";
            }
        }

        return a;
    }

    public boolean add(String no, ISMSDWH head, String user, String seq_current, int lineDetail, String delidatef, String delidatet) {

        user = checkNull(user);

        String order = checkNull(head.getISHORD());
        String seq = checkNull(seq_current);
//        String seq = checkNull(head.getISHSEQ());
        String trdt = head.getISHTRDT().replace("-", "");
        String uart = checkNull(head.getISHUART());
        String sorg = checkNull(head.getISHSORG());
        String weg = checkNull(head.getISHTWEG());
        String divi = checkNull(head.getISHDIVI());
        String sgrp = checkNull(head.getISHSGRP());
        String soff = checkNull(head.getISHSOFF());
        String movement = checkNull(head.getISHMVT());
        String cusno = checkNull(head.getISHCUNO());
        String cusname1 = checkNull(head.getISHCUNM1());
        String cusname2 = checkNull(head.getISHCUNM2());
        String cusadd1 = checkNull(head.getISHCUAD1());
        String cusadd2 = checkNull(head.getISHCUAD2());
        String po = checkNull(head.getISHBSTKD());
        String collec = checkNull(head.getISHSUBMI());
        String fg = checkNull(head.getISHMATLOT());
        String ddate = checkNull(head.getISHDDATE());
        String dtime = checkNull(head.getISHDTIME());
        String taxno = checkNull(head.getISHTAXNO());
        String branch = checkNull(head.getISHBRANCH01());
        String style = checkNull(head.getISHSTYLE());
        String color = checkNull(head.getISHCOLOR());
        String lot = checkNull(head.getISHLOT());
        String fgquean = checkNull(head.getISHAMTFG());
        String style2 = checkNull(head.getISHSTYLE2());
        String nor = String.valueOf(lineDetail);
        String pofg = checkNull(head.getISHPOFG());
        String whno = checkNull(head.getISHWHNO());
        String prgp = checkNull(head.getISHPGRP());
        String Lsts = checkNull(head.getISHLSTS());
        String Hsts = checkNull(head.getISHHSTS());
        String dest = checkNull(head.getISHDEST());
        String remk = checkNull(head.getISHREMK());
        String cuno2 = checkNull(head.getISHCUNO2());
        String dldt = checkNull(head.getISHDLDT());
        String dls = checkNull(head.getISHDLS());
        String odls = checkNull(head.getISHODLS());
        String crdt = checkNull(head.getISHCRDT());
        String crtm = checkNull(head.getISHCRTM());
        String suser = checkNull(head.getISHSUSER());
        String reason = checkNull(head.getISHREASON());
//        String edt = checkNull(head.getISHEDT());
//        String etm = checkNull(head.getISHETM());
//        String euser = checkNull(head.getISHEUSR());
//        String cdt = checkNull(head.getISHCDT());
//        String ctm = checkNull(head.getISHCTM());
//        String huser = checkNull(head.getISHUSER());

        if (trdt == null) {
            trdt = "NULL";
        } else {
            if (trdt.equals("")) {
                trdt = "NULL";
            }
        }
        if (nor == null) {
            nor = "NULL";
        } else {
            if (nor.equals("")) {
                nor = "NULL";
            }
        }

        boolean result = false;

        String sql = " INSERT INTO [RMShipment].[dbo].[ISMSDWH] (ISHCONO,ISHDWQNO,ISHORD,ISHSEQ,ISHTRDT,ISHUART,ISHSORG,ISHTWEG,ISHDIVI,ISHSGRP,ISHSOFF,ISHMVT,ISHCUNO,ISHCUNM1,ISHCUNM2,ISHCUAD1,ISHCUAD2\n"
                + ",ISHBSTKD,ISHSUBMI,ISHMATLOT,ISHDDATE,ISHDTIME,ISHTAXNO,ISHBRANCH01,ISHSTYLE,ISHCOLOR,ISHLOT,ISHAMTFG,ISHSTYLE2,ISHNOR,ISHPOFG,ISHWHNO,ISHPGRP,ISHLSTS,ISHHSTS\n"
                + ",ISHDEST,ISHREMK,ISHCUNO2,ISHDLDT,ISHDLS,ISHODLS,ISHCRDT,ISHCRTM,ISHSUSER,ISHEDT,ISHETM,ISHEUSR,ISHCDT,ISHCTM,ISHUSER,ISHREASON)\n"
                + "VALUES ('TWC'," + no + "," + order + "," + seq + ",FORMAT(CURRENT_TIMESTAMP,'yyyyMMdd')," + uart + "," + sorg + "," + weg + "," + divi + "," + sgrp + "," + soff + "," + movement + "," + cusno + "," + cusname1 + "," + cusname2 + "," + cusadd1 + "," + cusadd2 + "," + po + "," + collec + "," + fg + "," + ddate + "," + dtime + "," + taxno + "," + branch + "," + style + "," + color + "," + lot + "," + fgquean + "," + style2 + "," + nor + "," + pofg + ",(SELECT TOP 1 MSWHSE FROM [RMShipment].[dbo].[MSSUSER] WHERE USERID = " + user + " )," + prgp + "\n"
                + "," + Lsts + "," + Hsts + ",(SELECT [QDECOD] FROM [RMShipment].[dbo].[QDEST] WHERE QDEWHS = (SELECT TOP 1 MSWHSE FROM [RMShipment].[dbo].[MSSUSER] WHERE USERID = " + user + " ) AND QDEDESC = " + pofg + " )," + remk + "," + cuno2 + "," + dldt + "," + dls + "," + odls + "," + crdt + "," + crtm + "," + suser + ",NULL,NULL,NULL,FORMAT(CURRENT_TIMESTAMP,'yyyyMMdd'),FORMAT(CURRENT_TIMESTAMP,'HHmmss')," + user + "," + reason + ")";
//                + "," + Lsts + "," + Hsts + "," + dest + "," + remk + "," + cuno2 + "," + dldt + "," + dls + "," + odls + "," + crdt + "," + crtm + "," + suser + ",FORMAT(CURRENT_TIMESTAMP,'yyyyMMdd'),FORMAT(CURRENT_TIMESTAMP,'HHmmss'),NULL,FORMAT(CURRENT_TIMESTAMP,'yyyyMMdd'),FORMAT(CURRENT_TIMESTAMP,'HHmmss')," + user + ")";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);
            int record = ps.executeUpdate();
//
            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            ps.close();
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }
//
        return result;

    }

//    public boolean addSEQPlus(String no, ISMSDWH head, String user, String seqin) {
//
//        user = checkNull(user);
//
//        String order = checkNull(head.getISHORD());
//        String seq = seqin;
//        String trdt = head.getISHTRDT().replace("-", "");
//        String uart = checkNull(head.getISHUART());
//        String sorg = checkNull(head.getISHSORG());
//        String weg = checkNull(head.getISHTWEG());
//        String divi = checkNull(head.getISHDIVI());
//        String sgrp = checkNull(head.getISHSGRP());
//        String soff = checkNull(head.getISHSOFF());
//        String movement = checkNull(head.getISHMVT());
//        String cusno = checkNull(head.getISHCUNO());
//        String cusname1 = checkNull(head.getISHCUNM1());
//        String cusname2 = checkNull(head.getISHCUNM2());
//        String cusadd1 = checkNull(head.getISHCUAD1());
//        String cusadd2 = checkNull(head.getISHCUAD2());
//        String po = checkNull(head.getISHBSTKD());
//        String collec = checkNull(head.getISHSUBMI());
//        String fg = checkNull(head.getISHMATLOT());
//        String ddate = checkNull(head.getISHDDATE());
//        String dtime = checkNull(head.getISHDTIME());
//        String taxno = checkNull(head.getISHTAXNO());
//        String branch = checkNull(head.getISHBRANCH01());
//        String style = checkNull(head.getISHSTYLE());
//        String color = checkNull(head.getISHCOLOR());
//        String lot = checkNull(head.getISHLOT());
//        String fgquean = checkNull(head.getISHAMTFG());
//        String style2 = checkNull(head.getISHSTYLE2());
//        String nor = head.getISHNOR();
//        String pofg = checkNull(head.getISHPOFG());
//        String whno = checkNull(head.getISHWHNO());
//        String prgp = checkNull(head.getISHPGRP());
//        String Lsts = checkNull(head.getISHLSTS());
//        String Hsts = checkNull(head.getISHHSTS());
//        String dest = checkNull(head.getISHDEST());
//        String remk = checkNull(head.getISHREMK());
//        String cuno2 = checkNull(head.getISHCUNO2());
//        String dldt = checkNull(head.getISHDLDT());
//        String dls = checkNull(head.getISHDLS());
//        String odls = checkNull(head.getISHODLS());
//        String crdt = checkNull(head.getISHCRDT());
//        String crtm = checkNull(head.getISHCRTM());
//        String suser = checkNull(head.getISHSUSER());
////        String edt = checkNull(head.getISHEDT());
////        String etm = checkNull(head.getISHETM());
////        String euser = checkNull(head.getISHEUSR());
////        String cdt = checkNull(head.getISHCDT());
////        String ctm = checkNull(head.getISHCTM());
////        String huser = checkNull(head.getISHUSER());
//
//        if (trdt == null) {
//            trdt = "NULL";
//        } else {
//            if (trdt.equals("")) {
//                trdt = "NULL";
//            }
//        }
//        if (nor == null) {
//            nor = "NULL";
//        } else {
//            if (nor.equals("")) {
//                nor = "NULL";
//            }
//        }
//
//        boolean result = false;
//
//        String sql = " INSERT INTO [RMShipment].[dbo].[ISMSDWH] (ISHCONO,ISHDWQNO,ISHORD,ISHSEQ,ISHTRDT,ISHUART,ISHSORG,ISHTWEG,ISHDIVI,ISHSGRP,ISHSOFF,ISHMVT,ISHCUNO,ISHCUNM1,ISHCUNM2,ISHCUAD1,ISHCUAD2\n"
//                + ",ISHBSTKD,ISHSUBMI,ISHMATLOT,ISHDDATE,ISHDTIME,ISHTAXNO,ISHBRANCH01,ISHSTYLE,ISHCOLOR,ISHLOT,ISHAMTFG,ISHSTYLE2,ISHNOR,ISHPOFG,ISHWHNO,ISHPGRP,ISHLSTS,ISHHSTS\n"
//                + ",ISHDEST,ISHREMK,ISHCUNO2,ISHDLDT,ISHDLS,ISHODLS,ISHCRDT,ISHCRTM,ISHSUSER,ISHEDT,ISHETM,ISHEUSR,ISHCDT,ISHCTM,ISHUSER)\n"
//                + "VALUES ('TWC'," + no + "," + order + "," + seq + "," + trdt + "," + uart + "," + sorg + "," + weg + "," + divi + "," + sgrp + "," + soff + "," + movement + "," + cusno + "," + cusname1 + "," + cusname2 + "," + cusadd1 + "," + cusadd2 + "," + po + "," + collec + "," + fg + "," + ddate + "," + dtime + "," + taxno + "," + branch + "," + style + "," + color + "," + lot + "," + fgquean + "," + style2 + "," + nor + "," + pofg + "," + whno + "," + prgp + "\n"
//                + "," + Lsts + "," + Hsts + "," + dest + "," + remk + "," + cuno2 + "," + dldt + "," + dls + "," + odls + "," + crdt + "," + crtm + "," + suser + ",NULL,NULL,NULL,FORMAT(CURRENT_TIMESTAMP,'yyyyMMdd'),FORMAT(CURRENT_TIMESTAMP,'HHmmss')," + user + ")";
////                + "," + Lsts + "," + Hsts + "," + dest + "," + remk + "," + cuno2 + "," + dldt + "," + dls + "," + odls + "," + crdt + "," + crtm + "," + suser + ",FORMAT(CURRENT_TIMESTAMP,'yyyyMMdd'),FORMAT(CURRENT_TIMESTAMP,'HHmmss'),NULL,FORMAT(CURRENT_TIMESTAMP,'yyyyMMdd'),FORMAT(CURRENT_TIMESTAMP,'HHmmss')," + user + ")";
//
//        try {
//            PreparedStatement ps = connect.prepareStatement(sql);
//            int record = ps.executeUpdate();
////
//            if (record >= 1) {
//
//                result = true;
//
//            } else {
//
//                result = false;
//
//            }
//
//            ps.close();
//            connect.close();
//
//        } catch (Exception e) {
//
//            e.printStackTrace();
//
//        }
////
//        return result;
//
//    }
    public boolean addDET(String no, List<ISMSDWD> invDetailList, String user) {

        boolean result = false;

        String sql = " INSERT INTO [RMShipment].[dbo].[ISMSDWD] ([ISDCONO],[ISDDWQNO],[ISDORD],[ISDSEQ],[ISDLINO],[ISDITNO],[ISDBAT],[ISDMGRP],[ISDSTEXT]\n"
                + ",[ISDITCAT],[ISDRSRJ],[ISDRSRJT],[ISDPLANT],[ISDVT],[ISDSTRG],[ISDRQQTY],[ISDDLQTY],[ISDUNIT],[ISDBUNIT]\n"
                + ",[ISDCON1],[ISDCON2],[ISDPRICE],[ISDNETV],[ISDCUR],[ISDEXC],[ISDUNR01],[ISDUNR03],[ISDMTCTRL],[ISDPFCT]\n"
                + ",[ISDACGP],[ISDODT],[ISDUDT],[ISDRQDT],[ISDDLS],[ISDODLS],[ISDCRDT],[ISDCRTM],[ISDSUSER],[ISDDEST]\n"
                + ",[ISDREMK],[ISDEDT],[ISDETM],[ISDEUSR],[ISDCDT],[ISDCTM],[ISDUSER]) VALUES \n";

        for (int i = 0; i < invDetailList.size(); i++) {

            String ord = checkNull(invDetailList.get(i).getISDORD());
            String seq = checkNull(invDetailList.get(i).getISDSEQ());
            String lino = checkNull(invDetailList.get(i).getISDLINO());
            String ditno = checkNull(invDetailList.get(i).getISDITNO());
            String bat = checkNull(invDetailList.get(i).getISDBAT());
            String mgrp = checkNull(invDetailList.get(i).getISDMGRP());
            String text = checkNull(invDetailList.get(i).getISDSTEXT());
            String itcat = checkNull(invDetailList.get(i).getISDITCAT());
            String rsrj = checkNull(invDetailList.get(i).getISDRSRJ());
            String rsrjt = checkNull(invDetailList.get(i).getISDRSRJT());
            String plant = checkNull(invDetailList.get(i).getISDPLANT());
            String vt = checkNull(invDetailList.get(i).getISDVT());
            String strg = checkNull(invDetailList.get(i).getISDSTRG());
            String rqqty = invDetailList.get(i).getISDRQQTY();
            String dlqty = invDetailList.get(i).getISDDLQTY();
            String dunit = checkNull(invDetailList.get(i).getISDUNIT());
            String bunit = checkNull(invDetailList.get(i).getISDBUNIT());
            String con1 = checkNull(invDetailList.get(i).getISDCON1());
            String con2 = checkNull(invDetailList.get(i).getISDCON2());
            String price = invDetailList.get(i).getISDPRICE();
            String netv = invDetailList.get(i).getISDNETV();
            String cur = checkNull(invDetailList.get(i).getISDCUR());
            String exc = invDetailList.get(i).getISDEXC();
            String unr01 = invDetailList.get(i).getISDUNR01();
            String unr03 = invDetailList.get(i).getISDUNR03();
            String mtctrl = checkNull(invDetailList.get(i).getISDMTCTRL());
            String pfct = checkNull(invDetailList.get(i).getISDPFCT());
            String acgp = checkNull(invDetailList.get(i).getISDACGP());
            String odt = checkNull(invDetailList.get(i).getISDODT());
            String udt = checkNull(invDetailList.get(i).getISDUDT());
            String rqdt = checkNull(invDetailList.get(i).getISDRQDT());
            String dls = checkNull(invDetailList.get(i).getISDDLS());
            String odls = checkNull(invDetailList.get(i).getISDODLS());
            String crdt = checkNull(invDetailList.get(i).getISDCRDT());
            String crtm = checkNull(invDetailList.get(i).getISDCRTM());
            String suser = checkNull(invDetailList.get(i).getISDSUSER());
            String dest = checkNull(invDetailList.get(i).getISDDEST());
            String remk = checkNull(invDetailList.get(i).getISDREMK());
//            String userdet = checkNull(invDetailList.get(i).getISDEUSR());

            if (rqqty == null) {
                rqqty = "NULL";
            } else {
                if (rqqty.equals("")) {
                    rqqty = "NULL";
                }
            }

            if (dlqty == null) {
                dlqty = "NULL";
            } else {
                if (dlqty.equals("")) {
                    dlqty = "NULL";
                }
            }

            if (price == null) {
                price = "NULL";
            } else {
                if (price.equals("")) {
                    price = "NULL";
                }
            }

            if (netv == null) {
                netv = "NULL";
            } else {
                if (netv.equals("")) {
                    netv = "NULL";
                }
            }

            if (exc == null) {
                exc = "NULL";
            } else {
                if (exc.equals("")) {
                    exc = "NULL";
                }
            }

            if (unr01 == null) {
                unr01 = "NULL";
            } else {
                if (unr01.equals("")) {
                    unr01 = "NULL";
                }
            }

            if (unr03 == null) {
                unr03 = "NULL";
            } else {
                if (unr03.equals("")) {
                    unr03 = "NULL";
                }
            }

            sql += "	 ('TWC'," + no + "," + ord + "," + seq + "," + lino + "," + ditno + "," + bat + "," + mgrp + "," + text + "," + itcat
                    + "," + rsrj + "," + rsrjt + "," + plant + "," + vt + "," + strg + "," + rqqty + "," + dlqty + "," + dunit + "," + bunit + "," + con1
                    + "," + con2 + "," + price + "," + netv + "," + cur + "," + exc + "," + unr01 + "," + unr03 + "," + mtctrl + "," + pfct + "," + acgp
                    + "," + odt + "," + udt + "," + rqdt + "," + dls + "," + odls + "," + crdt + "," + crtm + "," + suser + "," + dest + "," + remk
                    + ",FORMAT(CURRENT_TIMESTAMP,'yyyyMMdd'),FORMAT(CURRENT_TIMESTAMP,'HHmmss'),NULL,FORMAT(CURRENT_TIMESTAMP,'yyyyMMdd'),FORMAT(CURRENT_TIMESTAMP,'HHmmss')," + user + ")";

            if (i != (invDetailList.size() - 1)) {
                sql += ",";
            }

        }

        try {

//            System.out.println("" + sql);
            PreparedStatement ps = connect.prepareStatement(sql);
            int record = ps.executeUpdate();
            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            ps.close();
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }
//
        return result;

    }

    public boolean insertDetailLineNotFound(String no, String seqin, ISMSDWD invDetailList, String user, String purglist, String purgf, String purgt) {

        boolean result = false;

        String ord = checkNull(invDetailList.getISDORD());
        String seq = seqin;
        String lino = checkNull(invDetailList.getISDLINO());
        String ditno = checkNull(invDetailList.getISDITNO());
        String bat = checkNull(invDetailList.getISDBAT());
        String mgrp = checkNull(invDetailList.getISDMGRP());
        String text = checkNull(invDetailList.getISDSTEXT());
        String itcat = checkNull(invDetailList.getISDITCAT());
        String rsrj = checkNull(invDetailList.getISDRSRJ());
        String rsrjt = checkNull(invDetailList.getISDRSRJT());
        String plant = checkNull(invDetailList.getISDPLANT());
        String vt = checkNull(invDetailList.getISDVT());
        String strg = checkNull(invDetailList.getISDSTRG());
        String rqqty = invDetailList.getISDRQQTY();
        String dlqty = invDetailList.getISDDLQTY();
        String dunit = checkNull(invDetailList.getISDUNIT());
        String bunit = checkNull(invDetailList.getISDBUNIT());
        String con1 = checkNull(invDetailList.getISDCON1());
        String con2 = checkNull(invDetailList.getISDCON2());
        String price = invDetailList.getISDPRICE();
        String netv = invDetailList.getISDNETV();
        String cur = checkNull(invDetailList.getISDCUR());
        String exc = invDetailList.getISDEXC();
        String unr01 = invDetailList.getISDUNR01();
        String unr03 = invDetailList.getISDUNR03();
        String mtctrl = checkNull(invDetailList.getISDMTCTRL());
        String pfct = checkNull(invDetailList.getISDPFCT());
        String acgp = checkNull(invDetailList.getISDACGP());
        String odt = checkNull(invDetailList.getISDODT());
        String udt = checkNull(invDetailList.getISDUDT());
        String rqdt = checkNull(invDetailList.getISDRQDT());
        String dls = checkNull(invDetailList.getISDDLS());
        String odls = checkNull(invDetailList.getISDODLS());
        String crdt = checkNull(invDetailList.getISDCRDT());
        String crtm = checkNull(invDetailList.getISDCRTM());
        String suser = checkNull(invDetailList.getISDSUSER());
        String dest = checkNull(invDetailList.getISDDEST());
        String remk = checkNull(invDetailList.getISDREMK());
        String purg = checkNull(invDetailList.getISDPURG());
//            String userdet = checkNull(invDetailList.getISDEUSR());

        if (rqqty == null) {
            rqqty = "NULL";
        } else {
            if (rqqty.equals("")) {
                rqqty = "NULL";
            }
        }

        if (dlqty == null) {
            dlqty = "NULL";
        } else {
            if (dlqty.equals("")) {
                dlqty = "NULL";
            }
        }

        if (price == null) {
            price = "NULL";
        } else {
            if (price.equals("")) {
                price = "NULL";
            }
        }

        if (netv == null) {
            netv = "NULL";
        } else {
            if (netv.equals("")) {
                netv = "NULL";
            }
        }

        if (exc == null) {
            exc = "NULL";
        } else {
            if (exc.equals("")) {
                exc = "NULL";
            }
        }

        if (unr01 == null) {
            unr01 = "NULL";
        } else {
            if (unr01.equals("")) {
                unr01 = "NULL";
            }
        }

        if (unr03 == null) {
            unr03 = "NULL";
        } else {
            if (unr03.equals("")) {
                unr03 = "NULL";
            }
        }

        user = checkNull(user);

        //condition for ISDRQQTY ,ISDDLQTY
        //if sts (ISDODLS) eq 'C' then [ISDRQQTY = 0]
        //else [ISDRQQTY = ISDRQQTY - ISDDLQTY] if value has - (nagative value) then [ISDRQQTY = 0]
        //Uncomment in prd ----------------
        if (!dlqty.equals("NULL") && !rqqty.equals("NULL")) {
//            if (odls.equals("B") || odls.equals("b") || odls.equals("'B'")) {
            rqqty = Double.toString(Double.parseDouble(rqqty) - Double.parseDouble(dlqty));
//            }
        }
        //Uncomment in prd ---------------- END
        String sql = "";

//        System.out.println("odls : " + odls);
        if (!odls.equals("C") && !odls.equals("c") && !odls.equals("'C'")) { //DLV_STAT_I != C
            if (rsrj.equals("") || rsrj.equals("null") || rsrj.equals("NULL")) { //if has value mean get reject then not insert

                sql = " INSERT INTO [RMShipment].[dbo].[ISMSDWD] ([ISDCONO],[ISDDWQNO],[ISDORD],[ISDSEQ],[ISDLINO],[ISDITNO],[ISDBAT],[ISDMGRP],[ISDSTEXT]\n"
                        + ",[ISDITCAT],[ISDRSRJ],[ISDRSRJT],[ISDPLANT],[ISDVT],[ISDSTRG],[ISDRQQTY],[ISDDLQTY],[ISDUNIT],[ISDBUNIT]\n"
                        + ",[ISDCON1],[ISDCON2],[ISDPRICE],[ISDNETV],[ISDCUR],[ISDEXC],[ISDUNR01],[ISDUNR03],[ISDMTCTRL],[ISDPFCT]\n"
                        + ",[ISDACGP],[ISDODT],[ISDUDT],[ISDRQDT],[ISDDLS],[ISDODLS],[ISDCRDT],[ISDCRTM],[ISDSUSER],[ISDDEST]\n"
                        + ",[ISDREMK],[ISDEDT],[ISDETM],[ISDEUSR],[ISDCDT],[ISDCTM],[ISDUSER],[ISDPURG]) VALUES \n";

                sql += " ('TWC'," + no + "," + ord + "," + seq + "," + lino + "," + ditno + "," + bat + "," + mgrp + "," + text + "," + itcat
                        + "," + rsrj + "," + rsrjt + "," + plant + "," + vt + "," + strg + "," + rqqty + "," + dlqty + "," + dunit + "," + bunit + "," + con1
                        + "," + con2 + "," + price + "," + netv + "," + cur + "," + exc + "," + unr01 + "," + unr03 + "," + mtctrl + "," + pfct + "," + acgp
                        + "," + odt + "," + udt + "," + rqdt + "," + dls + "," + odls + "," + crdt + "," + crtm + "," + suser + ",(SELECT [QDECOD] FROM [RMShipment].[dbo].[QDEST] WHERE QDEWHS = (SELECT TOP 1 MSWHSE FROM [RMShipment].[dbo].[MSSUSER] WHERE USERID = " + user + " ) AND QDEDESC = " + dest + " )," + remk
                        + ",FORMAT(CURRENT_TIMESTAMP,'yyyyMMdd'),FORMAT(CURRENT_TIMESTAMP,'HHmmss'),NULL,FORMAT(CURRENT_TIMESTAMP,'yyyyMMdd'),FORMAT(CURRENT_TIMESTAMP,'HHmmss')," + user + "," + purg + ")";

//            if (i != (invDetailList.size() - 1)) {
//                sql += ",";
//            }
            }
        }

        try {

//            System.out.println("" + sql);
            if (!sql.equals("")) {
                PreparedStatement ps = connect.prepareStatement(sql);
                int record = ps.executeUpdate();
                if (record >= 1) {
                    result = true;
                } else {
                    result = false;
                }

                ps.close();
                connect.close();
            }
        } catch (Exception e) {

            e.printStackTrace();

        }
        return result;

    }

//    public boolean addDETbySIZE(String no, List<ISMSDWD> invDetailList, List<ISMMASD> LineMas, String user, String seqin) {
//
//        boolean result = false;
//
//        String sql = " INSERT INTO [RMShipment].[dbo].[ISMSDWD] ([ISDCONO],[ISDDWQNO],[ISDORD],[ISDSEQ],[ISDLINO],[ISDITNO],[ISDPLANT]\n"
//                + "      ,[ISDVT],[ISDSTRG],[ISDRQQTY],[ISDUNIT],[ISDPRICE],[ISDUNR01],[ISDUNR03],[ISDMTCTRL],[ISDDEST],[ISDREMK]\n"
//                + "      ,[ISDEDT],[ISDETM],[ISDEUSR],[ISDCDT],[ISDCTM],[ISDUSER]) VALUES \n";
//
//        for (int i = 0; i < invDetailList.size(); i++) {
////        for (int i = LineMas.size(); i < invDetailList.size(); i++) {
//
//            System.out.println(LineMas.get(i).getISDORD().equals(invDetailList.get(i).getISDORD()));
//
//            if (LineMas.get(i).getISDORD().equals(invDetailList.get(i).getISDORD())) {
//                if (!LineMas.get(i).getISDLINO().equals(invDetailList.get(i).getISDLINO())) { // New Row In Same
//
//                }
//            }
//
//            String ord = checkNull(invDetailList.get(i).getISDORD());
//            String seq = seqin;
//            String lino = checkNull(invDetailList.get(i).getISDLINO());
//            String ditno = checkNull(invDetailList.get(i).getISDITNO());
//            String plant = checkNull(invDetailList.get(i).getISDPLANT());
//            String vt = checkNull(invDetailList.get(i).getISDVT());
//            String strg = checkNull(invDetailList.get(i).getISDSTRG());
//            String rqqty = invDetailList.get(i).getISDRQQTY();
//            String dunit = checkNull(invDetailList.get(i).getISDUNIT());
//            String price = invDetailList.get(i).getISDPRICE();
//            String unr01 = invDetailList.get(i).getISDUNR01();
//            String unr03 = invDetailList.get(i).getISDUNR03();
//            String mtctrl = checkNull(invDetailList.get(i).getISDMTCTRL());
//            String dest = checkNull(invDetailList.get(i).getISDDEST());
//            String remk = checkNull(invDetailList.get(i).getISDREMK());
//            String userdet = checkNull(invDetailList.get(i).getISDEUSR());
//
//            if (rqqty == null) {
//                rqqty = "NULL";
//            } else {
//                if (rqqty.equals("")) {
//                    rqqty = "NULL";
//                }
//            }
//
//            if (price == null) {
//                price = "NULL";
//            } else {
//                if (price.equals("")) {
//                    price = "NULL";
//                }
//            }
//
//            if (unr01 == null) {
//                unr01 = "NULL";
//            } else {
//                if (unr01.equals("")) {
//                    unr01 = "NULL";
//                }
//            }
//
//            if (unr03 == null) {
//                unr03 = "NULL";
//            } else {
//                if (unr03.equals("")) {
//                    unr03 = "NULL";
//                }
//            }
//
//            sql += "('TWC',(SELECT TOP 1 ISHDWQNO FROM ISMSDWH WHERE ISHORD = '" + ord + "')," + ord + "," + seq + "," + lino + "," + ditno + "," + plant + "," + vt + ","
//                    + strg + "," + rqqty + "," + dunit + "," + price + "," + unr01 + "," + unr03 + "," + mtctrl + "," + dest
//                    + "," + remk + ",FORMAT(CURRENT_TIMESTAMP,'yyyyMMdd'),FORMAT(CURRENT_TIMESTAMP,'HHmmss'),"
//                    + userdet + ",FORMAT(CURRENT_TIMESTAMP,'yyyyMMdd'),FORMAT(CURRENT_TIMESTAMP,'HHmmss')," + user + ")";
//
//            if (i != (invDetailList.size() - 1)) {
//                sql += ",";
//            }
//        }
//
//        try {
//
//            PreparedStatement ps = connect.prepareStatement(sql);
//            int record = ps.executeUpdate();
//            if (record >= 1) {
//
//                result = true;
//
//            } else {
//
//                result = false;
//
//            }
//
//            ps.close();
//            connect.close();
//
//        } catch (Exception e) {
//
//            e.printStackTrace();
//
//        }
//
//        return result;
//
//    }
//    public boolean addDETBySize(String no, List<ISMSDWD> invDetailList, int sizedown, int sizemas, String user) {
//
//        boolean result = false;
//
//        String sql = " INSERT INTO [RMShipment].[dbo].[ISMSDWD] ([ISDCONO],[ISDDWQNO],[ISDORD],[ISDSEQ],[ISDLINO],[ISDITNO],[ISDPLANT]\n"
//                + "      ,[ISDVT],[ISDSTRG],[ISDRQQTY],[ISDUNIT],[ISDPRICE],[ISDUNR01],[ISDUNR03],[ISDMTCTRL],[ISDDEST],[ISDREMK]\n"
//                + "      ,[ISDEDT],[ISDETM],[ISDEUSR],[ISDCDT],[ISDCTM],[ISDUSER]) VALUES \n";
//
//        for (int i = sizemas; i < sizedown; i++) {
//            sql += "	 ('TWC'," + no + "," + invDetailList.get(i).getISDORD() + "," + invDetailList.get(i).getISDSEQ() + "," + invDetailList.get(i).getISDLINO()
//                    + "," + invDetailList.get(i).getISDITNO() + "," + invDetailList.get(i).getISDPLANT() + "," + invDetailList.get(i).getISDVT() + ","
//                    + invDetailList.get(i).getISDSTRG() + "," + invDetailList.get(i).getISDRQQTY() + "," + invDetailList.get(i).getISDUNIT() + "," + invDetailList.get(i).getISDPRICE()
//                    + "," + invDetailList.get(i).getISDUNR01() + "," + invDetailList.get(i).getISDUNR03() + "," + invDetailList.get(i).getISDMTCTRL() + "," + invDetailList.get(i).getISDDEST()
//                    + "," + invDetailList.get(i).getISDREMK() + ",FORMAT(CURRENT_TIMESTAMP,'yyyyMMdd'),FORMAT(CURRENT_TIMESTAMP,'HHmmss'),"
//                    + user + ",FORMAT(CURRENT_TIMESTAMP,'yyyyMMdd'),FORMAT(CURRENT_TIMESTAMP,'HHmmss')," + user + ")";
//
//            if (i != (invDetailList.size() - 1)) {
//                sql += ",";
//            }
//        }
//
//        try {
//
//            PreparedStatement ps = connect.prepareStatement(sql);
//
//            int record = ps.executeUpdate();
//
//            if (record >= 1) {
//
//                result = true;
//
//            } else {
//
//                result = false;
//
//            }
//
//            connect.close();
//
//        } catch (Exception e) {
//
//            e.printStackTrace();
//
//        }
////
//        return result;
//
//    }
//    public boolean CheckISMSDWS() {
//
//        String sql = "SELECT TOP 1 * FROM [RMShipment].[dbo].[ISMSDWS] WHERE ISSCDT = '20220520'";
//
//        return true;
//    }
    public boolean addStockList(List<ISMSDWS> sl, String user) {

        user = checkNull(user);

        boolean result = false;

        String sql = " INSERT INTO [RMShipment].[dbo].[ISMSDWS] ([ISSCONO],[ISSMTNO],[ISSPLANT],[ISSSTORE],[ISSBTNO],[ISSMTGP],[ISSSBIN],[ISSMDES],[ISSBUOM],[ISSUNR]\n"
                + ",[ISSQI],[ISSBLOCK],[ISSTOTQ],[ISSMAVG],[ISSTOTA],[ISSEDT],[ISSETM],[ISSEUSR],[ISSCDT],[ISSCTM],[ISSUSER]) VALUES \n";

        for (int i = 0; i < sl.size(); i++) {
            String matNo = checkNull(sl.get(i).getISSMTNO());
            String plnt = checkNull(sl.get(i).getISSPLANT());
            String storeLo = checkNull(sl.get(i).getISSSTORE());
            String batchNo = checkNull(sl.get(i).getISSBTNO());
            String matGrp = checkNull(sl.get(i).getISSMTGP());
            String matCt = checkNull(sl.get(i).getISSSBIN());
            String matCtNm = checkNull(sl.get(i).getISSMDES());
            String unitM = checkNull(sl.get(i).getISSBUOM());
            String unrQTY = sl.get(i).getISSUNR();
            String qiQTY = sl.get(i).getISSQI();
            String blckQTY = sl.get(i).getISSBLOCK();
            String ttQTY = sl.get(i).getISSTOTQ();
            String movAVG = sl.get(i).getISSMAVG();
            String ttAMT = sl.get(i).getISSTOTA();

            if (unrQTY == null) {
                unrQTY = "NULL";
            } else {
                if (unrQTY.equals("")) {
                    unrQTY = "NULL";
                }
            }
            if (qiQTY == null) {
                qiQTY = "NULL";
            } else {
                if (qiQTY.equals("")) {
                    qiQTY = "NULL";
                }
            }
            if (blckQTY == null) {
                blckQTY = "NULL";
            } else {
                if (blckQTY.equals("")) {
                    blckQTY = "NULL";
                }
            }
            if (ttQTY == null) {
                ttQTY = "NULL";
            } else {
                if (ttQTY.equals("")) {
                    ttQTY = "NULL";
                }
            }
            if (movAVG == null) {
                movAVG = "NULL";
            } else {
                if (movAVG.equals("")) {
                    movAVG = "NULL";
                }
            }
            if (ttAMT == null) {
                ttAMT = "NULL";
            } else {
                if (ttAMT.equals("")) {
                    ttAMT = "NULL";
                }
            }

            sql += " ('TWC'," + matNo + "," + plnt + "," + storeLo + "," + batchNo + "," + matGrp + "," + matCt + "," + matCtNm + ","
                    + unitM + "," + unrQTY + "," + qiQTY + "," + blckQTY + "," + ttQTY + "," + movAVG + "," + ttAMT
                    + ",NULL,NULL,NULL,FORMAT(CURRENT_TIMESTAMP,'yyyyMMdd'),FORMAT(CURRENT_TIMESTAMP,'HHmmss')," + user + ") ";

            if (i != (sl.size() - 1)) {
                sql += ",\n";
            }

        }

//        System.out.println("" + sql);
        try {
            PreparedStatement ps = connect.prepareStatement(sql);
            int record = ps.executeUpdate();
//
            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            ps.close();
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

//    public boolean addISMMASHfWSC(ISMSDWH head, String user) {
//
//        String order = checkNull(head.getISHORD());
//        String seq = checkNull(head.getISHSEQ());
////        String trdt = head.getISHTRDT().replace("-", "");
//        String uart = checkNull(head.getISHUART());
//        String weg = checkNull(head.getISHTWEG());
//        String movement = checkNull(head.getISHMVT());
//        String cusno = checkNull(head.getISHCUNO());
//        String cusname1 = checkNull(head.getISHCUNM1());
//        String cusname2 = checkNull(head.getISHCUNM2());
//        String cusadd1 = checkNull(head.getISHCUAD1());
//        String cusadd2 = checkNull(head.getISHCUAD2());
//        String po = checkNull(head.getISHBSTKD());
//        String collec = checkNull(head.getISHSUBMI());
//        String fg = checkNull(head.getISHMATLOT());
//        String taxno = checkNull(head.getISHTAXNO());
//        String branch = checkNull(head.getISHBRANCH01());
//        String style = checkNull(head.getISHSTYLE());
//        String color = checkNull(head.getISHCOLOR());
//        String lot = checkNull(head.getISHLOT());
//        String fgquean = checkNull(head.getISHAMTFG());
//        String style2 = checkNull(head.getISHSTYLE2());
//        String nor = checkNull(head.getISHNOR());
//        String pofg = checkNull(head.getISHPOFG());
//        String whno = checkNull(head.getISHWHNO());
//        String prgp = checkNull(head.getISHPGRP());
//        String Lsts = checkNull(head.getISHLSTS());
//        String Hsts = checkNull(head.getISHHSTS());
//        String dest = checkNull(head.getISHDEST());
//        String remk = checkNull(head.getISHREMK());
//
//        String atum = checkNull(head.getISHDATUM());
//        String grpno = checkNull(head.getISHGRPNO());
//        String userhead = checkNull(head.getISHEUSR());
//
//        boolean result = false; //56
//
//        String sql = "INSERT INTO [RMShipment].[dbo].[ISMMASH] (ISHCONO,ISHORD,ISHSEQ,ISHTRDT,ISHUART,ISHTWEG,ISHMVT,ISHCUNO,ISHCUNM1,ISHCUNM2,ISHCUAD1,ISHCUAD2\n"
//                + ",ISHBSTKD,ISHSUBMI,ISHMATLOT,ISHTAXNO,ISHBRANCH01,ISHDATUM,ISHSTYLE,ISHCOLOR,ISHLOT,ISHAMTFG,ISHSTYLE2,ISHNOR,ISHPOFG\n"
//                + ",ISHGRPNO,ISHWHNO,ISHPGRP,ISHLSTS,ISHHSTS,ISHDEST,ISHRQDT,ISHRQTM,ISHAPDT,ISHAPTM,ISHAPUSR,ISHRCDT,ISHRCTM,ISHRCUSR,ISHPKDT,ISHPKTM,ISHPKUSR\n"
//                + ",ISHTPDT,ISHTPTM,ISHTPUSR,ISHP1DT,ISHA1DT,ISHT1DT,ISHJBNO,ISHREMARK,ISHEDT,ISHETM,ISHEUSR,ISHCDT,ISHCTM,ISHUSER) VALUES (\n"
//                + "'TWC'," + order + "," + seq + ",FORMAT(CURRENT_TIMESTAMP,'yyyyMMdd')," + uart + "," + weg + "," + movement + "," + cusno + "," + cusname1 + "," + cusname2 + "," + cusadd1 + "," + cusadd2 + "," + po + "," + collec + "," + fg + "," + taxno
//                + "," + branch + "," + atum + "," + style + "," + color + "," + lot + "," + fgquean + "," + style2 + "," + nor + "," + pofg + "," + grpno + "," + whno + "," + prgp + "," + Lsts + "," + Hsts + "," + dest + "\n"
//                + ",NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL," + remk + "\n"
//                + ",FORMAT(CURRENT_TIMESTAMP,'yyyyMMdd'),FORMAT(CURRENT_TIMESTAMP,'HHmmss')," + userhead + ",FORMAT(CURRENT_TIMESTAMP,'yyyyMMdd'),FORMAT(CURRENT_TIMESTAMP,'HHmmss')," + user + ")";
//
////        System.out.println("sql " + sql);
//        try {
//            PreparedStatement ps = connect.prepareStatement(sql);
//            int record = ps.executeUpdate();
//
//            if (record >= 1) {
//
//                result = true;
//
//            } else {
//
//                result = false;
//
//            }
//
//            ps.close();
//            connect.close();
//
//        } catch (Exception e) {
//
//            e.printStackTrace();
//
//        }
//
//        return result;
//
//    }
//    public boolean addISMMASHfWSCSEQ(ISMSDWH head, String user, String seqin) {
//
//        String order = checkNull(head.getISHORD());
//        String seq = seqin;
////        String trdt = head.getISHTRDT().replace("-", "");
//        String uart = checkNull(head.getISHUART());
//        String weg = checkNull(head.getISHTWEG());
//        String movement = checkNull(head.getISHMVT());
//        String cusno = checkNull(head.getISHCUNO());
//        String cusname1 = checkNull(head.getISHCUNM1());
//        String cusname2 = checkNull(head.getISHCUNM2());
//        String cusadd1 = checkNull(head.getISHCUAD1());
//        String cusadd2 = checkNull(head.getISHCUAD2());
//        String po = checkNull(head.getISHBSTKD());
//        String collec = checkNull(head.getISHSUBMI());
//        String fg = checkNull(head.getISHMATLOT());
//        String taxno = checkNull(head.getISHTAXNO());
//        String branch = checkNull(head.getISHBRANCH01());
//        String style = checkNull(head.getISHSTYLE());
//        String color = checkNull(head.getISHCOLOR());
//        String lot = checkNull(head.getISHLOT());
//        String fgquean = checkNull(head.getISHAMTFG());
//        String style2 = checkNull(head.getISHSTYLE2());
//        String nor = checkNull(head.getISHNOR());
//        String pofg = checkNull(head.getISHPOFG());
//        String whno = checkNull(head.getISHWHNO());
//        String prgp = checkNull(head.getISHPGRP());
//        String Lsts = checkNull(head.getISHLSTS());
//        String Hsts = checkNull(head.getISHHSTS());
//        String dest = checkNull(head.getISHDEST());
//        String remk = checkNull(head.getISHREMK());
//
//        String atum = checkNull(head.getISHDATUM());
//        String grpno = checkNull(head.getISHGRPNO());
//        String userhead = checkNull(head.getISHEUSR());
//
//        boolean result = false; //56
//
//        String sql = "INSERT INTO [RMShipment].[dbo].[ISMMASH] (ISHCONO,ISHORD,ISHSEQ,ISHTRDT,ISHUART,ISHTWEG,ISHMVT,ISHCUNO,ISHCUNM1,ISHCUNM2,ISHCUAD1,ISHCUAD2\n"
//                + ",ISHBSTKD,ISHSUBMI,ISHMATLOT,ISHTAXNO,ISHBRANCH01,ISHDATUM,ISHSTYLE,ISHCOLOR,ISHLOT,ISHAMTFG,ISHSTYLE2,ISHNOR,ISHPOFG\n"
//                + ",ISHGRPNO,ISHWHNO,ISHPGRP,ISHLSTS,ISHHSTS,ISHDEST,ISHRQDT,ISHRQTM,ISHAPDT,ISHAPTM,ISHAPUSR,ISHRCDT,ISHRCTM,ISHRCUSR,ISHPKDT,ISHPKTM,ISHPKUSR\n"
//                + ",ISHTPDT,ISHTPTM,ISHTPUSR,ISHP1DT,ISHA1DT,ISHT1DT,ISHJBNO,ISHREMARK,ISHEDT,ISHETM,ISHEUSR,ISHCDT,ISHCTM,ISHUSER) VALUES (\n"
//                + "'TWC'," + order + "," + seq + ",FORMAT(CURRENT_TIMESTAMP,'yyyyMMdd')," + uart + "," + weg + "," + movement + "," + cusno + "," + cusname1 + "," + cusname2 + "," + cusadd1 + "," + cusadd2 + "," + po + "," + collec + "," + fg + "," + taxno
//                + "," + branch + "," + atum + "," + style + "," + color + "," + lot + "," + fgquean + "," + style2 + "," + nor + "," + pofg + "," + grpno + "," + whno + "," + prgp + "," + Lsts + "," + Hsts + "," + dest + "\n"
//                + ",NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL," + remk + "\n"
//                + ",FORMAT(CURRENT_TIMESTAMP,'yyyyMMdd'),FORMAT(CURRENT_TIMESTAMP,'HHmmss')," + userhead + ",FORMAT(CURRENT_TIMESTAMP,'yyyyMMdd'),FORMAT(CURRENT_TIMESTAMP,'HHmmss')," + user + ")";
//
////        System.out.println("sql " + sql);
//        try {
//            PreparedStatement ps = connect.prepareStatement(sql);
//            int record = ps.executeUpdate();
//
//            if (record >= 1) {
//
//                result = true;
//
//            } else {
//
//                result = false;
//
//            }
//
//            ps.close();
//            connect.close();
//
//        } catch (Exception e) {
//
//            e.printStackTrace();
//
//        }
//
//        return result;
//
//    }
    public boolean addISMMASH(String order, String seq, String trdt, String uart, String weg, String movement, String cusno, String cusname1, String cusname2, String cusadd1, String cusadd2, String po,
            String collec, String fg, String taxno, String branch, String atum, String style, String color, String lot, String fgquean, String style2, String nor, String pofg, String grpno, String whno, String prgp,
            String dest, String reason, String user, String lsts, String hsts) {

//        String order = head.getISHORD();
//        String seq = head.getISHSEQ();
//        String trdt = head.getISHTRDT();
//        String uart = head.getISHUART();
//        String weg = head.getISHTWEG();
//        String movement = head.getISHMVT();
//        String cusno = head.getISHCUNO();
//        String cusname1 = head.getISHCUNM1();
//        String cusname2 = head.getISHCUNM2();
//        String cusadd1 = head.getISHCUAD1();
//        String cusadd2 = head.getISHCUAD2();
//        String po = head.getISHBSTKD();
//        String collec = head.getISHSUBMI();
//        String fg = head.getISHMATLOT();
//        String taxno = head.getISHTAXNO();
//        String branch = head.getISHBRANCH01();
//        String style = head.getISHSTYLE();
//        String color = head.getISHCOLOR();
//        String lot = head.getISHLOT();
//        String fgquean = head.getISHAMTFG();
//        String style2 = head.getISHSTYLE2();
//        String nor = head.getISHNOR();
//        String pofg = head.getISHPOFG();
//        String whno = head.getISHWHNO();
//        String prgp = head.getISHPGRP();
//        String Lsts = head.getISHLSTS();
//        String Hsts = head.getISHHSTS();
//        String dest = head.getISHDEST();
//        String remk = head.getISHREMK();
//
//        String atum = head.getISHDATUM();
//        String grpno = head.getISHGRPNO();
//        System.out.println("in ISMMASH");
        boolean result = false; //56

        String sql = "INSERT INTO ISMMASH (ISHCONO,ISHORD,ISHSEQ,ISHTRDT,ISHUART,ISHTWEG,ISHMVT,ISHCUNO,ISHCUNM1,ISHCUNM2,ISHCUAD1,ISHCUAD2\n"
                + ",ISHBSTKD,ISHSUBMI,ISHMATLOT,ISHTAXNO,ISHBRANCH01,ISHDATUM,ISHSTYLE,ISHCOLOR,ISHLOT,ISHAMTFG,ISHSTYLE2,ISHNOR,ISHPOFG\n"
                + ",ISHGRPNO,ISHWHNO,ISHPGRP,ISHLSTS,ISHHSTS,ISHDEST,ISHRQDT,ISHRQTM,ISHAPDT,ISHAPTM,ISHAPUSR,ISHRCDT,ISHRCTM,ISHRCUSR,ISHPKDT,ISHPKTM,ISHPKUSR\n"
                + ",ISHTPDT,ISHTPTM,ISHTPUSR,ISHP1DT,ISHA1DT,ISHT1DT,ISHJBNO,ISHREMARK,ISHEDT,ISHETM,ISHEUSR,ISHCDT,ISHCTM,ISHUSER,ISHREASON) VALUES (\n"
                + "'TWC',?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?\n"
                + ",NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,?\n"
                + ",FORMAT(CURRENT_TIMESTAMP,'yyyyMMdd'),FORMAT(CURRENT_TIMESTAMP,'HHmmss'),?,FORMAT(CURRENT_TIMESTAMP,'yyyyMMdd'),FORMAT(CURRENT_TIMESTAMP,'HHmmss'),?,?)";
//        String sql1 = "INSERT INTO ISMMASH (ISHCONO,ISHORD,ISHSEQ,ISHTRDT,ISHUART,ISHTWEG,ISHMVT,ISHCUNO,ISHCUNM1,ISHCUNM2,ISHCUAD1,ISHCUAD2\n"
//                + ",ISHBSTKD,ISHSUBMI,ISHMATLOT,ISHTAXNO,ISHBRANCH01,ISHDATUM,ISHSTYLE,ISHCOLOR,ISHLOT,ISHAMTFG,ISHSTYLE2,ISHNOR,ISHPOFG\n"
//                + ",ISHGRPNO,ISHWHNO,ISHPGRP,ISHLSTS,ISHHSTS,ISHDEST,ISHRQDT,ISHRQTM,ISHAPDT,ISHAPTM,ISHAPUSR,ISHRCDT,ISHRCTM,ISHRCUSR,ISHPKDT,ISHPKTM,ISHPKUSR\n"
//                + ",ISHTPDT,ISHTPTM,ISHTPUSR,ISHP1DT,ISHA1DT,ISHT1DT,ISHJBNO,ISHREMARK,ISHEDT,ISHETM,ISHEUSR,ISHCDT,ISHCTM,ISHUSER) VALUES (\n"
//                + "'TWC',"+order+"",?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,0,0,?\n"
//                + "NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL\n"
//                + ",FORMAT(CURRENT_TIMESTAMP,'yyyyMMdd'),FORMAT(CURRENT_TIMESTAMP,'HHmmss'),?,FORMAT(CURRENT_TIMESTAMP,'yyyyMMdd'),FORMAT(CURRENT_TIMESTAMP,'HHmmss'),?)";

        try {
//            System.out.println("sql1 " +sql1);
            PreparedStatement ps = connect.prepareStatement(sql);

            ps.setString(1, order);
            ps.setString(2, seq);
            ps.setString(3, trdt);
            ps.setString(4, uart);
            ps.setString(5, weg);
            ps.setString(6, movement);
            ps.setString(7, cusno);
            ps.setString(8, cusname1);
            ps.setString(9, cusname2);
            ps.setString(10, cusadd1);
            ps.setString(11, cusadd2);
            ps.setString(12, po);
            ps.setString(13, collec);
            ps.setString(14, fg);
            ps.setString(15, taxno);
            ps.setString(16, branch);
            ps.setString(17, atum);
            ps.setString(18, style);
            ps.setString(19, color);
            ps.setString(20, lot);
            ps.setString(21, fgquean);
            ps.setString(22, style2);
            ps.setString(23, nor);
            ps.setString(24, pofg);
            ps.setString(25, grpno);
            ps.setString(26, whno);
            ps.setString(27, prgp);
            ps.setString(28, lsts);
            ps.setString(29, lsts);
            ps.setString(30, dest);
            ps.setString(31, ""); //remk
            ps.setString(32, user);
            ps.setString(33, user);
            ps.setString(34, reason);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            ps.close();
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

//    public boolean addISMMASDfWSC(List<ISMSDWD> invDetailList, String user) {
//
//        boolean result = false;
//
//        String sql = "INSERT INTO [ISMMASD] (ISDCONO,ISDORD,ISDSEQ,ISDLINO,ISDSINO,ISDITNO,ISDPLANT,ISDVT,ISDSTRG,ISDRQQTY,ISDUNIT,ISDPRICE,ISDUNR01,ISDUNR03\n"
//                + ",ISDMTCTRL,ISDDEST,ISDRQDT,ISDRQTM,ISDAPDT,ISDAPTM,ISDAPUSER,ISDRCDT,ISDRCTM,ISDRCUSER,ISDPKQTY,ISDPKDT,ISDPKTM,ISDPKUSER,ISDTPQTY,ISDTPDT\n"
//                + ",ISDTPTM,ISDTPUSER,ISDSTS,ISDJBNO,ISDQNO,ISDREMARK,ISDISUNO,ISDTEMP,ISDEDT,ISDETM,ISDEUSR,ISDCDT,ISDCTM,ISDUSER,ISDSTAD,ISDSTSTMP) VALUES \n";
////                + "('TWC',?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,NULL,NULL,NULL,NULL,NULL,NULL\n"
////                + ",NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,?,NULL,NULL,NULL,0,NULL"
////                + ",FORMAT(CURRENT_TIMESTAMP,'yyyyMMdd'),FORMAT(CURRENT_TIMESTAMP,'HHmmss'),?,FORMAT(CURRENT_TIMESTAMP,'yyyyMMdd'),FORMAT(CURRENT_TIMESTAMP,'HHmmss'),?,'1',NULL\n"
////                + ")";
//
//        for (int i = 0; i < invDetailList.size(); i++) {
//
//            String order = checkNull(invDetailList.get(i).getISDORD());
//            String seq = checkNull(invDetailList.get(i).getISDSEQ());
//            String lino = checkNull(invDetailList.get(i).getISDLINO());
//            String sino = invDetailList.get(i).getISDSINO();
//            String dito = checkNull(invDetailList.get(i).getISDITNO());
//            String plant = checkNull(invDetailList.get(i).getISDPLANT());
//            String vt = checkNull(invDetailList.get(i).getISDVT());
//            String strg = checkNull(invDetailList.get(i).getISDSTRG());
//            String qty = invDetailList.get(i).getISDRQQTY();
//            String unit = checkNull(invDetailList.get(i).getISDUNIT());
//            String price = invDetailList.get(i).getISDPRICE();
//            String unr1 = invDetailList.get(i).getISDUNR01();
//            String unr3 = invDetailList.get(i).getISDUNR03();
//            String matctrl = checkNull(invDetailList.get(i).getISDMTCTRL());
//            String dest = checkNull(invDetailList.get(i).getISDDEST());
//            String sts = checkNull(invDetailList.get(i).getISDSTS());
//            String remrk = checkNull(invDetailList.get(i).getISDREMK());
//            String userdet = checkNull(invDetailList.get(i).getISDEUSR());
//
//            if (sino == null) {
//                sino = "NULL";
//            } else {
//                if (sino.equals("")) {
//                    sino = "NULL";
//                }
//            }
//
//            if (qty == null) {
//                qty = "NULL";
//            } else {
//                if (qty.equals("")) {
//                    qty = "NULL";
//                }
//            }
//
//            if (price == null) {
//                price = "NULL";
//            } else {
//                if (price.equals("")) {
//                    price = "NULL";
//                }
//            }
//
//            if (unr1 == null) {
//                unr1 = "NULL";
//            } else {
//                if (unr1.equals("")) {
//                    unr1 = "NULL";
//                }
//            }
//
//            if (unr3 == null) {
//                unr3 = "NULL";
//            } else {
//                if (unr3.equals("")) {
//                    unr3 = "NULL";
//                }
//            }
//
//            sql += "('TWC'," + order + "," + seq + "," + lino + "," + sino + "," + dito + "," + plant + "," + vt + "," + strg + "," + qty + "," + unit + "," + price + "," + unr1 + "," + unr3 + "," + matctrl + "," + dest + ",NULL,NULL,NULL,NULL,NULL,NULL\n"
//                    + ",NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL," + sts + ",NULL,NULL," + remrk + ",0,NULL"
//                    + ",FORMAT(CURRENT_TIMESTAMP,'yyyyMMdd'),FORMAT(CURRENT_TIMESTAMP,'HHmmss')," + userdet + ",FORMAT(CURRENT_TIMESTAMP,'yyyyMMdd'),FORMAT(CURRENT_TIMESTAMP,'HHmmss')," + user + ",'1',NULL\n"
//                    + ")";
//
//            if (i != (invDetailList.size() - 1)) {
//                sql += ",";
//            }
//        }
////        String sql2  = "INSERT INTO [ISMMASD] (ISDCONO,ISDORD,ISDSEQ,ISDLINO,ISDSINO,ISDITNO,ISDPLANT,ISDVT,ISDSTRG,ISDRQQTY,ISDUNIT,ISDPRICE,ISDUNR01,ISDUNR03\n"
////                + ",ISDMTCTRL,ISDDEST,ISDRQDT,ISDRQTM,ISDAPDT,ISDAPTM,ISDAPUSER,ISDRCDT,ISDRCTM,ISDRCUSER,ISDPKQTY,ISDPKDT,ISDPKTM,ISDPKUSER,ISDTPQTY,ISDTPDT\n"
////                + ",ISDTPTM,ISDTPUSER,ISDSTS,ISDJBNO,ISDQNO,ISDREMARK,ISDISUNO,ISDTEMP,ISDEDT,ISDETM,ISDEUSR,ISDCDT,ISDCTM,ISDUSER,ISDSTAD,ISDSTSTMP) VALUES (\n"
////                + "'TWC',"+order+","+seq+","+lino+","+sino+","+dito+","+plant+","+vt+","+strg+","+qty+","+unit+","+price+","+unr1+","+unr3+","+matctrl+","+dest+",NULL,NULL,NULL,NULL,NULL,NULL\n"
////                + ",NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,"+sts+",NULL,NULL,NULL,1,NULL"
////                + ",FORMAT(CURRENT_TIMESTAMP,'yyyyMMdd'),FORMAT(CURRENT_TIMESTAMP,'HHmmss'),"+user+",FORMAT(CURRENT_TIMESTAMP,'yyyyMMdd'),FORMAT(CURRENT_TIMESTAMP,'HHmmss'),"+user+",'1',NULL\n"
////                + ")";
//        try {
//
////            System.out.println("" + sql2);
//            PreparedStatement ps = connect.prepareStatement(sql);
//
////            ps.setString(1, order);
////            ps.setString(2, seq);
////            ps.setString(3, lino);
////            ps.setString(4, sino);
////            ps.setString(5, dito);
////            ps.setString(6, plant);
////            ps.setString(7, vt);
////            ps.setString(8, strg);
////            ps.setString(9, qty);
////            ps.setString(10, unit);
////            ps.setString(11, price);
////            ps.setString(12, unr1);
////            ps.setString(13, unr3);
////            ps.setString(14, matctrl);
////            ps.setString(15, dest);
////            ps.setString(16, sts);
////            ps.setString(17, user);
////            ps.setString(18, user);
//            int record = ps.executeUpdate();
//
//            if (record >= 1) {
//
//                result = true;
//
//            } else {
//
//                result = false;
//
//            }
//
//            ps.close();
//            connect.close();
//
//        } catch (Exception e) {
//
//            e.printStackTrace();
//
//        }
////
//        return result;
//
//    }
    public boolean addISMMASD(String order, String seq, String lino, String sino, String dito, String plant, String vt, String strg, String qty, String unit, String price, String unr1, String unr3, String matctrl, String dest, String purg, String user, String sts) {

        boolean result = false;

        String sql = "INSERT INTO [ISMMASD] (ISDCONO,ISDORD,ISDSEQ,ISDLINO,ISDSINO,ISDITNO,ISDPLANT,ISDVT,ISDSTRG,ISDRQQTY,ISDUNIT,ISDPRICE,ISDUNR01,ISDUNR03\n"
                + ",ISDMTCTRL,ISDDEST,ISDRQDT,ISDRQTM,ISDAPDT,ISDAPTM,ISDAPUSER,ISDRCDT,ISDRCTM,ISDRCUSER,ISDPKQTY,ISDPKDT,ISDPKTM,ISDPKUSER,ISDTPQTY,ISDTPDT\n"
                + ",ISDTPTM,ISDTPUSER,ISDSTS,ISDJBNO,ISDQNO,ISDREMARK,ISDISUNO,ISDTEMP,ISDEDT,ISDETM,ISDEUSR,ISDCDT,ISDCTM,ISDUSER,ISDSTAD,ISDSTSTMP,ISDPURG) VALUES \n"
                + "('TWC',?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,NULL,NULL,NULL,NULL,NULL,NULL\n"
                + ",NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,?,NULL,NULL,NULL,0,NULL"
                + ",FORMAT(CURRENT_TIMESTAMP,'yyyyMMdd'),FORMAT(CURRENT_TIMESTAMP,'HHmmss'),?,FORMAT(CURRENT_TIMESTAMP,'yyyyMMdd'),FORMAT(CURRENT_TIMESTAMP,'HHmmss'),?,'1',NULL,?\n"
                + ")";

//        for (int i = 0; i < invDetailList.size(); i++) {
//            String order = invDetailList.get(i).getISDORD();
//            String seq = invDetailList.get(i).getISDSEQ();
//            String lino = invDetailList.get(i).getISDLINO();
//            String sino = invDetailList.get(i).getISDSINO();
//            String dito = invDetailList.get(i).getISDITNO();
//            String plant = invDetailList.get(i).getISDPLANT();
//            String vt = invDetailList.get(i).getISDVT();
//            String strg = invDetailList.get(i).getISDSTRG();
//            String qty = invDetailList.get(i).getISDRQQTY();
//            String unit = invDetailList.get(i).getISDUNIT();
//            String price = invDetailList.get(i).getISDPRICE();
//            String unr1 = invDetailList.get(i).getISDUNR01();
//            String unr3 = invDetailList.get(i).getISDUNR03();
//            String matctrl = invDetailList.get(i).getISDMTCTRL();
//            String dest = invDetailList.get(i).getISDDEST();
//            String sts = invDetailList.get(i).getISDSTS();
//            String remrk = invDetailList.get(i).getISDREMK();
//            sql += "('TWC'," + order + "," + seq + "," + lino + "," + sino + "," + dito + "," + plant + "," + vt + "," + strg + "," + qty + "," + unit + "," + price + "," + unr1 + "," + unr3 + "," + matctrl + "," + dest + ",NULL,NULL,NULL,NULL,NULL,NULL\n"
//                    + ",NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL," + sts + ",NULL,NULL," + remrk + ",0,NULL"
//                    + ",FORMAT(CURRENT_TIMESTAMP,'yyyyMMdd'),FORMAT(CURRENT_TIMESTAMP,'HHmmss')," + user + ",FORMAT(CURRENT_TIMESTAMP,'yyyyMMdd'),FORMAT(CURRENT_TIMESTAMP,'HHmmss')," + user + ",'1',NULL\n"
//                    + ")";
//
//            if (i != (invDetailList.size() - 1)) {
//                sql += ",";
//            }
//        }
//        String sql2 = "INSERT INTO [ISMMASD] (ISDCONO,ISDORD,ISDSEQ,ISDLINO,ISDSINO,ISDITNO,ISDPLANT,ISDVT,ISDSTRG,ISDRQQTY,ISDUNIT,ISDPRICE,ISDUNR01,ISDUNR03\n"
//                + ",ISDMTCTRL,ISDDEST,ISDRQDT,ISDRQTM,ISDAPDT,ISDAPTM,ISDAPUSER,ISDRCDT,ISDRCTM,ISDRCUSER,ISDPKQTY,ISDPKDT,ISDPKTM,ISDPKUSER,ISDTPQTY,ISDTPDT\n"
//                + ",ISDTPTM,ISDTPUSER,ISDSTS,ISDJBNO,ISDQNO,ISDREMARK,ISDISUNO,ISDTEMP,ISDEDT,ISDETM,ISDEUSR,ISDCDT,ISDCTM,ISDUSER,ISDSTAD,ISDSTSTMP) VALUES (\n"
//                + "'TWC'," + order + "," + seq + "," + lino + "," + sino + "," + dito + "," + plant + "," + vt + "," + strg + "," + qty + "," + unit + "," + price + "," + unr1 + "," + unr3 + "," + matctrl + "," + dest + ",NULL,NULL,NULL,NULL,NULL,NULL\n"
//                + ",NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL," + sts + ",NULL,NULL,NULL,1,NULL"
//                + ",FORMAT(CURRENT_TIMESTAMP,'yyyyMMdd'),FORMAT(CURRENT_TIMESTAMP,'HHmmss')," + user + ",FORMAT(CURRENT_TIMESTAMP,'yyyyMMdd'),FORMAT(CURRENT_TIMESTAMP,'HHmmss')," + user + ",'1',NULL\n"
//                + ")";
        try {

            if (qty == null) {
                qty = "0.00";
            } else {
                if (qty.equals("")) {
                    qty = "0.00";
                }
            }
            if (price == null) {
                price = "0.00";
            } else {
                if (price.equals("")) {
                    price = "0.00";
                }
            }
            if (unr1 == null) {
                unr1 = "0.00";
            } else {
                if (unr1.equals("")) {
                    unr1 = "0.00";
                }
            }
            if (unr3 == null) {
                unr3 = "0.00";
            } else {
                if (unr3.equals("")) {
                    unr3 = "0.00";
                }
            }

            PreparedStatement ps = connect.prepareStatement(sql);

            ps.setString(1, order);
            ps.setString(2, seq);
            ps.setString(3, lino);
            ps.setString(4, sino);
            ps.setString(5, dito);
            ps.setString(6, plant);
            ps.setString(7, vt);
            ps.setString(8, strg);
            ps.setDouble(9, Double.parseDouble(qty));
            ps.setString(10, unit);
            ps.setDouble(11, Double.parseDouble(price));
            ps.setDouble(12, Double.parseDouble(unr1));
            ps.setDouble(13, Double.parseDouble(unr3));
            ps.setString(14, matctrl);
            ps.setString(15, dest);
            ps.setString(16, sts);
            ps.setString(17, user);
            ps.setString(18, user);
            ps.setString(19, purg);
            int record = ps.executeUpdate();

//            System.out.println("record " + record);
            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            ps.close();
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }
//
        return result;

    }

    public boolean addISMSDWD(String order, String seqNew, String dwnno, String no) {

        boolean result = false;
        //select by order ditno

        String sqlSel = "SELECT DW.*\n"
                + ",ISNULL(DE.[ISDSTS],'0') AS ISDSTS\n"
                + ",ISNULL(CASE WHEN DE.[ISDSTS] IS NULL THEN 'ขายเพิ่ม' ELSE (CASE WHEN DE.[ISDSTS] != '0' THEN '' ELSE '' END) END,'') AS REASON\n"
                + "FROM ISMSDWD DW\n"
                + "LEFT JOIN ISMMASD DE ON DE.ISDORD = DW.ISDORD AND DE.ISDLINO = DW.ISDLINO\n"
                + "WHERE DW.[ISDORD] = '" + order + "' AND (ISDSTS = 9 OR DE.ISDSTS IS NULL) AND DW.ISDSEQ = (SELECT TOP 1 ISHSEQ FROM ISMSDWH WHERE ISHORD = '" + order + "' AND ISHDWQNO = '" + no + "' ) AND DW.ISDDWQNO = '" + no + "'";

        try {

            Statement stmt = connect.createStatement();
            ResultSet rs = stmt.executeQuery(sqlSel);

            while (rs.next()) {

                if (rs.getString("ISDSTS").equals("9")) {

                    String sql = "INSERT INTO [ISMSDWD] ([ISDCONO],[ISDDWQNO],[ISDORD],[ISDSEQ],[ISDLINO],[ISDITNO],[ISDBAT],[ISDMGRP],[ISDSTEXT],[ISDITCAT],[ISDRSRJ],[ISDRSRJT],[ISDPLANT],[ISDVT],[ISDSTRG]\n"
                            + "      ,[ISDRQQTY],[ISDDLQTY],[ISDUNIT],[ISDBUNIT],[ISDCON1],[ISDCON2],[ISDPRICE],[ISDNETV],[ISDCUR],[ISDEXC],[ISDUNR01],[ISDUNR03],[ISDMTCTRL],[ISDPFCT]\n"
                            + "      ,[ISDACGP],[ISDODT],[ISDUDT],[ISDRQDT],[ISDDLS],[ISDODLS],[ISDCRDT],[ISDCRTM],[ISDSUSER],[ISDDEST],[ISDREMK],[ISDEDT],[ISDETM],[ISDEUSR],[ISDCDT],[ISDCTM],[ISDUSER]) VALUES \n"
                            + "('TWC',?,?,?,?,?,?,?,?,?,?,?,?,?,?\n"
                            + "      ,?,?,?,?,?,?,?,?,?,?,?,?,?,?\n"
                            + "      ,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

                    PreparedStatement ps = connect.prepareStatement(sql);

                    ps.setString(1, dwnno);
                    ps.setString(2, rs.getString("ISDORD"));
                    ps.setString(3, seqNew);
                    ps.setString(4, rs.getString("ISDLINO"));
                    ps.setString(5, rs.getString("ISDITNO"));
                    ps.setString(6, rs.getString("ISDBAT"));
                    ps.setString(7, rs.getString("ISDMGRP"));
                    ps.setString(8, rs.getString("ISDSTEXT"));
                    ps.setString(9, rs.getString("ISDITCAT"));
                    ps.setString(10, rs.getString("ISDRSRJ"));
                    ps.setString(11, rs.getString("ISDRSRJT"));
                    ps.setString(12, rs.getString("ISDPLANT"));
                    ps.setString(13, rs.getString("ISDVT"));
                    ps.setString(14, rs.getString("ISDSTRG"));
                    ps.setString(15, rs.getString("ISDRQQTY"));
                    ps.setString(16, rs.getString("ISDDLQTY"));
                    ps.setString(17, rs.getString("ISDUNIT"));
                    ps.setString(18, rs.getString("ISDBUNIT"));
                    ps.setString(19, rs.getString("ISDCON1"));
                    ps.setString(20, rs.getString("ISDCON2"));
                    ps.setString(21, rs.getString("ISDPRICE"));
                    ps.setString(22, rs.getString("ISDNETV"));
                    ps.setString(23, rs.getString("ISDCUR"));
                    ps.setString(24, rs.getString("ISDEXC"));
                    ps.setString(25, rs.getString("ISDUNR01"));
                    ps.setString(26, rs.getString("ISDUNR03"));
                    ps.setString(27, rs.getString("ISDMTCTRL"));
                    ps.setString(28, rs.getString("ISDPFCT"));
                    ps.setString(29, rs.getString("ISDACGP"));
                    ps.setString(30, rs.getString("ISDODT"));
                    ps.setString(31, rs.getString("ISDUDT"));
                    ps.setString(32, rs.getString("ISDRQDT"));
                    ps.setString(33, rs.getString("ISDDLS"));
                    ps.setString(34, rs.getString("ISDODLS"));
                    ps.setString(35, rs.getString("ISDCRDT"));
                    ps.setString(36, rs.getString("ISDCRTM"));
                    ps.setString(37, rs.getString("ISDSUSER"));
                    ps.setString(38, rs.getString("ISDDEST"));
                    ps.setString(39, rs.getString("ISDREMK"));
                    ps.setString(40, rs.getString("ISDEDT"));
                    ps.setString(41, rs.getString("ISDETM"));
                    ps.setString(42, rs.getString("ISDEUSR"));
                    ps.setString(43, rs.getString("ISDCDT"));
                    ps.setString(44, rs.getString("ISDCTM"));
                    ps.setString(45, rs.getString("ISDUSER"));

                    int record = ps.executeUpdate();

                    if (record >= 1) {
                        result = true;
                    } else {
                        result = false;
                    }

                    ps.close();

                }

            }

            stmt.close();
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

//    public boolean addISMMASDBySize(List<ISMSDWD> invDetailList, int sizedown, List<ISMMASD> LineMas, String user, String seqin) {
////        String order, String seq, String lino, String sino, String dito, String plant, String vt, String strg, String qty, String unit, String price, String unr1
////, String unr3, String matctrl, String dest, String user, String sts
//
//        boolean result = false;
//
//        String sql = "INSERT INTO [ISMMASD] (ISDCONO,ISDORD,ISDSEQ,ISDLINO,ISDSINO,ISDITNO,ISDPLANT,ISDVT,ISDSTRG,ISDRQQTY,ISDUNIT,ISDPRICE,ISDUNR01,ISDUNR03\n"
//                + ",ISDMTCTRL,ISDDEST,ISDRQDT,ISDRQTM,ISDAPDT,ISDAPTM,ISDAPUSER,ISDRCDT,ISDRCTM,ISDRCUSER,ISDPKQTY,ISDPKDT,ISDPKTM,ISDPKUSER,ISDTPQTY,ISDTPDT\n"
//                + ",ISDTPTM,ISDTPUSER,ISDSTS,ISDJBNO,ISDQNO,ISDREMARK,ISDISUNO,ISDTEMP,ISDEDT,ISDETM,ISDEUSR,ISDCDT,ISDCTM,ISDUSER,ISDSTAD,ISDSTSTMP) VALUES \n";
////                + "'TWC',?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,NULL,NULL,NULL,NULL,NULL,NULL\n"
////                + ",NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,?,NULL,NULL,NULL,0,NULL"
////                + ",FORMAT(CURRENT_TIMESTAMP,'yyyyMMdd'),FORMAT(CURRENT_TIMESTAMP,'HHmmss'),?,FORMAT(CURRENT_TIMESTAMP,'yyyyMMdd'),FORMAT(CURRENT_TIMESTAMP,'HHmmss'),?,'1',NULL\n"
////                + ")";
//
//        for (int i = LineMas.size(); i < invDetailList.size(); i++) {
//            String order = checkNull(invDetailList.get(i).getISDORD());
//            String seq = seqin;
//            String lino = checkNull(invDetailList.get(i).getISDLINO());
//            String sino = checkNull(invDetailList.get(i).getISDSINO());
//            String dito = checkNull(invDetailList.get(i).getISDITNO());
//            String plant = checkNull(invDetailList.get(i).getISDPLANT());
//            String vt = checkNull(invDetailList.get(i).getISDVT());
//            String strg = checkNull(invDetailList.get(i).getISDSTRG());
//            String qty = invDetailList.get(i).getISDRQQTY();
//            String unit = checkNull(invDetailList.get(i).getISDUNIT());
//            String price = invDetailList.get(i).getISDPRICE();
//            String unr1 = invDetailList.get(i).getISDUNR01();
//            String unr3 = invDetailList.get(i).getISDUNR03();
//            String matctrl = checkNull(invDetailList.get(i).getISDMTCTRL());
//            String dest = checkNull(invDetailList.get(i).getISDDEST());
//            String sts = checkNull(invDetailList.get(i).getISDSTS());
//            String remrk = checkNull(invDetailList.get(i).getISDREMK());
//
//            if (qty == null) {
//                qty = "NULL";
//            } else {
//                if (qty.equals("")) {
//                    qty = "NULL";
//                }
//            }
//
//            if (price == null) {
//                price = "NULL";
//            } else {
//                if (price.equals("")) {
//                    price = "NULL";
//                }
//            }
//
//            if (unr1 == null) {
//                unr1 = "NULL";
//            } else {
//                if (unr1.equals("")) {
//                    unr1 = "NULL";
//                }
//            }
//
//            if (unr3 == null) {
//                unr3 = "NULL";
//            } else {
//                if (unr3.equals("")) {
//                    unr3 = "NULL";
//                }
//            }
//
//            sql += "('TWC'," + order + "," + seq + "," + lino + "," + sino + "," + dito + "," + plant + "," + vt + "," + strg + "," + qty + "," + unit + "," + price + "," + unr1 + "," + unr3 + "," + matctrl + "," + dest + ",NULL,NULL,NULL,NULL,NULL,NULL\n"
//                    + ",NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL," + sts + ",NULL,NULL," + remrk + ",0,NULL"
//                    + ",FORMAT(CURRENT_TIMESTAMP,'yyyyMMdd'),FORMAT(CURRENT_TIMESTAMP,'HHmmss')," + user + ",FORMAT(CURRENT_TIMESTAMP,'yyyyMMdd'),FORMAT(CURRENT_TIMESTAMP,'HHmmss')," + user + ",'1',NULL\n"
//                    + ")";
//
//            if (i != (invDetailList.size() - 1)) {
//                sql += ",";
//            }
//        }
//
////        String sql2  = "INSERT INTO [ISMMASD] (ISDCONO,ISDORD,ISDSEQ,ISDLINO,ISDSINO,ISDITNO,ISDPLANT,ISDVT,ISDSTRG,ISDRQQTY,ISDUNIT,ISDPRICE,ISDUNR01,ISDUNR03\n"
////                + ",ISDMTCTRL,ISDDEST,ISDRQDT,ISDRQTM,ISDAPDT,ISDAPTM,ISDAPUSER,ISDRCDT,ISDRCTM,ISDRCUSER,ISDPKQTY,ISDPKDT,ISDPKTM,ISDPKUSER,ISDTPQTY,ISDTPDT\n"
////                + ",ISDTPTM,ISDTPUSER,ISDSTS,ISDJBNO,ISDQNO,ISDREMARK,ISDISUNO,ISDTEMP,ISDEDT,ISDETM,ISDEUSR,ISDCDT,ISDCTM,ISDUSER,ISDSTAD,ISDSTSTMP) VALUES (\n"
////                + "'TWC',"+order+","+seq+","+lino+","+sino+","+dito+","+plant+","+vt+","+strg+","+qty+","+unit+","+price+","+unr1+","+unr3+","+matctrl+","+dest+",NULL,NULL,NULL,NULL,NULL,NULL\n"
////                + ",NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,"+sts+",NULL,NULL,NULL,1,NULL"
////                + ",FORMAT(CURRENT_TIMESTAMP,'yyyyMMdd'),FORMAT(CURRENT_TIMESTAMP,'HHmmss'),"+user+",FORMAT(CURRENT_TIMESTAMP,'yyyyMMdd'),FORMAT(CURRENT_TIMESTAMP,'HHmmss'),"+user+",'1',NULL\n"
////                + ")";
//        try {
//
//            System.out.println(sql);
//            PreparedStatement ps = connect.prepareStatement(sql);
//
////            ps.setString(1, order);
////            ps.setString(2, seq);
////            ps.setString(3, lino);
////            ps.setString(4, sino);
////            ps.setString(5, dito);
////            ps.setString(6, plant);
////            ps.setString(7, vt);
////            ps.setString(8, strg);
////            ps.setString(9, qty);
////            ps.setString(10, unit);
////            ps.setString(11, price);
////            ps.setString(12, unr1);
////            ps.setString(13, unr3);
////            ps.setString(14, matctrl);
////            ps.setString(15, dest);
////            ps.setString(16, sts);
////            ps.setString(17, user);
////            ps.setString(18, user);
//            int record = ps.executeUpdate();
//
//            if (record >= 1) {
//
//                result = true;
//
//            } else {
//
//                result = false;
//
//            }
//
//            ps.close();
//            connect.close();
//
//        } catch (Exception e) {
//
//            e.printStackTrace();
//
//        }
////
//        return result;
//
//    }
    public boolean deleteISMMASH(String orderNo) {

        boolean result = false;

        String sql = "DELETE [ISMMASH] WHERE ISHORD = '" + orderNo + "'";
        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            ps.close();
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean deleteISMMASHSTS0(String orderNo, String line) {

        boolean result = false;

        String sql = "DELETE [ISMMASH] WHERE ISHORD = '" + orderNo + "' AND ISH AND ISHHSTS = '0' ";
        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            ps.close();
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean deleteISMMASHbySEQ(String orderNo, String seq) {

        boolean result = false;

        String sql = "DELETE [ISMMASH] WHERE ISHORD = '" + orderNo + "' AND ISHSEQ = '" + seq + "'";
        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            ps.close();
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean deleteISMMASHbySEQSts1(String orderNo, String seq) {

        boolean result = false;

        String sql = "DELETE [ISMMASH] WHERE ISHORD = '" + orderNo + "' AND ISHSEQ = '" + seq + "' AND ISHLSTS = '0' ";
        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            ps.close();
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean deleteISMMASD(String orderNo) {

        boolean result = false;

        String sql = "DELETE [ISMMASD] WHERE ISDORD = '" + orderNo + "'";
        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            ps.close();
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean deleteISMMASDSTS0(String orderNo, String line) {

        boolean result = false;

        String sql = "DELETE [ISMMASD] WHERE ISDORD = '" + orderNo + "' AND ISDLINO = '" + line + "' AND ISDSTS = '0' ";
        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            ps.close();
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean deleteISMMASDBySEQnLINE(String orderNo, String seq, String line) {

        boolean result = false;

        String sql = "DELETE [ISMMASD] WHERE ISDORD = '" + orderNo + "' AND ISDLINO = '" + line + "'";
//        String sql = "DELETE [ISMMASD] WHERE ISDORD = '" + orderNo + "' AND ISDSEQ = '" + seq + "' AND ISDLINO = '" + line + "'";
        try {
//            System.out.println("");
            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            ps.close();
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public JSONObject deleteDownloadS(String orderNo) {

        JSONObject res = new JSONObject();

        //DELETE MATCODE ISMSDWS IN SELECT ALL DISTINCT MATCODE
        String sqlSelMatCode = "DELETE [RMShipment].[dbo].[ISMSDWS] WHERE ISSMTNO COLLATE SQL_Latin1_General_CP1_CI_AS IN (SELECT DISTINCT ISDITNO COLLATE SQL_Latin1_General_CP1_CI_AS FROM [RMShipment].[dbo].[ISMSDWD] WHERE ISDORD = '" + orderNo + "')";

//        String sqlGetDWNNo = "SELECT DISTINCT [ISHDWQNO] FROM [RMShipment].[dbo].[ISMSDWH] WHERE [ISHORD] = '" + orderNo + "'";
//        ISMSDWH obj = new ISMSDWH();
//        System.out.println(sqlSelMatCode);
        try {
            Statement stmtS = connect.createStatement();
            Statement stmtIUD = connect.createStatement();

            int recDelS = stmtIUD.executeUpdate(sqlSelMatCode);

            if (recDelS > 0) {
                res.put("recDelS", true);
            } else {
                res.put("recDelS", false);
            }

//            ResultSet rs = stmtS.executeQuery(sqlGetDWNNo);
//            while (rs.next()) {
//                obj.setISHDWQNO(rs.getString("ISHDWQNO"));
//            }
//            String sqlDelDWN = "DELETE ISMSDWH WHERE ISHDWQNO = " + obj.getISHDWQNO() + "\n"
//                    + "DELETE ISMSDWD WHERE ISDDWQNO = " + obj.getISHDWQNO() + "";
//            int recDelD = stmtIUD.executeUpdate(sqlDelDWN);
//            if (recDelD > 0) {
//                res.put("recDelD", true);
//            } else {
//                res.put("recDelD", false);
//            }
//            
            stmtS.close();
            stmtIUD.close();
            connect.close();

        } catch (SQLException ex) {
            Logger.getLogger(ISM800Dao.class.getName()).log(Level.SEVERE, null, ex);
        }

        return res;

    }

    public JSONObject deleteDownload(String dwnno) {

        String sqlGetISHORDbyDWNNo = "SELECT DISTINCT [ISHORD],[ISHSEQ] FROM [RMShipment].[dbo].[ISMSDWH] WHERE [ISHDWQNO] = '" + dwnno + "'";

        JSONObject resout = new JSONObject();

        try {
            Statement stmtS = connect.createStatement();
            Statement stmtIUD = connect.createStatement();

            ResultSet rs = stmtS.executeQuery(sqlGetISHORDbyDWNNo);

            while (rs.next()) {

                JSONArray ja = new JSONArray();

                JSONObject res = new JSONObject();

                //DELETE MATCODE ISMSDWS IN SELECT ALL DISTINCT MATCODE
                String sqlSelMatCode = "DELETE [RMShipment].[dbo].[ISMSDWS] WHERE ISSMTNO COLLATE SQL_Latin1_General_CP1_CI_AS IN (SELECT DISTINCT ISDITNO COLLATE SQL_Latin1_General_CP1_CI_AS FROM [RMShipment].[dbo].[ISMSDWD] WHERE ISDORD = '" + rs.getString("ISHORD") + "' AND ISDSEQ = '" + rs.getString("ISHSEQ") + "' AND ISDDWQNO = '" + dwnno + "' )";
                int recDelS = stmtIUD.executeUpdate(sqlSelMatCode);

                if (recDelS > 0) {
                    res.put("recDelS" + rs.getString("ISHORD"), true);
                } else {
                    res.put("recDelS" + rs.getString("ISHORD"), false);
                }

                String sqlDelDWN = "DELETE ISMSDWH WHERE ISHORD = '" + rs.getString("ISHORD") + "' AND ISHSEQ = '" + rs.getString("ISHSEQ") + "' AND ISHDWQNO = '" + dwnno + "' \n"
                        + "DELETE ISMSDWD WHERE ISDORD = '" + rs.getString("ISHORD") + "' AND ISDSEQ = '" + rs.getString("ISHSEQ") + "' AND ISDDWQNO = '" + dwnno + "' ";

                int recDelD = stmtIUD.executeUpdate(sqlDelDWN);
                if (recDelD > 0) {
                    res.put("recDelD" + rs.getString("ISHORD"), true);
                } else {
                    res.put("recDelD" + rs.getString("ISHORD"), false);
                }

                ja.put(res);

                resout.put(rs.getString("ISHORD"), ja);

            }

            stmtS.close();
            stmtIUD.close();
            connect.close();

        } catch (SQLException ex) {
            Logger.getLogger(ISM800Dao.class.getName()).log(Level.SEVERE, null, ex);
        }

//        System.out.println(resout);
        return resout;

    }

    public JSONObject CKDWHbyORD(String orderNo) {

        JSONObject res = new JSONObject();

        boolean val = false;

        String sqlGetDWNNo = "SELECT TOP 1 * FROM [RMShipment].[dbo].[ISMSDWH] WHERE [ISHORD] = '" + orderNo + "'";

        try {
            Statement stmtS = connect.createStatement();

            ResultSet rs = stmtS.executeQuery(sqlGetDWNNo);

            while (rs.next()) {
                val = true;
            }

            res.put("DTDwnHead", val);

            stmtS.close();
            connect.close();

        } catch (SQLException ex) {
            Logger.getLogger(ISM800Dao.class.getName()).log(Level.SEVERE, null, ex);
        }

        return res;

    }

    public JSONObject CKDWDbyORD(String orderNo) {

        JSONObject res = new JSONObject();

        boolean val = false;

        String sqlGetDWNNo = "SELECT TOP 1 * FROM [RMShipment].[dbo].[ISMSDWD] WHERE [ISDORD] = '" + orderNo + "'";

        try {
            Statement stmtS = connect.createStatement();

            ResultSet rs = stmtS.executeQuery(sqlGetDWNNo);

            while (rs.next()) {
                val = true;
            }

            res.put("DTDwnDetail", val);

            stmtS.close();
            connect.close();

        } catch (SQLException ex) {
            Logger.getLogger(ISM800Dao.class.getName()).log(Level.SEVERE, null, ex);
        }

        return res;

    }

    public List<ISMSDWD> findEditDetailBy(String no, String date) {

        List<ISMSDWD> UAList = new ArrayList<ISMSDWD>();

        String sql = "SELECT DWH.ISHDWQNO,ISNULL(RIGHT(DWH.[ISHTRDT],2)+'/'+LEFT(RIGHT(DWH.[ISHTRDT],4),2)+'/'+LEFT(DWH.[ISHTRDT],4),'') AS SLDATE,DWH.ISHORD,DWH.ISHPGRP,DWH.ISHSUSER\n"
                + ",ISNULL(DWH.ISHSTYLE,'') AS ISHSTYLE,ISNULL(DWH.ISHCOLOR,'') AS ISHCOLOR,ISNULL(DWH.ISHLOT,'') AS ISHLOT,ISNULL(DWH.ISHREASON,'') AS ISHREASON\n"
                + ",MAH.[ISHORD] AS MASISHORD\n"
                + "FROM ISMSDWH DWH\n"
                + "LEFT JOIN ISMMASH MAH ON DWH.ISHORD = MAH.ISHORD\n"
                + "WHERE DWH.ISHDWQNO = '" + no + "' AND DWH.ISHSEQ = (SELECT TOP 1 ISHSEQ FROM ISMSDWH WHERE ISHTRDT = '" + date + "' AND ISHDWQNO = '" + no + "')  ";
//        String sql = "SELECT ISHDWQNO,ISNULL(RIGHT([ISHTRDT],2)+'/'+LEFT(RIGHT([ISHTRDT],4),2)+'/'+LEFT([ISHTRDT],4),'') AS SLDATE,ISHORD,ISHPGRP,ISHSUSER\n"
//                + ",ISNULL(ISHSTYLE,'') AS ISHSTYLE,ISNULL(ISHCOLOR,'') AS ISHCOLOR,ISNULL(ISHLOT,'') AS ISHLOT,ISNULL(ISHREASON,'') AS ISHREASON\n"
//                + "FROM ISMSDWH WHERE ISHDWQNO = '" + no + "' AND ISHSEQ = (SELECT TOP 1 ISHSEQ FROM ISMSDWH WHERE ISHTRDT = '" + date + "' AND ISHDWQNO = '" + no + "') ";
//        String sql = "SELECT ISNULL(DW.ISDDWQNO,'') AS ISDDWQNO\n"
//                + "			,(SELECT TOP 1 isnull(right([ISHTRDT],2)+'/'+left(right([ISHTRDT],4),2)+'/'+left([ISHTRDT],4),'') FROM ISMSDWH WHERE ISHORD = DW.ISDORD AND ISHTRDT = '" + date + "' ) AS SLDATE\n"
//                + "			,ISNULL(DW.ISDORD,'') AS ISDORD\n"
//                + "			,ISNULL(LEFT(DW.[ISDITNO],2),'') AS ISDITNO\n"
//                + "			,(SELECT (CASE WHEN F1.ISDSTS IS NULL THEN 'S' ELSE (CASE WHEN F1.ISDSTS != 0 THEN 'N' ELSE 'C' END) END) AS DIST FROM (\n"
//                + "				SELECT TOP 1 DE.ISDSTS\n"
//                + "				FROM ISMSDWD DW2\n"
//                + "				LEFT JOIN ISMMASD DE ON DE.ISDORD = DW2.ISDORD AND DE.ISDLINO = DW2.ISDLINO \n"
//                + "				WHERE DW2.ISDORD = DW.ISDORD AND DW2.ISDSEQ = (SELECT TOP 1 ISHSEQ FROM ISMSDWH WHERE ISHTRDT = '" + date + "' AND ISHDWQNO = '" + no + "' ) AND DW2.ISDDWQNO = '" + no + "' ORDER BY DE.ISDSTS ASC\n"
//                + "			) F1) AS ISDSTS ,ISDSUSER\n"
//                + "			,ISNULL(HD.ISHSTYLE,'') AS ISHSTYLE,ISNULL(HD.ISHCOLOR,'') AS ISHCOLOR,ISNULL(HD.ISHLOT,'') AS ISHLOT"
//                + "                     ,ISNULL(HD.ISHREASON,'') AS ISHREASON\n"
//                + "	FROM ISMSDWD DW\n"
//                + "	LEFT JOIN ISMMASD DE ON DE.ISDORD = DW.ISDORD AND DE.ISDLINO = DW.ISDLINO\n"
//                + "	LEFT JOIN ISMSDWH HD ON HD.ISHORD = DW.ISDORD\n"
//                + "	WHERE ISDDWQNO = '" + no + "' AND DW.ISDSEQ = (SELECT TOP 1 ISHSEQ FROM ISMSDWH WHERE ISHTRDT = '" + date + "' AND ISHDWQNO = '" + no + "' ) \n"
//                + "	GROUP BY DW.[ISDDWQNO],DW.[ISDORD],LEFT(DW.[ISDITNO],2),ISDSUSER,HD.ISHSTYLE,HD.ISHCOLOR,HD.ISHLOT,HD.ISHREASON\n"
//                + "";

        try {
//            System.out.println("sql ed " + sql);
            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                ISMSDWD p = new ISMSDWD();

                p.setISDDWQNO(result.getString("ISHDWQNO"));
                p.setISDLINO(result.getString("ISHSUSER"));
                p.setDate(result.getString("SLDATE"));
                p.setISDORD(result.getString("ISHORD"));
                p.setISDITNO(result.getString("ISHPGRP"));
//                p.setISDITNO(result.getString("ISDITNO").substring(0, 2));
//                p.setISDVT(result.getString("ISDSTS")); //status
                p.setISDREMK(result.getString("ISHREASON"));
                p.setSTYLE(result.getString("ISHSTYLE"));
                p.setCOLOR(result.getString("ISHCOLOR"));
                p.setLOT(result.getString("ISHLOT"));
                p.setISDBAT(result.getString("MASISHORD"));
//                p.setISDDWQNO(result.getString("ISDDWQNO"));
//                p.setISDLINO(result.getString("ISDSUSER"));
//                p.setDate(result.getString("SLDATE"));
//                p.setISDORD(result.getString("ISDORD"));
//                p.setISDITNO(result.getString("ISDITNO"));
////                p.setISDITNO(result.getString("ISDITNO").substring(0, 2));
//                p.setISDVT(result.getString("ISDSTS")); //status
//                p.setISDREMK(result.getString("ISHREASON"));
//                p.setSTYLE(result.getString("ISHSTYLE"));
//                p.setCOLOR(result.getString("ISHCOLOR"));
//                p.setLOT(result.getString("ISHLOT"));

                UAList.add(p);

            }

            ps.close();
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public String SelectNo(String no) {
        String max = "";

        String sql = "SELECT ISNULL([ISHDWQNO],0) AS ISHDWQNO FROM [RMShipment].[dbo].[ISMSDWH] WHERE [ISHORD] = '" + no + "'";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                max = result.getString("ISHDWQNO");

            }

            ps.close();
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return max;

    }

    public String GetORDByDWNno(String no) {
        String max = "";

        String sql = "SELECT TOP 1 ISHORD FROM [RMShipment].[dbo].[ISMSDWH] WHERE ISHDWQNO = '" + no + "'";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                max = result.getString("ISHORD");

            }

            ps.close();
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return max;

    }

    public String SelectMax() {
        String max = "";

        String sql = "SELECT (ISNULL(MAX(ISHDWQNO),0) + 1) AS MAXXY  FROM ISMSDWH";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                max = result.getString("MAXXY");

            }

            ps.close();
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return max;

    }

    public boolean CheckDWDbyORDnLine(String ord, String line) {
        boolean res = true;

        String sql = "SELECT * FROM ISMSDWD WHERE ISDORD = '" + ord + "' AND ISDLINO = '" + line + "'";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);
            ResultSet result = ps.executeQuery();

            while (result.next()) {
                res = false;
            }

            ps.close();
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return res;

    }

//    public String SelectLSTS(String cusno, String trdtf, String trdtt, String ordf, String ordt, String prgp) {
//        String sts = "";
//
//        String sql = "SELECT ISHHSTS FROM ISMMASH WHERE ISHCUNO = '0000" + cusno + "'\n"
//                //                + "AND ISHTRDT BETWEEN '" + trdtf.replaceAll("-", "") + "' AND '" + trdtt.replaceAll("-", "") + "'\n"
//                + "AND ISHORD BETWEEN '" + ordf + "' AND '" + ordt + "'\n"
//                + "AND ISHPGRP = '" + prgp + "'";
////        System.out.println(sql);
//
//        try {
//
//            PreparedStatement ps = connect.prepareStatement(sql);
//
//            ResultSet result = ps.executeQuery();
//
//            while (result.next()) {
//
//                sts = result.getString("ISHHSTS");
//
//            }
//
//            ps.close();
//            connect.close();
//
//        } catch (Exception e) {
//
//            e.printStackTrace();
//
//        }
//
//        return sts;
//
//    }
//    public String SelectMAXSEQ(String cusno, String trdtf, String trdtt, String ordf, String ordt, String prgp) {
//        String sts = "";
//
//        String sql = "SELECT MAX(ISHSEQ) + 1 AS SEQ FROM ISMMASH WHERE ISHCUNO = '0000" + cusno + "'\n"
//                //                + "AND ISHTRDT BETWEEN '" + trdtf.replaceAll("-", "") + "' AND '" + trdtt.replaceAll("-", "") + "'\n"
//                + "AND ISHORD BETWEEN '" + ordf + "' AND '" + ordt + "'\n"
//                + "AND ISHPGRP = '" + prgp + "'";
////        System.out.println("max seq : " + sql);
//
//        try {
//
//            PreparedStatement ps = connect.prepareStatement(sql);
//
//            ResultSet result = ps.executeQuery();
//
//            while (result.next()) {
//
//                sts = result.getString("SEQ");
//
//            }
//
//            ps.close();
//            connect.close();
//
//        } catch (Exception e) {
//
//            e.printStackTrace();
//
//        }
//
//        return sts;
//
//    }
//    public String SelectSEQ(String cusno, String trdtf, String trdtt, String ordf, String ordt, String prgp) {
//        String sts = "";
//
//        String sqll = "SELECT * FROM [RMShipment].[dbo].[ISMSDWH] WHERE ISHORD = '1420054631'";
//
//        String sql = "SELECT MAX(ISHSEQ) + 1 AS SEQ FROM ISMMASH WHERE ISHCUNO = '0000" + cusno + "'\n"
//                //                + "AND ISHTRDT BETWEEN '" + trdtf.replaceAll("-", "") + "' AND '" + trdtt.replaceAll("-", "") + "'\n"
//                + "AND ISHORD BETWEEN '" + ordf + "' AND '" + ordt + "'\n"
//                + "AND ISHPGRP = '" + prgp + "'";
////        System.out.println("max seq : " + sql);
//
//        try {
//
//            PreparedStatement ps = connect.prepareStatement(sql);
//
//            ResultSet result = ps.executeQuery();
//
//            while (result.next()) {
//
//                sts = result.getString("SEQ");
//
//            }
//
//            ps.close();
//            connect.close();
//
//        } catch (Exception e) {
//
//            e.printStackTrace();
//
//        }
//
//        return sts;
//
//    }
    public boolean deleteDownloadLine(String no, String line) {

        boolean result = false;

        String sql = "DELETE ISMSDWD WHERE ISDDWQNO = '" + no + "' AND ISDLINO = '" + line + "'";

//        System.out.println("" + sql);
        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            ps.close();
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public List<ISMMASD> GetMAS(String no) {

        List<ISMMASD> UAList = new ArrayList<ISMMASD>();

        String sql = "SELECT * FROM ISMMASD WHERE ISDORD = (SELECT TOP 1 ISDORD FROM ISMSDWD WHERE ISDDWQNO = '" + no + "')";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                ISMMASD p = new ISMMASD();

                p.setISDCONO(result.getString("ISDCONO"));
                p.setISDORD(result.getString("ISDORD"));
                p.setISDSEQ(result.getString("ISDSEQ"));
                p.setISDLINO(result.getString("ISDLINO"));
                p.setISDSINO(result.getString("ISDSINO"));
                p.setISDITNO(result.getString("ISDITNO"));
                p.setISDPLANT(result.getString("ISDPLANT"));
                p.setISDVT(result.getString("ISDVT"));
                p.setISDSTRG(result.getString("ISDSTRG"));
                p.setISDRQQTY(result.getString("ISDRQQTY"));
                p.setISDUNIT(result.getString("ISDUNIT"));
                p.setISDPRICE(result.getString("ISDPRICE"));
                p.setISDUNR01(result.getString("ISDUNR01"));
                p.setISDUNR03(result.getString("ISDUNR03"));
                p.setISDMTCTRL(result.getString("ISDMTCTRL"));
                p.setISDDEST(result.getString("ISDDEST"));
                p.setISDRQDT(result.getString("ISDRQDT"));
                p.setISDRQTM(result.getString("ISDRQTM"));
                p.setISDAPDT(result.getString("ISDAPDT"));
                p.setISDAPTM(result.getString("ISDAPTM"));
                p.setISDAPUSER(result.getString("ISDAPUSER"));
                p.setISDRCDT(result.getString("ISDRCDT"));
                p.setISDPKQTY(result.getString("ISDPKQTY"));
                p.setISDPKQTY(result.getString("ISDPKQTY"));
                p.setISDPKDT(result.getString("ISDPKDT"));
                p.setISDPKTM(result.getString("ISDPKTM"));
                p.setISDPKUSER(result.getString("ISDPKUSER"));
                p.setISDTPQTY(result.getString("ISDTPQTY"));
                p.setISDTPDT(result.getString("ISDTPDT"));
                p.setISDTPTM(result.getString("ISDTPTM"));
                p.setISDTPUSER(result.getString("ISDTPUSER"));
                p.setISDSTS(result.getString("ISDSTS"));
                p.setISDJBNO(result.getString("ISDJBNO"));
                p.setISDQNO(result.getString("ISDQNO"));
                p.setISDREMARK(result.getString("ISDREMARK"));
                p.setISDISUNO(result.getString("ISDISUNO"));
                p.setISDTEMP(result.getString("ISDTEMP"));
                p.setISDEDT(result.getString("ISDEDT"));
                p.setISDETM(result.getString("ISDETM"));
                p.setISDEUSR(result.getString("ISDEUSR"));
                p.setISDCDT(result.getString("ISDCDT"));
                p.setISDCTM(result.getString("ISDCTM"));
                p.setISDUSER(result.getString("ISDUSER"));
                p.setISDSTAD(result.getString("ISDSTAD"));
                p.setISDSTSTMP(result.getString("ISDSTSTMP"));
                p.setISDREMAIN(result.getString("ISDREMAIN"));

                UAList.add(p);

            }

            ps.close();
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

//    public List<ISMMASD> GetMASCreate(String cusno, String trdtf, String trdtt, String ordf, String ordt, String prgp) {
//
//        List<ISMMASD> UAList = new ArrayList<ISMMASD>();
//
//        String ord = "";
//        String grp = "";
//
//        if (!ordf.equals("") && !ordt.equals("")) {
//            ord = "AND ISHORD BETWEEN '" + ordf + "' AND '" + ordt + "'\n";
//        }
//
////        System.out.println(ordf.equals(""));
//        if (!prgp.equals("")) {
//            grp = "AND ISHPGRP = '" + prgp + "'";
//        }
//
//        String sql = "SELECT * FROM ISMMASD WHERE ISDORD = (SELECT TOP 1 ISHORD\n"
//                + "FROM [RMShipment].[dbo].[ISMMASH]\n"
//                + "WHERE ISHCUNO = '0000" + cusno + "'\n"
//                //                + "AND ISHTRDT BETWEEN '" + trdtf.replaceAll("-", "") + "' AND '" + trdtt.replaceAll("-", "") + "'\n"
//                + ord + grp + ")";
////        System.out.println(sql);
//
//        try {
//
//            PreparedStatement ps = connect.prepareStatement(sql);
//
//            ResultSet result = ps.executeQuery();
//
//            while (result.next()) {
//
//                ISMMASD p = new ISMMASD();
//
//                p.setISDCONO(result.getString("ISDCONO"));
//                p.setISDORD(result.getString("ISDORD"));
//                p.setISDSEQ(result.getString("ISDSEQ"));
//                p.setISDLINO(result.getString("ISDLINO"));
//                p.setISDSINO(result.getString("ISDSINO"));
//                p.setISDITNO(result.getString("ISDITNO"));
//                p.setISDPLANT(result.getString("ISDPLANT"));
//                p.setISDVT(result.getString("ISDVT"));
//                p.setISDSTRG(result.getString("ISDSTRG"));
//                p.setISDRQQTY(result.getString("ISDRQQTY"));
//                p.setISDUNIT(result.getString("ISDUNIT"));
//                p.setISDPRICE(result.getString("ISDPRICE"));
//                p.setISDUNR01(result.getString("ISDUNR01"));
//                p.setISDUNR03(result.getString("ISDUNR03"));
//                p.setISDMTCTRL(result.getString("ISDMTCTRL"));
//                p.setISDDEST(result.getString("ISDDEST"));
//                p.setISDRQDT(result.getString("ISDRQDT"));
//                p.setISDRQTM(result.getString("ISDRQTM"));
//                p.setISDAPDT(result.getString("ISDAPDT"));
//                p.setISDAPTM(result.getString("ISDAPTM"));
//                p.setISDAPUSER(result.getString("ISDAPUSER"));
//                p.setISDRCDT(result.getString("ISDRCDT"));
//                p.setISDPKQTY(result.getString("ISDPKQTY"));
//                p.setISDPKQTY(result.getString("ISDPKQTY"));
//                p.setISDPKDT(result.getString("ISDPKDT"));
//                p.setISDPKTM(result.getString("ISDPKTM"));
//                p.setISDPKUSER(result.getString("ISDPKUSER"));
//                p.setISDTPQTY(result.getString("ISDTPQTY"));
//                p.setISDTPDT(result.getString("ISDTPDT"));
//                p.setISDTPTM(result.getString("ISDTPTM"));
//                p.setISDTPUSER(result.getString("ISDTPUSER"));
//                p.setISDSTS(result.getString("ISDSTS"));
//                p.setISDJBNO(result.getString("ISDJBNO"));
//                p.setISDQNO(result.getString("ISDQNO"));
//                p.setISDREMARK(result.getString("ISDREMARK"));
//                p.setISDISUNO(result.getString("ISDISUNO"));
//                p.setISDTEMP(result.getString("ISDTEMP"));
//                p.setISDEDT(result.getString("ISDEDT"));
//                p.setISDETM(result.getString("ISDETM"));
//                p.setISDEUSR(result.getString("ISDEUSR"));
//                p.setISDCDT(result.getString("ISDCDT"));
//                p.setISDCTM(result.getString("ISDCTM"));
//                p.setISDUSER(result.getString("ISDUSER"));
//                p.setISDSTAD(result.getString("ISDSTAD"));
//                p.setISDSTSTMP(result.getString("ISDSTSTMP"));
//                p.setISDREMAIN(result.getString("ISDREMAIN"));
//
//                UAList.add(p);
//
//            }
//
//            ps.close();
//            connect.close();
//
//        } catch (Exception e) {
//
//            e.printStackTrace();
//
//        }
//
//        return UAList;
//
//    }
    public List<ISMSDWD> GetDWN(String no, String seq) {

        List<ISMSDWD> UAList = new ArrayList<ISMSDWD>();

        String sql = "SELECT * FROM ISMSDWD WHERE ISDDWQNO = '" + no + "' AND ISDSEQ = '" + seq + "' ";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                ISMSDWD p = new ISMSDWD();

                p.setISDCONO(result.getString("ISDCONO"));
                p.setISDDWQNO(result.getString("ISDDWQNO"));
                p.setISDORD(result.getString("ISDORD"));
                p.setISDSEQ(result.getString("ISDSEQ"));
                p.setISDLINO(result.getString("ISDLINO"));
                p.setISDITNO(result.getString("ISDITNO"));
                p.setISDPLANT(result.getString("ISDPLANT"));
                p.setISDVT(result.getString("ISDVT"));
                p.setISDSTRG(result.getString("ISDSTRG"));
                p.setISDRQQTY(result.getString("ISDRQQTY"));
                p.setISDUNIT(result.getString("ISDUNIT"));
                p.setISDPRICE(result.getString("ISDPRICE"));
                p.setISDUNR01(result.getString("ISDUNR01"));
                p.setISDUNR03(result.getString("ISDUNR03"));
                p.setISDMTCTRL(result.getString("ISDMTCTRL"));
                p.setISDDEST(result.getString("ISDDEST"));
                p.setISDREMK(result.getString("ISDREMK"));

                UAList.add(p);

            }

            ps.close();
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }
//    public List<ISMSDWD> GetDWNByORD(String ord) {
//
//        List<ISMSDWD> UAList = new ArrayList<ISMSDWD>();
//
//        String sql = "SELECT * FROM ISMSDWD WHERE ISDORD = '" + ord + "'";
////        System.out.println(sql);
//
//        try {
//
//            PreparedStatement ps = connect.prepareStatement(sql);
//
//            ResultSet result = ps.executeQuery();
//
//            while (result.next()) {
//
//                ISMSDWD p = new ISMSDWD();
//
//                p.setISDCONO(result.getString("ISDCONO"));
//                p.setISDDWQNO(result.getString("ISDDWQNO"));
//                p.setISDORD(result.getString("ISDORD"));
//                p.setISDSEQ(result.getString("ISDSEQ"));
//                p.setISDLINO(result.getString("ISDLINO"));
//                p.setISDITNO(result.getString("ISDITNO"));
//                p.setISDPLANT(result.getString("ISDPLANT"));
//                p.setISDVT(result.getString("ISDVT"));
//                p.setISDSTRG(result.getString("ISDSTRG"));
//                p.setISDRQQTY(result.getString("ISDRQQTY"));
//                p.setISDUNIT(result.getString("ISDUNIT"));
//                p.setISDPRICE(result.getString("ISDPRICE"));
//                p.setISDUNR01(result.getString("ISDUNR01"));
//                p.setISDUNR03(result.getString("ISDUNR03"));
//                p.setISDMTCTRL(result.getString("ISDMTCTRL"));
//                p.setISDDEST(result.getString("ISDDEST"));
//                p.setISDREMK(result.getString("ISDREMK"));
//
//                UAList.add(p);
//
//            }
//
//            ps.close();
//            connect.close();
//
//        } catch (Exception e) {
//
//            e.printStackTrace();
//
//        }
//
//        return UAList;
//
//    }

//    public List<ISMSDWD> GetDWNCreate(String cusno, String trdtf, String trdtt, String ordf, String ordt, String prgp) {
//
//        List<ISMSDWD> UAList = new ArrayList<ISMSDWD>();
//
//        String ord = "";
//        String grp = "";
//
//        if (!ordf.equals("") && !ordt.equals("")) {
//            ord = "AND ISHORD BETWEEN '" + ordf + "' AND '" + ordt + "'\n";
//        }
//
//        if (!prgp.equals("")) {
//            grp = "AND ISHPGRP = '" + prgp + "'";
//        }
//
//        String sql = "SELECT * FROM ISMSDWD WHERE ISDORD = (SELECT TOP 1 ISHORD\n"
//                + "FROM [RMShipment].[dbo].[ISMMASH]\n"
//                + "WHERE ISHCUNO = '0000" + cusno + "'\n"
//                + "AND ISHTRDT BETWEEN '" + trdtf.replaceAll("-", "") + "' AND '" + trdtt.replaceAll("-", "") + "'\n"
//                + ord + grp + ")";
////        System.out.println(sql);
//
//        try {
//
//            PreparedStatement ps = connect.prepareStatement(sql);
//
//            ResultSet result = ps.executeQuery();
//
//            while (result.next()) {
//
//                ISMSDWD p = new ISMSDWD();
//
//                p.setISDCONO(result.getString("ISDCONO"));
//                p.setISDDWQNO(result.getString("ISDDWQNO"));
//                p.setISDORD(result.getString("ISDORD"));
//                p.setISDSEQ(result.getString("ISDSEQ"));
//                p.setISDLINO(result.getString("ISDLINO"));
//                p.setISDITNO(result.getString("ISDITNO"));
//                p.setISDPLANT(result.getString("ISDPLANT"));
//                p.setISDVT(result.getString("ISDVT"));
//                p.setISDSTRG(result.getString("ISDSTRG"));
//                p.setISDRQQTY(result.getString("ISDRQQTY"));
//                p.setISDUNIT(result.getString("ISDUNIT"));
//                p.setISDPRICE(result.getString("ISDPRICE"));
//                p.setISDUNR01(result.getString("ISDUNR01"));
//                p.setISDUNR03(result.getString("ISDUNR03"));
//                p.setISDMTCTRL(result.getString("ISDMTCTRL"));
//                p.setISDDEST(result.getString("ISDDEST"));
//                p.setISDREMK(result.getString("ISDREMK"));
//
//                UAList.add(p);
//
//            }
//
//            ps.close();
//            connect.close();
//
//        } catch (Exception e) {
//
//            e.printStackTrace();
//
//        }
//
//        return UAList;
//
//    }
    public String SelectMAXSEQbyORDERNo(String no) {

        String sts = "";

        String sql = "SELECT (ISNULL(MAX(ISHSEQ),0) + 1) AS SEQNO FROM ISMMASH WHERE ISHORD = (SELECT TOP 1 ISDORD FROM ISMSDWD WHERE ISDDWQNO = '" + no + "')";

        try {
            Statement stmtS = connect.createStatement();
            ResultSet rs = stmtS.executeQuery(sql);

            if (rs != null) {
                while (rs.next()) {
                    sts = rs.getString("SEQNO");
                }
            }

            stmtS.close();
            connect.close();

        } catch (SQLException ex) {
            Logger.getLogger(ISM800Dao.class.getName()).log(Level.SEVERE, null, ex);
        }

        return sts;

    }

    public String SelectMaxSEQbyORDERNo(String orderno) {

        String sts = "";

        String sql = "SELECT (ISNULL(MAX(ISHSEQ),0) + 1) AS SEQNO FROM ISMSDWH WHERE ISHORD = '" + orderno + "'";

        try {
            Statement stmtS = connect.createStatement();
            ResultSet rs = stmtS.executeQuery(sql);

            if (rs != null) {
                while (rs.next()) {
                    sts = rs.getString("SEQNO");
                }
            }

            stmtS.close();
            connect.close();

        } catch (SQLException ex) {
            Logger.getLogger(ISM800Dao.class.getName()).log(Level.SEVERE, null, ex);
        }

        return sts;

    }

    public String SelectSEQbyORDERNo(String orderno) {

        String sts = "";

        String sql = "SELECT ISNULL(MAX(ISHSEQ),1) AS SEQNO FROM ISMMASH WHERE ISHORD = '" + orderno + "'";
//        String sql = "SELECT MAX (ISNULL(ISHSEQ,0)) AS SEQNO FROM ISMSDWH WHERE ISHORD = '" + orderno + "'";

        try {
            Statement stmtS = connect.createStatement();
            ResultSet rs = stmtS.executeQuery(sql);

            if (rs != null) {
                while (rs.next()) {
                    sts = rs.getString("SEQNO");
                }
            }

            stmtS.close();
            connect.close();

        } catch (SQLException ex) {
            Logger.getLogger(ISM800Dao.class.getName()).log(Level.SEVERE, null, ex);
        }

        return sts;

    }

//    public List<ISMSDWD> GetDWNbyLINE(String no, String line) {
//
//        List<ISMSDWD> UAList = new ArrayList<ISMSDWD>();
//
//        String sql = "SELECT * FROM ISMSDWD WHERE ISDDWQNO = '" + no + "' AND ISDLINO = '" + line + "'";
////        System.out.println(sql);
//
//        try {
//
//            PreparedStatement ps = connect.prepareStatement(sql);
//
//            ResultSet result = ps.executeQuery();
//
//            while (result.next()) {
//
//                ISMSDWD p = new ISMSDWD();
//
//                p.setISDCONO(result.getString("ISDCONO"));
//                p.setISDDWQNO(result.getString("ISDDWQNO"));
//                p.setISDORD(result.getString("ISDORD"));
//                p.setISDSEQ(result.getString("ISDSEQ"));
//                p.setISDLINO(result.getString("ISDLINO"));
//                p.setISDITNO(result.getString("ISDITNO"));
//                p.setISDPLANT(result.getString("ISDPLANT"));
//                p.setISDVT(result.getString("ISDVT"));
//                p.setISDSTRG(result.getString("ISDSTRG"));
//                p.setISDUNIT(result.getString("ISDUNIT"));
//                p.setISDUNR01(result.getString("ISDUNR01"));
//                p.setISDUNR03(result.getString("ISDUNR03"));
//                p.setISDMTCTRL(result.getString("ISDMTCTRL"));
//                p.setISDDEST(result.getString("ISDDEST"));
//                p.setISDREMK(result.getString("ISDREMK"));
//
//                UAList.add(p);
//
//            }
//            ps.close();
//            connect.close();
//
//        } catch (Exception e) {
//
//            e.printStackTrace();
//
//        }
//
//        return UAList;
//
//    }
//    public List<ISMMASH> GetMASHEAD(String no) {
//
//        List<ISMMASH> UAList = new ArrayList<ISMMASH>();
//
//        String sql = "SELECT * FROM ISMMASH WHERE ISHORD = (SELECT TOP 1 ISDORD FROM ISMSDWD WHERE ISDDWQNO = '" + no + "')";
////        System.out.println(sql);
//
//        try {
//
//            PreparedStatement ps = connect.prepareStatement(sql);
//
//            ResultSet result = ps.executeQuery();
//
//            while (result.next()) {
//
//                ISMMASH p = new ISMMASH();
//
//                p.setISHCONO(result.getString("ISHCONO"));
//                p.setISHORD(result.getString("ISHORD"));
//                p.setISHSEQ(result.getString("ISHSEQ"));
//                p.setISHTRDT(result.getString("ISHTRDT"));
//                p.setISHUART(result.getString("ISHUART"));
//                p.setISHTWEG(result.getString("ISHTWEG"));
//                p.setISHMVT(result.getString("ISHMVT"));
//                p.setISHCUNO(result.getString("ISHCUNO"));
//                p.setISHCUNM1(result.getString("ISHCUNM1"));
//                p.setISHCUNM2(result.getString("ISHCUNM2"));
//                p.setISHCUAD1(result.getString("ISHCUAD1"));
//                p.setISHCUAD2(result.getString("ISHCUAD2"));
//                p.setISHBSTKD(result.getString("ISHBSTKD"));
//                p.setISHSUBMI(result.getString("ISHSUBMI"));
//                p.setISHMATLOT(result.getString("ISHMATLOT"));
//                p.setISHTAXNO(result.getString("ISHTAXNO"));
//                p.setISHBRANCH01(result.getString("ISHBRANCH01"));
//                p.setISHDATUM(result.getString("ISHDATUM"));
//                p.setISHSTYLE(result.getString("ISHSTYLE"));
//                p.setISHCOLOR(result.getString("ISHCOLOR"));
//                p.setISHLOT(result.getString("ISHLOT"));
//                p.setISHAMTFG(result.getString("ISHAMTFG"));
//                p.setISHSTYLE2(result.getString("ISHSTYLE2"));
//                p.setISHNOR(result.getString("ISHNOR"));
//                p.setISHPOFG(result.getString("ISHPOFG"));
//                p.setISHGRPNO(result.getString("ISHGRPNO"));
//                p.setISHWHNO(result.getString("ISHWHNO"));
//                p.setISHPGRP(result.getString("ISHPGRP"));
//                p.setISHLSTS(result.getString("ISHLSTS"));
//                p.setISHHSTS(result.getString("ISHHSTS"));
//                p.setISHDEST(result.getString("ISHDEST"));
//                p.setISHP1DT(result.getString("ISHP1DT"));
//                p.setISHA1DT(result.getString("ISHA1DT"));
//                p.setISHT1DT(result.getString("ISHT1DT"));
//                p.setISHJBNO(result.getString("ISHJBNO"));
//                p.setISHREMARK(result.getString("ISHREMARK"));
//                p.setISHCDT(result.getString("ISHCDT"));
//                p.setISHCTM(result.getString("ISHCTM"));
//                p.setISHUSER(result.getString("ISHUSER"));
//
//                UAList.add(p);
//
//            }
//
//            ps.close();
//            connect.close();
//
//        } catch (Exception e) {
//
//            e.printStackTrace();
//
//        }
//
//        return UAList;
//
//    }
    public List<ISMSDWH> findEditinBy(String order, String dt) {

        List<ISMSDWH> UAList = new ArrayList<ISMSDWH>();

        dt = dt.split("/")[2] + dt.split("/")[1] + dt.split("/")[0];

        String sql = "SELECT DWH.ISHORD\n"
                //                + ",DWH.[ISHTRDT]\n"
                + ",(SELECT TOP 1 isnull(right(DWH.[ISHTRDT],2)+'/'+left(right(DWH.[ISHTRDT],4),2)+'/'+left(DWH.[ISHTRDT],4),'') FROM ISMSDWH het WHERE ISHORD = DWH.ISHORD) AS ISHTRDT\n"
                + ",DWH.ISHCUNO,DWH.ISHCUNM1,DWH.ISHDWQNO,DWH.ISHPGRP,MASH.ISHHSTS \n"
                + ",ISNULL(DWH.ISHREASON,'') AS REASON,ISNULL(DWH.ISHDLDT,'') AS DELI\n"
                + "FROM ISMSDWH DWH\n"
                + "LEFT JOIN ISMMASH MASH ON MASH.[ISHORD] =  DWH.ISHORD\n"
                + "WHERE DWH.ISHORD = '" + order + "' AND DWH.ISHTRDT = '" + dt + "' ";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                ISMSDWH p = new ISMSDWH();
//
                p.setNUM(result.getString("ISHDWQNO"));
                p.setISHORD(result.getString("ISHORD"));
                p.setISHTRDT(result.getString("ISHTRDT"));
                p.setISHCUNO(result.getString("ISHCUNO").replace("0000", ""));
                p.setISHCUNM1(result.getString("ISHCUNM1"));
                p.setISHPGRP(result.getString("ISHPGRP"));
                p.setSTATUS(result.getString("ISHHSTS"));
                p.setReason(result.getString("REASON"));

                String deli = result.getString("DELI");

                if (!deli.equals("")) {
                    deli = deli.split("-")[2] + "/" + deli.split("-")[1] + "/" + deli.split("-")[0];
                }

                p.setISHDLDT(deli);

                UAList.add(p);

            }

            ps.close();
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public List<ISMSDWD> findEditDetailByORD(String order, String no, String dt) {

        List<ISMSDWD> UAList = new ArrayList<ISMSDWD>();

        String sql = "SELECT DW.[ISDORD],DW.[ISDSEQ],DW.[ISDLINO],DW.[ISDITNO],ISNULL(DW.[ISDVT],'') AS ISDVT\n"
                + ",isnull((select top 1 [SAPDESC] from [SAPMAS] where [SAPMAT]=DW.[ISDITNO] collate SQL_Latin1_General_CP1_CI_AS),'') as [ISDDESC]\n"
                + ",isnull(format((isnull(DW.[ISDUNR01],0)+isnull(DW.[ISDUNR03],0)),'#,#0.00'),0) as [ISDSAPONH]\n"
                + ",isnull((SELECT format(sum([QRMQTY]),'#,#0.00')\n"
                + "FROM [QRMMAS]\n"
                + "where [QRMCODE]=DW.[ISDITNO] collate SQL_Latin1_General_CP1_CI_AS\n"
                + "and [QRMSTS]='2'),0) as [ISDWMSONH]\n"
                + ",isnull(format(DW.[ISDRQQTY],'#,#0.00'),0) as [ISDRQQTY]\n"
                + ",isnull(DW.[ISDUNIT],'') as [ISDUNIT]\n"
                + ",ISNULL(DE.[ISDSTS],'0') AS ISDSTS\n"
                + ",ISNULL(HD.ISHREASON,'') AS REASON\n"
                + ",ISNULL(HD.ISHSTYLE,'') AS ISHSTYLE,ISNULL(HD.ISHCOLOR,'') AS ISHCOLOR,ISNULL(HD.ISHLOT,'') AS ISHLOT \n"
                + ",ISNULL(DW.ISDPURG,'') AS ISDPURG \n"
                + ",ISNULL(DW.ISDSUSER,'') AS ISDSUSER \n"
                + "FROM ISMSDWD DW\n"
                + "LEFT JOIN ISMMASD DE ON DE.ISDORD = DW.ISDORD AND DE.ISDLINO = DW.ISDLINO\n"
                + "LEFT JOIN ISMSDWH HD ON HD.ISHORD = DW.ISDORD\n"
                + "WHERE DW.[ISDORD] = '" + order + "' AND DW.[ISDDWQNO] = '" + no + "' ";

        try {
//            System.out.println(sql);
            PreparedStatement ps = connect.prepareStatement(sql);
            ResultSet result = ps.executeQuery();

            while (result.next()) {

                ISMSDWD p = new ISMSDWD();

                p.setISDORD(result.getString("ISDORD"));
                p.setISDSEQ(result.getString("ISDSEQ"));
                p.setISDLINO(result.getString("ISDLINO"));
                p.setISDITNO(result.getString("ISDITNO"));
                p.setISDVT(result.getString("ISDVT"));
                p.setISDDESC(result.getString("ISDDESC"));
                p.setISDSAPONH(result.getString("ISDSAPONH"));
                p.setISDWMSONH(result.getString("ISDWMSONH"));
                p.setISDRQQTY(result.getString("ISDRQQTY"));
                p.setISDUNIT(result.getString("ISDUNIT"));
                p.setISDSTS(result.getString("ISDSTS"));
                p.setISDREMK(result.getString("REASON"));
                p.setSTYLE(result.getString("ISHSTYLE"));
                p.setCOLOR(result.getString("ISHCOLOR"));
                p.setLOT(result.getString("ISHLOT"));
                p.setISDPURG(result.getString("ISDPURG"));
                p.setISDSUSER(result.getString("ISDSUSER"));

                UAList.add(p);

            }

            ps.close();
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public List<ISMMASD> GetMASByORD(String order) {

        List<ISMMASD> UAList = new ArrayList<ISMMASD>();

        String sql = "SELECT * FROM ISMMASD WHERE ISDORD = '" + order + "' ORDER BY ISDLINO";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                ISMMASD p = new ISMMASD();

                p.setISDCONO(result.getString("ISDCONO"));
                p.setISDORD(result.getString("ISDORD"));
                p.setISDSEQ(result.getString("ISDSEQ"));
                p.setISDLINO(result.getString("ISDLINO"));
                p.setISDSINO(result.getString("ISDSINO"));
                p.setISDITNO(result.getString("ISDITNO"));
                p.setISDPLANT(result.getString("ISDPLANT"));
                p.setISDVT(result.getString("ISDVT"));
                p.setISDSTRG(result.getString("ISDSTRG"));
                p.setISDRQQTY(result.getString("ISDRQQTY"));
                p.setISDUNIT(result.getString("ISDUNIT"));
                p.setISDPRICE(result.getString("ISDPRICE"));
                p.setISDUNR01(result.getString("ISDUNR01"));
                p.setISDUNR03(result.getString("ISDUNR03"));
                p.setISDMTCTRL(result.getString("ISDMTCTRL"));
                p.setISDDEST(result.getString("ISDDEST"));
                p.setISDRQDT(result.getString("ISDRQDT"));
                p.setISDRQTM(result.getString("ISDRQTM"));
                p.setISDAPDT(result.getString("ISDAPDT"));
                p.setISDAPTM(result.getString("ISDAPTM"));
                p.setISDAPUSER(result.getString("ISDAPUSER"));
                p.setISDRCDT(result.getString("ISDRCDT"));
                p.setISDPKQTY(result.getString("ISDPKQTY"));
                p.setISDPKQTY(result.getString("ISDPKQTY"));
                p.setISDPKDT(result.getString("ISDPKDT"));
                p.setISDPKTM(result.getString("ISDPKTM"));
                p.setISDPKUSER(result.getString("ISDPKUSER"));
                p.setISDTPQTY(result.getString("ISDTPQTY"));
                p.setISDTPDT(result.getString("ISDTPDT"));
                p.setISDTPTM(result.getString("ISDTPTM"));
                p.setISDTPUSER(result.getString("ISDTPUSER"));
                p.setISDSTS(result.getString("ISDSTS"));
                p.setISDJBNO(result.getString("ISDJBNO"));
                p.setISDQNO(result.getString("ISDQNO"));
                p.setISDREMARK(result.getString("ISDREMARK"));
                p.setISDISUNO(result.getString("ISDISUNO"));
                p.setISDTEMP(result.getString("ISDTEMP"));
                p.setISDEDT(result.getString("ISDEDT"));
                p.setISDETM(result.getString("ISDETM"));
                p.setISDEUSR(result.getString("ISDEUSR"));
                p.setISDCDT(result.getString("ISDCDT"));
                p.setISDCTM(result.getString("ISDCTM"));
                p.setISDUSER(result.getString("ISDUSER"));
                p.setISDSTAD(result.getString("ISDSTAD"));
                p.setISDSTSTMP(result.getString("ISDSTSTMP"));
                p.setISDREMAIN(result.getString("ISDREMAIN"));

                UAList.add(p);

            }

            ps.close();
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public List<ISMMASD> GetMASByORDnSEQ(String order, String seq) {

        List<ISMMASD> UAList = new ArrayList<ISMMASD>();

        String sql = "SELECT * FROM ISMMASD WHERE ISDORD = '" + order + "' AND ISDSEQ = '" + seq + "' ORDER BY ISDLINO";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                ISMMASD p = new ISMMASD();

                p.setISDCONO(result.getString("ISDCONO"));
                p.setISDORD(result.getString("ISDORD"));
                p.setISDSEQ(result.getString("ISDSEQ"));
                p.setISDLINO(result.getString("ISDLINO"));
                p.setISDSINO(result.getString("ISDSINO"));
                p.setISDITNO(result.getString("ISDITNO"));
                p.setISDPLANT(result.getString("ISDPLANT"));
                p.setISDVT(result.getString("ISDVT"));
                p.setISDSTRG(result.getString("ISDSTRG"));
                p.setISDRQQTY(result.getString("ISDRQQTY"));
                p.setISDUNIT(result.getString("ISDUNIT"));
                p.setISDPRICE(result.getString("ISDPRICE"));
                p.setISDUNR01(result.getString("ISDUNR01"));
                p.setISDUNR03(result.getString("ISDUNR03"));
                p.setISDMTCTRL(result.getString("ISDMTCTRL"));
                p.setISDDEST(result.getString("ISDDEST"));
                p.setISDRQDT(result.getString("ISDRQDT"));
                p.setISDRQTM(result.getString("ISDRQTM"));
                p.setISDAPDT(result.getString("ISDAPDT"));
                p.setISDAPTM(result.getString("ISDAPTM"));
                p.setISDAPUSER(result.getString("ISDAPUSER"));
                p.setISDRCDT(result.getString("ISDRCDT"));
                p.setISDPKQTY(result.getString("ISDPKQTY"));
                p.setISDPKQTY(result.getString("ISDPKQTY"));
                p.setISDPKDT(result.getString("ISDPKDT"));
                p.setISDPKTM(result.getString("ISDPKTM"));
                p.setISDPKUSER(result.getString("ISDPKUSER"));
                p.setISDTPQTY(result.getString("ISDTPQTY"));
                p.setISDTPDT(result.getString("ISDTPDT"));
                p.setISDTPTM(result.getString("ISDTPTM"));
                p.setISDTPUSER(result.getString("ISDTPUSER"));
                p.setISDSTS(result.getString("ISDSTS"));
                p.setISDJBNO(result.getString("ISDJBNO"));
                p.setISDQNO(result.getString("ISDQNO"));
                p.setISDREMARK(result.getString("ISDREMARK"));
                p.setISDISUNO(result.getString("ISDISUNO"));
                p.setISDTEMP(result.getString("ISDTEMP"));
                p.setISDEDT(result.getString("ISDEDT"));
                p.setISDETM(result.getString("ISDETM"));
                p.setISDEUSR(result.getString("ISDEUSR"));
                p.setISDCDT(result.getString("ISDCDT"));
                p.setISDCTM(result.getString("ISDCTM"));
                p.setISDUSER(result.getString("ISDUSER"));
                p.setISDSTAD(result.getString("ISDSTAD"));
                p.setISDSTSTMP(result.getString("ISDSTSTMP"));
                p.setISDREMAIN(result.getString("ISDREMAIN"));

                UAList.add(p);

            }

            ps.close();
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public List<ISMSDWD> GetDWNByORD(String order, String no) {

        List<ISMSDWD> UAList = new ArrayList<ISMSDWD>();

        String sql = "SELECT * FROM ISMSDWD WHERE ISDORD = '" + order + "' ORDER BY ISDLINO";
//        String sql = "SELECT * FROM ISMSDWD WHERE ISDORD = '" + order + "' AND ISDDWQNO = '" + no + "' ORDER BY ISDLINO";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                ISMSDWD p = new ISMSDWD();

                p.setISDCONO(result.getString("ISDCONO"));
                p.setISDDWQNO(result.getString("ISDDWQNO"));
                p.setISDORD(result.getString("ISDORD"));
                p.setISDSEQ(result.getString("ISDSEQ"));
                p.setISDLINO(result.getString("ISDLINO"));
                p.setISDITNO(result.getString("ISDITNO"));
                p.setISDPLANT(result.getString("ISDPLANT"));
                p.setISDVT(result.getString("ISDVT"));
                p.setISDSTRG(result.getString("ISDSTRG"));
                p.setISDRQQTY(result.getString("ISDRQQTY"));
                p.setISDUNIT(result.getString("ISDUNIT"));
                p.setISDPRICE(result.getString("ISDPRICE"));
                p.setISDUNR01(result.getString("ISDUNR01"));
                p.setISDUNR03(result.getString("ISDUNR03"));
                p.setISDMTCTRL(result.getString("ISDMTCTRL"));
                p.setISDDEST(result.getString("ISDDEST"));
                p.setISDREMK(result.getString("ISDREMK"));

                UAList.add(p);

            }

            ps.close();
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public List<ISMSDWH> GetNotDupHMasnDwn(String order, String date, String no) {

        date = date.split("/")[2] + date.split("/")[1] + date.split("/")[0];

        List<ISMSDWH> UAList = new ArrayList<ISMSDWH>();

        String sql = "SELECT DW.*,ISNULL(MH.ISHLSTS,0) AS ISHLSTSM,ISNULL(MH.ISHHSTS,0) AS ISHHSTSM FROM ISMSDWH DW\n"
                + "FULL OUTER JOIN ISMMASH MH ON MH.ISHORD = DW.ISHORD AND MH.ISHSEQ = DW.ISHSEQ\n"
                + "WHERE DW.ISHORD = '" + order + "' AND DW.ISHTRDT = '" + date + "' AND DW.ISHDWQNO = '" + no + "' --AND (MH.ISHORD IS NULL OR MH.ISHSEQ IS NULL) \n"
                + "AND DW.ISHSEQ = (SELECT TOP 1 ISHSEQ FROM ISMSDWH WHERE ISHORD = '" + order + "' AND ISHTRDT = '" + date + "' AND ISHDWQNO = '" + no + "') ";
//        String sql = "SELECT DW.* FROM ISMSDWH DW\n"
//                + "FULL OUTER JOIN ISMMASH MH ON MH.ISHORD = DW.ISHORD AND MH.ISHSEQ = DW.ISHSEQ\n"
//                + "WHERE DW.ISHORD = '" + order + "' AND ((MH.ISHLSTS + MH.ISHHSTS) = 0  OR (MH.ISHORD IS NULL OR MH.ISHSEQ IS NULL))";
//        String sql = "SELECT DW.* FROM ISMSDWH DW\n"
//                + "FULL OUTER JOIN ISMMASH MH ON MH.ISHORD = DW.ISHORD AND MH.ISHSEQ = DW.ISHSEQ\n"
//                + "WHERE DW.ISHORD = '" + order + "' AND MH.ISHORD IS NULL AND MH.ISHSEQ IS NULL";

//        System.out.println(sql);
        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                ISMSDWH p = new ISMSDWH();

                p.setISHCONO(result.getString("ISHCONO"));
                p.setISHDWQNO(result.getString("ISHDWQNO"));
                p.setISHORD(result.getString("ISHORD"));
                p.setISHSEQ(result.getString("ISHSEQ"));
                p.setISHTRDT(result.getString("ISHTRDT"));
                p.setISHUART(result.getString("ISHUART"));
                p.setISHSORG(result.getString("ISHSORG"));
                p.setISHTWEG(result.getString("ISHTWEG"));
                p.setISHDIVI(result.getString("ISHDIVI"));
                p.setISHSGRP(result.getString("ISHSGRP"));
                p.setISHSOFF(result.getString("ISHSOFF"));
                p.setISHMVT(result.getString("ISHMVT"));
                p.setISHCUNO(result.getString("ISHCUNO"));
                p.setISHCUNM1(result.getString("ISHCUNM1"));
                p.setISHCUNM2(result.getString("ISHCUNM2"));
                p.setISHCUAD1(result.getString("ISHCUAD1"));
                p.setISHCUAD2(result.getString("ISHCUAD2"));
                p.setISHBSTKD(result.getString("ISHBSTKD"));
                p.setISHSUBMI(result.getString("ISHSUBMI"));
                p.setISHMATLOT(result.getString("ISHMATLOT"));
                p.setISHDDATE(result.getString("ISHDDATE"));
                p.setISHDTIME(result.getString("ISHDTIME"));
                p.setISHTAXNO(result.getString("ISHTAXNO"));
                p.setISHBRANCH01(result.getString("ISHBRANCH01"));
                p.setISHSTYLE(result.getString("ISHSTYLE"));
                p.setISHCOLOR(result.getString("ISHCOLOR"));
                p.setISHLOT(result.getString("ISHLOT"));
                p.setISHAMTFG(result.getString("ISHAMTFG"));
                p.setISHSTYLE2(result.getString("ISHSTYLE2"));
                p.setISHNOR(result.getString("ISHNOR"));
                p.setISHPOFG(result.getString("ISHPOFG"));
                p.setISHWHNO(result.getString("ISHWHNO"));
                p.setISHPGRP(result.getString("ISHPGRP"));
                p.setISHLSTS(result.getString("ISHLSTSM"));
                p.setISHHSTS(result.getString("ISHHSTSM"));
                p.setISHDEST(result.getString("ISHDEST"));
                p.setISHREMK(result.getString("ISHREMK"));
                p.setISHCUNO2(result.getString("ISHCUNO2"));
                p.setISHDLDT(result.getString("ISHDLDT"));
                p.setISHDLS(result.getString("ISHDLS"));
                p.setISHODLS(result.getString("ISHODLS"));
                p.setISHCRDT(result.getString("ISHCRDT"));
                p.setISHCRTM(result.getString("ISHCRTM"));
                p.setISHSUSER(result.getString("ISHSUSER"));
                p.setISHEDT(result.getString("ISHEDT"));
                p.setISHETM(result.getString("ISHETM"));
                p.setISHEUSR(result.getString("ISHEUSR"));
                p.setISHCDT(result.getString("ISHCDT"));
                p.setISHCTM(result.getString("ISHCTM"));
                p.setISHUSER(result.getString("ISHUSER"));
                p.setISHREASON(result.getString("ISHREASON"));

                UAList.add(p);

            }

            ps.close();
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public List<ISMSDWD> GetNotDupMasnDwn(String order, String date, String no) {

        date = date.split("/")[2] + date.split("/")[1] + date.split("/")[0];

        List<ISMSDWD> UAList = new ArrayList<ISMSDWD>();

        String sql = "SELECT DW.*,ISNULL(MD.ISDSTS,0) AS ISDSTSM FROM ISMSDWD DW \n"
                + "FULL OUTER JOIN ISMMASD MD ON DW.ISDORD = MD.ISDORD /*AND DW.ISDSEQ = MD.ISDSEQ*/ AND DW.ISDLINO = MD.ISDLINO \n"
                + "WHERE DW.ISDORD = '" + order + "' AND (ISNULL(DW.ISDUNR01,0) + ISNULL(DW.ISDUNR03,0)) != 0 AND DW.ISDSEQ = (SELECT TOP 1 ISHSEQ FROM ISMSDWH WHERE ISHORD = '" + order + "' AND ISHTRDT = '" + date + "' AND ISHDWQNO = '" + no + "') \n"
                + "AND (ISNULL(MD.ISDSTS,'0') = 0 OR ISNULL(MD.ISDSTS,'0') = 9 )  AND ISDDWQNO = '" + no + "' \n"
                + "ORDER BY DW.ISDLINO";
//        String sql = "SELECT DW.* FROM ISMSDWD DW\n"
//                + "FULL OUTER JOIN ISMMASD MD ON DW.ISDORD = MD.ISDORD AND DW.ISDSEQ = MD.ISDSEQ AND DW.ISDLINO = MD.ISDLINO\n"
//                + "WHERE DW.ISDORD = '" + order + "' AND (MD.ISDSTS = 0 OR (MD.ISDORD IS NULL OR MD.ISDSEQ IS NULL OR MD.ISDLINO IS NULL))\n"
//                + "ORDER BY DW.ISDLINO";

//        System.out.println(sql);
        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                ISMSDWD p = new ISMSDWD();

                p.setISDCONO(result.getString("ISDCONO"));
                p.setISDDWQNO(result.getString("ISDDWQNO"));
                p.setISDORD(result.getString("ISDORD"));
                p.setISDSEQ(result.getString("ISDSEQ"));
                p.setISDLINO(result.getString("ISDLINO"));
                p.setISDITNO(result.getString("ISDITNO"));
                p.setISDPLANT(result.getString("ISDPLANT"));
                p.setISDVT(result.getString("ISDVT"));
                p.setISDSTRG(result.getString("ISDSTRG"));
                p.setISDRQQTY(result.getString("ISDRQQTY"));
                p.setISDUNIT(result.getString("ISDUNIT"));
                p.setISDPRICE(result.getString("ISDPRICE"));
                p.setISDUNR01(result.getString("ISDUNR01"));
                p.setISDUNR03(result.getString("ISDUNR03"));
                p.setISDMTCTRL(result.getString("ISDMTCTRL"));
                p.setISDDEST(result.getString("ISDDEST"));
                p.setISDREMK(result.getString("ISDREMK"));
                p.setISDSTS(result.getString("ISDSTSM"));
                p.setISDPURG(result.getString("ISDPURG"));

                UAList.add(p);

            }

            ps.close();
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public List<ISMSDWD> GetNotDupMasnDwnPerLine(String order, String line, String seq) {

        List<ISMSDWD> UAList = new ArrayList<ISMSDWD>();

        String sql = "SELECT DW.* FROM ISMSDWD DW\n"
                + "FULL OUTER JOIN ISMMASD MD ON DW.ISDORD = MD.ISDORD /*AND DW.ISDSEQ = MD.ISDSEQ*/ AND DW.ISDLINO = MD.ISDLINO\n"
                + "WHERE DW.ISDORD = '" + order + "' AND DW.ISDLINO = '" + line + "' AND DW.ISDSEQ = '" + seq + "' AND MD.ISDORD IS NULL AND MD.ISDSEQ IS NULL AND MD.ISDLINO IS NULL\n"
                + "ORDER BY DW.ISDLINO";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                ISMSDWD p = new ISMSDWD();

                p.setISDCONO(result.getString("ISDCONO"));
                p.setISDDWQNO(result.getString("ISDDWQNO"));
                p.setISDORD(result.getString("ISDORD"));
                p.setISDSEQ(result.getString("ISDSEQ"));
                p.setISDLINO(result.getString("ISDLINO"));
                p.setISDITNO(result.getString("ISDITNO"));
                p.setISDPLANT(result.getString("ISDPLANT"));
                p.setISDVT(result.getString("ISDVT"));
                p.setISDSTRG(result.getString("ISDSTRG"));
                p.setISDRQQTY(result.getString("ISDRQQTY"));
                p.setISDUNIT(result.getString("ISDUNIT"));
                p.setISDPRICE(result.getString("ISDPRICE"));
                p.setISDUNR01(result.getString("ISDUNR01"));
                p.setISDUNR03(result.getString("ISDUNR03"));
                p.setISDMTCTRL(result.getString("ISDMTCTRL"));
                p.setISDDEST(result.getString("ISDDEST"));
                p.setISDREMK(result.getString("ISDREMK"));
                p.setISDPURG(result.getString("ISDPURG"));

                UAList.add(p);

            }

            ps.close();
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public List<ISMSDWH> GetDWNHeadByORD(String order, String no) {

        List<ISMSDWH> UAList = new ArrayList<ISMSDWH>();

        String sql = "SELECT * FROM ISMSDWH WHERE ISHORD = '" + order + "' AND ISHDWQNO = '" + no + "' ORDER BY ISHORD";

        try {

//            System.out.println(sql);
            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                ISMSDWH p = new ISMSDWH();

                p.setISHCONO(result.getString("ISHCONO"));
                p.setISHDWQNO(result.getString("ISHDWQNO"));
                p.setISHORD(result.getString("ISHORD"));
                p.setISHSEQ(result.getString("ISHSEQ"));
                p.setISHTRDT(result.getString("ISHTRDT"));
                p.setISHUART(result.getString("ISHUART"));
                p.setISHSORG(result.getString("ISHSORG"));
                p.setISHTWEG(result.getString("ISHTWEG"));
                p.setISHDIVI(result.getString("ISHDIVI"));
                p.setISHSGRP(result.getString("ISHSGRP"));
                p.setISHSOFF(result.getString("ISHSOFF"));
                p.setISHMVT(result.getString("ISHMVT"));
                p.setISHCUNO(result.getString("ISHCUNO"));
                p.setISHCUNM1(result.getString("ISHCUNM1"));
                p.setISHCUNM2(result.getString("ISHCUNM2"));
                p.setISHCUAD1(result.getString("ISHCUAD1"));
                p.setISHCUAD2(result.getString("ISHCUAD2"));
                p.setISHBSTKD(result.getString("ISHBSTKD"));
                p.setISHSUBMI(result.getString("ISHSUBMI"));
                p.setISHMATLOT(result.getString("ISHMATLOT"));
                p.setISHDDATE(result.getString("ISHDDATE"));
                p.setISHDTIME(result.getString("ISHDTIME"));
                p.setISHTAXNO(result.getString("ISHTAXNO"));
                p.setISHBRANCH01(result.getString("ISHBRANCH01"));
                p.setISHSTYLE(result.getString("ISHSTYLE"));
                p.setISHCOLOR(result.getString("ISHCOLOR"));
                p.setISHLOT(result.getString("ISHLOT"));
                p.setISHAMTFG(result.getString("ISHAMTFG"));
                p.setISHSTYLE2(result.getString("ISHSTYLE2"));
                p.setISHNOR(result.getString("ISHNOR"));
                p.setISHPOFG(result.getString("ISHPOFG"));
                p.setISHWHNO(result.getString("ISHWHNO"));
                p.setISHPGRP(result.getString("ISHPGRP"));
                p.setISHLSTS(result.getString("ISHLSTS"));
                p.setISHHSTS(result.getString("ISHHSTS"));
                p.setISHDEST(result.getString("ISHDEST"));
                p.setISHREMK(result.getString("ISHREMK"));
                p.setISHCUNO2(result.getString("ISHCUNO2"));
                p.setISHDLDT(result.getString("ISHDLDT"));
                p.setISHDLS(result.getString("ISHDLS"));
                p.setISHODLS(result.getString("ISHODLS"));
                p.setISHCRDT(result.getString("ISHCRDT"));
                p.setISHCRTM(result.getString("ISHCRTM"));
                p.setISHSUSER(result.getString("ISHSUSER"));
                p.setISHEDT(result.getString("ISHEDT"));
                p.setISHETM(result.getString("ISHETM"));
                p.setISHEUSR(result.getString("ISHEUSR"));
                p.setISHCDT(result.getString("ISHCDT"));
                p.setISHCTM(result.getString("ISHCTM"));
                p.setISHUSER(result.getString("ISHUSER"));

                UAList.add(p);

            }

            ps.close();
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public List<ISMSDWH> GetDWNHeadByORDWithSEQ(String order, String no, String seq) {

        List<ISMSDWH> UAList = new ArrayList<ISMSDWH>();

        String sql = "SELECT * FROM ISMSDWH WHERE ISHORD = '" + order + "' AND ISHDWQNO = '" + no + "' AND ISHSEQ = '" + seq + "' ORDER BY ISHORD";

        try {

//            System.out.println(sql);
            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                ISMSDWH p = new ISMSDWH();

                p.setISHCONO(result.getString("ISHCONO"));
                p.setISHDWQNO(result.getString("ISHDWQNO"));
                p.setISHORD(result.getString("ISHORD"));
                p.setISHSEQ(result.getString("ISHSEQ"));
                p.setISHTRDT(result.getString("ISHTRDT"));
                p.setISHUART(result.getString("ISHUART"));
                p.setISHSORG(result.getString("ISHSORG"));
                p.setISHTWEG(result.getString("ISHTWEG"));
                p.setISHDIVI(result.getString("ISHDIVI"));
                p.setISHSGRP(result.getString("ISHSGRP"));
                p.setISHSOFF(result.getString("ISHSOFF"));
                p.setISHMVT(result.getString("ISHMVT"));
                p.setISHCUNO(result.getString("ISHCUNO"));
                p.setISHCUNM1(result.getString("ISHCUNM1"));
                p.setISHCUNM2(result.getString("ISHCUNM2"));
                p.setISHCUAD1(result.getString("ISHCUAD1"));
                p.setISHCUAD2(result.getString("ISHCUAD2"));
                p.setISHBSTKD(result.getString("ISHBSTKD"));
                p.setISHSUBMI(result.getString("ISHSUBMI"));
                p.setISHMATLOT(result.getString("ISHMATLOT"));
                p.setISHDDATE(result.getString("ISHDDATE"));
                p.setISHDTIME(result.getString("ISHDTIME"));
                p.setISHTAXNO(result.getString("ISHTAXNO"));
                p.setISHBRANCH01(result.getString("ISHBRANCH01"));
                p.setISHSTYLE(result.getString("ISHSTYLE"));
                p.setISHCOLOR(result.getString("ISHCOLOR"));
                p.setISHLOT(result.getString("ISHLOT"));
                p.setISHAMTFG(result.getString("ISHAMTFG"));
                p.setISHSTYLE2(result.getString("ISHSTYLE2"));
                p.setISHNOR(result.getString("ISHNOR"));
                p.setISHPOFG(result.getString("ISHPOFG"));
                p.setISHWHNO(result.getString("ISHWHNO"));
                p.setISHPGRP(result.getString("ISHPGRP"));
                p.setISHLSTS(result.getString("ISHLSTS"));
                p.setISHHSTS(result.getString("ISHHSTS"));
                p.setISHDEST(result.getString("ISHDEST"));
                p.setISHREMK(result.getString("ISHREMK"));
                p.setISHCUNO2(result.getString("ISHCUNO2"));
                p.setISHDLDT(result.getString("ISHDLDT"));
                p.setISHDLS(result.getString("ISHDLS"));
                p.setISHODLS(result.getString("ISHODLS"));
                p.setISHCRDT(result.getString("ISHCRDT"));
                p.setISHCRTM(result.getString("ISHCRTM"));
                p.setISHSUSER(result.getString("ISHSUSER"));
                p.setISHEDT(result.getString("ISHEDT"));
                p.setISHETM(result.getString("ISHETM"));
                p.setISHEUSR(result.getString("ISHEUSR"));
                p.setISHCDT(result.getString("ISHCDT"));
                p.setISHCTM(result.getString("ISHCTM"));
                p.setISHUSER(result.getString("ISHUSER"));
                p.setISHREASON(result.getString("ISHREASON"));

                UAList.add(p);

            }

            ps.close();
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public String getCUSName(String cuno) {
        String max = "";

        String sql = "SELECT TOP 1 ISHCUNM1\n"
                + "FROM ISMSDWH DWH\n"
                + "WHERE ISHCUNO = '0000' + '" + cuno + "'";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                max = result.getString("ISHCUNM1");

            }

            ps.close();
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return max;

    }

    public JSONObject deleteDownloadIN(String no, String order) {

        JSONObject res = new JSONObject();

        try {
            Statement stmtIUD = connect.createStatement();

            String sqlDelSL = "DELETE [RMShipment].[dbo].[ISMSDWS] WHERE ISSMTNO COLLATE SQL_Latin1_General_CP1_CI_AS IN (SELECT DISTINCT ISDITNO COLLATE SQL_Latin1_General_CP1_CI_AS FROM [RMShipment].[dbo].[ISMMASD] WHERE ISDORD = '" + order + "') ";
            int DelSL = stmtIUD.executeUpdate(sqlDelSL);

            String sqlDelDwn = "DELETE ISMSDWD WHERE ISDDWQNO = '" + no + "' AND ISDORD = '" + order + "' "
                    + "DELETE ISMSDWH WHERE ISHDWQNO = '" + no + "' AND ISHORD = '" + order + "'";
            int DelDwn = stmtIUD.executeUpdate(sqlDelDwn);

//            String sqlDelMas = "DELETE [RMShipment].[dbo].[ISMMASD] WHERE ISDORD = '" + order + "' "
//                    + "DELETE [RMShipment].[dbo].[ISMMASH] WHERE ISHORD = '" + order + "' ";
//            int DelMas = stmtIUD.executeUpdate(sqlDelMas);
            if (DelSL > 0) {
                res.put("DelSL", true);
            } else {
                res.put("DelSL", false);
            }
            if (DelDwn > 0) {
                res.put("DelDwn", true);
            } else {
                res.put("DelDwn", false);
            }
//            if (DelMas > 0) {
//                res.put("DelMas", true);
//            } else {
//                res.put("DelMas", false);
//            }

            stmtIUD.close();
            connect.close();

        } catch (SQLException ex) {
            Logger.getLogger(ISM800Dao.class.getName()).log(Level.SEVERE, null, ex);
        }

        return res;

    }

    public boolean deleteDownloadLineIN(String no, String order, String line) {

        boolean result = false;

        try {
            Statement stmtIUD = connect.createStatement();

            String sqlSelMatCode = "DELETE ISMSDWS WHERE ISSMTNO COLLATE SQL_Latin1_General_CP1_CI_AS IN (SELECT DISTINCT ISDITNO COLLATE SQL_Latin1_General_CP1_CI_AS FROM ISMSDWD WHERE ISDDWQNO = '" + no + "' AND ISDORD = '" + order + "' AND ISDLINO = '" + line + "') ";
            int delSLbyMatCode = stmtIUD.executeUpdate(sqlSelMatCode);

            String sql = "DELETE ISMSDWD WHERE ISDDWQNO = '" + no + "' AND ISDORD = '" + order + "' AND ISDLINO = '" + line + "'";
            int delDwnDetail = stmtIUD.executeUpdate(sql);

//            String sqlDelMas = "DELETE [RMShipment].[dbo].[ISMMASD] WHERE ISDORD = '" + order + "' AND ISDLINO = '" + line + "'";
//            int delMaster = stmtIUD.executeUpdate(sqlDelMas);
            if (delDwnDetail > 0) {
                result = true;
            }

            stmtIUD.close();
            connect.close();

        } catch (SQLException ex) {
            Logger.getLogger(ISM800Dao.class.getName()).log(Level.SEVERE, null, ex);
        }

        return result;

    }

    public boolean deleteDownloadLineIN2(String no, String order, String line, String seq) {

        boolean result = false;

        try {

            Statement stmtIUD = connect.createStatement();

            String sqlSelMatCode = "DELETE ISMSDWS WHERE ISSMTNO COLLATE SQL_Latin1_General_CP1_CI_AS IN (SELECT DISTINCT ISDITNO COLLATE SQL_Latin1_General_CP1_CI_AS FROM ISMSDWD WHERE ISDDWQNO = '" + no + "' AND ISDORD = '" + order + "' AND ISDLINO = '" + line + "') ";
            int delSLbyMatCode = stmtIUD.executeUpdate(sqlSelMatCode);

            String sql = "DELETE ISMSDWD WHERE ISDDWQNO = '" + no + "' AND ISDORD = '" + order + "' AND ISDLINO = '" + line + "'";
            int delDwnDetail = stmtIUD.executeUpdate(sql);

            String updHead = "UPDATE [RMShipment].[dbo].[ISMSDWH] SET ISHNOR = (ISHNOR-1) WHERE ISHDWQNO = '" + no + "' AND ISHORD = '" + order + "' AND ISHSEQ = '" + seq + "'";
            int updVal = stmtIUD.executeUpdate(updHead);

//            String sqlDelMas = "DELETE [RMShipment].[dbo].[ISMMASD] WHERE ISDORD = '" + order + "' AND ISDLINO = '" + line + "'";
//            int delMaster = stmtIUD.executeUpdate(sqlDelMas);
            if (delDwnDetail > 0) {
                result = true;
            }

            stmtIUD.close();
            connect.close();

        } catch (SQLException ex) {
            Logger.getLogger(ISM800Dao.class.getName()).log(Level.SEVERE, null, ex);
        }

        return result;

    }

    public String SelectMAXSEQbyORD(String order) {
        String sts = "";

        String sql = "SELECT (ISNULL(MAX(ISDSEQ),0) + 1) AS SEQ FROM ISMMASD WHERE ISDORD = '" + order + "'";
//        String sql = "SELECT MAX(ISHSEQ) + 1 AS SEQ FROM ISMMASH WHERE ISHORD = '" + order + "'";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                sts = result.getString("SEQ");

            }

            ps.close();
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return sts;

    }

    public List<ISMMASH> GetMASHEADByORD(String order) {

        List<ISMMASH> UAList = new ArrayList<ISMMASH>();

        String sql = "SELECT * FROM ISMMASH WHERE ISHORD = '" + order + "'";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                ISMMASH p = new ISMMASH();

                p.setISHCONO(result.getString("ISHCONO"));
                p.setISHORD(result.getString("ISHORD"));
                p.setISHSEQ(result.getString("ISHSEQ"));
                p.setISHTRDT(result.getString("ISHTRDT"));
                p.setISHUART(result.getString("ISHUART"));
                p.setISHTWEG(result.getString("ISHTWEG"));
                p.setISHMVT(result.getString("ISHMVT"));
                p.setISHCUNO(result.getString("ISHCUNO"));
                p.setISHCUNM1(result.getString("ISHCUNM1"));
                p.setISHCUNM2(result.getString("ISHCUNM2"));
                p.setISHCUAD1(result.getString("ISHCUAD1"));
                p.setISHCUAD2(result.getString("ISHCUAD2"));
                p.setISHBSTKD(result.getString("ISHBSTKD"));
                p.setISHSUBMI(result.getString("ISHSUBMI"));
                p.setISHMATLOT(result.getString("ISHMATLOT"));
                p.setISHTAXNO(result.getString("ISHTAXNO"));
                p.setISHBRANCH01(result.getString("ISHBRANCH01"));
                p.setISHDATUM(result.getString("ISHDATUM"));
                p.setISHSTYLE(result.getString("ISHSTYLE"));
                p.setISHCOLOR(result.getString("ISHCOLOR"));
                p.setISHLOT(result.getString("ISHLOT"));
                p.setISHAMTFG(result.getString("ISHAMTFG"));
                p.setISHSTYLE2(result.getString("ISHSTYLE2"));
                p.setISHNOR(result.getString("ISHNOR"));
                p.setISHPOFG(result.getString("ISHPOFG"));
                p.setISHGRPNO(result.getString("ISHGRPNO"));
                p.setISHWHNO(result.getString("ISHWHNO"));
                p.setISHPGRP(result.getString("ISHPGRP"));
                p.setISHLSTS(result.getString("ISHLSTS"));
                p.setISHHSTS(result.getString("ISHHSTS"));
                p.setISHDEST(result.getString("ISHDEST"));
                p.setISHP1DT(result.getString("ISHP1DT"));
                p.setISHA1DT(result.getString("ISHA1DT"));
                p.setISHT1DT(result.getString("ISHT1DT"));
                p.setISHJBNO(result.getString("ISHJBNO"));
                p.setISHREMARK(result.getString("ISHREMARK"));
                p.setISHCDT(result.getString("ISHCDT"));
                p.setISHCTM(result.getString("ISHCTM"));
                p.setISHUSER(result.getString("ISHUSER"));

                UAList.add(p);

            }

            ps.close();
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public List<ISMMASH> GetMASHEADByORDWithSEQ(String order, String no, String seq) {

        List<ISMMASH> UAList = new ArrayList<ISMMASH>();

        String sql = "SELECT * FROM ISMMASH WHERE ISHORD = '" + order + "' AND ISHSEQ = '" + seq + "'";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                ISMMASH p = new ISMMASH();

                p.setISHCONO(result.getString("ISHCONO"));
                p.setISHORD(result.getString("ISHORD"));
                p.setISHSEQ(result.getString("ISHSEQ"));
                p.setISHTRDT(result.getString("ISHTRDT"));
                p.setISHUART(result.getString("ISHUART"));
                p.setISHTWEG(result.getString("ISHTWEG"));
                p.setISHMVT(result.getString("ISHMVT"));
                p.setISHCUNO(result.getString("ISHCUNO"));
                p.setISHCUNM1(result.getString("ISHCUNM1"));
                p.setISHCUNM2(result.getString("ISHCUNM2"));
                p.setISHCUAD1(result.getString("ISHCUAD1"));
                p.setISHCUAD2(result.getString("ISHCUAD2"));
                p.setISHBSTKD(result.getString("ISHBSTKD"));
                p.setISHSUBMI(result.getString("ISHSUBMI"));
                p.setISHMATLOT(result.getString("ISHMATLOT"));
                p.setISHTAXNO(result.getString("ISHTAXNO"));
                p.setISHBRANCH01(result.getString("ISHBRANCH01"));
                p.setISHDATUM(result.getString("ISHDATUM"));
                p.setISHSTYLE(result.getString("ISHSTYLE"));
                p.setISHCOLOR(result.getString("ISHCOLOR"));
                p.setISHLOT(result.getString("ISHLOT"));
                p.setISHAMTFG(result.getString("ISHAMTFG"));
                p.setISHSTYLE2(result.getString("ISHSTYLE2"));
                p.setISHNOR(result.getString("ISHNOR"));
                p.setISHPOFG(result.getString("ISHPOFG"));
                p.setISHGRPNO(result.getString("ISHGRPNO"));
                p.setISHWHNO(result.getString("ISHWHNO"));
                p.setISHPGRP(result.getString("ISHPGRP"));
                p.setISHLSTS(result.getString("ISHLSTS"));
                p.setISHHSTS(result.getString("ISHHSTS"));
                p.setISHDEST(result.getString("ISHDEST"));
                p.setISHP1DT(result.getString("ISHP1DT"));
                p.setISHA1DT(result.getString("ISHA1DT"));
                p.setISHT1DT(result.getString("ISHT1DT"));
                p.setISHJBNO(result.getString("ISHJBNO"));
                p.setISHREMARK(result.getString("ISHREMARK"));
                p.setISHCDT(result.getString("ISHCDT"));
                p.setISHCTM(result.getString("ISHCTM"));
                p.setISHUSER(result.getString("ISHUSER"));

                UAList.add(p);

            }

            ps.close();
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public List<ISMSDWD> GetDWNByORDByLine(String order, String no, String line) {

        List<ISMSDWD> UAList = new ArrayList<ISMSDWD>();

        String sql = "SELECT * FROM ISMSDWD WHERE ISDORD = '" + order + "' AND ISDDWQNO = '" + no + "' AND ISDLINO = '" + line + "'";

        try {
//            System.out.println("sql " + sql);

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                ISMSDWD p = new ISMSDWD();

                p.setISDCONO(result.getString("ISDCONO"));
                p.setISDDWQNO(result.getString("ISDDWQNO"));
                p.setISDORD(result.getString("ISDORD"));
                p.setISDSEQ(result.getString("ISDSEQ"));
                p.setISDLINO(result.getString("ISDLINO"));
                p.setISDITNO(result.getString("ISDITNO"));
                p.setISDPLANT(result.getString("ISDPLANT"));
                p.setISDVT(result.getString("ISDVT"));
                p.setISDSTRG(result.getString("ISDSTRG"));
                p.setISDRQQTY(result.getString("ISDRQQTY"));
                p.setISDUNIT(result.getString("ISDUNIT"));
                p.setISDPRICE(result.getString("ISDPRICE"));
                p.setISDUNR01(result.getString("ISDUNR01"));
                p.setISDUNR03(result.getString("ISDUNR03"));
                p.setISDMTCTRL(result.getString("ISDMTCTRL"));
                p.setISDDEST(result.getString("ISDDEST"));
                p.setISDREMK(result.getString("ISDREMK"));

                UAList.add(p);

            }

            ps.close();
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public JSONObject CheckDWNnMASDetail(String order) {

        JSONObject res = new JSONObject();

        try {
            Statement stmtIUD = connect.createStatement();

            String sqlDelSL = "SELECT * FROM ISMSDWD DW\n"
                    + "LEFT JOIN ISMMASD MD ON MD.ISDORD = DW.ISDORD AND MD.ISDLINO = DW.ISDLINO AND MD.ISDITNO = DW.ISDITNO\n"
                    + "WHERE DW.ISDORD = '" + order + "'";
            int DelSL = stmtIUD.executeUpdate(sqlDelSL);

            if (DelSL > 0) {
                res.put("CKVal", true);
            } else {
                res.put("CKVal", false);
            }

            stmtIUD.close();
            connect.close();

        } catch (SQLException ex) {
            Logger.getLogger(ISM800Dao.class.getName()).log(Level.SEVERE, null, ex);
        }

        return res;

    }

//    public boolean updateQTY1QTY3(String ord, String mat, String vt, String qty) {
//
//        boolean result = false;
//
//        String mode = "";
//
//        if (vt.equals("01")) {
//            mode = "ISDUNR01 = " + qty + "";
//        } else if (vt.equals("03")) {
//            mode = "ISDUNR03 = " + qty + "";
//        }
//
//        String sql = "UPDATE [RMShipment].[dbo].[ISMSDWD] SET " + mode + " WHERE ISDORD = '" + ord + "' AND ISDITNO = '" + mat + "' ";
//
//        try {
//            PreparedStatement ps = connect.prepareStatement(sql);
//            int record = ps.executeUpdate();
//
//            if (record >= 1) {
//                result = true;
//            } else {
//                result = false;
//            }
//
//            ps.close();
//            connect.close();
//
//        } catch (Exception e) {
//
//            e.printStackTrace();
//
//        }
//
//        return result;
//
//    }
    public List<ISMMASD> GetMASbySTS0n9(String order, String no) {

        List<ISMMASD> UAList = new ArrayList<ISMMASD>();

        String sql = "SELECT DW.*\n"
                + ",ISNULL(DE.[ISDSTS],'0') AS ISDSTS\n"
                + ",ISNULL(CASE WHEN DE.[ISDSTS] IS NULL THEN 'ขายเพิ่ม' ELSE (CASE WHEN DE.[ISDSTS] != '0' THEN '' ELSE '' END) END,'') AS REASON\n"
                + "FROM ISMSDWD DW\n"
                + "LEFT JOIN ISMMASD DE ON DE.ISDORD = DW.ISDORD AND DE.ISDLINO = DW.ISDLINO\n"
                + "WHERE DW.[ISDORD] = '" + order + "' AND (ISDSTS = 0 OR ISDSTS = 9 OR DE.ISDSTS IS NULL) AND DW.ISDSEQ = (SELECT TOP 1 ISHSEQ FROM ISMSDWH WHERE ISHORD = '" + order + "' AND ISHDWQNO = '" + no + "' ) AND DW.ISDDWQNO = '" + no + "' ";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                ISMMASD p = new ISMMASD();

                p.setISDCONO(result.getString("ISDCONO"));
                p.setISDORD(result.getString("ISDORD"));
                p.setISDSEQ(result.getString("ISDSEQ"));
                p.setISDLINO(result.getString("ISDLINO"));
//                p.setISDSINO(result.getString("ISDSINO"));
                p.setISDITNO(result.getString("ISDITNO"));
                p.setISDPLANT(result.getString("ISDPLANT"));
                p.setISDVT(result.getString("ISDVT"));
                p.setISDSTRG(result.getString("ISDSTRG"));
                p.setISDRQQTY(result.getString("ISDRQQTY"));
                p.setISDUNIT(result.getString("ISDUNIT"));
                p.setISDPRICE(result.getString("ISDPRICE"));
                p.setISDUNR01(result.getString("ISDUNR01"));
                p.setISDUNR03(result.getString("ISDUNR03"));
                p.setISDMTCTRL(result.getString("ISDMTCTRL"));
                p.setISDDEST(result.getString("ISDDEST"));
                p.setISDRQDT(result.getString("ISDRQDT"));
//                p.setISDRQTM(result.getString("ISDRQTM"));
//                p.setISDAPDT(result.getString("ISDAPDT"));
//                p.setISDAPTM(result.getString("ISDAPTM"));
//                p.setISDAPUSER(result.getString("ISDAPUSER"));
//                p.setISDRCDT(result.getString("ISDRCDT"));
//                p.setISDPKQTY(result.getString("ISDPKQTY"));
//                p.setISDPKQTY(result.getString("ISDPKQTY"));
//                p.setISDPKDT(result.getString("ISDPKDT"));
//                p.setISDPKTM(result.getString("ISDPKTM"));
//                p.setISDPKUSER(result.getString("ISDPKUSER"));
//                p.setISDTPQTY(result.getString("ISDTPQTY"));
//                p.setISDTPDT(result.getString("ISDTPDT"));
//                p.setISDTPTM(result.getString("ISDTPTM"));
//                p.setISDTPUSER(result.getString("ISDTPUSER"));
                p.setISDSTS(result.getString("ISDSTS"));
//                p.setISDJBNO(result.getString("ISDJBNO"));
//                p.setISDQNO(result.getString("ISDQNO"));
//                p.setISDREMARK(result.getString("ISDREMARK"));
//                p.setISDISUNO(result.getString("ISDISUNO"));
//                p.setISDTEMP(result.getString("ISDTEMP"));
                p.setISDEDT(result.getString("ISDEDT"));
                p.setISDETM(result.getString("ISDETM"));
                p.setISDEUSR(result.getString("ISDEUSR"));
                p.setISDCDT(result.getString("ISDCDT"));
                p.setISDCTM(result.getString("ISDCTM"));
                p.setISDUSER(result.getString("ISDUSER"));
//                p.setISDSTAD(result.getString("ISDSTAD"));
//                p.setISDSTSTMP(result.getString("ISDSTSTMP"));
//                p.setISDREMAIN(result.getString("ISDREMAIN"));

                UAList.add(p);

            }

            ps.close();
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public boolean updateSL(String ord, String mat, String vt, String qty, String seqNew, String seqOld) {

        boolean result = false;

        String mode = "";

        if (vt == null) {
            mode = "ISDUNR01 = " + qty + "";
        } else {
            if (vt.equals("01")) {
                mode = "ISDUNR01 = " + qty + "";
            } else if (vt.equals("03")) {
                mode = "ISDUNR03 = " + qty + "";
            } else if (vt.equals("null") || vt.equals("")) {
                mode = "ISDUNR01 = " + qty + "";
            }
        }

        String sqlMasH = "UPDATE [RMShipment].[dbo].[ISMMASH] SET ISHLSTS = '0',ISHHSTS = '0' WHERE ISHORD = '" + ord + "' AND ISHSEQ = '" + seqOld + "' ";
        String sqlMas = "UPDATE [RMShipment].[dbo].[ISMMASD] SET " + mode + " ,ISDSEQ = '" + seqNew + "', ISDSTS = '0' WHERE ISDORD = '" + ord + "' AND ISDITNO = '" + mat + "' AND ISDSEQ = '" + seqOld + "' ";

        String sqlDwnH = "UPDATE [RMShipment].[dbo].[ISMSDWH] SET ISHLSTS = '0',ISHHSTS = '0' WHERE ISHORD = '" + ord + "' AND ISHSEQ = '" + seqOld + "'";
        String sqlDwn = "UPDATE [RMShipment].[dbo].[ISMSDWD] SET " + mode + " ,ISDSEQ = '" + seqNew + "' WHERE ISDORD = '" + ord + "' AND ISDITNO = '" + mat + "' AND ISDSEQ = '" + seqOld + "' ";

        try {

            PreparedStatement psMasH = connect.prepareStatement(sqlMasH);
            int recordMasH = psMasH.executeUpdate();
            PreparedStatement psMas = connect.prepareStatement(sqlMas);
            int recordMas = psMas.executeUpdate();

            PreparedStatement psDwnH = connect.prepareStatement(sqlDwnH);
            int recordDwnH = psDwnH.executeUpdate();
            PreparedStatement psDwn = connect.prepareStatement(sqlDwn);
            int recordDwn = psDwn.executeUpdate();

            if (recordDwn >= 1) {
                result = true;
            } else {
                result = false;
            }

            psMasH.close();
            psMas.close();
            psDwnH.close();
            psDwn.close();
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean updateSL2(String ord, String mat, String vt, String qty) {

        boolean result = false;

        String mode = "";

        if (vt == null) {
            mode = "ISDUNR01 = " + qty + " , ISDUNR03 = NULL ";
        } else {
            if (vt.equals("01")) {
                mode = "ISDUNR01 = " + qty + " , ISDUNR03 = NULL ";
            } else if (vt.equals("03")) {
                mode = "ISDUNR03 = " + qty + " , ISDUNR01 = NULL ";
            } else if (vt.equals("null") || vt.equals("")) {
                mode = "ISDUNR01 = " + qty + " , ISDUNR03 = NULL ";
            }
        }

//        String sqlMas = "UPDATE [RMShipment].[dbo].[ISMMASD] SET " + mode + " WHERE ISDORD = '" + ord + "' AND ISDITNO = '" + mat + "' ";
        String sqlDwn = "UPDATE [RMShipment].[dbo].[ISMSDWD] SET " + mode + ", ISDEDT = FORMAT(CURRENT_TIMESTAMP,'yyyyMMdd'), ISDETM = FORMAT(CURRENT_TIMESTAMP,'HHmmss') WHERE ISDORD = '" + ord + "' AND ISDITNO = '" + mat + "' ";

        try {

//            PreparedStatement psMas = connect.prepareStatement(sqlMas);
//            int recordMas = psMas.executeUpdate();
            PreparedStatement psDwn = connect.prepareStatement(sqlDwn);
            int recordDwn = psDwn.executeUpdate();

            if (recordDwn >= 1) {
                result = true;
            } else {
                result = false;
            }

//            psMas.close();
            psDwn.close();
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean updateSL3(String ord, String seqNew, String seqOld, String dwnno, String no) {

        boolean result = false;

//        String ooo = "INSERT INTO ISMMASH ([ISHCONO],[ISHORD],[ISHSEQ],[ISHTRDT],[ISHUART],[ISHTWEG],[ISHMVT],[ISHCUNO],[ISHCUNM1],[ISHCUNM2],[ISHCUAD1],[ISHCUAD2],[ISHBSTKD],[ISHSUBMI],[ISHMATLOT],[ISHTAXNO],[ISHBRANCH01],[ISHDATUM],[ISHSTYLE],[ISHCOLOR],[ISHLOT],[ISHAMTFG],[ISHSTYLE2],[ISHNOR],[ISHPOFG],[ISHGRPNO],[ISHWHNO],[ISHPGRP],[ISHLSTS],[ISHHSTS],[ISHDEST],[ISHRQDT],[ISHRQTM],[ISHAPDT],[ISHAPTM],[ISHAPUSR],[ISHRCDT],[ISHRCTM],[ISHRCUSR],[ISHPKDT],[ISHPKTM],[ISHPKUSR],[ISHTPDT],[ISHTPTM],[ISHTPUSR],[ISHP1DT],[ISHA1DT],[ISHT1DT],[ISHJBNO],[ISHREMARK],[ISHEDT],[ISHETM],[ISHEUSR],[ISHCDT],[ISHCTM],[ISHUSER])\n"
//                + "        SELECT [ISHCONO],[ISHORD]," + seqNew + ",FORMAT(CURRENT_TIMESTAMP,'yyyyMMdd'),[ISHUART],[ISHTWEG],[ISHMVT],[ISHCUNO],[ISHCUNM1],[ISHCUNM2],[ISHCUAD1],[ISHCUAD2],[ISHBSTKD],[ISHSUBMI],[ISHMATLOT],[ISHTAXNO],[ISHBRANCH01],[ISHDATUM],[ISHSTYLE],[ISHCOLOR],[ISHLOT],[ISHAMTFG],[ISHSTYLE2],[ISHNOR],[ISHPOFG],[ISHGRPNO],[ISHWHNO],[ISHPGRP],'0','0',[ISHDEST],NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,[ISHREMARK],[ISHEDT],[ISHETM],[ISHEUSR],[ISHCDT],[ISHCTM],[ISHUSER] \n"
//                + "        FROM ISMMASH WHERE ISHORD = '" + ord + "' \n";
        String ooo2 = "INSERT INTO ISMSDWH ([ISHCONO],[ISHDWQNO],[ISHORD],[ISHSEQ],[ISHTRDT],[ISHUART],[ISHSORG],[ISHTWEG],[ISHDIVI],[ISHSGRP],[ISHSOFF],[ISHMVT],[ISHCUNO],[ISHCUNM1],[ISHCUNM2],[ISHCUAD1],[ISHCUAD2],[ISHBSTKD],[ISHSUBMI],[ISHMATLOT],[ISHDDATE],[ISHDTIME],[ISHTAXNO],[ISHBRANCH01],[ISHSTYLE],[ISHCOLOR],[ISHLOT],[ISHAMTFG],[ISHSTYLE2],[ISHNOR],[ISHPOFG],[ISHWHNO],[ISHPGRP],[ISHLSTS],[ISHHSTS],[ISHDEST],[ISHREMK],[ISHCUNO2],[ISHDLDT],[ISHDLS],[ISHODLS],[ISHCRDT],[ISHCRTM],[ISHSUSER],[ISHEDT],[ISHETM],[ISHEUSR],[ISHCDT],[ISHCTM],[ISHUSER])\n"
                + "        SELECT [ISHCONO]," + dwnno + ",[ISHORD]," + seqNew + ",FORMAT(CURRENT_TIMESTAMP,'yyyyMMdd'),[ISHUART],[ISHSORG],[ISHTWEG],[ISHDIVI],[ISHSGRP],[ISHSOFF],[ISHMVT],[ISHCUNO],[ISHCUNM1],[ISHCUNM2],[ISHCUAD1],[ISHCUAD2],[ISHBSTKD],[ISHSUBMI],[ISHMATLOT],[ISHDDATE],[ISHDTIME],[ISHTAXNO],[ISHBRANCH01],[ISHSTYLE],[ISHCOLOR],[ISHLOT],[ISHAMTFG],[ISHSTYLE2],(SELECT TOP 1 COUNT(1) FROM [RMShipment].[dbo].[ISMSDWD] WHERE ISDORD = '" + ord + "' AND ISDSEQ = '" + seqNew + "'),[ISHPOFG],[ISHWHNO],[ISHPGRP],[ISHLSTS],[ISHHSTS],[ISHDEST],[ISHREMK],[ISHCUNO2],[ISHDLDT],[ISHDLS],[ISHODLS],[ISHCRDT],[ISHCRTM],[ISHSUSER],[ISHEDT],[ISHETM],[ISHEUSR],[ISHCDT],[ISHCTM],[ISHUSER] \n"
                //                + "        SELECT [ISHCONO]," + dwnno + ",[ISHORD]," + seqNew + ",FORMAT(CURRENT_TIMESTAMP,'yyyyMMdd'),[ISHUART],[ISHSORG],[ISHTWEG],[ISHDIVI],[ISHSGRP],[ISHSOFF],[ISHMVT],[ISHCUNO],[ISHCUNM1],[ISHCUNM2],[ISHCUAD1],[ISHCUAD2],[ISHBSTKD],[ISHSUBMI],[ISHMATLOT],[ISHDDATE],[ISHDTIME],[ISHTAXNO],[ISHBRANCH01],[ISHSTYLE],[ISHCOLOR],[ISHLOT],[ISHAMTFG],[ISHSTYLE2],[ISHNOR],[ISHPOFG],[ISHWHNO],[ISHPGRP],[ISHLSTS],[ISHHSTS],[ISHDEST],[ISHREMK],[ISHCUNO2],[ISHDLDT],[ISHDLS],[ISHODLS],[ISHCRDT],[ISHCRTM],[ISHSUSER],[ISHEDT],[ISHETM],[ISHEUSR],[ISHCDT],[ISHCTM],[ISHUSER] \n"
                + "        FROM ISMSDWH WHERE ISHORD = '" + ord + "' AND ISHDWQNO = '" + no + "' \n";
        try {

//            PreparedStatement psMasH = connect.prepareStatement(ooo);
//            int recordMasH = psMasH.executeUpdate();
            PreparedStatement psDwnH = connect.prepareStatement(ooo2);
            int recordDwnH = psDwnH.executeUpdate();

            if (recordDwnH > 0) {
                result = true;
            }

//            psMasH.close();
            psDwnH.close();
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public String selectSTSMasHead(String orderno, String seq) {

        String sts = "";

//        String sql = "SELECT TOP 1 ISHLSTS FROM [RMShipment].[dbo].[ISMMASH] WHERE ISHORD = '" + orderno + "' ORDER BY ISHSEQ DESC";
        String sql = "SELECT TOP 1 ISNULL(MAX(ISHLSTS),0) AS ISHLSTS FROM [RMShipment].[dbo].[ISMMASH] WHERE ISHORD = '" + orderno + "' AND ISHSEQ = '" + seq + "'";

        try {
            Statement stmtS = connect.createStatement();
            ResultSet rs = stmtS.executeQuery(sql);

            if (rs != null) {
                while (rs.next()) {
                    sts = rs.getString("ISHLSTS");
                }
            }

            stmtS.close();
            connect.close();

        } catch (SQLException ex) {
            Logger.getLogger(ISM800Dao.class.getName()).log(Level.SEVERE, null, ex);
        }

        return sts;

    }

    public String findEditDetailByORDForEdit(String order, String no, String dt) {

        String sumSTS = "0";

        String sql = "SELECT ISNULL(DE.[ISDSTS],'0') AS ISDSTS\n"
                + "FROM ISMSDWD DW\n"
                + "LEFT JOIN ISMMASD DE ON DE.ISDORD = DW.ISDORD AND DE.ISDLINO = DW.ISDLINO\n"
                + "WHERE DW.[ISDORD] = '" + order + "' AND DW.[ISDDWQNO] = '" + no + "' ";

        try {
//            System.out.println(sql);
            PreparedStatement ps = connect.prepareStatement(sql);
            ResultSet result = ps.executeQuery();

            int sts = 0;

            while (result.next()) {
                sts += result.getInt("ISDSTS");
            }

            sumSTS = Integer.toString(sts);

            ps.close();
            connect.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return sumSTS;

    }

}
