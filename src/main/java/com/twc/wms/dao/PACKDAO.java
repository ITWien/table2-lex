/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.dao;

import com.twc.wms.database.database;
import com.twc.wms.entity.MPDATAP;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author nutthawoot.noo
 */
public class PACKDAO extends database {

    public void addMSSPACK(String GRPNO, int ISSUENO, String MATNR, int NO, String TYPE, float VALUE, String MTCTRL, String IDITEM) {

        try {
            //System.out.println("typ="+TYPE) ;
            //PreparedStatement statement = connect.prepareStatement("insert into MSSPACK(GRPNO, ISSUENO, MATNR, NO, TYPE, VALUE) values (?, ?, ?, ?, ?, ?)");
            PreparedStatement statement = connect.prepareStatement("insert into ISMPACK(GRPNO, ISSUENO, MATNR, NO, TYPE, VALUE,MTCTRL,MATID) values (?, ?, ?, ?, ?, ?, ?, ?)");  //new mod
            statement.setString(1, GRPNO);
            statement.setInt(2, ISSUENO);
            statement.setString(3, MATNR);
            statement.setInt(4, NO);
            statement.setString(5, TYPE);
            statement.setFloat(6, VALUE);
            statement.setString(7, MTCTRL); //new mod
            statement.setString(8, IDITEM); //new mod
            statement.executeUpdate();
            statement.close();
            //connect.close();
        } catch (SQLException err) {
            err.printStackTrace();
        }

    }

    public void updateHeadpack(String GRPNO, int ISSUENO, String MATNR, float VALUE) {

        try {
            //PreparedStatement statement = connect.prepareStatement("insert into MSSPACK(GRPNO, ISSUENO, MATNR, NO, TYPE, VALUE) values (?, ?, ?, ?, ?, ?)");
            PreparedStatement statement = connect.prepareStatement("UPDATE MSSTOPK SET ISSUETOPICK=(ISSUETOPICK+?) WHERE GRPNO=? AND ISSUENO=? AND MATNR=? ");  //new mod
            statement.setFloat(1, VALUE);
            statement.setString(2, GRPNO);
            statement.setInt(3, ISSUENO);
            statement.setString(4, MATNR);
            statement.executeUpdate();
            statement.close();
            //connect.close();
        } catch (SQLException err) {
            err.printStackTrace();
        }

    }

    public void updateHeadpackGroup(String GRPNO, int ISSUENO, String MATNR) {

        try {

            PreparedStatement statement = connect.prepareStatement("UPDATE ISMTOPK SET ISSUETOPICK=0 WHERE GRPNO=? AND ISSUENO=? AND MATNR=? ");  //new mod
            statement.setString(1, GRPNO);
            statement.setInt(2, ISSUENO);
            statement.setString(3, MATNR);
            statement.executeUpdate();
            statement.close();
            //connect.close();
        } catch (SQLException err) {
            err.printStackTrace();
        }

    }

    public void updateHeadpackLine(String GRPNO, int ISSUENO, float qtyid, String MATNR) {

        try {

            PreparedStatement statement = connect.prepareStatement("UPDATE ISMTOPK SET ISSUETOPICK=(ISSUETOPICK-?) WHERE GRPNO=? AND ISSUENO=? AND MATNR=? ");  //new mod
            statement.setFloat(1, qtyid);
            statement.setString(2, GRPNO);
            statement.setInt(3, ISSUENO);
            statement.setString(4, MATNR);
            statement.executeUpdate();
            statement.close();
            //connect.close();
        } catch (SQLException err) {
            err.printStackTrace();
        }

    }

    public void addMSSPACD(String GRPNO, int ISSUENO, String VBELN, String POSNR, String MATNR, int NO, String TYPE, float VALUE) {

        try {
            PreparedStatement statement = connect.prepareStatement("insert into ISMPACD(GRPNO, ISSUENO, VBELN, POSNR, MATNR, NO, TYPE, VALUE) values (?, ?, ?, ?, ?, ?, ?, ?)");
            statement.setString(1, GRPNO);
            statement.setInt(2, ISSUENO);
            statement.setString(3, VBELN);
            statement.setString(4, POSNR);
            statement.setString(5, MATNR);
            statement.setInt(6, NO);
            statement.setString(7, TYPE);
            statement.setFloat(8, VALUE);
            statement.executeUpdate();
        } catch (SQLException err) {
            err.printStackTrace();
        }

    }

    public void deleteMSSPACK(String GRPNO, int ISSUENO, String MATNR) {

        try {
            PreparedStatement statement = connect.prepareStatement("delete ISMPACK where GRPNO=? and ISSUENO=? and MATNR=?");
            statement.setString(1, GRPNO);
            statement.setInt(2, ISSUENO);
            statement.setString(3, MATNR);
            statement.executeUpdate();
        } catch (SQLException err) {
            err.printStackTrace();
        }

    }

    public void deleteMSSPACKLine(String GRPNO, int ISSUENO, String MATNRid) {

        try {
            PreparedStatement statement = connect.prepareStatement("delete ISMPACK where GRPNO=? and ISSUENO=? and MATID=?");
            statement.setString(1, GRPNO);
            statement.setInt(2, ISSUENO);
            statement.setString(3, MATNRid);
            statement.executeUpdate();
        } catch (SQLException err) {
            err.printStackTrace();
        }

    }

    public void deleteMSSPACD(String GRPNO, int ISSUENO, String MATNR) {

        try {
            PreparedStatement statement = connect.prepareStatement("delete ISMPACD where GRPNO=? and ISSUENO=? and MATNR=?");
            statement.setString(1, GRPNO);
            statement.setInt(2, ISSUENO);
            statement.setString(3, MATNR);
            statement.executeUpdate();
        } catch (SQLException err) {
            err.printStackTrace();
        }

    }

    public void deleteMSSPACDLine(String GRPNO, int ISSUENO, String MATNR, int LINENO) {

        try {
            PreparedStatement statement = connect.prepareStatement("delete ISMPACD where GRPNO=? and ISSUENO=? and MATNR=? and NO=? ");
            statement.setString(1, GRPNO);
            statement.setInt(2, ISSUENO);
            statement.setString(3, MATNR);
            statement.setInt(4, LINENO);

            statement.executeUpdate();
        } catch (SQLException err) {
            err.printStackTrace();
        }

    }

    public String getTypepack(String MATNRID, String MATNR) {

        String gettype = "";

        try {

            PreparedStatement statement = connect.prepareStatement(" SELECT QRMPACKTYPE FROM QRMMAS WHERE QRMID=?");
//            PreparedStatement statement = connect.prepareStatement(" exec dbo.getTypepack ? ");
            statement.setString(1, MATNRID);
            //statement.setString(2, MATNR);
            ResultSet set = statement.executeQuery();

            if (set.next()) {
                gettype = set.getString("QRMPACKTYPE");
                //System.out.println("id="+gettype);

            }
            set.close();
            statement.close();

        } catch (SQLException err) {
            err.printStackTrace();
        }

        return gettype;

    }

    public String getqrSts(String MATNRID) {

        String getsts = "";

        try {
            // edit for call store procedure
            PreparedStatement statement = connect.prepareStatement("SELECT QRMSTS FROM QRMMAS WHERE QRMID=? AND QRMPLANT='1000' ");
//            PreparedStatement statement = connect.prepareStatement(" exec dbo.getqrSts ? ");

            statement.setString(1, MATNRID);
            ResultSet set = statement.executeQuery();

            if (set.next()) {
                getsts = set.getString("QRMSTS");

            }
            set.close();
            statement.close();

        } catch (SQLException err) {
            err.printStackTrace();
        }

        return getsts;

    }

    public String getqriditem(String barcodeqr) {

        String getid = "";
        //System.out.println("barcodeqr="+barcodeqr);  //QRMPLANT=1050 AND

        try {
            PreparedStatement statement = connect.prepareStatement("SELECT QRMID FROM QRMMAS WHERE  QRMBARCODE=? ");
//            PreparedStatement statement = connect.prepareStatement("exec dbo.getqriditem ? ");

            //statement.setString(1, GRPNO);
            //statement.setString(2, ISSUENO);
            statement.setString(1, barcodeqr);

            ResultSet set = statement.executeQuery();

            if (set.next()) {
                getid = set.getString("QRMID");

            }
            set.close();
            statement.close();

        } catch (SQLException err) {
            err.printStackTrace();
        }

        return getid;

    }

    public String getqrId(String GRPNO, String ISSUENO, String MATNR) {

        String getid = "";

        try {
            PreparedStatement statement = connect.prepareStatement("SELECT MATID FROM MSSPACK WHERE GRPNO=? AND ISSUENO=? AND MATNR=? ");
            statement.setString(1, GRPNO);
            statement.setString(2, ISSUENO);
            statement.setString(3, MATNR);

            ResultSet set = statement.executeQuery();

            if (set.next()) {
                getid = set.getString("MATID");

            }
            set.close();
            statement.close();

        } catch (SQLException err) {
            err.printStackTrace();
        }

        return getid;

    }

    public void upqrstatusID(String MATNRID, String STATUS) {

        try {
            PreparedStatement statement = connect.prepareStatement("UPDATE QRMMAS SET QRMSTS=? WHERE QRMID=?");
//            PreparedStatement statement = connect.prepareStatement(" exec dbo.upqrstatusID ?,? ");

            statement.setString(1, STATUS);
            statement.setString(2, MATNRID);
            statement.executeUpdate();
            statement.close();

        } catch (SQLException err) {
            err.printStackTrace();
        }

    }

    public void upqrstsGroup(String GRPNO, int ISSUENO, String MATNR, String STATUS) {

        try {
            PreparedStatement statement = connect.prepareStatement(" UPDATE QRMMAS SET QRMSTS=? FROM ISMPACK  INNER JOIN QRMMAS ON QRMID=MATID WHERE GRPNO=? AND ISSUENO=? AND MATNR=? ");
            statement.setString(1, STATUS);
            statement.setString(2, GRPNO);
            statement.setInt(3, ISSUENO);
            statement.setString(4, MATNR);
            statement.executeUpdate();
            statement.close();

        } catch (SQLException err) {
            err.printStackTrace();
        }

    }

    public void upqrstsLine(String GRPNO, int ISSUENO, String MATNRid, String STATUS) {

        try {
            PreparedStatement statement = connect.prepareStatement(" UPDATE QRMMAS SET QRMSTS=? FROM ISMPACK  INNER JOIN QRMMAS ON QRMID=MATID WHERE GRPNO=? AND ISSUENO=? AND MATID=? ");
            statement.setString(1, STATUS);
            statement.setString(2, GRPNO);
            statement.setInt(3, ISSUENO);
            statement.setString(4, MATNRid);
            statement.executeUpdate();
            statement.close();

        } catch (SQLException err) {
            err.printStackTrace();
        }

    }

    public int getNo(String GRPNO, int ISSUENO, String MATNR) {

        int NO = 0;

        try {
            PreparedStatement statement = connect.prepareStatement("select MAX(NO) as NO from ISMPACK where GRPNO=? and ISSUENO=? and MATNR=?");
            statement.setString(1, GRPNO);
            statement.setInt(2, ISSUENO);
            statement.setString(3, MATNR);
            ResultSet set = statement.executeQuery();

            if (set.next()) {
                NO = set.getInt("NO");
            }

        } catch (SQLException err) {
            err.printStackTrace();
        }

        return NO;

    }

    public float getISSUETOPACK(String GRPNO, int ISSUENO, String MATNR) {

        float VALUE = 0.000f;

        try {
            PreparedStatement statement = connect.prepareStatement("select SUM(VALUE) as VALUE from ISMPACK where GRPNO=? and ISSUENO=? and MATNR=?");
            statement.setString(1, GRPNO);
            statement.setInt(2, ISSUENO);
            statement.setString(3, MATNR);
            ResultSet set = statement.executeQuery();

            if (set.next()) {
                VALUE = set.getFloat("VALUE");
            }

        } catch (SQLException err) {
            err.printStackTrace();
        }

        return VALUE;

    }

    public void addTranqr(String SWGRPST, String SWCARNO, String SWTRUCK, String userget) {

        try {
            //	System.out.println("sql=INSERT INTO QRMTRA (QRMCOM,QRMWHSE,QRMCODE,QRMVAL,QRMPLANT,QRMSTORAGE,QRMID,QRMROLL,QRMTRDT,QRMTRTM,QRMMVT,QRMTRT,QRMDOCNO,QRMSAPNO,QRMQTY,QRMBUN,QRMCDT,QRMUSER) " +
            //   	" SELECT 'TWC','WH2',QRMCODE,QRMVAL,QRMPLANT,QRMSTORAGE,QRMID,QRMROLL,convert(date,GETDATE(),102),CONVERT (time, SYSDATETIME()),'601','MWC',VBELN,'"+SWGRPST+"',QRMQTY,QRMBUN,convert(date,GETDATE(),102),'TWC' FROM " +
            // " ( SELECT QRMCODE,QRMVAL,QRMPLANT,QRMSTORAGE,QRMID,QRMROLL,QRMQTY,QRMBUN FROM SWGLINE INNER JOIN QRMMAS ON SWIDNO=QRMID AND SWIVNO='"+SWGRPST+"' AND SWCARNO='"+SWCARNO+"' AND SWITEM<>'') C " +
            //" INNER JOIN (SELECT A.VBELN,B.MATNR from MSSMASH A INNER JOIN MSSMASD B on A.VBELN=B.VBELN WHERE GRPNO='?' ) ");
            //PreparedStatement statement = connect.prepareStatement("insert into MSSPACK(GRPNO, ISSUENO, MATNR, NO, TYPE, VALUE) values (?, ?, ?, ?, ?, ?)");
            // PreparedStatement statement = connect.prepareStatement("insert into MSSPACK(GRPNO, ISSUENO, MATNR, NO, TYPE, VALUE,MTCTRL,MATID) values (?, ?, ?, ?, ?, ?, ?, ?)");  //new mod

            PreparedStatement statement = connect.prepareStatement(" INSERT INTO QRMTRA (QRMCOM,QRMWHSE,QRMCODE,QRMVAL,QRMPLANT,QRMSTORAGE,QRMID,QRMROLL,QRMTRDT,QRMTRTM,QRMMVT,QRMTRT,QRMDOCNO,QRMSAPNO,QRMQTY,QRMBUN,QRMCDT,QRMUSER) "
                    + " SELECT 'TWC',QRMWHSE,QRMCODE,QRMVAL,QRMPLANT,QRMSTORAGE,QRMID,QRMROLL,convert(date,GETDATE(),102),CONVERT (time, SYSDATETIME()),'601','MWC',VBELN,'" + SWGRPST + "',QRMQTY,QRMBUN,SYSDATETIME(),'" + userget + "' FROM "
                    + " ( SELECT QRMWHSE,QRMCODE,QRMVAL,QRMPLANT,QRMSTORAGE,QRMID,QRMROLL,QRMQTY,QRMBUN FROM SWGLINE INNER JOIN QRMMAS ON SWIDNO=QRMID AND SWIVNO='" + SWGRPST + "' AND SWCARNO='" + SWCARNO + "' AND SWITEM<>'') C "
                    + " INNER JOIN (SELECT A.VBELN,B.MATNR from MSSMASH A INNER JOIN MSSMASD B on A.VBELN=B.VBELN WHERE GRPNO=?  ) D  ON C.QRMCODE=D.MATNR  ");

            statement.setString(1, SWGRPST);
            //    statement.setInt(2, ISSUENO);
            //  statement.setString(3, MATNR);
            // 	statement.setInt(4, NO);
            // statement.setString(5, TYPE);
            // statement.setFloat(6, VALUE);
            // statement.setString(7, MTCTRL); //new mod
            // statement.setString(8, IDITEM); //new mod
            statement.executeUpdate();
            statement.close();
            //connect.close();
        } catch (SQLException err) {
            err.printStackTrace();
        }

    }

    public List<MPDATAP> getList(String GRPNO, int ISSUENO, String MATNR) {

        List<MPDATAP> listMPDATAP = new ArrayList<MPDATAP>();

        try {

            PreparedStatement statement = connect.prepareStatement("select * from ISMPACK where GRPNO=? and ISSUENO=? and MATNR=? order by NO");
            statement.setString(1, GRPNO);
            statement.setInt(2, ISSUENO);
            statement.setString(3, MATNR);;
            ResultSet set = statement.executeQuery();

            while (set.next()) {
                MPDATAP MPDATAP = new MPDATAP();
                MPDATAP.setNO(set.getInt("NO"));
                MPDATAP.setTYPE(set.getString("TYPE"));
                MPDATAP.setVALUE(set.getFloat("VALUE"));
                MPDATAP.setMATID(set.getString("MATID"));
                listMPDATAP.add(MPDATAP);
            }

        } catch (SQLException err) {
            err.printStackTrace();
        }

        return listMPDATAP;

    }

    public List<MPDATAP> getListDetail(String GRPNO, int ISSUENO, String MATNR) {

        List<MPDATAP> listMPDATAP = new ArrayList<MPDATAP>();

        try {

            PreparedStatement statement = connect.prepareStatement("declare @JBNO nvarchar(50) = ?\n"
                    + "declare @SEQ nvarchar(50) = ?\n"
                    + "declare @ITNO nvarchar(50) = ?\n"
                    + "SELECT \n"
                    + "VBELN, STYLE, COLOR, LOT, \n"
                    + "KWMENG, NO, TYPE, VALUE\n"
                    + "from(\n"
                    + "SELECT [ISDORD] as VBELN\n"
                    + ",'' as STYLE\n"
                    + ",'' as COLOR\n"
                    + ",'' as LOT \n"
                    + ",(select sum([ISDRQQTY]) FROM [ISMMASD]\n"
                    + "where [ISDJBNO] = @JBNO\n"
                    + "and [ISDSEQ] = @SEQ\n"
                    + "and [ISDITNO] = @ITNO\n"
                    + "and [ISDORD] = DD.[ISDORD]) as KWMENG\n"
                    + ",cast([ISDLINO] as int) as NO\n"
                    + ",[ISDUNIT] as TYPE\n"
                    + ",(select SUM(VALUE)\n"
                    + "  from ISMPACK where GRPNO=@JBNO and ISSUENO=@SEQ and MATNR=@ITNO) as VALUE\n"
                    + "FROM [ISMMASD] DD\n"
                    + "where [ISDJBNO] = @JBNO\n"
                    + "and [ISDSEQ] = @SEQ\n"
                    + "and [ISDITNO] = @ITNO\n"
                    + ")TMP\n"
                    + "group by VBELN, STYLE, COLOR, LOT, KWMENG, NO, TYPE, VALUE \n"
                    + "order by VBELN, NO");
            statement.setString(1, GRPNO);
            statement.setInt(2, ISSUENO);
            statement.setString(3, MATNR);
            ResultSet set = statement.executeQuery();

            while (set.next()) {
                MPDATAP MPDATAP = new MPDATAP();
                MPDATAP.setVBELN(set.getString("VBELN"));
                MPDATAP.setSTYLE(set.getString("STYLE"));
                MPDATAP.setCOLOR(set.getString("COLOR"));
                MPDATAP.setLOT(set.getString("LOT"));
                MPDATAP.setKWMENG(set.getFloat("KWMENG"));
                MPDATAP.setNO(set.getInt("NO"));
                MPDATAP.setTYPE(set.getString("TYPE"));
                MPDATAP.setVALUE(set.getFloat("VALUE"));
                listMPDATAP.add(MPDATAP);
            }

        } catch (SQLException err) {
            err.printStackTrace();
        }

        return listMPDATAP;

    }

}
