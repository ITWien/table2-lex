/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.dao;

import com.twc.wms.database.database;
import com.twc.wms.entity.QRKDETAIL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;
import java.io.FileOutputStream;
import java.io.IOException;
import com.itextpdf.text.Font.FontFamily;

/**
 *
 * @author nutthawoot.noo
 */
public class QRKDETAILDao extends database {

    public List<QRKDETAIL> findLoc(String wh, String srkno, String frkno, String sside, String fside, String scol, String fcol, String srow, String frow) {

        List<QRKDETAIL> loc = new ArrayList<QRKDETAIL>();

        String side = "";
        if (sside.equals("") && fside.equals("R")) {
            side = "AND QRKSIDE = '" + fside + "'";
        } else if (sside.equals("L") && fside.equals("")) {
            side = "AND QRKSIDE = '" + sside + "'";
        }

        String sql = "SELECT * FROM QRKDETAIL "
                + "WHERE QRKWHS = '" + wh + "' "
                + "AND QRKNO between '" + srkno + "' and '" + frkno + "' "
                + side
                + "AND QRKCOL between '" + scol + "' and '" + fcol + "' "
                + "AND QRKROW between '" + srow + "' and '" + frow + "' "
                + "ORDER BY QRKLOC ";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                QRKDETAIL p = new QRKDETAIL();
                p.setRkno(result.getString("QRKLOC"));

                loc.add(p);

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return loc;

    }

    public List<QRKDETAIL> findAll(String wh) {

        List<QRKDETAIL> QdestList = new ArrayList<QRKDETAIL>();

        String sql = "SELECT * FROM QRKDETAIL WHERE QRKWHS = '" + wh + "' ORDER BY QRKLOC";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                QRKDETAIL p = new QRKDETAIL();

                p.setZone1(result.getString("QRKWHS"));

                p.setZone(result.getString("QRKAREA"));

                p.setRkno(result.getString("QRKNO"));

                p.setSide(result.getString("QRKSIDE"));

                p.setColumn(result.getString("QRKCOL"));

                p.setRow(result.getString("QRKROW"));

                QdestList.add(p);

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return QdestList;

    }

    public String check(String wh, String zone, String loc) {
        PreparedStatement ps = null;
        ResultSet rs = null;
        String STRRETURN = null;
        try {
            String check = "Select * FROM [RMShipment].[dbo].[QRKDETAIL] where [QRKWHS] = '" + wh + "' "
                    + "AND [QRKAREA] = '" + zone + "' AND [QRKLOC] = '" + loc + "'";
            ps = connect.prepareStatement(check);
            rs = ps.executeQuery();
            if (rs.next()) {
                STRRETURN = "f";
            } else {
                STRRETURN = "t";
            }
        } catch (Exception exp) {
            exp.printStackTrace();
        }
        return STRRETURN;
    }

    public boolean add(QRKDETAIL qdest, String uid) {

        boolean result = false;

        String sql = "INSERT INTO QRKDETAIL "
                + "(QRKCOM, QRKWHS, QRKAREA, QRKNO, QRKSIDE, QRKCOL, "
                + "QRKROW, QRKLOC, QRKEDT, QRKCDT, QRKUSER)"
                + " VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ps.setString(1, "TWC");

            ps.setString(2, qdest.getZone1());

            ps.setString(3, qdest.getZone());

            ps.setString(4, qdest.getRkno());

            ps.setString(5, qdest.getSide());

            ps.setString(6, qdest.getColumn());

            ps.setString(7, qdest.getRow());

            String loc = qdest.getRkno() + qdest.getSide() + qdest.getColumn() + qdest.getRow();

            ps.setString(8, loc);

            Calendar cal = Calendar.getInstance();
            Timestamp timestamp = new Timestamp(cal.getTimeInMillis());

            ps.setTimestamp(9, timestamp);

            ps.setTimestamp(10, timestamp);

            ps.setString(11, uid);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public QRKDETAIL findByCod(String wh, String zone, String loc) {

        QRKDETAIL qd = new QRKDETAIL();

        String sql = "Select * FROM [RMShipment].[dbo].[QRKDETAIL] where [QRKWHS] = '" + wh + "' "
                + "AND [QRKAREA] = '" + zone + "' AND [QRKLOC] = '" + loc + "'";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                qd.setZone1(result.getString("QRKWHS"));

                qd.setZone(result.getString("QRKAREA"));

                qd.setRkno(result.getString("QRKNO"));

                qd.setSide(result.getString("QRKSIDE"));

                qd.setColumn(result.getString("QRKCOL"));

                qd.setRow(result.getString("QRKROW"));

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return qd;

    }

    public void delete(String wh, String zone, String loc) {

        String sql = "DELETE FROM QRKDETAIL "
                + "WHERE QRKWHS = ? AND QRKAREA = ? AND QRKLOC = ?";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ps.setString(1, wh);

            ps.setString(2, zone);

            ps.setString(3, loc);

            ps.executeUpdate();

        } catch (Exception e) {

            e.printStackTrace();

        }

    }
}
