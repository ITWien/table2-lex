/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.dao;

import com.twc.wms.database.database;
import com.twc.wms.entity.QRKHEAD;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 *
 * @author nutthawoot.noo
 */
public class QRKHEADDao extends database {

    public List<QRKHEAD> findAll() {

        List<QRKHEAD> QdestList = new ArrayList<QRKHEAD>();

        String sql = "SELECT * FROM QRKHEAD "
                + "ORDER BY QRKRKNO";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                QRKHEAD p = new QRKHEAD();

                p.setCode(result.getString("QRKWHS"));

                p.setDesc(result.getString("QRKRKNO"));

                p.setName(result.getString("QRKAREA"));

                QdestList.add(p);

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return QdestList;

    }

    public String check(String code, String wh) {
        PreparedStatement ps = null;
        ResultSet rs = null;
        String STRRETURN = null;
        try {
            String check = "Select * FROM [RMShipment].[dbo].[QRKHEAD] where [QRKRKNO] = '" + code + "' "
                    + "AND [QRKWHS] = '" + wh + "'";
            ps = connect.prepareStatement(check);
            rs = ps.executeQuery();
            if (rs.next()) {
                STRRETURN = "f";
            } else {
                STRRETURN = "t";
            }
        } catch (Exception exp) {
            exp.printStackTrace();
        }
        return STRRETURN;
    }

    public boolean add(QRKHEAD qdest, String uid) {

        boolean result = false;

        String sql = "INSERT INTO QRKHEAD"
                + " (QRKCOM, QRKWHS, QRKRKNO, QRKAREA, QRKEDT, QRKCDT, QRKUSER)"
                + " VALUES(?, ?, ?, ?, ?, ?, ?)";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ps.setString(1, "TWC");

            ps.setString(2, qdest.getCode());

            ps.setString(3, qdest.getDesc());

            ps.setString(4, qdest.getName());

            Calendar cal = Calendar.getInstance();
            Timestamp timestamp = new Timestamp(cal.getTimeInMillis());

            ps.setTimestamp(5, timestamp);

            ps.setTimestamp(6, timestamp);

            ps.setString(7, uid);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public QRKHEAD findByCod(String wh, String rkno) {

        QRKHEAD qd = new QRKHEAD();

        String sql = "SELECT * FROM QRKHEAD WHERE QRKWHS = ? AND QRKRKNO = ? ";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ps.setString(1, wh);

            ps.setString(2, rkno);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                qd.setCode(result.getString("QRKWHS"));

                qd.setDesc(result.getString("QRKRKNO"));

                qd.setName(result.getString("QRKAREA"));

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return qd;

    }

    public boolean edit(QRKHEAD qdest, String uid) {

        boolean result = false;

        String sql = "UPDATE QRKHEAD SET QRKAREA = ?, QRKEDT = ?, QRKUSER = ?  "
                + "WHERE QRKWHS = ? AND QRKRKNO = ?";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ps.setString(4, qdest.getCode());

            ps.setString(5, qdest.getDesc());

            ps.setString(1, qdest.getName());

            Calendar cal = Calendar.getInstance();
            Timestamp timestamp = new Timestamp(cal.getTimeInMillis());
            ps.setTimestamp(2, timestamp);

            ps.setString(3, uid);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public void delete(String wh, String rkno) {

        String sql = "DELETE FROM QRKHEAD "
                + "WHERE QRKWHS = ? AND QRKRKNO = ?";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ps.setString(1, wh);

            ps.setString(2, rkno);

            ps.executeUpdate();

        } catch (Exception e) {

            e.printStackTrace();

        }

    }
}
