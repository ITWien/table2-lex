/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.dao;

import com.twc.wms.database.database;

import java.sql.*;

import java.util.ArrayList;

import java.util.List;

import com.twc.wms.entity.WMS971;
import com.twc.wms.entity.SAPMAS;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import com.twc.wms.database.MSSQLDbConnectionPool;
import com.twc.wms.entity.Qdest;

/**
 *
 * @author nutthawoot.noo
 */
public class WMS971Dao extends database {

    public List<WMS971> getOnhand(String code, String plant) {

        List<WMS971> objList = new ArrayList<WMS971>();

        String sql = "SELECT [QRMCODE]\n"
                + "      ,[QRMDESC]\n"
                + "	  ,(select top 1 [SAPMGRPDESC] from [SAPMAS] where [SAPMAT]=[QRMCODE] and [SAPPLANT]=[QRMPLANT] and [SAPVAL]=[QRMVAL]) as [SAPMGRPDESC]\n"
                + "      ,[QRMPLANT]\n"
                + "      ,[QRMVAL]\n"
                + "      ,[QRMID]\n"
                + "      ,FORMAT(isnull([QRMQTY],0),'#,#0.000') as [QRMQTY]\n"
                + "      ,[QRMBUN]\n"
                + "	  ,[QRMPACKTYPE]\n"
                + "      ,case when ([QRMPACKTYPE]='BOX') then '1' else '0' end as BOX\n"
                + "      ,case when ([QRMPACKTYPE]='BAG' or [QRMPACKTYPE]='PACK') then '1' else '0' end as BAG\n"
                + "      ,case when ([QRMPACKTYPE]='ROLL') then '1' else '0' end as ROLL\n"
                + "      ,[QRMLOC]\n"
                + "      ,isnull(right([QRMLCUPDT],2)+'-'+left(right([QRMLCUPDT],4),2)+'-'+left([QRMLCUPDT],4),'') as [QRMLCUPDT]\n"
                + "      ,[QRMSTS]+' : '+(select top 1 [STSDESC] from [QRMMATSTS] where [STSCODE]=[QRMSTS]) as [QRMSTS]\n"
                + "  FROM [RMShipment].[dbo].[QRMMAS]\n"
                + "  WHERE [QRMCODE] like '%'+'" + code + "'+'%'\n"
                + "  AND [QRMPLANT]='" + plant + "'\n"
                + "  ORDER BY [QRMCODE],[QRMLOC]";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            String dupCode = "first";

            while (result.next()) {

                WMS971 p = new WMS971();

                if (dupCode.equals("first")) {
                    p.setCODE(result.getString("QRMCODE"));
                    p.setDESC(result.getString("QRMDESC"));
                    p.setGRPDESC(result.getString("SAPMGRPDESC"));

                    dupCode = result.getString("QRMCODE");

                } else if (dupCode.equals(result.getString("QRMCODE"))) {
                    p.setCODE("<b style=\"opacity: 0\" class=\"codeGrp\">" + result.getString("QRMCODE") + "</b>");
                    p.setDESC("<b style=\"opacity: 0\" class=\"codeGrp\">" + result.getString("QRMDESC") + "</b>");
                    p.setGRPDESC("<b style=\"opacity: 0\" class=\"codeGrp\">" + result.getString("SAPMGRPDESC") + "</b>");

                    dupCode = result.getString("QRMCODE");

                } else {
                    p.setCODE(result.getString("QRMCODE"));
                    p.setDESC(result.getString("QRMDESC"));
                    p.setGRPDESC(result.getString("SAPMGRPDESC"));

                    dupCode = result.getString("QRMCODE");

                }

                p.setPLANT(result.getString("QRMPLANT"));
                p.setVAL(result.getString("QRMVAL"));
                p.setID(result.getString("QRMID"));
                p.setQTY(result.getString("QRMQTY"));
                p.setUNIT(result.getString("QRMBUN"));
                p.setBOX(result.getString("BOX"));
                p.setBAG(result.getString("BAG"));
                p.setROLL(result.getString("ROLL"));
                p.setLOCATION(result.getString("QRMLOC"));
                p.setLCUPDT(result.getString("QRMLCUPDT"));
                p.setSTS(result.getString("QRMSTS"));

                objList.add(p);
            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return objList;

    }
}
