/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.dao;

import com.google.gson.JsonObject;
import com.twc.wms.database.database;
import com.twc.wms.entity.ISMMASD;
import com.twc.wms.entity.ISMMASH;
import com.twc.wms.entity.WH;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

/**
 *
 * @author nutthawoot.noo
 */
public class ISM100Dao extends database {

    public List<ISMMASH> getHead(String level, String pdg) {

        List<ISMMASH> UAList = new ArrayList<ISMMASH>();

        String STS = "";
        int vel = Integer.parseInt(level);
        if (vel >= 7) {
            STS = "AND [ISHLSTS] in ('1','2','3','4','5','6','9')\n";
//            STS = "AND [ISHLSTS] in ('1')\n";
        } else {
            STS = "AND [ISHLSTS] in ('0','1','2','3','4','5','6','9')\n";
//            STS = "AND [ISHLSTS] in ('0')\n";
        }

        String sql = "SELECT [ISHMVT]\n"
                + ",isnull(right([ISHTRDT],2)\n"
                + "+'/'+left(right([ISHTRDT],4),2)\n"
                + "+'/'+left([ISHTRDT],4),'') as [ISHTRDT]\n"
                + ",isnull([ISHORD],'') as [ISHORD]\n"
                + ",isnull([ISHSEQ],'') as [ISHSEQ]\n"
                + ",isnull(cast([ISHCUNO] as int),'') as [ISHCUNO]\n"
                + ",isnull([ISHCUNM1],'') as [ISHCUNM1]\n"
                + ",isnull([ISHSUBMI],'') as [ISHSUBMI]\n"
                + ",isnull([ISHSTYLE]+[ISHCOLOR],'') as [ISHMATCODE]\n"
                + ",isnull([ISHSTYLE],'') as [ISHSTYLE]\n"
                + ",isnull([ISHCOLOR],'') as [ISHCOLOR]\n"
                + ",isnull([ISHWHNO],'') as [ISHWHNO]\n"
                + ",case when ([ISHEUSR] = '' or [ISHEUSR] is null) then 'ok' else [ISHEUSR] end as [ISHEUSR]\n"
                + ",isnull('#'+[ISHLOT],'') as [ISHLOT]\n"
                + ",isnull(format(cast([ISHAMTFG] as decimal(16,6)),'#,#0'),'') as [ISHAMTFG]\n"
                + ",isnull(format([ISHNOR],'#,#0'),'0') as [ISHNOR]\n"
                + ",isnull(case when [ISHLSTS] = 0 then '-o' else '' end,'') as [LSTSI]\n"
                + ",isnull(case when [ISHHSTS] = 0 then '-o' else '' end,'') as [HSTSI]\n"
                + ",isnull([ISHLSTS],'') as [LSTS]\n"
                + ",isnull([ISHHSTS],'') as [HSTS]\n"
                + ",isnull([ISHLSTS]+' = '+(select top 1 [ISSDESC] from [ISMSTS] where [ISSSTS] = [ISHLSTS]),'') as [ISHLSTS]\n"
                + ",isnull([ISHHSTS]+' = '+(select top 1 [ISSDESC] from [ISMSTS] where [ISSSTS] = [ISHHSTS]),'') as [ISHHSTS]\n"
                + ",isnull(right([ISHRQDT],2)\n"
                + "+'/'+left(right([ISHRQDT],4),2)\n"
                + "+'/'+left([ISHRQDT],4),'') as [ISHRQDT]\n"
                // + ",isnull([ISMMASH].[ISHDEST] COLLATE SQL_Latin1_General_CP1_CI_AS +' : '+(select top 1 [QDEDESC] from [QDEST] where [QDEST].[QDECOD] = [ISMMASH].[ISHDEST] COLLATE SQL_Latin1_General_CP1_CI_AS),'') as [ISHDEST]\n"
                + ",isnull((select top 1 [QDEDESC] from [QDEST] where [QDEST].[QDECOD] = [ISMMASH].[ISHDEST] COLLATE SQL_Latin1_General_CP1_CI_AS),'') as [ISHDEST]\n"
                + ",isnull(right([ISHAPDT],2)\n"
                + "+'/'+left(right([ISHAPDT],4),2)\n"
                + "+'/'+left([ISHAPDT],4),'') as [ISHAPDT]\n"
                + ",isnull([ISHAPUSR]+' : '+(select top 1 SUBSTRING([USERS],0,CHARINDEX(' ', [USERS])) from [MSSUSER] where [USERID] = [ISHAPUSR]),'') as [ISHAPUSR]\n"
                + ",isnull([ISHUSER]+' : '+(select top 1 SUBSTRING([USERS],0,CHARINDEX(' ', [USERS])) from [MSSUSER] where [USERID] = [ISHUSER]),'') as [ISHUSER]\n"
                + "FROM [ISMMASH]\n"
                + "WHERE [ISHPGRP] = '" + pdg + "'\n"
                + STS
                + "ORDER BY [ISHORD] DESC";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                ISMMASH p = new ISMMASH();

                p.setISHMVT(result.getString("ISHMVT"));
                p.setISHTRDT(result.getString("ISHTRDT"));
                p.setISHORD(result.getString("ISHORD"));
                p.setISHSEQ(result.getString("ISHSEQ"));
                p.setISHCUNO(result.getString("ISHCUNO"));
                p.setISHCUNM1(result.getString("ISHCUNM1"));
                p.setISHSUBMI(result.getString("ISHSUBMI"));
                p.setISHMATCODE(result.getString("ISHMATCODE"));
                p.setISHSTYLE(result.getString("ISHSTYLE"));
                p.setISHCOLOR(result.getString("ISHCOLOR"));
                p.setISHLOT(result.getString("ISHLOT"));
                p.setISHAMTFG(result.getString("ISHAMTFG"));
                p.setISHNOR(result.getString("ISHNOR"));
                p.setLSTSI(result.getString("LSTSI"));
                p.setHSTSI(result.getString("HSTSI"));
                p.setLSTS(result.getString("LSTS"));
                p.setHSTS(result.getString("HSTS"));
                p.setISHLSTS(result.getString("ISHLSTS"));
                p.setISHHSTS(result.getString("ISHHSTS"));
                p.setISHRQDT(result.getString("ISHRQDT"));
                p.setISHDEST(result.getString("ISHDEST"));
                p.setISHAPDT(result.getString("ISHAPDT"));
                p.setISHAPUSR(result.getString("ISHAPUSR"));
                p.setISHUSER(result.getString("ISHUSER"));
                p.setISHEUSR(result.getString("ISHEUSR"));
                p.setISHWHNO(result.getString("ISHWHNO"));

                UAList.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public ISMMASH getAppHead(String orderNo, String seq) {

        ISMMASH p = new ISMMASH();

        String sql = "SELECT [ISHMVT]\n"
                + ",isnull(right([ISHTRDT],2)\n"
                + "+'/'+left(right([ISHTRDT],4),2)\n"
                + "+'/'+left([ISHTRDT],4),'') as [ISHTRDT]\n"
                + ",isnull([ISHORD],'') as [ISHORD]\n"
                + ",isnull([ISHSEQ],'') as [ISHSEQ]\n"
                + ",isnull(cast([ISHCUNO] as int),'') as [ISHCUNO]\n"
                + ",isnull([ISHCUNM1],'') as [ISHCUNM1]\n"
                + ",isnull([ISHSUBMI],'') as [ISHSUBMI]\n"
                + ",isnull([ISHREMARK],'') as [ISHREMARK]\n"
                + ",isnull([ISHSTYLE]+[ISHCOLOR],'') as [ISHMATCODE]\n"
                + ",isnull([ISHSTYLE],'') as [ISHSTYLE]\n"
                + ",isnull([ISHCOLOR],'') as [ISHCOLOR]\n"
                + ",isnull('#'+[ISHLOT],'') as [ISHLOT]\n"
                + ",isnull(format(cast([ISHAMTFG] as decimal(16,6)),'#,#0.00'),'') as [ISHAMTFG]\n"
                + ",isnull(case when [ISHLSTS] = 0 then '-o' else '' end,'') as [LSTSI]\n"
                + ",isnull(case when [ISHHSTS] = 0 then '-o' else '' end,'') as [HSTSI]\n"
                + ",isnull([ISHLSTS],'') as [LSTS]\n"
                + ",isnull([ISHHSTS],'') as [HSTS]\n"
                + ",isnull([ISHLSTS]+' = '+(select top 1 [ISSDESC] from [ISMSTS] where [ISSSTS] = [ISHLSTS]),'') as [ISHLSTS]\n"
                + ",isnull([ISHHSTS]+' = '+(select top 1 [ISSDESC] from [ISMSTS] where [ISSSTS] = [ISHHSTS]),'') as [ISHHSTS]\n"
                + ",isnull(right([ISHRQDT],2)\n"
                + "+'/'+left(right([ISHRQDT],4),2)\n"
                + "+'/'+left([ISHRQDT],4),'') as [ISHRQDT]\n"
                + ",isnull(right([ISHEDT],2)\n"
                + "+'/'+left(right([ISHEDT],4),2)\n"
                + "+'/'+left([ISHEDT],4),'') as [ISHEDT]\n"
                // + ",isnull([ISMMASH].[ISHDEST] COLLATE SQL_Latin1_General_CP1_CI_AS +' : '+(select top 1 [QDEDESC] from [QDEST] where [QDEST].[QDECOD] = [ISMMASH].[ISHDEST] COLLATE SQL_Latin1_General_CP1_CI_AS),'') as [ISHDEST]\n"
                + ",isnull((select top 1 [QDEDESC] from [QDEST] where [QDEST].[QDECOD] = [ISMMASH].[ISHDEST] COLLATE SQL_Latin1_General_CP1_CI_AS),'') as [ISHDEST]\n"
                + ",isnull(right([ISHAPDT],2)\n"
                + "+'/'+left(right([ISHAPDT],4),2)\n"
                + "+'/'+left([ISHAPDT],4),'') as [ISHAPDT]\n"
                + ",isnull((select top 1 [USERS] from [MSSUSER] where [USERID] = [ISHAPUSR]),'') as [ISHAPUSR]\n"
                + ",isnull((select top 1 [USERS] from [MSSUSER] where [USERID] = [ISHUSER]),'') as [ISHUSER]\n"
                + ",isnull((select top 1 [USERS] from [MSSUSER] where [USERID] = [ISHEUSR]),'') as [ISHEUSR]\n"
                + ",isnull(ISHRPUSR,'') AS ISHRPUSR\n"
                + ",isnull(ISHREASON,'') AS ISHREASON\n"
                + ",isnull(ISHBSTKD,'') AS ISHBSTKD\n"
                + "FROM [ISMMASH]\n"
                + "WHERE [ISHORD] = '" + orderNo + "' AND ISHSEQ = '" + seq + "' ";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                p.setISHMVT(result.getString("ISHMVT"));
                p.setISHTRDT(result.getString("ISHTRDT"));
                p.setISHORD(result.getString("ISHORD"));
                p.setISHSEQ(result.getString("ISHSEQ"));
                p.setISHCUNO(result.getString("ISHCUNO"));
                p.setISHCUNM1(result.getString("ISHCUNM1"));
                p.setISHSUBMI(result.getString("ISHSUBMI"));
                p.setISHMATCODE(result.getString("ISHMATCODE"));
                p.setISHSTYLE(result.getString("ISHSTYLE"));
                p.setISHCOLOR(result.getString("ISHCOLOR"));
                p.setISHLOT(result.getString("ISHLOT"));
                p.setISHAMTFG(result.getString("ISHAMTFG"));
                p.setLSTSI(result.getString("LSTSI"));
                p.setHSTSI(result.getString("HSTSI"));
                p.setLSTS(result.getString("LSTS"));
                p.setHSTS(result.getString("HSTS"));
                p.setISHLSTS(result.getString("ISHLSTS"));
                p.setISHHSTS(result.getString("ISHHSTS"));
                p.setISHRQDT(result.getString("ISHRQDT"));
                p.setISHDEST(result.getString("ISHDEST"));
                p.setISHAPDT(result.getString("ISHAPDT"));
                p.setISHAPUSR(result.getString("ISHAPUSR"));
                p.setISHUSER(result.getString("ISHUSER"));
                p.setISHREMARK(result.getString("ISHREMARK"));
                p.setISHEUSR(result.getString("ISHEUSR"));
                p.setISHEDT(result.getString("ISHEDT"));
                p.setISHRPUSR(result.getString("ISHRPUSR"));
                p.setISHREASON(result.getString("ISHREASON"));
                p.setISHBSTKD(result.getString("ISHBSTKD"));

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return p;

    }

    public List<ISMMASD> getDetail(String orderNo, String seq) {

        List<ISMMASD> UAList = new ArrayList<ISMMASD>();

        String sql = "SELECT isnull([ISDORD],'') as [ISDORD]\n"
                + "      ,isnull([ISDSEQ],'') as [ISDSEQ]\n"
                + "      ,isnull([ISDLINO],'') as [LINO]\n"
                + "      ,isnull(cast([ISDLINO] as int),'') as [ISDLINO]\n"
                + "      ,isnull(ISDVT,'') as [ISDVT]\n"
                + "      ,isnull([ISDSINO],0) as [ISDSINO]\n"
                + "      ,(case when (SELECT COUNT(OT.[ISDSINO])\n"
                + "	    FROM [ISMMASD] OT \n"
                + "	    WHERE OT.[ISDORD]=DET.[ISDORD] AND OT.[ISDSEQ] = DET.[ISDSEQ] \n"
                + "		AND OT.[ISDLINO]=DET.[ISDLINO]) != 1\n"
                + "		and ([ISDSINO] = 0) \n"
                + "		then format([ISDSINO],'0') \n"
                + "		when ([ISDSINO] != 0) \n"
                + "		then format([ISDSINO],'0') else '' end ) as FAM\n"
                + "      ,isnull([ISDITNO],'') as [ISDITNO]\n"
                + "	  ,isnull((select top 1 [SAPDESC] from [SAPMAS] where [SAPMAT]=[ISDITNO] collate SQL_Latin1_General_CP1_CI_AS),'') as [ISDDESC]\n"
                + "      ,isnull(format([ISDRQQTY],'#,#0.00'),0) as [ISDRQQTY]\n"
                + "	  ,isnull([ISDUNIT],'') as [ISDUNIT]\n"
                + "	  ,isnull(format((isnull([ISDUNR01],0)+isnull([ISDUNR03],0)),'#,#0.00'),0) as [ISDSAPONH]\n"
                + "	  ,isnull((SELECT format(sum([QRMQTY]),'#,#0.00')\n"
                + "	    FROM [QRMMAS]\n"
                + "	    where [QRMCODE]=[ISDITNO] collate SQL_Latin1_General_CP1_CI_AS\n"
                + "	    and [QRMSTS]='2'),0) as [ISDWMSONH]\n"
                + "      ,isnull([ISDMTCTRL],'') as [ISDMTCTRL]\n"
                + "      ,isnull(right([ISDRQDT],2)\n"
                + "       +'/'+left(right([ISDRQDT],4),2)\n"
                + "       +'/'+left([ISDRQDT],4),'') as [ISDRQDT]\n"
                //                + "	  ,isnull([ISDDEST] +' : '+(select top 1 [QDEDESC] from [QDEST] where [QDECOD] = [ISDDEST]),'') as [ISDDEST]\n"
                + "	  ,isnull((select top 1 [QDEDESC] from [QDEST] where [QDECOD] = [ISDDEST]),'') as [ISDDEST]\n"
                + "       ,isnull([ISDSTS]+' = '+(select top 1 [ISSDESC] from [ISMSTS] where [ISSSTS] = [ISDSTS]),'') as [ISDSTS]\n"
                + "       ,isnull(case when [ISDSTS] = '0' then '-o' else '' end,'') as [STSI]\n"
                + "       ,isnull([ISDSTS],'') as [STS] ,ISDREMAIN\n"
                + "  FROM [RMShipment].[dbo].[ISMMASD] DET\n"
                + "  WHERE [ISDORD] = '" + orderNo + "' AND [ISDSEQ] = '" + seq + "' \n"
                + "  ORDER BY [ISDLINO], [ISDSINO]";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                ISMMASD p = new ISMMASD();

                p.setISDORD(result.getString("ISDORD"));
                p.setISDSEQ(result.getString("ISDSEQ"));
                p.setLINO(result.getString("LINO"));
                p.setISDLINO(result.getString("ISDLINO"));
                p.setISDVT(result.getString("ISDVT"));
                p.setISDSINO(result.getString("ISDSINO"));
                p.setISDITNO(result.getString("ISDITNO"));
                p.setISDDESC(result.getString("ISDDESC"));
                p.setISDRQQTY(result.getString("ISDRQQTY"));
                p.setISDUNIT(result.getString("ISDUNIT"));
                p.setISDSAPONH(result.getString("ISDSAPONH"));
                p.setISDWMSONH(result.getString("ISDWMSONH"));
                p.setISDMTCTRL(result.getString("ISDMTCTRL"));
                p.setISDRQDT(result.getString("ISDRQDT"));
                p.setISDDEST(result.getString("ISDDEST"));
                p.setISDSTS(result.getString("ISDSTS"));
                p.setSTS(result.getString("STS"));
                p.setSTSI(result.getString("STSI"));
                p.setFAM(result.getString("FAM"));
                p.setISDREMAIN(result.getString("ISDREMAIN"));

                UAList.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public boolean updateReqDate(String orderNo, String reqDate, String uid, String seq) {

        SimpleDateFormat formatter = new SimpleDateFormat("HHmmss");
        Date date = new Date(System.currentTimeMillis());
        String tm = formatter.format(date);

        boolean result = false;

        String sql = "UPDATE [ISMMASH] "
                + "SET [ISHRQDT] = ? "
                + "   ,[ISHRQTM] = '" + tm + "' "
                + "   ,[ISHEDT] = format(current_timestamp,'yyyyMMdd') "
                + "   ,[ISHETM] = '" + tm + "' "
                + "   ,[ISHEUSR] = '" + uid + "' "
                + " WHERE [ISHORD] = ? AND [ISHSEQ] = ?";
        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ps.setString(1, reqDate);
            ps.setString(2, orderNo);
            ps.setString(3, seq);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean updateReqDateAllDet(String orderNo, String reqDate, String uid, String seq) {

        SimpleDateFormat formatter = new SimpleDateFormat("HHmmss");
        Date date = new Date(System.currentTimeMillis());
        String tm = formatter.format(date);

        boolean result = false;

        String sql = "UPDATE [ISMMASD] "
                + "SET [ISDRQDT] = ? "
                + "   ,[ISDRQTM] = '" + tm + "' "
                + "   ,[ISDEDT] = format(current_timestamp,'yyyyMMdd') "
                + "   ,[ISDETM] = '" + tm + "' "
                + "   ,[ISDEUSR] = '" + uid + "' "
                + " WHERE [ISDORD] = ? AND [ISDSEQ] = ?";
        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ps.setString(1, reqDate);
            ps.setString(2, orderNo);
            ps.setString(3, seq);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean updateReqDateDetail(String orderNo, String lino, String reqDate, String uid, String seq) {

        SimpleDateFormat formatter = new SimpleDateFormat("HHmmss");
        Date date = new Date(System.currentTimeMillis());
        String tm = formatter.format(date);

        boolean result = false;

        String sql = "UPDATE [ISMMASD] "
                + "SET [ISDRQDT] = ? "
                + "   ,[ISDRQTM] = '" + tm + "' "
                + "   ,[ISDEDT] = format(current_timestamp,'yyyyMMdd') "
                + "   ,[ISDETM] = '" + tm + "' "
                + "   ,[ISDEUSR] = '" + uid + "' "
                + " WHERE [ISDORD] = ? "
                + " AND CONCAT([ISDLINO],'-',[ISDSINO]) = ? AND [ISDSEQ] = ?";
        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ps.setString(1, reqDate);
            ps.setString(2, orderNo);
            ps.setString(3, lino);
            ps.setString(4, seq);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean updateDest(String orderNo, String dest, String uid, String seq) {

        SimpleDateFormat formatter = new SimpleDateFormat("HHmmss");
        Date date = new Date(System.currentTimeMillis());
        String tm = formatter.format(date);

        boolean result = false;

        String sql = "UPDATE [ISMMASH] "
                + "SET [ISHDEST] = ? "
                + "   ,[ISHEDT] = format(current_timestamp,'yyyyMMdd') "
                + "   ,[ISHETM] = '" + tm + "' "
                + "   ,[ISHEUSR] = '" + uid + "' "
                + " WHERE [ISHORD] = ? AND [ISHSEQ] = ?";
        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ps.setString(1, dest);
            ps.setString(2, orderNo);
            ps.setString(3, seq);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean updateDestAllDet(String orderNo, String dest, String uid, String seq) {

        SimpleDateFormat formatter = new SimpleDateFormat("HHmmss");
        Date date = new Date(System.currentTimeMillis());
        String tm = formatter.format(date);

        boolean result = false;

        String sql = "UPDATE [ISMMASD] "
                + "SET [ISDDEST] = ? "
                + "   ,[ISDEDT] = format(current_timestamp,'yyyyMMdd') "
                + "   ,[ISDETM] = '" + tm + "' "
                + "   ,[ISDEUSR] = '" + uid + "' "
                + " WHERE [ISDORD] = ? AND [ISDSEQ] = ?";
        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ps.setString(1, dest);
            ps.setString(2, orderNo);
            ps.setString(3, seq);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean updateDestDetail(String orderNo, String lino, String dest, String seq, String uid) {

        SimpleDateFormat formatter = new SimpleDateFormat("HHmmss");
        Date date = new Date(System.currentTimeMillis());
        String tm = formatter.format(date);

        boolean result = false;

        String sql = "UPDATE [ISMMASD] "
                + "SET [ISDDEST] = ? "
                + "   ,[ISDEDT] = format(current_timestamp,'yyyyMMdd') "
                + "   ,[ISDETM] = '" + tm + "' "
                + "   ,[ISDEUSR] = '" + uid + "' "
                + " WHERE [ISDORD] = ? "
                + " AND CONCAT([ISDLINO],'-',[ISDSINO]) = ? AND ISDSEQ = ? ";
        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ps.setString(1, dest);
            ps.setString(2, orderNo);
            ps.setString(3, lino);
            ps.setString(4, seq);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean updateReqApp(String orderNo, String uid, String seq) {

        SimpleDateFormat formatter = new SimpleDateFormat("HHmmss");
        Date date = new Date(System.currentTimeMillis());
        String tm = formatter.format(date);

        boolean result = false;

        String sql = "UPDATE [ISMMASH] "
                + "SET [ISHLSTS] = '1' "
                + "   ,[ISHHSTS] = '1' "
                + "   ,[ISHEDT] = format(current_timestamp,'yyyyMMdd') "
                + "   ,[ISHETM] = '" + tm + "' "
                + "   ,[ISHEUSR] = '" + uid + "' "
                + " WHERE [ISHORD] = ? AND [ISHSEQ] = ?";
        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ps.setString(1, orderNo);
            ps.setString(2, seq);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean updateMngApp(String orderNo, String uid, String seq) {

        SimpleDateFormat formatter = new SimpleDateFormat("HHmmss");
        Date date = new Date(System.currentTimeMillis());
        String tm = formatter.format(date);

        boolean result = false;

        String sql = "UPDATE [ISMMASH] "
                + "SET [ISHLSTS] = '2' "
                + "   ,[ISHHSTS] = '2' "
                + "   ,[ISHREMARK] = null "
                + "   ,[ISHAPDT] = format(current_timestamp,'yyyyMMdd') "
                + "   ,[ISHAPTM] = '" + tm + "' "
                + "   ,[ISHAPUSR] = ? "
                + "   ,[ISHEDT] = format(current_timestamp,'yyyyMMdd') "
                + "   ,[ISHETM] = '" + tm + "' "
                + " WHERE [ISHORD] = ? AND [ISHSEQ] = ?";
        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ps.setString(1, uid);
            ps.setString(2, orderNo);
            ps.setString(3, seq);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean updateMngAppAgain(String orderNo, String uid, String seq) {

        SimpleDateFormat formatter = new SimpleDateFormat("HHmmss");
        Date date = new Date(System.currentTimeMillis());
        String tm = formatter.format(date);

        boolean result = false;

        String sql = "UPDATE [ISMMASH] "
                + "SET [ISHLSTS] = '2' "
                + "   ,[ISHHSTS] = '2' "
                + "   ,[ISHREMARK] = null "
                + "   ,[ISHAPDT] = format(current_timestamp,'yyyyMMdd') "
                + "   ,[ISHAPTM] = '" + tm + "' "
                //                + "   ,[ISHAPUSR] = ? "
                + "   ,[ISHEDT] = format(current_timestamp,'yyyyMMdd') "
                + "   ,[ISHETM] = '" + tm + "' "
                + " WHERE [ISHORD] = ? AND [ISHSEQ] = ?";
        try {

            PreparedStatement ps = connect.prepareStatement(sql);

//            ps.setString(1, uid);
            ps.setString(1, orderNo);
            ps.setString(2, seq);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean deleteHeadAgain(String orderNo, String seq) {

        boolean result = false;

        String sql = "DELETE [ISMMASH] WHERE [ISHORD] = ? AND [ISHSEQ] = ?";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

//            ps.setString(1, uid);
            ps.setString(1, orderNo);
            ps.setString(2, seq);

            int record = ps.executeUpdate();

            if (record >= 1) {
                result = true;
            } else {
                result = false;
            }

            ps.close();
            connect.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;

    }

    public boolean updateBackwardH(String orderNo, String uid, String seq) {

        SimpleDateFormat formatter = new SimpleDateFormat("HHmmss");
        Date date = new Date(System.currentTimeMillis());
        String tm = formatter.format(date);

        boolean result = false;

        String sql = "UPDATE [ISMMASH] "
                + "SET [ISHLSTS] = '0' "
                + "   ,[ISHHSTS] = '0' "
                + "   ,[ISHREMARK] = null "
                + "   ,[ISHRQDT] = null"
                + "   ,[ISHRQTM] = null"
                //                + "   ,[ISHEUSR] = ? "
                + "   ,[ISHEDT] = format(current_timestamp,'yyyyMMdd') "
                + "   ,[ISHETM] = '" + tm + "' "
                + " WHERE [ISHORD] = ? AND [ISHSEQ] = ?";
//        System.out.println("" + sql); 
        try {
            PreparedStatement ps = connect.prepareStatement(sql);

//            ps.setString(1, uid);
            ps.setString(1, orderNo);
            ps.setString(2, seq);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean updateReqAppAllDet(String orderNo, String uid, String seq) {

        SimpleDateFormat formatter = new SimpleDateFormat("HHmmss");
        Date date = new Date(System.currentTimeMillis());
        String tm = formatter.format(date);

        boolean result = false;

        String sql = "UPDATE [ISMMASD] "
                + "SET [ISDSTS] = '1' "
                + "   ,[ISDEDT] = format(current_timestamp,'yyyyMMdd') "
                + "   ,[ISDETM] = '" + tm + "' "
                + "   ,[ISDEUSR] = '" + uid + "' "
                + " WHERE [ISDORD] = ? AND [ISDSEQ] = ?";
        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ps.setString(1, orderNo);
            ps.setString(2, seq);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean updateMngAppAllDet(String orderNo, String uid, String seq) {

        SimpleDateFormat formatter = new SimpleDateFormat("HHmmss");
        Date date = new Date(System.currentTimeMillis());
        String tm = formatter.format(date);

        boolean result = false;

        String sql = "UPDATE [ISMMASD] "
                + "SET [ISDSTS] = '2' "
                + "   ,[ISDREMARK] = null "
                + "   ,[ISDAPDT] = format(current_timestamp,'yyyyMMdd') "
                + "   ,[ISDAPTM] = '" + tm + "' "
                + "   ,[ISDAPUSER] = ? "
                + "   ,[ISDEDT] = format(current_timestamp,'yyyyMMdd') "
                + "   ,[ISDETM] = '" + tm + "' "
                + " WHERE [ISDORD] = ? AND [ISDSEQ] = ? ";
        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ps.setString(1, uid);
            ps.setString(2, orderNo);
            ps.setString(3, seq);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public JSONObject updateMngAppAllDetLine(String orderNo, String uid, String seq, String line) {

        JSONObject res = new JSONObject();

        SimpleDateFormat formatter = new SimpleDateFormat("HHmmss");
        Date date = new Date(System.currentTimeMillis());
        String tm = formatter.format(date);

        boolean result = false;

        String sql = "UPDATE [ISMMASD] "
                + "SET [ISDSTS] = '2' "
                + "   ,[ISDREMARK] = null "
                + "   ,[ISDAPDT] = format(current_timestamp,'yyyyMMdd') "
                + "   ,[ISDAPTM] = '" + tm + "' "
                //                + "   ,[ISDAPUSER] ?"
                + "   ,[ISDEDT] = format(current_timestamp,'yyyyMMdd') "
                + "   ,[ISDETM] = '" + tm + "' "
                + " WHERE [ISDORD] = ? AND [ISDSEQ] = ? AND CONCAT([ISDLINO],'-',[ISDSINO]) = ?";

        String sql2 = "UPDATE [ISMMASD] "
                + "SET [ISDREMAIN] = 'C' "
                + " WHERE [ISDORD] = ? AND CONCAT([ISDLINO],'-',[ISDSINO]) = ? AND [ISDREMAIN] = 'S' ";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

//            ps.setString(1, uid);
            ps.setString(1, orderNo);
            ps.setString(2, seq);
            ps.setString(3, line);

            PreparedStatement ps2 = connect.prepareStatement(sql2);
            ps2.setString(1, orderNo);
//            ps2.setString(2, Integer.toString(Integer.parseInt(seq) - 1));
            ps2.setString(2, line);

            int record = ps.executeUpdate();
            int record2 = ps2.executeUpdate();

            if (record >= 1) {
                res.put("upSts", true);
            } else {
                res.put("upSts", false);
            }

            if (record2 >= 1) {
                res.put("upRM", true);
            } else {
                res.put("upRM", false);
            }

            ps.close();
            ps2.close();
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return res;

    }

    public JSONObject deleteAllDetLine(String orderNo, String uid, String seq, String line) {

        JSONObject res = new JSONObject();

        String sql = "DELETE [ISMMASD] WHERE [ISDORD] = ? AND [ISDSEQ] = ? AND CONCAT([ISDLINO],'-',[ISDSINO]) = ? ";

        String sql2 = "UPDATE [ISMMASD] SET [ISDREMAIN] = '' WHERE [ISDORD] = ? AND CONCAT([ISDLINO],'-',[ISDSINO]) = ? AND [ISDREMAIN] = 'S' ";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

//            ps.setString(1, uid);
            ps.setString(1, orderNo);
            ps.setString(2, seq);
            ps.setString(3, line);

            PreparedStatement ps2 = connect.prepareStatement(sql2);
            ps2.setString(1, orderNo);
//            ps2.setString(2, Integer.toString(Integer.parseInt(seq) - 1));
            ps2.setString(2, line);

            int record = ps.executeUpdate();
            int record2 = ps2.executeUpdate();

            if (record >= 1) {
                res.put("delOld", true);
            } else {
                res.put("delOld", false);
            }

            if (record2 >= 1) {
                res.put("upClear", true);
            } else {
                res.put("upClear", false);
            }

            ps.close();
            ps2.close();
            connect.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return res;

    }

    public JSONObject closeAllDetLine(String orderNo, String uid, String seq, String line) {

        JSONObject res = new JSONObject();

        String sql = "UPDATE [ISMMASD] SET ISDREMAIN = 'C' WHERE [ISDORD] = ? AND [ISDSEQ] = ? AND CONCAT([ISDLINO],'-',[ISDSINO]) = ? "; //New

        String sql2 = "UPDATE [ISMMASD] SET [ISDREMAIN] = 'C' WHERE [ISDORD] = ? AND CONCAT([ISDLINO],'-',[ISDSINO]) = ? AND [ISDREMAIN] = 'S' "; //Old

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

//            ps.setString(1, uid);
            ps.setString(1, orderNo);
            ps.setString(2, seq);
            ps.setString(3, line);

            PreparedStatement ps2 = connect.prepareStatement(sql2);
            ps2.setString(1, orderNo);
//            ps2.setString(2, Integer.toString(Integer.parseInt(seq) - 1));
            ps2.setString(2, line);

            int record = ps.executeUpdate();
            int record2 = ps2.executeUpdate();

            if (record >= 1) {
                res.put("closeOld", true);
            } else {
                res.put("closeOld", false);
            }

            if (record2 >= 1) {
                res.put("closeNew", true);
            } else {
                res.put("closeNew", false);
            }

            ps.close();
            ps2.close();
            connect.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return res;

    }

    public boolean updateBackWardAllDet(String orderNo, String uid, String seq) {

        SimpleDateFormat formatter = new SimpleDateFormat("HHmmss");
        Date date = new Date(System.currentTimeMillis());
        String tm = formatter.format(date);

        boolean result = false;

        String sql = "UPDATE [ISMMASD] "
                + "SET [ISDSTS] = '0' "
                + "   ,[ISDREMARK] = null "
                + "   ,[ISDRQDT] = null"
                + "   ,[ISDRQTM] = null"
                //                + "   ,[ISDEUSR] = ? "
                + "   ,[ISDEDT] = format(current_timestamp,'yyyyMMdd') "
                + "   ,[ISDETM] = '" + tm + "' "
                + " WHERE [ISDORD] = ? AND [ISDSEQ] = ?";
//        System.out.println("" + sql);
        try {
            PreparedStatement ps = connect.prepareStatement(sql);

//            ps.setString(1, uid);
            ps.setString(1, orderNo);
            ps.setString(2, seq);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean updateSTS(String orderNo, String seq) {

        boolean result = false;

        String sql = "UPDATE [ISMMASH]\n"
                + "  set [ISHLSTS] = (SELECT min(ISDSTS)\n"
                + "                   FROM [ISMMASD]\n"
                + "                   WHERE [ISDORD] = [ISHORD] AND ISDSEQ = ISHSEQ)\n"
                + "     ,[ISHHSTS] = (SELECT max(ISDSTS)\n"
                + "                   FROM [ISMMASD]\n"
                + "                   WHERE [ISDORD] = [ISHORD] AND ISDSEQ = ISHSEQ)\n"
                + "where [ISHORD] = ? AND ISHSEQ = ?";
        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ps.setString(1, orderNo);
            ps.setString(2, seq);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean updateReqAppDetail(String orderNo, String lino, String uid, String seq) {

        SimpleDateFormat formatter = new SimpleDateFormat("HHmmss");
        Date date = new Date(System.currentTimeMillis());
        String tm = formatter.format(date);

        boolean result = false;

        String sql = "UPDATE [ISMMASD] "
                + "SET [ISDSTS] = '1' "
                + "   ,[ISDEDT] = format(current_timestamp,'yyyyMMdd') "
                + "   ,[ISDETM] = '" + tm + "' "
                + "   ,[ISDEUSR] = '" + uid + "' "
                + " WHERE [ISDORD] = ? "
                + " AND CONCAT([ISDLINO],'-',[ISDSINO]) = ? AND ISDSEQ = ? ";
        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ps.setString(1, orderNo);
            ps.setString(2, lino);
            ps.setString(3, seq);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean updateRemarkCancel(String orderNo, String remark, String uid, String seq) {

        SimpleDateFormat formatter = new SimpleDateFormat("HHmmss");
        Date date = new Date(System.currentTimeMillis());
        String tm = formatter.format(date);

        boolean result = false;

        String sql = "UPDATE [ISMMASH] "
                + "SET [ISHLSTS] = '9' "
                + "   ,[ISHHSTS] = '9' "
                + "   ,[ISHREMARK] = ? "
                + "   ,[ISHAPDT] = format(current_timestamp,'yyyyMMdd') "
                + "   ,[ISHAPTM] = '" + tm + "' "
                + "   ,[ISHAPUSR] = ? "
                + "   ,[ISHEDT] = format(current_timestamp,'yyyyMMdd') "
                + "   ,[ISHETM] = '" + tm + "' "
                + " WHERE [ISHORD] = ? AND [ISHSEQ] = ?";
        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ps.setString(1, remark);
            ps.setString(2, uid);
            ps.setString(3, orderNo);
            ps.setString(4, seq);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean updateRemarkCancelAllDet(String orderNo, String remark, String uid, String seq) {

        SimpleDateFormat formatter = new SimpleDateFormat("HHmmss");
        Date date = new Date(System.currentTimeMillis());
        String tm = formatter.format(date);

        boolean result = false;

        String sql = "UPDATE [ISMMASD] "
                + "SET [ISDSTS] = '9' "
                + "   ,[ISDREMARK] = ? "
                + "   ,[ISDAPDT] = format(current_timestamp,'yyyyMMdd') "
                + "   ,[ISDAPTM] = '" + tm + "' "
                + "   ,[ISDAPUSER] = ? "
                + "   ,[ISDEDT] = format(current_timestamp,'yyyyMMdd') "
                + "   ,[ISDETM] = '" + tm + "' "
                + " WHERE [ISDORD] = ? AND [ISDSEQ] = ?";
        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ps.setString(1, remark);
            ps.setString(2, uid);
            ps.setString(3, orderNo);
            ps.setString(4, seq);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean updateRemarkCancelDetail(String orderNo, String lino, String remark) {

        SimpleDateFormat formatter = new SimpleDateFormat("HHmmss");
        Date date = new Date(System.currentTimeMillis());
        String tm = formatter.format(date);

        boolean result = false;

        String sql = "UPDATE [ISMMASD] "
                + "SET [ISDSTS] = '9' "
                + "   ,[ISDREMARK] = ? "
                + "   ,[ISDEDT] = format(current_timestamp,'yyyyMMdd') "
                + "   ,[ISDETM] = '" + tm + "' "
                + " WHERE [ISDORD] = ? "
                + " AND CONCAT([ISDLINO],'-',[ISDSINO]) = ? ";
        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ps.setString(1, remark);
            ps.setString(2, orderNo);
            ps.setString(3, lino);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public List<WH> findWH(String uid) {

        List<WH> UAList = new ArrayList<WH>();

        String sql = "SELECT QWHCOD, QWHNAME, 'selected hidden' as SEL\n"
                + "FROM QWHMAS \n"
                + "WHERE QWHCOD = (select [MSWHSE] from [MSSUSER] where [USERID] = '" + uid + "')\n"
                + "AND QWHCOD like 'WH%'\n"
                + "UNION ALL\n"
                + "SELECT QWHCOD, QWHNAME, ''  \n"
                + "FROM QWHMAS WHERE QWHCOD like 'WH%'";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                WH p = new WH();

                p.setUid(result.getString("QWHCOD"));
                p.setName(result.getString("QWHNAME"));
                p.setWarehouse(result.getString("SEL"));

                UAList.add(p);

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public List<WH> findDest(String wh) {

        List<WH> UAList = new ArrayList<WH>();

        String sql = "SELECT [QDECOD]\n"
                + "      ,[QDEDESC]\n"
                + "  FROM [RMShipment].[dbo].[QDEST]\n"
                + "  WHERE [QDEWHS] = '" + wh + "'\n"
                + "  ORDER BY [QDECOD] ";

        try {

//            System.out.println(sql);
            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                WH p = new WH();

                p.setUid(result.getString("QDECOD"));

                p.setName(result.getString("QDEDESC"));

                UAList.add(p);

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public List<WH> findProductGroup(String uid) {

        List<WH> UAList = new ArrayList<WH>();

        String sql = "SELECT distinct [QUSPDGRP]\n"
                + "	  ,[QWHSUBPROD]\n"
                + "  FROM [QUSPDGRP] GP\n"
                + "  LEFT JOIN [QWHSTR] ST ON ST.[QWHMGRP] = GP.[QUSPDGRP]\n"
                + "  WHERE [QUSUSERID] = '" + uid + "'\n"
                + "  ORDER BY [QUSPDGRP]";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                WH p = new WH();

                p.setUid(result.getString("QUSPDGRP"));

                p.setName(result.getString("QWHSUBPROD"));

                UAList.add(p);

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public boolean addSplitDetail(String orderNo, String lino, String qty, String dest, String uid, String seq) {

        SimpleDateFormat formatter = new SimpleDateFormat("HHmmss");
        Date date = new Date(System.currentTimeMillis());
        String tm = formatter.format(date);

        boolean result = false;

        String DEST = "[ISDDEST]";
        if (!dest.equals("NULL")) {
            DEST = "'" + dest + "'";
        }

        String sql = "INSERT INTO [ISMMASD] ([ISDCONO]\n"
                + "      ,[ISDORD]\n"
                + "      ,[ISDLINO]\n"
                + "      ,[ISDSEQ]\n"
                + "      ,[ISDSINO]\n"
                + "      ,[ISDITNO]\n"
                + "      ,[ISDPLANT]\n"
                + "      ,[ISDVT]\n"
                + "      ,[ISDSTRG]\n"
                + "      ,[ISDRQQTY]\n"
                + "      ,[ISDUNIT]\n"
                + "      ,[ISDPRICE]\n"
                + "      ,[ISDUNR01]\n"
                + "      ,[ISDUNR03]\n"
                + "      ,[ISDMTCTRL]\n"
                + "      ,[ISDDEST]\n"
                + "      ,[ISDRQDT]\n"
                + "      ,[ISDRQTM]\n"
                + "      ,[ISDAPDT]\n"
                + "      ,[ISDAPTM]\n"
                + "      ,[ISDAPUSER]\n"
                + "      ,[ISDSTS]\n"
                + "      ,[ISDJBNO]\n"
                //                + "      ,[ISDQNO]\n"
                + "      ,[ISDREMARK]\n"
                + "      ,[ISDRCDT]\n"
                + "      ,[ISDRCTM]\n"
                + "      ,[ISDRCUSER]\n"
                + "      ,[ISDPKQTY]\n"
                + "      ,[ISDPKDT]\n"
                + "      ,[ISDPKTM]\n"
                + "      ,[ISDPKUSER]\n"
                + "      ,[ISDTPQTY]\n"
                + "      ,[ISDTPDT]\n"
                + "      ,[ISDTPTM]\n"
                + "      ,[ISDTPUSER]\n"
                + "      ,[ISDISUNO]\n"
                + "      ,[ISDTEMP]\n"
                + "      ,[ISDEDT]\n"
                + "      ,[ISDETM]\n"
                + "      ,[ISDEUSR]\n"
                + "      ,[ISDCDT]\n"
                + "      ,[ISDCTM]\n"
                + "      ,[ISDUSER])\n"
                //                + "      ,[ISDSTAD]\n"
                //                + "      ,[ISDSTSTMP])\n"
                + "SELECT [ISDCONO]\n"
                + "      ,[ISDORD]\n"
                + "      ,[ISDLINO]\n"
                //                + "      ,[ISDSEQ]\n"
                + "      ,(SELECT [ISHSEQ] FROM [RMShipment].[dbo].[ISMMASH] WHERE [ISHORD] = '" + orderNo + "' AND ISHSEQ = '" + seq + "' ) AS [ISDSEQ] \n"
                + "      ,(select max(ot.[ISDSINO])+1 from [ISMMASD] ot\n"
                + "	    where ot.[ISDORD]=dt.[ISDORD]\n"
                + "		and ot.[ISDLINO]=dt.[ISDLINO])\n"
                + "      ,[ISDITNO]\n"
                + "      ,[ISDPLANT]\n"
                + "      ,[ISDVT]\n"
                + "      ,[ISDSTRG]\n"
                + "      ,'" + qty + "'\n"
                + "      ,[ISDUNIT]\n"
                + "      ,[ISDPRICE]\n"
                + "      ,[ISDUNR01]\n"
                + "      ,[ISDUNR03]\n"
                + "      ,[ISDMTCTRL]\n"
                + "      ," + DEST + "\n"
                + "      ,[ISDRQDT]\n"
                + "      ,[ISDRQTM]\n"
                + "      ,[ISDAPDT]\n"
                + "      ,[ISDAPTM]\n"
                + "      ,[ISDAPUSER]\n"
                + "      ,[ISDSTS]\n"
                + "      ,[ISDJBNO]\n"
                //                + "      ,[ISDQNO]\n"
                + "      ,[ISDREMARK]\n"
                + "      ,[ISDRCDT]\n"
                + "      ,[ISDRCTM]\n"
                + "      ,[ISDRCUSER]\n"
                + "      ,[ISDPKQTY]\n"
                + "      ,[ISDPKDT]\n"
                + "      ,[ISDPKTM]\n"
                + "      ,[ISDPKUSER]\n"
                + "      ,[ISDTPQTY]\n"
                + "      ,[ISDTPDT]\n"
                + "      ,[ISDTPTM]\n"
                + "      ,[ISDTPUSER]\n"
                + "      ,[ISDISUNO]\n"
                + "      ,[ISDTEMP]\n"
                + "      ,format(current_timestamp,'yyyyMMdd')\n"
                + "      ,'" + tm + "'\n"
                + "      ,'" + uid + "'\n"
                + "      ,format(current_timestamp,'yyyyMMdd')\n"
                + "      ,'" + tm + "'\n"
                + "      ,'" + uid + "'\n"
                //                + "      ,[ISDSTAD]\n"
                //                + "      ,[ISDSTSTMP]\n"
                + "  FROM [RMShipment].[dbo].[ISMMASD] dt\n"
                + "  WHERE [ISDORD] = '" + orderNo + "'\n"
                + "  AND CONCAT([ISDLINO],'-',[ISDSINO]) = '" + lino + "'\n";
        try {
//            System.out.println(sql);
            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean deleteSOHead(String orderNo, String seq) {

        boolean result = false;

        String sql = "DELETE ISMMASH WHERE [ISHORD] = '" + orderNo + "' AND [ISHSEQ] = '" + seq + "' \n";
        try {

            PreparedStatement ps = connect.prepareStatement(sql);
            int record = ps.executeUpdate();

            if (record >= 1) {
                result = true;
            } else {
                result = false;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    public boolean deleteSODetail(String orderNo, String seq) {

        boolean result = false;

        String sql = "DELETE ISMMASD WHERE [ISDORD] = '" + orderNo + "' AND [ISDSEQ] = '" + seq + "' \n";
        try {

            PreparedStatement ps = connect.prepareStatement(sql);
            int record = ps.executeUpdate();

            if (record >= 1) {
                result = true;
            } else {
                result = false;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    public boolean deleteDetail(String orderNo, String lino) {

        boolean result = false;

        String sql = "DELETE ISMMASD \n"
                + "WHERE [ISDORD] = '" + orderNo + "'\n"
                + "AND CONCAT([ISDLINO],'-',[ISDSINO]) = '" + lino + "'\n";
        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean deleteDetailChild(String orderNo, String lino) {

        boolean result = false;
        String li = lino.split("-")[0];

        String sql = "DELETE ISMMASD \n"
                + "WHERE [ISDORD] = '" + orderNo + "'\n"
                + "AND [ISDLINO] = '" + li + "'\n"
                + "AND [ISDSINO] != '0'\n";
        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean runNoDetailChild(String orderNo, String lino) {

        boolean result = false;
        String li = lino.split("-")[0];

        String sql = "With UpdateData  As\n"
                + "(\n"
                + "SELECT [ISDSINO],\n"
                + "ROW_NUMBER() OVER (ORDER BY [ISDSINO]) AS RN\n"
                + "FROM [ISMMASD]\n"
                + "WHERE [ISMMASD].[ISDORD] = '" + orderNo + "'\n"
                + "  AND [ISMMASD].[ISDLINO] = '" + li + "'\n"
                + "  AND [ISMMASD].[ISDSINO] != '0'\n"
                + ")\n"
                + "UPDATE [ISMMASD] SET [ISDSINO] = RN\n"
                + "FROM [ISMMASD]\n"
                + "INNER JOIN UpdateData ON [ISMMASD].[ISDSINO] = UpdateData.[ISDSINO]\n"
                + "WHERE [ISMMASD].[ISDORD] = '" + orderNo + "'\n"
                + "  AND [ISMMASD].[ISDLINO] = '" + li + "'\n"
                + "  AND [ISMMASD].[ISDSINO] != '0'\n";
        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public String getLevel(String uid) {

        String level = "1";

        String sql = "SELECT MSLEVEL FROM MSSUSER\n"
                + "WHERE USERID = '" + uid + "'\n";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                level = result.getString("MSLEVEL");

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return level;

    }

    public String getEditUser(String orderNo) {

        String uid = "";

        String sql = "SELECT isnull([ISHEUSR],'') as [ISHEUSR]\n"
                + "FROM [RMShipment].[dbo].[ISMMASH]\n"
                + "WHERE [ISHORD] = '" + orderNo + "'";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                uid = result.getString("ISHEUSR");

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return uid;

    }

    public List<ISMMASD> getreMain(String pdg) {

        List<ISMMASD> UAList = new ArrayList<ISMMASD>();

        String sql = "SELECT MA.[ISDCONO],MA.[ISDORD],MA.[ISDSEQ],MA.[ISDLINO],MA.[ISDSINO],MA.[ISDITNO],MA.[ISDPLANT]\n"
                + ",ISNULL(MA.[ISDVT],'') AS ISDVT,MA.[ISDSTRG],MA.[ISDUNIT],MA.[ISDPRICE],MA.[ISDUNR01],MA.[ISDUNR03],MA.[ISDMTCTRL]\n"
                + ",MA.[ISDDEST],MA.[ISDRQDT],MA.[ISDRQTM],MA.[ISDAPDT],MA.[ISDAPTM],MA.[ISDAPUSER],MA.[ISDRCDT],MA.[ISDRCTM]\n"
                + ",MA.[ISDRCUSER],MA.[ISDPKQTY],MA.[ISDPKDT],MA.[ISDPKTM],MA.[ISDPKUSER],MA.[ISDTPQTY],MA.[ISDTPDT],MA.[ISDTPTM],MA.[ISDTPUSER]\n"
                + ",MA.[ISDSTS],MA.[ISDJBNO],MA.[ISDQNO],MA.[ISDREMARK],MA.[ISDISUNO],MA.[ISDTEMP],MA.[ISDEDT],MA.[ISDETM],MA.[ISDEUSR]\n"
                + ",MA.[ISDCDT],MA.[ISDCTM],MA.[ISDUSER],MA.[ISDSTAD],MA.[ISDSTSTMP],MA.[ISDREMAIN],ISNULL((SELECT TOP 1 [SAPDESC] FROM [SAPMAS] WHERE [SAPMAT]=MA.[ISDITNO] collate SQL_Latin1_General_CP1_CI_AS),'') AS [ISDDESC] \n"
                + ",ISNULL(FORMAT((ISNULL(MA.[ISDUNR01],0)+ISNULL(MA.[ISDUNR03],0)),'#,#0.00'),0) AS [ISDSAPONH] \n"
                + ",ISNULL((SELECT FORMAT(SUM([QRMQTY]),'#,#0.00') FROM [QRMMAS] WHERE [QRMCODE]=MA.[ISDITNO] collate SQL_Latin1_General_CP1_CI_AS and [QRMSTS]='2'),0) AS [ISDWMSONH] \n"
                + ",ISNULL(FORMAT(MA.[ISDRQQTY],'#,#0.00'),0) AS [ISDRQQTY]\n"
                + ",(SELECT TOP 1 DET.ISDSINO FROM [RMShipment].[dbo].[ISMMASD] DET WHERE DET.ISDORD = MA.ISDORD AND DET.ISDSEQ = MA.ISDSEQ AND DET.ISDLINO = MA.ISDLINO AND DET.ISDREMAIN = MA.ISDREMAIN  AND LEFT(DET.ISDITNO, 2) = LEFT(MA.ISDITNO, 2) AND DET.ISDSTS = MA.ISDSTS /*AND (ISNULL(DET.ISDRQQTY,0)-ISNULL(DET.ISDPKQTY,0)) = (ISNULL(MA.ISDRQQTY,0)-ISNULL(MA.ISDPKQTY,0))*/ ORDER BY DET.ISDSINO DESC) AS CHILD\n"
                + "FROM [RMShipment].[dbo].[ISMMASD] MA\n"
                + "WHERE (MA.ISDREMAIN = '' OR MA.ISDREMAIN IS NULL) AND LEFT(MA.ISDITNO, 2) = '" + pdg + "' AND MA.ISDSTS = '6' AND ((ISNULL(MA.ISDRQQTY,0)-ISNULL(MA.ISDPKQTY,0))>0) ORDER BY ISDORD,ISDSEQ,ISDLINO,ISDSINO";
//        String sql = "SELECT [ISDCONO],[ISDORD],[ISDSEQ],[ISDLINO],[ISDSINO],[ISDITNO],[ISDPLANT]\n"
//                + ",ISNULL([ISDVT],'') AS ISDVT,[ISDSTRG],[ISDUNIT],[ISDPRICE],[ISDUNR01],[ISDUNR03],[ISDMTCTRL]\n"
//                + ",[ISDDEST],[ISDRQDT],[ISDRQTM],[ISDAPDT],[ISDAPTM],[ISDAPUSER],[ISDRCDT],[ISDRCTM]\n"
//                + ",[ISDRCUSER],[ISDPKQTY],[ISDPKDT],[ISDPKTM],[ISDPKUSER],[ISDTPQTY],[ISDTPDT],[ISDTPTM],[ISDTPUSER]\n"
//                + ",[ISDSTS],[ISDJBNO],[ISDQNO],[ISDREMARK],[ISDISUNO],[ISDTEMP],[ISDEDT],[ISDETM],[ISDEUSR]\n"
//                + ",[ISDCDT],[ISDCTM],[ISDUSER],[ISDSTAD],[ISDSTSTMP],[ISDREMAIN],ISNULL((SELECT TOP 1 [SAPDESC] FROM [SAPMAS] WHERE [SAPMAT]=[ISDITNO] collate SQL_Latin1_General_CP1_CI_AS),'') AS [ISDDESC] \n"
//                + ",ISNULL(FORMAT((ISNULL([ISDUNR01],0)+ISNULL([ISDUNR03],0)),'#,#0.00'),0) AS [ISDSAPONH] \n"
//                + ",ISNULL((SELECT FORMAT(SUM([QRMQTY]),'#,#0.00') FROM [QRMMAS] WHERE [QRMCODE]=[ISDITNO] collate SQL_Latin1_General_CP1_CI_AS and [QRMSTS]='2'),0) AS [ISDWMSONH] \n"
//                + ",ISNULL(FORMAT([ISDRQQTY],'#,#0.00'),0) AS [ISDRQQTY] \n"
//                + "FROM [RMShipment].[dbo].[ISMMASD] \n"
//                + "WHERE (ISDREMAIN = '' OR ISDREMAIN IS NULL) AND LEFT(ISDITNO, 2) = '" + pdg + "' AND ISDSTS = '6' AND ((ISNULL(ISDRQQTY,0)-ISNULL(ISDPKQTY,0))>0)";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                ISMMASD p = new ISMMASD();

                p.setISDCONO(result.getString("ISDCONO"));
                p.setISDORD(result.getString("ISDORD"));
                p.setISDSEQ(result.getString("ISDSEQ"));
                p.setISDLINO(result.getString("ISDLINO"));
                p.setISDSINO(result.getString("ISDSINO"));
                p.setISDITNO(result.getString("ISDITNO"));
                p.setISDPLANT(result.getString("ISDPLANT"));
                p.setISDVT(result.getString("ISDVT"));
                p.setISDSTRG(result.getString("ISDSTRG"));
                p.setISDRQQTY(result.getString("ISDRQQTY"));
                p.setISDUNIT(result.getString("ISDUNIT"));
                p.setISDPRICE(result.getString("ISDPRICE"));
                p.setISDUNR01(result.getString("ISDUNR01"));
                p.setISDUNR03(result.getString("ISDUNR03"));
                p.setISDMTCTRL(result.getString("ISDMTCTRL"));
                p.setISDDEST(result.getString("ISDDEST"));
                p.setISDRQDT(result.getString("ISDRQDT"));
                p.setISDRQTM(result.getString("ISDRQTM"));
                p.setISDAPDT(result.getString("ISDAPDT"));
                p.setISDAPTM(result.getString("ISDAPTM"));
                p.setISDAPUSER(result.getString("ISDAPUSER"));
                p.setISDRCDT(result.getString("ISDRCDT"));
                p.setISDRCTM(result.getString("ISDRCTM"));
                p.setISDRCUSER(result.getString("ISDRCUSER"));
                p.setISDPKQTY(result.getString("ISDPKQTY"));
                p.setISDPKDT(result.getString("ISDPKDT"));
                p.setISDPKTM(result.getString("ISDPKTM"));
                p.setISDPKUSER(result.getString("ISDPKUSER"));
                p.setISDTPQTY(result.getString("ISDTPQTY"));
                p.setISDTPDT(result.getString("ISDTPDT"));
                p.setISDTPTM(result.getString("ISDTPTM"));
                p.setISDTPUSER(result.getString("ISDTPUSER"));
                p.setISDSTS(result.getString("ISDSTS"));
                p.setISDJBNO(result.getString("ISDJBNO"));
                p.setISDQNO(result.getString("ISDQNO"));
                p.setISDREMARK(result.getString("ISDREMARK"));
                p.setISDISUNO(result.getString("ISDISUNO"));
                p.setISDTEMP(result.getString("ISDTEMP"));
                p.setISDEDT(result.getString("ISDEDT"));
                p.setISDETM(result.getString("ISDETM"));
                p.setISDEUSR(result.getString("ISDEUSR"));
                p.setISDCDT(result.getString("ISDCDT"));
                p.setISDCTM(result.getString("ISDCTM"));
                p.setISDUSER(result.getString("ISDUSER"));
                p.setISDSTAD(result.getString("ISDSTAD"));
                p.setISDSTSTMP(result.getString("ISDSTSTMP"));
                p.setISDREMAIN(result.getString("ISDREMAIN"));
                p.setISDDESC(result.getString("ISDDESC").trim());
                p.setISDSAPONH(result.getString("ISDSAPONH"));
                p.setISDWMSONH(result.getString("ISDWMSONH"));
                p.setFAM(result.getString("CHILD"));

                UAList.add(p);

            }

            result.close();
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public boolean addRemainLine(String orderNo, String lino, String seqOld, String seqNew, String uid, String sino) {

        SimpleDateFormat formatter = new SimpleDateFormat("HHmmss");
        Date date = new Date(System.currentTimeMillis());
        String tm = formatter.format(date);

        boolean result = false;

        String sql = "INSERT INTO [ISMMASD] ([ISDCONO]\n"
                + "      ,[ISDORD]\n"
                + "      ,[ISDLINO]\n"
                + "      ,[ISDSEQ]\n"
                + "      ,[ISDSINO]\n"
                + "      ,[ISDITNO]\n"
                + "      ,[ISDPLANT]\n"
                + "      ,[ISDVT]\n"
                + "      ,[ISDSTRG]\n"
                + "      ,[ISDRQQTY]\n"
                + "      ,[ISDUNIT]\n"
                + "      ,[ISDPRICE]\n"
                + "      ,[ISDUNR01]\n"
                + "      ,[ISDUNR03]\n"
                + "      ,[ISDMTCTRL]\n"
                + "      ,[ISDDEST]\n"
                + "      ,[ISDAPUSER]\n"
                + "      ,[ISDSTS]\n"
                + "      ,[ISDJBNO]\n"
                + "      ,[ISDISUNO]\n"
                + "      ,[ISDTEMP]\n"
                + "      ,[ISDEDT]\n"
                + "      ,[ISDETM]\n"
                + "      ,[ISDEUSR]\n"
                + "      ,[ISDCDT]\n"
                + "      ,[ISDCTM]\n"
                + "      ,[ISDUSER]\n"
                + "      ,[ISDREMAIN])\n"
                + "SELECT [ISDCONO]\n"
                + "      ,[ISDORD]\n"
                + "      ,[ISDLINO]\n"
                + "      ,'" + seqNew + "'\n"
                + "      ,[ISDSINO]\n"
                + "      ,[ISDITNO]\n"
                + "      ,[ISDPLANT]\n"
                + "      ,[ISDVT]\n"
                + "      ,[ISDSTRG]\n"
                + "      ,[ISDRQQTY]\n"
                + "      ,[ISDUNIT]\n"
                + "      ,[ISDPRICE]\n"
                + "      ,[ISDUNR01]\n"
                + "      ,[ISDUNR03]\n"
                + "      ,[ISDMTCTRL]\n"
                + "      ,[ISDDEST]\n"
                + "      ,[ISDAPUSER]\n"
                + "      ,'0'\n"
                + "      ,[ISDJBNO]\n"
                + "      ,[ISDISUNO]\n"
                + "      ,[ISDTEMP]\n"
                + "      ,format(current_timestamp,'yyyyMMdd')\n"
                + "      ,'" + tm + "'\n"
                + "      ,'" + uid + "'\n"
                + "      ,format(current_timestamp,'yyyyMMdd')\n"
                + "      ,'" + tm + "'\n"
                + "      ,'" + uid + "'\n"
                + "      ,'R'\n"
                + "  FROM [RMShipment].[dbo].[ISMMASD] dt\n"
                + "  WHERE [ISDORD] = '" + orderNo + "'\n"
                + "  AND ISDLINO = '" + lino + "' AND ISDSEQ = '" + seqOld + "' AND ISDSINO = '" + sino + "' \n";
        try {

//            System.out.println(sql);
            PreparedStatement ps = connect.prepareStatement(sql);
            int record = ps.executeUpdate();

            if (record >= 1) {
                result = true;
            } else {
                result = false;
            }

            connect.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;

    }

    public boolean addRemainLineMom(String orderNo, String lino, String seqOld, String seqNew, String uid, String sino) {

        SimpleDateFormat formatter = new SimpleDateFormat("HHmmss");
        Date date = new Date(System.currentTimeMillis());
        String tm = formatter.format(date);

        boolean result = false;

        String sql = "INSERT INTO [ISMMASD] ([ISDCONO]\n"
                + "      ,[ISDORD]\n"
                + "      ,[ISDLINO]\n"
                + "      ,[ISDSEQ]\n"
                + "      ,[ISDSINO]\n"
                + "      ,[ISDITNO]\n"
                + "      ,[ISDPLANT]\n"
                + "      ,[ISDVT]\n"
                + "      ,[ISDSTRG]\n"
                + "      ,[ISDRQQTY]\n"
                + "      ,[ISDUNIT]\n"
                + "      ,[ISDPRICE]\n"
                + "      ,[ISDUNR01]\n"
                + "      ,[ISDUNR03]\n"
                + "      ,[ISDMTCTRL]\n"
                + "      ,[ISDDEST]\n"
                + "      ,[ISDAPUSER]\n"
                + "      ,[ISDSTS]\n"
                + "      ,[ISDJBNO]\n"
                + "      ,[ISDISUNO]\n"
                + "      ,[ISDTEMP]\n"
                + "      ,[ISDEDT]\n"
                + "      ,[ISDETM]\n"
                + "      ,[ISDEUSR]\n"
                + "      ,[ISDCDT]\n"
                + "      ,[ISDCTM]\n"
                + "      ,[ISDUSER]\n"
                + "      ,[ISDREMAIN])\n"
                + "SELECT [ISDCONO]\n"
                + "      ,[ISDORD]\n"
                + "      ,[ISDLINO]\n"
                + "      ,'" + seqNew + "'\n"
                + "      ,[ISDSINO]\n"
                + "      ,[ISDITNO]\n"
                + "      ,[ISDPLANT]\n"
                + "      ,[ISDVT]\n"
                + "      ,[ISDSTRG]\n"
                + "      ,'0'\n"
                + "      ,[ISDUNIT]\n"
                + "      ,[ISDPRICE]\n"
                + "      ,[ISDUNR01]\n"
                + "      ,[ISDUNR03]\n"
                + "      ,[ISDMTCTRL]\n"
                + "      ,[ISDDEST]\n"
                + "      ,[ISDAPUSER]\n"
                + "      ,'0'\n"
                + "      ,[ISDJBNO]\n"
                + "      ,[ISDISUNO]\n"
                + "      ,[ISDTEMP]\n"
                + "      ,format(current_timestamp,'yyyyMMdd')\n"
                + "      ,'" + tm + "'\n"
                + "      ,'" + uid + "'\n"
                + "      ,format(current_timestamp,'yyyyMMdd')\n"
                + "      ,'" + tm + "'\n"
                + "      ,'" + uid + "'\n"
                + "      ,'R'\n"
                + "  FROM [RMShipment].[dbo].[ISMMASD] dt\n"
                + "  WHERE [ISDORD] = '" + orderNo + "'\n"
                + "  AND ISDLINO = '" + lino + "' AND ISDSEQ = '" + seqOld + "' AND ISDSINO = '" + sino + "' \n";
        try {

//            System.out.println(sql);
            PreparedStatement ps = connect.prepareStatement(sql);
            int record = ps.executeUpdate();

            if (record >= 1) {
                result = true;
            } else {
                result = false;
            }

            connect.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;

    }

    public boolean addRemainHead(String orderNo, String seqOld, String seqNew, String uid) {

        SimpleDateFormat formatter = new SimpleDateFormat("HHmmss");
        Date date = new Date(System.currentTimeMillis());
        String tm = formatter.format(date);

        boolean result = false;

        String sql = "INSERT INTO [ISMMASH] ([ISHCONO]\n"
                + "      ,[ISHORD]\n"
                + "      ,[ISHSEQ]\n"
                + "      ,[ISHTRDT]\n"
                + "      ,[ISHUART]\n"
                + "      ,[ISHTWEG]\n"
                + "      ,[ISHMVT]\n"
                + "      ,[ISHCUNO]\n"
                + "      ,[ISHCUNM1]\n"
                + "      ,[ISHCUNM2]\n"
                + "      ,[ISHCUAD1]\n"
                + "      ,[ISHCUAD2]\n"
                + "      ,[ISHBSTKD]\n"
                + "      ,[ISHSUBMI]\n"
                + "      ,[ISHMATLOT]\n"
                + "      ,[ISHTAXNO]\n"
                + "      ,[ISHBRANCH01]\n"
                + "      ,[ISHDATUM]\n"
                + "      ,[ISHSTYLE]\n"
                + "      ,[ISHCOLOR]\n"
                + "      ,[ISHLOT]\n"
                + "      ,[ISHAMTFG]\n"
                + "      ,[ISHSTYLE2]\n"
                + "      ,[ISHNOR]\n"
                + "      ,[ISHPOFG]\n"
                + "      ,[ISHGRPNO]\n"
                + "      ,[ISHWHNO]\n"
                + "      ,[ISHPGRP]\n"
                + "      ,[ISHLSTS]\n"
                + "      ,[ISHHSTS]\n"
                + "      ,[ISHDEST]\n"
                //                + "      ,[ISHRQDT]\n"
                + "      ,[ISHAPUSR]\n"
                + "      ,[ISHP1DT]\n"
                + "      ,[ISHA1DT]\n"
                + "      ,[ISHT1DT]\n"
                + "      ,[ISHJBNO]\n"
                + "      ,[ISHEDT]\n"
                + "      ,[ISHETM]\n"
                + "      ,[ISHEUSR]\n"
                + "      ,[ISHCDT]\n"
                + "      ,[ISHCTM]\n"
                + "      ,[ISHUSER])\n"
                + "SELECT [ISHCONO]\n"
                + "      ,[ISHORD]\n"
                + "      ,'" + seqNew + "'\n"
                + "      ,format(current_timestamp,'yyyyMMdd')\n"
                //                + "      ,[ISHTRDT]\n"
                + "      ,[ISHUART]\n"
                + "      ,[ISHTWEG]\n"
                + "      ,[ISHMVT]\n"
                + "      ,[ISHCUNO]\n"
                + "      ,[ISHCUNM1]\n"
                + "      ,[ISHCUNM2]\n"
                + "      ,[ISHCUAD1]\n"
                + "      ,[ISHCUAD2]\n"
                + "      ,[ISHBSTKD]\n"
                + "      ,[ISHSUBMI]\n"
                + "      ,[ISHMATLOT]\n"
                + "      ,[ISHTAXNO]\n"
                + "      ,[ISHBRANCH01]\n"
                + "      ,[ISHDATUM]\n"
                + "      ,[ISHSTYLE]\n"
                + "      ,[ISHCOLOR]\n"
                + "      ,[ISHLOT]\n"
                + "      ,[ISHAMTFG]\n"
                + "      ,[ISHSTYLE2]\n"
                + "      ,[ISHNOR]\n"
                + "      ,[ISHPOFG]\n"
                + "      ,[ISHGRPNO]\n"
                + "      ,[ISHWHNO]\n"
                + "      ,[ISHPGRP]\n"
                + "      ,'0'\n"
                + "      ,'0'\n"
                + "      ,[ISHDEST]\n"
                //                + "      ,[ISHRQDT]\n"
                + "      ,[ISHAPUSR]\n"
                + "      ,[ISHP1DT]\n"
                + "      ,[ISHA1DT]\n"
                + "      ,[ISHT1DT]\n"
                + "      ,[ISHJBNO]\n"
                + "      ,format(current_timestamp,'yyyyMMdd')\n"
                + "      ,'" + tm + "'\n"
                + "      ,'" + uid + "'\n"
                + "      ,format(current_timestamp,'yyyyMMdd')\n"
                + "      ,'" + tm + "'\n"
                + "      ,'" + uid + "'\n"
                + "  FROM [RMShipment].[dbo].[ISMMASH] dt\n"
                + "  WHERE [ISHORD] = '" + orderNo + "' AND ISHSEQ = '" + seqOld + "' \n";
        try {
            PreparedStatement ps = connect.prepareStatement(sql);
            int record = ps.executeUpdate();

            if (record >= 1) {
                result = true;
            } else {
                result = false;
            }

            connect.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;

    }

    public String SelectMaxSEQbyORDERNo(String orderno) {

        String sts = "";

        String sql = "SELECT (ISNULL(MAX(ISHSEQ),0) + 1) AS SEQNO FROM ISMMASH WHERE ISHORD = '" + orderno + "'";

        try {
            Statement stmtS = connect.createStatement();
            ResultSet rs = stmtS.executeQuery(sql);

            if (rs != null) {
                while (rs.next()) {
                    sts = rs.getString("SEQNO");
                }
            }

            stmtS.close();
            connect.close();

        } catch (SQLException ex) {
            Logger.getLogger(ISM800Dao.class.getName()).log(Level.SEVERE, null, ex);
        }

        return sts;

    }

    public String SelectMaxCurSEQbyORDERNo(String orderno) {

        String sts = "";

        String sql = "SELECT (ISNULL(MAX(ISHSEQ),0)) AS SEQNO FROM ISMMASH WHERE ISHORD = '" + orderno + "'";

        try {
            Statement stmtS = connect.createStatement();
            ResultSet rs = stmtS.executeQuery(sql);

            if (rs != null) {
                while (rs.next()) {
                    sts = rs.getString("SEQNO");
                }
            }

            stmtS.close();
            connect.close();

        } catch (SQLException ex) {
            Logger.getLogger(ISM800Dao.class.getName()).log(Level.SEVERE, null, ex);
        }

        return sts;

    }

    public boolean updateDetRemainLine(String orderNo, String lino, String seq, String uid, String sino) {

        SimpleDateFormat formatter = new SimpleDateFormat("HHmmss");
        Date date = new Date(System.currentTimeMillis());
        String tm = formatter.format(date);

        boolean result = false;

        String sql = "UPDATE [ISMMASD] "
                + "SET [ISDREMAIN] = 'S' "
                + "   ,[ISDEDT] = format(current_timestamp,'yyyyMMdd') "
                + "   ,[ISDETM] = '" + tm + "' "
                + "   ,[ISDEUSR] = '" + uid + "' "
                + " WHERE [ISDORD] = ? "
                + " AND [ISDLINO] = ? AND ISDSEQ = ? AND ISDSINO = ? ";
        try {

//            System.out.println("orderNo " + orderNo + " line " + lino + " seq " + seq);
            PreparedStatement ps = connect.prepareStatement(sql);

            ps.setString(1, orderNo);
            ps.setString(2, lino);
            ps.setString(3, seq);
            ps.setString(4, sino);

            int record = ps.executeUpdate();

            if (record >= 1) {
                result = true;
            } else {
                result = false;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;

    }

    public boolean ckHeadMas(String orderNo, String seq) {

        boolean result = false;

        String sql = "SELECT TOP 1 * FROM [ISMMASH] WHERE [ISHORD] = ? AND ISHSEQ = ? AND ISHLSTS = '0' ";
        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ps.setString(1, orderNo);
            ps.setString(2, seq);

            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                result = true;
            }

            connect.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;

    }

    public boolean ckStsRemain(String orderNo, String seq) {

        boolean result = false;

        String sql = "SELECT TOP 1 ISDREMAIN FROM [RMShipment].[dbo].[ISMMASD] WHERE ISDORD = ? AND ISDSEQ = ? AND ISDREMAIN  = 'R' ";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ps.setString(1, orderNo);
            ps.setString(2, seq);

            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                result = true;
            }

            connect.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;

    }

    public String SelectRQ_QTYFromChild(String orderNo, String lino, String seq, String sino) {

        String qty = "";

        String sql = "SELECT ISNULL([ISDRQQTY],0) AS ISDRQQTY \n"
                + "  FROM [RMShipment].[dbo].[ISMMASD]\n"
                + "  WHERE [ISDORD] = '" + orderNo + "'\n"
                + "  AND ISDLINO = '" + lino + "' AND ISDSEQ = '" + seq + "' AND ISDSINO = '" + sino + "' ORDER BY ISDSINO ";

        try {
            Statement stmtS = connect.createStatement();
            ResultSet rs = stmtS.executeQuery(sql);

            if (rs != null) {
                while (rs.next()) {
                    qty = rs.getString("ISDRQQTY");
                }
            }

            stmtS.close();
            connect.close();

        } catch (SQLException ex) {
            Logger.getLogger(ISM800Dao.class.getName()).log(Level.SEVERE, null, ex);
        }

        return qty;

    }

    public boolean updateReqQTYToMom(String orderNo, String seq, String line, Double qty) {

        SimpleDateFormat formatter = new SimpleDateFormat("HHmmss");
        Date date = new Date(System.currentTimeMillis());
        String tm = formatter.format(date);

        boolean result = false;

        String sql = "UPDATE [ISMMASD] "
                + "SET [ISDRQQTY] = ? "
                + " WHERE [ISDORD] = ? AND [ISDSEQ] = ? AND [ISDLINO] = ? AND ISDSINO = '0' ";

//        System.out.println("order " + orderNo + " seq " + seq + " line " + line + " qty " + qty);
//        System.out.println(sql);
        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ps.setDouble(1, qty);
            ps.setString(2, orderNo);
            ps.setString(3, seq);
            ps.setString(4, line);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean updateRespStyHead(String orderNo, String seq, String uid, String Resptext) {

        SimpleDateFormat formatter = new SimpleDateFormat("HHmmss");
        Date date = new Date(System.currentTimeMillis());
        String tm = formatter.format(date);

        boolean result = false;

        String sql = "UPDATE [ISMMASH] "
                + "SET [ISHRPUSR] = ? "
                + "   ,[ISHEDT] = format(current_timestamp,'yyyyMMdd') "
                + "   ,[ISHETM] = '" + tm + "' "
                //                + "   ,[ISHEUSR] = '" + uid + "' "
                + " WHERE [ISHORD] = ? AND ISHSEQ = ? ";
        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ps.setString(1, Resptext.trim());
            ps.setString(2, orderNo);
            ps.setString(3, seq);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public String getStatus(String orderno, String seq) {

        String level = "0";

        String sql = "SELECT ISHLSTS,ISHHSTS FROM ISMMASH WHERE ISHORD = '" + orderno + "' AND ISHSEQ = '" + seq + "'\n";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {
                int hsts = Integer.parseInt(result.getString("ISHHSTS"));
                int lsts = Integer.parseInt(result.getString("ISHLSTS"));

                level = String.valueOf(hsts + lsts);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return level;

    }

}
