/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.dao;

import com.twc.wms.database.database;
import java.sql.*;

import java.util.ArrayList;

import java.util.List;

import com.twc.wms.entity.QPGTYPE;

/**
 *
 * @author nutthawoot.noo
 */
public class QPGTYPEDao extends database {
    
    public List<QPGTYPE> selectPGType() {

        List<QPGTYPE> WHList = new ArrayList<QPGTYPE>();

        String sql = "SELECT * FROM QPGTYPE";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                QPGTYPE p = new QPGTYPE();

                p.setCode(result.getString("QPGTYPE"));

                p.setName(result.getString("QPGDESC"));

                WHList.add(p);

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return WHList;

    }
    
    public String findPGTname(String id) {

        String pgtname = "";

        String sql = "SELECT * FROM QPGTYPE WHERE QPGTYPE = ?";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ps.setString(1, id);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                pgtname = result.getString("QPGDESC");

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return pgtname;

    }
    
    
}
