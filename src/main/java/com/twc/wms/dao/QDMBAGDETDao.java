/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.dao;

import com.twc.wms.database.database;
import com.twc.wms.entity.QDESTYP;
import com.twc.wms.entity.QDMBAG;
import com.twc.wms.entity.QDMBAGDET;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 *
 * @author nutthawoot.noo
 */
public class QDMBAGDETDao extends database {

    public String checkDet(String id) {
        PreparedStatement ps = null;
        ResultSet rs = null;
        String STRRETURN = null;
        try {
            String check = "SELECT [QDMBCOM]\n"
                    + "      ,[QDMBSTYLE]\n"
                    + "      ,[QDMBLINO]\n"
                    + "      ,[QDMBQTY]\n"
                    + "      ,[QDMBHEIGHT]\n"
                    + "      ,[QDMBEDT]\n"
                    + "      ,[QDMBCDT]\n"
                    + "      ,[QDMBUSER]\n"
                    + "      ,[QDMBID]\n"
                    + "  FROM [RMShipment].[dbo].[QDMBAGDET]\n"
                    + "  WHERE [QDMBID] = '" + id + "'";
//            System.out.println(check);
            ps = connect.prepareStatement(check);
            rs = ps.executeQuery();
            if (rs.next()) {
                STRRETURN = "f";
            } else {
                STRRETURN = "t";
            }
            connect.close();

        } catch (Exception exp) {
            exp.printStackTrace();
        }
        return STRRETURN;
    }

    public boolean updateBagDet(String code, String line, String qty, String height, String uid) {

        boolean result = false;

        if (uid.equals("null")) {
            uid = "";
        }

        String sql = "UPDATE [RMShipment].[dbo].[QDMBAGDET]\n"
                + "  SET [QDMBQTY] = '" + qty.replace("NaN", "0") + "'\n"
                + "      ,[QDMBHEIGHT] = '" + height.replace("NaN", "0") + "'\n"
                + "      ,[QDMBEDT] = CURRENT_TIMESTAMP\n"
                + "      ,[QDMBUSER] = '" + uid + "'\n"
                + "  WHERE [QDMBSTYLE] = '" + code + "'\n"
                + "  AND [QDMBLINO] = '" + line + "'";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public List<QDMBAGDET> findByStyle(String style) {

        List<QDMBAGDET> QDMBAGDETList = new ArrayList<QDMBAGDET>();

        String sql = "SELECT [QDMBSTYLE]\n"
                + "      ,[QDMBLINO]\n"
                + "      ,[QDMBQTY]\n"
                + "      ,[QDMBHEIGHT]\n"
                + "  FROM [RMShipment].[dbo].[QDMBAGDET]\n"
                + "  WHERE [QDMBSTYLE] = '" + style + "'";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                QDMBAGDET p = new QDMBAGDET();

                p.setQDMBSTYLE(result.getString("QDMBSTYLE"));
                p.setQDMBLINO(result.getString("QDMBLINO"));
                p.setQDMBQTY("<input type=\"text\" id=\"" + result.getString("QDMBLINO") + "-detQty\" name=\"detQty\" style=\"width:100px; text-align:right;\" value=\"" + result.getString("QDMBQTY") + "\">");
                p.setQDMBHEIGHT("<input type=\"text\" id=\"" + result.getString("QDMBLINO") + "-detHeight\" name=\"detHeight\" style=\"width:100px; text-align:right;\" value=\"" + (result.getString("QDMBHEIGHT").equals("0.00") ? "" : result.getString("QDMBHEIGHT")) + "\">");

                p.setOption("<a title=\"Delete\" style=\" cursor: pointer;\" onclick=\"DeleteDet('" + result.getString("QDMBSTYLE") + "','" + result.getString("QDMBLINO") + "');\"><i class=\"fa fa-times-circle-o\" aria-hidden=\"true\" style=\"font-size:25px; color: #e51e00;\"></i></a>");

                QDMBAGDETList.add(p);

            }
//
//            if (QDMBAGDETList.isEmpty()) {
//                QDMBAGDET p = new QDMBAGDET();
//
//                p.setQDMBSTYLE("1");
//                p.setQDMBLINO("");
//                p.setQDMBQTY("");
//                p.setQDMBHEIGHT("");
//
//                QDMBAGDETList.add(p);
//            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return QDMBAGDETList;

    }

    public boolean addDet(String code, String qty, String uid, String qrmid) {

        boolean result = false;

        if (uid.equals("null")) {
            uid = "";
        }

        String sql = "INSERT INTO [QDMBAGDET]"
                + " ([QDMBCOM]\n"
                + "      ,[QDMBSTYLE]\n"
                + "      ,[QDMBLINO]\n"
                + "      ,[QDMBQTY]\n"
                + "      ,[QDMBHEIGHT]\n"
                + "      ,[QDMBEDT]\n"
                + "      ,[QDMBCDT]\n"
                + "      ,[QDMBUSER],[QDMBID])"
                + " VALUES('TWC'"
                + ",'" + code + "'"
                + ",(SELECT ISNULL(MAX(QDMBLINO),0)+1 FROM QDMBAGDET WHERE QDMBSTYLE = '" + code + "')"
                + ",'" + qty + "'"
                + ",'0'"
                + ",CURRENT_TIMESTAMP"
                + ",CURRENT_TIMESTAMP"
                + ",'" + uid + "','" + qrmid + "')";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean add(String size, String desc, String inWidth, String inLength, String uid) {

        boolean result = false;

        if (uid.equals("null")) {
            uid = "";
        }

        String sql = "INSERT INTO [QDMBAG]"
                + " ([QDMCOM]\n"
                + "      ,[QDMSIZE]\n"
                + "      ,[QDMDESC]\n"
                + "      ,[QDMINWIDTH]\n"
                + "      ,[QDMINLENGTH]\n"
                + "      ,[QDMCMWIDTH]\n"
                + "      ,[QDMCMLENGTH]\n"
                + "      ,[QDMEDT]\n"
                + "      ,[QDMCDT]\n"
                + "      ,[QDMUSER])"
                + " VALUES('TWC'"
                + ",'" + size + "'"
                + ",'" + desc + "'"
                + "," + inWidth
                + "," + inLength
                + "," + inWidth + "*2.54"
                + "," + inLength + "*2.54"
                + ",CURRENT_TIMESTAMP"
                + ",CURRENT_TIMESTAMP"
                + ",'" + uid + "')";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean edit(String size, String desc, String inWidth, String inLength, String uid) {

        boolean result = false;

        if (uid.equals("null")) {
            uid = "";
        }

        String sql = "UPDATE [QDMBAG]\n"
                + "SET [QDMDESC] = '" + desc + "'"
                + "      ,[QDMINWIDTH] = " + inWidth
                + "      ,[QDMINLENGTH] = " + inLength
                + "      ,[QDMCMWIDTH] = " + inWidth + "*2.54"
                + "      ,[QDMCMLENGTH] = " + inLength + "*2.54"
                + "      ,[QDMEDT] = CURRENT_TIMESTAMP\n"
                + "      ,[QDMUSER] = '" + uid + "'"
                + "WHERE QDMSIZE = '" + size + "'";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public void deleteDet(String code, String line) {

        String sql = "DELETE FROM [QDMBAGDET] "
                + "WHERE [QDMBSTYLE] = '" + code + "'\n"
                + "  AND [QDMBLINO] = '" + line + "'";

        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            ps.executeUpdate();

        } catch (Exception e) {

            e.printStackTrace();

        }

    }

    public void delete(String size) {

        String sql = "DELETE FROM QDMBAG "
                + "WHERE QDMSIZE = '" + size + "'";

        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            ps.executeUpdate();

        } catch (Exception e) {

            e.printStackTrace();

        }

    }
}
