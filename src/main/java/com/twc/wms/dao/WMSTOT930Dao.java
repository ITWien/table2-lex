/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.dao;

import com.twc.wms.database.database;
import com.twc.wms.entity.WMSTOT930;

import java.sql.*;

import java.util.ArrayList;

import java.util.List;

import com.twc.wms.entity.XMSRMR;

/**
 *
 * @author nutthawoot.noo
 */
public class WMSTOT930Dao extends database {

    public boolean add(WMSTOT930 tot) {

        boolean result = false;

        String sql = "INSERT INTO [TOT930] ([TCOM]\n"
                + "      ,[TOHDT]\n"
                + "      ,[TPLANT]\n"
                + "      ,[TWHSE]\n"
                + "      ,[TMTCL]\n"
                + "      ,[TSKU]\n"
                + "      ,[TPCK]\n"
                + "      ,[TPER]\n"
                + "      ,[TAMT]\n"
                + "      ,[TCOSKU]\n"
                + "      ,[TCOPCK]\n"
                + "      ,[TCOPER]\n"
                + "      ,[TCOAMT]\n"
                + "      ,[TCRSKU]\n"
                + "      ,[TCRPCK]\n"
                + "      ,[TCRPER]\n"
                + "      ,[TCRAMT]\n"
                + "      ,[TICSKU]\n"
                + "      ,[TICPCK]\n"
                + "      ,[TICPER]\n"
                + "      ,[TICAMT]\n"
                + "      ,[TDMSKU]\n"
                + "      ,[TDMPCK]\n"
                + "      ,[TDMPER]\n"
                + "      ,[TDMAMT]\n"
                + "      ,[TDPSKU]\n"
                + "      ,[TDPPCK]\n"
                + "      ,[TDPPER]\n"
                + "      ,[TDPAMT]\n"
                + "      ,[TCDT]\n"
                + "      ,[TUSER])\n"
                + "VALUES (\n"
                + "'" + tot.getTCOM() + "',"
                + "'" + tot.getTOHDT() + "',"
                + "'" + tot.getTPLANT() + "',"
                + "'" + tot.getTWHSE() + "',"
                + "'" + tot.getTMTCL() + "',"
                + "'" + tot.getTSKU() + "',"
                + "'" + tot.getTPCK() + "',"
                + "'" + tot.getTPER() + "',"
                + "'" + tot.getTAMT() + "',"
                + "'" + tot.getTCOSKU() + "',"
                + "'" + tot.getTCOPCK() + "',"
                + "'" + tot.getTCOPER() + "',"
                + "'" + tot.getTCOAMT() + "',"
                + "'" + tot.getTCRSKU() + "',"
                + "'" + tot.getTCRPCK() + "',"
                + "'" + tot.getTCRPER() + "',"
                + "'" + tot.getTCRAMT() + "',"
                + "'" + tot.getTICSKU() + "',"
                + "'" + tot.getTICPCK() + "',"
                + "'" + tot.getTICPER() + "',"
                + "'" + tot.getTICAMT() + "',"
                + "'" + tot.getTDMSKU() + "',"
                + "'" + tot.getTDMPCK() + "',"
                + "'" + tot.getTDMPER() + "',"
                + "'" + tot.getTDMAMT() + "',"
                + "'" + tot.getTDPSKU() + "',"
                + "'" + tot.getTDPPCK() + "',"
                + "'" + tot.getTDPPER() + "',"
                + "'" + tot.getTDPAMT() + "',"
                + "CURRENT_TIMESTAMP,"
                + "'" + tot.getTUSER() + "'"
                + ")";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean delete(WMSTOT930 tot) {

        boolean result = false;

        String sql = "DELETE [TOT930]\n"
                + "WHERE [TOHDT] = '" + tot.getTOHDT() + "'\n"
                + "AND [TPLANT] = '" + tot.getTPLANT() + "'\n"
                + "AND [TWHSE] = '" + tot.getTWHSE() + "'\n"
                + "AND [TMTCL] = '" + tot.getTMTCL() + "'";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }
}
