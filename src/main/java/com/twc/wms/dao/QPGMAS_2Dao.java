/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.dao;

import com.twc.wms.database.database;

import java.sql.*;

import java.util.ArrayList;

import java.util.List;

import com.twc.wms.entity.QPGMAS_2;
import com.twc.wms.entity.UserAuth;
import java.util.Calendar;

/**
 *
 * @author nutthawoot.noo
 */
public class QPGMAS_2Dao extends database {

    public List<QPGMAS_2> findAll() {

        List<QPGMAS_2> UAList = new ArrayList<QPGMAS_2>();

        String sql = "SELECT * FROM QPGMAS "
                + "INNER JOIN QPGTYPE "
                + "ON QPGMAS.QPGTYPE = QPGTYPE.QPGTYPE";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                QPGMAS_2 p = new QPGMAS_2();

                p.setUid(result.getString("QPGID"));

                p.setName(result.getString("QPGNAME"));

                p.setType(result.getString("QPGTYPE") + " : " + result.getString("QPGDESC"));

                UserDao dao3 = new UserDao();
                UserAuth us = dao3.findByUid(result.getString("QPGUSER"));

                if (result.getString("QPGUSER") != null) {
                    p.setRespon(result.getString("QPGUSER") + " : " + us.getName());
                } else {
                    p.setRespon(result.getString("QPGUSER"));
                }

                UAList.add(p);

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public QPGMAS_2 findByUid(String id) {

        QPGMAS_2 ua = new QPGMAS_2();

        String sql = "SELECT * FROM QPGMAS "
                + "LEFT JOIN QPGTYPE "
                + "ON QPGMAS.QPGTYPE = QPGTYPE.QPGTYPE "
                + "LEFT JOIN QPGSYS "
                + "ON QPGMAS.QPGSMNU = QPGSYS.QPGSMNU "
                + "WHERE QPGID = ? ";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ps.setString(1, id);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                ua.setUid(result.getString("QPGID"));
                ua.setName(result.getString("QPGNAME"));
                ua.setSys(result.getString("QPGSMNU") + " : " + result.getString("QPGSYS") + " - " + result.getString("QPGSMNUNAME"));
                ua.setType(result.getString("QPGTYPE") + " : " + result.getString("QPGDESC"));
                ua.setRespon(result.getString("QPGUSER"));

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return ua;

    }

    public String check(String pid) {
        PreparedStatement ps = null;
        ResultSet rs = null;
        String STRRETURN = null;
        try {
            String check = "Select * FROM [RMShipment].[dbo].[QPGMAS] where [QPGID] = '" + pid + "'";
            ps = connect.prepareStatement(check);
            rs = ps.executeQuery();
            if (rs.next()) {
                STRRETURN = "f";
            } else {
                STRRETURN = "t";
            }
        } catch (Exception exp) {
            exp.printStackTrace();
        }
        return STRRETURN;
    }

    public boolean add(QPGMAS_2 ua, String uid, String sys) {

        boolean result = false;

        String sql = "INSERT INTO QPGMAS"
                + " (QPGCOM, QPGID, QPGNAME, QPGTYPE, QPGEDT, QPGCDT, QPGUSER, QPGSMNU)"
                + " VALUES(?, ?, ?, ?, ?, ?, ?, ?)";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ps.setString(1, "TWC");

            ps.setString(2, ua.getUid());

            ps.setString(3, ua.getName());

            ps.setString(4, ua.getType());

            Calendar cal = Calendar.getInstance();
            Timestamp timestamp = new Timestamp(cal.getTimeInMillis());

            ps.setTimestamp(5, timestamp);

            ps.setTimestamp(6, timestamp);

            ps.setString(7, uid);

            ps.setString(8, sys);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean edit(QPGMAS_2 ua, String sys) {

        boolean result = false;

        String sql = "UPDATE QPGMAS "
                + "SET QPGNAME = ?, QPGTYPE = ?, QPGEDT = ?, QPGUSER = ?, QPGSMNU = ?"
                + " WHERE QPGID = ?";
        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ps.setString(6, ua.getUid());
            ps.setString(1, ua.getName());
            ps.setString(2, ua.getType());

            Calendar cal = Calendar.getInstance();
            Timestamp timestamp = new Timestamp(cal.getTimeInMillis());

            ps.setTimestamp(3, timestamp);
            ps.setString(4, ua.getRespon());
            ps.setString(5, sys);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public void delete(String pid) {

        String sql = "DELETE FROM QPGMAS WHERE QPGID = ?";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ps.setString(1, pid);

            ps.executeUpdate();

        } catch (Exception e) {

            e.printStackTrace();

        }

    }
}
