/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.dao;

import com.twc.wms.database.MSSQLDbConnectionPool;
import com.twc.wms.database.database;

import java.sql.*;

import java.util.ArrayList;

import java.util.List;

import com.twc.wms.entity.MSSMATN;
import com.twc.wms.entity.QIDETAIL;

import com.twc.wms.entity.QIHEAD;
import java.text.DecimalFormat;

/**
 *
 * @author nutthawoot.noo
 */
public class QIHEADDao extends database {

    public boolean UpdateMvtDest(String mvt, String dest, String wh, String qno) {

        boolean result = false;

        String sql = "UPDATE QIHEAD "
                + "SET QIHMVT = '" + mvt + "', QIHDEST = '" + dest + "' "
                + "WHERE QIHWHS = '" + wh + "' "
                + "AND QIHQNO = '" + qno + "' ";

        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean UPQTY(String wh, String qno, String tqty) {

        boolean result = false;

        String sql = "UPDATE QIHEAD "
                + "SET QIHTOTQTY = '" + tqty + "' "
                + "WHERE QIHWHS = '" + wh + "' "
                + "AND QIHQNO = '" + qno + "' ";

        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean SETSTS(String max, String min, String wh, String qno, String tqty) {

        boolean result = false;

        String sql = "UPDATE QIHEAD "
                + "SET QIHSTS = '" + min + "', QIHHISTS = '" + max + "', QIHTOTQTY = '" + tqty + "' "
                + "WHERE QIHWHS = '" + wh + "' "
                + "AND QIHQNO = '" + qno + "' ";

        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean SETSTS2(String wh, String qno) {

        boolean result = false;

        String sql = "UPDATE QIHEAD "
                + "SET QIHSTS = '8', QIHHISTS = '8', QIHEDT = CURRENT_TIMESTAMP "
                + "WHERE QIHWHS = '" + wh + "' "
                + "AND QIHQNO = '" + qno + "' ";
//        System.out.println(sql);

        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public QIHEAD findByQno(String wh, String qno) {

        DecimalFormat formatDou = new DecimalFormat("#,##0.000");

        QIHEAD p = new QIHEAD();

        String sql = "SELECT QIHWHS, QWHNAME, SUBSTRING(CONVERT(VARCHAR(25), QIHTRDT, 126), 1, 10) as QIHTRDT, "
                + "       QIHQNO, QIHMVT, QMVDESC, QIHDEST + ' : ' + QDEST.QDEDESC as QIHDEST,  "
                + "	   QIHUSER + ' : ' + SUBSTRING(MSSUSER.USERS,0,CHARINDEX(' ',MSSUSER.USERS,0)) as QIHUSER,  "
                + "	   QIHSET, QSETHD.QSETOTSQ, QIHPDGP, QIHMTCTRL + ' : ' + MSSMATN.MATCNAME as QIHMTCTRL,  "
                + "	   QIHSTS + ' : ' + QRMSTS.STSDESC as QIHSTS, QIHSTS as STS, QIHTOTQTY "
                + "FROM QIHEAD "
                + "INNER JOIN MSSMATN "
                + "ON QIHEAD.QIHMTCTRL = MSSMATN.LGPBE "
                + "INNER JOIN MSSUSER "
                + "ON QIHEAD.QIHUSER = MSSUSER.USERID  "
                + "INNER JOIN QDEST "
                + "ON QIHEAD.QIHDEST = QDEST.QDECOD  "
                + "LEFT JOIN QRMSTS "
                + "ON QIHEAD.QIHSTS = QRMSTS.STSCODE "
                + "LEFT JOIN QSETHD "
                + "ON QIHEAD.QIHSET = QSETHD.QSECOD "
                + "LEFT JOIN QMVT "
                + "ON QIHEAD.QIHMVT = QMVT.QMVMVT "
                + "LEFT JOIN QWHMAS "
                + "ON QIHEAD.QIHWHS = QWHMAS.QWHCOD "
                + "WHERE QIHWHS = '" + wh + "' "
                + "AND QIHQNO = '" + qno + "' "
                + "ORDER BY QIHTRDT ";

        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                p.setWh(result.getString("QIHWHS"));
                p.setWhn(result.getString("QWHNAME"));
                p.setTransDate(result.getString("QIHTRDT"));
                p.setQueueNo(result.getString("QIHQNO"));
                p.setMvt(result.getString("QIHMVT"));
                p.setMvtn(result.getString("QMVDESC"));
                p.setDest(result.getString("QIHDEST"));
                p.setUser(result.getString("QIHUSER"));
                p.setSet(result.getString("QIHSET"));
                p.setSetTotal(result.getString("QSETOTSQ"));
                p.setProductGroup(result.getString("QIHPDGP"));
                p.setMatCtrl(result.getString("QIHMTCTRL"));
                p.setStatus(result.getString("QIHSTS"));

                double tot = Double.parseDouble(result.getString("QIHTOTQTY"));
                String tqt = formatDou.format(tot);

                p.setTotalQty(tqt);

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return p;

    }

    public List<QIHEAD> findAll(String wh, String mc) {

        List<QIHEAD> UAList = new ArrayList<QIHEAD>();

        DecimalFormat formatDou = new DecimalFormat("#,##0.000");

        String sql = "SELECT QIHWHS, SUBSTRING(CONVERT(VARCHAR(25), QIHTRDT, 126), 1, 10) as QIHTRDT, "
                + "       QIHQNO, QIHMVT, QIHDEST + ' : ' + QDEST.QDEDESC as QIHDEST,  "
                + "	   QIHUSER + ' : ' + SUBSTRING(MSSUSER.USERS,0,CHARINDEX(' ',MSSUSER.USERS,0)) as QIHUSER,  "
                + "	   QIHSET, QSETHD.QSETOTSQ, QIHPDGP, QIHMTCTRL + ' : ' + MSSMATN.MATCNAME as QIHMTCTRL,  "
                + "	   QIHSTS + ' : ' + QRMSTS.STSDESC as QIHSTS, QIHSTS as STS, "
                + "        QIHHISTS + ' : ' + HI.STSDESC as QIHHISTS, QIHHISTS as HISTS, "
                + "        QIHTOTQTY "
                + "FROM QIHEAD "
                + "INNER JOIN MSSMATN "
                + "ON QIHEAD.QIHMTCTRL = MSSMATN.LGPBE "
                + "INNER JOIN MSSUSER "
                + "ON QIHEAD.QIHUSER = MSSUSER.USERID  "
                + "INNER JOIN QDEST "
                + "ON QIHEAD.QIHDEST = QDEST.QDECOD  "
                + "LEFT JOIN QRMSTS "
                + "ON QIHEAD.QIHSTS = QRMSTS.STSCODE "
                + "LEFT JOIN QRMSTS HI "
                + "ON QIHEAD.QIHHISTS = HI.STSCODE "
                + "LEFT JOIN QSETHD "
                + "ON QIHEAD.QIHSET = QSETHD.QSECOD "
                + "WHERE QIHWHS = '" + wh + "' "
                + "AND QIHMTCTRL = '" + mc + "' "
                + "AND (QIHSTS = '1' OR QIHSTS = '0')"
                + "ORDER BY QIHTRDT DESC, QIHQNO DESC ";

//        System.out.println(sql);
        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                QIHEAD p = new QIHEAD();

                p.setWh(result.getString("QIHWHS"));
                p.setTransDate(result.getString("QIHTRDT"));
                p.setQueueNo(result.getString("QIHQNO"));
                p.setMvt(result.getString("QIHMVT"));
                p.setDest(result.getString("QIHDEST"));
                p.setUser(result.getString("QIHUSER"));
                p.setSet(result.getString("QIHSET"));
                p.setSetTotal(result.getString("QSETOTSQ"));
                p.setProductGroup(result.getString("QIHPDGP"));
                p.setMatCtrl(result.getString("QIHMTCTRL"));
                p.setStatus(result.getString("QIHSTS"));
                p.setHistatus(result.getString("QIHHISTS"));

                double tot = Double.parseDouble(result.getString("QIHTOTQTY"));
                String tqt = formatDou.format(tot);

                p.setTotalQty(tqt);

                String ck = "";
                String editOpt = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
                if (result.getString("STS") != null) {
                    if (result.getString("HISTS") != null) {
                        if (!result.getString("HISTS").trim().equals("C") || !result.getString("STS").trim().equals("C")) {
                            if (!result.getString("HISTS").trim().equals("6") || !result.getString("STS").trim().equals("6")) {
                                if (!result.getString("HISTS").trim().equals("5") || !result.getString("STS").trim().equals("5")) {
                                    if (!result.getString("HISTS").trim().equals("2") || !result.getString("STS").trim().equals("2")) {
                                        editOpt = "<a href=\"edit?qno=" + result.getString("QIHQNO").trim() + "&wh=" + result.getString("QIHWHS").trim() + "\" class=\"btn btn-default\" style=\"padding: 3px 6px;\" >\n"
                                                + "   <i class=\"fa fa-pencil fa-lg\" style=\"color:darkblue;\"></i>\n"
                                                + "</a>";
                                        ck = "<input style=\"width: 30px; height: 30px;\" type=\"checkbox\" name=\"selectCk\" value=\"" + result.getString("QIHQNO") + "-" + result.getString("QIHWHS") + "-" + result.getString("QIHMVT") + "\">";
                                    }
                                }
                            }
                        }
                    }
                }

                p.setEditOption(editOpt);
                p.setCkBox(ck);

                UAList.add(p);

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public List<QIHEAD> findAllWMS311(String wh, String mc) {

        List<QIHEAD> UAList = new ArrayList<QIHEAD>();

        DecimalFormat formatDou = new DecimalFormat("#,##0.000");

        String sql = "SELECT QIHWHS, SUBSTRING(CONVERT(VARCHAR(25), QIHTRDT, 126), 1, 10) as QIHTRDT, "
                + "       QIHQNO, QIHMVT, QIHDEST + ' : ' + QDEST.QDEDESC as QIHDEST,  "
                + "	   QIHUSER + ' : ' + SUBSTRING(MSSUSER.USERS,0,CHARINDEX(' ',MSSUSER.USERS,0)) as QIHUSER,  "
                + "	   QIHSET, QSETHD.QSETOTSQ, QIHPDGP, QIHMTCTRL + ' : ' + MSSMATN.MATCNAME as QIHMTCTRL,  "
                + "	   QIHSTS + ' : ' + QRMSTS.STSDESC as QIHSTS, QIHSTS as STS, "
                + "        QIHHISTS + ' : ' + HI.STSDESC as QIHHISTS, QIHHISTS as HISTS, "
                + "        QIHTOTQTY "
                + "FROM QIHEAD "
                + "INNER JOIN MSSMATN "
                + "ON QIHEAD.QIHMTCTRL = MSSMATN.LGPBE "
                + "INNER JOIN MSSUSER "
                + "ON QIHEAD.QIHUSER = MSSUSER.USERID  "
                + "INNER JOIN QDEST "
                + "ON QIHEAD.QIHDEST = QDEST.QDECOD  "
                + "LEFT JOIN QRMSTS "
                + "ON QIHEAD.QIHSTS = QRMSTS.STSCODE "
                + "LEFT JOIN QRMSTS HI "
                + "ON QIHEAD.QIHHISTS = HI.STSCODE "
                + "LEFT JOIN QSETHD "
                + "ON QIHEAD.QIHSET = QSETHD.QSECOD "
                + "WHERE QIHWHS = '" + wh + "' "
                + "AND QIHMTCTRL = '" + mc + "' "
                + "AND (QIHSTS = 'R')"
                + "ORDER BY QIHTRDT DESC, QIHQNO DESC ";

//        System.out.println(sql);
        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                QIHEAD p = new QIHEAD();

                p.setWh(result.getString("QIHWHS"));
                p.setTransDate(result.getString("QIHTRDT"));
                p.setQueueNo(result.getString("QIHQNO"));
                p.setMvt(result.getString("QIHMVT"));
                p.setDest(result.getString("QIHDEST"));
                p.setUser(result.getString("QIHUSER"));
                p.setSet(result.getString("QIHSET"));
                p.setSetTotal(result.getString("QSETOTSQ"));
                p.setProductGroup(result.getString("QIHPDGP"));
                p.setMatCtrl(result.getString("QIHMTCTRL"));
                p.setStatus(result.getString("QIHSTS"));
                p.setHistatus(result.getString("QIHHISTS"));

                double tot = Double.parseDouble(result.getString("QIHTOTQTY"));
                String tqt = formatDou.format(tot);

                p.setTotalQty(tqt);

                String ck = "";
                String editOpt = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
                if (result.getString("STS") != null) {
                    if (result.getString("HISTS") != null) {
                        if (result.getString("HISTS").trim().equals("R") || result.getString("STS").trim().equals("R")) {
                            editOpt = "<a href=\"edit?qno=" + result.getString("QIHQNO").trim() + "&wh=" + result.getString("QIHWHS").trim() + "\" class=\"btn btn-default\" style=\"padding: 3px 6px;\" >\n"
                                    + "   <i class=\"fa fa-pencil fa-lg\" style=\"color:darkblue;\"></i>\n"
                                    + "</a>";
                            ck = "<input type=\"checkbox\" name=\"selectCk\" value=\"" + result.getString("QIHQNO") + "-" + result.getString("QIHWHS") + "-" + result.getString("QIHMVT") + "\">";
                        }
                    }
                }

                p.setEditOption(editOpt);
                p.setCkBox(ck);

                UAList.add(p);

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public List<MSSMATN> findByUid(String whF, String whT) {

        List<MSSMATN> UAList = new ArrayList<MSSMATN>();
        List<String> TMP = new ArrayList<String>();

        DecimalFormat formatDou = new DecimalFormat("#,##0.000");

        String TRDT = "";
        String WHS = "";

        if (!whF.equals("")) {
            if (!whF.equals("TWC") && !whT.equals("TWC")) {
                WHS = "AND QIHWHS BETWEEN '" + whF + "' AND '" + whT + "' \n";
            }
        }

//        if (!sd.equals("")) {
//            TRDT = "AND (SUBSTRING(CONVERT(VARCHAR(25), QIHTRDT, 126), 1, 10) BETWEEN '" + sd + "' AND '" + ed + "') ";
//        }
        String sql = "SELECT QIHWHS, QWHNAME, SUBSTRING(CONVERT(VARCHAR(25), QIHTRDT, 126), 1, 10) as QIHTRDT, QIHMTCTRL, MATCNAME, \n"
                + "(SELECT COUNT(QIHQNO) as TOTALQNO FROM QIHEAD WHERE QIHWHS = HD.QIHWHS AND QIHMTCTRL = HD.QIHMTCTRL AND (QIHSTS = '1' OR QIHSTS = '0')) as TOTALQNO, \n"
                + "(SELECT COUNT(QIDQTY)\n"
                + "FROM(\n"
                + "SELECT (SELECT TOP 1 QIHWHS FROM QIHEAD WHERE QIHQNO = QIDQNO) as WHS, \n"
                + "       (SELECT TOP 1 QIHMTCTRL FROM QIHEAD WHERE QIHQNO = QIDQNO) as MATCT, \n"
                + "	   QIDQNO, QIDQTY, QIDSTS\n"
                + "FROM QIDETAIL) TMP\n"
                + "WHERE WHS != 'TWC'\n"
                + "AND WHS = HD.QIHWHS\n"
                + "AND MATCT = HD.QIHMTCTRL\n"
                + "AND (QIDSTS = '0' OR QIDSTS = '1')) as TOTALLIST \n"
                + "FROM QIHEAD HD \n"
                + "INNER JOIN MSSMATN \n"
                + "ON HD.QIHMTCTRL = MSSMATN.LGPBE \n"
                + "INNER JOIN QWHMAS \n"
                + "ON HD.QIHWHS = QWHMAS.QWHCOD \n"
                + "WHERE (QIHSTS = '1' OR QIHSTS = '0') \n"
                + TRDT
                + WHS
                + "AND ISNULL(HD.QIHMVT,'') != 'XXX'\n"
                + "ORDER BY QIHWHS, QIHMTCTRL \n";

//        System.out.println(sql);
        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            String oldWH = "";
            int iter = 1;

            while (result.next()) {

                if (oldWH.equals("")) {
                    oldWH = result.getString("QIHWHS");
                }

                if (!TMP.contains(result.getString("QIHMTCTRL"))) {

                    MSSMATN p = new MSSMATN();
                    p.setUid(result.getString("QIHMTCTRL"));
                    p.setWarehouse(result.getString("QIHWHS"));
                    p.setTolQno(result.getString("TOTALQNO"));
                    p.setTolQty(result.getString("TOTALLIST"));
                    p.setDesc(result.getString("MATCNAME"));

                    if (!result.getString("QIHWHS").equals(oldWH)) {
                        String newWHS = "<hr>\n"
                                + "<b>" + result.getString("QIHWHS") + " : " + result.getString("QWHNAME") + "</b><br>";
                        p.setName(newWHS);
                        oldWH = result.getString("QIHWHS");
                    } else if (iter == 1) {
                        String newWHS = "<b>" + result.getString("QIHWHS") + " : " + result.getString("QWHNAME") + "</b><br>";
                        p.setName(newWHS);
                        oldWH = result.getString("QIHWHS");
                    }

                    TMP.add(result.getString("QIHMTCTRL"));
                    UAList.add(p);
                }
                iter++;
            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public List<MSSMATN> findByUidWMS311(String whF, String whT) {

        List<MSSMATN> UAList = new ArrayList<MSSMATN>();
        List<String> TMP = new ArrayList<String>();

        DecimalFormat formatDou = new DecimalFormat("#,##0.000");

        String TRDT = "";
        String WHS = "";

        if (!whF.equals("")) {
            if (!whF.equals("TWC") && !whT.equals("TWC")) {
                WHS = "AND QIHWHS BETWEEN '" + whF + "' AND '" + whT + "' ";
            }
        }

//        if (!sd.equals("")) {
//            TRDT = "AND (SUBSTRING(CONVERT(VARCHAR(25), QIHTRDT, 126), 1, 10) BETWEEN '" + sd + "' AND '" + ed + "') ";
//        }
        String sql = "SELECT QIHWHS, QWHNAME, SUBSTRING(CONVERT(VARCHAR(25), QIHTRDT, 126), 1, 10) as QIHTRDT, QIHMTCTRL, MATCNAME, "
                + "(SELECT COUNT(QIHQNO) as TOTALQNO FROM QIHEAD WHERE QIHWHS = HD.QIHWHS AND QIHMTCTRL = HD.QIHMTCTRL AND (QIHSTS = 'R')) as TOTALQNO, "
                + "CASE WHEN (SELECT SUM(QIDQTY)\n"
                + "FROM(\n"
                + "SELECT (SELECT TOP 1 QIHWHS FROM QIHEAD WHERE QIHQNO = QIDQNO) as WHS, \n"
                + "       (SELECT TOP 1 QIHMTCTRL FROM QIHEAD WHERE QIHQNO = QIDQNO) as MATCT, \n"
                + "	   QIDQNO, QIDQTY, QIDSTS\n"
                + "FROM QIDETAIL) TMP\n"
                + "WHERE WHS != 'TWC'\n"
                + "AND WHS = HD.QIHWHS\n"
                + "AND MATCT = HD.QIHMTCTRL\n"
                + "AND (QIDSTS = 'R')) IS NULL THEN 0 \n"
                + "ELSE \n"
                + "(SELECT SUM(QIDQTY)\n"
                + "FROM(\n"
                + "SELECT (SELECT TOP 1 QIHWHS FROM QIHEAD WHERE QIHQNO = QIDQNO) as WHS, \n"
                + "       (SELECT TOP 1 QIHMTCTRL FROM QIHEAD WHERE QIHQNO = QIDQNO) as MATCT, \n"
                + "	   QIDQNO, QIDQTY, QIDSTS\n"
                + "FROM QIDETAIL) TMP\n"
                + "WHERE WHS != 'TWC'\n"
                + "AND WHS = HD.QIHWHS\n"
                + "AND MATCT = HD.QIHMTCTRL\n"
                + "AND (QIDSTS = 'R')) \n"
                + "END as TOTALQTY "
                + "FROM QIHEAD HD "
                + "INNER JOIN MSSMATN "
                + "ON HD.QIHMTCTRL = MSSMATN.LGPBE "
                + "INNER JOIN QWHMAS "
                + "ON HD.QIHWHS = QWHMAS.QWHCOD "
                + "WHERE (QIHSTS = 'R') "
                + TRDT
                + WHS
                + "ORDER BY QIHWHS, QIHMTCTRL ";

//        System.out.println(sql);
        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            String oldWH = "";
            int iter = 1;

            while (result.next()) {

                if (oldWH.equals("")) {
                    oldWH = result.getString("QIHWHS");
                }

                if (!TMP.contains(result.getString("QIHMTCTRL"))) {

                    MSSMATN p = new MSSMATN();
                    p.setUid(result.getString("QIHMTCTRL"));
                    p.setWarehouse(result.getString("QIHWHS"));
                    p.setTolQno(result.getString("TOTALQNO"));

                    double tot = Double.parseDouble(result.getString("TOTALQTY"));
                    String tqt = formatDou.format(tot);

                    p.setTolQty(tqt);
                    p.setDesc(result.getString("MATCNAME"));

                    if (!result.getString("QIHWHS").equals(oldWH)) {
                        String newWHS = "<hr>\n"
                                + "<b>" + result.getString("QIHWHS") + " : " + result.getString("QWHNAME") + "</b><br>";
                        p.setName(newWHS);
                        oldWH = result.getString("QIHWHS");
                    } else if (iter == 1) {
                        String newWHS = "<b>" + result.getString("QIHWHS") + " : " + result.getString("QWHNAME") + "</b><br>";
                        p.setName(newWHS);
                        oldWH = result.getString("QIHWHS");
                    }

                    TMP.add(result.getString("QIHMTCTRL"));
                    UAList.add(p);
                }
                iter++;
            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public List<MSSMATN> findByUidWMS320(String whF, String whT) {

        List<MSSMATN> UAList = new ArrayList<MSSMATN>();
        List<String> TMP = new ArrayList<String>();

        String TRDT = "";
        String WHS = "";

        if (!whF.equals("")) {
            if (!whF.equals("TWC") && !whT.equals("TWC")) {
                WHS = "AND QIHWHS BETWEEN '" + whF + "' AND '" + whT + "' ";
            }
        }

//        if (!sd.equals("")) {
//            TRDT = "AND (SUBSTRING(CONVERT(VARCHAR(25), QIHTRDT, 126), 1, 10) BETWEEN '" + sd + "' AND '" + ed + "') ";
//        }
        String sql = "SELECT QIHWHS, QWHNAME, SUBSTRING(CONVERT(VARCHAR(25), QIHTRDT, 126), 1, 10) as QIHTRDT, QIHMTCTRL, MATCNAME "
                + "FROM QIHEAD "
                + "INNER JOIN MSSMATN "
                + "ON QIHEAD.QIHMTCTRL = MSSMATN.LGPBE "
                + "INNER JOIN QWHMAS "
                + "ON QIHEAD.QIHWHS = QWHMAS.QWHCOD "
                + "WHERE (QIHSTS = '2') "
                + TRDT
                + WHS
                + "ORDER BY QIHWHS, QIHMTCTRL ";

        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            String oldWH = "";
            int iter = 1;

            while (result.next()) {

                if (oldWH.equals("")) {
                    oldWH = result.getString("QIHWHS");
                }

                if (!TMP.contains(result.getString("QIHMTCTRL"))) {

                    MSSMATN p = new MSSMATN();
                    p.setUid(result.getString("QIHMTCTRL"));
                    p.setDesc(result.getString("MATCNAME").trim());
                    p.setWarehouse(result.getString("QIHWHS"));

                    if (!result.getString("QIHWHS").equals(oldWH)) {
                        String newWHS = "<hr>\n"
                                + "<b>" + result.getString("QIHWHS") + " : " + result.getString("QWHNAME") + "</b><br>";
                        p.setName(newWHS);
                        oldWH = result.getString("QIHWHS");
                    } else if (iter == 1) {
                        String newWHS = "<b>" + result.getString("QIHWHS") + " : " + result.getString("QWHNAME") + "</b><br>";
                        p.setName(newWHS);
                        oldWH = result.getString("QIHWHS");
                    }

                    QIHEADDao dao = new QIHEADDao();
                    List<MSSMATN> whList = dao.findPopupMat(result.getString("QIHMTCTRL").trim());

                    p.setWhList(whList);

                    TMP.add(result.getString("QIHMTCTRL"));
                    if (whList.size() > 1) {
                        UAList.add(p);
                    }

                }
                iter++;
            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public List<MSSMATN> findByUidWMS320X(String whF, String whT) {

        List<MSSMATN> UAList = new ArrayList<MSSMATN>();
        List<String> TMP = new ArrayList<String>();

        String TRDT = "";
        String WHS = "";

        if (!whF.equals("")) {
            if (!whF.equals("TWC") && !whT.equals("TWC")) {
                WHS = "AND QIHWHS BETWEEN '" + whF + "' AND '" + whT + "' ";
            }
        }

//        if (!sd.equals("")) {
//            TRDT = "AND (SUBSTRING(CONVERT(VARCHAR(25), QIHTRDT, 126), 1, 10) BETWEEN '" + sd + "' AND '" + ed + "') ";
//        }
        String sql = "SELECT DISTINCT QIHWHS, QWHNAME\n"
                + ",(CASE WHEN QIDMTCTRL = '' OR QIDMTCTRL IS NULL THEN (select top 1 [SAPMCTRL] from [SAPMAS] where [SAPMAT] = QIDMAT) ELSE QIDMTCTRL END) AS QIHMTCTRL\n"
                + ", MATCNAME \n"
                + "FROM QIHEAD \n"
                + "LEFT JOIN QIDETAIL ON QIDQNO = QIHQNO\n"
                + "INNER JOIN MSSMATN ON (CASE WHEN QIDMTCTRL = '' OR QIDMTCTRL IS NULL THEN (select top 1 [SAPMCTRL] from [SAPMAS] where [SAPMAT] = QIDMAT) ELSE QIDMTCTRL END) = MSSMATN.LGPBE \n"
                + "INNER JOIN QWHMAS ON QIHEAD.QIHWHS = QWHMAS.QWHCOD \n"
                + "WHERE (QIHSTS = '5') "
                + TRDT
                + WHS
                + "ORDER BY QIHWHS, QIHMTCTRL ";
//        System.out.println(sql);

        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            String oldWH = "";
            int iter = 1;

            while (result.next()) {

                if (oldWH.equals("")) {
                    oldWH = result.getString("QIHWHS");
                }

                if (!TMP.contains(result.getString("QIHMTCTRL"))) {

                    MSSMATN p = new MSSMATN();
                    p.setUid(result.getString("QIHMTCTRL"));
                    p.setDesc(result.getString("MATCNAME").trim());
                    p.setWarehouse(result.getString("QIHWHS"));

                    if (!result.getString("QIHWHS").equals(oldWH)) {
                        String newWHS = "<hr>\n"
                                + "<b>" + result.getString("QIHWHS") + " : " + result.getString("QWHNAME") + "</b><br>";
                        p.setName(newWHS);
                        oldWH = result.getString("QIHWHS");
                    } else if (iter == 1) {
                        String newWHS = "<b>" + result.getString("QIHWHS") + " : " + result.getString("QWHNAME") + "</b><br>";
                        p.setName(newWHS);
                        oldWH = result.getString("QIHWHS");
                    }

                    QIHEADDao dao = new QIHEADDao();
                    List<MSSMATN> whList = dao.findPopupMatX(result.getString("QIHMTCTRL").trim());

                    p.setWhList(whList);

                    TMP.add(result.getString("QIHMTCTRL"));
                    if (whList.size() > 1) {
                        UAList.add(p);
                    }

                }
                iter++;
            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public List<MSSMATN> findByUidWMS341(String whF, String whT) {

        List<MSSMATN> UAList = new ArrayList<MSSMATN>();
        List<String> TMP = new ArrayList<String>();

        String TRDT = "";
        String WHS = "";

        if (!whF.equals("")) {
            if (!whF.equals("TWC") && !whT.equals("TWC")) {
                WHS = "AND QIHWHS BETWEEN '" + whF + "' AND '" + whT + "' ";
            }
        }

//        if (!sd.equals("")) {
//            TRDT = "AND (SUBSTRING(CONVERT(VARCHAR(25), QIHTRDT, 126), 1, 10) BETWEEN '" + sd + "' AND '" + ed + "') ";
//        }
        String sql = "SELECT QIHWHS, QWHNAME, SUBSTRING(CONVERT(VARCHAR(25), QIHTRDT, 126), 1, 10) as QIHTRDT, QIHMTCTRL, MATCNAME "
                + "FROM QIHEAD "
                + "INNER JOIN MSSMATN "
                + "ON QIHEAD.QIHMTCTRL = MSSMATN.LGPBE "
                + "INNER JOIN QWHMAS "
                + "ON QIHEAD.QIHWHS = QWHMAS.QWHCOD "
                + "WHERE (QIHSTS = '5' OR QIHSTS = '6' OR QIHSTS = '7') "
                + TRDT
                + WHS
                + "ORDER BY QIHWHS, QIHMTCTRL ";

        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            String oldWH = "";
            int iter = 1;

            while (result.next()) {

                if (oldWH.equals("")) {
                    oldWH = result.getString("QIHWHS");
                }

                if (!TMP.contains(result.getString("QIHMTCTRL"))) {

                    MSSMATN p = new MSSMATN();
                    p.setUid(result.getString("QIHMTCTRL"));
                    p.setDesc(result.getString("MATCNAME").trim());
                    p.setWarehouse(result.getString("QIHWHS"));

                    if (!result.getString("QIHWHS").equals(oldWH)) {
                        String newWHS = "<hr>\n"
                                + "<b>" + result.getString("QIHWHS") + " : " + result.getString("QWHNAME") + "</b><br>";
                        p.setName(newWHS);
                        oldWH = result.getString("QIHWHS");
                    } else if (iter == 1) {
                        String newWHS = "<b>" + result.getString("QIHWHS") + " : " + result.getString("QWHNAME") + "</b><br>";
                        p.setName(newWHS);
                        oldWH = result.getString("QIHWHS");
                    }

                    QIHEADDao dao = new QIHEADDao();
                    List<MSSMATN> whList = dao.findPopupMat341(result.getString("QIHMTCTRL").trim());

                    p.setWhList(whList);

                    TMP.add(result.getString("QIHMTCTRL"));
                    if (whList.size() > 1) {
                        UAList.add(p);
                    }

                }
                iter++;
            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public List<MSSMATN> findByUidWMS320Dest(String whF, String whT) {

        List<MSSMATN> UAList = new ArrayList<MSSMATN>();
        List<String> TMP = new ArrayList<String>();

        String TRDT = "";
        String WHS = "";

        if (!whF.equals("")) {
            if (!whF.equals("TWC") && !whT.equals("TWC")) {
                WHS = "AND QIHWHS BETWEEN '" + whF + "' AND '" + whT + "' ";
            }
        }

//        if (!sd.equals("")) {
//            TRDT = "AND (SUBSTRING(CONVERT(VARCHAR(25), QIHTRDT, 126), 1, 10) BETWEEN '" + sd + "' AND '" + ed + "') ";
//        }
        String sql = "SELECT QIHWHS, SUBSTRING(CONVERT(VARCHAR(25), QIHTRDT, 126), 1, 10) as QIHTRDT, "
                + "QIHDEST, QDEST.QDEDESC, QDEST.QDETYPE, QDESTYP.QDEDESC as TYPEDESC "
                + "FROM QIHEAD "
                + "INNER JOIN QDEST "
                + "ON QIHEAD.QIHDEST = QDEST.QDECOD  "
                + "INNER JOIN QDESTYP "
                + "ON QDEST.QDETYPE = QDESTYP.QDETYPE "
                + "WHERE QIHSTS = '2' "
                + TRDT
                + WHS
                + "ORDER BY QDEST.QDETYPE, QIHDEST ";

        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            String oldWH = "";
            int iter = 1;

            while (result.next()) {

                if (oldWH.equals("")) {
                    oldWH = result.getString("TYPEDESC");
                }

                if (!TMP.contains(result.getString("QIHDEST"))) {

                    MSSMATN p = new MSSMATN();
                    p.setUid(result.getString("QIHDEST"));
                    p.setWarehouse(result.getString("QIHWHS"));
                    p.setDesc(result.getString("QDEDESC"));

                    if (!result.getString("TYPEDESC").equals(oldWH)) {
                        String newWHS = "<hr>\n"
                                + "<b>" + result.getString("QDETYPE") + " : " + result.getString("TYPEDESC") + "</b><br>";
                        p.setName(newWHS);
                        oldWH = result.getString("TYPEDESC");
                    } else if (iter == 1) {
                        String newWHS = "<b>" + result.getString("QDETYPE") + " : " + result.getString("TYPEDESC") + "</b><br>";
                        p.setName(newWHS);
                        oldWH = result.getString("TYPEDESC");
                    }

                    QIHEADDao dao = new QIHEADDao();
                    List<MSSMATN> whList = dao.findPopup(result.getString("QIHDEST").trim(), whF, whT);

                    p.setWhList(whList);

                    TMP.add(result.getString("QIHDEST"));
                    UAList.add(p);
                }
                iter++;
            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public List<MSSMATN> findByUidWMS340Dest(String whF, String whT) {

        List<MSSMATN> UAList = new ArrayList<MSSMATN>();
        List<String> TMP = new ArrayList<String>();

        String TRDT = "";
        String WHS = "";

        if (!whF.equals("")) {
            if (!whF.equals("TWC") && !whT.equals("TWC")) {
                WHS = "AND QIHWHS BETWEEN '" + whF + "' AND '" + whT + "' ";
            }
        }

//        if (!sd.equals("")) {
//            TRDT = "AND (SUBSTRING(CONVERT(VARCHAR(25), QIHTRDT, 126), 1, 10) BETWEEN '" + sd + "' AND '" + ed + "') ";
//        }
        String sql = "SELECT QIHWHS, SUBSTRING(CONVERT(VARCHAR(25), QIHTRDT, 126), 1, 10) as QIHTRDT, "
                + "QIHDEST, QDEST.QDEDESC, QDEST.QDETYPE, QDESTYP.QDEDESC as TYPEDESC "
                + "FROM QIHEAD "
                + "INNER JOIN QDEST "
                + "ON QIHEAD.QIHDEST = QDEST.QDECOD  "
                + "INNER JOIN QDESTYP "
                + "ON QDEST.QDETYPE = QDESTYP.QDETYPE "
                + "WHERE (QIHSTS = '2') "
                + TRDT
                + WHS
                + "ORDER BY QDEST.QDETYPE, QIHDEST ";

        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            String oldWH = "";
            int iter = 1;

            while (result.next()) {

                if (oldWH.equals("")) {
                    oldWH = result.getString("TYPEDESC");
                }

                if (!TMP.contains(result.getString("QIHDEST"))) {

                    MSSMATN p = new MSSMATN();
                    p.setUid(result.getString("QIHDEST"));
                    p.setWarehouse(result.getString("QIHWHS"));
                    p.setDesc(result.getString("QDEDESC"));

                    if (!result.getString("TYPEDESC").equals(oldWH)) {
                        String newWHS = "<hr>\n"
                                + "<b>" + result.getString("QDETYPE") + " : " + result.getString("TYPEDESC") + "</b><br>";
                        p.setName(newWHS);
                        oldWH = result.getString("TYPEDESC");
                    } else if (iter == 1) {
                        String newWHS = "<b>" + result.getString("QDETYPE") + " : " + result.getString("TYPEDESC") + "</b><br>";
                        p.setName(newWHS);
                        oldWH = result.getString("TYPEDESC");
                    }

                    QIHEADDao dao = new QIHEADDao();
                    List<MSSMATN> whList = dao.findPopupDEST340(result.getString("QIHDEST").trim(), whF);

                    p.setWhList(whList);

                    TMP.add(result.getString("QIHDEST"));
                    if (whList.size() > 1) {
                        UAList.add(p);
                    }

                }
                iter++;
            }

            connect.close();
        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public List<MSSMATN> findByUidWMS340DestX(String whF, String whT) {

        List<MSSMATN> UAList = new ArrayList<MSSMATN>();
        List<String> TMP = new ArrayList<String>();

        String TRDT = "";
        String WHS = "";

        if (!whF.equals("")) {
            if (!whF.equals("TWC") && !whT.equals("TWC")) {
                WHS = "AND QIHWHS BETWEEN '" + whF + "' AND '" + whT + "' ";
            }
        }

//        if (!sd.equals("")) {
//            TRDT = "AND (SUBSTRING(CONVERT(VARCHAR(25), QIHTRDT, 126), 1, 10) BETWEEN '" + sd + "' AND '" + ed + "') ";
//        }
        String sql = "SELECT QIHWHS, SUBSTRING(CONVERT(VARCHAR(25), QIHTRDT, 126), 1, 10) as QIHTRDT, "
                + "QIHDEST, QDEST.QDEDESC, QDEST.QDETYPE, QDESTYP.QDEDESC as TYPEDESC "
                + "FROM QIHEAD "
                + "INNER JOIN QDEST "
                + "ON QIHEAD.QIHDEST = QDEST.QDECOD  "
                + "INNER JOIN QDESTYP "
                + "ON QDEST.QDETYPE = QDESTYP.QDETYPE "
                + "WHERE (QIHSTS = '5') "
                + TRDT
                + WHS
                + "ORDER BY QDEST.QDETYPE, QIHDEST ";
//        System.out.println(sql);

        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            String oldWH = "";
            int iter = 1;

            while (result.next()) {

                if (oldWH.equals("")) {
                    oldWH = result.getString("TYPEDESC");
                }

                if (!TMP.contains(result.getString("QIHDEST"))) {

                    MSSMATN p = new MSSMATN();
                    p.setUid(result.getString("QIHDEST"));
                    p.setWarehouse(result.getString("QIHWHS"));
                    p.setDesc(result.getString("QDEDESC"));

                    if (!result.getString("TYPEDESC").equals(oldWH)) {
                        String newWHS = "<hr>\n"
                                + "<b>" + result.getString("QDETYPE") + " : " + result.getString("TYPEDESC") + "</b><br>";
                        p.setName(newWHS);
                        oldWH = result.getString("TYPEDESC");
                    } else if (iter == 1) {
                        String newWHS = "<b>" + result.getString("QDETYPE") + " : " + result.getString("TYPEDESC") + "</b><br>";
                        p.setName(newWHS);
                        oldWH = result.getString("TYPEDESC");
                    }

                    QIHEADDao dao = new QIHEADDao();
                    List<MSSMATN> whList = dao.findPopupDEST340X(result.getString("QIHDEST").trim(), whF);

                    p.setWhList(whList);

                    TMP.add(result.getString("QIHDEST"));
                    if (whList.size() > 1) {
                        UAList.add(p);
                    }

                }
                iter++;
            }

            connect.close();
        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public List<MSSMATN> findByUidWMS341Dest(String whF, String whT) {

        List<MSSMATN> UAList = new ArrayList<MSSMATN>();
        List<String> TMP = new ArrayList<String>();

        String TRDT = "";
        String WHS = "";

        if (!whF.equals("")) {
            if (!whF.equals("TWC") && !whT.equals("TWC")) {
                WHS = "AND QIHWHS BETWEEN '" + whF + "' AND '" + whT + "' ";
            }
        }

//        if (!sd.equals("")) {
//            TRDT = "AND (SUBSTRING(CONVERT(VARCHAR(25), QIHTRDT, 126), 1, 10) BETWEEN '" + sd + "' AND '" + ed + "') ";
//        }
        String sql = "SELECT QIHWHS, SUBSTRING(CONVERT(VARCHAR(25), QIHTRDT, 126), 1, 10) as QIHTRDT, "
                + "QIHDEST, QDEST.QDEDESC, QDEST.QDETYPE, QDESTYP.QDEDESC as TYPEDESC "
                + "FROM QIHEAD "
                + "INNER JOIN QDEST "
                + "ON QIHEAD.QIHDEST = QDEST.QDECOD  "
                + "INNER JOIN QDESTYP "
                + "ON QDEST.QDETYPE = QDESTYP.QDETYPE "
                + "WHERE (QIHSTS = '5' OR QIHSTS = '6' OR QIHSTS = '7') "
                + TRDT
                + WHS
                + "ORDER BY QDEST.QDETYPE, QIHDEST ";

        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            String oldWH = "";
            int iter = 1;

            while (result.next()) {

                if (oldWH.equals("")) {
                    oldWH = result.getString("TYPEDESC");
                }

                if (!TMP.contains(result.getString("QIHDEST"))) {

                    MSSMATN p = new MSSMATN();
                    p.setUid(result.getString("QIHDEST"));
                    p.setWarehouse(result.getString("QIHWHS"));
                    p.setDesc(result.getString("QDEDESC"));

                    if (!result.getString("TYPEDESC").equals(oldWH)) {
                        String newWHS = "<hr>\n"
                                + "<b>" + result.getString("QDETYPE") + " : " + result.getString("TYPEDESC") + "</b><br>";
                        p.setName(newWHS);
                        oldWH = result.getString("TYPEDESC");
                    } else if (iter == 1) {
                        String newWHS = "<b>" + result.getString("QDETYPE") + " : " + result.getString("TYPEDESC") + "</b><br>";
                        p.setName(newWHS);
                        oldWH = result.getString("TYPEDESC");
                    }

                    QIHEADDao dao = new QIHEADDao();
                    List<MSSMATN> whList = dao.findPopupDEST341(result.getString("QIHDEST").trim(), whF);

                    p.setWhList(whList);

                    TMP.add(result.getString("QIHDEST"));
                    if (whList.size() > 1) {
                        UAList.add(p);
                    }

                }
                iter++;
            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public List<MSSMATN> findPopupDEST340(String dest, String wh) {

        List<MSSMATN> UAList = new ArrayList<MSSMATN>();

        String sql = "SELECT distinct HD.QIHDEST, HD.QIHWHS, HD.QIHMTCTRL, MT.MATCNAME,\n"
                + "(SELECT SUM(SKU)\n"
                + "FROM(\n"
                + "SELECT (SELECT COUNT(DISTINCT QIDMAT) FROM QIDETAIL WHERE QIDQNO = QIHQNO AND QIDSTS = '2') AS SKU\n"
                + "FROM QIHEAD\n"
                + "WHERE QIHDEST =  HD.QIHDEST\n"
                + "AND QIHWHS = HD.QIHWHS\n"
                + "AND QIHMTCTRL = HD.QIHMTCTRL\n"
                + ") SKU) AS SKU,\n"
                + "(SELECT SUM(QPACK)\n"
                + "FROM(\n"
                + "SELECT (SELECT COUNT(QIDPACKTYPE) FROM QIDETAIL WHERE QIDQNO = QIHQNO AND QIDSTS = '2') AS QPACK\n"
                + "FROM QIHEAD\n"
                + "WHERE QIHDEST = HD.QIHDEST\n"
                + "AND QIHWHS = HD.QIHWHS\n"
                + "AND QIHMTCTRL = HD.QIHMTCTRL\n"
                + ") QPK) AS QPACK,\n"
                + "0 As M3\n"
                + "FROM QIHEAD HD\n"
                + "INNER JOIN MSSMATN MT\n"
                + "ON HD.QIHMTCTRL = MT.LGPBE \n"
                + "WHERE HD.QIHDEST = '" + dest + "'\n"
                + "AND HD.QIHWHS = '" + wh + "' "
                + "ORDER BY HD.QIHWHS, HD.QIHMTCTRL ";
//        System.out.println(sql);

        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            int sumSKU = 0;
            int sumQPK = 0;
            int sumBAG = 0;
            int sumROLL = 0;
            int sumBOX = 0;

            while (result.next()) {

                MSSMATN p = new MSSMATN();

                p.setUid(result.getString("QIHMTCTRL") + " : " + result.getString("MATCNAME").trim()); //MAT

                QIHEADDao dao = new QIHEADDao();
                String[] pop = dao.findSKUforPopupDEST340(result.getString("QIHDEST").trim(),
                        result.getString("QIHWHS").trim(), result.getString("QIHMTCTRL").trim());

                p.setName(pop[0]); //SKU

                p.setWarehouse(pop[1]); //QPACK
                p.setDesc(pop[2]); //M3

                p.setTolQno(pop[3]); //bag
                p.setTolQty(pop[4]); //roll
                p.setBox(pop[5]); //box

                sumSKU += Integer.parseInt(pop[0]);
                sumQPK += Integer.parseInt(pop[1]);
                sumBAG += Integer.parseInt(pop[3]);
                sumROLL += Integer.parseInt(pop[4]);
                sumBOX += Integer.parseInt(pop[5]);

                if (!pop[1].equals("0")) {
                    UAList.add(p);
                }

            }

            MSSMATN p = new MSSMATN();

            p.setUid("TOTAL Mat Ctrl"); //MAT
            p.setName(Integer.toString(sumSKU)); //SKU
            p.setWarehouse(Integer.toString(sumQPK)); //QPACK
            p.setDesc("0"); //M3

            p.setTolQno(Integer.toString(sumBAG)); //bag
            p.setTolQty(Integer.toString(sumROLL)); //roll
            p.setBox(Integer.toString(sumBOX)); //box

            UAList.add(p);

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public List<MSSMATN> findPopupDEST340X(String dest, String wh) {

        List<MSSMATN> UAList = new ArrayList<MSSMATN>();

//        String sql = "SELECT distinct HD.QIHDEST, HD.QIHWHS, HD.QIHMTCTRL, MT.MATCNAME,\n"
//                + "(SELECT SUM(SKU)\n"
//                + "FROM(\n"
//                + "SELECT (SELECT COUNT(DISTINCT QIDMAT) FROM QIDETAIL WHERE QIDQNO = QIHQNO AND QIDSTS = '5') AS SKU\n"
//                + "FROM QIHEAD\n"
//                + "WHERE QIHDEST =  HD.QIHDEST\n"
//                + "AND QIHWHS = HD.QIHWHS\n"
//                + "AND QIHMTCTRL = HD.QIHMTCTRL\n"
//                + ") SKU) AS SKU,\n"
//                + "(SELECT SUM(QPACK)\n"
//                + "FROM(\n"
//                + "SELECT (SELECT COUNT(QIDPACKTYPE) FROM QIDETAIL WHERE QIDQNO = QIHQNO AND QIDSTS = '5') AS QPACK\n"
//                + "FROM QIHEAD\n"
//                + "WHERE QIHDEST = HD.QIHDEST\n"
//                + "AND QIHWHS = HD.QIHWHS\n"
//                + "AND QIHMTCTRL = HD.QIHMTCTRL\n"
//                + ") QPK) AS QPACK,\n"
//                + "0 As M3\n"
//                + "FROM QIHEAD HD\n"
//                + "INNER JOIN MSSMATN MT\n"
//                + "ON HD.QIHMTCTRL = MT.LGPBE \n"
//                + "WHERE HD.QIHDEST = '" + dest + "'\n"
//                + "AND HD.QIHWHS = '" + wh + "' "
//                + "ORDER BY HD.QIHWHS, HD.QIHMTCTRL ";
        String sql = "SELECT distinct HD.QIHDEST, HD.QIHWHS\n"
                + ",CASE WHEN QIDMTCTRL = '' OR QIDMTCTRL IS NULL THEN (select top 1 [SAPMCTRL] from [SAPMAS] where [SAPMAT] = QIDMAT) ELSE QIDMTCTRL END AS QIHMTCTRL\n"
                + ", MT.MATCNAME\n"
                + "FROM QIHEAD HD\n"
                + "LEFT JOIN QIDETAIL ON QIDQNO = QIHQNO\n"
                + "INNER JOIN MSSMATN MT\n"
                + "ON (CASE WHEN QIDMTCTRL = '' OR QIDMTCTRL IS NULL THEN (select top 1 [SAPMCTRL] from [SAPMAS] where [SAPMAT] = QIDMAT) ELSE QIDMTCTRL END) = MT.LGPBE\n"
                + "WHERE HD.QIHDEST = '" + dest + "'\n"
                + "AND HD.QIHWHS = '" + wh + "' \n"
                + "AND CASE WHEN QIDMTCTRL = '' OR QIDMTCTRL IS NULL THEN (select top 1 [SAPMCTRL] from [SAPMAS] where [SAPMAT] = QIDMAT) ELSE QIDMTCTRL END IS NOT NULL\n"
                + "ORDER BY HD.QIHWHS, QIHMTCTRL";
//        System.out.println(sql);

        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            int sumSKU = 0;
            int sumQPK = 0;
            int sumBAG = 0;
            int sumROLL = 0;
            int sumBOX = 0;

            while (result.next()) {

                MSSMATN p = new MSSMATN();

                p.setUid(result.getString("QIHMTCTRL") + " : " + result.getString("MATCNAME").trim()); //MAT

                QIHEADDao dao = new QIHEADDao();
                String[] pop = dao.findSKUforPopupDEST340X(result.getString("QIHDEST").trim(),
                        result.getString("QIHWHS").trim(), result.getString("QIHMTCTRL").trim());

                p.setName(pop[0]); //SKU

                p.setWarehouse(pop[1]); //QPACK
                p.setDesc(pop[2]); //M3

                p.setTolQno(pop[3]); //bag
                p.setTolQty(pop[4]); //roll
                p.setBox(pop[5]); //box

                if (!pop[1].equals("0")) {
                    UAList.add(p);

                    sumSKU += Integer.parseInt(pop[0]);
                    sumQPK += Integer.parseInt(pop[1]);
                    sumBAG += Integer.parseInt(pop[3]);
                    sumROLL += Integer.parseInt(pop[4]);
                    sumBOX += Integer.parseInt(pop[5]);
                }

            }

            MSSMATN p = new MSSMATN();

            p.setUid(" : "); //MAT

            QIHEADDao dao = new QIHEADDao();
            String[] pop = dao.findSKUforPopupDEST340X(dest.trim(),
                    wh.trim(), "");

            p.setName(pop[0]); //SKU

            p.setWarehouse(pop[1]); //QPACK
            p.setDesc(pop[2]); //M3

            p.setTolQno(pop[3]); //bag
            p.setTolQty(pop[4]); //roll
            p.setBox(pop[5]); //box

            if (!pop[1].equals("0")) {
                UAList.add(p);

                sumSKU += Integer.parseInt(pop[0]);
                sumQPK += Integer.parseInt(pop[1]);
                sumBAG += Integer.parseInt(pop[3]);
                sumROLL += Integer.parseInt(pop[4]);
                sumBOX += Integer.parseInt(pop[5]);
            }

            MSSMATN p2 = new MSSMATN();

            p2.setUid("TOTAL Mat Ctrl"); //MAT
            p2.setName(Integer.toString(sumSKU)); //SKU
            p2.setWarehouse(Integer.toString(sumQPK)); //QPACK
            p2.setDesc("0"); //M3

            p2.setTolQno(Integer.toString(sumBAG)); //bag
            p2.setTolQty(Integer.toString(sumROLL)); //roll
            p2.setBox(Integer.toString(sumBOX)); //box

            UAList.add(p2);

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public List<MSSMATN> findPopupDEST341(String dest, String wh) {

        List<MSSMATN> UAList = new ArrayList<MSSMATN>();

        String sql = "SELECT distinct HD.QIHDEST, HD.QIHWHS, HD.QIHMTCTRL, MT.MATCNAME,\n"
                + "(SELECT SUM(SKU)\n"
                + "FROM(\n"
                + "SELECT (SELECT COUNT(DISTINCT QIDMAT) FROM QIDETAIL WHERE QIDQNO = QIHQNO AND (QIDSTS = '5' OR QIDSTS = '6' OR QIDSTS = '7')) AS SKU\n"
                + "FROM QIHEAD\n"
                + "WHERE QIHDEST =  HD.QIHDEST\n"
                + "AND QIHWHS = HD.QIHWHS\n"
                + "AND QIHMTCTRL = HD.QIHMTCTRL\n"
                + ") SKU) AS SKU,\n"
                + "(SELECT SUM(QPACK)\n"
                + "FROM(\n"
                + "SELECT (SELECT COUNT(QIDPACKTYPE) FROM QIDETAIL WHERE QIDQNO = QIHQNO AND (QIDSTS = '5' OR QIDSTS = '6' OR QIDSTS = '7')) AS QPACK\n"
                + "FROM QIHEAD\n"
                + "WHERE QIHDEST = HD.QIHDEST\n"
                + "AND QIHWHS = HD.QIHWHS\n"
                + "AND QIHMTCTRL = HD.QIHMTCTRL\n"
                + ") QPK) AS QPACK,\n"
                + "0 As M3\n"
                + "FROM QIHEAD HD\n"
                + "INNER JOIN MSSMATN MT\n"
                + "ON HD.QIHMTCTRL = MT.LGPBE \n"
                + "WHERE HD.QIHDEST = '" + dest + "'\n"
                + "AND HD.QIHWHS = '" + wh + "' "
                + "ORDER BY HD.QIHWHS, HD.QIHMTCTRL ";

        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            int sumSKU = 0;
            int sumQPK = 0;

            while (result.next()) {

                MSSMATN p = new MSSMATN();

                p.setUid(result.getString("QIHMTCTRL") + " : " + result.getString("MATCNAME").trim()); //MAT

                QIHEADDao dao = new QIHEADDao();
                String[] pop = dao.findSKUforPopupDEST341(result.getString("QIHDEST").trim(),
                        result.getString("QIHWHS").trim(), result.getString("QIHMTCTRL").trim());

                p.setName(pop[0]); //SKU

                p.setWarehouse(pop[1]); //QPACK
                p.setDesc(pop[2]); //M3

                sumSKU += Integer.parseInt(pop[0]);
                sumQPK += Integer.parseInt(pop[1]);

                if (!pop[1].equals("0")) {
                    UAList.add(p);
                }

            }

            MSSMATN p = new MSSMATN();

            p.setUid("TOTAL Mat Ctrl"); //MAT
            p.setName(Integer.toString(sumSKU)); //SKU
            p.setWarehouse(Integer.toString(sumQPK)); //QPACK
            p.setDesc("0"); //M3

            UAList.add(p);

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public String[] findSKUforPopupDEST340(String dest, String wh, String mat) {

        String[] p = new String[6];

        String sql = "SELECT COUNT(DISTINCT QIDMAT) AS SKU, \n"
                + "(SELECT COUNT(DISTINCT CASE WHEN QIDGPQR IS NULL THEN QIDID WHEN QIDGPQR = '' THEN QIDID ELSE QIDGPQR END)\n"
                + "FROM QIDETAIL \n"
                + "WHERE (select top 1 QIHDEST from QIHEAD where QIHQNO = QIDQNO) = '" + dest + "' \n"
                + "AND (select top 1 QIHMTCTRL from QIHEAD where QIHQNO = QIDQNO) = '" + mat + "' \n"
                + "AND QIDWHS = '" + wh + "' \n"
                + "AND (QIDSTS = '2')) AS QPACK,\n \n"
                + "(SELECT COUNT(DISTINCT CASE WHEN QIDGPQR IS NULL THEN QIDID WHEN QIDGPQR = '' THEN QIDID ELSE QIDGPQR END)\n"
                + "FROM QIDETAIL \n"
                + "WHERE (select top 1 QIHDEST from QIHEAD where QIHQNO = QIDQNO) = '" + dest + "' \n"
                + "AND (select top 1 QIHMTCTRL from QIHEAD where QIHQNO = QIDQNO) = '" + mat + "' \n"
                + "AND QIDWHS = '" + wh + "' \n"
                + "AND (QIDSTS = '2')\n"
                + "AND QIDPACKTYPE = 'BAG') AS BAG, \n"
                + "(SELECT COUNT(DISTINCT CASE WHEN QIDGPQR IS NULL THEN QIDID WHEN QIDGPQR = '' THEN QIDID ELSE QIDGPQR END)\n"
                + "FROM QIDETAIL \n"
                + "WHERE (select top 1 QIHDEST from QIHEAD where QIHQNO = QIDQNO) = '" + dest + "' \n"
                + "AND (select top 1 QIHMTCTRL from QIHEAD where QIHQNO = QIDQNO) = '" + mat + "' \n"
                + "AND QIDWHS = '" + wh + "' \n"
                + "AND (QIDSTS = '2')\n"
                + "AND QIDPACKTYPE = 'ROLL') AS ROLL, \n"
                + "(SELECT COUNT(DISTINCT CASE WHEN QIDGPQR IS NULL THEN QIDID WHEN QIDGPQR = '' THEN QIDID ELSE QIDGPQR END)\n"
                + "FROM QIDETAIL \n"
                + "WHERE (select top 1 QIHDEST from QIHEAD where QIHQNO = QIDQNO) = '" + dest + "' \n"
                + "AND (select top 1 QIHMTCTRL from QIHEAD where QIHQNO = QIDQNO) = '" + mat + "' \n"
                + "AND QIDWHS = '" + wh + "' \n"
                + "AND (QIDSTS = '2')\n"
                + "AND QIDPACKTYPE = 'BOX') AS BOX, \n"
                + "0 AS M3 \n"
                + "FROM QIDETAIL \n"
                + "LEFT JOIN QRMSTS \n"
                + "ON QIDETAIL.QIDSTS = QRMSTS.STSCODE \n"
                + "WHERE (select top 1 QIHDEST from QIHEAD where QIHQNO = QIDQNO) = '" + dest + "' \n"
                + "AND (select top 1 QIHMTCTRL from QIHEAD where QIHQNO = QIDQNO) = '" + mat + "' \n"
                + "AND QIDWHS = '" + wh + "' \n"
                + "AND (QIDSTS = '2') ";
//        System.out.println(sql);
        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                p[0] = result.getString("SKU");
                p[1] = result.getString("QPACK");
                p[2] = result.getString("M3");
                p[3] = result.getString("BAG");
                p[4] = result.getString("ROLL");
                p[5] = result.getString("BOX");

            }

        } catch (Exception e) {

            e.printStackTrace();

        }

        return p;

    }

    public String[] findSKUforPopupDEST340X(String dest, String wh, String mat) {

        String[] p = new String[6];

        String sql = "SELECT COUNT(DISTINCT QIDMAT) AS SKU, \n"
                + "(SELECT COUNT(DISTINCT CASE WHEN QIDGPQR IS NULL THEN QIDID WHEN QIDGPQR = '' THEN QIDID ELSE QIDGPQR END)\n"
                + "FROM QIDETAIL \n"
                + "WHERE (select top 1 QIHDEST from QIHEAD where QIHQNO = QIDQNO) = '" + dest + "' \n"
                + "AND (CASE WHEN QIDMTCTRL = '' OR QIDMTCTRL IS NULL THEN (select top 1 [SAPMCTRL] from [SAPMAS] where [SAPMAT] = QIDMAT) ELSE QIDMTCTRL END) = '" + mat + "' \n"
                + "AND QIDWHS = '" + wh + "' \n"
                + "AND (QIDSTS = '5') AND QIDSHDT is null) AS QPACK,\n \n"
                + "(SELECT COUNT(DISTINCT CASE WHEN QIDGPQR IS NULL THEN QIDID WHEN QIDGPQR = '' THEN QIDID ELSE QIDGPQR END)\n"
                + "FROM QIDETAIL \n"
                + "WHERE (select top 1 QIHDEST from QIHEAD where QIHQNO = QIDQNO) = '" + dest + "' \n"
                + "AND (CASE WHEN QIDMTCTRL = '' OR QIDMTCTRL IS NULL THEN (select top 1 [SAPMCTRL] from [SAPMAS] where [SAPMAT] = QIDMAT) ELSE QIDMTCTRL END) = '" + mat + "' \n"
                + "AND QIDWHS = '" + wh + "' \n"
                + "AND (QIDSTS = '5')\n"
                + "AND QIDPACKTYPE = 'BAG' AND QIDSHDT is null) AS BAG, \n"
                + "(SELECT COUNT(DISTINCT CASE WHEN QIDGPQR IS NULL THEN QIDID WHEN QIDGPQR = '' THEN QIDID ELSE QIDGPQR END)\n"
                + "FROM QIDETAIL \n"
                + "WHERE (select top 1 QIHDEST from QIHEAD where QIHQNO = QIDQNO) = '" + dest + "' \n"
                + "AND (CASE WHEN QIDMTCTRL = '' OR QIDMTCTRL IS NULL THEN (select top 1 [SAPMCTRL] from [SAPMAS] where [SAPMAT] = QIDMAT) ELSE QIDMTCTRL END) = '" + mat + "' \n"
                + "AND QIDWHS = '" + wh + "' \n"
                + "AND (QIDSTS = '5')\n"
                + "AND QIDPACKTYPE = 'ROLL' AND QIDSHDT is null) AS ROLL, \n"
                + "(SELECT COUNT(DISTINCT CASE WHEN QIDGPQR IS NULL THEN QIDID WHEN QIDGPQR = '' THEN QIDID ELSE QIDGPQR END)\n"
                + "FROM QIDETAIL \n"
                + "WHERE (select top 1 QIHDEST from QIHEAD where QIHQNO = QIDQNO) = '" + dest + "' \n"
                + "AND (CASE WHEN QIDMTCTRL = '' OR QIDMTCTRL IS NULL THEN (select top 1 [SAPMCTRL] from [SAPMAS] where [SAPMAT] = QIDMAT) ELSE QIDMTCTRL END) = '" + mat + "' \n"
                + "AND QIDWHS = '" + wh + "' \n"
                + "AND (QIDSTS = '5')\n"
                + "AND QIDPACKTYPE = 'BOX' AND QIDSHDT is null) AS BOX, \n"
                + "0 AS M3 \n"
                + "FROM QIDETAIL \n"
                + "LEFT JOIN QRMSTS \n"
                + "ON QIDETAIL.QIDSTS = QRMSTS.STSCODE \n"
                + "WHERE (select top 1 QIHDEST from QIHEAD where QIHQNO = QIDQNO) = '" + dest + "' \n"
                + "AND (CASE WHEN QIDMTCTRL = '' OR QIDMTCTRL IS NULL THEN (select top 1 [SAPMCTRL] from [SAPMAS] where [SAPMAT] = QIDMAT) ELSE QIDMTCTRL END) = '" + mat + "' \n"
                + "AND QIDWHS = '" + wh + "' \n"
                + "AND (QIDSTS = '5') AND QIDSHDT is null ";
//        System.out.println(sql);
//        System.out.println("**************************");
        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                p[0] = result.getString("SKU");
                p[1] = result.getString("QPACK");
                p[2] = result.getString("M3");
                p[3] = result.getString("BAG");
                p[4] = result.getString("ROLL");
                p[5] = result.getString("BOX");

            }

        } catch (Exception e) {

            e.printStackTrace();

        }

        return p;

    }

    public String[] findSKUforPopupDEST341(String dest, String wh, String mat) {

        String[] p = new String[3];

        String sql = "SELECT COUNT(DISTINCT QIDMAT) AS SKU, \n"
                + "(SELECT COUNT(DISTINCT CASE WHEN QIDGPQR IS NULL THEN QIDID WHEN QIDGPQR = '' THEN QIDID ELSE QIDGPQR END)\n"
                + "FROM QIDETAIL \n"
                + "WHERE (select top 1 QIHDEST from QIHEAD where QIHQNO = QIDQNO) = '" + dest + "' \n"
                + "AND (select top 1 QIHMTCTRL from QIHEAD where QIHQNO = QIDQNO) = '" + mat + "' \n"
                + "AND QIDWHS = '" + wh + "' \n"
                + "AND (QIDSTS = '5' OR QIDSTS = '6' OR QIDSTS = '7')) AS QPACK, \n"
                + "0 AS M3 \n"
                + "FROM QIDETAIL \n"
                + "LEFT JOIN QRMSTS \n"
                + "ON QIDETAIL.QIDSTS = QRMSTS.STSCODE \n"
                + "WHERE (select top 1 QIHDEST from QIHEAD where QIHQNO = QIDQNO) = '" + dest + "' \n"
                + "AND (select top 1 QIHMTCTRL from QIHEAD where QIHQNO = QIDQNO) = '" + mat + "' \n"
                + "AND QIDWHS = '" + wh + "' \n"
                + "AND (QIDSTS = '5' OR QIDSTS = '6' OR QIDSTS = '7') ";

        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                p[0] = result.getString("SKU");
                p[1] = result.getString("QPACK");
                p[2] = result.getString("M3");

            }

        } catch (Exception e) {

            e.printStackTrace();

        }

        return p;

    }

    public String findSKUforPopupDEST320(String dest, String wh) {

        String sku = "";
        List<String> matList = new ArrayList<String>();

        String sql = "SELECT QIHQNO \n"
                + "FROM QIHEAD\n"
                + "WHERE QIHDEST =  '" + dest + "'\n"
                + "AND QIHWHS = '" + wh + "'\n";

        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                String qno = result.getString("QIHQNO");

                QIHEADDao dao = new QIHEADDao();
                List<String> mList = dao.findSubSKUPopupDEST340(qno);

                for (int i = 0; i < mList.size(); i++) {
                    if (!matList.contains(mList.get(i))) {
                        matList.add(mList.get(i));
                    }
                }

            }

            sku = Integer.toString(matList.size());

        } catch (Exception e) {

            e.printStackTrace();

        }

        return sku;

    }

    public List<String> findSubSKUPopupDEST340(String qno) {

        List<String> UAList = new ArrayList<String>();

        String sql = "SELECT DISTINCT QIDMAT FROM QIDETAIL WHERE QIDQNO = '" + qno + "'";

        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                UAList.add(result.getString("QIDMAT"));

            }

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public List<MSSMATN> findPopup(String dest, String whF, String whT) {

        List<MSSMATN> UAList = new ArrayList<MSSMATN>();

        String WHS = "";

        if (!whF.equals("")) {
            if (!whF.equals("TWC") && !whT.equals("TWC")) {
                WHS = "AND QWHCOD BETWEEN '" + whF + "' AND '" + whT + "' ";
            }
        }

        String sql = "SELECT QWHCOD + ' : ' + QWHNAME AS WHS, QWHCOD, "
                + "(SELECT SUM(SKU) "
                + "FROM( "
                + "SELECT (SELECT COUNT(DISTINCT QIDMAT) FROM QIDETAIL WHERE QIDQNO = QIHQNO AND (QIDSTS = '2' OR QIDSTS = '5')) AS SKU "
                + "FROM QIHEAD "
                + "WHERE QIHDEST = '" + dest + "' "
                + "AND QIHWHS = QWHCOD "
                + ") SKU) AS SKU, "
                + "(SELECT SUM(QPACK) "
                + "FROM( "
                + "SELECT (SELECT COUNT(QIDPACKTYPE) FROM QIDETAIL WHERE QIDQNO = QIHQNO AND (QIDSTS = '2' OR QIDSTS = '5')) AS QPACK "
                + "FROM QIHEAD "
                + "WHERE QIHDEST = '" + dest + "' "
                + "AND QIHWHS = QWHCOD "
                + ") QPK) AS QPACK, "
                + "0 AS M3 "
                + "FROM QWHMAS  "
                + "WHERE QWHCOD != 'TWC' "
                + WHS;

        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            int sumSKU = 0;
            int sumQPACK = 0;

            while (result.next()) {

                MSSMATN p = new MSSMATN();

                p.setWarehouse(result.getString("WHS")); //WH

                if (result.getString("SKU") != null) {
                    p.setUid(result.getString("SKU")); //SKU
                    sumSKU += Integer.parseInt(result.getString("SKU"));
                } else {
                    p.setUid("0"); //SKU
                }

                if (result.getString("QPACK") != null) {
                    p.setName(result.getString("QPACK")); //QPACK
                    sumQPACK += Integer.parseInt(result.getString("QPACK"));
                } else {
                    p.setName("0"); //QPACK
                }

                if (result.getString("M3") != null) {
                    p.setDesc(result.getString("M3")); //M3
                } else {
                    p.setDesc("0"); //M3
                }

                if (!result.getString("QPACK").equals("0")) {
                    UAList.add(p);
                }

            }

            MSSMATN p = new MSSMATN();

            p.setWarehouse("TWC : ���. ������"); //WH
            p.setUid(Integer.toString(sumSKU)); //SKU
            p.setName(Integer.toString(sumQPACK)); //QPACK
            p.setDesc("0"); //M3

            UAList.add(p);

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public List<MSSMATN> findPopupMat(String mat) {

        List<MSSMATN> UAList = new ArrayList<MSSMATN>();

        String sql = "SELECT distinct HD.QIHWHS, HD.QIHMTCTRL, DE.QDETYPE, HD.QIHDEST, DE.QDEDESC,\n"
                + "(SELECT SUM(SKU)\n"
                + "FROM(\n"
                + "SELECT (SELECT COUNT(DISTINCT QIDMAT) FROM QIDETAIL WHERE QIDQNO = QIHQNO AND QIDSTS = '2') AS SKU\n"
                + "FROM QIHEAD\n"
                + "WHERE QIHDEST =  HD.QIHDEST\n"
                + "AND QIHMTCTRL = HD.QIHMTCTRL\n"
                + ") SKU) AS SKU,\n"
                + "(SELECT SUM(QPACK)\n"
                + "FROM(\n"
                + "SELECT (SELECT COUNT(QIDPACKTYPE) FROM QIDETAIL WHERE QIDQNO = QIHQNO AND QIDSTS = '2') AS QPACK\n"
                + "FROM QIHEAD\n"
                + "WHERE QIHDEST = HD.QIHDEST\n"
                + "AND QIHMTCTRL = HD.QIHMTCTRL\n"
                + ") QPK) AS QPACK,\n"
                + "0 As M3\n"
                + "FROM QIHEAD HD\n"
                + "INNER JOIN QDEST DE\n"
                + "ON HD.QIHDEST = DE.QDECOD \n"
                + "WHERE QIHMTCTRL = '" + mat + "' \n"
                + "ORDER BY DE.QDETYPE, HD.QIHDEST";

        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            int sumSKU = 0;
            int sumQPK = 0;
            int sumBAG = 0;
            int sumROLL = 0;
            int sumBOX = 0;

            while (result.next()) {

                MSSMATN p = new MSSMATN();

                p.setUid(result.getString("QIHDEST") + " : " + result.getString("QDEDESC")); //DEST

                QIHEADDao dao = new QIHEADDao();
                String[] pop = dao.findSKUforPopupDEST340(result.getString("QIHDEST").trim(),
                        result.getString("QIHWHS").trim(), result.getString("QIHMTCTRL").trim());

                p.setName(pop[0]); //SKU

                p.setWarehouse(pop[1]); //QPACK
                p.setDesc(pop[2]); //M3

                p.setTolQno(pop[3]); //bag
                p.setTolQty(pop[4]); //roll
                p.setBox(pop[5]); //box

                sumSKU += Integer.parseInt(pop[0]);
                sumQPK += Integer.parseInt(pop[1]);
                sumBAG += Integer.parseInt(pop[3]);
                sumROLL += Integer.parseInt(pop[4]);
                sumBOX += Integer.parseInt(pop[5]);

                if (!pop[1].equals("0")) {
                    UAList.add(p);
                }

            }

            MSSMATN p = new MSSMATN();

            p.setUid("TOTAL Destination"); //DEST
            p.setName(Integer.toString(sumSKU)); //SKU
            p.setWarehouse(Integer.toString(sumQPK)); //QPACK
            p.setDesc("0"); //M3

            p.setTolQno(Integer.toString(sumBAG)); //bag
            p.setTolQty(Integer.toString(sumROLL)); //roll
            p.setBox(Integer.toString(sumBOX)); //box

            UAList.add(p);
        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public List<MSSMATN> findPopupMatX(String mat) {

        List<MSSMATN> UAList = new ArrayList<MSSMATN>();

//        String sql = "SELECT distinct HD.QIHWHS, HD.QIHMTCTRL, DE.QDETYPE, HD.QIHDEST, DE.QDEDESC,\n"
//                + "(SELECT SUM(SKU)\n"
//                + "FROM(\n"
//                + "SELECT (SELECT COUNT(DISTINCT QIDMAT) FROM QIDETAIL WHERE QIDQNO = QIHQNO AND QIDSTS = '5') AS SKU\n"
//                + "FROM QIHEAD\n"
//                + "WHERE QIHDEST =  HD.QIHDEST\n"
//                + "AND QIHMTCTRL = HD.QIHMTCTRL\n"
//                + ") SKU) AS SKU,\n"
//                + "(SELECT SUM(QPACK)\n"
//                + "FROM(\n"
//                + "SELECT (SELECT COUNT(QIDPACKTYPE) FROM QIDETAIL WHERE QIDQNO = QIHQNO AND QIDSTS = '5') AS QPACK\n"
//                + "FROM QIHEAD\n"
//                + "WHERE QIHDEST = HD.QIHDEST\n"
//                + "AND QIHMTCTRL = HD.QIHMTCTRL\n"
//                + ") QPK) AS QPACK,\n"
//                + "0 As M3\n"
//                + "FROM QIHEAD HD\n"
//                + "INNER JOIN QDEST DE\n"
//                + "ON HD.QIHDEST = DE.QDECOD \n"
//                + "WHERE QIHMTCTRL = '" + mat + "' \n"
//                + "ORDER BY DE.QDETYPE, HD.QIHDEST";
        String sql = "SELECT distinct HD.QIHWHS\n"
                + ",CASE WHEN QIDMTCTRL = '' OR QIDMTCTRL IS NULL THEN (select top 1 [SAPMCTRL] from [SAPMAS] where [SAPMAT] = QIDMAT) ELSE QIDMTCTRL END AS QIHMTCTRL\n"
                + ", DE.QDETYPE, HD.QIHDEST, DE.QDEDESC\n"
                + "FROM QIHEAD HD\n"
                + "LEFT JOIN QIDETAIL ON QIDQNO = QIHQNO\n"
                + "INNER JOIN QDEST DE\n"
                + "ON HD.QIHDEST = DE.QDECOD \n"
                + "WHERE (CASE WHEN QIDMTCTRL = '' OR QIDMTCTRL IS NULL THEN (select top 1 [SAPMCTRL] from [SAPMAS] where [SAPMAT] = QIDMAT) ELSE QIDMTCTRL END) = '" + mat + "' \n"
                + "ORDER BY DE.QDETYPE, HD.QIHDEST";
//        System.out.println(sql);
//        System.out.println("********");

        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            int sumSKU = 0;
            int sumQPK = 0;
            int sumBAG = 0;
            int sumROLL = 0;
            int sumBOX = 0;

            while (result.next()) {

                MSSMATN p = new MSSMATN();

                p.setUid(result.getString("QIHDEST") + " : " + result.getString("QDEDESC")); //DEST

                QIHEADDao dao = new QIHEADDao();
                String[] pop = dao.findSKUforPopupDEST340X(result.getString("QIHDEST").trim(),
                        result.getString("QIHWHS").trim(), result.getString("QIHMTCTRL").trim());

                p.setName(pop[0]); //SKU

                p.setWarehouse(pop[1]); //QPACK
                p.setDesc(pop[2]); //M3

                p.setTolQno(pop[3]); //bag
                p.setTolQty(pop[4]); //roll
                p.setBox(pop[5]); //box

                if (!pop[1].equals("0")) {
                    UAList.add(p);

                    sumSKU += Integer.parseInt(pop[0]);
                    sumQPK += Integer.parseInt(pop[1]);
                    sumBAG += Integer.parseInt(pop[3]);
                    sumROLL += Integer.parseInt(pop[4]);
                    sumBOX += Integer.parseInt(pop[5]);
                }

            }

            MSSMATN p = new MSSMATN();

            p.setUid("TOTAL Destination"); //DEST
            p.setName(Integer.toString(sumSKU)); //SKU
            p.setWarehouse(Integer.toString(sumQPK)); //QPACK
            p.setDesc("0"); //M3

            p.setTolQno(Integer.toString(sumBAG)); //bag
            p.setTolQty(Integer.toString(sumROLL)); //roll
            p.setBox(Integer.toString(sumBOX)); //box

            UAList.add(p);
        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public List<MSSMATN> findPopupMat341(String mat) {

        List<MSSMATN> UAList = new ArrayList<MSSMATN>();

        String sql = "SELECT distinct HD.QIHWHS, HD.QIHMTCTRL, DE.QDETYPE, HD.QIHDEST, DE.QDEDESC,\n"
                + "(SELECT SUM(SKU)\n"
                + "FROM(\n"
                + "SELECT (SELECT COUNT(DISTINCT QIDMAT) FROM QIDETAIL WHERE QIDQNO = QIHQNO AND (QIDSTS = '5' OR QIDSTS = '6' OR QIDSTS = '7')) AS SKU\n"
                + "FROM QIHEAD\n"
                + "WHERE QIHDEST =  HD.QIHDEST\n"
                + "AND QIHMTCTRL = HD.QIHMTCTRL\n"
                + ") SKU) AS SKU,\n"
                + "(SELECT SUM(QPACK)\n"
                + "FROM(\n"
                + "SELECT (SELECT COUNT(QIDPACKTYPE) FROM QIDETAIL WHERE QIDQNO = QIHQNO AND (QIDSTS = '5' OR QIDSTS = '6' OR QIDSTS = '7')) AS QPACK\n"
                + "FROM QIHEAD\n"
                + "WHERE QIHDEST = HD.QIHDEST\n"
                + "AND QIHMTCTRL = HD.QIHMTCTRL\n"
                + ") QPK) AS QPACK,\n"
                + "0 As M3\n"
                + "FROM QIHEAD HD\n"
                + "INNER JOIN QDEST DE\n"
                + "ON HD.QIHDEST = DE.QDECOD \n"
                + "WHERE QIHMTCTRL = '" + mat + "' \n"
                + "ORDER BY DE.QDETYPE, HD.QIHDEST";

        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            int sumSKU = 0;
            int sumQPK = 0;

            while (result.next()) {

                MSSMATN p = new MSSMATN();

                p.setUid(result.getString("QIHDEST") + " : " + result.getString("QDEDESC")); //DEST

                QIHEADDao dao = new QIHEADDao();
                String[] pop = dao.findSKUforPopupDEST341(result.getString("QIHDEST").trim(),
                        result.getString("QIHWHS").trim(), result.getString("QIHMTCTRL").trim());

                p.setName(pop[0]); //SKU

                p.setWarehouse(pop[1]); //QPACK
                p.setDesc(pop[2]); //M3

                sumSKU += Integer.parseInt(pop[0]);
                sumQPK += Integer.parseInt(pop[1]);

                if (!pop[1].equals("0")) {
                    UAList.add(p);
                }

            }

            MSSMATN p = new MSSMATN();

            p.setUid("TOTAL Destination"); //DEST
            p.setName(Integer.toString(sumSKU)); //SKU
            p.setWarehouse(Integer.toString(sumQPK)); //QPACK
            p.setDesc("0"); //M3

            UAList.add(p);

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public List<MSSMATN> findSubMenuForDest(String dest, String wh) {

        List<MSSMATN> UAList = new ArrayList<MSSMATN>();

        String sql = "SELECT QIHWHS, QIHMTCTRL "
                + "FROM QIHEAD "
                + "WHERE QIHSTS = '2'"
                + "AND QIHWHS = '" + wh + "' "
                + "AND QIHDEST = '" + dest + "' ";

        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            String oldWH = "";

            while (result.next()) {

                MSSMATN p = new MSSMATN();

                p.setWarehouse(result.getString("QIHWHS"));
                p.setUid(result.getString("QIHMTCTRL"));

                if (!result.getString("QIHWHS").equals(oldWH)) {
                    String newWHS = "<hr style=\"height: 3px;\"><b>" + result.getString("QIHWHS") + "</b><br>";
                    p.setName(newWHS);
                    oldWH = result.getString("QIHWHS");
                }

                UAList.add(p);

            }

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public List<MSSMATN> findMatCForDest340(String dest, String wh, String mat) {

        List<MSSMATN> UAList = new ArrayList<MSSMATN>();

        String MAT = "";
        if (!mat.trim().equals("TOTAL")) {
            MAT = "AND QIHMTCTRL = '" + mat + "' ";
        }

        String WHS = "";
        if (!wh.trim().equals("TWC")) {
            WHS = "AND QIHWHS = '" + wh + "' ";
        }

        String sql = "SELECT distinct QIHWHS, QWHNAME, QIHDEST, QIHMTCTRL, MATCNAME "
                + "FROM QIHEAD "
                + "LEFT JOIN MSSMATN "
                + "ON QIHEAD.QIHMTCTRL = MSSMATN.LGPBE "
                + "LEFT JOIN QWHMAS "
                + "ON QIHEAD.QIHWHS = QWHMAS.QWHCOD "
                + "WHERE QIHDEST = '" + dest + "' "
                + MAT
                + WHS
                + "ORDER BY QIHWHS, QIHDEST, QIHMTCTRL ";

        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            String oldWH = "";

            while (result.next()) {

                MSSMATN p = new MSSMATN();

                p.setUid(result.getString("QIHMTCTRL"));
                p.setName(result.getString("MATCNAME"));

                String whname = result.getString("QIHWHS") + " : " + result.getString("QWHNAME");

                String whsort = "<hr style=\"border-top: 3px solid #ccc;\">"
                        + "<table width=\"100%\">\n"
                        + "                            <tr>\n"
                        + "                                <td width=\"50%\" align=\"left\"><b style=\"color: #00399b;\">Warehouse :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + whname + "</b></td>\n"
                        + "                            </tr>\n"
                        + "                        </table>";

                String whsort1 = "<table width=\"100%\">\n"
                        + "                            <tr>\n"
                        + "                                <td width=\"50%\" align=\"left\"><b style=\"color: #00399b;\">Warehouse :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + whname + "</b></td>\n"
                        + "                            </tr>\n"
                        + "                        </table>";

//                String endWH = "<hr style=\"height: 3px;\">"
//                        + "<table width=\"100%\">\n"
//                        + "                        <tr>\n"
//                        + "                            <td width=\"100%\" align=\"center\"><b style=\"color: #00399b;\">*** End of " + whname + " ***</b></td>\n"
//                        + "                        </tr>\n"
//                        + "                    </table>";
                if (oldWH.equals("")) {
                    p.setWarehouse(whsort1);
                    oldWH = result.getString("QIHWHS").trim();
                } else if (!oldWH.equals(result.getString("QIHWHS").trim())) {
                    p.setWarehouse(whsort);
                    oldWH = result.getString("QIHWHS").trim();
                }

//                p.setDesc(endWH);
                QIDETAILDao dao = new QIDETAILDao();
                List<QIDETAIL> detList = dao.findDetailDest340(result.getString("QIHWHS"),
                        result.getString("QIHDEST"), result.getString("QIHMTCTRL"));

                p.setDetList(detList);

                if (detList.size() > 1) {
                    UAList.add(p);
                }

            }

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public List<MSSMATN> findMatCForDest341(String dest, String wh, String mat) {

        List<MSSMATN> UAList = new ArrayList<MSSMATN>();

        String MAT = "";
        if (!mat.trim().equals("TOTAL")) {
            MAT = "AND QIHMTCTRL = '" + mat + "' ";
        }

        String WHS = "";
        if (!wh.trim().equals("TWC")) {
            WHS = "AND QIHWHS = '" + wh + "' ";
        }

        String sql = "SELECT distinct QIHWHS, QWHNAME, QIHDEST, QIHMTCTRL, MATCNAME "
                + "FROM QIHEAD "
                + "LEFT JOIN MSSMATN "
                + "ON QIHEAD.QIHMTCTRL = MSSMATN.LGPBE "
                + "LEFT JOIN QWHMAS "
                + "ON QIHEAD.QIHWHS = QWHMAS.QWHCOD "
                + "WHERE QIHDEST = '" + dest + "' "
                + MAT
                + WHS
                + "ORDER BY QIHWHS, QIHDEST, QIHMTCTRL ";

        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            String oldWH = "";

            while (result.next()) {

                MSSMATN p = new MSSMATN();

                p.setUid(result.getString("QIHMTCTRL"));
                p.setName(result.getString("MATCNAME"));

                String whname = result.getString("QIHWHS") + " : " + result.getString("QWHNAME");

                String whsort = "<hr style=\"border-top: 3px solid #ccc;\">"
                        + "<table width=\"100%\">\n"
                        + "                            <tr>\n"
                        + "                                <td width=\"50%\" align=\"left\"><b style=\"color: #00399b;\">Warehouse :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + whname + "</b></td>\n"
                        + "                            </tr>\n"
                        + "                        </table>";

                String whsort1 = "<table width=\"100%\">\n"
                        + "                            <tr>\n"
                        + "                                <td width=\"50%\" align=\"left\"><b style=\"color: #00399b;\">Warehouse :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + whname + "</b></td>\n"
                        + "                            </tr>\n"
                        + "                        </table>";

//                String endWH = "<hr style=\"height: 3px;\">"
//                        + "<table width=\"100%\">\n"
//                        + "                        <tr>\n"
//                        + "                            <td width=\"100%\" align=\"center\"><b style=\"color: #00399b;\">*** End of " + whname + " ***</b></td>\n"
//                        + "                        </tr>\n"
//                        + "                    </table>";
                if (oldWH.equals("")) {
                    p.setWarehouse(whsort1);
                    oldWH = result.getString("QIHWHS").trim();
                } else if (!oldWH.equals(result.getString("QIHWHS").trim())) {
                    p.setWarehouse(whsort);
                    oldWH = result.getString("QIHWHS").trim();
                }

//                p.setDesc(endWH);
                QIDETAILDao dao = new QIDETAILDao();
                List<QIDETAIL> detList = dao.findDetailDest341(result.getString("QIHWHS"),
                        result.getString("QIHDEST"), result.getString("QIHMTCTRL"));

                p.setDetList(detList);

                if (detList.size() > 1) {
                    UAList.add(p);
                }

            }

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public List<MSSMATN> findDestForMatC340All(String dest, String wh, String mat) {

        List<MSSMATN> UAList = new ArrayList<MSSMATN>();

        String WHS = "";
        if (!wh.trim().equals("TWC")) {
            WHS = "AND QIHWHS = '" + wh + "' ";
        }

        String sql = "SELECT distinct QIHWHS, QWHNAME, QIHMTCTRL, QIHDEST, QDEDESC "
                + "FROM QIHEAD "
                + "INNER JOIN QDEST "
                + "ON QIHEAD.QIHDEST = QDEST.QDECOD "
                + "INNER JOIN QWHMAS "
                + "ON QIHEAD.QIHWHS = QWHMAS.QWHCOD "
                + "WHERE QIHMTCTRL = '" + mat + "' "
                + WHS
                + "ORDER BY QIHWHS, QIHMTCTRL, QIHDEST ";

        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            String oldWH = "";

            while (result.next()) {

                MSSMATN p = new MSSMATN();

                p.setUid(result.getString("QIHDEST"));
                p.setName(result.getString("QDEDESC"));

                String whname = result.getString("QIHWHS") + " : " + result.getString("QWHNAME");

                String whsort = "<hr style=\"border-top: 3px solid #ccc;\">"
                        + "<table width=\"100%\">\n"
                        + "                            <tr>\n"
                        + "                                <td width=\"50%\" align=\"left\"><b style=\"color: #00399b;\">Warehouse :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + whname + "</b></td>\n"
                        + "                            </tr>\n"
                        + "                        </table>";

                String whsort1 = "<table width=\"100%\">\n"
                        + "                            <tr>\n"
                        + "                                <td width=\"50%\" align=\"left\"><b style=\"color: #00399b;\">Warehouse :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + whname + "</b></td>\n"
                        + "                            </tr>\n"
                        + "                        </table>";

//                String endWH = "<hr style=\"height: 3px;\">"
//                        + "<table width=\"100%\">\n"
//                        + "                        <tr>\n"
//                        + "                            <td width=\"100%\" align=\"center\"><b style=\"color: #00399b;\">*** End of " + whname + " ***</b></td>\n"
//                        + "                        </tr>\n"
//                        + "                    </table>";
                if (oldWH.equals("")) {
                    p.setWarehouse(whsort1);
                    oldWH = result.getString("QIHWHS").trim();
                } else if (!oldWH.equals(result.getString("QIHWHS").trim())) {
                    p.setWarehouse(whsort);
                    oldWH = result.getString("QIHWHS").trim();
                }

//                p.setDesc(endWH);
                QIDETAILDao dao = new QIDETAILDao();
                List<QIDETAIL> detList = dao.findDetailMat340(result.getString("QIHWHS"),
                        result.getString("QIHDEST"), result.getString("QIHMTCTRL"));

                p.setDetList(detList);

                if (detList.size() > 1) {
                    UAList.add(p);
                }

            }

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public List<MSSMATN> findDestForMatC341All(String dest, String wh, String mat) {

        List<MSSMATN> UAList = new ArrayList<MSSMATN>();

        String WHS = "";
        if (!wh.trim().equals("TWC")) {
            WHS = "AND QIHWHS = '" + wh + "' ";
        }

        String sql = "SELECT distinct QIHWHS, QWHNAME, QIHMTCTRL, QIHDEST, QDEDESC "
                + "FROM QIHEAD "
                + "INNER JOIN QDEST "
                + "ON QIHEAD.QIHDEST = QDEST.QDECOD "
                + "INNER JOIN QWHMAS "
                + "ON QIHEAD.QIHWHS = QWHMAS.QWHCOD "
                + "WHERE QIHMTCTRL = '" + mat + "' "
                + WHS
                + "ORDER BY QIHWHS, QIHMTCTRL, QIHDEST ";

        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            String oldWH = "";

            while (result.next()) {

                MSSMATN p = new MSSMATN();

                p.setUid(result.getString("QIHDEST"));
                p.setName(result.getString("QDEDESC"));

                String whname = result.getString("QIHWHS") + " : " + result.getString("QWHNAME");

                String whsort = "<hr style=\"border-top: 3px solid #ccc;\">"
                        + "<table width=\"100%\">\n"
                        + "                            <tr>\n"
                        + "                                <td width=\"50%\" align=\"left\"><b style=\"color: #00399b;\">Warehouse :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + whname + "</b></td>\n"
                        + "                            </tr>\n"
                        + "                        </table>";

                String whsort1 = "<table width=\"100%\">\n"
                        + "                            <tr>\n"
                        + "                                <td width=\"50%\" align=\"left\"><b style=\"color: #00399b;\">Warehouse :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + whname + "</b></td>\n"
                        + "                            </tr>\n"
                        + "                        </table>";

//                String endWH = "<hr style=\"height: 3px;\">"
//                        + "<table width=\"100%\">\n"
//                        + "                        <tr>\n"
//                        + "                            <td width=\"100%\" align=\"center\"><b style=\"color: #00399b;\">*** End of " + whname + " ***</b></td>\n"
//                        + "                        </tr>\n"
//                        + "                    </table>";
                if (oldWH.equals("")) {
                    p.setWarehouse(whsort1);
                    oldWH = result.getString("QIHWHS").trim();
                } else if (!oldWH.equals(result.getString("QIHWHS").trim())) {
                    p.setWarehouse(whsort);
                    oldWH = result.getString("QIHWHS").trim();
                }

//                p.setDesc(endWH);
                QIDETAILDao dao = new QIDETAILDao();
                List<QIDETAIL> detList = dao.findDetailMat341(result.getString("QIHWHS"),
                        result.getString("QIHDEST"), result.getString("QIHMTCTRL"));

                p.setDetList(detList);

                if (detList.size() > 1) {
                    UAList.add(p);
                }

            }

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public List<MSSMATN> findDestForMatC340(String dest, String wh, String mat) {

        List<MSSMATN> UAList = new ArrayList<MSSMATN>();

        String WHS = "";
        if (!wh.trim().equals("TWC")) {
            WHS = "AND QIHWHS = '" + wh + "' ";
        }

        String sql = "SELECT distinct QIHWHS, QWHNAME, QIHMTCTRL, QIHDEST, QDEDESC "
                + "FROM QIHEAD "
                + "INNER JOIN QDEST "
                + "ON QIHEAD.QIHDEST = QDEST.QDECOD "
                + "INNER JOIN QWHMAS "
                + "ON QIHEAD.QIHWHS = QWHMAS.QWHCOD "
                + "WHERE QIHMTCTRL = '" + mat + "' "
                + "AND QIHDEST = '" + dest + "' "
                + WHS
                + "ORDER BY QIHWHS, QIHMTCTRL, QIHDEST ";

        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            String oldWH = "";

            while (result.next()) {

                MSSMATN p = new MSSMATN();

                p.setUid(result.getString("QIHDEST"));
                p.setName(result.getString("QDEDESC"));

                String whname = result.getString("QIHWHS") + " : " + result.getString("QWHNAME");

                String whsort = "<hr style=\"border-top: 3px solid #ccc;\">"
                        + "<table width=\"100%\">\n"
                        + "                            <tr>\n"
                        + "                                <td width=\"50%\" align=\"left\"><b style=\"color: #00399b;\">Warehouse :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + whname + "</b></td>\n"
                        + "                            </tr>\n"
                        + "                        </table>";

                String whsort1 = "<table width=\"100%\">\n"
                        + "                            <tr>\n"
                        + "                                <td width=\"50%\" align=\"left\"><b style=\"color: #00399b;\">Warehouse :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + whname + "</b></td>\n"
                        + "                            </tr>\n"
                        + "                        </table>";

//                String endWH = "<hr style=\"height: 3px;\">"
//                        + "<table width=\"100%\">\n"
//                        + "                        <tr>\n"
//                        + "                            <td width=\"100%\" align=\"center\"><b style=\"color: #00399b;\">*** End of " + whname + " ***</b></td>\n"
//                        + "                        </tr>\n"
//                        + "                    </table>";
                if (oldWH.equals("")) {
                    p.setWarehouse(whsort1);
                    oldWH = result.getString("QIHWHS").trim();
                } else if (!oldWH.equals(result.getString("QIHWHS").trim())) {
                    p.setWarehouse(whsort);
                    oldWH = result.getString("QIHWHS").trim();
                }

//                p.setDesc(endWH);
                QIDETAILDao dao = new QIDETAILDao();
                List<QIDETAIL> detList = dao.findDetailMat340(result.getString("QIHWHS"),
                        result.getString("QIHDEST"), result.getString("QIHMTCTRL"));

                p.setDetList(detList);

                if (detList.size() > 1) {
                    UAList.add(p);
                }

            }

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public List<MSSMATN> findDestForMatC341(String dest, String wh, String mat) {

        List<MSSMATN> UAList = new ArrayList<MSSMATN>();

        String WHS = "";
        if (!wh.trim().equals("TWC")) {
            WHS = "AND QIHWHS = '" + wh + "' ";
        }

        String sql = "SELECT distinct QIHWHS, QWHNAME, QIHMTCTRL, QIHDEST, QDEDESC "
                + "FROM QIHEAD "
                + "INNER JOIN QDEST "
                + "ON QIHEAD.QIHDEST = QDEST.QDECOD "
                + "INNER JOIN QWHMAS "
                + "ON QIHEAD.QIHWHS = QWHMAS.QWHCOD "
                + "WHERE QIHMTCTRL = '" + mat + "' "
                + "AND QIHDEST = '" + dest + "' "
                + WHS
                + "ORDER BY QIHWHS, QIHMTCTRL, QIHDEST ";

        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            String oldWH = "";

            while (result.next()) {

                MSSMATN p = new MSSMATN();

                p.setUid(result.getString("QIHDEST"));
                p.setName(result.getString("QDEDESC"));

                String whname = result.getString("QIHWHS") + " : " + result.getString("QWHNAME");

                String whsort = "<hr style=\"border-top: 3px solid #ccc;\">"
                        + "<table width=\"100%\">\n"
                        + "                            <tr>\n"
                        + "                                <td width=\"50%\" align=\"left\"><b style=\"color: #00399b;\">Warehouse :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + whname + "</b></td>\n"
                        + "                            </tr>\n"
                        + "                        </table>";

                String whsort1 = "<table width=\"100%\">\n"
                        + "                            <tr>\n"
                        + "                                <td width=\"50%\" align=\"left\"><b style=\"color: #00399b;\">Warehouse :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + whname + "</b></td>\n"
                        + "                            </tr>\n"
                        + "                        </table>";

//                String endWH = "<hr style=\"height: 3px;\">"
//                        + "<table width=\"100%\">\n"
//                        + "                        <tr>\n"
//                        + "                            <td width=\"100%\" align=\"center\"><b style=\"color: #00399b;\">*** End of " + whname + " ***</b></td>\n"
//                        + "                        </tr>\n"
//                        + "                    </table>";
                if (oldWH.equals("")) {
                    p.setWarehouse(whsort1);
                    oldWH = result.getString("QIHWHS").trim();
                } else if (!oldWH.equals(result.getString("QIHWHS").trim())) {
                    p.setWarehouse(whsort);
                    oldWH = result.getString("QIHWHS").trim();
                }

//                p.setDesc(endWH);
                QIDETAILDao dao = new QIDETAILDao();
                List<QIDETAIL> detList = dao.findDetailMat341(result.getString("QIHWHS"),
                        result.getString("QIHDEST"), result.getString("QIHMTCTRL"));

                p.setDetList(detList);

                if (detList.size() > 1) {
                    UAList.add(p);
                }

            }

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public List<MSSMATN> findMatCForDest(String dest, String wh) {

        List<MSSMATN> UAList = new ArrayList<MSSMATN>();

        String WHS = "";
        if (!wh.trim().equals("TWC")) {
            WHS = "AND QIHWHS = '" + wh + "' ";
        }

        String sql = "SELECT distinct QIHWHS, QWHNAME, QIHDEST, QIHMTCTRL, MATCNAME "
                + "FROM QIHEAD "
                + "INNER JOIN MSSMATN "
                + "ON QIHEAD.QIHMTCTRL = MSSMATN.LGPBE "
                + "INNER JOIN QWHMAS "
                + "ON QIHEAD.QIHWHS = QWHMAS.QWHCOD "
                + "WHERE QIHDEST = '" + dest + "' "
                + WHS
                + "ORDER BY QIHWHS, QIHDEST, QIHMTCTRL ";

        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            String oldWH = "";

            while (result.next()) {

                MSSMATN p = new MSSMATN();

                p.setUid(result.getString("QIHMTCTRL"));
                p.setName(result.getString("MATCNAME"));

                String whname = result.getString("QIHWHS") + " : " + result.getString("QWHNAME");

                String whsort = "<hr style=\"border-top: 3px solid #ccc;\">"
                        + "<table width=\"100%\">\n"
                        + "                            <tr>\n"
                        + "                                <td width=\"50%\" align=\"left\"><b style=\"color: #00399b;\">Warehouse :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + whname + "</b></td>\n"
                        + "                            </tr>\n"
                        + "                        </table>";

                String whsort1 = "<table width=\"100%\">\n"
                        + "                            <tr>\n"
                        + "                                <td width=\"50%\" align=\"left\"><b style=\"color: #00399b;\">Warehouse :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + whname + "</b></td>\n"
                        + "                            </tr>\n"
                        + "                        </table>";

//                String endWH = "<hr style=\"height: 3px;\">"
//                        + "<table width=\"100%\">\n"
//                        + "                        <tr>\n"
//                        + "                            <td width=\"100%\" align=\"center\"><b style=\"color: #00399b;\">*** End of " + whname + " ***</b></td>\n"
//                        + "                        </tr>\n"
//                        + "                    </table>";
                if (oldWH.equals("")) {
                    p.setWarehouse(whsort1);
                    oldWH = result.getString("QIHWHS").trim();
                } else if (!oldWH.equals(result.getString("QIHWHS").trim())) {
                    p.setWarehouse(whsort);
                    oldWH = result.getString("QIHWHS").trim();
                }

//                p.setDesc(endWH);
                QIDETAILDao dao = new QIDETAILDao();
                List<QIDETAIL> detList = dao.findDetailDest(result.getString("QIHWHS"),
                        result.getString("QIHDEST"), result.getString("QIHMTCTRL"));

                p.setDetList(detList);

                if (!detList.isEmpty()) {
                    UAList.add(p);
                }

            }

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public List<MSSMATN> findDestForMat(String mat, String wh, String dest) {

        List<MSSMATN> UAList = new ArrayList<MSSMATN>();

        String WHS = "";
        if (!wh.trim().equals("TWC")) {
            WHS = "AND QIHWHS = '" + wh + "' ";
        }

        String sql = "SELECT distinct QIHWHS, QIHMTCTRL, QDETYPE, QIHDEST, QDEDESC "
                + "FROM QIHEAD "
                + "INNER JOIN QDEST "
                + "ON QIHEAD.QIHDEST = QDEST.QDECOD "
                + "WHERE QIHMTCTRL = '" + mat + "' "
                + "AND QIHDEST = '" + dest + "' "
                + WHS
                + "ORDER BY QDETYPE, QIHDEST ";

        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                MSSMATN p = new MSSMATN();

                p.setUid(result.getString("QIHDEST"));
                p.setName(result.getString("QDEDESC"));

                QIDETAILDao dao = new QIDETAILDao();
                List<QIDETAIL> detList = dao.findDetailDest(result.getString("QIHWHS"),
                        result.getString("QIHDEST"), result.getString("QIHMTCTRL"));

                p.setDetList(detList);

                if (!detList.isEmpty()) {
                    UAList.add(p);
                }

            }

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public List<MSSMATN> findDestForMatAll(String mat, String wh) {

        List<MSSMATN> UAList = new ArrayList<MSSMATN>();

        String WHS = "";
        if (!wh.trim().equals("TWC")) {
            WHS = "AND QIHWHS = '" + wh + "' ";
        }

        String sql = "SELECT distinct QIHWHS, QIHMTCTRL, QDETYPE, QIHDEST, QDEDESC "
                + "FROM QIHEAD "
                + "INNER JOIN QDEST "
                + "ON QIHEAD.QIHDEST = QDEST.QDECOD "
                + "WHERE QIHMTCTRL = '" + mat + "' "
                + WHS
                + "ORDER BY QDETYPE, QIHDEST ";

        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                MSSMATN p = new MSSMATN();

                p.setUid(result.getString("QIHDEST"));
                p.setName(result.getString("QDEDESC"));

                QIDETAILDao dao = new QIDETAILDao();
                List<QIDETAIL> detList = dao.findDetailDest(result.getString("QIHWHS"),
                        result.getString("QIHDEST"), result.getString("QIHMTCTRL"));

                p.setDetList(detList);

                if (!detList.isEmpty()) {
                    UAList.add(p);
                }

            }

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public List<MSSMATN> findSubMenuForMat(String mc, String wh) {

        List<MSSMATN> UAList = new ArrayList<MSSMATN>();

        String sql = "SELECT QIHWHS, QIHDEST, QDEST.QDEDESC, QDEST.QDETYPE, QDESTYP.QDEDESC as TYPEDESC "
                + "FROM QIHEAD "
                + "INNER JOIN QDEST "
                + "ON QIHEAD.QIHDEST = QDEST.QDECOD "
                + "INNER JOIN QDESTYP "
                + "ON QDEST.QDETYPE = QDESTYP.QDETYPE "
                + "WHERE QIHSTS = '2' "
                + "AND QIHWHS = '" + wh + "' "
                + "AND QIHMTCTRL = '" + mc + "' ";

        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            String oldWH = "";

            while (result.next()) {

                MSSMATN p = new MSSMATN();

                p.setWarehouse(result.getString("QIHWHS"));
                p.setUid(result.getString("QIHDEST"));
                p.setDesc(result.getString("QDEDESC"));

                if (!result.getString("TYPEDESC").equals(oldWH)) {
                    String newWHS = "<hr style=\"height: 3px;\"><b>" + result.getString("TYPEDESC") + "</b><br>";
                    p.setName(newWHS);
                    oldWH = result.getString("TYPEDESC");
                }

                UAList.add(p);

            }

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public MSSMATN findByDest(String dest, String wh) {

        MSSMATN p = new MSSMATN();

        String sql = "SELECT TOP 1 QIHWHS, QIHDEST, QDEST.QDEDESC, QDEST.QDETYPE, QDESTYP.QDEDESC as TYPEDESC "
                + "FROM QIHEAD "
                + "INNER JOIN QDEST "
                + "ON QIHEAD.QIHDEST = QDEST.QDECOD "
                + "INNER JOIN QDESTYP "
                + "ON QDEST.QDETYPE = QDESTYP.QDETYPE "
                + "WHERE QIHSTS = '2' "
                + "AND QIHWHS = '" + wh + "' "
                + "AND QIHDEST = '" + dest + "' ";

        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                p.setWarehouse(result.getString("QIHWHS"));
                p.setUid(result.getString("QIHDEST"));
                p.setDesc(result.getString("QDEDESC"));
                p.setName(result.getString("QDETYPE") + " : " + result.getString("TYPEDESC"));

            }

        } catch (Exception e) {

            e.printStackTrace();

        }

        return p;

    }

    public String findMVT(String wh, String qno) {

        String mvt = "";

        String sql = "SELECT [QIHMVT]\n"
                + "  FROM [RMShipment].[dbo].[QIHEAD]\n"
                + "  where [QIHWHS] = '" + wh + "'\n"
                + "  and [QIHQNO] = '" + qno + "'";
//        System.out.println(sql);

        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                mvt = result.getString("QIHMVT");

            }

        } catch (Exception e) {

            e.printStackTrace();

        }

        return mvt;

    }
}
