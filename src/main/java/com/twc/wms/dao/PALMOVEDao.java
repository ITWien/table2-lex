/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.dao;

import com.twc.wms.database.MSSQLDbConnectionPool;
import com.twc.wms.database.database;
import com.twc.wms.entity.PALMOVE;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 *
 * @author nutthawoot.noo
 */
public class PALMOVEDao extends database {

    public List<PALMOVE> findAll(String uid) {

        List<PALMOVE> QsetList = new ArrayList<PALMOVE>();

        String sql = "SELECT * FROM PALMOVE "
                + "WHERE USERS = '" + uid + "' AND STATUS = 'N'";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                PALMOVE p = new PALMOVE();

                p.setNo(result.getString("NO"));
                p.setId(result.getString("ID"));
                p.setCode(result.getString("CODE"));
                p.setDesc(result.getString("CODEDESC"));
                p.setFrom(result.getString("PALFROM"));
                p.setTo(result.getString("PALTO"));

                QsetList.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return QsetList;

    }

    public String check(String code) {
        PreparedStatement ps = null;
        ResultSet rs = null;
        String STRRETURN = null;
        try {
            String check = "Select * FROM [RMShipment].[dbo].[PALMOVE] where [ID] = '" + code + "' AND [STATUS] = 'N'";
            ps = connect.prepareStatement(check);
            rs = ps.executeQuery();
            if (rs.next()) {
                STRRETURN = "f";
            } else {
                STRRETURN = "t";
            }
        } catch (Exception exp) {
            exp.printStackTrace();
        }
        return STRRETURN;
    }

    public boolean add(String id, String code, String desc, String from, String uid) {

        boolean result = false;

        String sql = "INSERT INTO PALMOVE (ID, CODE, CODEDESC, PALFROM, STATUS, USERS) "
                + "VALUES('" + id + "', '" + code + "', '" + desc + "', '" + from + "', 'N', '" + uid + "') ";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean editTO(String no, String to) {

        boolean result = false;

        String sql = "UPDATE PALMOVE SET PALTO = '" + to + "' "
                + "WHERE NO = '" + no + "' ";

        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean edit(String no, int mno, String uid) {

        boolean result = false;

        String sql = "UPDATE PALMOVE SET STATUS = 'Y', MOVENO = '" + mno + "', MOVEDT = CURRENT_TIMESTAMP, USERS = '" + uid + "' "
                + "WHERE NO = '" + no + "' ";

        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public String MaxMNO() {

        String max = "0";

        String sql = "SELECT MAX(MOVENO) as MAXNO\n"
                + "FROM PALMOVE";

        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                if (result.getString("MAXNO") != null) {
                    max = result.getString("MAXNO");
                }

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return max;

    }

    public void delete(String cod) {

        String sql = "DELETE FROM PALMOVE "
                + "WHERE NO = ? ";

        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            ps.setString(1, cod);

            ps.executeUpdate();

        } catch (Exception e) {

            e.printStackTrace();

        }

    }
}
