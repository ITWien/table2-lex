/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.dao;

import com.twc.wms.database.MSSQLDbConnectionPool;
import com.twc.wms.database.database;
import java.sql.*;

import java.util.ArrayList;

import java.util.List;

import com.twc.wms.entity.PGMU;

import java.util.Calendar;

/**
 *
 * @author nutthawoot.noo
 */
public class PGMUDao extends database {

    public List<PGMU> findAll(String uid) {

        List<PGMU> UAList = new ArrayList<PGMU>();

        String sql = "SELECT * FROM QPGMNU "
                + "FULL OUTER JOIN QPGMAS ON QPGMNU.QPGPGID=QPGMAS.QPGID "
                + "WHERE QPGUSERID = ?";

        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            ps.setString(1, uid);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                PGMU p = new PGMU();

                p.setUid(result.getString("QPGPGID"));

                p.setName(result.getString("QPGNAME"));

                UAList.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public String check(String uid, String pid) {
        PreparedStatement ps = null;
        ResultSet rs = null;
        String STRRETURN = null;
        try {
            String check = "Select * FROM [RMShipment].[dbo].[QPGMNU] "
                    + "where [QPGUSERID] = '" + uid + "' AND [QPGPGID] = '" + pid + "'";
            ps = connect.prepareStatement(check);
            rs = ps.executeQuery();
            if (rs.next()) {
                STRRETURN = "f";
            } else {
                STRRETURN = "t";
            }
            connect.close();
        } catch (Exception exp) {
            exp.printStackTrace();
        }
        return STRRETURN;
    }

    public boolean add(PGMU ua, String uid) {

        boolean result = false;

        String sql = "INSERT INTO QPGMNU"
                + " (QPGUSERID, QPGPGID, QPGEDT, QPGCDT, QPGUSER)"
                + " VALUES(?, ?, ?, ?, ?)";

        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            ps.setString(1, ua.getUid());

            ps.setString(2, ua.getName());

            Calendar cal = Calendar.getInstance();
            Timestamp timestamp = new Timestamp(cal.getTimeInMillis());

            ps.setTimestamp(3, timestamp);

            ps.setTimestamp(4, timestamp);

            ps.setString(5, uid);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public void delete(String uid, String pid) {

        String sql = "DELETE FROM QPGMNU "
                + "WHERE QPGUSERID = ? AND QPGPGID = ?";

        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            ps.setString(1, uid);
            ps.setString(2, pid);

            ps.executeUpdate();

        } catch (Exception e) {

            e.printStackTrace();

        }

    }
}
