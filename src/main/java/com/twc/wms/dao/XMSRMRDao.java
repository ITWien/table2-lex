/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.dao;

import com.twc.wms.database.database;

import java.sql.*;

import java.util.ArrayList;

import java.util.List;

import com.twc.wms.entity.XMSRMR;

/**
 *
 * @author nutthawoot.noo
 */
public class XMSRMRDao extends database {

    public List<XMSRMR> findByUid(String uid) {

        List<XMSRMR> UAList = new ArrayList<XMSRMR>();

        String sql = "SELECT [XMSRMRMAT]\n"
                + "      ,[XMSRMRDESC]\n"
                + "      ,[XMSRMRQTY]\n"
                + "      ,[XMSRMRPACK]\n"
                + "      ,[XMSRMRPLANT]\n"
                + "      ,[XMSRMRQRID]\n"
                + "      ,[XMSRMRUSER]+' : '+USERS AS [XMSRMRUSER]\n"
                + "  FROM [RMShipment].[dbo].[XMSRMR]\n"
                + "  LEFT JOIN MSSUSER ON MSSUSER.USERID = XMSRMR.XMSRMRUSER\n"
                + "  WHERE [XMSRMRUSER] = '" + uid + "'";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                XMSRMR p = new XMSRMR();

                p.setXMSRMRMAT(result.getString("XMSRMRMAT"));
                p.setXMSRMRDESC(result.getString("XMSRMRDESC"));
                p.setXMSRMRQTY(result.getString("XMSRMRQTY"));
                p.setXMSRMRPACK(result.getString("XMSRMRPACK"));
                p.setXMSRMRPLANT(result.getString("XMSRMRPLANT"));
                p.setXMSRMRQRID(result.getString("XMSRMRQRID"));
                p.setXMSRMRUSER(result.getString("XMSRMRUSER"));

                UAList.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public List<String> findQRIDByGPQR(String qr) {

        List<String> UAList = new ArrayList<String>();

        String sql = "SELECT DISTINCT [QIDID]\n"
                + "  FROM [RMShipment].[dbo].[QIDETAIL]\n"
                + "  where [QIDGPQR] = '" + qr + "'";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                UAList.add(result.getString("QIDID"));

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public boolean add(String qr, String uid) {

        boolean result = false;

        String sql = "INSERT INTO [XMSRMR]\n"
                + "SELECT 'TWC'\n"
                + "      ,[QRMCODE]\n"
                + "      ,[QRMDESC]\n"
                + "      ,[QRMQTY]\n"
                + "      ,[QRMPACKTYPE]\n"
                + "	  ,[QRMPLANT]\n"
                + "	  ,[QRMID]\n"
                + "	  ,CURRENT_TIMESTAMP\n"
                + "	  ,CURRENT_TIMESTAMP\n"
                + "      ,'" + uid + "'\n"
                + "  FROM [RMShipment].[dbo].[QRMMAS]\n"
                + "  where QRMID = '" + qr + "'";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean addTRA(String qr, String uid) {

        boolean result = false;

        String sql = "INSERT INTO [QRMTRA] ([QRMCOM]\n"
                + "      ,[QRMWHSE]\n"
                + "      ,[QRMCODE]\n"
                + "      ,[QRMVAL]\n"
                + "      ,[QRMPLANT]\n"
                + "      ,[QRMSTORAGE]\n"
                + "      ,[QRMID]\n"
                + "      ,[QRMROLL]\n"
                + "      ,[QRMTRDT]\n"
                + "      ,[QRMTRTM]\n"
                + "      ,[QRMMVT]\n"
                + "      ,[QRMTRT]\n"
                + "      ,[QRMQTY]\n"
                + "      ,[QRMBUN]\n"
                + "      ,[QRMCDT]\n"
                + "      ,[QRMUSER])\n"
                + "SELECT [QRMCOM]\n"
                + "      ,[QRMWHSE]\n"
                + "      ,[QRMCODE]\n"
                + "      ,[QRMVAL]\n"
                + "      ,[QRMPLANT]\n"
                + "      ,[QRMSTORAGE]\n"
                + "      ,[QRMID]\n"
                + "      ,[QRMROLL]\n"
                + "      ,CURRENT_TIMESTAMP\n"
                + "      ,CURRENT_TIMESTAMP\n"
                + "      ,'202'\n"
                + "      ,'202'\n"
                + "      ,[QRMQTY]\n"
                + "      ,[QRMBUN]\n"
                + "      ,CURRENT_TIMESTAMP\n"
                + "      ,'" + uid + "'\n"
                //                + "      ,'LEX'\n"
                + "FROM [RMShipment].[dbo].[QRMMAS]\n"
                + "WHERE QRMID IN ('" + qr + "')";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean edit(String qrid, String qty, String pack, String plant) {

        boolean result = false;

        String sql = "UPDATE QRMMAS \n"
                + "SET QRMQTY = '" + qty + "' \n"
                + "    ,QRMALQTY = '" + qty + "' \n"
                + "    ,QRMPACKTYPE = '" + pack + "' \n"
                + "    ,QRMPLANT = '" + plant + "' \n"
                + "    ,QRMSTS = '2' \n"
                + "    ,QRMEDT = CURRENT_TIMESTAMP \n"
                + "WHERE QRMID = '" + qrid + "' \n";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean delete(String qr, String uid) {

        boolean result = false;

        String WHERE = "";

        if (qr.equals("all")) {
            WHERE = "WHERE [XMSRMRUSER] = '" + uid + "'\n";
        } else {
            WHERE = "WHERE [XMSRMRQRID] = '" + qr + "'\n"
                    + "AND [XMSRMRUSER] = '" + uid + "'\n";
        }

        String sql = "DELETE [XMSRMR]\n" + WHERE;
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }
}
