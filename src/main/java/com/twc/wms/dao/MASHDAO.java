/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.dao;

import com.twc.wms.database.database;
import com.twc.wms.entity.MPDATAH;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author nutthawoot.noo
 */
public class MASHDAO extends database {

    public void add(MPDATAH MPDATAH) {

        try {
            PreparedStatement statement = connect.prepareStatement("insert into MSSMASH(LINETYP, VBELN, AUDAT, AUART, VTWEG, BWART, KUNNR, NAME1, NAME2, ADDR1, ADDR2, BSTKD, SUBMI, MATLOT, TAXNO, BRANCH01, DATUM, STYLE, COLOR, LOT, AMTFG, POFG, GRPNO) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
            statement.setString(1, MPDATAH.getLINETYP());
            statement.setString(2, MPDATAH.getVBELN());
            statement.setDate(3, new java.sql.Date(MPDATAH.getAUDAT().getTime()));
            statement.setString(4, MPDATAH.getAUART());
            statement.setString(5, MPDATAH.getVTWEG());
            statement.setString(6, MPDATAH.getBWART());
            statement.setString(7, MPDATAH.getKUNNR());
            statement.setString(8, MPDATAH.getNAME1());
            statement.setString(9, MPDATAH.getNAME2());
            statement.setString(10, MPDATAH.getADDR1());
            statement.setString(11, MPDATAH.getADDR2());
            statement.setString(12, MPDATAH.getBSTKD());
            statement.setString(13, MPDATAH.getSUBMI());
            statement.setString(14, MPDATAH.getMATLOT());
            statement.setString(15, MPDATAH.getTAXNO());
            statement.setString(16, MPDATAH.getBRANCH01());
            statement.setString(17, MPDATAH.getDATUMStr());
            statement.setString(18, MPDATAH.getSTYLE());
            statement.setString(19, MPDATAH.getCOLOR());
            statement.setString(20, MPDATAH.getLOT());
            statement.setString(21, MPDATAH.getAMTFG());
            statement.setString(22, MPDATAH.getPOFG());
            statement.setString(23, MPDATAH.getGRPNO());
            statement.executeUpdate();
        } catch (SQLException err) {
            err.printStackTrace();
        }

    }

    public boolean checkDuplicate(MPDATAH MPDATAH) {

        boolean status = false;

        try {
            PreparedStatement statement = connect.prepareStatement("select * from MSSMASH where VBELN=? and POFG=?");
            statement.setString(1, MPDATAH.getVBELN());
            statement.setString(2, MPDATAH.getPOFG());
            ResultSet set = statement.executeQuery();

            if (set.next()) {
                status = true;
            }

        } catch (SQLException err) {
            status = true;
            err.printStackTrace();
        }

        return status;

    }

    public String getMaxGroup(String POFG) {

        String maxGroup = "";

        try {

            PreparedStatement statement = connect.prepareStatement("select case when GRPNO != '' then MAX(GRPNO) else 'Data not found' end as GRPNO from MSSMASH where POFG=? group by GRPNO");
            statement.setString(1, POFG);
            ResultSet set = statement.executeQuery();

            while (set.next()) {
                maxGroup = set.getString("GRPNO");
            }

        } catch (SQLException err) {
            err.printStackTrace();
        }

        return maxGroup;

    }

    public List<MPDATAH> getEmptyGroup(String POFG) {

        List<MPDATAH> listMPDATAH = new ArrayList<MPDATAH>();

        try {

            PreparedStatement statement = connect.prepareStatement(" SELECT ROW_NUMBER() OVER(order by RowNum) AS LINETYP,a.VBELN,a.AUDAT,a.BSTKD,a.POFG,substring(MATNR,1,8) as MATNR from ( SELECT  ROW_NUMBER()  OVER( PARTITION BY substring(b.MATNR,1,8) ORDER BY substring(b.MATNR,1,8)) AS RowNum , a.VBELN,a.AUDAT,a.BSTKD,a.POFG,substring(b.MATNR,1,8) as MATNR FROM MSSMASH a INNER JOIN MSSMASD b ON  a.VBELN=b.VBELN WHERE POFG=? and GRPNO='' )  a WHERE  RowNum=1 order by  a.VBELN ");
            statement.setString(1, POFG);
            ResultSet set = statement.executeQuery();

            while (set.next()) {
                MPDATAH MPDATAH = new MPDATAH();

                MPDATAH.setLINETYP(set.getString("LINETYP"));
                MPDATAH.setVBELN(set.getString("VBELN"));
                MPDATAH.setAUDAT(set.getDate("AUDAT"));
                MPDATAH.setBSTKD(set.getString("BSTKD"));
                MPDATAH.setSTYLE(set.getString("MATNR"));
                MPDATAH.setPOFG(set.getString("POFG"));
                listMPDATAH.add(MPDATAH);
            }

        } catch (SQLException err) {
            err.printStackTrace();
        }

        return listMPDATAH;

    }

    public void updateGroupSet(String GRPNO, String WHSE, String POFG) {

        try {
            PreparedStatement statement = connect.prepareStatement("update MSSMASH set GRPNO=?,MWHSE=? where POFG=? and GRPNO=''");
            statement.setString(1, GRPNO);
            statement.setString(2, WHSE);
            statement.setString(3, POFG);
            statement.executeUpdate();
            statement.close();

        } catch (SQLException err) {
            err.printStackTrace();
        }

    }

    public void DelOrno(String orno) {

        //System.out.println(orno);    //new mod
        try {
            PreparedStatement statement = connect.prepareStatement(" DELETE FROM MSSMASH  WHERE VBELN=? ");
            statement.setString(1, orno);
            //  statement.setString(2, WHSE);
            //  statement.setString(3, POFG);
            statement.executeUpdate();
            statement.close();

            PreparedStatement statement2 = connect.prepareStatement(" DELETE FROM MSSMASD  WHERE VBELN=? ");
            statement2.setString(1, orno);
            //  statement.setString(2, WHSE);
            //  statement.setString(3, POFG);
            statement2.executeUpdate();
            statement2.close();

        } catch (SQLException err) {
            err.printStackTrace();
        }

    }

    public String getCustomerName(String POFG) {

        String name = "";

        try {

            PreparedStatement statement = connect.prepareStatement("select [ISHCUNM1] + ' ' + [ISHCUNM2] as NAME from [ISMMASH] where [ISHPOFG] = ?");
            statement.setString(1, POFG);
            ResultSet set = statement.executeQuery();

            if (set.next()) {
                name = set.getString("NAME");
            }

        } catch (SQLException err) {
            err.printStackTrace();
        }

        return name;

    }

    public String[] getDropDownGroup(String POFG) {

        int count = 0;
        String[] group = null;

        try {

            PreparedStatement statement = connect.prepareStatement("select distinct [ISHJBNO]\n"
                    + "from [ISMMASH] \n"
                    + "where [ISHPOFG] = ? \n"
                    + "and [ISHJBNO] is not null \n"
                    + "order by [ISHJBNO] desc", ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            statement.setString(1, POFG);
            ResultSet set = statement.executeQuery();

            if (set.next()) {
                set.last();
                group = new String[set.getRow()];
                set.beforeFirst();
                while (set.next()) {
                    group[count] = set.getString("ISHJBNO");
                    count++;
                }

            }

        } catch (SQLException err) {
            err.printStackTrace();
        }

        return group;

    }

    public String[] getDropDownSaleNo(String POFG, String GRPNO, String ISSUENO1, String ISSUENO2, String VBELN) {

        int count = 0;
        String[] group = null;

        try {

            PreparedStatement statement = connect.prepareStatement("SELECT DISTINCT MASH.VBELN AS VBELN FROM MSSMASH MASH FULL JOIN MSSMASD MASD ON MASH.VBELN = MASD.VBELN FULL JOIN MSSSEQN SEQN ON MASD.ISSUENO = SEQN.ISSUENO WHERE MASH.POFG = ? AND MASH.GRPNO = ? AND (SEQN.ISSUENO BETWEEN ? AND ?) AND MASH.VBELN >= ?", ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            statement.setString(1, POFG);
            statement.setString(2, GRPNO);
            statement.setString(3, ISSUENO1);
            statement.setString(4, ISSUENO2);
            statement.setString(5, VBELN);
            ResultSet set = statement.executeQuery();

            if (set.next()) {
                set.last();
                group = new String[set.getRow()];
                set.beforeFirst();
                while (set.next()) {
                    group[count] = set.getString("VBELN");
                    count++;
                }

            }

        } catch (SQLException err) {
            err.printStackTrace();
        }
        return group;
    }

}
