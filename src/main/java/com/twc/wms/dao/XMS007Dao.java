/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.dao;

import com.twc.wms.database.database;
import java.sql.*;

import java.util.ArrayList;

import java.util.List;

import com.twc.wms.entity.QRMMAS;

/**
 *
 * @author nutthawoot.noo
 */
public class XMS007Dao extends database {

    public boolean insertParentQRMTRA(String id, String cdt, String remainQty, String parentPackType, String uid) {

        boolean result = false;

        String sql = "INSERT INTO QRMTRA \n"
                + "([QRMCOM]\n"
                + "      ,[QRMWHSE]\n"
                + "      ,[QRMCODE]\n"
                + "      ,[QRMVAL]\n"
                + "      ,[QRMPLANT]\n"
                + "      ,[QRMSTORAGE]\n"
                + "      ,[QRMID]\n"
                + "      ,[QRMROLL]\n"
                + "      ,[QRMTRDT]\n"
                + "      ,[QRMTRTM]\n"
                + "      ,[QRMMVT]\n"
                + "      ,[QRMTRT]\n"
                + "      ,[QRMDOCNO]\n"
                + "      ,[QRMSAPNO]\n"
                + "      ,[QRMQTY]\n"
                + "      ,[QRMBUN]\n"
                + "      ,[QRMCDT]\n"
                + "      ,[QRMUSER])\n"
                + "SELECT [QRMCOM]\n"
                + "      ,[QRMWHSE]\n"
                + "      ,[QRMCODE]\n"
                + "      ,[QRMVAL]\n"
                + "      ,[QRMPLANT]\n"
                + "      ,[QRMSTORAGE]\n"
                + "      ,[QRMID]\n"
                + "      ,[QRMROLL]\n"
                + "	  ,format(current_timestamp,'yyyy-MM-dd')\n"
                + "	  ,format(current_timestamp,'HH:mm:ss.fff')\n"
                + "	  ,'902'\n"
                + "	  ,'SPOT'\n"
                + "	  ,null\n"
                + "	  ,null\n"
                + "      ," + remainQty + " - [QRMQTY]\n"
                + "      ,[QRMBUN]\n"
                + "	  ,current_timestamp\n"
                + "      ,'" + uid + "'\n"
                + "  FROM [RMShipment].[dbo].[QRMMAS]\n"
                + "  where qrmid='" + id + "'\n"
                + "  and [QRMCDT]='" + cdt + "'";

        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean updateParentQRMMAS(String id, String cdt, String remainQty, String parentPackType, String uid) {

        boolean result = false;

        String sql = "UPDATE [RMShipment].[dbo].[QRMMAS]\n"
                + "SET QRMQTY = '" + remainQty + "'\n"
                + "   ,QRMALQTY = '" + remainQty + "'\n"
                + "   ,QRMPACKTYPE = '" + parentPackType + "'\n"
                + "   ,QRMEDT = CURRENT_TIMESTAMP\n"
                + "   ,QRMUSER = '" + uid + "'\n"
                + "  WHERE QRMID = '" + id + "'\n"
                + "  AND QRMCDT = '" + cdt + "'";

        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean insertQRMTRA(String id, String cdt, String qrmroll, String qrmqty, String qrmpacktype, String qrmid, String uid) {

        boolean result = false;

        String sql = "INSERT INTO QRMTRA \n"
                + "([QRMCOM]\n"
                + "      ,[QRMWHSE]\n"
                + "      ,[QRMCODE]\n"
                + "      ,[QRMVAL]\n"
                + "      ,[QRMPLANT]\n"
                + "      ,[QRMSTORAGE]\n"
                + "      ,[QRMID]\n"
                + "      ,[QRMROLL]\n"
                + "      ,[QRMTRDT]\n"
                + "      ,[QRMTRTM]\n"
                + "      ,[QRMMVT]\n"
                + "      ,[QRMTRT]\n"
                + "      ,[QRMDOCNO]\n"
                + "      ,[QRMSAPNO]\n"
                + "      ,[QRMQTY]\n"
                + "      ,[QRMBUN]\n"
                + "      ,[QRMCDT]\n"
                + "      ,[QRMUSER])\n"
                + "SELECT [QRMCOM]\n"
                + "      ,[QRMWHSE]\n"
                + "      ,[QRMCODE]\n"
                + "      ,[QRMVAL]\n"
                + "      ,[QRMPLANT]\n"
                + "      ,[QRMSTORAGE]\n"
                + "      ,'" + qrmid + "'\n"
                + "      ,'" + qrmroll + "'\n"
                + "	  ,format(current_timestamp,'yyyy-MM-dd')\n"
                + "	  ,format(current_timestamp,'HH:mm:ss.fff')\n"
                + "	  ,'901'\n"
                + "	  ,'SPIN'\n"
                + "	  ,null\n"
                + "	  ,null\n"
                + "      ,'" + qrmqty + "'\n"
                + "      ,[QRMBUN]\n"
                + "	  ,current_timestamp\n"
                + "      ,'" + uid + "'\n"
                + "  FROM [RMShipment].[dbo].[QRMMAS]\n"
                + "  where qrmid='" + id + "'\n"
                + "  and [QRMCDT]='" + cdt + "'";

        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean insertQRMMAS(String id, String cdt, String qrmroll, String qrmqty, String qrmpacktype, String qrmid, String uid) {

        boolean result = false;

        String sql = "INSERT INTO QRMMAS \n"
                + "([QRMCOM]\n"
                + "      ,[QRMID]\n"
                + "      ,[QRMCODE]\n"
                + "      ,[QRMVAL]\n"
                + "      ,[QRMROLL]\n"
                + "      ,[QRMIDTEMP]\n"
                + "      ,[QRMDESC]\n"
                + "      ,[QRMPO]\n"
                + "      ,[QRMTAX]\n"
                + "      ,[QRMPACKTYPE]\n"
                + "      ,[QRMQTY]\n"
                + "      ,[QRMALQTY]\n"
                + "      ,[QRMBUN]\n"
                + "      ,[QRMAUN]\n"
                + "      ,[QRMCVFAC]\n"
                + "      ,[QRMCVFQTY]\n"
                + "      ,[QRMSTS]\n"
                + "      ,[QRMPARENT]\n"
                + "      ,[QRMPDDATE]\n"
                + "      ,[QRMISDATE]\n"
                + "      ,[QRMRCDATE]\n"
                + "      ,[QRMTFDATE]\n"
                + "      ,[QRMLOTNO]\n"
                + "      ,[QRMDYNO]\n"
                + "      ,[QRMQIS]\n"
                + "      ,[QRMWHSE]\n"
                + "      ,[QRMZONE]\n"
                + "      ,[QRMPALLET]\n"
                + "      ,[QRMRACKNO]\n"
                + "      ,[QRMSIDE]\n"
                + "      ,[QRMROW]\n"
                + "      ,[QRMCOL]\n"
                + "      ,[QRMLOC]\n"
                + "      ,[QRMPLANT]\n"
                + "      ,[QRMSTORAGE]\n"
                + "      ,[QRMBARCODE]\n"
                + "      ,[QRMSUPCODE]\n"
                + "      ,[QRMEDT]\n"
                + "      ,[QRMCDT]\n"
                + "      ,[QRMUSER])\n"
                + "SELECT [QRMCOM]\n"
                + "      ,'" + qrmid + "'\n"
                + "      ,[QRMCODE]\n"
                + "      ,[QRMVAL]\n"
                + "      ,'" + qrmroll + "'\n"
                + "      ,[QRMIDTEMP]\n"
                + "      ,[QRMDESC]\n"
                + "      ,[QRMPO]\n"
                + "      ,[QRMTAX]\n"
                + "      ,'" + qrmpacktype + "'\n"
                + "      ,'" + qrmqty + "'\n"
                + "      ,'" + qrmqty + "'\n"
                + "      ,[QRMBUN]\n"
                + "      ,[QRMAUN]\n"
                + "      ,[QRMCVFAC]\n"
                + "      ,[QRMCVFQTY]\n"
                + "      ,[QRMSTS]\n"
                + "      ,'" + id + "'\n"
                + "      ,[QRMPDDATE]\n"
                + "      ,[QRMISDATE]\n"
                + "      ,[QRMRCDATE]\n"
                + "      ,[QRMTFDATE]\n"
                + "      ,[QRMLOTNO]\n"
                + "      ,[QRMDYNO]\n"
                + "      ,[QRMQIS]\n"
                + "      ,[QRMWHSE]\n"
                + "      ,[QRMZONE]\n"
                + "      ,[QRMPALLET]\n"
                + "      ,[QRMRACKNO]\n"
                + "      ,[QRMSIDE]\n"
                + "      ,[QRMROW]\n"
                + "      ,[QRMCOL]\n"
                + "      ,[QRMLOC]\n"
                + "      ,[QRMPLANT]\n"
                + "      ,[QRMSTORAGE]\n"
                + "      ,[QRMBARCODE]\n"
                + "      ,[QRMSUPCODE]\n"
                + "      ,current_timestamp\n"
                + "      ,current_timestamp\n"
                + "      ,'" + uid + "'\n"
                + "  FROM [RMShipment].[dbo].[QRMMAS]\n"
                + "  where qrmid='" + id + "'\n"
                + "  and [QRMCDT]='" + cdt + "'";

        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean updateGenRoll(String code, String len) {

        boolean result = false;

        String sql = "update [RMShipment].[dbo].[QGENRO]\n"
                + "  set [QGENDATE] = format(current_timestamp,'yyyy-MM-dd')\n"
                + "     ,[QGENROLL] = [QGENROLL] + " + len + "\n"
                + "  WHERE [QGENCODE] = '" + code + "'";

        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public String getGenRoll(String code) {

        String sql = "SELECT [QGENDATE]\n"
                + "      ,[QGENCODE]\n"
                + "      ,[QGENROLL]+1 AS QGENROLL\n"
                + "  FROM [RMShipment].[dbo].[QGENRO]\n"
                + "  WHERE [QGENCODE] = '" + code + "'";
//        System.out.println(sql);

        String last = "0";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                last = result.getString("QGENROLL");

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return last;

    }

    public String getDate() {

        String sql = "SELECT FORMAT(CURRENT_TIMESTAMP,'yyMMdd') AS [DATE]";
//        System.out.println(sql);

        String date = "";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                date = result.getString("DATE");

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return date;

    }

    public String getGenQr(String id) {

        String sql = "SELECT [QGENDATE]\n"
                + "      ,[QGENSAPPGRP]\n"
                + "      ,cast([QGENID] as int)+1 as [QGENID]\n"
                + "  FROM [RMShipment].[dbo].[QGENQR]\n"
                + "  where [QGENSAPPGRP] = RIGHT(LEFT('" + id + "',6),2)\n"
                + "  and QGENDATE = FORMAT(CURRENT_TIMESTAMP,'yyyy-MM-dd')";
//        System.out.println(sql);

        String last = "0";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                last = result.getString("QGENID");

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return last;

    }

    public boolean updateGenQr(String id, String len) {

        boolean result = false;

        String sql = "update [RMShipment].[dbo].[QGENQR]\n"
                + "set [QGENID] = format(cast([QGENID] as int)+" + len + ",'00000')\n"
                + "  where [QGENSAPPGRP] = RIGHT(LEFT('" + id + "',6),2)\n"
                + "  and QGENDATE = FORMAT(CURRENT_TIMESTAMP,'yyyy-MM-dd')";

        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean insertGenQr(String id) {

        boolean result = false;

        String sql = "INSERT INTO [QGENQR]\n"
                + "      ([QGENDATE]\n"
                + "      ,[QGENSAPPGRP]\n"
                + "      ,[QGENID])\n"
                + "  VALUES (FORMAT(CURRENT_TIMESTAMP,'yyyy-MM-dd')\n"
                + "         ,RIGHT(LEFT('" + id + "',6),2)\n"
                + "	    ,'00000')";

        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean checkLastQr(String id) {

        boolean check = false;

        String sql = "SELECT TOP 1 [QGENDATE]\n"
                + "      ,[QGENSAPPGRP]\n"
                + "      ,[QGENID]\n"
                + "  FROM [RMShipment].[dbo].[QGENQR]\n"
                + "  WHERE [QGENDATE] = FORMAT(CURRENT_TIMESTAMP,'yyyy-MM-dd')\n"
                + "  AND [QGENSAPPGRP] = RIGHT(LEFT('" + id + "',6),2)";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            if (result.next()) {

                check = true;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return check;

    }

    public QRMMAS findMaster(String idcode, String type) {

        QRMMAS p = new QRMMAS();

        String sql = "SELECT [QRMID]\n"
                + "      ,[QRMCODE]\n"
                + "      ,[QRMVAL]\n"
                + "      ,[QRMROLL]\n"
                + "      ,[QRMIDTEMP]\n"
                + "      ,[QRMDESC]\n"
                + "      ,[QRMPO]\n"
                + "      ,[QRMTAX]\n"
                + "      ,[QRMPACKTYPE]\n"
                + "      ,format(QRMQTY,'#0.000') as [QRMQTY]\n"
                + "      ,format(QRMALQTY,'#0.000') as [QRMALQTY]\n"
                + "      ,[QRMBUN]\n"
                + "      ,[QRMAUN]\n"
                + "      ,format(QRMCVFAC,'#0.000') as [QRMCVFAC]\n"
                + "      ,format(QRMCVFQTY,'#0.000') as [QRMCVFQTY]\n"
                + "      ,[QRMSTS]\n"
                + "      ,[QRMPARENT]\n"
                + "      ,[QRMPDDATE]\n"
                + "      ,[QRMISDATE]\n"
                + "      ,[QRMRCDATE]\n"
                + "      ,[QRMTFDATE]\n"
                + "      ,[QRMLOTNO]\n"
                + "      ,[QRMDYNO]\n"
                + "      ,[QRMQIS]\n"
                + "      ,[QRMWHSE]\n"
                + "      ,[QRMZONE]\n"
                + "      ,[QRMPALLET]\n"
                + "      ,[QRMRACKNO]\n"
                + "      ,[QRMSIDE]\n"
                + "      ,[QRMROW]\n"
                + "      ,[QRMCOL]\n"
                + "      ,[QRMLOC]\n"
                + "      ,[QRMPLANT]\n"
                + "      ,[QRMSTORAGE]\n"
                + "      ,[QRMBARCODE]\n"
                + "      ,[QRMSUPCODE]\n"
                + "      ,[QRMEDT]\n"
                + "      ,[QRMCDT]\n"
                + "      ,[QRMUSER]\n"
                + "      ,(SELECT TOP 1 [SAPMCTRL]+' : '+[SAPMCTRLDESC] FROM [SAPMAS] WHERE [SAPMAT] = [QRMCODE]) AS [MATCTRL]\n"
                + "FROM QRMMAS \n"
                + "WHERE " + type + " = '" + idcode + "' \n";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                p.setQRMID(result.getString("QRMID"));
                p.setQRMCODE(result.getString("QRMCODE"));
                p.setQRMVAL(result.getString("QRMVAL"));
                p.setQRMROLL(result.getString("QRMROLL"));
                p.setQRMIDTEMP(result.getString("QRMIDTEMP"));
                p.setQRMDESC(result.getString("QRMDESC"));
                p.setQRMPO(result.getString("QRMPO"));
                p.setQRMTAX(result.getString("QRMTAX"));
                p.setQRMPACKTYPE(result.getString("QRMPACKTYPE"));
                p.setQRMQTY(result.getString("QRMQTY"));
                p.setQRMALQTY(result.getString("QRMALQTY"));
                p.setQRMBUN(result.getString("QRMBUN"));
                p.setQRMAUN(result.getString("QRMAUN"));
                p.setQRMCVFAC(result.getString("QRMCVFAC"));
                p.setQRMCVFQTY(result.getString("QRMCVFQTY"));
                p.setQRMSTS(result.getString("QRMSTS"));
                p.setQRMPARENT(result.getString("QRMPARENT"));
                p.setQRMPDDATE(result.getString("QRMPDDATE"));
                p.setQRMISDATE(result.getString("QRMISDATE"));
                p.setQRMRCDATE(result.getString("QRMRCDATE"));
                p.setQRMTFDATE(result.getString("QRMTFDATE"));
                p.setQRMLOTNO(result.getString("QRMLOTNO"));
                p.setQRMDYNO(result.getString("QRMDYNO"));
                p.setQRMQIS(result.getString("QRMQIS"));
                p.setQRMWHSE(result.getString("QRMWHSE"));
                p.setQRMZONE(result.getString("QRMZONE"));
                p.setQRMPALLET(result.getString("QRMPALLET"));
                p.setQRMRACKNO(result.getString("QRMRACKNO"));
                p.setQRMSIDE(result.getString("QRMSIDE"));
                p.setQRMROW(result.getString("QRMROW"));
                p.setQRMCOL(result.getString("QRMCOL"));
                p.setQRMLOC(result.getString("QRMLOC"));
                p.setQRMPLANT(result.getString("QRMPLANT"));
                p.setQRMSTORAGE(result.getString("QRMSTORAGE"));
                p.setQRMBARCODE(result.getString("QRMBARCODE"));
                p.setQRMSUPCODE(result.getString("QRMSUPCODE"));
                p.setQRMEDT(result.getString("QRMEDT"));
                p.setQRMCDT(result.getString("QRMCDT"));
                p.setQRMUSER(result.getString("QRMUSER"));
                p.setMATCTRL(result.getString("MATCTRL"));

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return p;

    }

}
