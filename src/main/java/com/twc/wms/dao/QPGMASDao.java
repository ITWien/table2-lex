/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.dao;

import com.twc.wms.database.database;

import java.sql.*;

import java.util.ArrayList;

import java.util.List;

import com.twc.wms.entity.QPGMAS;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author nutthawoot.noo
 */
public class QPGMASDao extends database {

    public List<QPGMAS> findAll() {

        List<QPGMAS> UAList = new ArrayList<QPGMAS>();

        String sql = "SELECT * FROM QPGMAS";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                QPGMAS p = new QPGMAS();

                p.setUid(result.getString("QPGID"));

                p.setName(result.getString("QPGNAME"));

                UAList.add(p);

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }
}
