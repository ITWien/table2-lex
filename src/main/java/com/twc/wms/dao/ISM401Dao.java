/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.twc.wms.dao;

import com.twc.wms.database.database;
import com.twc.wms.entity.ISMMASH;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author 93176
 */
public class ISM401Dao extends database {

    public List<ISMMASH> CustomerData() {

        List<ISMMASH> UAList = new ArrayList<ISMMASH>();

        String sql = "SELECT DISTINCT [ISHCUNO],[ISHCUNM1] FROM [RMShipment].[dbo].[ISMMASH]";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);
            ResultSet result = ps.executeQuery();

            while (result.next()) {

                ISMMASH p = new ISMMASH();
                p.setISHCUNO(result.getString("ISHCUNO"));
                p.setISHCUNM1(result.getString("ISHCUNM1"));

                UAList.add(p);

            }
            ps.close();
            connect.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return UAList;

    }

    public List<ISMMASH> SalesOrderDataByCus(String CusNo) {

        List<ISMMASH> UAList = new ArrayList<ISMMASH>();

        String sql = "SELECT DISTINCT ISHORD FROM [RMShipment].[dbo].[ISMMASH] WHERE ISHCUNO LIKE '%" + CusNo + "%'";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);
            ResultSet result = ps.executeQuery();

            while (result.next()) {

                ISMMASH p = new ISMMASH();
                p.setISHORD(result.getString("ISHORD"));

                UAList.add(p);

            }
            ps.close();
            connect.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return UAList;

    }

    public List<ISMMASH> SeqDataByCusnSales(String CusNo, String salesfrom, String salesto) {

        List<ISMMASH> UAList = new ArrayList<ISMMASH>();

        String sql = "SELECT DISTINCT ISHSEQ FROM [RMShipment].[dbo].[ISMMASH] WHERE ISHCUNO LIKE '%" + CusNo.trim() + "%' AND ISHORD BETWEEN '" + salesfrom.trim() + "' AND '" + salesto.trim() + "' ";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);
            ResultSet result = ps.executeQuery();

            while (result.next()) {

                ISMMASH p = new ISMMASH();
                p.setISHSEQ(result.getString("ISHSEQ"));

                UAList.add(p);

            }
            ps.close();
            connect.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return UAList;

    }

}
