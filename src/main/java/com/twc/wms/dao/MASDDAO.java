/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.dao;

import com.twc.wms.database.database;
import com.twc.wms.entity.MPDATAD;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author nutthawoot.noo
 */
public class MASDDAO extends database {

    public void add(MPDATAD MPDATAD) {

        try {
            PreparedStatement statement = connect.prepareStatement("insert into MSSMASD(LINETYP, VBELN, POSNR, MATNR, WERKS, BWTAR, LGORT, KWMENG, ISSUENO) values (?, ?, ?, ?, ?, ?, ?, ?, ?)");
            statement.setString(1, MPDATAD.getLINETYP());
            statement.setString(2, MPDATAD.getVBELN());
            statement.setString(3, MPDATAD.getPOSNR());
            statement.setString(4, MPDATAD.getMATNR());
            statement.setString(5, MPDATAD.getWERKS());
            statement.setString(6, MPDATAD.getBWTAR());
            statement.setString(7, MPDATAD.getLGORT());
            statement.setFloat(8, MPDATAD.getKWMENG());
            statement.setInt(9, MPDATAD.getISSUENO());
            statement.executeUpdate();
        } catch (SQLException err) {
            err.printStackTrace();
        }

    }

    public void addFact(MPDATAD MPDATAD, float NKWMENG, String STATUS, Date DATE, int ISSUENO) {

        try {
            PreparedStatement statement = connect.prepareStatement("insert into MSSMASF(LINETYP, VBELN, POSNR, MATNR, WERKS, BWTAR, LGORT, KWMENG, ISSUENO, NKWMENG, STATUS, DATE) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
            statement.setString(1, MPDATAD.getLINETYP());
            statement.setString(2, MPDATAD.getVBELN());
            statement.setString(3, MPDATAD.getPOSNR());
            statement.setString(4, MPDATAD.getMATNR());
            statement.setString(5, MPDATAD.getWERKS());
            statement.setString(6, MPDATAD.getBWTAR());
            statement.setString(7, MPDATAD.getLGORT());
            statement.setFloat(8, NKWMENG);
            statement.setInt(9, ISSUENO);
            statement.setFloat(10, MPDATAD.getKWMENG());
            statement.setString(11, STATUS);
            statement.setDate(12, new java.sql.Date(DATE.getTime()));
            statement.executeUpdate();
        } catch (SQLException err) {
            err.printStackTrace();
        }

    }

    public void update(MPDATAD MPDATAD) {

        try {
            PreparedStatement statement = connect.prepareStatement("update MSSMASD set KWMENG=? where VBELN=? and POSNR=? and MATNR=?");
            statement.setFloat(1, MPDATAD.getKWMENG());
            statement.setString(2, MPDATAD.getVBELN());
            statement.setString(3, MPDATAD.getPOSNR());
            statement.setString(4, MPDATAD.getMATNR());
            statement.executeUpdate();
        } catch (SQLException err) {
            err.printStackTrace();
        }

    }

    public void updateOnhandMss(String plant, String whls) {   //new mod update onhand from wms to mss

        try {
            PreparedStatement statement = connect.prepareStatement(" UPDATE A SET UNR01=SUMQ FROM MSSMASM A INNER JOIN (SELECT QRMCODE,SUM(QRMALQTY) as SUMQ FROM QRMMAS WHERE QRMPLANT=? AND QRMWHSE=? AND QRMVAL<>03 AND QRMSTS=2 group by QRMCODE) B ON A.MATNR=B.QRMCODE collate Thai_CI_AS ");
            statement.setString(1, plant);
            statement.setString(2, whls);
            statement.executeUpdate();
        } catch (SQLException err) {
            err.printStackTrace();
        }

    }

    public boolean checkDuplicate(MPDATAD MPDATAD) {

        boolean status = false;

        try {
            PreparedStatement statement = connect.prepareStatement("select * from MSSMASD where VBELN=? and POSNR=? and MATNR=?");
            statement.setString(1, MPDATAD.getVBELN());
            statement.setString(2, MPDATAD.getPOSNR());
            statement.setString(3, MPDATAD.getMATNR());
            ResultSet set = statement.executeQuery();

            if (set.next()) {
                status = true;
            }

        } catch (SQLException err) {
            status = true;
            err.printStackTrace();
        }

        return status;

    }

    public float getKWMENG(MPDATAD MPDATAD) {

        float KWMENG = 0.000f;

        try {
            PreparedStatement statement = connect.prepareStatement("select KWMENG from MSSMASD where VBELN=? and POSNR=? and MATNR=?");
            statement.setString(1, MPDATAD.getVBELN());
            statement.setString(2, MPDATAD.getPOSNR());
            statement.setString(3, MPDATAD.getMATNR());
            ResultSet set = statement.executeQuery();

            if (set.next()) {
                KWMENG = set.getFloat("KWMENG");
            }

        } catch (SQLException err) {
            err.printStackTrace();
        }

        return KWMENG;

    }

    public int getLineIssueNo(MPDATAD MPDATAD) {

        int ISSUENO = 0;

        try {
            PreparedStatement statement = connect.prepareStatement("select ISSUENO from MSSMASD where VBELN=? and POSNR=? and MATNR=?");
            statement.setString(1, MPDATAD.getVBELN());
            statement.setString(2, MPDATAD.getPOSNR());
            statement.setString(3, MPDATAD.getMATNR());
            ResultSet set = statement.executeQuery();

            if (set.next()) {
                ISSUENO = set.getInt("ISSUENO");
            }

        } catch (SQLException err) {
            err.printStackTrace();
        }

        return ISSUENO;

    }

    public int getIssueNo(String VBELN) {

        int issueNo = 0;

        try {

            PreparedStatement statement = connect.prepareStatement("select distinct max(ISSUENO) as ISSUENO from MSSMASD where VBELN = ?");
            statement.setString(1, VBELN);
            ResultSet set = statement.executeQuery();

            if (set.next()) {
                issueNo = set.getInt("ISSUENO");
            }

        } catch (SQLException err) {
            err.printStackTrace();
        }

        return issueNo;

    }

    public String[] getDropDownSQ(String POFG) {

        int count = 0;
        String[] sq = null;

        try {

            PreparedStatement statement = connect.prepareStatement("SELECT distinct [ISHSEQ], CHAR([ISHSEQ]+64) as [SEQNAME]\n"
                    + "FROM [ISMMASH]\n"
                    + "where [ISHSEQ] is not null\n"
                    + "and [ISHPOFG] = ?\n"
                    + "order by [ISHSEQ] desc", ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            statement.setString(1, POFG);
            ResultSet set = statement.executeQuery();

            if (set.next()) {
                set.last();
                sq = new String[set.getRow()];
                set.beforeFirst();
                while (set.next()) {
                    sq[count] = set.getString("ISHSEQ") + "|" + set.getString("SEQNAME");
                    count++;
                }

            }

            statement.close();
            set.close();

        } catch (SQLException err) {
            err.printStackTrace();
        }

        return sq;

    }

    public String[] getWareHouse(String POFG) {

        int count = 0;
        String[] sq = null;

        try {

            PreparedStatement statement = connect.prepareStatement(" SELECT QWHCOD,QWHNAME FROM QWHMAS  ORDER BY QWHCOD  ", ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            //statement.setString(1, POFG);
            ResultSet set = statement.executeQuery();

            if (set.next()) {
                set.last();
                sq = new String[set.getRow()];
                set.beforeFirst();
                while (set.next()) {
                    sq[count] = set.getString("QWHCOD") + "|" + set.getString("QWHNAME");
                    count++;
                }

            }

            statement.close();
            set.close();

        } catch (SQLException err) {
            err.printStackTrace();
        }

        return sq;

    }

    public String[] getDropDownSQ__2(String POFG, String ISSUENO1) {

        int count = 0;
        String[] sq = null;

        try {

            PreparedStatement statement = connect.prepareStatement("select distinct A.ISSUENO, ISSUENAME from MSSMASD A left join MSSMASH B on A.VBELN = B.VBELN left join MSSSEQN C on A.ISSUENO = C.ISSUENO  where B.POFG = ? and GRPNO != '' and A.ISSUENO >= ? order by A.ISSUENO desc", ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            statement.setString(1, POFG);
            statement.setString(2, ISSUENO1);
            ResultSet set = statement.executeQuery();

            if (set.next()) {
                set.last();
                sq = new String[set.getRow()];
                set.beforeFirst();
                while (set.next()) {
                    sq[count] = set.getString("ISSUENO") + "|" + set.getString("ISSUENAME");
                    count++;
                }

            }

        } catch (SQLException err) {
            err.printStackTrace();
        }

        return sq;

    }

    public String[] getDropDownInv(String GRPNO) {

        int count = 0;
        String[] Inv = null;

        try {

            PreparedStatement statement = connect.prepareStatement("SELECT DISTINCT SWIVNO FROM SWGLINE WHERE SWGRPST = ?", ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            statement.setString(1, GRPNO);
            ResultSet set = statement.executeQuery();

            if (set.next()) {
                set.last();
                Inv = new String[set.getRow()];
                set.beforeFirst();
                while (set.next()) {
                    Inv[count] = set.getString("SWIVNO");
                    count++;
                }

            }

        } catch (SQLException err) {
            err.printStackTrace();
        }

        return Inv;

    }

    public String[] getDropDownCA(String INVNO, int type) {

        String orderBy = "";
        int count = 0;
        String[] Ca = null;

        try {

            if (type == 2) {
                orderBy = "DESC";
            }

            PreparedStatement statement = connect.prepareStatement("SELECT DISTINCT SWCARNO FROM SWGLINE WHERE SWIVNO = ? ORDER BY SWCARNO " + orderBy, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            statement.setString(1, INVNO);
            ResultSet set = statement.executeQuery();

            if (set.next()) {
                set.last();
                Ca = new String[set.getRow()];
                set.beforeFirst();
                while (set.next()) {
                    Ca[count] = set.getString("SWCARNO");
                    count++;
                }

            }

        } catch (SQLException err) {
            err.printStackTrace();
        }

        return Ca;

    }

    public List<MPDATAD> getDetailForPack(String GRPNO, int ISSUENO, String MATNR) {

        List<MPDATAD> list = new ArrayList<MPDATAD>();

        try {

            PreparedStatement statement = connect.prepareStatement("SELECT [ISDORD] as VBELN\n"
                    + "      ,[ISDLINO] as POSNR\n"
                    + "      ,[ISDITNO] as MATNR\n"
                    + "	  ,CEILING(isnull(sum([ISDRQQTY]),0) * 100) / 100 as KWMENG\n"
                    + "      ,isnull(sum([ISDTEMP]),0) as TEMP\n"
                    + "  FROM [RMShipment].[dbo].[ISMMASD]\n"
                    + "  where [ISDJBNO] = ?\n"
                    + "  and [ISDSEQ] = ?\n"
                    + "  and [ISDITNO] = ?\n"
                    + "  and isnull([ISDRQQTY],0) - isnull([ISDTEMP],0) > 0\n"
                    + "  group by [ISDORD],[ISDLINO],[ISDITNO]\n"
                    + "  order by [ISDORD]");
            statement.setString(1, GRPNO);
            statement.setInt(2, ISSUENO);
            statement.setString(3, MATNR);
            ResultSet set = statement.executeQuery();

            while (set.next()) {
                MPDATAD MPDATAD = new MPDATAD();
                MPDATAD.setVBELN(set.getString("VBELN"));
                MPDATAD.setPOSNR(set.getString("POSNR"));
                MPDATAD.setMATNR(set.getString("MATNR"));
                MPDATAD.setKWMENG(set.getFloat("KWMENG"));
                MPDATAD.setTEMP(set.getFloat("TEMP"));
                list.add(MPDATAD);
            }

        } catch (SQLException err) {
            err.printStackTrace();
        }

        return list;

    }

    public List<MPDATAD> getDetailForClear(String GRPNO, int ISSUENO, String MATNR) {

        List<MPDATAD> list = new ArrayList<MPDATAD>();

        try {

            PreparedStatement statement = connect.prepareStatement("SELECT [ISDORD] as VBELN\n"
                    + "      ,[ISDLINO] as POSNR\n"
                    + "      ,[ISDITNO] as MATNR\n"
                    + "      ,isnull(sum([ISDRQQTY]),0) as KWMENG\n"
                    + "      ,isnull(sum([ISDTEMP]),0) as TEMP\n"
                    + "  FROM [RMShipment].[dbo].[ISMMASD]\n"
                    + "  where [ISDJBNO] = ?\n"
                    + "  and [ISDSEQ] = ?\n"
                    + "  and [ISDITNO] = ?\n"
                    + "  group by [ISDORD],[ISDLINO],[ISDITNO]\n"
                    + "  order by [ISDORD]");
            statement.setString(1, GRPNO);
            statement.setInt(2, ISSUENO);
            statement.setString(3, MATNR);
            ResultSet set = statement.executeQuery();

            while (set.next()) {
                MPDATAD MPDATAD = new MPDATAD();
                MPDATAD.setVBELN(set.getString("VBELN"));
                MPDATAD.setPOSNR(set.getString("POSNR"));
                MPDATAD.setMATNR(set.getString("MATNR"));
                MPDATAD.setKWMENG(set.getFloat("KWMENG"));
                MPDATAD.setTEMP(set.getFloat("TEMP"));
                list.add(MPDATAD);
            }

        } catch (SQLException err) {
            err.printStackTrace();
        }

        return list;

    }

    public void updateTemp(float TEMP, String VBELN, String POSNR, String MATNR) {

        try {
            PreparedStatement statement = connect.prepareStatement("update ISMMASD set [ISDTEMP]=? where [ISDORD]=? and [ISDLINO]=? and [ISDITNO]=?");
            statement.setFloat(1, TEMP);
            statement.setString(2, VBELN);
            statement.setString(3, POSNR);
            statement.setString(4, MATNR);
            statement.executeUpdate();
        } catch (SQLException err) {
            err.printStackTrace();
        }

    }

    public void deleteDetail(MPDATAD MPDATAD) {

        try {
            PreparedStatement statement = connect.prepareStatement("delete from MSSMASD where VBELN=? and POSNR=? and MATNR=?");
            statement.setString(1, MPDATAD.getVBELN());
            statement.setString(2, MPDATAD.getPOSNR());
            statement.setString(3, MPDATAD.getMATNR());
            statement.executeUpdate();
        } catch (SQLException err) {
            err.printStackTrace();
        }

    }

    public void updateStatus(String GRPNO, String ISSUENO, String MATNR, int STATUS, int STATUSTMP) {

        try {
            PreparedStatement statement = connect.prepareStatement("update ISMMASD set [ISDSTAD]=? , [ISDSTSTMP]=? where [ISDJBNO]=? and [ISDSEQ]=? and [ISDITNO]=?");
            statement.setInt(1, STATUS);
            statement.setInt(2, STATUSTMP);
            statement.setString(3, GRPNO);
            statement.setString(4, ISSUENO);
            statement.setString(5, MATNR);
            statement.executeUpdate();

            //NEW MOD
            // PreparedStatement statement = connect.prepareStatement("UPDATE MSSPACK SET CHKUP=4 WHERE STATUS=1 AND GRPNO=? and ISSUENO=? and MATNR=?");
            // statement.setInt(1, STATUS);
            // statement.setString(2, GRPNO);
            // statement.setString(3, ISSUENO);
            // statement.setString(4, MATNR);
            // statement.executeUpdate();
        } catch (SQLException err) {
            err.printStackTrace();
        } finally {
            try {
                if (connect != null) {
                    connect.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

    }

    public void updateStatusTmp(String GRPNO, String ISSUENO, String MATNR, int STATUS) {

        try {
            PreparedStatement statement = connect.prepareStatement("update ISMMASD set [ISDSTSTMP]=? where [ISDJBNO]=? and [ISDSEQ]=? and [ISDITNO]=?");
            statement.setInt(1, STATUS);
            statement.setString(2, GRPNO);
            statement.setString(3, ISSUENO);
            statement.setString(4, MATNR);
            statement.executeUpdate();

            //NEW MOD
            // PreparedStatement statement = connect.prepareStatement("UPDATE MSSPACK SET CHKUP=4 WHERE STATUS=1 AND GRPNO=? and ISSUENO=? and MATNR=?");
            // statement.setInt(1, STATUS);
            // statement.setString(2, GRPNO);
            // statement.setString(3, ISSUENO);
            // statement.setString(4, MATNR);
            // statement.executeUpdate();
        } catch (SQLException err) {
            err.printStackTrace();
        }

    }

    public void addQIHEAD(String GRPNO, String ISSUENO, String MATNR, int STS) {

        try {

            PreparedStatement statement = connect.prepareStatement("insert into [XIHEAD] ([QIHCOM]\n"
                    + "      ,[QIHWHS]\n"
                    + "      ,[QIHTRDT]\n"
                    + "      ,[QIHQNO]\n"
                    + "      ,[QIHMVT]\n"
                    + "      ,[QIHDEST]\n"
                    + "      ,[QIHTOTD]\n"
                    + "      ,[QIHSET]\n"
                    + "      ,[QIHSETTD]\n"
                    + "      ,[QIHPDGP]\n"
                    + "      ,[QIHMTCTRL]\n"
                    + "      ,[QIHSTS]\n"
                    + "      ,[QIHHISTS]\n"
                    + "      ,[QIHTOTQTY]\n"
                    + "      ,[QIHEDT]\n"
                    + "      ,[QIHCDT]\n"
                    + "      ,[QIHUSER]\n"
                    + "      ,[QIHPDTYPE])\n"
                    + "select ISHCONO, ISHWHNO, format(ISHT1DT,'####-##-##'), \n"
                    + "ISDQNO, '601', ISDDEST, \n"
                    + "(select count(*)\n"
                    + "FROM [ISMPACK]\n"
                    + "where [GRPNO]=[ISDJBNO]\n"
                    + "and [ISSUENO]=[ISDSEQ]\n"
                    + "and [MATNR]=[ISDITNO]), \n"
                    + "0, 0, ISHPGRP, ISDMTCTRL, ?, ?,\n"
                    + "(select sum(VALUE)\n"
                    + "FROM [ISMPACK]\n"
                    + "where [GRPNO]=[ISDJBNO]\n"
                    + "and [ISSUENO]=[ISDSEQ]\n"
                    + "and [MATNR]=[ISDITNO]), \n"
                    + "current_timestamp, current_timestamp, ISDUSER, null\n"
                    + "from ISMMASD dt\n"
                    + "left join ISMMASH hd on hd.[ISHORD] = dt.[ISDORD]\n"
                    + "and hd.[ISHSEQ] = dt.[ISDSEQ]\n"
                    + "where [ISDJBNO]=? and [ISDSEQ]=? and [ISDITNO]=?");
            statement.setInt(1, STS);
            statement.setInt(2, STS);
            statement.setString(3, GRPNO);
            statement.setString(4, ISSUENO);
            statement.setString(5, MATNR);
            statement.executeUpdate();

        } catch (SQLException err) {
            err.printStackTrace();
        } finally {
            try {
                if (connect != null) {
                    connect.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

    }

    public void addQIDETAIL(String GRPNO, String ISSUENO, String MATNR, int STS) {

        try {

            PreparedStatement statement = connect.prepareStatement("insert into [XIDETAIL] ([QIDCOM]\n"
                    + "      ,[QIDWHS]\n"
                    + "      ,[QIDTRDT]\n"
                    + "      ,[QIDQNO]\n"
                    + "      ,[QIDLINE]\n"
                    + "      ,[QIDMAT]\n"
                    + "      ,[QIDID]\n"
                    + "      ,[QIDMAT2]\n"
                    + "      ,[QIDID2]\n"
                    + "      ,[QIDROLL]\n"
                    + "      ,[QIDGPQR]\n"
                    + "      ,[QIDQTY]\n"
                    + "      ,[QIDBUN]\n"
                    + "      ,[QIDPACKTYPE]\n"
                    + "      ,[QIDREMARK]\n"
                    + "      ,[QIDSTS]\n"
                    + "      ,[QIDDLDT]\n"
                    + "      ,[QIDAPUSR]\n"
                    + "      ,[QIDAPDT]\n"
                    + "      ,[QIDRCUSR]\n"
                    + "      ,[QIDRCDT]\n"
                    + "      ,[QIDSHUSR]\n"
                    + "      ,[QIDSHDT]\n"
                    + "      ,[QIDTPUSR]\n"
                    + "      ,[QIDTPDT]\n"
                    + "      ,[QIDRUNNO]\n"
                    + "      ,[QIDROUND]\n"
                    + "      ,[QIDOCNO]\n"
                    + "      ,[QIDQRCD]\n"
                    + "      ,[QIDDTDT]\n"
                    + "      ,[QIDDTUSR]\n"
                    + "      ,[QIDEDT]\n"
                    + "      ,[QIDCDT]\n"
                    + "      ,[QIDUSER]\n"
                    + "      ,[QIDFLAG]\n"
                    + "      ,[QIDMTCTRL]\n"
                    + "      ,[QIDPDGP]\n"
                    + "      ,[QIDPDTYPE])\n"
                    + "select ISHCONO, ISHWHNO, format(ISHT1DT,'####-##-##'), \n"
                    + "ISDQNO, (select isnull(max([QIDLINE]),0)+[NO] from XIDETAIL\n"
                    + "where [QIDWHS] = ISHWHNO\n"
                    + "and format([QIDTRDT],'yyyy-MM-dd') = format(ISHT1DT,'####-##-##')\n"
                    + "and [QIDQNO] = ISDQNO), [MATNR], [MATID], null, null, null, null,\n"
                    + "VALUE, [ISDUNIT], [TYPE], null, ?, null, [ISHAPUSR],\n"
                    + "(format([ISHAPDT],'####-##-##')+' '+format([ISHAPTM],'##:##:##')),\n"
                    + "[ISHRCUSR],\n"
                    + "(format([ISHRCDT],'####-##-##')+' '+format([ISHRCTM],'##:##:##')),\n"
                    + "null, null,\n"
                    + "[ISHTPUSR],\n"
                    + "(format([ISHTPDT],'####-##-##')+' '+format([ISHTPTM],'##:##:##')),\n"
                    + "null, null, null, null, null, null,\n"
                    + "current_timestamp, current_timestamp, ISDUSER, null,\n"
                    + "ISDMTCTRL, ISHPGRP, null\n"
                    + "from ISMMASD dt\n"
                    + "left join ISMMASH hd on hd.[ISHORD] = dt.[ISDORD]\n"
                    + "and hd.[ISHSEQ] = dt.[ISDSEQ]\n"
                    + "left join [ISMPACK] pk on [GRPNO]=[ISDJBNO]\n"
                    + "and [ISSUENO]=[ISDSEQ]\n"
                    + "and [MATNR]=[ISDITNO]\n"
                    + "where [ISDJBNO]=? and [ISDSEQ]=? and [ISDITNO]=?");
            statement.setInt(1, STS);
            statement.setString(2, GRPNO);
            statement.setString(3, ISSUENO);
            statement.setString(4, MATNR);
            statement.executeUpdate();

        } catch (SQLException err) {
            err.printStackTrace();
        } finally {
            try {
                if (connect != null) {
                    connect.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

    }

    public int checkDupQIHEAD(String GRPNO, String ISSUENO, String MATNR) {

        int cnt = 0;

        try {

            PreparedStatement statement = connect.prepareStatement("select (SELECT count(*)\n"
                    + "  FROM [XIHEAD]\n"
                    + "  where [QIHWHS]=WH\n"
                    + "  and format([QIHTRDT],'yyyy-MM-dd')=TRDT\n"
                    + "  and [QIHQNO]=QNO) as CNT\n"
                    + "from(\n"
                    + "select ISHWHNO as WH, format(ISHT1DT,'####-##-##') as TRDT, ISDQNO as QNO\n"
                    + "from ISMMASD dt\n"
                    + "left join ISMMASH hd on hd.[ISHORD] = dt.[ISDORD]\n"
                    + "and hd.[ISHSEQ] = dt.[ISDSEQ]\n"
                    + "where [ISDJBNO]=? and [ISDSEQ]=? and [ISDITNO]=?\n"
                    + ")tmp");
            statement.setString(1, GRPNO);
            statement.setString(2, ISSUENO);
            statement.setString(3, MATNR);
            ResultSet set = statement.executeQuery();

            while (set.next()) {
                cnt = set.getInt("CNT");
            }

        } catch (SQLException err) {
            err.printStackTrace();
        } finally {
            try {
                if (connect != null) {
                    connect.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return cnt;

    }

    public String getSQLupdateHead(String GRPNO, String ISSUENO, String MATNR) {

        String sql = "";
        String WH = "";
        String TRDT = "";
        String QNO = "";
        String CNT = "";
        String SUM = "";

        try {

            PreparedStatement statement = connect.prepareStatement("select WH,TRDT,QNO,\n"
                    + "isnull((select count(*)\n"
                    + "FROM [ISMPACK]\n"
                    + "where [GRPNO]=[ISDJBNO]\n"
                    + "and [ISSUENO]=[ISDSEQ]\n"
                    + "and [MATNR]=[ISDITNO]),0) as CNT,\n"
                    + "isnull((select sum(VALUE)\n"
                    + "FROM [ISMPACK]\n"
                    + "where [GRPNO]=[ISDJBNO]\n"
                    + "and [ISSUENO]=[ISDSEQ]\n"
                    + "and [MATNR]=[ISDITNO]),0) as [SUM]\n"
                    + "from(\n"
                    + "select ISHWHNO as WH, format(ISHT1DT,'####-##-##') as TRDT, ISDQNO as QNO\n"
                    + ",[ISDJBNO],[ISDSEQ],[ISDITNO]\n"
                    + "from ISMMASD dt\n"
                    + "left join ISMMASH hd on hd.[ISHORD] = dt.[ISDORD]\n"
                    + "and hd.[ISHSEQ] = dt.[ISDSEQ]\n"
                    + "where [ISDJBNO]=? and [ISDSEQ]=? and [ISDITNO]=?\n"
                    + ")tmp");
            statement.setString(1, GRPNO);
            statement.setString(2, ISSUENO);
            statement.setString(3, MATNR);
            ResultSet set = statement.executeQuery();

            while (set.next()) {
                WH = set.getString("WH");
                TRDT = set.getString("TRDT");
                QNO = set.getString("QNO");
                CNT = set.getString("CNT");
                SUM = set.getString("SUM");

                sql = "update [RMShipment].[dbo].[XIHEAD]\n"
                        + "  set [QIHTOTD] = [QIHTOTD]+" + CNT + "\n"
                        + "     ,[QIHTOTQTY] = [QIHTOTQTY]+" + SUM + "\n"
                        + "  where [QIHWHS] = '" + WH + "'\n"
                        + "  and format([QIHTRDT],'yyyy-MM-dd') = '" + TRDT + "'\n"
                        + "  and [QIHQNO] = '" + QNO + "'\n";
            }

        } catch (SQLException err) {
            err.printStackTrace();
        } finally {
            try {
                if (connect != null) {
                    connect.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return sql;

    }

    public String getSQLdeleteHeadDetail(String GRPNO, String ISSUENO, String MATNR) {

        String sql = "";
        String WH = "";
        String TRDT = "";
        String QNO = "";

        try {

            PreparedStatement statement = connect.prepareStatement("select WH,TRDT,QNO,\n"
                    + "isnull((select count(*)\n"
                    + "FROM [ISMPACK]\n"
                    + "where [GRPNO]=[ISDJBNO]\n"
                    + "and [ISSUENO]=[ISDSEQ]\n"
                    + "and [MATNR]=[ISDITNO]),0) as CNT,\n"
                    + "isnull((select sum(VALUE)\n"
                    + "FROM [ISMPACK]\n"
                    + "where [GRPNO]=[ISDJBNO]\n"
                    + "and [ISSUENO]=[ISDSEQ]\n"
                    + "and [MATNR]=[ISDITNO]),0) as [SUM]\n"
                    + "from(\n"
                    + "select ISHWHNO as WH, format(ISHT1DT,'####-##-##') as TRDT, ISDQNO as QNO\n"
                    + ",[ISDJBNO],[ISDSEQ],[ISDITNO]\n"
                    + "from ISMMASD dt\n"
                    + "left join ISMMASH hd on hd.[ISHORD] = dt.[ISDORD]\n"
                    + "and hd.[ISHSEQ] = dt.[ISDSEQ]\n"
                    + "where [ISDJBNO]=? and [ISDSEQ]=? and [ISDITNO]=?\n"
                    + ")tmp");
            statement.setString(1, GRPNO);
            statement.setString(2, ISSUENO);
            statement.setString(3, MATNR);
            ResultSet set = statement.executeQuery();

            while (set.next()) {
                WH = set.getString("WH");
                TRDT = set.getString("TRDT");
                QNO = set.getString("QNO");

                sql = "delete XIHEAD\n"
                        + "where [QIHWHS] = '" + WH + "'\n"
                        + "and format([QIHTRDT],'yyyy-MM-dd') = '" + TRDT + "'\n"
                        + "and [QIHQNO] = '" + QNO + "'\n"
                        + "\n"
                        + "delete XIDETAIL\n"
                        + "where [QIDWHS] = '" + WH + "'\n"
                        + "and format([QIDTRDT],'yyyy-MM-dd') = '" + TRDT + "'\n"
                        + "and [QIDQNO] = '" + QNO + "'\n";
            }

        } catch (SQLException err) {
            err.printStackTrace();
        } finally {
            try {
                if (connect != null) {
                    connect.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return sql;

    }

    public void execSQL(String sql) {

        try {
            PreparedStatement statement = connect.prepareStatement(sql);
            statement.executeUpdate();

        } catch (SQLException err) {
            err.printStackTrace();
        } finally {
            try {
                if (connect != null) {
                    connect.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

    }

    public void updateStatusQRMMAS(String GRPNO, String ISSUENO, String MATNR, int STATUS) {

        try {
            PreparedStatement statement = connect.prepareStatement("update QRMMAS\n"
                    + "set QRMSTS = ?\n"
                    + "where QRMID in (select [MATID]\n"
                    + "from ISMMASD dt\n"
                    + "left join [ISMPACK] pk on [GRPNO]=[ISDJBNO]\n"
                    + "and [ISSUENO]=[ISDSEQ]\n"
                    + "and [MATNR]=[ISDITNO]\n"
                    + "where [ISDJBNO]=? and [ISDSEQ]=? and [ISDITNO]=?)");
            statement.setInt(1, STATUS);
            statement.setString(2, GRPNO);
            statement.setString(3, ISSUENO);
            statement.setString(4, MATNR);
            statement.executeUpdate();

            //NEW MOD
            // PreparedStatement statement = connect.prepareStatement("UPDATE MSSPACK SET CHKUP=4 WHERE STATUS=1 AND GRPNO=? and ISSUENO=? and MATNR=?");
            // statement.setInt(1, STATUS);
            // statement.setString(2, GRPNO);
            // statement.setString(3, ISSUENO);
            // statement.setString(4, MATNR);
            // statement.executeUpdate();
        } catch (SQLException err) {
            err.printStackTrace();
        }

    }

    public void updateSTATTo3(String GRPNO, String ISSUENO, String MATNR, int STATUS) {

        try {

            PreparedStatement statement = connect.prepareStatement("UPDATE [RMShipment].[dbo].[ISMSTAT] SET [STATUS] = 3 WHERE [GRPNO] = ? AND [ISSUENO] = ? AND [MATNR] = ?");

            statement.setString(1, GRPNO);
            statement.setString(2, ISSUENO);
            statement.setString(3, MATNR);

            statement.executeUpdate();

        } catch (SQLException err) {
            err.printStackTrace();
        } finally {
            try {
                if (connect != null) {
                    connect.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

    }

    public void updateStatusHead(String GRPNO, String ISSUENO, String MAT, int STATUS, String typeSTS) {

        try {
            PreparedStatement statement = connect.prepareStatement("UPDATE ISMMASH SET " + typeSTS + " = ?\n"
                    + "WHERE ISHORD = (SELECT ISDORD FROM [RMShipment].[dbo].[ISMMASD]  WHERE \n"
                    + "ISDJBNO = ? \n"
                    + "AND ISDMTCTRL = ? \n"
                    + "AND ISDITNO = ?)");

            statement.setInt(1, STATUS);
            statement.setString(2, GRPNO);
            statement.setString(3, MAT);
            statement.setString(4, ISSUENO);

            statement.executeUpdate();

        } catch (SQLException err) {
            err.printStackTrace();
        } finally {
            try {
                if (connect != null) {
                    connect.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

    }

    public void updateStatusDetail(String GRPNO, String ISSUENO, String MAT, int STATUS, String userid, String pick) {

        SimpleDateFormat formatter = new SimpleDateFormat("HHmmss");
        Date date = new Date(System.currentTimeMillis());
        String tm = formatter.format(date);

        try {
            PreparedStatement statement = connect.prepareStatement("UPDATE [RMShipment].[dbo].[ISMMASD] SET ISDSTS = ?, ISDPKDT = format(current_timestamp,'yyyyMMdd'),ISDPKTM = ?, ISDPKUSER = ?, ISDPKQTY = ? WHERE \n"
                    + "ISDJBNO = ? \n"
                    + "AND ISDMTCTRL = ? \n"
                    + "AND ISDITNO = ?");

            statement.setInt(1, STATUS);
            statement.setString(2, tm);
            statement.setString(3, userid);
            statement.setString(4, pick);
            statement.setString(5, GRPNO);
            statement.setString(6, MAT);
            statement.setString(7, ISSUENO);

            statement.executeUpdate();

        } catch (SQLException err) {
            err.printStackTrace();
        } finally {
            try {
                if (connect != null) {
                    connect.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public String getSTSHead(String GRPNO, String MATNR) {

        String STS = "";

        try {

            PreparedStatement statement = connect.prepareStatement("SELECT TOP 1 ISHHSTS FROM ISMMASH WHERE ISHORD = (SELECT TOP 1 ISDORD FROM ISMMASD WHERE ISDJBNO = ? AND ISDITNO = ?)");

            statement.setString(1, GRPNO);
            statement.setString(2, MATNR);
            ResultSet set = statement.executeQuery();

            while (set.next()) {
                STS = set.getString("ISHHSTS");
            }

        } catch (SQLException err) {
            err.printStackTrace();
        } finally {
            try {
                if (connect != null) {
                    connect.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return STS;

    }

    public String getSTSDetail(String GRPNO, String MATNR) {

        String STS = "";

        try {

            PreparedStatement statement = connect.prepareStatement("SELECT TOP 1 ISDSTS FROM ISMMASD WHERE ISDJBNO = ? AND ISDITNO = ?");

            statement.setString(1, GRPNO);
            statement.setString(2, MATNR);
            ResultSet set = statement.executeQuery();

            while (set.next()) {
                STS = set.getString("ISDSTS");
            }

        } catch (SQLException err) {
            err.printStackTrace();
        } finally {
            try {
                if (connect != null) {
                    connect.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return STS;

    }

    public boolean getCKSTS(String GRPNO, String MATNR, String MATCTRL) {

        boolean ck = true;

        String sql = "SELECT DISTINCT ISDSTS FROM ISMMASD \n"
                + "WHERE ISDORD = (SELECT TOP 1 ISDORD FROM ISMMASD WHERE ISDJBNO = ? AND ISDITNO = ? AND ISDMTCTRL = ?)";

        try {

            PreparedStatement statement = connect.prepareStatement(sql);

            statement.setString(1, GRPNO);
            statement.setString(2, MATNR);
            statement.setString(3, MATCTRL);
            ResultSet set = statement.executeQuery();

            while (set.next()) {
                if (set.getString("ISDSTS").equals("5")) {
                    ck = false;
                }
            }

        } catch (SQLException err) {
            err.printStackTrace();
        } finally {
            try {
                if (connect != null) {
                    connect.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return ck;

    }

    public boolean updateBackwardH(String jbno, String seq, String mat) {

        SimpleDateFormat formatter = new SimpleDateFormat("HHmmss");
        Date date = new Date(System.currentTimeMillis());
        String tm = formatter.format(date);

        boolean result = false;

        String sql = "UPDATE [ISMMASH] "
                + "SET [ISHLSTS] = '0' "
                + "   ,[ISHHSTS] = '0' "
                + "   ,[ISHRQDT] = null"
                + "   ,[ISHRQTM] = null"
                + "   ,[ISHAPDT] = null"
                + "   ,[ISHAPTM] = null"
                + "   ,[ISHAPUSR] = null"
                + "   ,[ISHRCDT] = null"
                + "   ,[ISHRCTM] = null"
                + "   ,[ISHRCUSR] = null"
                + "   ,[ISHJBNO] = null"
                + "   ,[ISHP1DT] = null"
                + "   ,[ISHA1DT] = null"
                + "   ,[ISHT1DT] = null"
                + "   ,[ISHPKDT] = null"
                + "   ,[ISHPKTM] = null"
                + "   ,[ISHPKUSR] = null"
                + "   ,[ISHEDT] = format(current_timestamp,'yyyyMMdd') "
                + "   ,[ISHETM] = '" + tm + "' "
                + " WHERE ISHORD = (SELECT TOP 1 ISDORD FROM ISMMASD WHERE ISDJBNO = ? AND ISDITNO = ?) AND ISHSEQ = ?";
//        System.out.println("" + sql); 
        try {
            PreparedStatement ps = connect.prepareStatement(sql);

//            ps.setString(1, uid);
            ps.setString(1, jbno);
            ps.setString(2, mat);
            ps.setString(3, seq);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

        } catch (Exception e) {

            e.printStackTrace();

        } finally {
            try {
                if (connect != null) {
                    connect.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return result;

    }

    public boolean updateBackWardAllDet(String jbno, String seq, String mat) {

        SimpleDateFormat formatter = new SimpleDateFormat("HHmmss");
        Date date = new Date(System.currentTimeMillis());
        String tm = formatter.format(date);

        boolean result = false;

        String sql = "UPDATE [ISMMASD] "
                + "SET [ISDSTS] = '0' "
                + "   ,[ISDRQDT] = null"
                + "   ,[ISDRQTM] = null"
                + "   ,[ISDAPDT] = null"
                + "   ,[ISDAPTM] = null"
                + "   ,[ISDAPUSER] = null"
                + "   ,[ISDRCDT] = null"
                + "   ,[ISDRCTM] = null"
                + "   ,[ISDRCUSER] = null"
                + "   ,[ISDJBNO] = null"
                + "   ,[ISDQNO] = null"
                + "   ,[ISDPKDT] = null"
                + "   ,[ISDPKTM] = null"
                + "   ,[ISDPKUSER] = null"
                + "   ,[ISDPKQTY] = null"
                + "   ,[ISDTEMP] = null"
                + "   ,[ISDSTAD] = 1"
                + "   ,[ISDSTSTMP] = NULL"
                + "   ,[ISDEDT] = format(current_timestamp,'yyyyMMdd') "
                + "   ,[ISDETM] = '" + tm + "' "
                + " WHERE ISDORD = (SELECT TOP 1 ISDORD FROM ISMMASD WHERE ISDJBNO = ? AND ISDITNO = ?) AND ISDSEQ = ?";
//        System.out.println("" + sql);
        try {
            PreparedStatement ps = connect.prepareStatement(sql);

//            ps.setString(1, uid);
            ps.setString(1, jbno);
            ps.setString(2, mat);
            ps.setString(3, seq);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

        } catch (Exception e) {

            e.printStackTrace();

        } finally {
            try {
                if (connect != null) {
                    connect.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return result;

    }

}
