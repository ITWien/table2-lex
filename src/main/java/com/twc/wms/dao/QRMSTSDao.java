/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.dao;

import com.twc.wms.database.database;
import com.twc.wms.entity.QDESTYP;
import com.twc.wms.entity.Qdest;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 *
 * @author nutthawoot.noo
 */
public class QRMSTSDao extends database {

    public List<Qdest> findAll() {

        List<Qdest> QdestList = new ArrayList<Qdest>();

        String sql = "SELECT * FROM QRMSTS ";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                Qdest p = new Qdest();

                p.setCode(result.getString("STSCODE"));

                p.setDesc(result.getString("STSDESC"));

                QdestList.add(p);

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return QdestList;

    }

    public String check(String code) {
        PreparedStatement ps = null;
        ResultSet rs = null;
        String STRRETURN = null;
        try {
            String check = "Select * FROM [RMShipment].[dbo].[QRMSTS] where [STSCODE] = '" + code + "'";
            ps = connect.prepareStatement(check);
            rs = ps.executeQuery();
            if (rs.next()) {
                STRRETURN = "f";
            } else {
                STRRETURN = "t";
            }
        } catch (Exception exp) {
            exp.printStackTrace();
        }
        return STRRETURN;
    }

    public boolean add(String code, String desc, String uid) {

        boolean result = false;

        String sql = "INSERT INTO QRMSTS"
                + " (STSCOM, STSCODE, STSDESC, STSEDT, STSCDT, STSUSER)"
                + " VALUES('TWC', '" + code + "', '" + desc + "', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '" + uid + "')";

//        System.out.println(sql);
        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean edit(String code, String desc, String uid) {

        boolean result = false;

        String sql = "UPDATE QRMSTS SET STSDESC = '" + desc + "', STSEDT = CURRENT_TIMESTAMP"
                + ", STSUSER = '" + uid + "'"
                + " WHERE STSCODE = '" + code + "'";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public void delete(String code) {

        String sql = "DELETE FROM QRMSTS WHERE STSCODE = '" + code + "'";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ps.executeUpdate();

        } catch (Exception e) {

            e.printStackTrace();

        }

    }

}
