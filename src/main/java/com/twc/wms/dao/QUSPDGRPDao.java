/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.dao;

import com.twc.wms.database.database;
import com.twc.wms.entity.QUSPDGRP;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author 93176
 */
public class QUSPDGRPDao extends database {
    
    public List<QUSPDGRP> findProdGrpByUid(String uid) {

        List<QUSPDGRP> UAList = new ArrayList<QUSPDGRP>();

        String sql = "SELECT GR.[QUSCOM]\n"
                + "      ,GR.[QUSRUNNO]\n"
                + "      ,GR.[QUSUSERID]\n"
                + "      ,GR.[QUSPDGRP]\n"
                + "	  ,ST.[QWHBGRP]\n"
                + "      ,GR.[QUSEDT]\n"
                + "      ,GR.[QUSCDT]\n"
                + "      ,GR.[QUSUSER]\n"
                + "  FROM [RMShipment].[dbo].[QUSPDGRP] GR\n"
                + "  LEFT JOIN [RMShipment].[dbo].[QWHSTR] ST ON GR.[QUSPDGRP] = ST.[QWHMGRP]\n"
                + "  WHERE GR.[QUSUSERID] = ?";

        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            ps.setString(1, uid);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                QUSPDGRP p = new QUSPDGRP();

                p.setQUSPDGRP(result.getString("QUSPDGRP"));
                p.setQWHBGRP(result.getString("QWHBGRP"));
                p.setQUSUSER(result.getString("QUSUSER"));
                p.setQUSUSERID(result.getString("QUSUSERID"));

                UAList.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public List<QUSPDGRP> findProdGrp() {

        List<QUSPDGRP> UAList = new ArrayList<QUSPDGRP>();

        String sql = "SELECT DISTINCT GR.[QUSPDGRP]\n"
                + "	  ,ST.[QWHBGRP]\n"
                + "  FROM [RMShipment].[dbo].[QUSPDGRP] GR\n"
                + "  LEFT JOIN [RMShipment].[dbo].[QWHSTR] ST ON GR.[QUSPDGRP] = ST.[QWHMGRP]";

        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                QUSPDGRP p = new QUSPDGRP();

                p.setQUSPDGRP(result.getString("QUSPDGRP"));
                p.setQWHBGRP(result.getString("QWHBGRP"));

                UAList.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public String checkPrdGrp(String uid, String pid) {
        PreparedStatement ps = null;
        ResultSet rs = null;
        String STRRETURN = null;
        try {
            String check = "SELECT GR.[QUSCOM]\n"
                    + "      ,GR.[QUSRUNNO]\n"
                    + "      ,GR.[QUSUSERID]\n"
                    + "      ,GR.[QUSPDGRP]\n"
                    + "     ,ST.[QWHBGRP]\n"
                    + "      ,GR.[QUSEDT]\n"
                    + "      ,GR.[QUSCDT]\n"
                    + "      ,GR.[QUSUSER]\n"
                    + "  FROM [RMShipment].[dbo].[QUSPDGRP] GR\n"
                    + "  LEFT JOIN [RMShipment].[dbo].[QWHSTR] ST ON GR.[QUSPDGRP] = ST.[QWHMGRP]\n"
                    + "  WHERE GR.[QUSUSERID] = '" + uid + "' AND GR.[QUSPDGRP] = '" + pid + "'";

//            System.out.println(check);
            ps = connect.prepareStatement(check);
            rs = ps.executeQuery();
            if (rs.next()) {
                STRRETURN = "f";
            } else {
                STRRETURN = "t";
            }
            connect.close();
        } catch (Exception exp) {
            exp.printStackTrace();
        }
        return STRRETURN;
    }

    public boolean addProdGrp(QUSPDGRP ua, String uid) {

        boolean result = false;

        String sql = "INSERT INTO [RMShipment].[dbo].[QUSPDGRP]  \n"
                + "([QUSCOM],[QUSUSERID],[QUSPDGRP],[QUSEDT],[QUSCDT],[QUSUSER])\n"
                + "VALUES (?,?,?,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,?)";

//        System.out.println(sql);
        try {
            PreparedStatement ps = connect.prepareStatement(sql);
            ps.setString(1, "TWC");
            ps.setString(2, ua.getQUSUSERID());
            ps.setString(3, ua.getQUSPDGRP());
            ps.setString(4, uid);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public QUSPDGRP findProdGrpByUidObj(String uid, String pid) {
        
        String sql = "SELECT GR.[QUSCOM]\n"
                + "      ,GR.[QUSRUNNO]\n"
                + "      ,GR.[QUSUSERID]\n"
                + "      ,GR.[QUSPDGRP]\n"
                + "	 ,ST.[QWHBGRP]\n"
                + "      ,GR.[QUSEDT]\n"
                + "      ,GR.[QUSCDT]\n"
                + "      ,GR.[QUSUSER]\n"
                + "  FROM [RMShipment].[dbo].[QUSPDGRP] GR\n"
                + "  LEFT JOIN [RMShipment].[dbo].[QWHSTR] ST ON GR.[QUSPDGRP] = ST.[QWHMGRP]\n"
                + "  WHERE GR.[QUSUSERID] = ? AND GR.[QUSPDGRP] = ?";

        QUSPDGRP p = new QUSPDGRP();

        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            ps.setString(1, uid);
            ps.setString(2, pid);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                p.setQUSPDGRP(result.getString("QUSPDGRP"));
                p.setQWHBGRP(result.getString("QWHBGRP"));
                p.setQUSUSER(result.getString("QUSUSER"));

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return p;

    }

    public void deleteProdGrp(String uid, String pid) {

        String sql = "DELETE [RMShipment].[dbo].[QUSPDGRP] WHERE [QUSUSERID]= ? AND [QUSPDGRP] = ?";

        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            ps.setString(1, uid);
            ps.setString(2, pid);

            ps.executeUpdate();

        } catch (Exception e) {

            e.printStackTrace();

        }

    }
    
}
