/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.dao;

import com.twc.wms.database.database;

import java.sql.*;

import java.util.ArrayList;

import java.util.List;

import com.twc.wms.entity.XMSBRHEAD;
import com.twc.wms.entity.SAPMAS;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import com.twc.wms.database.MSSQLDbConnectionPool;
import com.twc.wms.entity.Qdest;

/**
 *
 * @author nutthawoot.noo
 */
public class XMSBRHEADDao extends database {

    public List<String> findDupQno(XMSBRHEAD head, String qno) {

        List<String> qnoList = new ArrayList<String>();

        String sql = "SELECT [XMSBRHQNO]\n"
                + "  FROM [RMShipment].[dbo].[XMSBRHEAD]\n"
                + "  WHERE FORMAT([XMSBRHSPDT],'yyyy-MM-dd') = '" + head.getXMSBRHSPDT() + "'\n"
                + "  AND [XMSBRHDEST] = '" + head.getXMSBRHDEST() + "'\n"
                + "  AND [XMSBRHROUND] = '" + head.getXMSBRHROUND() + "'\n"
                + "  AND [XMSBRHLICENNO] = '" + head.getXMSBRHLICENNO() + "'\n"
                + "  AND XMSBRHQNO != '" + qno + "'";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                String p = "";
                p = result.getString("XMSBRHQNO");
                qnoList.add(p);

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return qnoList;

    }

    public List<Qdest> findShipDate() {

        List<Qdest> objList = new ArrayList<Qdest>();

        String sql = "SELECT DISTINCT FORMAT([XMSOTHSPDT],'yyyy-MM-dd') AS CODE\n"
                + "  ,FORMAT([XMSOTHSPDT],'dd/MM/yyyy') AS DESCS\n"
                + "  FROM [RMShipment].[dbo].[XMSOTHEAD]\n"
                + "  ORDER BY CODE DESC";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                Qdest p = new Qdest();

                p.setCode(result.getString("CODE"));
                p.setDesc(result.getString("DESCS"));

                objList.add(p);

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return objList;

    }

    public List<Qdest> findRound(String shipDate) {

        List<Qdest> objList = new ArrayList<Qdest>();

        String sql = "SELECT DISTINCT [XMSOTHROUND]\n"
                + "  FROM [RMShipment].[dbo].[XMSOTHEAD]\n"
                + "  WHERE FORMAT(XMSOTHSPDT,'yyyy-MM-dd') = '" + shipDate + "'\n"
                + "  ORDER BY [XMSOTHROUND]";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                Qdest p = new Qdest();

                p.setCode(result.getString("XMSOTHROUND"));
//                p.setDesc(result.getString("QDEDESC"));

                objList.add(p);

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return objList;

    }

    public List<Qdest> findLino(String shipDate) {

        List<Qdest> objList = new ArrayList<Qdest>();

        String sql = "SELECT DISTINCT [XMSOTHLICENNO]\n"
                + "  FROM [RMShipment].[dbo].[XMSOTHEAD]\n"
                + "  LEFT JOIN [QDEST] ON [QDEST].[QDECOD] = [XMSOTHEAD].[XMSOTHDEST]\n"
                + "  WHERE FORMAT(XMSOTHSPDT,'yyyy-MM-dd') = '" + shipDate + "'\n"
                + "  AND [QDETYPE] = '2'\n"
                + "  ORDER BY [XMSOTHLICENNO]";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                Qdest p = new Qdest();

                p.setCode(result.getString("XMSOTHLICENNO"));
//                p.setDesc(result.getString("QDEDESC"));

                objList.add(p);

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return objList;

    }

    public boolean addDocno(String qno) {

        boolean result = false;

        String sql = "UPDATE [RMShipment].[dbo].[XMSBRHEAD]\n"
                + "SET [XMSBRHDOCNO] = (\n"
                + "                    SELECT FORMAT(CURRENT_TIMESTAMP,'yy')+'-'+FORMAT(CURRENT_TIMESTAMP,'MM')+'-'+FORMAT([QNBLAST]+1,'0000')\n"
                + "  FROM [QNBSER]\n"
                + "  WHERE [QNBTYPE] = 'LB'\n"
                + "  AND [QNBGROUP] = FORMAT(CURRENT_TIMESTAMP,'MM')\n"
                + "  AND [QNBYEAR] = FORMAT(CURRENT_TIMESTAMP,'yyyy')\n"
                + "					)\n"
                + "  WHERE [XMSBRHQNO] = '" + qno + "' AND XMSBRHDOCNO IS NULL ";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public XMSBRHEAD findByQno(String qno) {

        XMSBRHEAD p = new XMSBRHEAD();

        String sql = "SELECT FORMAT([XMSBRHSPDT],'yyyy-MM-dd') AS [XMSBRHSPDT]\n"
                + "      ,[XMSBRHDEST]\n"
                + "      ,[XMSBRHROUND]\n"
                + "      ,[XMSBRHLICENNO]\n"
                + "  FROM [RMShipment].[dbo].[XMSBRHEAD]\n"
                + "  WHERE [XMSBRHQNO] = '" + qno + "'";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                p.setXMSBRHSPDT(result.getString("XMSBRHSPDT"));
                p.setXMSBRHDEST(result.getString("XMSBRHDEST"));
                p.setXMSBRHROUND(result.getString("XMSBRHROUND"));
                p.setXMSBRHLICENNO(result.getString("XMSBRHLICENNO"));

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return p;

    }

    public String findMaxQno() {

        String p = "";

        String sql = "SELECT ISNULL(MAX(XMSBRHQNO),0)+1 AS MAXQNO FROM XMSBRHEAD ";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                p = result.getString("MAXQNO");

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return p;

    }

    public List<XMSBRHEAD> findAll(String shipDate) {

        List<XMSBRHEAD> UAList = new ArrayList<XMSBRHEAD>();

        String sql = "SELECT [XMSBRHQNO]\n"
                + "      ,[XMSBRHSPDT]\n"
                + "      ,[XMSBRHDEST] AS DEST\n"
                + "      ,[XMSBRHDEST]+' : '+[QDEDESC] AS [XMSBRHDEST]\n"
                + "      ,[XMSBRHROUND]\n"
                + "      ,[XMSBRHLICENNO]\n"
                + "      ,[XMSBRHDRIVER]\n"
                + "      ,[XMSBRHFOLLOWER]\n"
                + "      ,format([XMSBRHWH1],'#,#0') as [XMSBRHWH1]\n"
                + "      ,format([XMSBRHWH2],'#,#0') as [XMSBRHWH2]\n"
                + "      ,format([XMSBRHWH3],'#,#0') as [XMSBRHWH3]\n"
                + "      ,format([XMSBRHTOTWH],'#,#0') as [XMSBRHTOTWH]\n"
                + "      ,[XMSBRHUSER]+' : '+[USERS] as [XMSBRHUSER]\n"
                + "      ,[XMSBRHDOCNO]\n"
                + "  FROM [RMShipment].[dbo].[XMSBRHEAD]\n"
                + "  LEFT JOIN [QDEST] ON [QDEST].[QDECOD] = [XMSBRHEAD].[XMSBRHDEST]\n"
                + "  LEFT JOIN [MSSUSER] ON [MSSUSER].[USERID] = [XMSBRHEAD].[XMSBRHUSER]\n"
                + "  WHERE FORMAT([XMSBRHSPDT],'yyyy-MM-dd') = '" + shipDate + "'\n"
                + "UNION ALL\n"
                + "SELECT NULL,NULL,NULL,NULL,NULL,[XMSBRHLICENNO],'TOTAL : '+[XMSBRHLICENNO],NULL\n"
                + "	  ,[XMSBRHWH1]\n"
                + "      ,[XMSBRHWH2]\n"
                + "      ,[XMSBRHWH3]\n"
                + "      ,[XMSBRHTOTWH]\n"
                + "	  ,NULL,NULL\n"
                + "FROM(\n"
                + "SELECT [XMSBRHLICENNO]\n"
                + "	  ,format(SUM(isnull([XMSBRHWH1],0)),'#,#0') as [XMSBRHWH1]\n"
                + "      ,format(SUM(isnull([XMSBRHWH2],0)),'#,#0') as [XMSBRHWH2]\n"
                + "      ,format(SUM(isnull([XMSBRHWH3],0)),'#,#0') as [XMSBRHWH3]\n"
                + "      ,format(SUM(isnull([XMSBRHTOTWH],0)),'#,#0') as [XMSBRHTOTWH]\n"
                + "  FROM [RMShipment].[dbo].[XMSBRHEAD]\n"
                + "  LEFT JOIN [QDEST] ON [QDEST].[QDECOD] = [XMSBRHEAD].[XMSBRHDEST]\n"
                + "  LEFT JOIN [MSSUSER] ON [MSSUSER].[USERID] = [XMSBRHEAD].[XMSBRHUSER]\n"
                + "  WHERE FORMAT([XMSBRHSPDT],'yyyy-MM-dd') = '" + shipDate + "'\n"
                + "  GROUP BY [XMSBRHLICENNO]\n"
                + ")TMPTOT\n"
                + "ORDER BY [XMSBRHLICENNO],[XMSBRHQNO]";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                if (result.getString("XMSBRHDRIVER").contains("TOTAL")) {
                    XMSBRHEAD p = new XMSBRHEAD();

                    p.setXMSBRHQNO(result.getString("XMSBRHQNO"));
                    p.setXMSBRHSPDT(result.getString("XMSBRHSPDT"));
                    p.setXMSBRHDEST(result.getString("XMSBRHDEST"));
                    p.setXMSBRHROUND(result.getString("XMSBRHROUND"));
                    p.setXMSBRHLICENNO("<b style=\"opacity: 0;\">" + result.getString("XMSBRHLICENNO") + "</b>");
                    p.setXMSBRHDRIVER("<b style=\"color: #00399b;\">" + result.getString("XMSBRHDRIVER") + "</b>");
                    p.setXMSBRHFOLLOWER(result.getString("XMSBRHFOLLOWER"));
                    p.setXMSBRHWH1("<b style=\"color: #00399b;\">" + result.getString("XMSBRHWH1") + "</b>");
                    p.setXMSBRHWH2("<b style=\"color: #00399b;\">" + result.getString("XMSBRHWH2") + "</b>");
                    p.setXMSBRHWH3("<b style=\"color: #00399b;\">" + result.getString("XMSBRHWH3") + "</b>");
                    p.setXMSBRHTOTWH("<b style=\"color: #00399b;\">" + result.getString("XMSBRHTOTWH") + "</b>");
                    p.setXMSBRHUSER(result.getString("XMSBRHUSER"));

//                    p.setOptions("<a href=\"print?printBy=dest&wh=" + wh + "&shipDate=" + shipDate + "&dest=" + result.getString("DEST") + "\" target=\"_blank\"><i class=\"fa fa-print\" style=\"font-size:28px; padding-left: 10px; color: #c21b1b;\"></i></a>");
                    UAList.add(p);
                } else {
                    XMSBRHEAD p = new XMSBRHEAD();
                    p.setXMSBRHQNO(result.getString("XMSBRHQNO"));
                    p.setXMSBRHSPDT(result.getString("XMSBRHSPDT"));

                    if (result.getString("XMSBRHDEST") == null) {
                        if (result.getString("DEST").trim().equals("KPC")) {
                            p.setXMSBRHDEST("��Թ������ (E01,E02,B20,B21)");

                        } else if (result.getString("DEST").trim().equals("WSC")) {
                            p.setXMSBRHDEST("��������Ҫ� (E03,B22)");

                        } else if (result.getString("DEST").trim().equals("WLC")) {
                            p.setXMSBRHDEST("�����Ӿٹ (E04,B23)");

                        } else if (result.getString("DEST").trim().equals("MWC")) {
                            p.setXMSBRHDEST("���¹�������� (E05,B24)");

                        }
                    } else {
                        p.setXMSBRHDEST(result.getString("XMSBRHDEST"));
                    }

                    p.setXMSBRHROUND(result.getString("XMSBRHROUND"));
                    p.setXMSBRHLICENNO(result.getString("XMSBRHLICENNO"));
                    p.setXMSBRHDRIVER(result.getString("XMSBRHDRIVER"));
                    p.setXMSBRHFOLLOWER(result.getString("XMSBRHFOLLOWER"));
                    p.setXMSBRHWH1(result.getString("XMSBRHWH1"));
                    p.setXMSBRHWH2(result.getString("XMSBRHWH2"));
                    p.setXMSBRHWH3(result.getString("XMSBRHWH3"));
                    p.setXMSBRHTOTWH(result.getString("XMSBRHTOTWH"));
                    p.setXMSBRHUSER(result.getString("XMSBRHUSER"));
                    p.setXMSBRHDOCNO(result.getString("XMSBRHDOCNO"));

                    p.setOptions("<a href=\"edit?qno=" + result.getString("XMSBRHQNO") + "\"><i class=\"fa fa-edit\" style=\"font-size:28px; padding-left: 10px\"></i></a>\n"
                            + "<a onclick=\"document.getElementById('myModal-del-" + result.getString("XMSBRHQNO") + "').style.display = 'block';\" style=\"cursor: pointer;\"><i class=\"fa fa-trash\" style=\"font-size:28px; padding-left: 10px\"></i></a>\n"
                            + "<a href=\"print?qno=" + result.getString("XMSBRHQNO") + "\" target=\"_blank\"><i class=\"fa fa-print\" style=\"font-size:28px; padding-left: 10px\"></i></a>\n");

                    UAList.add(p);
                }

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public boolean edit(XMSBRHEAD head) {

        boolean result = false;

        String sql = "UPDATE XMSBRHEAD "
                + "SET [XMSBRHSPDT] = '" + head.getXMSBRHSPDT() + "'\n"
                + "      ,[XMSBRHDEST] = '" + head.getXMSBRHDEST() + "'\n"
                + "      ,[XMSBRHROUND] = '" + head.getXMSBRHROUND() + "'\n"
                + "      ,[XMSBRHLICENNO] = '" + head.getXMSBRHLICENNO() + "'\n"
                + "      ,[XMSBRHDRIVER] = '" + head.getXMSBRHDRIVER() + "'\n"
                + "      ,[XMSBRHFOLLOWER] = '" + head.getXMSBRHFOLLOWER() + "'\n"
                + "      ,[XMSBRHWH1] = '" + head.getXMSBRHWH1() + "'\n"
                + "      ,[XMSBRHWH2] = '" + head.getXMSBRHWH2() + "'\n"
                + "      ,[XMSBRHWH3] = '" + head.getXMSBRHWH3() + "'\n"
                + "      ,[XMSBRHTOTWH] = '" + head.getXMSBRHTOTWH() + "'\n"
                + "      ,[XMSBRHEDT] = CURRENT_TIMESTAMP\n"
                + "      ,[XMSBRHUSER] = '" + head.getXMSBRHUSER() + "'\n"
                + "WHERE [XMSBRHQNO] = '" + head.getXMSBRHQNO() + "'";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean add(XMSBRHEAD head) {

        boolean result = false;

        String sql = "INSERT INTO XMSBRHEAD "
                + "([XMSBRHCOM]\n"
                + "      ,[XMSBRHQNO]\n"
                + "      ,[XMSBRHSPDT]\n"
                + "      ,[XMSBRHDEST]\n"
                + "      ,[XMSBRHROUND]\n"
                + "      ,[XMSBRHLICENNO]\n"
                + "      ,[XMSBRHDRIVER]\n"
                + "      ,[XMSBRHFOLLOWER]\n"
                + "      ,[XMSBRHWH1]\n"
                + "      ,[XMSBRHWH2]\n"
                + "      ,[XMSBRHWH3]\n"
                + "      ,[XMSBRHTOTWH]\n"
                + "      ,[XMSBRHEDT]\n"
                + "      ,[XMSBRHCDT]\n"
                + "      ,[XMSBRHUSER]) "
                + "VALUES('TWC'"
                + ",'" + head.getXMSBRHQNO() + "'"
                + ",'" + head.getXMSBRHSPDT() + "'"
                + ",'" + head.getXMSBRHDEST() + "'"
                + ",'" + head.getXMSBRHROUND() + "'"
                + ",'" + head.getXMSBRHLICENNO() + "'"
                + ",'" + head.getXMSBRHDRIVER() + "'"
                + ",'" + head.getXMSBRHFOLLOWER() + "'"
                + ",'" + head.getXMSBRHWH1() + "'"
                + ",'" + head.getXMSBRHWH2() + "'"
                + ",'" + head.getXMSBRHWH3() + "'"
                + ",'" + head.getXMSBRHTOTWH() + "'"
                + ",CURRENT_TIMESTAMP"
                + ",CURRENT_TIMESTAMP"
                + ",'" + head.getXMSBRHUSER() + "') ";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public void delete(String qno) {

        String sql = "DELETE FROM XMSBRHEAD WHERE XMSBRHQNO = '" + qno + "'";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ps.executeUpdate();

        } catch (Exception e) {

            e.printStackTrace();

        }

    }
}
