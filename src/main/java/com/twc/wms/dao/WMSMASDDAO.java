/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.dao;

import com.twc.wms.database.database;

import java.sql.*;

import java.util.ArrayList;

import java.util.List;

import com.twc.wms.entity.MSSMATN;
import com.twc.wms.entity.SAPMAS;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import com.twc.wms.database.MSSQLDbConnectionPool;
import com.twc.wms.entity.WMSMASD;
import com.twc.wms.entity.WMSMASH;

/**
 *
 * @author nutthawoot.noo
 */
public class WMSMASDDAO extends database {

    public void deleteDetail(WMSMASD d) {

        String sql = "delete from WMSMASD where VBELN=? and POSNR=? and MATNR=?";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ps.setString(1, d.getVBELN());
            ps.setString(2, d.getPOSNR());
            ps.setString(3, d.getMATNR());

            ps.executeUpdate();

        } catch (Exception e) {

            e.printStackTrace();

        }

    }

    public boolean update(WMSMASD d) {

        boolean result = false;

        String sql = "update WMSMASD "
                + "set KWMENG='" + d.getKWMENG() + "' "
                + "where VBELN='" + d.getVBELN() + "' and POSNR='" + d.getPOSNR() + "' and MATNR='" + d.getMATNR() + "'";
        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public String findLineIssueNo(WMSMASD d) {

        String is = "0";

        String sql = "select ISSUENO from WMSMASD where VBELN='" + d.getVBELN() + "' and POSNR='" + d.getPOSNR() + "' and MATNR='" + d.getMATNR() + "'";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                if (result.getString("ISSUENO") != null) {
                    is = result.getString("ISSUENO");
                }

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return is;

    }

    public boolean addFact(WMSMASD D, float paramFloat, String paramString, java.util.Date paramDate, int paramInt) {

        boolean result = false;

        String sql = "insert into WMSMASF"
                + "(LINETYP, VBELN, POSNR, MATNR, WERKS, BWTAR, LGORT, "
                + "KWMENG, ISSUENO, NKWMENG, STATUS, DATE) "
                + "values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ps.setString(1, D.getLINETYP());
            ps.setString(2, D.getVBELN());
            ps.setString(3, D.getPOSNR());
            ps.setString(4, D.getMATNR());
            ps.setString(5, D.getWERKS());
            ps.setString(6, D.getBWTAR());
            ps.setString(7, D.getLGORT());
            ps.setFloat(8, paramFloat);
            ps.setInt(9, paramInt);
            ps.setString(10, D.getKWMENG());
            ps.setString(11, paramString);
            ps.setDate(12, new java.sql.Date(paramDate.getTime()));

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public String findKWMENG(WMSMASD d) {

        String is = "0";

        String sql = "select KWMENG from WMSMASD where VBELN='" + d.getVBELN() + "' and POSNR='" + d.getPOSNR() + "' and MATNR='" + d.getMATNR() + "'";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                if (result.getString("KWMENG") != null) {
                    is = result.getString("KWMENG");
                }

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return is;

    }

    public String findISSUENO(String id) {

        String is = "0";

        String sql = "SELECT DISTINCT MAX(ISSUENO) AS ISSUENO FROM WMSMASD WHERE VBELN = '" + id + "'";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                if (result.getString("ISSUENO") != null) {
                    is = result.getString("ISSUENO");
                }

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return is;

    }

    public List<MSSMATN> findAll() {

        List<MSSMATN> UAList = new ArrayList<MSSMATN>();

        String sql = "SELECT * FROM MSSMATN "
                + "INNER JOIN QWHMAS ON MSSMATN.MSWHSE=QWHMAS.QWHCOD "
                + "ORDER BY LGPBE ";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                MSSMATN p = new MSSMATN();

                p.setUid(result.getString("LGPBE"));

                p.setName(result.getString("MATCNAME"));

                p.setWarehouse(result.getString("MSWHSE") + " : " + result.getString("QWHNAME"));

                UAList.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public String check(WMSMASD d) {
        PreparedStatement ps = null;
        ResultSet rs = null;
        String STRRETURN = null;
        try {
            String check = "SELECT * FROM WMSMASD WHERE VBELN='" + d.getVBELN() + "' AND POSNR='" + d.getPOSNR() + "' AND MATNR='" + d.getMATNR() + "'";
            ps = connect.prepareStatement(check);
            rs = ps.executeQuery();
            if (rs.next()) {
                STRRETURN = "f";
            } else {
                STRRETURN = "t";
            }
            connect.close();

        } catch (Exception exp) {
            exp.printStackTrace();
        }
        return STRRETURN;
    }

    public boolean add(WMSMASD d) {

        boolean result = false;

        String sql = "insert into WMSMASD"
                + "(LINETYP, VBELN, POSNR, MATNR, WERKS, BWTAR, LGORT, KWMENG, ISSUENO) "
                + "values (?, ?, ?, ?, ?, ?, ?, ?, ?)";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ps.setString(1, d.getLINETYP());
            ps.setString(2, d.getVBELN());
            ps.setString(3, d.getPOSNR());
            ps.setString(4, d.getMATNR());
            ps.setString(5, d.getWERKS());
            ps.setString(6, d.getBWTAR());
            ps.setString(7, d.getLGORT());
            ps.setFloat(8, Float.parseFloat(d.getKWMENG()));
            ps.setInt(9, Integer.parseInt(d.getISSUENO()));

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public MSSMATN findByUid(String id) {

        MSSMATN ua = new MSSMATN();

        String sql = "SELECT * FROM MSSMATN WHERE LGPBE = ?";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ps.setString(1, id);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                ua.setUid(result.getString("LGPBE"));
                ua.setName(result.getString("MATCNAME"));
                ua.setWarehouse(result.getString("MSWHSE"));

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return ua;

    }

    public boolean edit(MSSMATN ua) {

        boolean result = false;

        String sql = "UPDATE MSSMATN "
                + "SET MSWHSE = ?, MATCNAME = ?"
                + " WHERE LGPBE = ?";
        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ps.setString(3, ua.getUid());
            ps.setString(1, ua.getWarehouse());
            ps.setString(2, ua.getName());

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public void delete(String uid) {

        String sql = "DELETE FROM MSSMATN WHERE LGPBE = ?";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ps.setString(1, uid);

            ps.executeUpdate();

        } catch (Exception e) {

            e.printStackTrace();

        }

    }
}
