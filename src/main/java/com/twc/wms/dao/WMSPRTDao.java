/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.dao;

import com.twc.wms.database.MSSQLDbConnectionPool;
import com.twc.wms.database.database;
import java.sql.*;

import java.util.ArrayList;

import java.util.List;

import com.twc.wms.entity.PGMU;
import com.twc.wms.entity.WMS910PRT;

import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author nutthawoot.noo
 */
public class WMSPRTDao extends database {

    public List<WMS910PRT> showWMS910(String warehouse, String onhandDate, String matCtrl, String action) {

        List<WMS910PRT> UAList = new ArrayList<WMS910PRT>();

        String ACTION = "";

        if (action.equals("NotEqual")) {
            ACTION = "WHERE QCKDIFF != 0\n";
        }

        String[] date = onhandDate.split("/");
        String dateOn = date[2] + date[1] + date[0];
        int no = 0;

        Statement stmtCre = null;

        String sqlCre = "CREATE TABLE #tempCHKCNT \n"
                + "(\n"
                + "    CHKWH NVARCHAR(3) COLLATE Thai_CI_AS,\n"
                + "	CHKMATCTRL NVARCHAR(4) COLLATE Thai_CI_AS,\n"
                + "	CHKONHDATE NVARCHAR(8) COLLATE Thai_CI_AS,\n"
                + "	CHKCNT DECIMAL(5, 0)\n"
                + ")\n"
                + "\n"
                + "INSERT INTO #tempCHKCNT\n"
                + "SELECT CHKWH,CHKMATCTRL,FORMAT([CHKONHDATE],'yyyyMMdd') AS CHKONHDATE,ISNULL(COUNT([CHKID]),0) AS CHKCNT FROM [TABCHK]\n"
                + "WHERE [CHKWH] = '" + warehouse + "' AND [CHKMATCTRL] = '" + matCtrl + "'\n"
                + "AND FORMAT([CHKONHDATE],'yyyyMMdd') = '" + dateOn + "'\n"
                + "GROUP BY CHKWH,CHKMATCTRL,CHKONHDATE";

//        System.out.println(sqlCre);
        try {
            stmtCre = connect.createStatement();
            stmtCre.executeUpdate(sqlCre);
        } catch (SQLException ex) {
            Logger.getLogger(WMSPRTDao.class.getName()).log(Level.SEVERE, null, ex);
        }

        String sql = "SELECT SAPWHS,SAPWHSDESC,SAPMTCTRL,SAPMCTRLDESC,SAPPLANT,SAPLOCATION,SAPMAT,SAPDESC,SAPVAL,SAPBUN\n"
                + "	,FORMAT(SAPPUNRES,'#,#0.000') AS SAPPUNRES\n"
                + "	,FORMAT(SAPQI,'#,#0.000') AS SAPQI\n"
                + "	,FORMAT(QI,'#,#0.000') AS QI\n"
                + "	,FORMAT(SAPAVG,'#,#0.00') AS SAPAVG\n"
                + "	,FORMAT(SAPAMOUNT,'#,#0.00') AS SAPAMOUNT\n"
                + "	,FORMAT(SAPTOTAL,'#,#0.000') AS SAPTOTAL\n"
                + "	,FORMAT(CHKQTY,'#,#0.000') AS QCKSUM\n"
                + "	,FORMAT(DIFFMISS,'#,#0.000') AS DIFFMISS\n"
                + "	,FORMAT(DIFFOVER,'#,#0.000') AS DIFFOVER\n"
                + "	,FORMAT([SAPAVG]*DIFFMISS,'#,#0.00') AS AMTMISS \n"
                + "	,FORMAT([SAPAVG]*DIFFOVER,'#,#0.00') AS AMTOVER \n"
                + "	,FORMAT(CHKQTY*SAPAVG,'#,#0.00') AS WMS_AMT\n"
                + "	,(SELECT TOP 1 CHKCNT FROM #tempCHKCNT AS TEMP WHERE TEMP.CHKWH = SAPWHS AND TEMP.CHKMATCTRL = SAPMTCTRL AND TEMP.CHKONHDATE = SAPTRDT) AS PACKPCS\n"
                + "	,(SELECT ISNULL(COUNT([CHKID]),0)\n"
                + "	  FROM [TABCHK]\n"
                + "	  WHERE [CHKWH] = SAPWHS\n"
                + "	  AND [CHKMATCTRL] = SAPMTCTRL\n"
                + "	  AND [CHKMATCODE] = SAPMAT\n"
                + "	  AND FORMAT([CHKONHDATE],'yyyyMMdd') = SAPTRDT) AS PACKPCS_COR \n"
                + "\n"
                + "	FROM(\n"
                + "\n"
                + "	SELECT SAPWHS, (SELECT TOP 1 QWHNAME FROM QWHMAS WHERE QWHCOD = SAPWHS) AS SAPWHSDESC, SAPTRDT, SAPMTCTRL, SAPONH.SAPMCTRLDESC, SAPONH.SAPMAT AS SAPMAT\n"
                + "		,(SELECT TOP 1 ISNULL(SAPDESC,'') FROM SAPMAS WHERE SAPMAS.SAPMAT = SAPONH.SAPMAT OR SAPMAS.SAPMAT = QCKSTK.QCKMAT ) AS SAPDESC, SAPTOTAL AS SAPTOTAL, QCKWHCD, QCKOHDT, QCKMTCTRL, QCKMAT, QCKCKDT\n"
                + "		,QCKSAP,ISNULL(QCKSUM,0) AS CHKQTY, ISNULL(SAPTOTAL,0) - ISNULL(QCKSUM,0) AS QCKDIFF\n"
                + "		,'00' AS SAPVAL\n"
                + "		, SAPONH.SAPBUN\n"
                + "		,SAPONH.SAPPLANT, SAPONH.SAPLOCATION, SAPONH.[SAPPUNRES] AS SAPPUNRES, (ISNULL(SAPONH.[SAPPUNRES],0) + ISNULL(SAPONH.[SAPQI],0)) AS [SAPQI]\n"
                + "		,ISNULL(SAPONH.[SAPQI],0) AS [QI]\n"
                + "		,SAPONH.[SAPAVG] AS SAPAVG, SAPONH.[SAPAMOUNT] AS SAPAMOUNT\n"
                + "		,CASE WHEN (ISNULL(SAPTOTAL,0) - ISNULL(QCKSUM,0)) > 0 THEN (ISNULL(SAPTOTAL,0) - ISNULL(QCKSUM,0)) ELSE 0 END AS DIFFMISS\n"
                + "		,CASE WHEN (ISNULL(SAPTOTAL,0) - ISNULL(QCKSUM,0)) < 0 THEN ABS(ISNULL(SAPTOTAL,0) - ISNULL(QCKSUM,0)) ELSE 0 END AS DIFFOVER\n"
                + "\n"
                + "	FROM SAPONH AS SAPONH  \n"
                + "		FULL JOIN QCKSTK AS QCKSTK ON SAPONH.SAPMAT = QCKSTK.QCKMAT AND SAPONH.SAPMTCTRL = QCKSTK.QCKMTCTRL AND SAPONH.SAPWHS = QCKSTK.QCKWHCD AND SAPTRDT = convert(varchar,QCKOHDT, 112) \n"
                + "		--LEFT JOIN SAPMAS AS SAPMAS ON (SAPMAS.SAPMAT = SAPONH.SAPMAT OR SAPMAS.SAPMAT = QCKSTK.QCKMAT)\n"
                + "			/*AND SAPMAS.SAPPLANT = SAPONH.SAPPLANT AND SAPMAS.SAPVAL = SAPONH.SAPVAL AND SAPMAS.SAPLOCATION = SAPONH.SAPLOCATION*/\n"
                + "		FULL OUTER JOIN QRMMAS AS QMAS ON QMAS.QRMCODE = SAPONH.SAPMAT AND QMAS.QRMVAL = SAPONH.SAPVAL AND QMAS.QRMSTS = '2'\n"
                + "\n"
                + "	WHERE ((SAPMTCTRL = ? AND SAPWHS = ? AND SAPTRDT = ?) OR (QCKMTCTRL = ? AND QCKWHCD = ? AND convert(varchar,QCKOHDT, 103) = ?)) \n"
                + "	AND (SAPONH.SAPVAL = '' OR QMAS.QRMVAL = '' OR SAPONH.SAPVAL = '00' OR QMAS.QRMVAL = '00' OR SAPONH.SAPVAL = '0' OR QMAS.QRMVAL = '0') --and SAPONH.SAPMAT = '1FNY0154BUH'\n"
                + "	GROUP BY SAPONH.SAPWHS,SAPONH.SAPTRDT,SAPONH.SAPMTCTRL,SAPONH.SAPMCTRLDESC,SAPONH.SAPMAT/*,SAPMAS.SAPDESC*/,SAPONH.SAPTOTAL,QCKSTK.QCKWHCD,QCKSTK.QCKOHDT\n"
                + "	,QCKSTK.QCKMTCTRL,QCKSUM,QCKSTK.QCKMAT,QCKSTK.QCKCKDT,QCKSTK.QCKSAP,SAPONH.SAPBUN,SAPONH.SAPPLANT,SAPONH.SAPLOCATION,SAPONH.SAPPUNRES\n"
                + "	,SAPONH.SAPQI,SAPONH.SAPAVG,SAPONH.SAPAMOUNT,QMAS.QRMVAL,SAPONH.SAPVAL\n"
                + "\n"
                + "	UNION ALL\n"
                + "\n"
                + "	SELECT SAPWHS, (SELECT TOP 1 QWHNAME FROM QWHMAS WHERE QWHCOD = SAPWHS) AS SAPWHSDESC, SAPTRDT, SAPMTCTRL, SAPONH.SAPMCTRLDESC, SAPONH.SAPMAT AS SAPMAT\n"
                + "		,(SELECT TOP 1 ISNULL(SAPDESC,'') FROM SAPMAS WHERE SAPMAS.SAPMAT = SAPONH.SAPMAT OR SAPMAS.SAPMAT = QCKSTK.QCKMAT ) AS SAPDESC, SAPTOTAL AS SAPTOTAL, QCKWHCD, QCKOHDT, QCKMTCTRL, QCKMAT, QCKCKDT\n"
                + "		,QCKSAP,ISNULL(QCKSUM,0) AS CHKQTY, ISNULL(SAPTOTAL,0) - ISNULL(QCKSUM,0) AS QCKDIFF\n"
                + "		,'01' AS SAPVAL\n"
                + "		, SAPONH.SAPBUN\n"
                + "		,SAPONH.SAPPLANT, SAPONH.SAPLOCATION, SAPONH.[SAPPUNRES] AS SAPPUNRES, (ISNULL(SAPONH.[SAPPUNRES],0) + ISNULL(SAPONH.[SAPQI],0)) AS [SAPQI]\n"
                + "		,ISNULL(SAPONH.[SAPQI],0) AS [QI]\n"
                + "		,SAPONH.[SAPAVG] AS SAPAVG, SAPONH.[SAPAMOUNT] AS SAPAMOUNT\n"
                + "		,CASE WHEN (ISNULL(SAPTOTAL,0) - ISNULL(QCKSUM,0)) > 0 THEN (ISNULL(SAPTOTAL,0) - ISNULL(QCKSUM,0)) ELSE 0 END AS DIFFMISS\n"
                + "		,CASE WHEN (ISNULL(SAPTOTAL,0) - ISNULL(QCKSUM,0)) < 0 THEN ABS(ISNULL(SAPTOTAL,0) - ISNULL(QCKSUM,0)) ELSE 0 END AS DIFFOVER\n"
                + "\n"
                + "	FROM SAPONH AS SAPONH  \n"
                + "		FULL JOIN QCKSTK AS QCKSTK ON SAPONH.SAPMAT = QCKSTK.QCKMAT AND SAPONH.SAPMTCTRL = QCKSTK.QCKMTCTRL AND SAPONH.SAPWHS = QCKSTK.QCKWHCD AND SAPTRDT = convert(varchar,QCKOHDT, 112) \n"
                + "		--LEFT JOIN SAPMAS AS SAPMAS ON (SAPMAS.SAPMAT = SAPONH.SAPMAT OR SAPMAS.SAPMAT = QCKSTK.QCKMAT)\n"
                + "			/*AND SAPMAS.SAPPLANT = SAPONH.SAPPLANT AND SAPMAS.SAPVAL = SAPONH.SAPVAL AND SAPMAS.SAPLOCATION = SAPONH.SAPLOCATION*/\n"
                + "		FULL OUTER JOIN QRMMAS AS QMAS ON QMAS.QRMCODE = SAPONH.SAPMAT AND QMAS.QRMVAL = SAPONH.SAPVAL AND QMAS.QRMSTS = '2'\n"
                + "\n"
                + "	WHERE ((SAPMTCTRL = ? AND SAPWHS = ? AND SAPTRDT = ?) OR (QCKMTCTRL = ? AND QCKWHCD = ? AND convert(varchar,QCKOHDT, 103) = ?)) \n"
                + "	AND (SAPONH.SAPVAL = '01' OR QMAS.QRMVAL = '01' OR SAPONH.SAPVAL = '1' OR QMAS.QRMVAL = '1') --and SAPONH.SAPMAT = '1FNY0154BUH'\n"
                + "	GROUP BY SAPONH.SAPWHS,SAPONH.SAPTRDT,SAPONH.SAPMTCTRL,SAPONH.SAPMCTRLDESC,SAPONH.SAPMAT/*,SAPMAS.SAPDESC*/,SAPONH.SAPTOTAL,QCKSTK.QCKWHCD,QCKSTK.QCKOHDT\n"
                + "	,QCKSTK.QCKMTCTRL,QCKSUM,QCKSTK.QCKMAT,QCKSTK.QCKCKDT,QCKSTK.QCKSAP,SAPONH.SAPBUN,SAPONH.SAPPLANT,SAPONH.SAPLOCATION,SAPONH.SAPPUNRES\n"
                + "	,SAPONH.SAPQI,SAPONH.SAPAVG,SAPONH.SAPAMOUNT,QMAS.QRMVAL,SAPONH.SAPVAL\n"
                + "\n"
                + "    UNION ALL\n"
                + "\n"
                + "	\n"
                + "	SELECT SAPWHS, (SELECT TOP 1 QWHNAME FROM QWHMAS WHERE QWHCOD = SAPWHS) AS SAPWHSDESC, SAPTRDT, SAPMTCTRL, SAPONH.SAPMCTRLDESC, SAPONH.SAPMAT AS SAPMAT\n"
                + "		,(SELECT TOP 1 ISNULL(SAPDESC,'') FROM SAPMAS WHERE SAPMAS.SAPMAT = SAPONH.SAPMAT OR SAPMAS.SAPMAT = QCKSTK.QCKMAT ) AS SAPDESC, SAPTOTAL AS SAPTOTAL, QCKWHCD, QCKOHDT, QCKMTCTRL, QCKMAT, QCKCKDT\n"
                + "		,QCKSAP,ISNULL(QCKSUM,0) AS CHKQTY, ISNULL(SAPTOTAL,0) - ISNULL(QCKSUM,0) AS QCKDIFF\n"
                + "		,'03' AS SAPVAL\n"
                + "		, SAPONH.SAPBUN\n"
                + "		,SAPONH.SAPPLANT, SAPONH.SAPLOCATION, SAPONH.[SAPPUNRES] AS SAPPUNRES, (ISNULL(SAPONH.[SAPPUNRES],0) + ISNULL(SAPONH.[SAPQI],0)) AS [SAPQI]\n"
                + "		,ISNULL(SAPONH.[SAPQI],0) AS [QI]\n"
                + "		,SAPONH.[SAPAVG] AS SAPAVG, SAPONH.[SAPAMOUNT] AS SAPAMOUNT\n"
                + "		,CASE WHEN (ISNULL(SAPTOTAL,0) - ISNULL(QCKSUM,0)) > 0 THEN (ISNULL(SAPTOTAL,0) - ISNULL(QCKSUM,0)) ELSE 0 END AS DIFFMISS\n"
                + "		,CASE WHEN (ISNULL(SAPTOTAL,0) - ISNULL(QCKSUM,0)) < 0 THEN ABS(ISNULL(SAPTOTAL,0) - ISNULL(QCKSUM,0)) ELSE 0 END AS DIFFOVER\n"
                + "\n"
                + "	FROM SAPONH AS SAPONH  \n"
                + "		FULL JOIN QCKSTK AS QCKSTK ON SAPONH.SAPMAT = QCKSTK.QCKMAT AND SAPONH.SAPMTCTRL = QCKSTK.QCKMTCTRL AND SAPONH.SAPWHS = QCKSTK.QCKWHCD AND SAPTRDT = convert(varchar,QCKOHDT, 112) \n"
                + "		--LEFT JOIN SAPMAS AS SAPMAS ON (SAPMAS.SAPMAT = SAPONH.SAPMAT OR SAPMAS.SAPMAT = QCKSTK.QCKMAT)\n"
                + "			/*AND SAPMAS.SAPPLANT = SAPONH.SAPPLANT AND SAPMAS.SAPVAL = SAPONH.SAPVAL AND SAPMAS.SAPLOCATION = SAPONH.SAPLOCATION*/\n"
                + "		FULL OUTER JOIN QRMMAS AS QMAS ON QMAS.QRMCODE = SAPONH.SAPMAT AND QMAS.QRMVAL = SAPONH.SAPVAL AND QMAS.QRMSTS = '2'\n"
                + "\n"
                + "	WHERE ((SAPMTCTRL = ? AND SAPWHS = ? AND SAPTRDT = ?) OR (QCKMTCTRL = ? AND QCKWHCD = ? AND convert(varchar,QCKOHDT, 103) = ?)) \n"
                + "	AND (SAPONH.SAPVAL = '03' OR QMAS.QRMVAL = '03' OR SAPONH.SAPVAL = '3' OR QMAS.QRMVAL = '3') --and SAPONH.SAPMAT = '1FNY0154BUH'\n"
                + "	GROUP BY SAPONH.SAPWHS,SAPONH.SAPTRDT,SAPONH.SAPMTCTRL,SAPONH.SAPMCTRLDESC,SAPONH.SAPMAT/*,SAPMAS.SAPDESC*/,SAPONH.SAPTOTAL,QCKSTK.QCKWHCD,QCKSTK.QCKOHDT\n"
                + "	,QCKSTK.QCKMTCTRL,QCKSUM,QCKSTK.QCKMAT,QCKSTK.QCKCKDT,QCKSTK.QCKSAP,SAPONH.SAPBUN,SAPONH.SAPPLANT,SAPONH.SAPLOCATION,SAPONH.SAPPUNRES\n"
                + "	,SAPONH.SAPQI,SAPONH.SAPAVG,SAPONH.SAPAMOUNT,QMAS.QRMVAL,SAPONH.SAPVAL\n"
                + "\n"
                + ") CHECKSTOCK\n"
                + "\n"
                + ACTION
                + "ORDER BY CHECKSTOCK.SAPMAT,CHECKSTOCK.SAPVAL\n"
                + "\n"
                + "drop table #tempCHKCNT";

//        String sql = "SELECT SAPWHS,SAPWHSDESC,SAPMTCTRL,SAPMCTRLDESC,SAPPLANT,SAPLOCATION,SAPMAT,SAPDESC,SAPVAL,SAPBUN\n"
//                + "	,FORMAT(SAPPUNRES,'#,#0.000') AS SAPPUNRES\n"
//                + "	,FORMAT(SAPQI,'#,#0.000') AS SAPQI\n"
//                + "	,FORMAT(QI,'#,#0.000') AS QI\n"
//                + "	,FORMAT(SAPAVG,'#,#0.00') AS SAPAVG\n"
//                + "	,FORMAT(SAPAMOUNT,'#,#0.00') AS SAPAMOUNT\n"
//                + "	,FORMAT(SAPTOTAL,'#,#0.000') AS SAPTOTAL\n"
//                + "	,FORMAT(CHKQTY,'#,#0.000') AS QCKSUM\n"
//                + "	,FORMAT(DIFFMISS,'#,#0.000') AS DIFFMISS\n"
//                + "	,FORMAT(DIFFOVER,'#,#0.000') AS DIFFOVER\n"
//                + "	,FORMAT([SAPAVG]*DIFFMISS,'#,#0.00') AS AMTMISS \n"
//                + "	,FORMAT([SAPAVG]*DIFFOVER,'#,#0.00') AS AMTOVER \n"
//                + "	,FORMAT(CHKQTY*SAPAVG,'#,#0.00') AS WMS_AMT\n"
//                + "	,(SELECT TOP 1 CHKCNT FROM #tempCHKCNT AS TEMP WHERE TEMP.CHKWH = SAPWHS AND TEMP.CHKMATCTRL = SAPMTCTRL AND TEMP.CHKONHDATE = SAPTRDT) AS PACKPCS\n"
//                + "	,PACKPCS_COR\n"
//                + "\n"
//                + "	FROM(\n"
//                + "\n"
//                + "	SELECT SAPWHS, (SELECT TOP 1 QWHNAME FROM QWHMAS WHERE QWHCOD = SAPWHS) AS SAPWHSDESC, SAPTRDT, SAPMTCTRL, SAPONH.SAPMCTRLDESC, SAPONH.SAPMAT AS SAPMAT\n"
//                + "		,ISNULL(SAPDESC, '') AS SAPDESC, SAPTOTAL, QCKWHCD, QCKOHDT, QCKMTCTRL, QCKMAT, QCKCKDT\n"
//                + "		,QCKSAP,ISNULL(SUM(TBCK.CHKQTY),0) AS CHKQTY, ISNULL(SAPTOTAL,0) - ISNULL(SUM(TBCK.CHKQTY),0) AS QCKDIFF\n"
//                + "		,'' AS SAPVAL\n"
//                + "		, SAPONH.SAPBUN\n"
//                + "		,SAPONH.SAPPLANT, SAPONH.SAPLOCATION, SAPONH.[SAPPUNRES], (ISNULL(SAPONH.[SAPPUNRES],0) + ISNULL(SAPONH.[SAPQI],0)) AS [SAPQI]\n"
//                + "		,ISNULL(SAPONH.[SAPQI],0) AS [QI]\n"
//                + "		,SAPONH.[SAPAVG], SAPONH.[SAPAMOUNT]\n"
//                + "		,CASE WHEN (ISNULL(SAPTOTAL,0) - ISNULL(SUM(TBCK.CHKQTY),0)) > 0 THEN (ISNULL(SAPTOTAL,0) - ISNULL(SUM(TBCK.CHKQTY),0)) ELSE 0 END AS DIFFMISS\n"
//                + "		,CASE WHEN (ISNULL(SAPTOTAL,0) - ISNULL(SUM(TBCK.CHKQTY),0)) < 0 THEN ABS(ISNULL(SAPTOTAL,0) - ISNULL(SUM(TBCK.CHKQTY),0)) ELSE 0 END AS DIFFOVER\n"
//                + "		,ISNULL(COUNT(TBCK.CHKID),0) AS PACKPCS_COR\n"
//                + "	\n"
//                + "	FROM SAPONH AS SAPONH  \n"
//                + "		FULL JOIN QCKSTK AS QCKSTK ON SAPONH.SAPMAT = QCKSTK.QCKMAT AND SAPONH.SAPMTCTRL = QCKSTK.QCKMTCTRL AND SAPONH.SAPWHS = QCKSTK.QCKWHCD AND SAPTRDT = convert(varchar,QCKOHDT, 112) \n"
//                + "		LEFT JOIN SAPMAS AS SAPMAS ON (SAPMAS.SAPMAT = SAPONH.SAPMAT OR SAPMAS.SAPMAT = QCKSTK.QCKMAT)\n"
//                + "			AND SAPMAS.SAPPLANT = SAPONH.SAPPLANT AND SAPMAS.SAPVAL = SAPONH.SAPVAL AND SAPMAS.SAPLOCATION = SAPONH.SAPLOCATION\n"
//                + "		FULL OUTER JOIN QRMMAS AS QMAS ON QMAS.QRMCODE = SAPONH.SAPMAT /*AND QMAS.QRMVAL = SAPONH.SAPVAL*/ AND QMAS.QRMSTS = '2'\n"
//                + "		LEFT JOIN TABCHK AS TBCK ON TBCK.CHKWH = QMAS.QRMWHSE AND TBCK.CHKMATCODE = QMAS.QRMCODE AND convert(varchar,TBCK.CHKONHDATE, 103) = ? AND QMAS.QRMID = TBCK.CHKID AND SAPONH.SAPMTCTRL = TBCK.CHKMATCTRL\n"
//                + "\n"
//                + "	WHERE ((SAPMTCTRL = ? AND SAPWHS = ? AND SAPTRDT = ?) OR (QCKMTCTRL = ? AND QCKWHCD = ? AND convert(varchar,QCKOHDT, 103) = ?)) \n"
//                + "	AND (SAPONH.SAPVAL = '' OR QMAS.QRMVAL = '')\n"
//                + "	GROUP BY SAPONH.SAPWHS,SAPONH.SAPTRDT,SAPONH.SAPMTCTRL,SAPONH.SAPMCTRLDESC,SAPONH.SAPMAT,SAPMAS.SAPDESC,SAPONH.SAPTOTAL,QCKSTK.QCKWHCD,QCKSTK.QCKOHDT\n"
//                + "	,QCKSTK.QCKMTCTRL,QCKSTK.QCKMAT,QCKSTK.QCKCKDT,QCKSTK.QCKSAP,SAPONH.SAPBUN,SAPONH.SAPPLANT,SAPONH.SAPLOCATION,SAPONH.SAPPUNRES\n"
//                + "	,SAPONH.SAPQI,SAPONH.SAPAVG,SAPONH.SAPAMOUNT,QMAS.QRMVAL,SAPONH.SAPVAL\n"
//                + "\n"
//                + "	UNION ALL\n"
//                + "\n"
//                + "	SELECT SAPWHS, (SELECT TOP 1 QWHNAME FROM QWHMAS WHERE QWHCOD = SAPWHS) AS SAPWHSDESC, SAPTRDT, SAPMTCTRL, SAPONH.SAPMCTRLDESC, SAPONH.SAPMAT AS SAPMAT\n"
//                + "		,ISNULL(SAPDESC, '') AS SAPDESC, SAPTOTAL, QCKWHCD, QCKOHDT, QCKMTCTRL, QCKMAT, QCKCKDT\n"
//                + "		,QCKSAP,ISNULL(SUM(TBCK.CHKQTY),0) AS CHKQTY, ISNULL(SAPTOTAL,0) - ISNULL(SUM(TBCK.CHKQTY),0) AS QCKDIFF\n"
//                + "		,'01' AS SAPVAL\n"
//                + "		, SAPONH.SAPBUN\n"
//                + "		,SAPONH.SAPPLANT, SAPONH.SAPLOCATION, SAPONH.[SAPPUNRES], (ISNULL(SAPONH.[SAPPUNRES],0) + ISNULL(SAPONH.[SAPQI],0)) AS [SAPQI]\n"
//                + "		,ISNULL(SAPONH.[SAPQI],0) AS [QI]\n"
//                + "		,SAPONH.[SAPAVG], SAPONH.[SAPAMOUNT]\n"
//                + "		,CASE WHEN (ISNULL(SAPTOTAL,0) - ISNULL(SUM(TBCK.CHKQTY),0)) > 0 THEN (ISNULL(SAPTOTAL,0) - ISNULL(SUM(TBCK.CHKQTY),0)) ELSE 0 END AS DIFFMISS\n"
//                + "		,CASE WHEN (ISNULL(SAPTOTAL,0) - ISNULL(SUM(TBCK.CHKQTY),0)) < 0 THEN ABS(ISNULL(SAPTOTAL,0) - ISNULL(SUM(TBCK.CHKQTY),0)) ELSE 0 END AS DIFFOVER\n"
//                + "		,ISNULL(COUNT(TBCK.CHKID),0) AS PACKPCS_COR\n"
//                + "\n"
//                + "	FROM SAPONH AS SAPONH  \n"
//                + "		FULL JOIN QCKSTK AS QCKSTK ON SAPONH.SAPMAT = QCKSTK.QCKMAT AND SAPONH.SAPMTCTRL = QCKSTK.QCKMTCTRL AND SAPONH.SAPWHS = QCKSTK.QCKWHCD AND SAPTRDT = convert(varchar,QCKOHDT, 112) \n"
//                + "		LEFT JOIN SAPMAS AS SAPMAS ON (SAPMAS.SAPMAT = SAPONH.SAPMAT OR SAPMAS.SAPMAT = QCKSTK.QCKMAT)\n"
//                + "			AND SAPMAS.SAPPLANT = SAPONH.SAPPLANT AND SAPMAS.SAPVAL = SAPONH.SAPVAL AND SAPMAS.SAPLOCATION = SAPONH.SAPLOCATION\n"
//                + "		FULL OUTER JOIN QRMMAS AS QMAS ON QMAS.QRMCODE = SAPONH.SAPMAT AND QMAS.QRMVAL = SAPONH.SAPVAL AND QMAS.QRMSTS = '2'\n"
//                + "		LEFT JOIN TABCHK AS TBCK ON TBCK.CHKWH = QMAS.QRMWHSE AND TBCK.CHKMATCODE = QMAS.QRMCODE AND convert(varchar,TBCK.CHKONHDATE, 103) = ? AND QMAS.QRMID = TBCK.CHKID AND SAPONH.SAPMTCTRL = TBCK.CHKMATCTRL\n"
//                + "\n"
//                + "	WHERE ((SAPMTCTRL = ? AND SAPWHS = ? AND SAPTRDT = ?) OR (QCKMTCTRL = ? AND QCKWHCD = ? AND convert(varchar,QCKOHDT, 103) = ?)) \n"
//                + "	AND (SAPONH.SAPVAL = '01' OR QMAS.QRMVAL = '01')\n"
//                + "	GROUP BY SAPONH.SAPWHS,SAPONH.SAPTRDT,SAPONH.SAPMTCTRL,SAPONH.SAPMCTRLDESC,SAPONH.SAPMAT,SAPMAS.SAPDESC,SAPONH.SAPTOTAL,QCKSTK.QCKWHCD,QCKSTK.QCKOHDT\n"
//                + "	,QCKSTK.QCKMTCTRL,QCKSTK.QCKMAT,QCKSTK.QCKCKDT,QCKSTK.QCKSAP,SAPONH.SAPBUN,SAPONH.SAPPLANT,SAPONH.SAPLOCATION,SAPONH.SAPPUNRES\n"
//                + "	,SAPONH.SAPQI,SAPONH.SAPAVG,SAPONH.SAPAMOUNT,QMAS.QRMVAL,SAPONH.SAPVAL\n"
//                + "\n"
//                + "    UNION ALL\n"
//                + "\n"
//                + "	SELECT SAPWHS, (SELECT TOP 1 QWHNAME FROM QWHMAS WHERE QWHCOD = SAPWHS) AS SAPWHSDESC, SAPTRDT, SAPMTCTRL, SAPONH.SAPMCTRLDESC, SAPONH.SAPMAT AS SAPMAT\n"
//                + "		,ISNULL(SAPDESC, '') AS SAPDESC, SAPTOTAL, QCKWHCD, QCKOHDT, QCKMTCTRL, QCKMAT, QCKCKDT\n"
//                + "		,QCKSAP,ISNULL(SUM(TBCK.CHKQTY),0) AS CHKQTY, ISNULL(SAPTOTAL,0) - ISNULL(SUM(TBCK.CHKQTY),0) AS QCKDIFF\n"
//                + "		,'03' AS SAPVAL\n"
//                + "		, SAPONH.SAPBUN\n"
//                + "		,SAPONH.SAPPLANT, SAPONH.SAPLOCATION, SAPONH.[SAPPUNRES], (ISNULL(SAPONH.[SAPPUNRES],0) + ISNULL(SAPONH.[SAPQI],0)) AS [SAPQI]\n"
//                + "		,ISNULL(SAPONH.[SAPQI],0) AS [QI]\n"
//                + "		,SAPONH.[SAPAVG], SAPONH.[SAPAMOUNT]\n"
//                + "		,CASE WHEN (ISNULL(SAPTOTAL,0) - ISNULL(SUM(TBCK.CHKQTY),0)) > 0 THEN (ISNULL(SAPTOTAL,0) - ISNULL(SUM(TBCK.CHKQTY),0)) ELSE 0 END AS DIFFMISS\n"
//                + "		,CASE WHEN (ISNULL(SAPTOTAL,0) - ISNULL(SUM(TBCK.CHKQTY),0)) < 0 THEN ABS(ISNULL(SAPTOTAL,0) - ISNULL(SUM(TBCK.CHKQTY),0)) ELSE 0 END AS DIFFOVER\n"
//                + "		,ISNULL(COUNT(TBCK.CHKID),0) AS PACKPCS_COR\n"
//                + "	\n"
//                + "	FROM SAPONH AS SAPONH  \n"
//                + "		FULL JOIN QCKSTK AS QCKSTK ON SAPONH.SAPMAT = QCKSTK.QCKMAT AND SAPONH.SAPMTCTRL = QCKSTK.QCKMTCTRL AND SAPONH.SAPWHS = QCKSTK.QCKWHCD AND SAPTRDT = convert(varchar,QCKOHDT, 112) \n"
//                + "		LEFT JOIN SAPMAS AS SAPMAS ON (SAPMAS.SAPMAT = SAPONH.SAPMAT OR SAPMAS.SAPMAT = QCKSTK.QCKMAT)\n"
//                + "			AND SAPMAS.SAPPLANT = SAPONH.SAPPLANT AND SAPMAS.SAPVAL = SAPONH.SAPVAL AND SAPMAS.SAPLOCATION = SAPONH.SAPLOCATION\n"
//                + "		FULL OUTER JOIN QRMMAS AS QMAS ON QMAS.QRMCODE = SAPONH.SAPMAT AND QMAS.QRMVAL = SAPONH.SAPVAL AND QMAS.QRMSTS = '2'\n"
//                + "		LEFT JOIN TABCHK AS TBCK ON TBCK.CHKWH = QMAS.QRMWHSE AND TBCK.CHKMATCODE = QMAS.QRMCODE AND convert(varchar,TBCK.CHKONHDATE, 103) = ? AND QMAS.QRMID = TBCK.CHKID AND SAPONH.SAPMTCTRL = TBCK.CHKMATCTRL\n"
//                + "\n"
//                + "	WHERE ((SAPMTCTRL = ? AND SAPWHS = ? AND SAPTRDT = ?) OR (QCKMTCTRL = ? AND QCKWHCD = ? AND convert(varchar,QCKOHDT, 103) = ?)) \n"
//                + "	AND (SAPONH.SAPVAL = '03' OR QMAS.QRMVAL = '03')\n"
//                + "	GROUP BY SAPONH.SAPWHS,SAPONH.SAPTRDT,SAPONH.SAPMTCTRL,SAPONH.SAPMCTRLDESC,SAPONH.SAPMAT,SAPMAS.SAPDESC,SAPONH.SAPTOTAL,QCKSTK.QCKWHCD,QCKSTK.QCKOHDT\n"
//                + "	,QCKSTK.QCKMTCTRL,QCKSTK.QCKMAT,QCKSTK.QCKCKDT,QCKSTK.QCKSAP,SAPONH.SAPBUN,SAPONH.SAPPLANT,SAPONH.SAPLOCATION,SAPONH.SAPPUNRES\n"
//                + "	,SAPONH.SAPQI,SAPONH.SAPAVG,SAPONH.SAPAMOUNT,QMAS.QRMVAL,SAPONH.SAPVAL\n"
//                + "\n"
//                + ") CHECKSTOCK\n"
//                + "\n"
//                + ACTION
//                + "ORDER BY CHECKSTOCK.SAPMAT,CHECKSTOCK.SAPVAL\n"
//                + "\n"
//                + "drop table #tempCHKCNT";
//        String sqlold = "SELECT SAPWHS,SAPWHSDESC,SAPMTCTRL,SAPMCTRLDESC,SAPPLANT,SAPLOCATION,SAPMAT,SAPDESC,SAPVAL,SAPBUN\n"
//                + ",FORMAT(SAPPUNRES,'#,#0.000') AS SAPPUNRES\n"
//                + ",FORMAT(SAPQI,'#,#0.000') AS SAPQI\n"
//                + ",FORMAT(QI,'#,#0.000') AS QI\n"
//                + ",FORMAT(SAPAVG,'#,#0.00') AS SAPAVG\n"
//                + ",FORMAT(SAPAMOUNT,'#,#0.00') AS SAPAMOUNT\n"
//                + ",FORMAT(SAPTOTAL,'#,#0.000') AS SAPTOTAL\n"
//                + ",FORMAT(QCKSUM,'#,#0.000') AS QCKSUM\n"
//                + ",FORMAT(DIFFMISS,'#,#0.000') AS DIFFMISS\n"
//                + ",FORMAT(DIFFOVER,'#,#0.000') AS DIFFOVER\n"
//                + ",FORMAT([SAPAVG]*DIFFMISS,'#,#0.00') AS AMTMISS \n"
//                + ",FORMAT([SAPAVG]*DIFFOVER,'#,#0.00') AS AMTOVER \n"
//                + ",FORMAT(QCKSUM*SAPAVG,'#,#0.00') AS WMS_AMT \n"
//                + ",(SELECT ISNULL(COUNT([CHKID]),0)\n"
//                + "  FROM [TABCHK]\n"
//                + "  WHERE [CHKWH] = SAPWHS\n"
//                + "  AND [CHKMATCTRL] = SAPMTCTRL\n"
//                + "  AND FORMAT([CHKONHDATE],'yyyyMMdd') = SAPTRDT) AS PACKPCS \n"
//                + ",(SELECT ISNULL(COUNT([CHKID]),0)\n"
//                + "  FROM [TABCHK]\n"
//                + "  WHERE [CHKWH] = SAPWHS\n"
//                + "  AND [CHKMATCTRL] = SAPMTCTRL\n"
//                + "  AND [CHKMATCODE] = SAPMAT\n"
//                + "  AND FORMAT([CHKONHDATE],'yyyyMMdd') = SAPTRDT) AS PACKPCS_COR \n"
//                + "FROM(\n"
//                + "SELECT DISTINCT SAPWHS, (SELECT TOP 1 QWHNAME FROM QWHMAS WHERE QWHCOD = SAPWHS) AS SAPWHSDESC, SAPTRDT, SAPMTCTRL, SAPONH.SAPMCTRLDESC, SAPONH.SAPMAT AS SAPMAT\n"
//                + ",ISNULL(SAPDESC, '') AS SAPDESC, SAPTOTAL, QCKWHCD, QCKOHDT, QCKMTCTRL, QCKMAT, QCKCKDT\n"
//                + ",QCKSAP, QCKSUM, ISNULL(SAPTOTAL,0) - ISNULL(QCKSUM,0) AS QCKDIFF, SAPONH.SAPVAL, SAPONH.SAPBUN\n"
//                + ",SAPONH.SAPPLANT, SAPONH.SAPLOCATION, SAPONH.[SAPPUNRES], (ISNULL(SAPONH.[SAPPUNRES],0) + ISNULL(SAPONH.[SAPQI],0)) AS [SAPQI]\n"
//                + ",ISNULL(SAPONH.[SAPQI],0) AS [QI]\n"
//                + ",SAPONH.[SAPAVG], SAPONH.[SAPAMOUNT]\n"
//                + ",CASE WHEN (ISNULL(SAPTOTAL,0) - ISNULL(QCKSUM,0)) > 0 THEN (ISNULL(SAPTOTAL,0) - ISNULL(QCKSUM,0)) ELSE 0 END AS DIFFMISS\n"
//                + ",CASE WHEN (ISNULL(SAPTOTAL,0) - ISNULL(QCKSUM,0)) < 0 THEN ABS(ISNULL(SAPTOTAL,0) - ISNULL(QCKSUM,0)) ELSE 0 END AS DIFFOVER\n"
//                + "FROM SAPONH AS SAPONH  \n"
//                + "FULL JOIN QCKSTK AS QCKSTK ON SAPONH.SAPMAT = QCKSTK.QCKMAT AND SAPONH.SAPMTCTRL = QCKSTK.QCKMTCTRL AND SAPONH.SAPWHS = QCKSTK.QCKWHCD AND SAPTRDT = convert(varchar,QCKOHDT, 112) \n"
//                + "LEFT JOIN SAPMAS AS SAPMAS ON SAPMAS.SAPMAT = SAPONH.SAPMAT OR SAPMAS.SAPMAT = QCKSTK.QCKMAT \n"
//                + "WHERE (SAPMTCTRL = ? AND SAPWHS = ? AND SAPTRDT = ?) OR (QCKMTCTRL = ? AND QCKWHCD = ? AND convert(varchar,QCKOHDT, 103) = ?)\n"
//                + ")CHECKSTOCK\n"
//                + ACTION
//                + "ORDER BY CHECKSTOCK.SAPMAT ";
//        System.out.println(sql);
//        System.out.println("------");
//        System.out.println(sqlold);
        try {

            PreparedStatement ps = connect.prepareStatement(sql);
//            ps.setString(1, matCtrl);
//            ps.setString(2, warehouse);
//            ps.setString(3, dateOn);
//            ps.setString(4, matCtrl);
//            ps.setString(5, warehouse);
//            ps.setString(6, onhandDate);

            ps.setString(1, matCtrl);
            ps.setString(2, warehouse);
            ps.setString(3, dateOn);
            ps.setString(4, matCtrl);
            ps.setString(5, warehouse);
            ps.setString(6, onhandDate);

            ps.setString(7, matCtrl);
            ps.setString(8, warehouse);
            ps.setString(9, dateOn);
            ps.setString(10, matCtrl);
            ps.setString(11, warehouse);
            ps.setString(12, onhandDate);

            ps.setString(13, matCtrl);
            ps.setString(14, warehouse);
            ps.setString(15, dateOn);
            ps.setString(16, matCtrl);
            ps.setString(17, warehouse);
            ps.setString(18, onhandDate);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                WMS910PRT p = new WMS910PRT();

                p.setNO(Integer.toString(++no));
                p.setSAPWHS(result.getString("SAPWHS"));
                p.setSAPWHSDESC(result.getString("SAPWHSDESC"));
                p.setSAPMTCTRL(result.getString("SAPMTCTRL"));
                p.setSAPMCTRLDESC(result.getString("SAPMCTRLDESC"));
                p.setSAPPLANT(result.getString("SAPPLANT"));
                p.setSAPLOCATION(result.getString("SAPLOCATION"));
                p.setSAPMAT(result.getString("SAPMAT"));
                p.setSAPDESC(result.getString("SAPDESC"));
                p.setSAPVAL(result.getString("SAPVAL"));
                p.setSAPBUN(result.getString("SAPBUN"));
                p.setSAPPUNRES(result.getString("SAPPUNRES"));
                p.setSAPQI(result.getString("SAPQI"));
                p.setSAPAVG(result.getString("SAPAVG"));
                p.setSAPAMOUNT(result.getString("SAPAMOUNT"));
                p.setSAPTOTAL(result.getString("SAPTOTAL"));
                p.setQCKSUM(result.getString("QCKSUM"));
                p.setDIFFMISS(result.getString("DIFFMISS"));
                p.setDIFFOVER(result.getString("DIFFOVER"));
                p.setAMTMISS(result.getString("AMTMISS"));
                p.setAMTOVER(result.getString("AMTOVER"));
                p.setWMS_AMT(result.getString("WMS_AMT"));
                p.setQI(result.getString("QI"));
                p.setPACKPCS(result.getString("PACKPCS"));
                p.setPACKPCS_COR(result.getString("PACKPCS_COR"));

                if (result.getString("SAPVAL").equals("")) {
                    p.setThisRedLine("Red");
                } else {
                    p.setThisRedLine("");
                }

                UAList.add(p);

            }

            stmtCre.close();
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

}
