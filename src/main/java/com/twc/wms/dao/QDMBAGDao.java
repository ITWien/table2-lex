/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.dao;

import com.twc.wms.database.database;
import com.twc.wms.entity.QDESTYP;
import com.twc.wms.entity.QDMBAG;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 *
 * @author nutthawoot.noo
 */
public class QDMBAGDao extends database {

    public List<QDMBAG> findAll() {

        List<QDMBAG> QDMBAGList = new ArrayList<QDMBAG>();

        String sql = "SELECT [QDMSIZE]\n"
                + "      ,[QDMDESC]\n"
                + "      ,[QDMINWIDTH]\n"
                + "      ,[QDMINLENGTH]\n"
                + "      ,[QDMCMWIDTH]\n"
                + "      ,[QDMCMLENGTH]\n"
                + "      ,FORMAT([QDMCDT],'yyyy-MM-dd') AS [QDMCDT]\n"
                + "      ,[QDMUSER]+' : '+[USERS] AS [QDMUSER]\n"
                + "  FROM [RMShipment].[dbo].[QDMBAG]\n"
                + "  LEFT JOIN [MSSUSER] ON [MSSUSER].[USERID] = [QDMBAG].[QDMUSER]\n"
                + "  ORDER BY [QDMSIZE]";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            int it = 0;

            while (result.next()) {

                it++;

                QDMBAG p = new QDMBAG();

                p.setQDMSIZE(result.getString("QDMSIZE"));
                p.setQDMDESC(result.getString("QDMDESC") + "<input type=\"hidden\" id=\"DESC-" + result.getString("QDMSIZE") + "\" value=\"" + result.getString("QDMDESC").replace("\"", "''") + "\">");
                p.setQDMINWIDTH(result.getString("QDMINWIDTH"));
                p.setQDMINLENGTH(result.getString("QDMINLENGTH"));
                p.setQDMCMWIDTH(result.getString("QDMCMWIDTH") + "<input type=\"hidden\" id=\"WIDTH-" + result.getString("QDMSIZE") + "\" value=\"" + result.getString("QDMCMWIDTH") + "\">");
                p.setQDMCMLENGTH(result.getString("QDMCMLENGTH") + "<input type=\"hidden\" id=\"LENGTH-" + result.getString("QDMSIZE") + "\" value=\"" + result.getString("QDMCMLENGTH") + "\">");
                p.setQDMCDT(result.getString("QDMCDT"));

                String user = "";
                if (result.getString("QDMUSER") != null) {
                    user = result.getString("QDMUSER").split(" ")[0] + " : " + result.getString("QDMUSER").split(" ")[2];
                }
                p.setQDMUSER(user);

                String checked = "";
//                if (it == 1) {
//                    checked = "checked";
//                }

                p.setCheckBax("<input id=\"size-" + result.getString("QDMSIZE") + "\" name=\"size\" type=\"radio\" value=\"" + result.getString("QDMSIZE") + "\" style=\"width: 25px; height: 25px; cursor: pointer\" " + checked + ">");

                p.setOption("<a title=\"Edit\" style=\" cursor: pointer;\" data-toggle=\"modal\" data-target=\"#myModal-edit-" + result.getString("QDMSIZE") + "\"><i class=\"fa fa-edit\" aria-hidden=\"true\" style=\"font-size:25px;\"></i></a>\n"
                        + "<div id=\"myModal-edit-" + result.getString("QDMSIZE") + "\" class=\"modal fade\" role=\"dialog\">\n"
                        + "    <div class=\"modal-dialog\">\n"
                        + "        <!-- Modal content-->\n"
                        + "        <div class=\"modal-content\" style=\"width: auto; height: auto;\">\n"
                        + "            <div class=\"modal-header\" style=\" text-align: center;\">\n"
                        + "                <button type=\"button\" class=\"close\" data-toggle=\"modal\" data-target=\"#myModal-edit-" + result.getString("QDMSIZE") + "\">&times;</button>\n"
                        + "                <h4 class=\"modal-title\">Edit Mode</h4>\n"
                        + "            </div>\n"
                        + "            <div class=\"modal-body\" style=\" text-align: center;\">\n"
                        + "                <h4>Size : " + result.getString("QDMSIZE") + "</h4>\n"
                        + "                <h4>Desc : <input type=\"text\" id=\"edit-desc-" + result.getString("QDMSIZE") + "\" style=\"width: 100px;\" value=\"" + result.getString("QDMDESC").replace("\"", "''") + "\"></h4>\n"
                        + "                <h4>���ҧ (ૹ������) : <input type=\"text\" id=\"edit-inWidth-" + result.getString("QDMSIZE") + "\" style=\"text-align: right; width: 80px;\" value=\"" + result.getString("QDMCMWIDTH") + "\"></h4>\n"
                        + "                <h4>��� (ૹ������) : <input type=\"text\" id=\"edit-inLength-" + result.getString("QDMSIZE") + "\" style=\"text-align: right; width: 80px;\" value=\"" + result.getString("QDMCMLENGTH") + "\"></h4>\n"
                        + "            </div>\n"
                        + "            <div class=\"modal-footer\" style=\" text-align: center;\">\n"
                        + "                <button type=\"button\" class=\"btn btn-default\" data-toggle=\"modal\" data-target=\"#myModal-edit-" + result.getString("QDMSIZE") + "\">Cancel</button>\n"
                        + "                <button type=\"button\" class=\"btn btn-primary\" onclick=\"editBag('" + result.getString("QDMSIZE") + "');\">Confirm</button>\n"
                        + "            </div>\n"
                        + "        </div>\n"
                        + "    </div>\n"
                        + "</div>"
                        + "&nbsp;&nbsp;"
                        + "<a title=\"Delete\" style=\" cursor: pointer;\" data-toggle=\"modal\" data-target=\"#myModal-del-" + result.getString("QDMSIZE") + "\"><i class=\"fa fa-trash\" aria-hidden=\"true\" style=\"font-size:25px;\"></i></a>\n"
                        + "<div id=\"myModal-del-" + result.getString("QDMSIZE") + "\" class=\"modal fade\" role=\"dialog\">\n"
                        + "    <div class=\"modal-dialog\">\n"
                        + "        <!-- Modal content-->\n"
                        + "        <div class=\"modal-content\" style=\"width: auto; height: auto;\">\n"
                        + "            <div class=\"modal-header\" style=\" text-align: center;\">\n"
                        + "                <button type=\"button\" class=\"close\" data-toggle=\"modal\" data-target=\"#myModal-del-" + result.getString("QDMSIZE") + "\">&times;</button>\n"
                        + "                <h4 class=\"modal-title\">Confirm Delete</h4>\n"
                        + "            </div>\n"
                        + "            <div class=\"modal-body\" style=\" text-align: center;\">\n"
                        + "                <h4>Size : " + result.getString("QDMSIZE") + "</h4>\n"
                        + "                <h4>Desc : " + result.getString("QDMDESC") + "</h4>\n"
                        + "            </div>\n"
                        + "            <div class=\"modal-footer\" style=\" text-align: center;\">\n"
                        + "                <button type=\"button\" class=\"btn btn-default\" data-toggle=\"modal\" data-target=\"#myModal-del-" + result.getString("QDMSIZE") + "\">Cancel</button>\n"
                        + "                <button type=\"button\" class=\"btn btn-danger\" onclick=\"deleteBag('" + result.getString("QDMSIZE") + "');\">Delete</button>\n"
                        + "            </div>\n"
                        + "        </div>\n"
                        + "    </div>\n"
                        + "</div>"
                );

                QDMBAGList.add(p);

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return QDMBAGList;

    }

    public boolean add(String size, String desc, String inWidth, String inLength, String uid) {

        boolean result = false;

        if (uid.equals("null")) {
            uid = "";
        }

        String sql = "INSERT INTO [QDMBAG]"
                + " ([QDMCOM]\n"
                + "      ,[QDMSIZE]\n"
                + "      ,[QDMDESC]\n"
                + "      ,[QDMINWIDTH]\n"
                + "      ,[QDMINLENGTH]\n"
                + "      ,[QDMCMWIDTH]\n"
                + "      ,[QDMCMLENGTH]\n"
                + "      ,[QDMEDT]\n"
                + "      ,[QDMCDT]\n"
                + "      ,[QDMUSER])"
                + " VALUES('TWC'"
                + ",'" + size + "'"
                + ",'" + desc + "'"
                + "," + inWidth + "/2.54"
                + "," + inLength + "/2.54"
                + "," + inWidth
                + "," + inLength
                + ",CURRENT_TIMESTAMP"
                + ",CURRENT_TIMESTAMP"
                + ",'" + uid + "')";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean edit(String size, String desc, String inWidth, String inLength, String uid) {

        boolean result = false;

        if (uid.equals("null")) {
            uid = "";
        }

        String sql = "UPDATE [QDMBAG]\n"
                + "SET [QDMDESC] = '" + desc + "'"
                + "      ,[QDMINWIDTH] = " + inWidth + "/2.54"
                + "      ,[QDMINLENGTH] = " + inLength + "/2.54"
                + "      ,[QDMCMWIDTH] = " + inWidth
                + "      ,[QDMCMLENGTH] = " + inLength
                + "      ,[QDMEDT] = CURRENT_TIMESTAMP\n"
                + "      ,[QDMUSER] = '" + uid + "'"
                + "WHERE QDMSIZE = '" + size + "'";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public void delete(String size) {

        String sql = "DELETE FROM QDMBAG "
                + "WHERE QDMSIZE = '" + size + "'";

        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            ps.executeUpdate();

        } catch (Exception e) {

            e.printStackTrace();

        }

    }
}
