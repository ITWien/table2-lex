/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.dao;

import com.twc.wms.entity.SAPMAS_2;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import com.twc.wms.database.database;
import com.twc.wms.entity.QRMTRA_2;

/**
 *
 * @author nutthawoot.noo
 */
public class SAPMAS_2Dao extends database {

    public SAPMAS_2 findAll(String mc) {

        SAPMAS_2 p = new SAPMAS_2();

        String sql = "SELECT * FROM SAPMAS WHERE SAPMAT = ? ";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ps.setString(1, mc);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                p.setDesc(result.getString("SAPDESC"));
                p.setMgroup(result.getString("SAPMGRP"));
                p.setMgdesc(result.getString("SAPMGRPDESC"));
                p.setMatCtrl(result.getString("SAPMCTRL"));
                p.setMatCtrlName(result.getString("SAPMCTRLDESC"));

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return p;

    }

    public QRMTRA_2 findSAPDesc(String code, String plant, String val) {

        QRMTRA_2 p = new QRMTRA_2();

        String sql = "SELECT SAPDESC FROM SAPMAS "
                + "WHERE SAPMAT = ? "
                + "AND SAPPLANT = ? "
                + "AND SAPVAL = ? ";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ps.setString(1, code);
            ps.setString(2, plant);
            ps.setString(3, val);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                p.setName(result.getString("SAPDESC"));

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return p;

    }
}
