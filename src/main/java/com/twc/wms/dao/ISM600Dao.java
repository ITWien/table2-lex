package com.twc.wms.dao;

import com.twc.wms.database.database;
import com.twc.wms.entity.XIDETAIL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author 93176
 */
public class ISM600Dao extends database {

    public List<XIDETAIL> findMat600X(String wh, String dest) {

        List<XIDETAIL> UAList = new ArrayList<XIDETAIL>();

        String sql = "SELECT DISTINCT QIDWHS, QIHDEST\n"
                + ",CASE WHEN QIDMTCTRL = '' OR QIDMTCTRL IS NULL THEN (select top 1 [SAPMCTRL] from [SAPMAS] where [SAPMAT] = QIDMAT) ELSE QIDMTCTRL END AS MATCTRL\n"
                + ",MATCNAME\n"
                + "FROM XIDETAIL\n"
                + "LEFT JOIN XIHEAD ON QIHQNO = QIDQNO\n"
                + "LEFT JOIN MSSMATN ON LGPBE = (CASE WHEN QIDMTCTRL = '' OR QIDMTCTRL IS NULL THEN (select top 1 [SAPMCTRL] from [SAPMAS] where [SAPMAT] = QIDMAT) ELSE QIDMTCTRL END)\n"
                + "WHERE QIDWHS = '" + wh + "'\n"
                + "AND QIHDEST = '" + dest + "'\n"
                + "ORDER BY MATCTRL";

//        System.out.println(sql);
        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                XIDETAIL p = new XIDETAIL();

                p.setId(result.getString("MATCTRL"));
                p.setDesc(result.getString("MATCNAME"));

                UAList.add(p);

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public List<XIDETAIL> findDetail600X(String wh, String dest, String shipDate, String sts, String mat, String MY) {

        List<XIDETAIL> UAList = new ArrayList<XIDETAIL>();

        String WH = "";
        String DEST = "";
        String SHIP = "";
        String MAT = "";

        if (wh != null && !wh.equals("")) {
            WH = "AND QIDWHS = '" + wh + "' \n";
        }

        if (!mat.equalsIgnoreCase("TOTAL") && !mat.trim().equals("All")) {
            MAT = "AND (CASE WHEN QIDMTCTRL = '' OR QIDMTCTRL IS NULL THEN (select top 1 [SAPMCTRL] from [SAPMAS] where [SAPMAT] = QIDMAT) ELSE QIDMTCTRL END) = '" + mat + "'\n";
        }

        if (dest != null && !dest.equals("") && !dest.equalsIgnoreCase("TOTAL")) {
            DEST = "AND (select top 1 QIHDEST from XIHEAD where QIHQNO = QIDQNO) = '" + dest + "' \n";
        }

//        if (shipDate != null && !shipDate.equals("")) {
//            SHIP = "AND QIDSHDT = '" + shipDate + "' \n";
//        }
        if (sts.equals("6") && MY != null) {
//            SHIP = "AND FORMAT(QIDSHDT,'yyyy-MM') = LEFT('" + MY + "',7) \n";
            SHIP = "AND YEAR(QIDTRDT) = '" + MY.split("-")[0] + "' AND MONTH(QIDTRDT) = '" + MY.split("-")[1] + "'";
        } else if ((sts.equals("2") || sts.equals("5")) && MY != null) {
            SHIP = "AND YEAR(QIDTRDT) = '" + MY.split("-")[0] + "' AND MONTH(QIDTRDT) = '" + MY.split("-")[1] + "'";
        }
        DecimalFormat formatDou = new DecimalFormat("#,##0.00");

        String sql = "SELECT QIDWHS, (select top 1 QIHDEST from XIHEAD where QIHQNO = QIDQNO) AS DEST, \n"
                + "       (CASE WHEN QIDMTCTRL = '' OR QIDMTCTRL IS NULL THEN (select top 1 [SAPMCTRL] from [SAPMAS] where [SAPMAT] = QIDMAT) ELSE QIDMTCTRL END) AS MATCTRL, \n"
                + "       QIDQNO, QIDMAT, (select top 1 QRMDESC from QRMMAS_TEMPA where QRMCODE = QIDMAT) AS QIDDESC, \n"
                + "       QIDGPQR, QIDID, \n"
                + "       QIDQTY, QIDBUN, QIDPACKTYPE, QIDSTS + ' : ' + STSDESC AS QIDSTS, QIDSTS AS STS, \n"
                + "       (select top 1 QIHMVT from XIHEAD where QIHQNO = QIDQNO) AS MVT, \n"
                + "       (select top 1 QMVDESC from QMVT where QMVMVT = (select top 1 QIHMVT from XIHEAD where QIHQNO = QIDQNO)) AS MVTN, \n"
                + "       QIDSHDT, QIDLINE, \n"
                + "       QIDWHS + ' : ' + \n"
                + "	  SUBSTRING(CONVERT(VARCHAR(25), QIDSHDT, 126), 9, 2) + '/' + \n"
                + "	  SUBSTRING(CONVERT(VARCHAR(25), QIDSHDT, 126), 6, 2) + '/' + \n"
                + "	  SUBSTRING(CONVERT(VARCHAR(25), QIDSHDT, 126), 1, 4) as SHIPDATE, \n"
                + "       CASE WHEN cast(QIDRUNNO as nvarchar) IS NULL THEN ' ' ELSE cast(QIDRUNNO as nvarchar) END AS QIDRUNNO, \n"
                + "       QIDROUND \n"
                + "FROM XIDETAIL \n"
                + "INNER JOIN QRMSTS \n"
                + "ON XIDETAIL.QIDSTS = QRMSTS.STSCODE \n"
                + "WHERE QIDQNO != ' ' \n"
                + WH
                + DEST
                + SHIP
                + "AND (QIDSTS = '" + sts + "') AND QIDRUNNO is null AND QIDQTY is not null \n"
                + MAT
                + "ORDER BY MATCTRL, QIDMAT, QIDPACKTYPE \n";
//        System.out.println(sql);

        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            String mc = "";
            String desc = "";
            String gqr = "";
            String id = "";
            double qty = 0;
            String um = "";
            int qp = 0;
            String pack = "";
            String mvt = "";
            double sumqty = 0;
            double summ3 = 0;
            int sumbox = 0;
            int sumbag = 0;
            int sumroll = 0;
            String oldGID = "";

            while (result.next()) {

                mc = result.getString("QIDMAT");
                desc = result.getString("QIDDESC");
                if (result.getString("QIDGPQR") == null) {
                    gqr = "<b style=\"opacity: 0.0;\">1</b>";
                } else if (result.getString("QIDGPQR").trim().equals("")) {
                    gqr = "<b style=\"opacity: 0.0;\">1</b>";
                } else {
                    gqr = result.getString("QIDGPQR");
                }
                id = result.getString("QIDID");
                qty += Double.parseDouble(result.getString("QIDQTY"));
                um = result.getString("QIDBUN");
                qp += 1;
                pack = result.getString("QIDPACKTYPE");
                mvt = result.getString("MVT");

                XIDETAIL p = new XIDETAIL();
                p.setNo(Integer.toString(qp)); // Q'pack
                p.setMatc(mc);
                p.setDesc(desc);
                p.setGID(gqr);
                p.setId(id);
                p.setUm(um);
                p.setPack(pack);

                p.setSd(result.getString("SHIPDATE") != null ? result.getString("SHIPDATE").split(" : ")[1] : "");

                p.setSts(result.getString("QIDSTS"));
                p.setMvt(mvt);
                p.setQno(result.getString("QIDQNO"));
                p.setMvtn(result.getString("MVTN"));
                p.setM3(formatDou.format(0));
                String tqt = formatDou.format(qty);
                p.setQty(tqt);
                sumqty += qty;
                summ3 += 0;

                String ck = "";
                String round = "";
                String runno = "";
                if (result.getString("QIDRUNNO") != null && !result.getString("QIDRUNNO").equals(" ")) {
                    runno = result.getString("QIDRUNNO");
                }
                String rnd = "";
                if (result.getString("QIDROUND") != null && !result.getString("QIDROUND").equals(" ")) {
                    rnd = result.getString("QIDROUND");
                }
                p.setShipmentDate(runno);
                if (result.getString("QIDGPQR") == null) {
                    ck = "<input style=\"width: 30px; height: 30px;\" type=\"checkbox\" id=\"selectCk-" + result.getString("QIDID") + "\" name=\"selectCk\" value=\"" + result.getString("QIDID") + "-" + result.getString("QIDQNO") + "-" + result.getString("SHIPDATE") + "-" + result.getString("QIDRUNNO") + "-" + result.getString("QIDLINE") + "-" + result.getString("STS") + "-" + result.getString("QIDROUND") + "\">";
                    round = "<input type=\"text\" name=\"round\" value=\"" + rnd + "\" style=\"width: 100px; text-align: center;\" disabled><b style=\"opacity: 0;\">" + rnd + "</b>";

                    if (pack.equals("BOX")) {
                        p.setBox(Integer.toString(qp));
                        sumbox += qp;
                    } else if (pack.equals("BAG") || pack.equals("PACK")) {
                        p.setBag(Integer.toString(qp));
                        sumbag += qp;
                    } else if (pack.equals("ROLL")) {
                        p.setRoll(Integer.toString(qp));
                        sumroll += qp;
                    }
                } else if (result.getString("QIDGPQR").trim().equals("")) {
                    ck = "<input style=\"width: 30px; height: 30px;\" type=\"checkbox\" id=\"selectCk-" + result.getString("QIDID") + "\" name=\"selectCk\" value=\"" + result.getString("QIDID") + "-" + result.getString("QIDQNO") + "-" + result.getString("SHIPDATE") + "-" + result.getString("QIDRUNNO") + "-" + result.getString("QIDLINE") + "-" + result.getString("STS") + "-" + result.getString("QIDROUND") + "\">";
                    round = "<input type=\"text\" name=\"round\" value=\"" + rnd + "\" style=\"width: 100px; text-align: center;\" disabled><b style=\"opacity: 0;\">" + rnd + "</b>";

                    if (pack.equals("BOX")) {
                        p.setBox(Integer.toString(qp));
                        sumbox += qp;
                    } else if (pack.equals("BAG") || pack.equals("PACK")) {
                        p.setBag(Integer.toString(qp));
                        sumbag += qp;
                    } else if (pack.equals("ROLL")) {
                        p.setRoll(Integer.toString(qp));
                        sumroll += qp;
                    }
                } else {
                    if (oldGID.equals("")) {
                        oldGID = result.getString("QIDGPQR");
                        ck = "<input style=\"width: 30px; height: 30px;\" type=\"checkbox\" id=\"selectCk-" + result.getString("QIDGPQR") + "\" name=\"selectCk\" value=\"" + result.getString("QIDGPQR") + "-" + result.getString("QIDQNO") + "-" + result.getString("SHIPDATE") + "-" + result.getString("QIDRUNNO") + "-" + result.getString("QIDLINE") + "-" + result.getString("STS") + "-" + result.getString("QIDROUND") + "\">";
                        round = "<input type=\"text\" name=\"round\" value=\"" + rnd + "\" style=\"width: 100px; text-align: center;\" disabled><b style=\"opacity: 0;\">" + rnd + "</b>";

                        if (pack.equals("BOX")) {
                            p.setBox(Integer.toString(qp));
                            sumbox += qp;
                        } else if (pack.equals("BAG") || pack.equals("PACK")) {
                            p.setBag(Integer.toString(qp));
                            sumbag += qp;
                        } else if (pack.equals("ROLL")) {
                            p.setRoll(Integer.toString(qp));
                            sumroll += qp;
                        }
                    } else {
                        if (!result.getString("QIDGPQR").equals(oldGID)) {
                            oldGID = result.getString("QIDGPQR");
                            ck = "<input style=\"width: 30px; height: 30px;\" type=\"checkbox\" id=\"selectCk-" + result.getString("QIDGPQR") + "\" name=\"selectCk\" value=\"" + result.getString("QIDGPQR") + "-" + result.getString("QIDQNO") + "-" + result.getString("SHIPDATE") + "-" + result.getString("QIDRUNNO") + "-" + result.getString("QIDLINE") + "-" + result.getString("STS") + "-" + result.getString("QIDROUND") + "\">";
                            round = "<input type=\"text\" name=\"round\" value=\"" + rnd + "\" style=\"width: 100px; text-align: center;\" disabled><b style=\"opacity: 0;\">" + rnd + "</b>";

                            if (pack.equals("BOX")) {
                                p.setBox(Integer.toString(qp));
                                sumbox += qp;
                            } else if (pack.equals("BAG") || pack.equals("PACK")) {
                                p.setBag(Integer.toString(qp));
                                sumbag += qp;
                            } else if (pack.equals("ROLL")) {
                                p.setRoll(Integer.toString(qp));
                                sumroll += qp;
                            }
                        }
                    }
                }

                p.setCkBox(ck);
                p.setRunno(round);

                UAList.add(p);

                qty = 0;
                qp = 0;

            }

            XIDETAIL p = new XIDETAIL();
            p.setGID("<b style=\"opacity: 0.0;\">0</b>");
            p.setMatc("<b style=\"opacity: 0.0;\">0</b>");
            p.setDesc("<b style=\"opacity: 0.0;\">0</b>");
            p.setId("<b>TOTAL</b>");
            p.setBox("<b>" + Integer.toString(sumbox) + "</b>");
            p.setBag("<b>" + Integer.toString(sumbag) + "</b>");
            p.setRoll("<b>" + Integer.toString(sumroll) + "</b>");
            p.setM3("<b>" + formatDou.format(summ3) + "</b>");
            p.setQty("<b>" + formatDou.format(sumqty) + "</b>");
            UAList.add(p);

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public boolean rcQI2To5(String sts, String qid, String qno, String line) {

        boolean result = false;

        String sql = "UPDATE XIDETAIL SET QIDSTS = '" + sts + "' WHERE QIDID = '" + qid + "' AND QIDQNO = '" + qno + "' AND QIDLINE = '" + line + "' ";

//        System.out.println("" + sql);
        try {

            PreparedStatement ps = connect.prepareStatement(sql);
//            PreparedStatement ps = null;

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public String findMVT(String wh, String qno) {

        String mvt = "";

        String sql = "SELECT [QIHMVT]\n"
                + "  FROM [RMShipment].[dbo].[XIHEAD]\n"
                + "  where [QIHWHS] = '" + wh + "'\n"
                + "  and [QIHQNO] = '" + qno + "'";
//        System.out.println(sql);

        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                mvt = result.getString("QIHMVT");

            }

        } catch (Exception e) {

            e.printStackTrace();

        }

        return mvt;

    }

    public boolean SETSTS(String max, String min, String wh, String qno, String tqty) {

        boolean result = false;

        String sql = "UPDATE XIHEAD "
                + "SET QIHSTS = '" + min + "', QIHHISTS = '" + max + "', QIHTOTQTY = '" + tqty + "' "
                + "WHERE QIHWHS = '" + wh + "' "
                + "AND QIHQNO = '" + qno + "' ";

        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public List<String> UngroupQRID(String id) {

        List<String> UAList = new ArrayList<String>();

        String[] parts = id.split("-");
        id = parts[0];

        String sql = "SELECT QIDID, QIDQNO, QIDLINE\n"
                + "FROM XIDETAIL\n"
                + "WHERE QIDGPQR = '" + id + "' ";

        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                String p = result.getString("QIDID").trim() + "-"
                        + result.getString("QIDQNO").trim() + "-"
                        + result.getString("QIDLINE").trim();
                UAList.add(p);

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public boolean HoldISM600(String id, String user) {

        boolean result = false;

        String where = "";

        String[] parts = id.split("-");
        id = parts[0];
        String qno = parts[1];

        if (id.length() < 17) {
            where = "QIDGPQR";
        } else if (id.length() == 17) {
            where = "QIDID";
        }

        String sql = "UPDATE XIDETAIL "
                + "SET QIDSTS = 'H', QIDTPUSR = '" + user + "', QIDTPDT = CURRENT_TIMESTAMP, QIDEDT = CURRENT_TIMESTAMP "
                + "WHERE " + where + " = '" + id + "' "
                + "AND QIDQNO = '" + qno + "' ";

        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public String[] getSTS(String wh, String qno) {
        String[] p = new String[2];

        String sql = "SELECT MAX(QIDSTS) as MAXS, MIN(QIDSTS) as MINS\n"
                + "from(\n"
                + "SELECT QIDSTS FROM XIDETAIL\n"
                + "  where QIDWHS = '" + wh + "'\n"
                + "  and QIDQNO in ('" + qno + "')\n"
                + ") TMP";
//        System.out.println(sql);

        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                p[0] = result.getString("MAXS");
                p[1] = result.getString("MINS");

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return p;
    }

    public String getTQY(String wh, String qno) {
        String p = "";

        String sql = "SELECT SUM(QIDQTY) AS TOTAL FROM XIDETAIL\n"
                + "  where QIDWHS = '" + wh + "'\n"
                + "  and QIDQNO in ('" + qno + "')\n"
                + "  and (QIDSTS = '2' OR QIDSTS = '5' OR QIDSTS = '6' OR QIDSTS = 'C' OR QIDSTS = '7') ";
//        System.out.println(sql);

        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                p = result.getString("TOTAL");

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return p;
    }

    public boolean transport(String id, String user) {

        boolean result = false;

        String where = "";

        String[] parts = id.split("-");
        id = parts[0];
        String qno = parts[1];

        if (id.length() < 17) {
            where = "QIDGPQR";
        } else if (id.length() == 17) {
            where = "QIDID";
        }

        String sql = "UPDATE XIDETAIL "
                + "SET QIDSTS = '6', QIDTPUSR = '" + user + "', QIDTPDT = CURRENT_TIMESTAMP, QIDEDT = CURRENT_TIMESTAMP "
                + "WHERE " + where + " = '" + id + "' "
                + "AND QIDQNO = '" + qno + "' ";

        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean roundX(String id, String user, String round, String sd) {

        boolean result = false;

        String where = "";

        String[] parts = id.split("-");
        id = parts[0];
        String qno = parts[1];

        if (id.length() < 17) {
            where = "QIDGPQR";
        } else if (id.length() == 17) {
            where = "QIDID";
        }

        String sql = "UPDATE XIDETAIL "
                + "SET QIDROUND = '" + round + "', QIDSHDT = '" + sd + "', QIDEDT = CURRENT_TIMESTAMP "
                + "WHERE " + where + " = '" + id + "' "
                + "AND QIDQNO = '" + qno + "' "
                + "AND QIDSTS = '6'";
//        System.out.println(sql);

        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public ResultSet findForPrintISM600X(String[] id, String wh, String dest) throws SQLException {

        List<String> shdtList = new ArrayList<String>();
        StringBuilder list = new StringBuilder();
        for (int i = 0; i < id.length; i++) {
//            if (i == 0) {
//                if (id[i].split("-")[6].equals("null")) {
//                    list.append("'0'");
//                }
//            }
//            if (!id[i].split("-")[6].equals("null")) {
            if (!id[i].split("-")[0].equals(" ")) {
                if (i == 0) {
                    list.append("'").append(id[i].split("-")[0]).append("'");
                } else {
                    list.append(",'").append(id[i].split("-")[0]).append("'");
                }
            }
//            }

            String tmp = id[i].split("-")[2].split(" : ")[1];
            tmp = tmp.replace("/", "-");
            tmp = tmp.split("-")[2] + "-" + tmp.split("-")[1] + "-" + tmp.split("-")[0];
            if (!shdtList.contains(tmp)) {
                shdtList.add(tmp);
            }
        }

        StringBuilder list2 = new StringBuilder();
        for (int i = 0; i < shdtList.size(); i++) {
            if (i == 0) {
                list2.append("'").append(shdtList.get(i)).append("'");
            } else {
                list2.append(",'").append(shdtList.get(i)).append("'");
            }
        }

//        String sql = "SELECT QIDSHDT +'-'+ QIDROUND AS DTRO, QIDSHDT, QIDROUND, MATCTRL, QIDMAT, QIDDESC, QIDPACKTYPE, \n"
//                + "CASE WHEN BAG = 1 THEN COUNT(*) ELSE 0 END AS BAG, \n"
//                + "CASE WHEN ROLL = 1 THEN COUNT(*) ELSE 0 END AS ROLL, \n"
//                + "CASE WHEN BOX = 1 THEN COUNT(*) ELSE 0 END AS BOX, \n"
//                + "QIDQNO, \n"
//                + "(SELECT TOP 1 QIDRUNNO \n"
//                + "FROM XIDETAIL DD\n"
//                + "LEFT JOIN XIHEAD HH ON HH.QIHQNO = DD.QIDQNO\n"
//                + "LEFT JOIN QRMMAS_TEMPA MM ON MM.QRMCODE = DD.QIDMAT\n"
//                + "WHERE QIDROUND = TMP.QIDROUND\n"
//                + "AND HH.QIHMTCTRL = TMP.MATCTRL\n"
//                + "AND QIDMAT = TMP.QIDMAT\n"
//                + "AND MM.QRMDESC = TMP.QIDDESC\n"
//                + "AND QIDPACKTYPE = TMP.QIDPACKTYPE\n"
//                + "AND QIDQNO = TMP.QIDQNO\n"
//                + "AND HH.QIHMVT = TMP.MVT\n"
//                + ") AS QIDRUNNO, \n"
//                + "QIDGPQR, MVT, TOTD, QIDOCNO,DOCLEN\n"
//                + "FROM(\n"
//                + "SELECT FORMAT(QIDSHDT,'yyyy-MM-dd') AS QIDSHDT, QIDROUND,\n"
//                + "       (select top 1 QIHMTCTRL from XIHEAD where QIHQNO = QIDQNO) AS MATCTRL,\n"
//                + "       QIDMAT, (select top 1 QRMDESC from QRMMAS_TEMPA where QRMCODE = QIDMAT) AS QIDDESC,\n"
//                + "	   QIDPACKTYPE,\n"
//                + "	   CASE WHEN QIDPACKTYPE = 'BAG' THEN 1 WHEN QIDPACKTYPE = 'PACK' THEN 1 ELSE 0 END AS BAG,\n"
//                + "	   CASE WHEN QIDPACKTYPE = 'ROLL' THEN 1 ELSE 0 END AS ROLL,\n"
//                + "	   CASE WHEN QIDPACKTYPE = 'BOX' THEN 1 ELSE 0 END AS BOX,\n"
//                + "       QIDQNO, QIDGPQR,\n"
//                + "	   (select top 1 QIHMVT from XIHEAD where QIHQNO = QIDQNO) AS MVT, \n"
//                + "        (select top 1 QIHTOTD from XIHEAD where QIHQNO = QIDQNO) AS TOTD, \n"
//                + "	   QIDOCNO,LEN(QIDOCNO) AS DOCLEN\n"
//                + "FROM XIDETAIL\n"
//                + "WHERE (select top 1 QIHDEST from XIHEAD where QIHQNO = QIDQNO) = '" + dest + "'\n"
//                + "AND QIDWHS = '" + wh + "'\n"
//                //                + "AND QIDSHDT = '" + shipDate + "'\n"
//                + "AND QIDID IN (" + list + ")\n"
//                + ") TMP\n"
//                + "GROUP BY QIDSHDT, QIDROUND, MATCTRL, QIDMAT, QIDDESC, QIDPACKTYPE, BAG, ROLL, BOX, QIDQNO, QIDGPQR, MVT, TOTD, QIDOCNO,DOCLEN\n"
//                + "ORDER BY QIDSHDT, QIDROUND, MATCTRL, QIDGPQR, QIDMAT, QIDQNO, MVT, QIDOCNO,DOCLEN";
        String sql = "SELECT DTRO, QIDSHDT, QIDROUND, MATCTRL, QIDMAT, QIDDESC, \n"
                + "CASE WHEN CNT = 1 THEN (CASE WHEN QIDGPQR IS NULL THEN SUM(BAG) ELSE (CASE WHEN SUM(BAG) > 1 THEN 1 ELSE SUM(BAG) END) END) WHEN CNT != 1 AND QIDGPQR IS NULL THEN SUM(BAG) ELSE 0 END AS BAG, \n"
                + "CASE WHEN CNT = 1 THEN (CASE WHEN QIDGPQR IS NULL THEN SUM(ROLL) ELSE (CASE WHEN SUM(ROLL) > 1 THEN 1 ELSE SUM(ROLL) END) END) WHEN CNT != 1 AND QIDGPQR IS NULL THEN SUM(ROLL) ELSE 0 END AS ROLL, \n"
                + "CASE WHEN CNT = 1 THEN (CASE WHEN QIDGPQR IS NULL THEN SUM(BOX) ELSE (CASE WHEN SUM(BOX) > 1 THEN 1 ELSE SUM(BOX) END) END) WHEN CNT != 1 AND QIDGPQR IS NULL THEN SUM(BOX) ELSE 0 END AS BOX,\n"
                + "QIDQNO, QIDGPQR, MVT, TOTD, QIDOCNO, DOCLEN, QIDCDT, QIDID, CNT\n"
                + "FROM(\n"
                + "SELECT QIDSHDT +'-'+ QIDROUND AS DTRO, QIDSHDT, QIDROUND, MATCTRL, QIDMAT, QIDDESC, \n"
                + "BAG, ROLL, BOX, QIDQNO, QIDGPQR, MVT, TOTD, QIDOCNO, DOCLEN, QIDCDT, QIDID, CNT\n"
                + "FROM(\n"
                + "SELECT FORMAT(QIDSHDT,'yyyy-MM-dd') AS QIDSHDT, QIDROUND,\n"
                + "       CASE WHEN QIDMTCTRL = '' OR QIDMTCTRL IS NULL THEN (select top 1 [SAPMCTRL] from [SAPMAS] where [SAPMAT] = QIDMAT) ELSE QIDMTCTRL END AS MATCTRL,\n"
                //                + "       (select top 1 QIHMTCTRL from XIHEAD where QIHQNO = QIDQNO) AS MATCTRL,\n"
                + "       QIDMAT, (select top 1 QRMDESC from QRMMAS_TEMPA where QRMCODE = QIDMAT) AS QIDDESC,\n"
                + "	   CASE WHEN QIDPACKTYPE = 'BAG' THEN 1 WHEN QIDPACKTYPE = 'PACK' THEN 1 ELSE 0 END AS BAG,\n"
                + "	   CASE WHEN QIDPACKTYPE = 'ROLL' THEN 1 ELSE 0 END AS ROLL,\n"
                + "	   CASE WHEN QIDPACKTYPE = 'BOX' THEN 1 ELSE 0 END AS BOX,\n"
                + "       QIDQNO, QIDGPQR, \n"
                + "	   (select top 1 QIHMVT from XIHEAD where QIHQNO = QIDQNO) AS MVT,\n"
                + "	   (select top 1 QIHTOTD from XIHEAD where QIHQNO = QIDQNO) AS TOTD,\n"
                + "	   QIDOCNO,LEN(QIDOCNO) AS DOCLEN, QIDCDT, CASE WHEN QIDGPQR IS NULL THEN QIDID ELSE QIDGPQR END as QIDID\n"
                + "       ,ROW_NUMBER() \n"
                + "	   OVER(PARTITION BY (CASE WHEN QIDGPQR IS NULL THEN QIDID ELSE QIDGPQR END) ORDER BY QIDSHDT, QIDROUND, QIDQNO, QIDCDT, (select top 1 QIHMTCTRL from XIHEAD where QIHQNO = QIDQNO), QIDGPQR, QIDMAT, (select top 1 QIHMVT from XIHEAD where QIHQNO = QIDQNO), QIDOCNO, LEN(QIDOCNO)) as CNT\n"
                + "FROM XIDETAIL\n"
                + "WHERE (select top 1 QIHDEST from XIHEAD where QIHQNO = QIDQNO) = '" + dest + "'\n"
                + "AND QIDWHS = '" + wh + "'\n"
                + "AND QIDSHDT IN (" + list2 + ")\n"
                + "AND (QIDID IN (" + list + ") OR QIDGPQR IN (" + list + "))\n"
                + ") TMP\n"
                + ")TMP2\n"
                + "WHERE DTRO is not null\n"
                + "GROUP BY DTRO, QIDSHDT, QIDROUND, MATCTRL, QIDMAT, QIDDESC, QIDQNO, QIDGPQR, MVT, TOTD, QIDOCNO, DOCLEN, QIDCDT, QIDID, CNT\n"
                + "ORDER BY QIDSHDT, QIDROUND, QIDQNO, QIDCDT, MATCTRL, QIDGPQR, QIDMAT, MVT, QIDOCNO, DOCLEN";

//        System.out.println(sql);
        ResultSet result = null;

        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            result = ps.executeQuery();
        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean cancelISM600(String id, String user) {

        boolean result = false;

        String where = "";

        String[] parts = id.split("-");
        id = parts[0];
        String qno = parts[1];

        if (id.length() < 17) {
            where = "QIDGPQR";
        } else if (id.length() == 17) {
            where = "QIDID";
        }

        String sql = "UPDATE XIDETAIL "
                + "SET QIDSTS = 'R', QIDTPUSR = '" + user + "', QIDTPDT = CURRENT_TIMESTAMP, QIDEDT = CURRENT_TIMESTAMP "
                + "WHERE " + where + " = '" + id + "' "
                + "AND QIDQNO = '" + qno + "' ";

        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public ResultSet findForPrintISM600X2(String wh, String dest, String shipDate, String round) throws SQLException {

        String sql = "SELECT DTRO, QIDSHDT, QIDROUND, MATCTRL, QIDMAT, QIDDESC, \n"
                + "CASE WHEN CNT = 1 THEN (CASE WHEN QIDGPQR IS NULL THEN SUM(BAG) ELSE (CASE WHEN SUM(BAG) > 1 THEN 1 ELSE SUM(BAG) END) END) WHEN CNT != 1 AND QIDGPQR IS NULL THEN SUM(BAG) ELSE 0 END AS BAG, \n"
                + "CASE WHEN CNT = 1 THEN (CASE WHEN QIDGPQR IS NULL THEN SUM(ROLL) ELSE (CASE WHEN SUM(ROLL) > 1 THEN 1 ELSE SUM(ROLL) END) END) WHEN CNT != 1 AND QIDGPQR IS NULL THEN SUM(ROLL) ELSE 0 END AS ROLL, \n"
                + "CASE WHEN CNT = 1 THEN (CASE WHEN QIDGPQR IS NULL THEN SUM(BOX) ELSE (CASE WHEN SUM(BOX) > 1 THEN 1 ELSE SUM(BOX) END) END) WHEN CNT != 1 AND QIDGPQR IS NULL THEN SUM(BOX) ELSE 0 END AS BOX,\n"
                + "QIDQNO, QIDGPQR, MVT, TOTD, QIDOCNO, DOCLEN, QIDCDT, QIDID, CNT\n"
                + "FROM(\n"
                + "SELECT QIDSHDT +'-'+ QIDROUND AS DTRO, QIDSHDT, QIDROUND, MATCTRL, QIDMAT, QIDDESC, \n"
                + "BAG, ROLL, BOX, QIDQNO, QIDGPQR, MVT, TOTD, QIDOCNO, DOCLEN, QIDCDT, QIDID, CNT\n"
                + "FROM(\n"
                + "SELECT FORMAT(QIDSHDT,'yyyy-MM-dd') AS QIDSHDT, QIDROUND,\n"
                + "       CASE WHEN QIDMTCTRL = '' OR QIDMTCTRL IS NULL THEN (select top 1 [SAPMCTRL] from [SAPMAS] where [SAPMAT] = QIDMAT) ELSE QIDMTCTRL END AS MATCTRL,\n"
                + "       QIDMAT, (select top 1 QRMDESC from QRMMAS_TEMPA where QRMCODE = QIDMAT) AS QIDDESC,\n"
                + "	   CASE WHEN QIDPACKTYPE = 'BAG' THEN 1 WHEN QIDPACKTYPE = 'PACK' THEN 1 ELSE 0 END AS BAG,\n"
                + "	   CASE WHEN QIDPACKTYPE = 'ROLL' THEN 1 ELSE 0 END AS ROLL,\n"
                + "	   CASE WHEN QIDPACKTYPE = 'BOX' THEN 1 ELSE 0 END AS BOX,\n"
                + "       QIDQNO, QIDGPQR, \n"
                + "	   (select top 1 QIHMVT from XIHEAD where QIHQNO = QIDQNO) AS MVT,\n"
                + "	   (select top 1 QIHTOTD from XIHEAD where QIHQNO = QIDQNO) AS TOTD,\n"
                + "	   QIDOCNO,LEN(QIDOCNO) AS DOCLEN, QIDCDT, CASE WHEN QIDGPQR IS NULL THEN QIDID ELSE QIDGPQR END as QIDID\n"
                + "       ,ROW_NUMBER() \n"
                + "	   OVER(PARTITION BY (CASE WHEN QIDGPQR IS NULL THEN QIDID ELSE QIDGPQR END) ORDER BY QIDSHDT, QIDROUND, QIDQNO, QIDCDT, (select top 1 QIHMTCTRL from XIHEAD where QIHQNO = QIDQNO), QIDGPQR, QIDMAT, (select top 1 QIHMVT from XIHEAD where QIHQNO = QIDQNO), QIDOCNO, LEN(QIDOCNO)) as CNT\n"
                + "FROM XIDETAIL\n"
                + "WHERE (select top 1 QIHDEST from XIHEAD where QIHQNO = QIDQNO) = '" + dest + "'\n"
                + "AND QIDWHS = '" + wh + "'\n"
                + "AND QIDSHDT = '" + shipDate + "'\n"
                + "AND QIDROUND = '" + round + "'\n"
                + ") TMP\n"
                + ")TMP2\n"
                + "WHERE DTRO is not null\n"
                + "GROUP BY DTRO, QIDSHDT, QIDROUND, MATCTRL, QIDMAT, QIDDESC, QIDQNO, QIDGPQR, MVT, TOTD, QIDOCNO, DOCLEN, QIDCDT, QIDID, CNT\n"
                + "ORDER BY QIDSHDT, QIDROUND, QIDQNO, QIDCDT, MATCTRL, QIDGPQR, QIDMAT, MVT, QIDOCNO, DOCLEN";

//        System.out.println(sql);
        ResultSet result = null;

        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            result = ps.executeQuery();
        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public ResultSet findForPrint2ISM600X(String[] id, String wh, String dest, String shipDate) throws SQLException {

        StringBuilder list = new StringBuilder();
        for (int i = 0; i < id.length; i++) {
            if (i == 0) {
                if (id[i].split("-")[6].equals("null")) {
                    list.append("'0'");
                }
            }
            if (!id[i].split("-")[6].equals("null")) {
                if (!id[i].split("-")[3].equals(" ")) {
                    if (i == 0) {
                        list.append("'").append(id[i].split("-")[3]).append("'");
                    } else {
                        list.append(",'").append(id[i].split("-")[3]).append("'");
                    }
                }
            }
        }

        String sql = "SELECT QIDSHDT +'-'+ QIDROUND AS DTRO, QIDSHDT, QIDROUND, MATCTRL, QIDMAT, QIDDESC, QIDPACKTYPE,\n"
                + "CASE WHEN BAG = 1 THEN COUNT(*) ELSE 0 END AS BAG,\n"
                + "CASE WHEN ROLL = 1 THEN COUNT(*) ELSE 0 END AS ROLL,\n"
                + "CASE WHEN BOX = 1 THEN COUNT(*) ELSE 0 END AS BOX,\n"
                + "QIDQNO,\n"
                + "(SELECT TOP 1 QIDRUNNO\n"
                + "FROM XIDETAIL DD\n"
                + "LEFT JOIN XIHEAD HH ON HH.QIHQNO = DD.QIDQNO\n"
                + "LEFT JOIN QRMMAS_TEMPA MM ON MM.QRMCODE = DD.QIDMAT\n"
                + "WHERE QIDROUND = TMP.QIDROUND\n"
                + "AND HH.QIHMTCTRL = TMP.MATCTRL\n"
                + "AND QIDMAT = TMP.QIDMAT\n"
                + "AND MM.QRMDESC = TMP.QIDDESC\n"
                + "AND QIDPACKTYPE = TMP.QIDPACKTYPE\n"
                + "AND QIDQNO = TMP.QIDQNO\n"
                + "AND HH.QIHMVT = TMP.MVT\n"
                + ") AS QIDRUNNO,\n"
                + "QIDGPQR, MVT, QIDOCNO,\n"
                + "(\n"
                + "SELECT MIN(QIDRUNNO)\n"
                + "FROM XIDETAIL DD\n"
                + "LEFT JOIN XIHEAD HH ON HH.QIHQNO = DD.QIDQNO\n"
                + "WHERE QIDROUND = TMP.QIDROUND\n"
                + "AND HH.QIHMTCTRL = TMP.MATCTRL\n"
                + "AND QIDMAT = TMP.QIDMAT\n"
                + "AND QIDPACKTYPE = TMP.QIDPACKTYPE\n"
                + "AND QIDQNO = TMP.QIDQNO\n"
                + "AND HH.QIHMVT = TMP.MVT\n"
                + "AND HH.QIHDEST = '" + dest + "'\n"
                + "AND QIDWHS = '" + wh + "'\n"
                //                + "AND QIDSHDT = '" + shipDate + "'\n"
                + ") AS MINRUN,\n"
                + "(\n"
                + "SELECT MAX(QIDRUNNO)\n"
                + "FROM XIDETAIL DD\n"
                + "LEFT JOIN XIHEAD HH ON HH.QIHQNO = DD.QIDQNO\n"
                + "WHERE QIDROUND = TMP.QIDROUND\n"
                + "AND HH.QIHMTCTRL = TMP.MATCTRL\n"
                + "AND QIDMAT = TMP.QIDMAT\n"
                + "AND QIDPACKTYPE = TMP.QIDPACKTYPE\n"
                + "AND QIDQNO = TMP.QIDQNO\n"
                + "AND HH.QIHMVT = TMP.MVT\n"
                + "AND HH.QIHDEST = '" + dest + "'\n"
                + "AND QIDWHS = '" + wh + "'\n"
                //                + "AND QIDSHDT = '" + shipDate + "'\n"
                + ") AS MAXRUN\n"
                + "FROM(\n"
                + "SELECT FORMAT(QIDSHDT,'yyyy-MM-dd') AS QIDSHDT, QIDROUND,\n"
                + "       (select top 1 QIHMTCTRL from XIHEAD where QIHQNO = QIDQNO) AS MATCTRL,\n"
                + "       QIDMAT, (select top 1 QRMDESC from QRMMAS_TEMPA where QRMCODE = QIDMAT) AS QIDDESC,\n"
                + "	   QIDPACKTYPE,\n"
                + "	   CASE WHEN QIDPACKTYPE = 'BAG' THEN 1 WHEN QIDPACKTYPE = 'PACK' THEN 1 ELSE 0 END AS BAG,\n"
                + "	   CASE WHEN QIDPACKTYPE = 'ROLL' THEN 1 ELSE 0 END AS ROLL,\n"
                + "	   CASE WHEN QIDPACKTYPE = 'BOX' THEN 1 ELSE 0 END AS BOX,\n"
                + "       QIDQNO, QIDGPQR,\n"
                + "	   (select top 1 QIHMVT from XIHEAD where QIHQNO = QIDQNO) AS MVT,\n"
                + "	   QIDOCNO\n"
                + "FROM XIDETAIL\n"
                + "WHERE (select top 1 QIHDEST from XIHEAD where QIHQNO = QIDQNO) = '" + dest + "'\n"
                + "AND QIDWHS = '" + wh + "'\n"
                //                + "AND QIDSHDT = '" + shipDate + "'\n"
                + "AND QIDRUNNO IN (" + list + ")\n"
                + ") TMP\n"
                + "GROUP BY QIDSHDT, QIDROUND, MATCTRL, QIDMAT, QIDDESC, QIDPACKTYPE, BAG, ROLL, BOX, QIDQNO, QIDGPQR, MVT, QIDOCNO\n"
                + "ORDER BY QIDSHDT, QIDROUND, QIDRUNNO, MATCTRL, QIDGPQR, QIDMAT, QIDQNO, MVT, QIDOCNO ";
//        System.out.println(sql);
        ResultSet result = null;

        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            result = ps.executeQuery();
        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean UpdateStatus(String id, String sts) {

        boolean result = false;

        String sql = "UPDATE QRMMAS_TEMPA "
                + "SET QRMSTS = '" + sts + "' "
                + "WHERE QRMID = '" + id + "' ";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean AddQRMTRA(String id, String user) {

        boolean result = false;

        String sql = "insert into QRMTRA_TEMPA\n"
                + "SELECT QIDCOM as QRMCOM, \n"
                + "QIDWHS as QRMWHSE, \n"
                + "QIDMAT as QRMCODE, \n"
                + "QRMVAL as QRMVAL, \n"
                + "QRMPLANT as QRMPLANT, \n"
                + "QRMSTORAGE as QRMSTORAGE, \n"
                + "QIDID as QRMID, \n"
                + "QRMROLL as QRMROLL,\n"
                + "SUBSTRING(CONVERT(VARCHAR(25), CURRENT_TIMESTAMP, 126), 1, 10) as QRMTRDT, \n"
                + "SUBSTRING(CONVERT(VARCHAR(25), CURRENT_TIMESTAMP, 126), 12, 13) as QRMTRTM, \n"
                + "(SELECT TOP 1 QIHMVT FROM XIHEAD WHERE QIHQNO = QIDQNO) as QRMMVT, \n"
                + "(SELECT TOP 1 QMVTRN FROM QMVT WHERE QMVMVT = (SELECT TOP 1 QIHMVT FROM XIHEAD WHERE QIHQNO = QIDQNO)) as QRMTRT,\n"
                + "QIDQNO as QRMDOCNO, \n"
                + "NULL as QRMSAPNO, \n"
                + "(QIDQTY*(-1)) as QRMQTY, \n"
                + "QIDBUN as QRMBUN, \n"
                + "CURRENT_TIMESTAMP as QRMCDT, \n"
                + "'" + user + "' as QRMUSER\n"
                + "FROM XIDETAIL\n"
                + "LEFT JOIN QRMMAS_TEMPA ON XIDETAIL.QIDID = QRMMAS_TEMPA.QRMID \n"
                + "WHERE QIDID = '" + id + "' ";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

}
