/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.dao;

import com.twc.wms.database.database;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

/**
 *
 * @author nutthawoot.noo
 */
public class TOPKDAO extends database {

    public void addMSSTOPK(String GRPNO, int ISSUENO, String MATNR, float ISSUETOPICK) {

        try {
            PreparedStatement statement = connect.prepareStatement("insert into ISMTOPK(GRPNO, ISSUENO, MATNR, ISSUETOPICK) values (?, ?, ?, ?)");
            statement.setString(1, GRPNO);
            statement.setInt(2, ISSUENO);
            statement.setString(3, MATNR);
            statement.setFloat(4, ISSUETOPICK);
            statement.executeUpdate();
        } catch (SQLException err) {
            err.printStackTrace();
        }

    }

    public void addMSSTAPK(String GRPNO, int ISSUENO, String MATNR, float ISSUETOPICK, Date ISSUEDATE) {

        try {
            PreparedStatement statement = connect.prepareStatement("insert into ISMTAPK(GRPNO, ISSUENO, MATNR, ISSUEPICK, ISSUEDATE) values (?, ?, ?, ?, ?)");
            statement.setString(1, GRPNO);
            statement.setInt(2, ISSUENO);
            statement.setString(3, MATNR);
            statement.setFloat(4, ISSUETOPICK);
            statement.setDate(5, new java.sql.Date(ISSUEDATE.getTime()));
            statement.executeUpdate();
        } catch (SQLException err) {
            err.printStackTrace();
        }

    }

    public void update(String GRPNO, int ISSUENO, String MATNR, float ISSUETOPICK) {

        try {
            PreparedStatement statement = connect.prepareStatement("update ISMTOPK set ISSUETOPICK=? where GRPNO=? and ISSUENO=? and MATNR=?");
            statement.setFloat(1, ISSUETOPICK);
            statement.setString(2, GRPNO);
            statement.setInt(3, ISSUENO);
            statement.setString(4, MATNR);
            statement.executeUpdate();
        } catch (SQLException err) {
            err.printStackTrace();
        }

    }

    public float getISSUETOPICK(String GRPNO, int ISSUENO, String MATNR) {

        float ISSUETOPICK = 0.000f;

        try {
            PreparedStatement statement = connect.prepareStatement("select ISSUETOPICK from ISMTOPK where GRPNO=? and ISSUENO=? and MATNR=?");
            statement.setString(1, GRPNO);
            statement.setInt(2, ISSUENO);
            statement.setString(3, MATNR);
            ResultSet set = statement.executeQuery();

            if (set.next()) {
                ISSUETOPICK = set.getFloat("ISSUETOPICK");
            }

        } catch (SQLException err) {
            err.printStackTrace();
        }

        return ISSUETOPICK;

    }

    public boolean checkDuplicate(String GRPNO, int ISSUENO, String MATNR) {

        boolean status = false;

        try {
            PreparedStatement statement = connect.prepareStatement("select * from ISMTOPK where GRPNO=? and ISSUENO=? and MATNR=?");
            statement.setString(1, GRPNO);
            statement.setInt(2, ISSUENO);
            statement.setString(3, MATNR);
            ResultSet set = statement.executeQuery();

            if (set.next()) {
                status = true;
            }

        } catch (SQLException err) {
            status = true;
            err.printStackTrace();
        }

        return status;

    }

}
