/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.dao;

import com.twc.wms.entity.QRMTRA;
import com.twc.wms.entity.SAPMAS_2;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import com.twc.wms.database.database;

/**
 *
 * @author nutthawoot.noo
 */
public class QRMTRADao extends database {

    public boolean AddQRMTRApallet(String id, String user) {

        boolean result = false;

        String sql = "INSERT into QRMTRA\n"
                + "SELECT QRMCOM\n"
                + "      ,QRMWHSE\n"
                + "	  ,QRMCODE\n"
                + "	  ,QRMVAL\n"
                + "	  ,QRMPLANT\n"
                + "	  ,QRMSTORAGE\n"
                + "	  ,QRMID\n"
                + "	  ,QRMROLL\n"
                + "	  ,SUBSTRING(CONVERT(VARCHAR(25), QRMEDT, 126), 1, 10) as QRMTRDT\n"
                + "      ,SUBSTRING(CONVERT(VARCHAR(25), QRMEDT, 126), 12, 13) as QRMTRTM\n"
                + "      ,'900' as QRMMVT\n"
                + "	  ,'MVPL' as QRMTRT\n"
                + "	  ,NULL as QRMDOCNO\n"
                + "	  ,NULL as QRMSAPNO\n"
                + "	  ,QRMQTY\n"
                + "      ,QRMBUN\n"
                + "	  ,CURRENT_TIMESTAMP as QRMCDT\n"
                + "	  ,'" + user + "' as QRMUSER\n"
                + "  FROM QRMMAS\n"
                + "  WHERE QRMID = '" + id + "'";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean AddQRMTRA(String id, String user) {

        boolean result = false;

        String sql = "insert into QRMTRA\n"
                + "SELECT QIDCOM as QRMCOM, \n"
                + "QIDWHS as QRMWHSE, \n"
                + "QIDMAT as QRMCODE, \n"
                + "QRMVAL as QRMVAL, \n"
                + "QRMPLANT as QRMPLANT, \n"
                + "QRMSTORAGE as QRMSTORAGE, \n"
                + "QIDID as QRMID, \n"
                + "QRMROLL as QRMROLL,\n"
                + "SUBSTRING(CONVERT(VARCHAR(25), CURRENT_TIMESTAMP, 126), 1, 10) as QRMTRDT, \n"
                + "SUBSTRING(CONVERT(VARCHAR(25), CURRENT_TIMESTAMP, 126), 12, 13) as QRMTRTM, \n"
                + "(SELECT TOP 1 QIHMVT FROM QIHEAD WHERE QIHQNO = QIDQNO) as QRMMVT, \n"
                + "(SELECT TOP 1 QMVTRN FROM QMVT WHERE QMVMVT = (SELECT TOP 1 QIHMVT FROM QIHEAD WHERE QIHQNO = QIDQNO)) as QRMTRT,\n"
                + "QIDQNO as QRMDOCNO, \n"
                + "NULL as QRMSAPNO, \n"
                + "(QIDQTY*(-1)) as QRMQTY, \n"
                + "QIDBUN as QRMBUN, \n"
                + "CURRENT_TIMESTAMP as QRMCDT, \n"
                + "'" + user + "' as QRMUSER\n"
                + "FROM QIDETAIL\n"
                + "LEFT JOIN QRMMAS ON QIDETAIL.QIDID = QRMMAS.QRMID \n"
                + "WHERE QIDID = '" + id + "' ";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public List<QRMTRA> findByMc(String wh, String sd, String ed) {

        List<QRMTRA> MCNList = new ArrayList<QRMTRA>();

        String sql = "SELECT * FROM QRMTRA "
                + "WHERE QRMWHSE = ? "
                + "AND QRMTRDT BETWEEN ? AND ? ";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ps.setString(1, wh);
            ps.setString(2, sd);
            ps.setString(3, ed);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                QRMTRA p = new QRMTRA();

                p.setMatcode4(result.getString("QRMRUNNO"));

                String TranTime = result.getString("QRMTRTM");
                String date = result.getString("QRMTRDT");

                date = date.trim();
                String year = date.trim().substring(0, 4);
                String month = date.trim().substring(5, 7);
                String day = date.trim().substring(8, 10);
                date = day + "-" + month + "-" + year;

                p.setMatcode(date + " " + TranTime.substring(0, 8));
                p.setDesc(result.getString("QRMMVT"));
                p.setPlant(result.getString("QRMTRT"));
                p.setVtype(result.getString("QRMCODE"));
                String mat = result.getString("QRMCODE");

                SAPMAS_2Dao dao1 = new SAPMAS_2Dao();
                SAPMAS_2 ua = dao1.findAll(mat);

                p.setMtype(ua.getDesc());

                p.setUm(result.getString("QRMID"));
                p.setMgroup(result.getString("QRMDOCNO"));
                p.setMgdesc(result.getString("QRMSAPNO"));

                p.setMatcode1(ua.getMatCtrl());

                p.setMatcode2(result.getString("QRMQTY"));
                p.setPgroup(result.getString("QRMBUN"));

                String Cdt = result.getString("QRMCDT");
                Cdt = Cdt.trim();
                String cyear = Cdt.trim().substring(0, 4);
                String cmonth = Cdt.trim().substring(5, 7);
                String cday = Cdt.trim().substring(8, 10);
                String ctime = Cdt.trim().substring(11, 19);
                Cdt = cday + "-" + cmonth + "-" + cyear + " " + ctime;
                p.setMatcode3(Cdt);

                p.setLocation(result.getString("QRMUSER"));

                MCNList.add(p);

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return MCNList;

    }

    public QRMTRA findByRunno(String rn) {

        QRMTRA p = new QRMTRA();

        String sql = "SELECT * FROM QRMTRA "
                + "WHERE QRMRUNNO = ? ";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ps.setString(1, rn);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                p.setMatcode4(result.getString("QRMRUNNO"));

                String TranTime = result.getString("QRMTRTM");
                String date = result.getString("QRMTRDT");

                date = date.trim();
                String year = date.trim().substring(0, 4);
                String month = date.trim().substring(5, 7);
                String day = date.trim().substring(8, 10);
                date = day + "-" + month + "-" + year;

                p.setMatcode(date + " " + TranTime.substring(0, 8));
                p.setDesc(result.getString("QRMMVT"));
                p.setPlant(result.getString("QRMTRT"));
                p.setVtype(result.getString("QRMCODE"));
                String mat = result.getString("QRMCODE");

                SAPMAS_2Dao dao1 = new SAPMAS_2Dao();
                SAPMAS_2 ua = dao1.findAll(mat);

                p.setMtype(ua.getDesc());

                p.setUm(result.getString("QRMID"));
                p.setMgroup(result.getString("QRMDOCNO"));
                p.setMgdesc(result.getString("QRMSAPNO"));

                p.setMatcode1(ua.getMatCtrl());

                p.setMatcode2(result.getString("QRMQTY"));
                p.setPgroup(result.getString("QRMBUN"));
                p.setMatcode5(result.getString("QRMROLL"));

                String Cdt = result.getString("QRMCDT");
                Cdt = Cdt.trim();
                String cyear = Cdt.trim().substring(0, 4);
                String cmonth = Cdt.trim().substring(5, 7);
                String cday = Cdt.trim().substring(8, 10);
                String ctime = Cdt.trim().substring(11, 19);
                Cdt = cday + "-" + cmonth + "-" + cyear + " " + ctime;
                p.setMatcode3(Cdt);

                p.setLocation(result.getString("QRMUSER"));

                p.setMatcode6(result.getString("QRMSTORAGE"));
                p.setMatcode7(result.getString("QRMPLANT"));
                p.setMatcode8(result.getString("QRMVAL"));
                p.setMatcode9(ua.getMatCtrlName());
                p.setMatcode10(ua.getMgroup());
                p.setMatcode11(ua.getMgdesc());

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return p;

    }
}
