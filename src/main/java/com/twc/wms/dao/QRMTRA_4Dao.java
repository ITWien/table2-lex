/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.dao;

import com.twc.wms.database.database;
import com.twc.wms.entity.QRMTRA_2;
import java.sql.*;

import java.util.ArrayList;

import java.util.List;

import com.twc.wms.entity.QRMTRA_3;
import com.twc.wms.entity.UserAuth;

/**
 *
 * @author nutthawoot.noo
 */
public class QRMTRA_4Dao extends database {

    public List<QRMTRA_3> selectPlant() {

        List<QRMTRA_3> QMList = new ArrayList<QRMTRA_3>();

        String sql = "SELECT DISTINCT TOP 10 QRMPLANT FROM QRMTRA "
                + "WHERE (QRMMVT = '901' OR QRMMVT = '902') "
                + "ORDER BY QRMPLANT ";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                QRMTRA_3 p = new QRMTRA_3();

                p.setPlant(result.getString("QRMPLANT"));

                QMList.add(p);

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return QMList;

    }

    public List<QRMTRA_3> selectMatCode() {

        List<QRMTRA_3> QMList = new ArrayList<QRMTRA_3>();

        String sql = "SELECT DISTINCT TOP 10 QRMCODE FROM QRMTRA "
                + "WHERE (QRMMVT = '901' OR QRMMVT = '902') "
                + "ORDER BY QRMCODE ";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                QRMTRA_3 p = new QRMTRA_3();

                p.setMatCode(result.getString("QRMCODE"));

                QMList.add(p);

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return QMList;

    }

    public List<QRMTRA_3> selectUser() {

        List<QRMTRA_3> QMList = new ArrayList<QRMTRA_3>();

        String sql = "SELECT DISTINCT TOP 10 QRMUSER FROM QRMTRA "
                + "WHERE (QRMMVT = '901' OR QRMMVT = '902') "
                + "ORDER BY QRMUSER ";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                QRMTRA_3 p = new QRMTRA_3();

                p.setUser(result.getString("QRMUSER"));

                QMList.add(p);

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return QMList;

    }

    public ResultSet findTranDate(String StranDate, String FtranDate,
            String Swh, String Fwh, String Splant, String Fplant,
            String SmatCode, String FmatCode, String Suser, String Fuser) {

        List<QRMTRA_3> WHList = new ArrayList<QRMTRA_3>();

        String QRMWHSE = "";
        String QRMPLANT = "";
        String QRMCODE = "";
        String QRMUSER = "";

        if (!Swh.equals("")) {
            QRMWHSE = "AND (QRMWHSE BETWEEN '" + Swh + "' AND '" + Fwh + "') ";
        }
        if (!Splant.equals("")) {
            QRMPLANT = "AND (QRMPLANT BETWEEN '" + Splant + "' AND '" + Fplant + "') ";
        }
        if (!SmatCode.equals("")) {
            QRMCODE = "AND (QRMCODE BETWEEN '" + SmatCode + "' AND '" + FmatCode + "') ";
        }
        if (!Suser.equals("")) {
            QRMUSER = "AND (QRMUSER BETWEEN '" + Suser + "' AND '" + Fuser + "') ";
        }

        String sql = "SELECT QRMWHSE + ' : ' + (select QWHNAME from QWHMAS where QWHCOD = QRMWHSE) as QRMWHSE, QRMPLANT,\n"
                + "SUBSTRING(CONVERT(VARCHAR(25), QRMTRDT, 126), 9, 2) + '-' +\n"
                + "SUBSTRING(CONVERT(VARCHAR(25), QRMTRDT, 126), 6, 2) + '-' +\n"
                + "SUBSTRING(CONVERT(VARCHAR(25), QRMTRDT, 126), 1, 4) as QRMTRDT,\n"
                + "TRA.QRMCODE, "
                + "CASE WHEN (SELECT TOP 1 QRMDESC FROM QRMMAS WHERE QRMCODE = TRA.QRMCODE) IS NULL THEN\n"
                + "(SELECT TOP 1 SAPDESC FROM SAPMAS WHERE SAPMAT = TRA.QRMCODE AND SAPPLANT = TRA.QRMPLANT AND SAPVAL = TRA.QRMVAL) ELSE\n"
                + "(SELECT TOP 1 QRMDESC FROM QRMMAS WHERE QRMCODE = TRA.QRMCODE) END as NAME,\n"
                + "QRMVAL, QRMID,\n"
                + "(SELECT TOP 1 QRMPARENT FROM QRMMAS WHERE QRMID = TRA.QRMID) as PARENT, \n"
                + "QRMQTY as QTY, QRMBUN, QRMMVT, QRMTRT,\n"
                + "QRMUSER, (SELECT USERS FROM MSSUSER WHERE USERID = QRMUSER) as USERNAME\n"
                + "FROM QRMTRA TRA "
                + "WHERE (QRMTRDT BETWEEN '" + StranDate + "' AND '" + FtranDate + "') "
                + QRMWHSE
                + QRMPLANT
                + QRMCODE
                + QRMUSER
                + "AND (QRMMVT = '901' OR QRMMVT = '902') "
                + "AND (QRMTRT = 'SPIN' OR QRMTRT = 'SPOT')"
                + "ORDER BY QRMWHSE, QRMPLANT, QRMTRDT, QRMCODE, QRMCDT, QRMMVT DESC ";
//        String sql = "SELECT * FROM QRMTRA "
//                + "WHERE (QRMTRDT BETWEEN '" + StranDate + "' AND '" + FtranDate + "') "
//                + QRMWHSE
//                + QRMPLANT
//                + QRMCODE
//                + QRMUSER
//                + "AND (QRMMVT = '901' OR QRMMVT = '902') "
//                + "AND (QRMTRT = 'SPIN' OR QRMTRT = 'SPOT')"
//                + "ORDER BY QRMWHSE, QRMPLANT, QRMTRDT, QRMCODE, QRMCDT, QRMMVT DESC ";
        ResultSet result = null;
        try {

            PreparedStatement ps = connect.prepareStatement(sql);
            result = ps.executeQuery();

//            while (result.next()) {
//
//                QRMTRA_3 p = new QRMTRA_3();
//
//                WarehouseDao dao4 = new WarehouseDao();
//
//                p.setWH(result.getString("QRMWHSE") + " " + dao4.findWHname(result.getString("QRMWHSE")));
//                p.setPlant(result.getString("QRMPLANT"));
//
//                String trdDB = "";
//                if (result.getString("QRMTRDT") == null) {
//                    trdDB = "-";
//                } else {
//                    trdDB = result.getString("QRMTRDT");
//                }
//
//                String trDate = trdDB.substring(8, 10) + "-" + trdDB.substring(5, 7) + "-" + trdDB.substring(0, 4);
//                p.setTranDate(trDate);
//
//                p.setMatCode(result.getString("QRMCODE"));
//
//                String qty = result.getString("QRMQTY");
//                qty = qty.substring(0, qty.length() - 3);
//                p.setQty(qty);
//                p.setUm(result.getString("QRMBUN"));
//
//                QRMMASDao dao1 = new QRMMASDao();
//                QRMTRA_2 ms = dao1.findByCode(result.getString("QRMCODE"));
//
//                if (ms.getName() == null) {
//                    SAPMAS_2Dao dao5 = new SAPMAS_2Dao();
//                    QRMTRA_2 mas3 = dao5.findSAPDesc(result.getString("QRMCODE"),
//                            result.getString("QRMPLANT"), result.getString("QRMVAL"));
//                    p.setName(mas3.getName());
//                } else {
//                    p.setName(ms.getName());
//                }
//
//                p.setID(result.getString("QRMID"));
//                p.setPo(result.getString("QRMVAL"));//value
//                p.setPack(result.getString("QRMMVT"));//mvt
//                p.setNL(result.getString("QRMTRT"));//trt
//
//                QRMMASDao dao6 = new QRMMASDao();
//                QRMTRA_2 QM = dao6.findDESC(result.getString("QRMID"));
//                String parent = " ";
//                if (QM.getTax() != null) {
//                    parent = QM.getTax();
//                }
//                p.setTax(parent);//parent
//
//                UserDao dao2 = new UserDao();
//                UserAuth ua = dao2.findByUid(result.getString("QRMUSER"));
//
//                if (ua.getName() == null) {
//                    p.setUser(result.getString("QRMUSER"));
//                } else {
//                    String name = ua.getName();
//                    String[] parts = name.split(" ");
//                    String Fname = parts[0];
//
//                    p.setUser(result.getString("QRMUSER") + " : " + Fname);
//                }
//
//                WHList.add(p);
//
//            }
//
//            connect.close();
        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

}
