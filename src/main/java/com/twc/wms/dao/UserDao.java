/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.dao;

import com.twc.wms.database.database;

import java.sql.*;

import java.util.ArrayList;

import java.util.List;

import com.twc.wms.entity.UserAuth;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author nutthawoot.noo
 */
public class UserDao extends database {

    public List<UserAuth> findAll() {

        List<UserAuth> UAList = new ArrayList<UserAuth>();

        String sql = "SELECT * FROM MSSUSER "
                + "INNER JOIN QWHMAS ON MSSUSER.MSWHSE=QWHMAS.QWHCOD WHERE MSCONO = 'TWC' "
                + "ORDER BY USERID";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                UserAuth p = new UserAuth();

                p.setUid(result.getString("USERID"));

                p.setName(result.getString("USERS"));

                p.setLevel(Integer.toString(result.getInt("MSLEVEL")));

                p.setWarehouse(result.getString("MSWHSE") + " : " + result.getString("QWHNAME"));

                p.setDepartment(result.getString("MSDPARTNO"));

                p.setMenuset(result.getString("MSMENUSET"));

                UAList.add(p);

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public List<UserAuth> findAllIT() {

        List<UserAuth> UAList = new ArrayList<UserAuth>();

        String sql = "SELECT * FROM MSSUSER "
                + "INNER JOIN QWHMAS ON MSSUSER.MSWHSE=QWHMAS.QWHCOD "
                + "WHERE MSLEVEL > 8 "
                + "AND USERID != '90342' "
                + "ORDER BY USERID";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                UserAuth p = new UserAuth();

                p.setUid(result.getString("USERID"));

                p.setName(result.getString("USERS"));

                p.setLevel(Integer.toString(result.getInt("MSLEVEL")));

                p.setWarehouse(result.getString("MSWHSE") + " : " + result.getString("QWHNAME"));

                p.setDepartment(result.getString("MSDPARTNO"));

                p.setMenuset(result.getString("MSMENUSET"));

                UAList.add(p);

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public boolean checkLogin(String username, String password) {

        boolean result = false;

        String sql = "SELECT PASSWORD, MSLEVEL FROM MSSUSER"
                + " WHERE USERID = ? ";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ps.setString(1, username);

            ResultSet rs = ps.executeQuery();

            while (rs.next()) {

                if (password.equals(rs.getString("PASSWORD"))) {

                    if (rs.getInt("MSLEVEL") >= 7) {

                        result = true;

                    }

                } else {

                    result = false;

                }

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public String check(String id) {
        PreparedStatement ps = null;
        ResultSet rs = null;
        String STRRETURN = null;
        try {
            String check = "Select * FROM [RMShipment].[dbo].[MSSUSER] where [USERID] = '" + id + "'";
            ps = connect.prepareStatement(check);
            rs = ps.executeQuery();
            if (rs.next()) {
                STRRETURN = "f";
            } else {
                STRRETURN = "t";
            }
        } catch (Exception exp) {
            exp.printStackTrace();
        }
        return STRRETURN;
    }

    public boolean add(UserAuth ua, String uid, String path) {

        boolean result = false;

        String sql = "INSERT INTO MSSUSER"
                + " (MSCONO, MSWHSE, USERS, PASSWORD, USERID, MSLEVEL, MSDPARTNO, MSMENUSET, MSDATE, MSTIME, MSCHDATE, MSCHTIME, MSUSERRG, MSPICTURE)"
                + " VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, '" + path + "')";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ps.setString(1, "TWC");

            ps.setString(2, ua.getWarehouse());

            ps.setString(3, ua.getName());

            ps.setString(4, ua.getPassword());

            ps.setString(5, ua.getUid());

            ps.setString(6, ua.getLevel());

            ps.setString(7, ua.getDepartment());

            ps.setString(8, ua.getMenuset());

            Date date = new Date();
            DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
            DateFormat timeFormat = new SimpleDateFormat("HHmmss");

            String iDate = dateFormat.format(date);
            int conDate = Integer.parseInt(iDate);
            iDate = Integer.toString(conDate);
            String iTime = timeFormat.format(date);

            ps.setString(9, iDate);

            ps.setString(10, iTime);

            ps.setString(11, iDate);

            ps.setString(12, iTime);

            ps.setString(13, uid);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public UserAuth findByUid(String id) {

        UserAuth ua = new UserAuth();

        String sql = "SELECT * FROM MSSUSER WHERE USERID = ?";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ps.setString(1, id);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                ua.setUid(result.getString("USERID"));
                ua.setPassword(result.getString("PASSWORD"));
                ua.setName(result.getString("USERS"));
                ua.setLevel(result.getString("MSLEVEL"));
                ua.setWarehouse(result.getString("MSWHSE"));
                ua.setDepartment(result.getString("MSDPARTNO"));
                ua.setMenuset(result.getString("MSMENUSET"));
                if (result.getString("MSPICTURE").equals("NO IMAGE")) {
                    ua.setPath("male.jpg");
                } else {
                    ua.setPath(result.getString("MSPICTURE"));
                }

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return ua;

    }

    public boolean edit(UserAuth ua, String uid, String path) {

        boolean result = false;

        String sql = "UPDATE MSSUSER "
                + "SET PASSWORD = ?, USERS = ?, MSLEVEL = ?, MSDPARTNO = ?, MSMENUSET = ?, MSCHDATE = ?, MSCHTIME = ?, MSWHSE = ?, MSUSERRG = ?, MSPICTURE = '" + path + "' "
                + "WHERE USERID = ?";
        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ps.setString(10, ua.getUid());
            ps.setString(1, ua.getPassword());
            ps.setString(2, ua.getName());
            ps.setString(3, ua.getLevel());
            ps.setString(4, ua.getDepartment());
            ps.setString(5, ua.getMenuset());

            Date date = new Date();
            DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
            DateFormat timeFormat = new SimpleDateFormat("HHmmss");

            String iDate = dateFormat.format(date);
            int conDate = Integer.parseInt(iDate);
            iDate = Integer.toString(conDate);
            String iTime = timeFormat.format(date);

            ps.setString(6, iDate);
            ps.setString(7, iTime);
            ps.setString(8, ua.getWarehouse());
            ps.setString(9, uid);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public void delete(String uid) {

        String sql = "DELETE FROM MSSUSER WHERE USERID = ?";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ps.setString(1, uid);

            ps.executeUpdate();

        } catch (Exception e) {

            e.printStackTrace();

        }

    }
}
