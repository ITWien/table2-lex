/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.dao;

import com.twc.wms.database.database;
import com.twc.wms.entity.ISMMASD;
import com.twc.wms.entity.ISMMASH;
import com.twc.wms.entity.ISMPACD;
import com.twc.wms.entity.ISMPACK;
import com.twc.wms.entity.ISMSTAT;
import com.twc.wms.entity.ISMTAPK;
import com.twc.wms.entity.ISMTOPK;
import com.twc.wms.entity.WH;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

/**
 *
 * @author nutthawoot.noo
 */
public class ISM300Dao extends database {

    public String getCustomerName(String cus) {

        String cusName = "";

        String sql = "select top 1 NAME1 + ' ' + NAME2 as NAME from MSSMASH where POFG = '" + cus + "'";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                cusName = result.getString("NAME");

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return cusName;

    }

    public String getMatName(String mat) {

        String matName = "";

        String sql = "select MATCNAME from MSSMATN where LGPBE = '" + mat + "'";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                matName = result.getString("MATCNAME");

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return matName;

    }

    public List<String> getJobNoList(String cus) {

        List<String> UAList = new ArrayList<String>();

        String sql = "SELECT distinct [ISHJBNO]\n"
                + "FROM [ISMMASH]\n"
                + "where [ISHJBNO] is not null\n"
                + "and [ISHPOFG] = '" + cus + "'\n"
                + "order by [ISHJBNO] desc";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                String jobNo = result.getString("ISHJBNO");
                UAList.add(jobNo);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public List<String> getSeqNoList(String cus) {

        List<String> UAList = new ArrayList<String>();

        String sql = "SELECT distinct [ISHSEQ]\n"
                + "FROM [ISMMASH]\n"
                + "where [ISHSEQ] is not null\n"
                + "and [ISHPOFG] = '" + cus + "'\n"
                + "order by [ISHSEQ] desc";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                String seqNo = result.getString("ISHSEQ");
                UAList.add(seqNo);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public List<ISMMASD> getDetailList(String cus, String mat, String jobNo, String seqNo) {

        List<ISMMASD> UAList = new ArrayList<ISMMASD>();

        String sql = "";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

//                ISMMASD p = new ISMMASD();
//                String seqNo = result.getString("ISHSEQ");
//                UAList.add(seqNo);
            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public JSONObject changeSTSForBackward(String cus, String matctrl, String jobno, String seq, String matcode) {

        JSONObject res = new JSONObject();

        try {

            boolean p = true;

            Statement updSts = connect.createStatement();

            String sqlUpD = "UPDATE [RMShipment].[dbo].[ISMMASD] SET ISDSTS = '4', ISDJBNO = NULL, ISDPKQTY = NULL \n"
                    + "WHERE ISDITNO = '" + matcode + "' \n"
                    + "AND ISDJBNO = '" + jobno + "' \n"
                    + "AND ISDSEQ = '" + seq + "' \n"
                    + "AND ISDMTCTRL = '" + matctrl + "' AND ISDSTS = '5'\n";

            String sqlUpH = "UPDATE FROM [RMShipment].[dbo].[ISMMASH] SET ISHLSTS = '4' \n"
                    + "WHERE ISHPOFG = '" + cus + "' AND ISHJBNO = '" + jobno + "' \n"
                    + "AND ISHSEQ = '" + seq + "' \n"
                    + "AND ISHORD = (SELECT TOP 1 ISDORD FROM [RMShipment].[dbo].[ISMMASD] WHERE ISDITNO = '" + matcode + "' AND ISDJBNO = " + jobno + " AND ISDSEQ = '" + seq + "' AND ISDMTCTRL = '" + matctrl + "')";

            boolean reupd = updSts.execute(sqlUpD); //update detail Status by 
            boolean reuph = updSts.execute(sqlUpH); //update head Low Status by 

            res.put("upD", reupd);
            res.put("upHL", reuph);

            if (reupd) { //update detail complete

                Statement selSts = connect.createStatement();

                String sqlD = "SELECT DISTINCT ISDSTS FROM [RMShipment].[dbo].[ISMMASD] \n"
                        + "WHERE ISDJBNO = '" + jobno + "' AND ISDSEQ = '" + seq + "'\n"
                        + "ORDER BY ISDSTS DESC";
//                String sqlD = "SELECT * FROM [RMShipment].[dbo].[ISMMASD] WHERE ISDJBNO = '" + jobno + "' AND ISDSEQ = '" + seq + "'";
                ResultSet rs = selSts.executeQuery(sqlD);

                while (rs.next()) {
                    if (!rs.getString("ISDSTS").equals("4")) {
                        p = false;
                    }
                }

                if (p) { //Status all is 4
                    String sqlUpHH = "UPDATE FROM [RMShipment].[dbo].[ISMMASH] SET ISHHSTS = '4',ISHJBNO = NULL \n"
                            + "WHERE ISHPOFG = '" + cus + "' AND ISHJBNO = '" + jobno + "' \n"
                            + "AND ISHSEQ = '" + seq + "' \n"
                            + "AND ISHORD = (SELECT TOP 1 ISDORD FROM [RMShipment].[dbo].[ISMMASD] WHERE ISDITNO = '" + matcode + "' AND ISDJBNO = " + jobno + " AND ISDSEQ = '" + seq + "' AND ISDMTCTRL = '" + matctrl + "')";

                    boolean reuphh = updSts.execute(sqlUpHH); //update head High Status by , Clear Jobno

                    res.put("upHH", reuphh);

                }

                selSts.close();

            } else {
                res.put("upHH", false);
            }

            updSts.close();
            connect.close();

        } catch (SQLException ex) {
            Logger.getLogger(ISM300Dao.class.getName()).log(Level.SEVERE, null, ex);
        }

//        String sqlH = "SELECT * FROM [RMShipment].[dbo].[ISMMASH] \n"
//                + "WHERE ISHPOFG = '" + cus + "' AND ISHJBNO = '" + jobno + "' \n"
//                + "AND ISHSEQ = '" + seq + "' AND ISHORD = (SELECT TOP 1 ISDORD FROM [RMShipment].[dbo].[ISMMASD] WHERE ISDITNO = '" + matcode + "' AND ISDJBNO = '" + jobno + "' AND ISDSEQ = '" + seq + "' AND ISDMTCTRL = '" + matctrl + "')";
        return res;
    }

    public JSONObject delForBackward(String cus, String matctrl, String jobno, String seq, String matcode) {

        JSONObject res = new JSONObject();

        try {

            //---------- ISMPACK ------------
            Statement selCK = connect.createStatement();

            String sqlpack = "SELECT * FROM [RMShipment].[dbo].[ISMPACK] \n"
                    + "WHERE MTCTRL = '" + matctrl + "' AND GRPNO = '" + jobno + "' AND ISSUENO = '" + seq + "' \n"
                    + "AND MATNR = '" + matcode + "'";

            ResultSet rs = selCK.executeQuery(sqlpack);

            List<ISMPACK> listPack = new ArrayList<ISMPACK>();

            while (rs.next()) {
                ISMPACK p = new ISMPACK();
                p.setGRPNO(rs.getString("GRPNO"));
                p.setISSUENO(rs.getInt("ISSUENO"));

                listPack.add(p);
            }

            Statement delstmt = connect.createStatement();

            if (!listPack.isEmpty()) { //Found Data

                String sqlDelPack = "DELETE [RMShipment].[dbo].[ISMPACK] \n"
                        + "WHERE MTCTRL = '" + matctrl + "' AND GRPNO = '" + jobno + "' AND ISSUENO = '" + seq + "' \n"
                        + "AND MATNR = '" + matcode + "'";

                boolean del = delstmt.execute(sqlDelPack);

                res.put("delPack", del);

            } else {
                res.put("delPack", false);
            }

            //---------- ISMPACK ------------ END
            //---------- ISMSTAT ------------
            String sqlstat = "SELECT * FROM [RMShipment].[dbo].[ISMSTAT] "
                    + "WHERE GRPNO = '" + jobno + "' AND ISSUENO = '" + seq + "' AND MATNR = '" + matcode + "'";

            ResultSet rsstat = selCK.executeQuery(sqlstat);

            List<ISMSTAT> listStat = new ArrayList<ISMSTAT>();

            while (rsstat.next()) {
                ISMSTAT pstat = new ISMSTAT();
                pstat.setGRPNO(rsstat.getString("GRPNO"));
                listStat.add(pstat);
            }

            if (!listStat.isEmpty()) {
                String sqldelStat = "DELETE [RMShipment].[dbo].[ISMSTAT] "
                        + "WHERE GRPNO = '" + jobno + "' AND ISSUENO = '" + seq + "' AND MATNR = '" + matcode + "'";
                boolean delstat = delstmt.execute(sqldelStat);

                res.put("delStat", delstat);
            } else {
                res.put("delStat", false);
            }

            //---------- ISMSTAT ------------ END
            //---------- ISMTAPK ------------
            String sqltapk = "SELECT * FROM [RMShipment].[dbo].[ISMTAPK] "
                    + "WHERE GRPNO = '" + jobno + "' AND ISSUENO = '" + seq + "' AND MATNR = '" + matcode + "'";

            ResultSet rstapk = selCK.executeQuery(sqltapk);

            List<ISMTAPK> listtapk = new ArrayList<ISMTAPK>();

            while (rstapk.next()) {
                ISMTAPK ptapk = new ISMTAPK();
                ptapk.setGRPNO(rstapk.getString("GRPNO"));

                listtapk.add(ptapk);
            }

            if (!listtapk.isEmpty()) {
                String sqldeltapk = "DELETE [RMShipment].[dbo].[ISMTAPK] "
                        + "WHERE GRPNO = '" + jobno + "' AND ISSUENO = '" + seq + "' AND MATNR = '" + matcode + "'";

                boolean deltapk = delstmt.execute(sqldeltapk);

                res.put("delTapk", deltapk);
            } else {
                res.put("delTapk", false);
            }

            //---------- ISMTAPK ------------ END
            //---------- ISMPACD ------------
            String sqlpacd = "SELECT * FROM [RMShipment].[dbo].[ISMPACD] "
                    + "WHERE GRPNO = '" + jobno + "' AND ISSUENO = '" + seq + "' AND MATNR = '" + matcode + "'";

            ResultSet rspacd = selCK.executeQuery(sqlpacd);

            List<ISMPACD> listpacd = new ArrayList<ISMPACD>();

            while (rspacd.next()) {
                ISMPACD ppacd = new ISMPACD();
                ppacd.setGRPNO(rspacd.getString("GRPNO"));

                listpacd.add(ppacd);
            }

            if (!listpacd.isEmpty()) {
                String sqldelpacd = "DELETE [RMShipment].[dbo].[ISMPACD] "
                        + "WHERE GRPNO = '" + jobno + "' AND ISSUENO = '" + seq + "' AND MATNR = '" + matcode + "'";

                boolean delpacd = delstmt.execute(sqldelpacd);

                res.put("delPacd", delpacd);
            } else {
                res.put("delPacd", false);
            }

            //---------- ISMPACD ------------ END
            //---------- ISMTOPK ------------ 
            String sqltopk = "SELECT * FROM [RMShipment].[dbo].[ISMTOPK] "
                    + "WHERE GRPNO = '" + jobno + "' AND ISSUENO = '" + seq + "' AND MATNR = '" + matcode + "'";

            ResultSet rstopk = selCK.executeQuery(sqltopk);

            List<ISMTOPK> listtopk = new ArrayList<ISMTOPK>();

            while (rstopk.next()) {
                ISMTOPK ptopk = new ISMTOPK();
                ptopk.setGRPNO(rstopk.getString("GRPNO"));

                listtopk.add(ptopk);
            }

            if (!listtopk.isEmpty()) {
                String sqldeltopk = "DELETE [RMShipment].[dbo].[ISMTOPK] "
                        + "WHERE GRPNO = '" + jobno + "' AND ISSUENO = '" + seq + "' AND MATNR = '" + matcode + "'";

                boolean deltopk = delstmt.execute(sqldeltopk);

                res.put("delTopk", deltopk);
            } else {
                res.put("delTopk", false);
            }

            //---------- ISMTOPK ------------ END
            //delPack
            //delStat
            //delTapk
            //delPacd
            //delTopk
        } catch (SQLException ex) {
            Logger.getLogger(ISM300Dao.class.getName()).log(Level.SEVERE, null, ex);
        }

        return res;
    }

    public JSONObject UPDPick(String jobno, int seq, String matcode, String matctrl, float value) {

        JSONObject res = new JSONObject();

        try {

            Statement stmtSel = connect.createStatement();

            String sqlSel = "SELECT ISNULL(ISDPKQTY,0.0) AS ISDPKQTY FROM [RMShipment].[dbo].[ISMMASD] \n"
                    + "WHERE ISDITNO = '" + matcode + "' \n"
                    + "AND ISDJBNO = '" + jobno + "' \n"
                    + "AND ISDSEQ = '" + seq + "' \n"
                    + "AND ISDMTCTRL = '" + matctrl + "'\n";

            ResultSet DetObject = stmtSel.executeQuery(sqlSel);

            double pkqty = 0.0;

            while (DetObject.next()) {
                pkqty = DetObject.getDouble("ISDPKQTY"); //old Picking Qty
            }

            pkqty = pkqty + value;

            Statement updSts = connect.createStatement();

            String sqlUpD = "UPDATE [RMShipment].[dbo].[ISMMASD] SET ISDPKQTY = '" + pkqty + "' \n"
                    + "WHERE ISDITNO = '" + matcode + "' \n"
                    + "AND ISDJBNO = '" + jobno + "' \n"
                    + "AND ISDSEQ = '" + seq + "' \n"
                    + "AND ISDMTCTRL = '" + matctrl + "'\n";

            boolean reupd = updSts.execute(sqlUpD); //update detail Status by 

            res.put("upD", reupd);

            updSts.close();
            connect.close();

        } catch (SQLException ex) {
            Logger.getLogger(ISM300Dao.class.getName()).log(Level.SEVERE, null, ex);
        }

        return res;
    }

    public JSONObject DELAllPick(String jobno, int seq, String matcode, String matctrl) {

        JSONObject res = new JSONObject();

        try {

            Statement updSts = connect.createStatement();

            String sqlUpD = "UPDATE [RMShipment].[dbo].[ISMMASD] SET ISDPKQTY = '0' \n"
                    + "WHERE ISDITNO = '" + matcode + "' \n"
                    + "AND ISDJBNO = '" + jobno + "' \n"
                    + "AND ISDSEQ = '" + seq + "' \n"
                    + "AND ISDMTCTRL = '" + matctrl + "'\n";

            boolean reupd = updSts.execute(sqlUpD); //update detail Status by 

            res.put("upD", reupd);

            updSts.close();
            connect.close();

        } catch (SQLException ex) {
            Logger.getLogger(ISM300Dao.class.getName()).log(Level.SEVERE, null, ex);
        }

        return res;
    }

    public JSONObject DELLinePick(String jobno, int seq, String matcode, String matctrl, float QTYID) {

        JSONObject res = new JSONObject();

        try {

            Statement stmtSel = connect.createStatement();

            String sqlSel = "SELECT ISNULL(ISDPKQTY,0.0) AS ISDPKQTY FROM [RMShipment].[dbo].[ISMMASD] \n"
                    + "WHERE ISDITNO = '" + matcode + "' \n"
                    + "AND ISDJBNO = '" + jobno + "' \n"
                    + "AND ISDSEQ = '" + seq + "' \n"
                    + "AND ISDMTCTRL = '" + matctrl + "'\n";

            ResultSet DetObject = stmtSel.executeQuery(sqlSel);

            double pkqty = 0.0;

            while (DetObject.next()) {
                pkqty = DetObject.getDouble("ISDPKQTY"); //old Picking Qty
            }

            pkqty = pkqty - QTYID;

            Statement updSts = connect.createStatement();

            String sqlUpD = "UPDATE [RMShipment].[dbo].[ISMMASD] SET ISDPKQTY = '" + pkqty + "' \n"
                    + "WHERE ISDITNO = '" + matcode + "' \n"
                    + "AND ISDJBNO = '" + jobno + "' \n"
                    + "AND ISDSEQ = '" + seq + "' \n"
                    + "AND ISDMTCTRL = '" + matctrl + "'\n";

            boolean reupd = updSts.execute(sqlUpD); //update detail Status by 

            res.put("upD", reupd);

            updSts.close();
            connect.close();

        } catch (SQLException ex) {
            Logger.getLogger(ISM300Dao.class.getName()).log(Level.SEVERE, null, ex);
        }

        return res;
    }

}
