/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.dao;

import com.twc.wms.entity.SAPMAS;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import com.twc.wms.database.database;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author nutthawoot.noo
 */
public class SAPMASDao extends database {

    public List<SAPMAS> findByMc(String mc, String matcod) {

        List<SAPMAS> MCNList = new ArrayList<SAPMAS>();

        String sql = "SELECT * FROM SAPMAS WHERE SAPMCTRL = ? AND SAPMAT LIKE '%'+ ? +'%'";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ps.setString(1, mc);
            ps.setString(2, matcod);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                SAPMAS p = new SAPMAS();

                p.setMatcode(result.getString("SAPMAT"));
                p.setDesc(result.getString("SAPDESC"));
                p.setPlant(result.getString("SAPPLANT"));
                p.setVtype(result.getString("SAPVAL"));
                p.setMtype(result.getString("SAPMTYP"));
                p.setUm(result.getString("SAPBUN"));
                p.setMgroup(result.getString("SAPMGRP"));
                p.setMgdesc(result.getString("SAPMGRPDESC"));
                p.setMatCtrl(result.getString("SAPMCTRL"));
                p.setMatCtrlName(result.getString("SAPMCTRLDESC"));
                p.setPgroup(result.getString("SAPPGRP"));
                p.setPgroupName(result.getString("SAPPGRPNAME"));
                p.setLocation(result.getString("SAPLOCATION"));

                MCNList.add(p);

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return MCNList;

    }

    public boolean edit(SAPMAS ua, String uid) {

        boolean result = false;

        String sql = "UPDATE SAPMAS "
                + "SET SAPDESC = ?, SAPPLANT = ?, "
                + "SAPMTYP = ?, SAPBUN = ?, SAPMGRP = ?, SAPMGRPDESC = ?, "
                + "SAPMCTRL = ?, SAPMCTRLDESC = ?, SAPPGRP = ?, SAPPGRPNAME = ?, "
                + "SAPLOCATION = ?, SAPEDT = ?, SAPUSER = ?"
                + " WHERE SAPMAT = ? AND SAPVAL = ? AND SAPPLANT = ?";
        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ps.setString(1, ua.getDesc());
            ps.setString(2, ua.getPlant());

            ps.setString(3, ua.getMtype());
            ps.setString(4, ua.getUm());
            ps.setString(5, ua.getMgroup());
            ps.setString(6, ua.getMgdesc());
            ps.setString(7, ua.getMatCtrl());
            ps.setString(8, ua.getMatCtrlName());
            ps.setString(9, ua.getPgroup());
            ps.setString(10, ua.getPgroupName());
            ps.setString(11, ua.getLocation());

            Date date = new Date();
            DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");

            String iDate = dateFormat.format(date);
            int conDate = Integer.parseInt(iDate);
            iDate = Integer.toString(conDate);

            ps.setString(12, iDate);
            ps.setString(13, uid);
            ps.setString(14, ua.getMatcode());
            ps.setString(15, ua.getVtype());
            ps.setString(16, ua.getPlant());

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public SAPMAS findMcname(String mc) {

        SAPMAS p = new SAPMAS();

        String sql = "SELECT * FROM SAPMAS WHERE SAPMCTRL = ?";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ps.setString(1, mc);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                p.setMatCtrlName(result.getString("SAPMCTRLDESC"));

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return p;

    }

    public SAPMAS findAll(String mc, String vt, String plant) {

        SAPMAS p = new SAPMAS();

        String sql = "SELECT * FROM SAPMAS "
                + "WHERE SAPMAT = ? "
                + "AND SAPVAL = ? "
                + "AND SAPPLANT = ? ";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ps.setString(1, mc);
            ps.setString(2, vt);
            ps.setString(3, plant);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                p.setMatcode(result.getString("SAPMAT"));
                p.setDesc(result.getString("SAPDESC"));
                p.setPlant(result.getString("SAPPLANT"));
                p.setVtype(result.getString("SAPVAL"));
                p.setMtype(result.getString("SAPMTYP"));
                p.setUm(result.getString("SAPBUN"));
                p.setMgroup(result.getString("SAPMGRP"));
                p.setMgdesc(result.getString("SAPMGRPDESC"));
                p.setMatCtrl(result.getString("SAPMCTRL"));
                p.setMatCtrlName(result.getString("SAPMCTRLDESC"));
                p.setPgroup(result.getString("SAPPGRP"));
                p.setPgroupName(result.getString("SAPPGRPNAME"));
                p.setLocation(result.getString("SAPLOCATION"));

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return p;

    }

    public List<SAPMAS> findMC() {

        List<SAPMAS> MCList = new ArrayList<SAPMAS>();

        String sql = "SELECT DISTINCT SAPMCTRL, SAPMCTRLDESC "
                + "FROM SAPMAS "
                + "ORDER BY SAPMCTRL";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                SAPMAS p = new SAPMAS();

                p.setMatCtrl(result.getString("SAPMCTRL"));
                p.setMatCtrlName(result.getString("SAPMCTRLDESC"));

                MCList.add(p);

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return MCList;

    }
}
