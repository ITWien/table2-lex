/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.dao;

import com.twc.wms.database.database;

import java.sql.*;

import java.util.ArrayList;

import java.util.List;

import com.twc.wms.entity.WH;
import com.twc.wms.entity.QRMTRA;
import com.twc.wms.entity.WMS950;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author nutthawoot.noo
 */
public class WHDao extends database {

    public List<WMS950> findAll950RM(String WH, String PLANT, String MATCT, String RM, String ID) {

        List<WMS950> UAList = new ArrayList<WMS950>();

        String sql = "SELECT *\n"
                + "FROM(\n"
                + "SELECT QRMWHSE AS QWHCOD, QWHNAME AS QWHNAME, QRMPLANT AS PLANT, MATCTRL AS MATCT, RM, QRMCODE AS MATCO, SAPDESC AS MATDESC\n"
                + ",FORMAT(SUM(CASE WHEN QRMSTS IN ('1','2','3','4') THEN QRMQTY ELSE 0 END),'#,#0.00') AS QTY_1234\n"
                + ",FORMAT(SUM(CASE WHEN QRMSTS IN ('4','5') THEN QRMQTY ELSE 0 END),'#,#0.00') AS QTY_45\n"
                + ",FORMAT(SUM(CASE WHEN QRMSTS = '1' THEN QRMQTY ELSE 0 END),'#,#0.00') AS QTY_1\n"
                + ",FORMAT(SUM(CASE WHEN QRMSTS = '2' THEN QRMQTY ELSE 0 END),'#,#0.00') AS QTY_2\n"
                + ",FORMAT(SUM(CASE WHEN QRMSTS = '3' THEN QRMQTY ELSE 0 END),'#,#0.00') AS QTY_3\n"
                + ",FORMAT(SUM(CASE WHEN QRMSTS = '4' THEN QRMQTY ELSE 0 END),'#,#0.00') AS QTY_4\n"
                + ",FORMAT(SUM(CASE WHEN QRMSTS = '5' THEN QRMQTY ELSE 0 END),'#,#0.00') AS QTY_5\n"
                + ",FORMAT(SUM(CASE WHEN QRMSTS = '6' THEN QRMQTY ELSE 0 END),'#,#0.00') AS QTY_6\n"
                + ",FORMAT(SUM(CASE WHEN QRMSTS = '7' THEN QRMQTY ELSE 0 END),'#,#0.00') AS QTY_7\n"
                + ",FORMAT(SUM(CASE WHEN QRMSTS = 'O' THEN QRMQTY ELSE 0 END),'#,#0.00') AS QTY_O\n"
                + ",FORMAT(SUM(CASE WHEN QRMSTS = 'P' THEN QRMQTY ELSE 0 END),'#,#0.00') AS QTY_P\n"
                + "FROM(\n"
                + "SELECT QRMWHSE, QWHNAME, QRMPLANT\n"
                + ",(SELECT TOP 1 SAPMCTRL \n"
                + "         FROM SAPMAS WHERE SAPMAT = QRMCODE\n"
                + "		 AND SAPPLANT = QRMPLANT\n"
                + "		 AND SAPVAL = QRMVAL) AS MATCTRL\n"
                + ",LEFT(QRMCODE,8) AS RM, QRMCODE\n"
                + ",(SELECT TOP 1 SAPDESC \n"
                + "         FROM SAPMAS WHERE SAPMAT = QRMCODE\n"
                + "		 AND SAPPLANT = QRMPLANT\n"
                + "		 AND SAPVAL = QRMVAL) AS SAPDESC\n"
                + ",QRMSTS, QRMQTY\n"
                + "FROM QRMMAS\n"
                + "LEFT JOIN QWHMAS ON QWHMAS.QWHCOD = QRMMAS.QRMWHSE\n"
                + "WHERE QRMWHSE NOT IN ('','TWC')\n"
                + ")TWC1000\n"
                + "WHERE MATCTRL NOT IN ('')\n"
                + "AND RM NOT IN ('')\n"
                + "AND QRMCODE NOT IN ('')\n"
                + "AND QRMWHSE = '" + WH + "'\n"
                + "AND QRMPLANT = '" + PLANT + "'\n"
                + "AND MATCTRL = '" + MATCT + "'\n"
                + "AND RM = '" + RM + "'\n"
                + "GROUP BY QRMWHSE, QWHNAME, QRMPLANT, MATCTRL, RM, QRMCODE, SAPDESC\n"
                + ")PART4\n"
                + "LEFT JOIN MSSMATN ON MSSMATN.LGPBE = PART4.MATCT\n"
                + "WHERE QWHCOD = MSWHSE\n"
                + "--END PART 4\n"
                + "ORDER BY QWHCOD, QWHNAME, PLANT, MATCT, RM, MATCO, MATDESC";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            String wh = "";
            String plant = "";
            String matc = "";
            String rm = "";

            while (result.next()) {

                WMS950 p = new WMS950();

                p.setDT_RowId(ID);

//                if (wh.equals(result.getString("QWHCOD"))) {
                p.setWh("<b style=\"opacity: 0;\">" + result.getString("QWHCOD") + " : " + result.getString("QWHNAME") + "</b>");
//                    wh = result.getString("QWHCOD");
//                } else {
//                    p.setWh(result.getString("QWHCOD") + " : " + result.getString("QWHNAME"));
//                    wh = result.getString("QWHCOD");
//                }

//                if (plant.equals(result.getString("PLANT"))) {
                p.setPlant("<b style=\"opacity: 0;\">" + result.getString("PLANT") + "</b>");
//                    plant = result.getString("PLANT");
//                } else {
//                    p.setPlant(result.getString("PLANT"));
//                    plant = result.getString("PLANT");
//                }

//                if (matc.equals(result.getString("MATCT"))) {
                p.setMatCt("<b style=\"opacity: 0;\">" + result.getString("MATCT") + "</b>");
//                    matc = result.getString("MATCT");
//                } else {
//                    p.setMatCt(result.getString("MATCT"));
//                    matc = result.getString("MATCT");
//                }

//                if (rm.equals(result.getString("RM"))) {
                p.setRm("<b style=\"opacity: 0;\">" + result.getString("RM") + "</b>");
//                    rm = result.getString("RM");
//                } else {
//                    p.setRm(result.getString("RM"));
//                    rm = result.getString("RM");
//                }

                p.setWhN(result.getString("QWHCOD"));
                p.setPlantN(result.getString("PLANT"));
                p.setMatCtN(result.getString("MATCT"));
                p.setRmN(result.getString("RM"));
                p.setMatCoN(result.getString("MATCO"));
                p.setDescN(result.getString("MATDESC"));
                p.setIsSub("YES");

                if (result.getString("PLANT").equals("TOTAL")) {
                    p.setMatCo("<b>" + result.getString("MATCO") + "</b>");
                    p.setDesc("<b>" + result.getString("MATDESC") + "</b>");
                    p.setSts123("<b>" + result.getString("QTY_1234") + "</b>");
                    p.setSts456("<b>" + result.getString("QTY_45") + "</b>");
                    p.setSts1("<b>" + result.getString("QTY_1") + "</b>");
                    p.setSts2("<b>" + result.getString("QTY_2") + "</b>");
                    p.setSts3("<b>" + result.getString("QTY_3") + "</b>");
                    p.setSts4("<b>" + result.getString("QTY_4") + "</b>");
                    p.setSts5("<b>" + result.getString("QTY_5") + "</b>");
                    p.setSts6("<b>" + result.getString("QTY_6") + "</b>");
                    p.setSts7("<b>" + result.getString("QTY_7") + "</b>");
                    p.setStsO("<b>" + result.getString("QTY_O") + "</b>");
                    p.setStsP("<b>" + result.getString("QTY_P") + "</b>");
                } else {
                    p.setMatCo(result.getString("MATCO"));
                    p.setDesc(result.getString("MATDESC"));
                    p.setSts123(result.getString("QTY_1234"));
                    p.setSts456(result.getString("QTY_45"));
                    p.setSts1(result.getString("QTY_1"));
                    p.setSts2(result.getString("QTY_2"));
                    p.setSts3(result.getString("QTY_3"));
                    p.setSts4(result.getString("QTY_4"));
                    p.setSts5(result.getString("QTY_5"));
                    p.setSts6(result.getString("QTY_6"));
                    p.setSts7(result.getString("QTY_7"));
                    p.setStsO(result.getString("QTY_O"));
                    p.setStsP(result.getString("QTY_P"));
                }

                UAList.add(p);

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public List<WMS950> findAll950MatCt(String WH, String PLANT, String MATCT, String ID) {

        List<WMS950> UAList = new ArrayList<WMS950>();

        String sql = "SELECT *\n"
                + "FROM(\n"
                + "SELECT QRMWHSE AS QWHCOD, QWHNAME AS QWHNAME, QRMPLANT AS PLANT, MATCTRL AS MATCT, RM,'' AS MATCO,'' AS MATDESC\n"
                + ",FORMAT(SUM(CASE WHEN QRMSTS IN ('1','2','3','4') THEN QRMQTY ELSE 0 END),'#,#0.00') AS QTY_1234\n"
                + ",FORMAT(SUM(CASE WHEN QRMSTS IN ('4','5') THEN QRMQTY ELSE 0 END),'#,#0.00') AS QTY_45\n"
                + ",FORMAT(SUM(CASE WHEN QRMSTS = '1' THEN QRMQTY ELSE 0 END),'#,#0.00') AS QTY_1\n"
                + ",FORMAT(SUM(CASE WHEN QRMSTS = '2' THEN QRMQTY ELSE 0 END),'#,#0.00') AS QTY_2\n"
                + ",FORMAT(SUM(CASE WHEN QRMSTS = '3' THEN QRMQTY ELSE 0 END),'#,#0.00') AS QTY_3\n"
                + ",FORMAT(SUM(CASE WHEN QRMSTS = '4' THEN QRMQTY ELSE 0 END),'#,#0.00') AS QTY_4\n"
                + ",FORMAT(SUM(CASE WHEN QRMSTS = '5' THEN QRMQTY ELSE 0 END),'#,#0.00') AS QTY_5\n"
                + ",FORMAT(SUM(CASE WHEN QRMSTS = '6' THEN QRMQTY ELSE 0 END),'#,#0.00') AS QTY_6\n"
                + ",FORMAT(SUM(CASE WHEN QRMSTS = '7' THEN QRMQTY ELSE 0 END),'#,#0.00') AS QTY_7\n"
                + ",FORMAT(SUM(CASE WHEN QRMSTS = 'O' THEN QRMQTY ELSE 0 END),'#,#0.00') AS QTY_O\n"
                + ",FORMAT(SUM(CASE WHEN QRMSTS = 'P' THEN QRMQTY ELSE 0 END),'#,#0.00') AS QTY_P\n"
                + "FROM(\n"
                + "SELECT QRMWHSE, QWHNAME, QRMPLANT\n"
                + ",(SELECT TOP 1 SAPMCTRL \n"
                + "         FROM SAPMAS WHERE SAPMAT = QRMCODE\n"
                + "		 AND SAPPLANT = QRMPLANT\n"
                + "		 AND SAPVAL = QRMVAL) AS MATCTRL\n"
                + ",LEFT(QRMCODE,8) AS RM, QRMCODE\n"
                + ",(SELECT TOP 1 SAPDESC \n"
                + "         FROM SAPMAS WHERE SAPMAT = QRMCODE\n"
                + "		 AND SAPPLANT = QRMPLANT\n"
                + "		 AND SAPVAL = QRMVAL) AS SAPDESC\n"
                + ",QRMSTS, QRMQTY\n"
                + "FROM QRMMAS\n"
                + "LEFT JOIN QWHMAS ON QWHMAS.QWHCOD = QRMMAS.QRMWHSE\n"
                + "WHERE QRMWHSE NOT IN ('','TWC')\n"
                + ")TWC1000\n"
                + "WHERE MATCTRL NOT IN ('')\n"
                + "AND QRMWHSE = '" + WH + "'\n"
                + "AND QRMPLANT = '" + PLANT + "'\n"
                + "AND MATCTRL = '" + MATCT + "'\n"
                + "GROUP BY QRMWHSE, QWHNAME, QRMPLANT, MATCTRL, RM\n"
                + ")PART3\n"
                + "LEFT JOIN MSSMATN ON MSSMATN.LGPBE = PART3.MATCT\n"
                + "WHERE QWHCOD = MSWHSE\n"
                + "--END PART 3\n"
                + "ORDER BY QWHCOD, QWHNAME, PLANT, MATCT, RM, MATCO, MATDESC";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            String wh = "";
            String plant = "";
            String matc = "";
            String rm = "";

            while (result.next()) {

                WMS950 p = new WMS950();

                p.setDT_RowId(ID);

//                if (wh.equals(result.getString("QWHCOD"))) {
                p.setWh("<b style=\"opacity: 0;\">" + result.getString("QWHCOD") + " : " + result.getString("QWHNAME") + "</b>");
//                    wh = result.getString("QWHCOD");
//                } else {
//                    p.setWh(result.getString("QWHCOD") + " : " + result.getString("QWHNAME"));
//                    wh = result.getString("QWHCOD");
//                }

//                if (plant.equals(result.getString("PLANT"))) {
                p.setPlant("<b style=\"opacity: 0;\">" + result.getString("PLANT") + "</b>");
//                    plant = result.getString("PLANT");
//                } else {
//                    p.setPlant(result.getString("PLANT"));
//                    plant = result.getString("PLANT");
//                }

//                if (matc.equals(result.getString("MATCT"))) {
                p.setMatCt("<b style=\"opacity: 0;\">" + result.getString("MATCT") + "</b>");
//                    matc = result.getString("MATCT");
//                } else {
//                    p.setMatCt(result.getString("MATCT"));
//                    matc = result.getString("MATCT");
//                }

                if (rm.equals(result.getString("RM"))) {
                    p.setRm("<b style=\"opacity: 0;\">" + result.getString("RM") + "</b>");
                    rm = result.getString("RM");
                } else {
                    p.setRm(result.getString("RM"));
                    rm = result.getString("RM");
                }

                p.setWhN(result.getString("QWHCOD"));
                p.setPlantN(result.getString("PLANT"));
                p.setMatCtN(result.getString("MATCT"));
                p.setRmN(result.getString("RM"));
                p.setMatCoN(result.getString("MATCO"));
                p.setDescN(result.getString("MATDESC"));
                p.setIsSub("YES");

                if (result.getString("PLANT").equals("TOTAL")) {
                    p.setMatCo("<b>" + result.getString("MATCO") + "</b>");
                    p.setDesc("<b>" + result.getString("MATDESC") + "</b>");
                    p.setSts123("<b>" + result.getString("QTY_1234") + "</b>");
                    p.setSts456("<b>" + result.getString("QTY_45") + "</b>");
                    p.setSts1("<b>" + result.getString("QTY_1") + "</b>");
                    p.setSts2("<b>" + result.getString("QTY_2") + "</b>");
                    p.setSts3("<b>" + result.getString("QTY_3") + "</b>");
                    p.setSts4("<b>" + result.getString("QTY_4") + "</b>");
                    p.setSts5("<b>" + result.getString("QTY_5") + "</b>");
                    p.setSts6("<b>" + result.getString("QTY_6") + "</b>");
                    p.setSts7("<b>" + result.getString("QTY_7") + "</b>");
                    p.setStsO("<b>" + result.getString("QTY_O") + "</b>");
                    p.setStsP("<b>" + result.getString("QTY_P") + "</b>");
                } else {
                    p.setMatCo(result.getString("MATCO"));
                    p.setDesc(result.getString("MATDESC"));
                    p.setSts123(result.getString("QTY_1234"));
                    p.setSts456(result.getString("QTY_45"));
                    p.setSts1(result.getString("QTY_1"));
                    p.setSts2(result.getString("QTY_2"));
                    p.setSts3(result.getString("QTY_3"));
                    p.setSts4(result.getString("QTY_4"));
                    p.setSts5(result.getString("QTY_5"));
                    p.setSts6(result.getString("QTY_6"));
                    p.setSts7(result.getString("QTY_7"));
                    p.setStsO(result.getString("QTY_O"));
                    p.setStsP(result.getString("QTY_P"));
                }

                UAList.add(p);

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public List<WMS950> findAll950Plant(String WH, String PLANT, String ID) {

        List<WMS950> UAList = new ArrayList<WMS950>();

        String sql = "SELECT *\n"
                + "FROM(\n"
                + "SELECT QRMWHSE AS QWHCOD, QWHNAME AS QWHNAME, QRMPLANT AS PLANT, MATCTRL AS MATCT,'' AS RM,'' AS MATCO,'' AS MATDESC\n"
                + ",FORMAT(SUM(CASE WHEN QRMSTS IN ('1','2','3','4') THEN QRMQTY ELSE 0 END),'#,#0.00') AS QTY_1234\n"
                + ",FORMAT(SUM(CASE WHEN QRMSTS IN ('4','5') THEN QRMQTY ELSE 0 END),'#,#0.00') AS QTY_45\n"
                + ",FORMAT(SUM(CASE WHEN QRMSTS = '1' THEN QRMQTY ELSE 0 END),'#,#0.00') AS QTY_1\n"
                + ",FORMAT(SUM(CASE WHEN QRMSTS = '2' THEN QRMQTY ELSE 0 END),'#,#0.00') AS QTY_2\n"
                + ",FORMAT(SUM(CASE WHEN QRMSTS = '3' THEN QRMQTY ELSE 0 END),'#,#0.00') AS QTY_3\n"
                + ",FORMAT(SUM(CASE WHEN QRMSTS = '4' THEN QRMQTY ELSE 0 END),'#,#0.00') AS QTY_4\n"
                + ",FORMAT(SUM(CASE WHEN QRMSTS = '5' THEN QRMQTY ELSE 0 END),'#,#0.00') AS QTY_5\n"
                + ",FORMAT(SUM(CASE WHEN QRMSTS = '6' THEN QRMQTY ELSE 0 END),'#,#0.00') AS QTY_6\n"
                + ",FORMAT(SUM(CASE WHEN QRMSTS = '7' THEN QRMQTY ELSE 0 END),'#,#0.00') AS QTY_7\n"
                + ",FORMAT(SUM(CASE WHEN QRMSTS = 'O' THEN QRMQTY ELSE 0 END),'#,#0.00') AS QTY_O\n"
                + ",FORMAT(SUM(CASE WHEN QRMSTS = 'P' THEN QRMQTY ELSE 0 END),'#,#0.00') AS QTY_P\n"
                + "FROM(\n"
                + "SELECT QRMWHSE, QWHNAME, QRMPLANT\n"
                + ",(SELECT TOP 1 SAPMCTRL \n"
                + "         FROM SAPMAS WHERE SAPMAT = QRMCODE\n"
                + "		 AND SAPPLANT = QRMPLANT\n"
                + "		 AND SAPVAL = QRMVAL) AS MATCTRL\n"
                + ",LEFT(QRMCODE,8) AS RM, QRMCODE\n"
                + ",(SELECT TOP 1 SAPDESC \n"
                + "         FROM SAPMAS WHERE SAPMAT = QRMCODE\n"
                + "		 AND SAPPLANT = QRMPLANT\n"
                + "		 AND SAPVAL = QRMVAL) AS SAPDESC\n"
                + ",QRMSTS, QRMQTY\n"
                + "FROM QRMMAS\n"
                + "LEFT JOIN QWHMAS ON QWHMAS.QWHCOD = QRMMAS.QRMWHSE\n"
                + "WHERE QRMWHSE NOT IN ('','TWC')\n"
                + ")TWC1000\n"
                + "WHERE MATCTRL NOT IN ('')\n"
                + "AND QRMWHSE = '" + WH + "'\n"
                + "AND QRMPLANT = '" + PLANT + "'\n"
                + "GROUP BY QRMWHSE, QWHNAME, QRMPLANT, MATCTRL\n"
                + ")PART2\n"
                + "LEFT JOIN MSSMATN ON MSSMATN.LGPBE = PART2.MATCT\n"
                + "WHERE QWHCOD = MSWHSE\n"
                + "--END PART 2\n"
                + "ORDER BY QWHCOD, QWHNAME, PLANT, MATCT, RM, MATCO, MATDESC";

//        System.out.println(sql);
        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            String wh = "";
            String plant = "";
            String matc = "";
            String rm = "";

            while (result.next()) {

                WMS950 p = new WMS950();

                p.setDT_RowId(ID);

//                if (wh.equals(result.getString("QWHCOD"))) {
                p.setWh("<b style=\"opacity: 0;\">" + result.getString("QWHCOD") + " : " + result.getString("QWHNAME") + "</b>");
//                    wh = result.getString("QWHCOD");
//                } else {
//                    p.setWh(result.getString("QWHCOD") + " : " + result.getString("QWHNAME"));
//                    wh = result.getString("QWHCOD");
//                }

//                if (plant.equals(result.getString("PLANT"))) {
                p.setPlant("<b style=\"opacity: 0;\">" + result.getString("PLANT") + "</b>");
//                    plant = result.getString("PLANT");
//                } else {
//                    p.setPlant(result.getString("PLANT"));
//                    plant = result.getString("PLANT");
//                }

                if (matc.equals(result.getString("MATCT"))) {
                    p.setMatCt("<b style=\"opacity: 0;\">" + result.getString("MATCT") + "</b>");
                    matc = result.getString("MATCT");
                } else {
                    p.setMatCt(result.getString("MATCT"));
                    matc = result.getString("MATCT");
                }

                if (rm.equals(result.getString("RM"))) {
                    p.setRm("<b style=\"opacity: 0;\">" + result.getString("RM") + "</b>");
                    rm = result.getString("RM");
                } else {
                    p.setRm(result.getString("RM"));
                    rm = result.getString("RM");
                }

                p.setWhN(result.getString("QWHCOD"));
                p.setPlantN(result.getString("PLANT"));
                p.setMatCtN(result.getString("MATCT"));
                p.setRmN(result.getString("RM"));
                p.setMatCoN(result.getString("MATCO"));
                p.setDescN(result.getString("MATDESC"));
                p.setIsSub("YES");

                if (result.getString("PLANT").equals("TOTAL")) {
                    p.setMatCo("<b>" + result.getString("MATCO") + "</b>");
                    p.setDesc("<b>" + result.getString("MATDESC") + "</b>");
                    p.setSts123("<b>" + result.getString("QTY_1234") + "</b>");
                    p.setSts456("<b>" + result.getString("QTY_45") + "</b>");
                    p.setSts1("<b>" + result.getString("QTY_1") + "</b>");
                    p.setSts2("<b>" + result.getString("QTY_2") + "</b>");
                    p.setSts3("<b>" + result.getString("QTY_3") + "</b>");
                    p.setSts4("<b>" + result.getString("QTY_4") + "</b>");
                    p.setSts5("<b>" + result.getString("QTY_5") + "</b>");
                    p.setSts6("<b>" + result.getString("QTY_6") + "</b>");
                    p.setSts7("<b>" + result.getString("QTY_7") + "</b>");
                    p.setStsO("<b>" + result.getString("QTY_O") + "</b>");
                    p.setStsP("<b>" + result.getString("QTY_P") + "</b>");
                } else {
                    p.setMatCo(result.getString("MATCO"));
                    p.setDesc(result.getString("MATDESC"));
                    p.setSts123(result.getString("QTY_1234"));
                    p.setSts456(result.getString("QTY_45"));
                    p.setSts1(result.getString("QTY_1"));
                    p.setSts2(result.getString("QTY_2"));
                    p.setSts3(result.getString("QTY_3"));
                    p.setSts4(result.getString("QTY_4"));
                    p.setSts5(result.getString("QTY_5"));
                    p.setSts6(result.getString("QTY_6"));
                    p.setSts7(result.getString("QTY_7"));
                    p.setStsO(result.getString("QTY_O"));
                    p.setStsP(result.getString("QTY_P"));
                }

                UAList.add(p);

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public List<WMS950> findAll950RMOld(String wh, String plant, String whn, String matct) {

        List<WMS950> UAList = new ArrayList<WMS950>();

        String sql = "DECLARE @WH NVARCHAR(10) = '" + wh + "' \n"
                + "DECLARE @PLANT NVARCHAR(10) = '" + plant + "' \n"
                + "DECLARE @MATCT NVARCHAR(10) = '" + matct + "'\n"
                + "SELECT RM\n"
                + ",FORMAT(ISNULL((SELECT SUM(QRMQTY) FROM QRMMAS \n"
                + "	WHERE QRMSTS IN ('1','2','3') AND QRMWHSE = @WH \n"
                + "	AND QRMPLANT = @PLANT\n"
                + "	AND (SELECT TOP 1 SAPMCTRL FROM SAPMAS \n"
                + "	WHERE SAPMAT = QRMCODE AND SAPPLANT = QRMPLANT) = MATCTRL\n"
                + "	AND LEFT(QRMCODE,8) = RM),0),'#,#0.00') AS QTY_123\n"
                + ",FORMAT(ISNULL((SELECT SUM(QRMQTY) FROM QRMMAS \n"
                + "	WHERE QRMSTS IN ('4','5','6') AND QRMWHSE = @WH \n"
                + "	AND QRMPLANT = @PLANT\n"
                + "	AND (SELECT TOP 1 SAPMCTRL FROM SAPMAS \n"
                + "	WHERE SAPMAT = QRMCODE AND SAPPLANT = QRMPLANT) = MATCTRL\n"
                + "	AND LEFT(QRMCODE,8) = RM),0),'#,#0.00') AS QTY_456\n"
                + ",FORMAT(ISNULL((SELECT SUM(QRMQTY) FROM QRMMAS \n"
                + "	WHERE QRMSTS = '1' AND QRMWHSE = @WH \n"
                + "	AND QRMPLANT = @PLANT\n"
                + "	AND (SELECT TOP 1 SAPMCTRL FROM SAPMAS \n"
                + "	WHERE SAPMAT = QRMCODE AND SAPPLANT = QRMPLANT) = MATCTRL\n"
                + "	AND LEFT(QRMCODE,8) = RM),0),'#,#0.00') AS QTY_1\n"
                + ",FORMAT(ISNULL((SELECT SUM(QRMQTY) FROM QRMMAS \n"
                + "	WHERE QRMSTS = '2' AND QRMWHSE = @WH \n"
                + "	AND QRMPLANT = @PLANT\n"
                + "	AND (SELECT TOP 1 SAPMCTRL FROM SAPMAS \n"
                + "	WHERE SAPMAT = QRMCODE AND SAPPLANT = QRMPLANT) = MATCTRL\n"
                + "	AND LEFT(QRMCODE,8) = RM),0),'#,#0.00') AS QTY_2\n"
                + ",FORMAT(ISNULL((SELECT SUM(QRMQTY) FROM QRMMAS \n"
                + "	WHERE QRMSTS = '3' AND QRMWHSE = @WH \n"
                + "	AND QRMPLANT = @PLANT\n"
                + "	AND (SELECT TOP 1 SAPMCTRL FROM SAPMAS \n"
                + "	WHERE SAPMAT = QRMCODE AND SAPPLANT = QRMPLANT) = MATCTRL\n"
                + "	AND LEFT(QRMCODE,8) = RM),0),'#,#0.00') AS QTY_3\n"
                + ",FORMAT(ISNULL((SELECT SUM(QRMQTY) FROM QRMMAS \n"
                + "	WHERE QRMSTS = '4' AND QRMWHSE = @WH \n"
                + "	AND QRMPLANT = @PLANT\n"
                + "	AND (SELECT TOP 1 SAPMCTRL FROM SAPMAS \n"
                + "	WHERE SAPMAT = QRMCODE AND SAPPLANT = QRMPLANT) = MATCTRL\n"
                + "	AND LEFT(QRMCODE,8) = RM),0),'#,#0.00') AS QTY_4\n"
                + ",FORMAT(ISNULL((SELECT SUM(QRMQTY) FROM QRMMAS \n"
                + "	WHERE QRMSTS = '5' AND QRMWHSE = @WH \n"
                + "	AND QRMPLANT = @PLANT\n"
                + "	AND (SELECT TOP 1 SAPMCTRL FROM SAPMAS \n"
                + "	WHERE SAPMAT = QRMCODE AND SAPPLANT = QRMPLANT) = MATCTRL\n"
                + "	AND LEFT(QRMCODE,8) = RM),0),'#,#0.00') AS QTY_5\n"
                + ",FORMAT(ISNULL((SELECT SUM(QRMQTY) FROM QRMMAS \n"
                + "	WHERE QRMSTS = '6' AND QRMWHSE = @WH \n"
                + "	AND QRMPLANT = @PLANT\n"
                + "	AND (SELECT TOP 1 SAPMCTRL FROM SAPMAS \n"
                + "	WHERE SAPMAT = QRMCODE AND SAPPLANT = QRMPLANT) = MATCTRL\n"
                + "	AND LEFT(QRMCODE,8) = RM),0),'#,#0.00') AS QTY_6\n"
                + ",FORMAT(ISNULL((SELECT SUM(QRMQTY) FROM QRMMAS \n"
                + "	WHERE QRMSTS = '7' AND QRMWHSE = @WH \n"
                + "	AND QRMPLANT = @PLANT\n"
                + "	AND (SELECT TOP 1 SAPMCTRL FROM SAPMAS \n"
                + "	WHERE SAPMAT = QRMCODE AND SAPPLANT = QRMPLANT) = MATCTRL\n"
                + "	AND LEFT(QRMCODE,8) = RM),0),'#,#0.00') AS QTY_7\n"
                + ",FORMAT(ISNULL((SELECT SUM(QRMQTY) FROM QRMMAS \n"
                + "	WHERE QRMSTS = 'O' AND QRMWHSE = @WH \n"
                + "	AND QRMPLANT = @PLANT\n"
                + "	AND (SELECT TOP 1 SAPMCTRL FROM SAPMAS \n"
                + "	WHERE SAPMAT = QRMCODE AND SAPPLANT = QRMPLANT) = MATCTRL\n"
                + "	AND LEFT(QRMCODE,8) = RM),0),'#,#0.00') AS QTY_O\n"
                + ",FORMAT(ISNULL((SELECT SUM(QRMQTY) FROM QRMMAS \n"
                + "	WHERE QRMSTS = 'P' AND QRMWHSE = @WH \n"
                + "	AND QRMPLANT = @PLANT\n"
                + "	AND (SELECT TOP 1 SAPMCTRL FROM SAPMAS \n"
                + "	WHERE SAPMAT = QRMCODE AND SAPPLANT = QRMPLANT) = MATCTRL\n"
                + "	AND LEFT(QRMCODE,8) = RM),0),'#,#0.00') AS QTY_P\n"
                + "FROM(\n"
                + "SELECT DISTINCT QRMWHSE, QRMPLANT\n"
                + ",(SELECT TOP 1 SAPMCTRL FROM SAPMAS WHERE SAPMAT = QRMCODE AND SAPPLANT = QRMPLANT) AS MATCTRL\n"
                + ",LEFT(QRMCODE,8) AS RM\n"
                + "FROM QRMMAS\n"
                + "WHERE QRMWHSE = @WH\n"
                + "AND QRMPLANT = @PLANT\n"
                + ")TMP\n"
                + "WHERE MATCTRL = @MATCT\n"
                + "ORDER BY MATCTRL";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                WMS950 p = new WMS950();

                p.setWh("<b style=\"opacity: 0;\">" + whn + "</b>");
                p.setPlant("<b style=\"opacity: 0;\">" + plant + "</b>");
                p.setMatCt("<b style=\"opacity: 0;\">" + matct + "</b>");

                p.setRm(result.getString("RM"));
                p.setSts123(result.getString("QTY_123"));
                p.setSts456(result.getString("QTY_456"));
                p.setSts1(result.getString("QTY_1"));
                p.setSts2(result.getString("QTY_2"));
                p.setSts3(result.getString("QTY_3"));
                p.setSts4(result.getString("QTY_4"));
                p.setSts5(result.getString("QTY_5"));
                p.setSts6(result.getString("QTY_6"));
                p.setSts7(result.getString("QTY_7"));
                p.setStsO(result.getString("QTY_O"));
                p.setStsP(result.getString("QTY_P"));

                UAList.add(p);

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public List<WMS950> findAll950MatCt() {

        List<WMS950> UAList = new ArrayList<WMS950>();

        String sql = "SELECT QRMWHSE, QRMPLANT, MATCTRL\n"
                + ",SUM(CASE WHEN QRMSTS IN ('1','2','') THEN QRMQTY ELSE 0 END) AS QTY_123\n"
                + ",SUM(CASE WHEN QRMSTS IN ('4','5') THEN QRMQTY ELSE 0 END) AS QTY_45\n"
                + ",SUM(CASE WHEN QRMSTS = '1' THEN QRMQTY ELSE 0 END) AS QTY_1\n"
                + ",SUM(CASE WHEN QRMSTS = '2' THEN QRMQTY ELSE 0 END) AS QTY_2\n"
                + ",SUM(CASE WHEN QRMSTS = '3' THEN QRMQTY ELSE 0 END) AS QTY_3\n"
                + ",SUM(CASE WHEN QRMSTS = '4' THEN QRMQTY ELSE 0 END) AS QTY_4\n"
                + ",SUM(CASE WHEN QRMSTS = '5' THEN QRMQTY ELSE 0 END) AS QTY_5\n"
                + ",SUM(CASE WHEN QRMSTS = '6' THEN QRMQTY ELSE 0 END) AS QTY_6\n"
                + ",SUM(CASE WHEN QRMSTS = '7' THEN QRMQTY ELSE 0 END) AS QTY_7\n"
                + ",SUM(CASE WHEN QRMSTS = 'O' THEN QRMQTY ELSE 0 END) AS QTY_O\n"
                + ",SUM(CASE WHEN QRMSTS = 'P' THEN QRMQTY ELSE 0 END) AS QTY_P\n"
                + "FROM(\n"
                + "SELECT QRMWHSE+' : '+QWHNAME AS QRMWHSE, QRMPLANT\n"
                + ",(SELECT TOP 1 SAPMCTRL \n"
                + "         FROM SAPMAS WHERE SAPMAT = QRMCODE\n"
                + "		 AND SAPPLANT = QRMPLANT\n"
                + "		 AND SAPVAL = QRMVAL) AS MATCTRL\n"
                + ",LEFT(QRMCODE,8) AS RM, QRMCODE\n"
                + ",(SELECT TOP 1 SAPDESC \n"
                + "         FROM SAPMAS WHERE SAPMAT = QRMCODE\n"
                + "		 AND SAPPLANT = QRMPLANT\n"
                + "		 AND SAPVAL = QRMVAL) AS SAPDESC\n"
                + ",QRMSTS, QRMQTY\n"
                + "FROM QRMMAS\n"
                + "LEFT JOIN QWHMAS ON QWHMAS.QWHCOD = QRMMAS.QRMWHSE\n"
                + "WHERE QRMWHSE NOT IN ('','TWC')\n"
                + ")TWC1000\n"
                + "GROUP BY QRMWHSE, QRMPLANT, MATCTRL\n"
                + "ORDER BY QRMWHSE, QRMPLANT, MATCTRL";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                WMS950 p = new WMS950();

                p.setWh("<b style=\"opacity: 1;\">" + result.getString("QRMWHSE") + "</b>");
                p.setPlant("<b style=\"opacity: 1;\">" + result.getString("QRMPLANT") + "</b>");

                p.setMatCt(result.getString("MATCTRL"));
                p.setSts123(result.getString("QTY_123"));
                p.setSts456(result.getString("QTY_45"));
                p.setSts1(result.getString("QTY_1"));
                p.setSts2(result.getString("QTY_2"));
                p.setSts3(result.getString("QTY_3"));
                p.setSts4(result.getString("QTY_4"));
                p.setSts5(result.getString("QTY_5"));
                p.setSts6(result.getString("QTY_6"));
                p.setSts7(result.getString("QTY_7"));
                p.setStsO(result.getString("QTY_O"));
                p.setStsP(result.getString("QTY_P"));

                UAList.add(p);

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public List<WMS950> findAll950() {

        List<WMS950> UAList = new ArrayList<WMS950>();

        String sql = "SELECT QWHCOD, QWHNAME, 'TOTAL' AS PLANT,'' AS MATCT,'' AS RM,'' AS MATCO,'' AS MATDESC\n"
                + ",FORMAT(ISNULL(CASE WHEN QWHCOD = 'TWC' \n"
                + "      THEN (SELECT SUM(QRMQTY) FROM QRMMAS LEFT JOIN MSSMATN ON MSSMATN.LGPBE = (SELECT TOP 1 SAPMCTRL FROM SAPMAS WHERE SAPMAT = QRMCODE AND SAPPLANT = QRMPLANT AND SAPVAL = QRMVAL)\n"
                + "	       WHERE QRMSTS IN ('1','2','3','4') AND QRMWHSE IS NOT NULL AND QRMWHSE != 'TWC' AND QRMPLANT IN ('1000','1050') AND QRMWHSE = MSWHSE)\n"
                + "	  ELSE (SELECT SUM(QRMQTY) FROM QRMMAS LEFT JOIN MSSMATN ON MSSMATN.LGPBE = (SELECT TOP 1 SAPMCTRL FROM SAPMAS WHERE SAPMAT = QRMCODE AND SAPPLANT = QRMPLANT AND SAPVAL = QRMVAL)\n"
                + "	       WHERE QRMSTS IN ('1','2','3','4') AND QRMWHSE = QWHCOD AND QRMPLANT IN ('1000','1050') AND QRMWHSE = MSWHSE)\n"
                + "	  END,0),'#,#0.00') AS QTY_1234\n"
                + ",FORMAT(ISNULL(CASE WHEN QWHCOD = 'TWC' \n"
                + "      THEN (SELECT SUM(QRMQTY) FROM QRMMAS LEFT JOIN MSSMATN ON MSSMATN.LGPBE = (SELECT TOP 1 SAPMCTRL FROM SAPMAS WHERE SAPMAT = QRMCODE AND SAPPLANT = QRMPLANT AND SAPVAL = QRMVAL)\n"
                + "	       WHERE QRMSTS IN ('4','5') AND QRMWHSE IS NOT NULL AND QRMWHSE != 'TWC' AND QRMPLANT IN ('1000','1050') AND QRMWHSE = MSWHSE)\n"
                + "	  ELSE (SELECT SUM(QRMQTY) FROM QRMMAS LEFT JOIN MSSMATN ON MSSMATN.LGPBE = (SELECT TOP 1 SAPMCTRL FROM SAPMAS WHERE SAPMAT = QRMCODE AND SAPPLANT = QRMPLANT AND SAPVAL = QRMVAL)\n"
                + "	       WHERE QRMSTS IN ('4','5') AND QRMWHSE = QWHCOD AND QRMPLANT IN ('1000','1050') AND QRMWHSE = MSWHSE)\n"
                + "	  END,0),'#,#0.00') AS QTY_45\n"
                + ",FORMAT(ISNULL(CASE WHEN QWHCOD = 'TWC' \n"
                + "      THEN (SELECT SUM(QRMQTY) FROM QRMMAS LEFT JOIN MSSMATN ON MSSMATN.LGPBE = (SELECT TOP 1 SAPMCTRL FROM SAPMAS WHERE SAPMAT = QRMCODE AND SAPPLANT = QRMPLANT AND SAPVAL = QRMVAL)\n"
                + "	       WHERE QRMSTS = '1' AND QRMWHSE IS NOT NULL AND QRMWHSE != 'TWC' AND QRMPLANT IN ('1000','1050') AND QRMWHSE = MSWHSE)\n"
                + "	  ELSE (SELECT SUM(QRMQTY) FROM QRMMAS LEFT JOIN MSSMATN ON MSSMATN.LGPBE = (SELECT TOP 1 SAPMCTRL FROM SAPMAS WHERE SAPMAT = QRMCODE AND SAPPLANT = QRMPLANT AND SAPVAL = QRMVAL)\n"
                + "	       WHERE QRMSTS = '1' AND QRMWHSE = QWHCOD AND QRMPLANT IN ('1000','1050') AND QRMWHSE = MSWHSE)\n"
                + "	  END,0),'#,#0.00') AS QTY_1\n"
                + ",FORMAT(ISNULL(CASE WHEN QWHCOD = 'TWC' \n"
                + "      THEN (SELECT SUM(QRMQTY) FROM QRMMAS LEFT JOIN MSSMATN ON MSSMATN.LGPBE = (SELECT TOP 1 SAPMCTRL FROM SAPMAS WHERE SAPMAT = QRMCODE AND SAPPLANT = QRMPLANT AND SAPVAL = QRMVAL)\n"
                + "	       WHERE QRMSTS = '2' AND QRMWHSE IS NOT NULL AND QRMWHSE != 'TWC' AND QRMPLANT IN ('1000','1050') AND QRMWHSE = MSWHSE)\n"
                + "	  ELSE (SELECT SUM(QRMQTY) FROM QRMMAS LEFT JOIN MSSMATN ON MSSMATN.LGPBE = (SELECT TOP 1 SAPMCTRL FROM SAPMAS WHERE SAPMAT = QRMCODE AND SAPPLANT = QRMPLANT AND SAPVAL = QRMVAL)\n"
                + "	       WHERE QRMSTS = '2' AND QRMWHSE = QWHCOD AND QRMPLANT IN ('1000','1050') AND QRMWHSE = MSWHSE)\n"
                + "	  END,0),'#,#0.00') AS QTY_2\n"
                + ",FORMAT(ISNULL(CASE WHEN QWHCOD = 'TWC' \n"
                + "      THEN (SELECT SUM(QRMQTY) FROM QRMMAS LEFT JOIN MSSMATN ON MSSMATN.LGPBE = (SELECT TOP 1 SAPMCTRL FROM SAPMAS WHERE SAPMAT = QRMCODE AND SAPPLANT = QRMPLANT AND SAPVAL = QRMVAL)\n"
                + "	       WHERE QRMSTS = '3' AND QRMWHSE IS NOT NULL AND QRMWHSE != 'TWC' AND QRMPLANT IN ('1000','1050') AND QRMWHSE = MSWHSE)\n"
                + "	  ELSE (SELECT SUM(QRMQTY) FROM QRMMAS LEFT JOIN MSSMATN ON MSSMATN.LGPBE = (SELECT TOP 1 SAPMCTRL FROM SAPMAS WHERE SAPMAT = QRMCODE AND SAPPLANT = QRMPLANT AND SAPVAL = QRMVAL)\n"
                + "	       WHERE QRMSTS = '3' AND QRMWHSE = QWHCOD AND QRMPLANT IN ('1000','1050') AND QRMWHSE = MSWHSE)\n"
                + "	  END,0),'#,#0.00') AS QTY_3\n"
                + ",FORMAT(ISNULL(CASE WHEN QWHCOD = 'TWC' \n"
                + "      THEN (SELECT SUM(QRMQTY) FROM QRMMAS LEFT JOIN MSSMATN ON MSSMATN.LGPBE = (SELECT TOP 1 SAPMCTRL FROM SAPMAS WHERE SAPMAT = QRMCODE AND SAPPLANT = QRMPLANT AND SAPVAL = QRMVAL)\n"
                + "	       WHERE QRMSTS = '4' AND QRMWHSE IS NOT NULL AND QRMWHSE != 'TWC' AND QRMPLANT IN ('1000','1050') AND QRMWHSE = MSWHSE)\n"
                + "	  ELSE (SELECT SUM(QRMQTY) FROM QRMMAS LEFT JOIN MSSMATN ON MSSMATN.LGPBE = (SELECT TOP 1 SAPMCTRL FROM SAPMAS WHERE SAPMAT = QRMCODE AND SAPPLANT = QRMPLANT AND SAPVAL = QRMVAL)\n"
                + "	       WHERE QRMSTS = '4' AND QRMWHSE = QWHCOD AND QRMPLANT IN ('1000','1050') AND QRMWHSE = MSWHSE)\n"
                + "	  END,0),'#,#0.00') AS QTY_4\n"
                + ",FORMAT(ISNULL(CASE WHEN QWHCOD = 'TWC' \n"
                + "      THEN (SELECT SUM(QRMQTY) FROM QRMMAS LEFT JOIN MSSMATN ON MSSMATN.LGPBE = (SELECT TOP 1 SAPMCTRL FROM SAPMAS WHERE SAPMAT = QRMCODE AND SAPPLANT = QRMPLANT AND SAPVAL = QRMVAL)\n"
                + "	       WHERE QRMSTS = '5' AND QRMWHSE IS NOT NULL AND QRMWHSE != 'TWC' AND QRMPLANT IN ('1000','1050') AND QRMWHSE = MSWHSE)\n"
                + "	  ELSE (SELECT SUM(QRMQTY) FROM QRMMAS LEFT JOIN MSSMATN ON MSSMATN.LGPBE = (SELECT TOP 1 SAPMCTRL FROM SAPMAS WHERE SAPMAT = QRMCODE AND SAPPLANT = QRMPLANT AND SAPVAL = QRMVAL)\n"
                + "	       WHERE QRMSTS = '5' AND QRMWHSE = QWHCOD AND QRMPLANT IN ('1000','1050') AND QRMWHSE = MSWHSE)\n"
                + "	  END,0),'#,#0.00') AS QTY_5\n"
                + ",FORMAT(ISNULL(CASE WHEN QWHCOD = 'TWC' \n"
                + "      THEN (SELECT SUM(QRMQTY) FROM QRMMAS LEFT JOIN MSSMATN ON MSSMATN.LGPBE = (SELECT TOP 1 SAPMCTRL FROM SAPMAS WHERE SAPMAT = QRMCODE AND SAPPLANT = QRMPLANT AND SAPVAL = QRMVAL)\n"
                + "	       WHERE QRMSTS = '6' AND QRMWHSE IS NOT NULL AND QRMWHSE != 'TWC' AND QRMPLANT IN ('1000','1050') AND QRMWHSE = MSWHSE)\n"
                + "	  ELSE (SELECT SUM(QRMQTY) FROM QRMMAS LEFT JOIN MSSMATN ON MSSMATN.LGPBE = (SELECT TOP 1 SAPMCTRL FROM SAPMAS WHERE SAPMAT = QRMCODE AND SAPPLANT = QRMPLANT AND SAPVAL = QRMVAL)\n"
                + "	       WHERE QRMSTS = '6' AND QRMWHSE = QWHCOD AND QRMPLANT IN ('1000','1050') AND QRMWHSE = MSWHSE)\n"
                + "	  END,0),'#,#0.00') AS QTY_6\n"
                + ",FORMAT(ISNULL(CASE WHEN QWHCOD = 'TWC' \n"
                + "      THEN (SELECT SUM(QRMQTY) FROM QRMMAS LEFT JOIN MSSMATN ON MSSMATN.LGPBE = (SELECT TOP 1 SAPMCTRL FROM SAPMAS WHERE SAPMAT = QRMCODE AND SAPPLANT = QRMPLANT AND SAPVAL = QRMVAL)\n"
                + "	       WHERE QRMSTS = '7' AND QRMWHSE IS NOT NULL AND QRMWHSE != 'TWC' AND QRMPLANT IN ('1000','1050') AND QRMWHSE = MSWHSE)\n"
                + "	  ELSE (SELECT SUM(QRMQTY) FROM QRMMAS LEFT JOIN MSSMATN ON MSSMATN.LGPBE = (SELECT TOP 1 SAPMCTRL FROM SAPMAS WHERE SAPMAT = QRMCODE AND SAPPLANT = QRMPLANT AND SAPVAL = QRMVAL)\n"
                + "	       WHERE QRMSTS = '7' AND QRMWHSE = QWHCOD AND QRMPLANT IN ('1000','1050') AND QRMWHSE = MSWHSE)\n"
                + "	  END,0),'#,#0.00') AS QTY_7\n"
                + ",FORMAT(ISNULL(CASE WHEN QWHCOD = 'TWC' \n"
                + "      THEN (SELECT SUM(QRMQTY) FROM QRMMAS LEFT JOIN MSSMATN ON MSSMATN.LGPBE = (SELECT TOP 1 SAPMCTRL FROM SAPMAS WHERE SAPMAT = QRMCODE AND SAPPLANT = QRMPLANT AND SAPVAL = QRMVAL)\n"
                + "	       WHERE QRMSTS = 'O' AND QRMWHSE IS NOT NULL AND QRMWHSE != 'TWC' AND QRMPLANT IN ('1000','1050') AND QRMWHSE = MSWHSE)\n"
                + "	  ELSE (SELECT SUM(QRMQTY) FROM QRMMAS LEFT JOIN MSSMATN ON MSSMATN.LGPBE = (SELECT TOP 1 SAPMCTRL FROM SAPMAS WHERE SAPMAT = QRMCODE AND SAPPLANT = QRMPLANT AND SAPVAL = QRMVAL)\n"
                + "	       WHERE QRMSTS = 'O' AND QRMWHSE = QWHCOD AND QRMPLANT IN ('1000','1050') AND QRMWHSE = MSWHSE)\n"
                + "	  END,0),'#,#0.00') AS QTY_O\n"
                + ",FORMAT(ISNULL(CASE WHEN QWHCOD = 'TWC' \n"
                + "      THEN (SELECT SUM(QRMQTY) FROM QRMMAS LEFT JOIN MSSMATN ON MSSMATN.LGPBE = (SELECT TOP 1 SAPMCTRL FROM SAPMAS WHERE SAPMAT = QRMCODE AND SAPPLANT = QRMPLANT AND SAPVAL = QRMVAL)\n"
                + "	       WHERE QRMSTS = 'P' AND QRMWHSE IS NOT NULL AND QRMWHSE != 'TWC' AND QRMPLANT IN ('1000','1050') AND QRMWHSE = MSWHSE)\n"
                + "	  ELSE (SELECT SUM(QRMQTY) FROM QRMMAS LEFT JOIN MSSMATN ON MSSMATN.LGPBE = (SELECT TOP 1 SAPMCTRL FROM SAPMAS WHERE SAPMAT = QRMCODE AND SAPPLANT = QRMPLANT AND SAPVAL = QRMVAL)\n"
                + "	       WHERE QRMSTS = 'P' AND QRMWHSE = QWHCOD AND QRMPLANT IN ('1000','1050') AND QRMWHSE = MSWHSE)\n"
                + "	  END,0),'#,#0.00') AS QTY_P\n"
                + "FROM QWHMAS\n"
                + "UNION ALL\n"
                + "SELECT QWHCOD, QWHNAME, '1000' AS PLANT,'' AS MATCT,'' AS RM,'' AS MATCO,'' AS MATDESC\n"
                + ",FORMAT(ISNULL(CASE WHEN QWHCOD = 'TWC' \n"
                + "      THEN (SELECT SUM(QRMQTY) FROM QRMMAS LEFT JOIN MSSMATN ON MSSMATN.LGPBE = (SELECT TOP 1 SAPMCTRL FROM SAPMAS WHERE SAPMAT = QRMCODE AND SAPPLANT = QRMPLANT AND SAPVAL = QRMVAL)\n"
                + "	       WHERE QRMSTS IN ('1','2','3','4') AND QRMWHSE IS NOT NULL AND QRMWHSE != 'TWC' AND QRMPLANT = '1000' AND QRMWHSE = MSWHSE)\n"
                + "	  ELSE (SELECT SUM(QRMQTY) FROM QRMMAS LEFT JOIN MSSMATN ON MSSMATN.LGPBE = (SELECT TOP 1 SAPMCTRL FROM SAPMAS WHERE SAPMAT = QRMCODE AND SAPPLANT = QRMPLANT AND SAPVAL = QRMVAL)\n"
                + "	       WHERE QRMSTS IN ('1','2','3','4') AND QRMWHSE = QWHCOD AND QRMPLANT = '1000' AND QRMWHSE = MSWHSE)\n"
                + "	  END,0),'#,#0.00') AS QTY_1234\n"
                + ",FORMAT(ISNULL(CASE WHEN QWHCOD = 'TWC' \n"
                + "      THEN (SELECT SUM(QRMQTY) FROM QRMMAS LEFT JOIN MSSMATN ON MSSMATN.LGPBE = (SELECT TOP 1 SAPMCTRL FROM SAPMAS WHERE SAPMAT = QRMCODE AND SAPPLANT = QRMPLANT AND SAPVAL = QRMVAL)\n"
                + "	       WHERE QRMSTS IN ('4','5') AND QRMWHSE IS NOT NULL AND QRMWHSE != 'TWC' AND QRMPLANT = '1000' AND QRMWHSE = MSWHSE)\n"
                + "	  ELSE (SELECT SUM(QRMQTY) FROM QRMMAS LEFT JOIN MSSMATN ON MSSMATN.LGPBE = (SELECT TOP 1 SAPMCTRL FROM SAPMAS WHERE SAPMAT = QRMCODE AND SAPPLANT = QRMPLANT AND SAPVAL = QRMVAL)\n"
                + "	       WHERE QRMSTS IN ('4','5') AND QRMWHSE = QWHCOD AND QRMPLANT = '1000' AND QRMWHSE = MSWHSE)\n"
                + "	  END,0),'#,#0.00') AS QTY_45\n"
                + ",FORMAT(ISNULL(CASE WHEN QWHCOD = 'TWC' \n"
                + "      THEN (SELECT SUM(QRMQTY) FROM QRMMAS LEFT JOIN MSSMATN ON MSSMATN.LGPBE = (SELECT TOP 1 SAPMCTRL FROM SAPMAS WHERE SAPMAT = QRMCODE AND SAPPLANT = QRMPLANT AND SAPVAL = QRMVAL)\n"
                + "	       WHERE QRMSTS = '1' AND QRMWHSE IS NOT NULL AND QRMWHSE != 'TWC' AND QRMPLANT = '1000' AND QRMWHSE = MSWHSE)\n"
                + "	  ELSE (SELECT SUM(QRMQTY) FROM QRMMAS LEFT JOIN MSSMATN ON MSSMATN.LGPBE = (SELECT TOP 1 SAPMCTRL FROM SAPMAS WHERE SAPMAT = QRMCODE AND SAPPLANT = QRMPLANT AND SAPVAL = QRMVAL)\n"
                + "	       WHERE QRMSTS = '1' AND QRMWHSE = QWHCOD AND QRMPLANT = '1000' AND QRMWHSE = MSWHSE)\n"
                + "	  END,0),'#,#0.00') AS QTY_1\n"
                + ",FORMAT(ISNULL(CASE WHEN QWHCOD = 'TWC' \n"
                + "      THEN (SELECT SUM(QRMQTY) FROM QRMMAS LEFT JOIN MSSMATN ON MSSMATN.LGPBE = (SELECT TOP 1 SAPMCTRL FROM SAPMAS WHERE SAPMAT = QRMCODE AND SAPPLANT = QRMPLANT AND SAPVAL = QRMVAL)\n"
                + "	       WHERE QRMSTS = '2' AND QRMWHSE IS NOT NULL AND QRMWHSE != 'TWC' AND QRMPLANT = '1000' AND QRMWHSE = MSWHSE)\n"
                + "	  ELSE (SELECT SUM(QRMQTY) FROM QRMMAS LEFT JOIN MSSMATN ON MSSMATN.LGPBE = (SELECT TOP 1 SAPMCTRL FROM SAPMAS WHERE SAPMAT = QRMCODE AND SAPPLANT = QRMPLANT AND SAPVAL = QRMVAL)\n"
                + "	       WHERE QRMSTS = '2' AND QRMWHSE = QWHCOD AND QRMPLANT = '1000' AND QRMWHSE = MSWHSE)\n"
                + "	  END,0),'#,#0.00') AS QTY_2\n"
                + ",FORMAT(ISNULL(CASE WHEN QWHCOD = 'TWC' \n"
                + "      THEN (SELECT SUM(QRMQTY) FROM QRMMAS LEFT JOIN MSSMATN ON MSSMATN.LGPBE = (SELECT TOP 1 SAPMCTRL FROM SAPMAS WHERE SAPMAT = QRMCODE AND SAPPLANT = QRMPLANT AND SAPVAL = QRMVAL)\n"
                + "	       WHERE QRMSTS = '3' AND QRMWHSE IS NOT NULL AND QRMWHSE != 'TWC' AND QRMPLANT = '1000' AND QRMWHSE = MSWHSE)\n"
                + "	  ELSE (SELECT SUM(QRMQTY) FROM QRMMAS LEFT JOIN MSSMATN ON MSSMATN.LGPBE = (SELECT TOP 1 SAPMCTRL FROM SAPMAS WHERE SAPMAT = QRMCODE AND SAPPLANT = QRMPLANT AND SAPVAL = QRMVAL)\n"
                + "	       WHERE QRMSTS = '3' AND QRMWHSE = QWHCOD AND QRMPLANT = '1000' AND QRMWHSE = MSWHSE)\n"
                + "	  END,0),'#,#0.00') AS QTY_3\n"
                + ",FORMAT(ISNULL(CASE WHEN QWHCOD = 'TWC' \n"
                + "      THEN (SELECT SUM(QRMQTY) FROM QRMMAS LEFT JOIN MSSMATN ON MSSMATN.LGPBE = (SELECT TOP 1 SAPMCTRL FROM SAPMAS WHERE SAPMAT = QRMCODE AND SAPPLANT = QRMPLANT AND SAPVAL = QRMVAL)\n"
                + "	       WHERE QRMSTS = '4' AND QRMWHSE IS NOT NULL AND QRMWHSE != 'TWC' AND QRMPLANT = '1000' AND QRMWHSE = MSWHSE)\n"
                + "	  ELSE (SELECT SUM(QRMQTY) FROM QRMMAS LEFT JOIN MSSMATN ON MSSMATN.LGPBE = (SELECT TOP 1 SAPMCTRL FROM SAPMAS WHERE SAPMAT = QRMCODE AND SAPPLANT = QRMPLANT AND SAPVAL = QRMVAL)\n"
                + "	       WHERE QRMSTS = '4' AND QRMWHSE = QWHCOD AND QRMPLANT = '1000' AND QRMWHSE = MSWHSE)\n"
                + "	  END,0),'#,#0.00') AS QTY_4\n"
                + ",FORMAT(ISNULL(CASE WHEN QWHCOD = 'TWC' \n"
                + "      THEN (SELECT SUM(QRMQTY) FROM QRMMAS LEFT JOIN MSSMATN ON MSSMATN.LGPBE = (SELECT TOP 1 SAPMCTRL FROM SAPMAS WHERE SAPMAT = QRMCODE AND SAPPLANT = QRMPLANT AND SAPVAL = QRMVAL)\n"
                + "	       WHERE QRMSTS = '5' AND QRMWHSE IS NOT NULL AND QRMWHSE != 'TWC' AND QRMPLANT = '1000' AND QRMWHSE = MSWHSE)\n"
                + "	  ELSE (SELECT SUM(QRMQTY) FROM QRMMAS LEFT JOIN MSSMATN ON MSSMATN.LGPBE = (SELECT TOP 1 SAPMCTRL FROM SAPMAS WHERE SAPMAT = QRMCODE AND SAPPLANT = QRMPLANT AND SAPVAL = QRMVAL)\n"
                + "	       WHERE QRMSTS = '5' AND QRMWHSE = QWHCOD AND QRMPLANT = '1000' AND QRMWHSE = MSWHSE)\n"
                + "	  END,0),'#,#0.00') AS QTY_5\n"
                + ",FORMAT(ISNULL(CASE WHEN QWHCOD = 'TWC' \n"
                + "      THEN (SELECT SUM(QRMQTY) FROM QRMMAS LEFT JOIN MSSMATN ON MSSMATN.LGPBE = (SELECT TOP 1 SAPMCTRL FROM SAPMAS WHERE SAPMAT = QRMCODE AND SAPPLANT = QRMPLANT AND SAPVAL = QRMVAL)\n"
                + "	       WHERE QRMSTS = '6' AND QRMWHSE IS NOT NULL AND QRMWHSE != 'TWC' AND QRMPLANT = '1000' AND QRMWHSE = MSWHSE)\n"
                + "	  ELSE (SELECT SUM(QRMQTY) FROM QRMMAS LEFT JOIN MSSMATN ON MSSMATN.LGPBE = (SELECT TOP 1 SAPMCTRL FROM SAPMAS WHERE SAPMAT = QRMCODE AND SAPPLANT = QRMPLANT AND SAPVAL = QRMVAL)\n"
                + "	       WHERE QRMSTS = '6' AND QRMWHSE = QWHCOD AND QRMPLANT = '1000' AND QRMWHSE = MSWHSE)\n"
                + "	  END,0),'#,#0.00') AS QTY_6\n"
                + ",FORMAT(ISNULL(CASE WHEN QWHCOD = 'TWC' \n"
                + "      THEN (SELECT SUM(QRMQTY) FROM QRMMAS LEFT JOIN MSSMATN ON MSSMATN.LGPBE = (SELECT TOP 1 SAPMCTRL FROM SAPMAS WHERE SAPMAT = QRMCODE AND SAPPLANT = QRMPLANT AND SAPVAL = QRMVAL)\n"
                + "	       WHERE QRMSTS = '7' AND QRMWHSE IS NOT NULL AND QRMWHSE != 'TWC' AND QRMPLANT = '1000' AND QRMWHSE = MSWHSE)\n"
                + "	  ELSE (SELECT SUM(QRMQTY) FROM QRMMAS LEFT JOIN MSSMATN ON MSSMATN.LGPBE = (SELECT TOP 1 SAPMCTRL FROM SAPMAS WHERE SAPMAT = QRMCODE AND SAPPLANT = QRMPLANT AND SAPVAL = QRMVAL)\n"
                + "	       WHERE QRMSTS = '7' AND QRMWHSE = QWHCOD AND QRMPLANT = '1000' AND QRMWHSE = MSWHSE)\n"
                + "	  END,0),'#,#0.00') AS QTY_7\n"
                + ",FORMAT(ISNULL(CASE WHEN QWHCOD = 'TWC' \n"
                + "      THEN (SELECT SUM(QRMQTY) FROM QRMMAS LEFT JOIN MSSMATN ON MSSMATN.LGPBE = (SELECT TOP 1 SAPMCTRL FROM SAPMAS WHERE SAPMAT = QRMCODE AND SAPPLANT = QRMPLANT AND SAPVAL = QRMVAL)\n"
                + "	       WHERE QRMSTS = 'O' AND QRMWHSE IS NOT NULL AND QRMWHSE != 'TWC' AND QRMPLANT = '1000' AND QRMWHSE = MSWHSE)\n"
                + "	  ELSE (SELECT SUM(QRMQTY) FROM QRMMAS LEFT JOIN MSSMATN ON MSSMATN.LGPBE = (SELECT TOP 1 SAPMCTRL FROM SAPMAS WHERE SAPMAT = QRMCODE AND SAPPLANT = QRMPLANT AND SAPVAL = QRMVAL)\n"
                + "	       WHERE QRMSTS = 'O' AND QRMWHSE = QWHCOD AND QRMPLANT = '1000' AND QRMWHSE = MSWHSE)\n"
                + "	  END,0),'#,#0.00') AS QTY_O\n"
                + ",FORMAT(ISNULL(CASE WHEN QWHCOD = 'TWC' \n"
                + "      THEN (SELECT SUM(QRMQTY) FROM QRMMAS LEFT JOIN MSSMATN ON MSSMATN.LGPBE = (SELECT TOP 1 SAPMCTRL FROM SAPMAS WHERE SAPMAT = QRMCODE AND SAPPLANT = QRMPLANT AND SAPVAL = QRMVAL)\n"
                + "	       WHERE QRMSTS = 'P' AND QRMWHSE IS NOT NULL AND QRMWHSE != 'TWC' AND QRMPLANT = '1000' AND QRMWHSE = MSWHSE)\n"
                + "	  ELSE (SELECT SUM(QRMQTY) FROM QRMMAS LEFT JOIN MSSMATN ON MSSMATN.LGPBE = (SELECT TOP 1 SAPMCTRL FROM SAPMAS WHERE SAPMAT = QRMCODE AND SAPPLANT = QRMPLANT AND SAPVAL = QRMVAL)\n"
                + "	       WHERE QRMSTS = 'P' AND QRMWHSE = QWHCOD AND QRMPLANT = '1000' AND QRMWHSE = MSWHSE)\n"
                + "	  END,0),'#,#0.00') AS QTY_P\n"
                + "FROM QWHMAS\n"
                + "UNION ALL\n"
                + "SELECT QWHCOD, QWHNAME, '1050' AS PLANT,'' AS MATCT,'' AS RM,'' AS MATCO,'' AS MATDESC\n"
                + ",FORMAT(ISNULL(CASE WHEN QWHCOD = 'TWC' \n"
                + "      THEN (SELECT SUM(QRMQTY) FROM QRMMAS LEFT JOIN MSSMATN ON MSSMATN.LGPBE = (SELECT TOP 1 SAPMCTRL FROM SAPMAS WHERE SAPMAT = QRMCODE AND SAPPLANT = QRMPLANT AND SAPVAL = QRMVAL)\n"
                + "	       WHERE QRMSTS IN ('1','2','3','4') AND QRMWHSE IS NOT NULL AND QRMWHSE != 'TWC' AND QRMPLANT = '1050' AND QRMWHSE = MSWHSE)\n"
                + "	  ELSE (SELECT SUM(QRMQTY) FROM QRMMAS LEFT JOIN MSSMATN ON MSSMATN.LGPBE = (SELECT TOP 1 SAPMCTRL FROM SAPMAS WHERE SAPMAT = QRMCODE AND SAPPLANT = QRMPLANT AND SAPVAL = QRMVAL)\n"
                + "	       WHERE QRMSTS IN ('1','2','3','4') AND QRMWHSE = QWHCOD AND QRMPLANT = '1050' AND QRMWHSE = MSWHSE)\n"
                + "	  END,0),'#,#0.00') AS QTY_1234\n"
                + ",FORMAT(ISNULL(CASE WHEN QWHCOD = 'TWC' \n"
                + "      THEN (SELECT SUM(QRMQTY) FROM QRMMAS LEFT JOIN MSSMATN ON MSSMATN.LGPBE = (SELECT TOP 1 SAPMCTRL FROM SAPMAS WHERE SAPMAT = QRMCODE AND SAPPLANT = QRMPLANT AND SAPVAL = QRMVAL)\n"
                + "	       WHERE QRMSTS IN ('4','5') AND QRMWHSE IS NOT NULL AND QRMWHSE != 'TWC' AND QRMPLANT = '1050' AND QRMWHSE = MSWHSE)\n"
                + "	  ELSE (SELECT SUM(QRMQTY) FROM QRMMAS LEFT JOIN MSSMATN ON MSSMATN.LGPBE = (SELECT TOP 1 SAPMCTRL FROM SAPMAS WHERE SAPMAT = QRMCODE AND SAPPLANT = QRMPLANT AND SAPVAL = QRMVAL)\n"
                + "	       WHERE QRMSTS IN ('4','5') AND QRMWHSE = QWHCOD AND QRMPLANT = '1050' AND QRMWHSE = MSWHSE)\n"
                + "	  END,0),'#,#0.00') AS QTY_45\n"
                + ",FORMAT(ISNULL(CASE WHEN QWHCOD = 'TWC' \n"
                + "      THEN (SELECT SUM(QRMQTY) FROM QRMMAS LEFT JOIN MSSMATN ON MSSMATN.LGPBE = (SELECT TOP 1 SAPMCTRL FROM SAPMAS WHERE SAPMAT = QRMCODE AND SAPPLANT = QRMPLANT AND SAPVAL = QRMVAL)\n"
                + "	       WHERE QRMSTS = '1' AND QRMWHSE IS NOT NULL AND QRMWHSE != 'TWC' AND QRMPLANT = '1050' AND QRMWHSE = MSWHSE)\n"
                + "	  ELSE (SELECT SUM(QRMQTY) FROM QRMMAS LEFT JOIN MSSMATN ON MSSMATN.LGPBE = (SELECT TOP 1 SAPMCTRL FROM SAPMAS WHERE SAPMAT = QRMCODE AND SAPPLANT = QRMPLANT AND SAPVAL = QRMVAL)\n"
                + "	       WHERE QRMSTS = '1' AND QRMWHSE = QWHCOD AND QRMPLANT = '1050' AND QRMWHSE = MSWHSE)\n"
                + "	  END,0),'#,#0.00') AS QTY_1\n"
                + ",FORMAT(ISNULL(CASE WHEN QWHCOD = 'TWC' \n"
                + "      THEN (SELECT SUM(QRMQTY) FROM QRMMAS LEFT JOIN MSSMATN ON MSSMATN.LGPBE = (SELECT TOP 1 SAPMCTRL FROM SAPMAS WHERE SAPMAT = QRMCODE AND SAPPLANT = QRMPLANT AND SAPVAL = QRMVAL)\n"
                + "	       WHERE QRMSTS = '2' AND QRMWHSE IS NOT NULL AND QRMWHSE != 'TWC' AND QRMPLANT = '1050' AND QRMWHSE = MSWHSE)\n"
                + "	  ELSE (SELECT SUM(QRMQTY) FROM QRMMAS LEFT JOIN MSSMATN ON MSSMATN.LGPBE = (SELECT TOP 1 SAPMCTRL FROM SAPMAS WHERE SAPMAT = QRMCODE AND SAPPLANT = QRMPLANT AND SAPVAL = QRMVAL)\n"
                + "	       WHERE QRMSTS = '2' AND QRMWHSE = QWHCOD AND QRMPLANT = '1050' AND QRMWHSE = MSWHSE)\n"
                + "	  END,0),'#,#0.00') AS QTY_2\n"
                + ",FORMAT(ISNULL(CASE WHEN QWHCOD = 'TWC' \n"
                + "      THEN (SELECT SUM(QRMQTY) FROM QRMMAS LEFT JOIN MSSMATN ON MSSMATN.LGPBE = (SELECT TOP 1 SAPMCTRL FROM SAPMAS WHERE SAPMAT = QRMCODE AND SAPPLANT = QRMPLANT AND SAPVAL = QRMVAL)\n"
                + "	       WHERE QRMSTS = '3' AND QRMWHSE IS NOT NULL AND QRMWHSE != 'TWC' AND QRMPLANT = '1050' AND QRMWHSE = MSWHSE)\n"
                + "	  ELSE (SELECT SUM(QRMQTY) FROM QRMMAS LEFT JOIN MSSMATN ON MSSMATN.LGPBE = (SELECT TOP 1 SAPMCTRL FROM SAPMAS WHERE SAPMAT = QRMCODE AND SAPPLANT = QRMPLANT AND SAPVAL = QRMVAL)\n"
                + "	       WHERE QRMSTS = '3' AND QRMWHSE = QWHCOD AND QRMPLANT = '1050' AND QRMWHSE = MSWHSE)\n"
                + "	  END,0),'#,#0.00') AS QTY_3\n"
                + ",FORMAT(ISNULL(CASE WHEN QWHCOD = 'TWC' \n"
                + "      THEN (SELECT SUM(QRMQTY) FROM QRMMAS LEFT JOIN MSSMATN ON MSSMATN.LGPBE = (SELECT TOP 1 SAPMCTRL FROM SAPMAS WHERE SAPMAT = QRMCODE AND SAPPLANT = QRMPLANT AND SAPVAL = QRMVAL)\n"
                + "	       WHERE QRMSTS = '4' AND QRMWHSE IS NOT NULL AND QRMWHSE != 'TWC' AND QRMPLANT = '1050' AND QRMWHSE = MSWHSE)\n"
                + "	  ELSE (SELECT SUM(QRMQTY) FROM QRMMAS LEFT JOIN MSSMATN ON MSSMATN.LGPBE = (SELECT TOP 1 SAPMCTRL FROM SAPMAS WHERE SAPMAT = QRMCODE AND SAPPLANT = QRMPLANT AND SAPVAL = QRMVAL)\n"
                + "	       WHERE QRMSTS = '4' AND QRMWHSE = QWHCOD AND QRMPLANT = '1050' AND QRMWHSE = MSWHSE)\n"
                + "	  END,0),'#,#0.00') AS QTY_4\n"
                + ",FORMAT(ISNULL(CASE WHEN QWHCOD = 'TWC' \n"
                + "      THEN (SELECT SUM(QRMQTY) FROM QRMMAS LEFT JOIN MSSMATN ON MSSMATN.LGPBE = (SELECT TOP 1 SAPMCTRL FROM SAPMAS WHERE SAPMAT = QRMCODE AND SAPPLANT = QRMPLANT AND SAPVAL = QRMVAL)\n"
                + "	       WHERE QRMSTS = '5' AND QRMWHSE IS NOT NULL AND QRMWHSE != 'TWC' AND QRMPLANT = '1050' AND QRMWHSE = MSWHSE)\n"
                + "	  ELSE (SELECT SUM(QRMQTY) FROM QRMMAS LEFT JOIN MSSMATN ON MSSMATN.LGPBE = (SELECT TOP 1 SAPMCTRL FROM SAPMAS WHERE SAPMAT = QRMCODE AND SAPPLANT = QRMPLANT AND SAPVAL = QRMVAL)\n"
                + "	       WHERE QRMSTS = '5' AND QRMWHSE = QWHCOD AND QRMPLANT = '1050' AND QRMWHSE = MSWHSE)\n"
                + "	  END,0),'#,#0.00') AS QTY_5\n"
                + ",FORMAT(ISNULL(CASE WHEN QWHCOD = 'TWC' \n"
                + "      THEN (SELECT SUM(QRMQTY) FROM QRMMAS LEFT JOIN MSSMATN ON MSSMATN.LGPBE = (SELECT TOP 1 SAPMCTRL FROM SAPMAS WHERE SAPMAT = QRMCODE AND SAPPLANT = QRMPLANT AND SAPVAL = QRMVAL)\n"
                + "	       WHERE QRMSTS = '6' AND QRMWHSE IS NOT NULL AND QRMWHSE != 'TWC' AND QRMPLANT = '1050' AND QRMWHSE = MSWHSE)\n"
                + "	  ELSE (SELECT SUM(QRMQTY) FROM QRMMAS LEFT JOIN MSSMATN ON MSSMATN.LGPBE = (SELECT TOP 1 SAPMCTRL FROM SAPMAS WHERE SAPMAT = QRMCODE AND SAPPLANT = QRMPLANT AND SAPVAL = QRMVAL)\n"
                + "	       WHERE QRMSTS = '6' AND QRMWHSE = QWHCOD AND QRMPLANT = '1050' AND QRMWHSE = MSWHSE)\n"
                + "	  END,0),'#,#0.00') AS QTY_6\n"
                + ",FORMAT(ISNULL(CASE WHEN QWHCOD = 'TWC' \n"
                + "      THEN (SELECT SUM(QRMQTY) FROM QRMMAS LEFT JOIN MSSMATN ON MSSMATN.LGPBE = (SELECT TOP 1 SAPMCTRL FROM SAPMAS WHERE SAPMAT = QRMCODE AND SAPPLANT = QRMPLANT AND SAPVAL = QRMVAL)\n"
                + "	       WHERE QRMSTS = '7' AND QRMWHSE IS NOT NULL AND QRMWHSE != 'TWC' AND QRMPLANT = '1050' AND QRMWHSE = MSWHSE)\n"
                + "	  ELSE (SELECT SUM(QRMQTY) FROM QRMMAS LEFT JOIN MSSMATN ON MSSMATN.LGPBE = (SELECT TOP 1 SAPMCTRL FROM SAPMAS WHERE SAPMAT = QRMCODE AND SAPPLANT = QRMPLANT AND SAPVAL = QRMVAL)\n"
                + "	       WHERE QRMSTS = '7' AND QRMWHSE = QWHCOD AND QRMPLANT = '1050' AND QRMWHSE = MSWHSE)\n"
                + "	  END,0),'#,#0.00') AS QTY_7\n"
                + ",FORMAT(ISNULL(CASE WHEN QWHCOD = 'TWC' \n"
                + "      THEN (SELECT SUM(QRMQTY) FROM QRMMAS LEFT JOIN MSSMATN ON MSSMATN.LGPBE = (SELECT TOP 1 SAPMCTRL FROM SAPMAS WHERE SAPMAT = QRMCODE AND SAPPLANT = QRMPLANT AND SAPVAL = QRMVAL)\n"
                + "	       WHERE QRMSTS = 'O' AND QRMWHSE IS NOT NULL AND QRMWHSE != 'TWC' AND QRMPLANT = '1050' AND QRMWHSE = MSWHSE)\n"
                + "	  ELSE (SELECT SUM(QRMQTY) FROM QRMMAS LEFT JOIN MSSMATN ON MSSMATN.LGPBE = (SELECT TOP 1 SAPMCTRL FROM SAPMAS WHERE SAPMAT = QRMCODE AND SAPPLANT = QRMPLANT AND SAPVAL = QRMVAL)\n"
                + "	       WHERE QRMSTS = 'O' AND QRMWHSE = QWHCOD AND QRMPLANT = '1050' AND QRMWHSE = MSWHSE)\n"
                + "	  END,0),'#,#0.00') AS QTY_O\n"
                + ",FORMAT(ISNULL(CASE WHEN QWHCOD = 'TWC' \n"
                + "      THEN (SELECT SUM(QRMQTY) FROM QRMMAS LEFT JOIN MSSMATN ON MSSMATN.LGPBE = (SELECT TOP 1 SAPMCTRL FROM SAPMAS WHERE SAPMAT = QRMCODE AND SAPPLANT = QRMPLANT AND SAPVAL = QRMVAL)\n"
                + "	       WHERE QRMSTS = 'P' AND QRMWHSE IS NOT NULL AND QRMWHSE != 'TWC' AND QRMPLANT = '1050' AND QRMWHSE = MSWHSE)\n"
                + "	  ELSE (SELECT SUM(QRMQTY) FROM QRMMAS LEFT JOIN MSSMATN ON MSSMATN.LGPBE = (SELECT TOP 1 SAPMCTRL FROM SAPMAS WHERE SAPMAT = QRMCODE AND SAPPLANT = QRMPLANT AND SAPVAL = QRMVAL)\n"
                + "	       WHERE QRMSTS = 'P' AND QRMWHSE = QWHCOD AND QRMPLANT = '1050' AND QRMWHSE = MSWHSE)\n"
                + "	  END,0),'#,#0.00') AS QTY_P\n"
                + "FROM QWHMAS\n"
                + "--END PART 1\n"
                + "ORDER BY QWHCOD, QWHNAME, PLANT, MATCT, RM, MATCO, MATDESC";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            String wh = "";
            String plant = "";
            String matc = "";
            String rm = "";

            while (result.next()) {

                WMS950 p = new WMS950();

                p.setDT_RowId("HeadFirst");

                if (wh.equals(result.getString("QWHCOD"))) {
                    p.setWh("<b style=\"opacity: 0;\">" + result.getString("QWHCOD") + " : " + result.getString("QWHNAME") + "</b>");
                    wh = result.getString("QWHCOD");
                } else {
                    p.setWh(result.getString("QWHCOD") + " : " + result.getString("QWHNAME"));
                    wh = result.getString("QWHCOD");
                }

                if (plant.equals(result.getString("PLANT"))) {
                    p.setPlant("<b style=\"opacity: 0;\">" + result.getString("PLANT") + "</b>");
                    plant = result.getString("PLANT");
                } else {
                    p.setPlant(result.getString("PLANT"));
                    plant = result.getString("PLANT");
                }

                if (matc.equals(result.getString("MATCT"))) {
                    p.setMatCt("<b style=\"opacity: 0;\">" + result.getString("MATCT") + "</b>");
                    matc = result.getString("MATCT");
                } else {
                    p.setMatCt(result.getString("MATCT"));
                    matc = result.getString("MATCT");
                }

                if (rm.equals(result.getString("RM"))) {
                    p.setRm("<b style=\"opacity: 0;\">" + result.getString("RM") + "</b>");
                    rm = result.getString("RM");
                } else {
                    p.setRm(result.getString("RM"));
                    rm = result.getString("RM");
                }

                p.setWhN(result.getString("QWHCOD"));
                p.setPlantN(result.getString("PLANT"));
                p.setMatCtN(result.getString("MATCT"));
                p.setRmN(result.getString("RM"));
                p.setMatCoN(result.getString("MATCO"));
                p.setDescN(result.getString("MATDESC"));

                if (result.getString("PLANT").equals("TOTAL")) {
                    p.setPlant("<b>" + result.getString("PLANT") + "</b>");
                    p.setMatCo("<b>" + result.getString("MATCO") + "</b>");
                    p.setDesc("<b>" + result.getString("MATDESC") + "</b>");
                    p.setSts123("<b>" + result.getString("QTY_1234") + "</b>");
                    p.setSts456("<b>" + result.getString("QTY_45") + "</b>");
                    p.setSts1("<b>" + result.getString("QTY_1") + "</b>");
                    p.setSts2("<b>" + result.getString("QTY_2") + "</b>");
                    p.setSts3("<b>" + result.getString("QTY_3") + "</b>");
                    p.setSts4("<b>" + result.getString("QTY_4") + "</b>");
                    p.setSts5("<b>" + result.getString("QTY_5") + "</b>");
                    p.setSts6("<b>" + result.getString("QTY_6") + "</b>");
                    p.setSts7("<b>" + result.getString("QTY_7") + "</b>");
                    p.setStsO("<b>" + result.getString("QTY_O") + "</b>");
                    p.setStsP("<b>" + result.getString("QTY_P") + "</b>");
                } else {
                    p.setMatCo(result.getString("MATCO"));
                    p.setDesc(result.getString("MATDESC"));
                    p.setSts123(result.getString("QTY_1234"));
                    p.setSts456(result.getString("QTY_45"));
                    p.setSts1(result.getString("QTY_1"));
                    p.setSts2(result.getString("QTY_2"));
                    p.setSts3(result.getString("QTY_3"));
                    p.setSts4(result.getString("QTY_4"));
                    p.setSts5(result.getString("QTY_5"));
                    p.setSts6(result.getString("QTY_6"));
                    p.setSts7(result.getString("QTY_7"));
                    p.setStsO(result.getString("QTY_O"));
                    p.setStsP(result.getString("QTY_P"));
                }

                UAList.add(p);

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public List<WH> findAll() {

        List<WH> UAList = new ArrayList<WH>();

        String sql = "SELECT * FROM QWHMAS";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                WH p = new WH();

                p.setUid(result.getString("QWHCOD"));

                p.setName(result.getString("QWHNAME"));

                UAList.add(p);

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public List<WH> findAllnoTWC() {

        List<WH> UAList = new ArrayList<WH>();

        String sql = "SELECT * FROM QWHMAS WHERE QWHCOD != 'TWC' ";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                WH p = new WH();

                p.setUid(result.getString("QWHCOD"));

                p.setName(result.getString("QWHNAME"));

                UAList.add(p);

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public WH findByUid(String id) {

        WH ua = new WH();

        String sql = "SELECT * FROM QWHMAS WHERE QWHCOD = ?";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ps.setString(1, id);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                ua.setName(result.getString("QWHNAME"));

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return ua;

    }
}
