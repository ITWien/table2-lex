/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.dao;

import com.twc.wms.database.database;

import java.sql.*;

import java.util.ArrayList;

import java.util.List;

import com.twc.wms.entity.XMSOTHEAD;
import com.twc.wms.entity.SAPMAS;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import com.twc.wms.database.MSSQLDbConnectionPool;
import com.twc.wms.entity.Qdest;

/**
 *
 * @author nutthawoot.noo
 */
public class XMSOTHEADDao extends database {

    public boolean addDocno(String qno) {

        boolean result = false;

        String sql = "UPDATE [RMShipment].[dbo].[XMSOTHEAD]\n"
                + "SET [XMSOTHDOCNO] = (\n"
                + "                    SELECT FORMAT(CURRENT_TIMESTAMP,'yy')+'-'+FORMAT(CURRENT_TIMESTAMP,'MM')+'-'+FORMAT([QNBLAST]+1,'0000')\n"
                + "  FROM [QNBSER]\n"
                + "  WHERE [QNBTYPE] = REPLACE([XMSOTHWHS],'WH','L')\n"
                + "  AND [QNBGROUP] = FORMAT(CURRENT_TIMESTAMP,'MM')\n"
                + "  AND [QNBYEAR] = FORMAT(CURRENT_TIMESTAMP,'yyyy')\n"
                + "					)\n"
                + "  WHERE [XMSOTHQNO] = '" + qno + "' AND XMSOTHDOCNO IS NULL ";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public XMSOTHEAD findByQno(String qno) {

        XMSOTHEAD p = new XMSOTHEAD();

        String sql = "SELECT [XMSOTHQNO]\n"
                + "      ,[XMSOTHWHS]\n"
                + "	  ,[QWHNAME] AS WHSNAME\n"
                + "      ,FORMAT([XMSOTHSPDT],'yyyy-MM-dd') AS [XMSOTHSPDT]\n"
                + "      ,[XMSOTHDEST]\n"
                + "	  ,[QDEDESC] AS DESTNAME\n"
                + "      ,[XMSOTHROUND]\n"
                + "      ,[XMSOTHTYPE]\n"
                + "      ,[XMSOTHLICENNO]\n"
                + "      ,[XMSOTHDRIVER]\n"
                + "      ,[XMSOTHFOLLOWER]\n"
                + "      ,[XMSOTHBAG]\n"
                + "      ,[XMSOTHROLL]\n"
                + "      ,[XMSOTHBOX]\n"
                + "      ,[XMSOTHPCS]\n"
                + "      ,[XMSOTHTOTDOC]\n"
                + "  FROM [RMShipment].[dbo].[XMSOTHEAD]\n"
                + "  LEFT JOIN [QWHMAS] ON [QWHMAS].[QWHCOD] = [XMSOTHEAD].[XMSOTHWHS]\n"
                + "  LEFT JOIN QDEST ON QDEST.QDECOD = [XMSOTHEAD].[XMSOTHDEST]\n"
                + "  WHERE XMSOTHQNO = '" + qno + "'";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                p.setXMSOTHQNO(result.getString("XMSOTHQNO"));
                p.setXMSOTHWHS(result.getString("XMSOTHWHS"));
                p.setWhn(result.getString("WHSNAME"));

                p.setXMSOTHSPDT(result.getString("XMSOTHSPDT"));
                p.setXMSOTHDEST(result.getString("XMSOTHDEST"));
                p.setDestn(result.getString("DESTNAME"));

                p.setXMSOTHROUND(result.getString("XMSOTHROUND"));
                p.setXMSOTHTYPE(result.getString("XMSOTHTYPE"));
                p.setXMSOTHLICENNO(result.getString("XMSOTHLICENNO"));
                p.setXMSOTHDRIVER(result.getString("XMSOTHDRIVER"));
                p.setXMSOTHFOLLOWER(result.getString("XMSOTHFOLLOWER"));
                p.setXMSOTHBAG(result.getString("XMSOTHBAG"));
                p.setXMSOTHROLL(result.getString("XMSOTHROLL"));
                p.setXMSOTHBOX(result.getString("XMSOTHBOX"));
                p.setXMSOTHPCS(result.getString("XMSOTHPCS"));
                p.setXMSOTHTOTDOC(result.getString("XMSOTHTOTDOC"));

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return p;

    }

    public String findMaxQno() {

        String p = "";

        String sql = "SELECT ISNULL(MAX(XMSOTHQNO),0)+1 AS MAXQNO FROM XMSOTHEAD ";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                p = result.getString("MAXQNO");

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return p;

    }

    public List<XMSOTHEAD> findAll(String wh, String shipDate) {

        List<XMSOTHEAD> UAList = new ArrayList<XMSOTHEAD>();

        String sql = "SELECT [XMSOTHQNO]\n"
                + "      ,[XMSOTHWHS]\n"
                + "      ,[XMSOTHSPDT]\n"
                + "      ,[XMSOTHDEST] AS [DEST]\n"
                + "      ,[XMSOTHDEST]+' : '+[QDEDESC] AS [XMSOTHDEST]\n"
                + "      ,[XMSOTHROUND]\n"
                + "      ,[XMSOTHTYPE]\n"
                + "      ,[XMSOTHLICENNO]\n"
                + "      ,[XMSOTHDRIVER]\n"
                + "      ,[XMSOTHFOLLOWER]\n"
                + "      ,format(isnull([XMSOTHBAG],0),'#,#0') as XMSOTHBAG\n"
                + "      ,format(isnull([XMSOTHROLL],0),'#,#0') as XMSOTHROLL\n"
                + "      ,format(isnull([XMSOTHBOX],0),'#,#0') as XMSOTHBOX\n"
                + "      ,format(isnull([XMSOTHPCS],0),'#,#0') as XMSOTHPCS\n"
                + "      ,format([XMSOTHTOTDOC],'#,#0') as XMSOTHTOTDOC\n"
                + "	  ,[XMSOTHUSER]+' : '+[USERS] AS [XMSOTHUSER]\n"
                + "	  ,[XMSOTHDOCNO]\n"
                + "  FROM [RMShipment].[dbo].[XMSOTHEAD]\n"
                + "  LEFT JOIN [MSSUSER] ON [USERID] = [XMSOTHUSER]\n"
                + "  LEFT JOIN [QDEST] ON [QDECOD] = [XMSOTHDEST]\n"
                + "  WHERE [XMSOTHWHS] = '" + wh + "' AND FORMAT([XMSOTHSPDT],'yyyy-MM-dd') = '" + shipDate + "'\n"
                + "UNION ALL\n"
                + "SELECT NULL, NULL, NULL, XMSOTHDEST\n"
                + ",[XMSOTHDEST]+' : '+[QDEDESC] AS [XMSOTHDEST]\n"
                + ",NULL, NULL, NULL, 'TOTAL : '+[XMSOTHDEST], NULL\n"
                + ",XMSOTHBAG, XMSOTHROLL, XMSOTHBOX, XMSOTHPCS, [XMSOTHTOTDOC], NULL, NULL\n"
                + "FROM(\n"
                + "SELECT [XMSOTHDEST]\n"
                + "      ,format(SUM(isnull([XMSOTHBAG],0)),'#,#0') as XMSOTHBAG\n"
                + "      ,format(SUM(isnull([XMSOTHROLL],0)),'#,#0') as XMSOTHROLL\n"
                + "      ,format(SUM(isnull([XMSOTHBOX],0)),'#,#0') as XMSOTHBOX\n"
                + "      ,format(SUM(isnull([XMSOTHPCS],0)),'#,#0') as XMSOTHPCS\n"
                + "      ,format(SUM(isnull([XMSOTHTOTDOC],0)),'#,#0') as [XMSOTHTOTDOC]\n"
                + "  FROM [RMShipment].[dbo].[XMSOTHEAD]\n"
                + "  WHERE [XMSOTHWHS] = '" + wh + "' AND FORMAT([XMSOTHSPDT],'yyyy-MM-dd') = '" + shipDate + "'\n"
                + "  GROUP BY [XMSOTHDEST]\n"
                + ")TMPTOT\n"
                + "LEFT JOIN [QDEST] ON [QDECOD] = [XMSOTHDEST]\n"
                + "ORDER BY [XMSOTHDEST], [XMSOTHQNO]";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                if (result.getString("XMSOTHDRIVER").contains("TOTAL")) {
                    XMSOTHEAD p = new XMSOTHEAD();

                    p.setXMSOTHQNO(result.getString("XMSOTHQNO"));
                    p.setXMSOTHWHS(result.getString("XMSOTHWHS"));
                    p.setXMSOTHSPDT(result.getString("XMSOTHSPDT"));
                    p.setXMSOTHDEST("<b style=\"opacity: 0;\">" + result.getString("XMSOTHDEST") + "</b>");
                    p.setXMSOTHROUND(result.getString("XMSOTHROUND"));
                    p.setXMSOTHTYPE(result.getString("XMSOTHTYPE"));
                    p.setXMSOTHLICENNO(result.getString("XMSOTHLICENNO"));
                    p.setXMSOTHDRIVER("<b style=\"color: #00399b;\">" + result.getString("XMSOTHDRIVER") + "</b>");
                    p.setXMSOTHFOLLOWER(result.getString("XMSOTHFOLLOWER"));
                    p.setXMSOTHBAG("<b style=\"color: #00399b;\">" + result.getString("XMSOTHBAG") + "</b>");
                    p.setXMSOTHROLL("<b style=\"color: #00399b;\">" + result.getString("XMSOTHROLL") + "</b>");
                    p.setXMSOTHBOX("<b style=\"color: #00399b;\">" + result.getString("XMSOTHBOX") + "</b>");
                    p.setXMSOTHPCS("<b style=\"color: #00399b;\">" + result.getString("XMSOTHPCS") + "</b>");
                    p.setXMSOTHTOTDOC("<b style=\"color: #00399b;\">" + result.getString("XMSOTHTOTDOC") + "</b>");
                    p.setXMSOTHUSER(result.getString("XMSOTHUSER"));

//                    p.setOptions("<a href=\"print?printBy=dest&wh=" + wh + "&shipDate=" + shipDate + "&dest=" + result.getString("DEST") + "\" target=\"_blank\"><i class=\"fa fa-print\" style=\"font-size:28px; padding-left: 10px; color: #c21b1b;\"></i></a>");
                    UAList.add(p);
                } else {
                    XMSOTHEAD p = new XMSOTHEAD();

                    p.setXMSOTHQNO(result.getString("XMSOTHQNO"));
                    p.setXMSOTHWHS(result.getString("XMSOTHWHS"));
                    p.setXMSOTHSPDT(result.getString("XMSOTHSPDT"));
                    p.setXMSOTHDEST(result.getString("XMSOTHDEST"));
                    p.setXMSOTHROUND(result.getString("XMSOTHROUND"));
                    p.setXMSOTHTYPE(result.getString("XMSOTHTYPE"));
                    p.setXMSOTHLICENNO(result.getString("XMSOTHLICENNO"));
                    p.setXMSOTHDRIVER(result.getString("XMSOTHDRIVER"));
                    p.setXMSOTHFOLLOWER(result.getString("XMSOTHFOLLOWER"));
                    p.setXMSOTHBAG(result.getString("XMSOTHBAG"));
                    p.setXMSOTHROLL(result.getString("XMSOTHROLL"));
                    p.setXMSOTHBOX(result.getString("XMSOTHBOX"));
                    p.setXMSOTHPCS(result.getString("XMSOTHPCS"));
                    p.setXMSOTHTOTDOC(result.getString("XMSOTHTOTDOC"));
                    p.setXMSOTHUSER(result.getString("XMSOTHUSER"));
                    p.setXMSOTHDOCNO(result.getString("XMSOTHDOCNO"));

                    String type = "";
                    if (result.getString("XMSOTHTYPE").equals("��ż�ҹ")) {
                        type = "bill";

                    } else if (result.getString("XMSOTHTYPE").equals("�ѵ�شԺ")) {
                        type = "mat";

                    } else if (result.getString("XMSOTHTYPE").equals("����")) {
                        type = "ot";

                    }

                    p.setOptions("<a href=\"edit?qno=" + result.getString("XMSOTHQNO") + "\"><i class=\"fa fa-edit\" style=\"font-size:28px; padding-left: 10px\"></i></a>\n"
                            + "<a onclick=\"document.getElementById('myModal-del-" + result.getString("XMSOTHQNO") + "').style.display = 'block';\" style=\"cursor: pointer;\"><i class=\"fa fa-trash\" style=\"font-size:28px; padding-left: 10px\"></i></a>\n"
                            + "<a href=\"print?printBy=qno&size=a4&wh=" + wh + "&qno=" + result.getString("XMSOTHQNO") + "&type=" + type + "\" target=\"_blank\"><span style=\"font-size:25px; padding-left: 10px\">A4</span></a>\n"
                            + "<a href=\"print?printBy=qno&size=a5&wh=" + wh + "&qno=" + result.getString("XMSOTHQNO") + "&type=" + type + "\" target=\"_blank\"><span style=\"font-size:25px; padding-left: 10px\">A5</span></a>");

                    UAList.add(p);
                }

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public boolean edit(XMSOTHEAD head) {

        boolean result = false;

        String sql = "UPDATE XMSOTHEAD "
                + "SET [XMSOTHWHS] = '" + head.getXMSOTHWHS() + "'\n"
                + "      ,[XMSOTHSPDT] = '" + head.getXMSOTHSPDT() + "'\n"
                + "      ,[XMSOTHDEST] = '" + head.getXMSOTHDEST() + "'\n"
                + "      ,[XMSOTHROUND] = '" + head.getXMSOTHROUND() + "'\n"
                + "      ,[XMSOTHTYPE] = '" + head.getXMSOTHTYPE() + "'\n"
                + "      ,[XMSOTHLICENNO] = '" + head.getXMSOTHLICENNO() + "'\n"
                + "      ,[XMSOTHDRIVER] = '" + head.getXMSOTHDRIVER() + "'\n"
                + "      ,[XMSOTHFOLLOWER] = '" + head.getXMSOTHFOLLOWER() + "'\n"
                + "      ,[XMSOTHBAG] = '" + head.getXMSOTHBAG() + "'\n"
                + "      ,[XMSOTHROLL] = '" + head.getXMSOTHROLL() + "'\n"
                + "      ,[XMSOTHBOX] = '" + head.getXMSOTHBOX() + "'\n"
                + "      ,[XMSOTHPCS] = '" + head.getXMSOTHPCS() + "'\n"
                + "      ,[XMSOTHTOTDOC] = '" + head.getXMSOTHTOTDOC() + "'\n"
                + "      ,[XMSOTHEDT] = CURRENT_TIMESTAMP\n"
                + "      ,[XMSOTHUSER] = '" + head.getXMSOTHUSER() + "'\n"
                + "WHERE [XMSOTHQNO] = '" + head.getXMSOTHQNO() + "'";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean add(XMSOTHEAD head) {

        boolean result = false;

        String sql = "INSERT INTO XMSOTHEAD "
                + "([XMSOTHCOM]\n"
                + "      ,[XMSOTHQNO]\n"
                + "      ,[XMSOTHWHS]\n"
                + "      ,[XMSOTHSPDT]\n"
                + "      ,[XMSOTHDEST]\n"
                + "      ,[XMSOTHROUND]\n"
                + "      ,[XMSOTHTYPE]\n"
                + "      ,[XMSOTHLICENNO]\n"
                + "      ,[XMSOTHDRIVER]\n"
                + "      ,[XMSOTHFOLLOWER]\n"
                + "      ,[XMSOTHBAG]\n"
                + "      ,[XMSOTHROLL]\n"
                + "      ,[XMSOTHBOX]\n"
                + "      ,[XMSOTHPCS]\n"
                + "      ,[XMSOTHTOTDOC]\n"
                + "      ,[XMSOTHEDT]\n"
                + "      ,[XMSOTHCDT]\n"
                + "      ,[XMSOTHUSER]) "
                + "VALUES('TWC'"
                + ",'" + head.getXMSOTHQNO() + "'"
                + ",'" + head.getXMSOTHWHS() + "'"
                + ",'" + head.getXMSOTHSPDT() + "'"
                + ",'" + head.getXMSOTHDEST() + "'"
                + ",'" + head.getXMSOTHROUND() + "'"
                + ",'" + head.getXMSOTHTYPE() + "'"
                + ",'" + head.getXMSOTHLICENNO() + "'"
                + ",'" + head.getXMSOTHDRIVER() + "'"
                + ",'" + head.getXMSOTHFOLLOWER() + "'"
                + ",'" + head.getXMSOTHBAG() + "'"
                + ",'" + head.getXMSOTHROLL() + "'"
                + ",'" + head.getXMSOTHBOX() + "'"
                + ",'" + head.getXMSOTHPCS() + "'"
                + ",'" + head.getXMSOTHTOTDOC() + "'"
                + ",CURRENT_TIMESTAMP"
                + ",CURRENT_TIMESTAMP"
                + ",'" + head.getXMSOTHUSER() + "') ";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public void delete(String qno) {

        String sql = "DELETE FROM XMSOTHEAD WHERE XMSOTHQNO = '" + qno + "'";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ps.executeUpdate();

        } catch (Exception e) {

            e.printStackTrace();

        }

    }
}
