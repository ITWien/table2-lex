/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.dao;

import com.twc.wms.database.database;
import com.twc.wms.entity.QDESTYP;
import com.twc.wms.entity.Qdest;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 *
 * @author nutthawoot.noo
 */
public class QdestXMSDao extends database {

    public List<Qdest> findByType(String shipDate) {

        List<Qdest> QdestList = new ArrayList<Qdest>();

        String sql = "SELECT *\n"
                + "FROM(\n"
                + "SELECT A.QDECOD, A.QDEDESC, A.QDETYPE, B.QDEDESC as QDEDESC2 \n"
                + "  ,(SELECT COUNT([XMSOTHDEST]) FROM [XMSOTHEAD] WHERE [XMSOTHDEST] = A.QDECOD AND FORMAT(XMSOTHSPDT,'yyyy-MM-dd') = '" + shipDate + "') AS CNT\n"
                + "  FROM QDEST as A\n"
                + "  LEFT JOIN QDESTYP as B \n"
                + "  ON A.QDETYPE = B.QDETYPE\n"
                + "  WHERE A.QDETYPE = '2'\n"
                + "  AND A.QDECOD not in ('E01','E02','B20','B21','E03','B22','E04','B23','E05','B24')\n"
                + "UNION ALL\n"
                + "SELECT 'KPC', '��Թ������ (E01,E02,B20,B21)', '2', 'External (TWC Group)', (SELECT COUNT([XMSOTHDEST]) FROM [XMSOTHEAD] WHERE ([XMSOTHDEST] = 'E01' OR [XMSOTHDEST] = 'E02' OR [XMSOTHDEST] = 'B20' OR [XMSOTHDEST] = 'B21') AND FORMAT(XMSOTHSPDT,'yyyy-MM-dd') = '" + shipDate + "')\n"
                + "UNION ALL\n"
                + "SELECT 'WSC', '��������Ҫ� (E03,B22)', '2', 'External (TWC Group)', (SELECT COUNT([XMSOTHDEST]) FROM [XMSOTHEAD] WHERE ([XMSOTHDEST] = 'E03' OR [XMSOTHDEST] = 'B22') AND FORMAT(XMSOTHSPDT,'yyyy-MM-dd') = '" + shipDate + "')\n"
                + "UNION ALL\n"
                + "SELECT 'WLC', '�����Ӿٹ (E04,B23)', '2', 'External (TWC Group)', (SELECT COUNT([XMSOTHDEST]) FROM [XMSOTHEAD] WHERE ([XMSOTHDEST] = 'E04' OR [XMSOTHDEST] = 'B23') AND FORMAT(XMSOTHSPDT,'yyyy-MM-dd') = '" + shipDate + "')\n"
                + "UNION ALL\n"
                + "SELECT 'MWC', '���¹�������� (E05,B24)', '2', 'External (TWC Group)', (SELECT COUNT([XMSOTHDEST]) FROM [XMSOTHEAD] WHERE ([XMSOTHDEST] = 'E05' OR [XMSOTHDEST] = 'B24') AND FORMAT(XMSOTHSPDT,'yyyy-MM-dd') = '" + shipDate + "')\n"
                + ")TMP \n"
                + "WHERE CNT > 0\n"
                + "ORDER BY QDECOD";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                Qdest p = new Qdest();

                p.setCode(result.getString("QDECOD"));

                p.setDesc(result.getString("QDEDESC"));

                p.setType(result.getString("QDETYPE") + " : " + result.getString("QDEDESC2"));

                QdestList.add(p);

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return QdestList;

    }

    public List<Qdest> findByWhs(String wh) {

        List<Qdest> QdestList = new ArrayList<Qdest>();

        String sql = "SELECT A.QDECOD, A.QDEDESC, A.QDETYPE, B.QDEDESC as QDEDESC2 FROM QDEST as A\n"
                + "  LEFT JOIN QDESTYP as B \n"
                + "  ON A.QDETYPE = B.QDETYPE"
                + "  WHERE QDEWHS = '" + wh + "'";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                Qdest p = new Qdest();

                p.setCode(result.getString("QDECOD"));

                p.setDesc(result.getString("QDEDESC"));

                p.setType(result.getString("QDETYPE") + " : " + result.getString("QDEDESC2"));

                QdestList.add(p);

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return QdestList;

    }

    public List<Qdest> findAll() {

        List<Qdest> QdestList = new ArrayList<Qdest>();

        String sql = "SELECT A.QDECOD, A.QDEDESC, A.QDETYPE, B.QDEDESC as QDEDESC2, A.QDEWHS FROM QDEST as A\n"
                + "  LEFT JOIN QDESTYP as B \n"
                + "  ON A.QDETYPE = B.QDETYPE";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                Qdest p = new Qdest();

                p.setCode(result.getString("QDECOD"));

                p.setDesc(result.getString("QDEDESC"));

                p.setType(result.getString("QDETYPE") + " : " + result.getString("QDEDESC2"));

                p.setWh(result.getString("QDEWHS"));

                QdestList.add(p);

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return QdestList;

    }

    public String check(String code) {
        PreparedStatement ps = null;
        ResultSet rs = null;
        String STRRETURN = null;
        try {
            String check = "Select * FROM [RMShipment].[dbo].[QDEST] where [QDECOD] = '" + code + "'";
            ps = connect.prepareStatement(check);
            rs = ps.executeQuery();
            if (rs.next()) {
                STRRETURN = "f";
            } else {
                STRRETURN = "t";
            }
        } catch (Exception exp) {
            exp.printStackTrace();
        }
        return STRRETURN;
    }

    public boolean add(Qdest qdest, String uid, String detype) {

        boolean result = false;

        String sql = "INSERT INTO QDEST"
                + " (QDECOM, QDECOD, QDEDESC, QDENAME, QDEADDR1, QDEADDR2, QDEADDR3, QDEADDR4, QDEEDT, QDECDT, QDEUSER, QDETYPE)"
                + " VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ps.setString(1, "TWC");

            ps.setString(2, qdest.getCode());

            ps.setString(3, qdest.getDesc());

            ps.setString(4, qdest.getName());

            ps.setString(5, qdest.getAdrs1());

            ps.setString(6, qdest.getAdrs2());

            ps.setString(7, qdest.getAdrs3());

            ps.setString(8, qdest.getAdrs4());

            ps.setString(12, detype);

            Calendar cal = Calendar.getInstance();
            Timestamp timestamp = new Timestamp(cal.getTimeInMillis());
            ps.setTimestamp(9, timestamp);

            ps.setTimestamp(10, timestamp);

            ps.setString(11, uid);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public Qdest findByCod(String cod) {

        Qdest qd = new Qdest();

        String sql = "SELECT * FROM QDEST WHERE QDECOD = ?";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ps.setString(1, cod);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                qd.setCode(result.getString("QDECOD"));

                qd.setDesc(result.getString("QDEDESC"));

                qd.setName(result.getString("QDENAME"));

                qd.setAdrs1(result.getString("QDEADDR1"));

                qd.setAdrs2(result.getString("QDEADDR2"));

                qd.setAdrs3(result.getString("QDEADDR3"));

                qd.setAdrs4(result.getString("QDEADDR4"));

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return qd;

    }

    public boolean edit(Qdest qdest, String uid, String detype) {

        boolean result = false;

        String sql = "UPDATE QDEST SET QDEDESC = ?, QDEEDT = ?"
                + ", QDENAME = ?, QDEADDR1 = ?, QDEADDR2 = ?, QDEADDR3 = ?, QDEADDR4 = ?, QDEUSER = ?, QDETYPE = ?"
                + " WHERE QDECOD = ?";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ps.setString(10, qdest.getCode());

            ps.setString(1, qdest.getDesc());

            Calendar cal = Calendar.getInstance();
            Timestamp timestamp = new Timestamp(cal.getTimeInMillis());
            ps.setTimestamp(2, timestamp);

            ps.setString(3, qdest.getName());

            ps.setString(4, qdest.getAdrs1());

            ps.setString(5, qdest.getAdrs2());

            ps.setString(6, qdest.getAdrs3());

            ps.setString(7, qdest.getAdrs4());

            ps.setString(8, uid);

            ps.setString(9, detype);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public void delete(String cod) {

        String sql = "DELETE FROM QDEST WHERE QDECOD = ?";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ps.setString(1, cod);

            ps.executeUpdate();

        } catch (Exception e) {

            e.printStackTrace();

        }

    }

    public List<QDESTYP> Selecttype() {

        List<QDESTYP> QdesttypeList = new ArrayList<QDESTYP>();

        String sql = "SELECT * FROM QDESTYP";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                QDESTYP p = new QDESTYP();

                p.setQDETYPE(result.getString("QDETYPE"));
                p.setQDEDESC(result.getString("QDEDESC"));

                QdesttypeList.add(p);

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return QdesttypeList;

    }
}
