/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.dao;

import com.twc.wms.database.database;
import com.twc.wms.entity.QRMTRA_2;
import java.sql.*;

import java.util.ArrayList;

import java.util.List;

import com.twc.wms.entity.QRMTRA_3;
import com.twc.wms.entity.UserAuth;

/**
 *
 * @author nutthawoot.noo
 */
public class QRMTRA_3Dao extends database {

    public List<QRMTRA_3> selectPO() {

        List<QRMTRA_3> QMList = new ArrayList<QRMTRA_3>();

        String sql = "SELECT DISTINCT TOP 10 QRMDOCNO FROM QRMTRA "
                + "WHERE (QRMMVT = '201' OR QRMMVT = '261' OR QRMMVT = '601') "
                + "ORDER BY QRMDOCNO ";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                QRMTRA_3 p = new QRMTRA_3();

                p.setPo(result.getString("QRMDOCNO"));

                QMList.add(p);

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return QMList;

    }

    public List<QRMTRA_3> selectPlant() {

        List<QRMTRA_3> QMList = new ArrayList<QRMTRA_3>();

        String sql = "SELECT DISTINCT TOP 10 QRMPLANT FROM QRMTRA "
                + "WHERE (QRMMVT = '201' OR QRMMVT = '261' OR QRMMVT = '601') "
                + "ORDER BY QRMPLANT ";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                QRMTRA_3 p = new QRMTRA_3();

                p.setPlant(result.getString("QRMPLANT"));

                QMList.add(p);

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return QMList;

    }

    public List<QRMTRA_3> selectTax() {

        List<QRMTRA_3> QMList = new ArrayList<QRMTRA_3>();

        String sql = "SELECT DISTINCT TOP 10 QRMSAPNO FROM QRMTRA "
                + "WHERE (QRMMVT = '201' OR QRMMVT = '261' OR QRMMVT = '601') "
                + "ORDER BY QRMSAPNO ";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                QRMTRA_3 p = new QRMTRA_3();

                p.setTax(result.getString("QRMSAPNO"));

                QMList.add(p);

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return QMList;

    }

    public List<QRMTRA_3> selectMatCode() {

        List<QRMTRA_3> QMList = new ArrayList<QRMTRA_3>();

        String sql = "SELECT DISTINCT TOP 10 QRMCODE FROM QRMTRA "
                + "WHERE (QRMMVT = '201' OR QRMMVT = '261' OR QRMMVT = '601') "
                + "ORDER BY QRMCODE ";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                QRMTRA_3 p = new QRMTRA_3();

                p.setMatCode(result.getString("QRMCODE"));

                QMList.add(p);

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return QMList;

    }

    public List<QRMTRA_3> selectUser() {

        List<QRMTRA_3> QMList = new ArrayList<QRMTRA_3>();

        String sql = "SELECT DISTINCT TOP 10 QRMUSER FROM QRMTRA "
                + "WHERE (QRMMVT = '201' OR QRMMVT = '261' OR QRMMVT = '601') "
                + "ORDER BY QRMUSER ";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                QRMTRA_3 p = new QRMTRA_3();

                p.setUser(result.getString("QRMUSER"));

                QMList.add(p);

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return QMList;

    }

    public ResultSet findTranDate(String StranDate, String FtranDate,
            String Swh, String Fwh, String Splant, String Fplant,
            String Spo, String Fpo, String Stax, String Ftax,
            String SmatCode, String FmatCode, String Suser, String Fuser) {

        List<QRMTRA_3> WHList = new ArrayList<QRMTRA_3>();

        String QRMWHSE = "";
        String QRMPLANT = "";
        String QRMDOCNO = "";
        String QRMSAPNO = "";
        String QRMCODE = "";
        String QRMUSER = "";

        if (!Swh.equals("")) {
            QRMWHSE = "AND (QRMWHSE BETWEEN '" + Swh + "' AND '" + Fwh + "') ";
        }
        if (!Splant.equals("")) {
            QRMPLANT = "AND (QRMPLANT BETWEEN '" + Splant + "' AND '" + Fplant + "') ";
        }
        if (!Spo.equals("")) {
            QRMDOCNO = "AND (QRMDOCNO BETWEEN '" + Spo + "' AND '" + Fpo + "') ";
        }
        if (!Stax.equals("")) {
            QRMSAPNO = "AND (QRMSAPNO BETWEEN '" + Stax + "' AND '" + Ftax + "') ";
        }
        if (!SmatCode.equals("")) {
            QRMCODE = "AND (QRMCODE BETWEEN '" + SmatCode + "' AND '" + FmatCode + "') ";
        }
        if (!Suser.equals("")) {
            QRMUSER = "AND (QRMUSER BETWEEN '" + Suser + "' AND '" + Fuser + "') ";
        }

        String sql = "SELECT distinct QRMWHSE + ' : ' + (select QWHNAME from QWHMAS where QWHCOD = QRMWHSE) as QRMWHSE, QRMPLANT,\n"
                + "SUBSTRING(CONVERT(VARCHAR(25), QRMTRDT, 126), 9, 2) + '-' +\n"
                + "SUBSTRING(CONVERT(VARCHAR(25), QRMTRDT, 126), 6, 2) + '-' +\n"
                + "SUBSTRING(CONVERT(VARCHAR(25), QRMTRDT, 126), 1, 4) as QRMTRDT,\n"
                + "CASE WHEN QRMDOCNO IS NULL THEN ' ' ELSE QRMDOCNO END AS QRMDOCNO, CASE WHEN QRMSAPNO IS NULL THEN ' ' ELSE QRMSAPNO END AS QRMSAPNO, TRA.QRMCODE,\n"
                + "CASE WHEN (SELECT TOP 1 QRMDESC FROM QRMMAS WHERE QRMCODE = TRA.QRMCODE) IS NULL THEN\n"
                + "(SELECT TOP 1 SAPDESC FROM SAPMAS WHERE SAPMAT = TRA.QRMCODE AND SAPPLANT = TRA.QRMPLANT AND SAPVAL = TRA.QRMVAL) ELSE\n"
                + "(SELECT TOP 1 QRMDESC FROM QRMMAS WHERE QRMCODE = TRA.QRMCODE) END as NAME,\n"
                + "(SELECT SUM(QRMQTY) FROM QRMTRA TA WHERE (QRMTRDT BETWEEN '" + StranDate + "' AND '" + FtranDate + "') " + QRMWHSE + QRMPLANT + QRMDOCNO + QRMSAPNO + QRMCODE + QRMUSER + " AND (QRMMVT = '201' OR QRMMVT = '261' OR QRMMVT = '601') AND QRMCODE = TRA.QRMCODE) AS QTY,\n"
                + "QRMBUN,\n"
                + "(SELECT COUNT(QRMCODE) FROM QRMTRA TA WHERE (QRMTRDT BETWEEN '" + StranDate + "' AND '" + FtranDate + "') " + QRMWHSE + QRMPLANT + QRMDOCNO + QRMSAPNO + QRMCODE + QRMUSER + " AND (QRMMVT = '201' OR QRMMVT = '261' OR QRMMVT = '601') AND QRMCODE = TRA.QRMCODE AND (SELECT TOP 1 QRMPACKTYPE FROM QRMMAS MS WHERE QRMCODE = TA.QRMCODE) = 'BOX') as BOX,\n"
                + "(SELECT COUNT(QRMCODE) FROM QRMTRA TA WHERE (QRMTRDT BETWEEN '" + StranDate + "' AND '" + FtranDate + "') " + QRMWHSE + QRMPLANT + QRMDOCNO + QRMSAPNO + QRMCODE + QRMUSER + " AND (QRMMVT = '201' OR QRMMVT = '261' OR QRMMVT = '601') AND QRMCODE = TRA.QRMCODE AND ((SELECT TOP 1 QRMPACKTYPE FROM QRMMAS MS WHERE QRMCODE = TA.QRMCODE) = 'BAG' OR (SELECT TOP 1 QRMPACKTYPE FROM QRMMAS MS WHERE QRMCODE = TA.QRMCODE) = 'PACK')) as BAG,\n"
                + "(SELECT COUNT(QRMCODE) FROM QRMTRA TA WHERE (QRMTRDT BETWEEN '" + StranDate + "' AND '" + FtranDate + "') " + QRMWHSE + QRMPLANT + QRMDOCNO + QRMSAPNO + QRMCODE + QRMUSER + " AND (QRMMVT = '201' OR QRMMVT = '261' OR QRMMVT = '601') AND QRMCODE = TRA.QRMCODE AND (SELECT TOP 1 QRMPACKTYPE FROM QRMMAS MS WHERE QRMCODE = TA.QRMCODE) = 'ROLL') as ROLL\n"
                + "FROM QRMTRA TRA\n"
                + "WHERE (QRMTRDT BETWEEN '" + StranDate + "' AND '" + FtranDate + "') "
                + QRMWHSE + QRMPLANT + QRMDOCNO + QRMSAPNO + QRMCODE + QRMUSER
                + "AND (QRMMVT = '201' OR QRMMVT = '261' OR QRMMVT = '601')\n"
                + "ORDER BY QRMWHSE, QRMPLANT, QRMTRDT, QRMDOCNO, QRMSAPNO, QRMCODE";
//        String sql = "SELECT * FROM QRMTRA "
//                + "WHERE (QRMTRDT BETWEEN '" + StranDate + "' AND '" + FtranDate + "') "
//                + QRMWHSE
//                + QRMPLANT
//                + QRMDOCNO
//                + QRMSAPNO
//                + QRMCODE
//                + QRMUSER
//                + "AND (QRMMVT = '201' OR QRMMVT = '261' OR QRMMVT = '601') "
//                + "ORDER BY QRMWHSE, QRMPLANT, QRMTRDT, QRMDOCNO, QRMSAPNO, QRMCODE ";
//        String sql = "SELECT * FROM QRMTRA "
//                + "WHERE QRMRUNNO BETWEEN '2011' AND '2114' "
//                + "ORDER BY QRMWHSE, QRMPLANT, QRMTRDT, QRMDOCNO, QRMSAPNO, QRMCODE ";
//                + " WHERE (QRMTRDT BETWEEN '2018-09-01' AND '2018-11-30')"
//                + " AND (QRMWHSE BETWEEN 'TWC' AND 'WH3')"
//                + " AND (QRMPLANT BETWEEN '1000' AND '1050')"
//                + " AND (QRMMVT = '201' OR QRMMVT = '261' OR QRMMVT = '601')";
//        System.out.println(sql);
        ResultSet result = null;
        try {

            PreparedStatement ps = connect.prepareStatement(sql);
            result = ps.executeQuery();

//            while (result.next()) {
//
//                QRMTRA_3 p = new QRMTRA_3();
//
//                WarehouseDao dao4 = new WarehouseDao();
//
//                p.setWH(result.getString("QRMWHSE") + " " + dao4.findWHname(result.getString("QRMWHSE")));
//                p.setPlant(result.getString("QRMPLANT"));
//
//                String trdDB = "";
//                if (result.getString("QRMTRDT") == null) {
//                    trdDB = "-";
//                } else {
//                    trdDB = result.getString("QRMTRDT");
//                }
//
//                String trDate = trdDB.substring(8, 10) + "-" + trdDB.substring(5, 7) + "-" + trdDB.substring(0, 4);
//                p.setTranDate(trDate);
//
//                if (result.getString("QRMDOCNO") == null) {
//                    p.setPo("-");
//                } else {
//                    p.setPo(result.getString("QRMDOCNO"));
//                }
//
//                if (result.getString("QRMSAPNO") == null) {
//                    p.setTax("-");
//                } else {
//                    p.setTax(result.getString("QRMSAPNO"));
//                }
//
//                p.setMatCode(result.getString("QRMCODE"));
//
//                String qty = result.getString("QRMQTY");
//                qty = qty.substring(0, qty.length() - 3);
//                p.setQty(qty);
//                p.setUm(result.getString("QRMBUN"));
//
//                QRMMASDao dao1 = new QRMMASDao();
//                QRMTRA_2 ms = dao1.findByCode(result.getString("QRMCODE"));
//                int box = 0, bag = 0, roll = 0;
//
//                if (ms.getPack() != null) {
//                    if (ms.getPack().trim().equals("BOX")) {
//                        box += 1;
//                    } else if (ms.getPack().trim().equals("BAG") || ms.getPack().trim().equals("PACK")) {
//                        bag += 1;
//                    } else if (ms.getPack().trim().equals("ROLL")) {
//                        roll += 1;
//                    }
//                }
//
//                if (ms.getName() == null) {
//                    SAPMAS_2Dao dao5 = new SAPMAS_2Dao();
//                    QRMTRA_2 mas3 = dao5.findSAPDesc(result.getString("QRMCODE"),
//                            result.getString("QRMPLANT"), result.getString("QRMVAL"));
//                    p.setName(mas3.getName());
//                } else {
//                    p.setName(ms.getName());
//                }
//
//                p.setBox(box);
//                p.setBag(bag);
//                p.setRoll(roll);
//
//                WHList.add(p);
//
//            }
//
//            connect.close();
        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public ResultSet findDetail(String StranDate, String FtranDate,
            String Swh, String Fwh, String Splant, String Fplant,
            String Spo, String Fpo, String Stax, String Ftax,
            String SmatCode, String FmatCode, String Suser, String Fuser) {

        List<QRMTRA_3> WHList = new ArrayList<QRMTRA_3>();

        String QRMWHSE = "";
        String QRMPLANT = "";
        String QRMDOCNO = "";
        String QRMSAPNO = "";
        String QRMCODE = "";
        String QRMUSER = "";

        if (!Swh.equals("")) {
            QRMWHSE = "AND (QRMWHSE BETWEEN '" + Swh + "' AND '" + Fwh + "') ";
        }
        if (!Splant.equals("")) {
            QRMPLANT = "AND (QRMPLANT BETWEEN '" + Splant + "' AND '" + Fplant + "') ";
        }
        if (!Spo.equals("")) {
            QRMDOCNO = "AND (QRMDOCNO BETWEEN '" + Spo + "' AND '" + Fpo + "') ";
        }
        if (!Stax.equals("")) {
            QRMSAPNO = "AND (QRMSAPNO BETWEEN '" + Stax + "' AND '" + Ftax + "') ";
        }
        if (!SmatCode.equals("")) {
            QRMCODE = "AND (QRMCODE BETWEEN '" + SmatCode + "' AND '" + FmatCode + "') ";
        }
        if (!Suser.equals("")) {
            QRMUSER = "AND (QRMUSER BETWEEN '" + Suser + "' AND '" + Fuser + "') ";
        }

        String sql = "SELECT QRMWHSE + ' : ' + (select QWHNAME from QWHMAS where QWHCOD = QRMWHSE) as QRMWHSE, QRMPLANT,\n"
                + "SUBSTRING(CONVERT(VARCHAR(25), QRMTRDT, 126), 9, 2) + '-' +\n"
                + "SUBSTRING(CONVERT(VARCHAR(25), QRMTRDT, 126), 6, 2) + '-' +\n"
                + "SUBSTRING(CONVERT(VARCHAR(25), QRMTRDT, 126), 1, 4) as QRMTRDT,\n"
                + "CASE WHEN QRMDOCNO IS NULL THEN ' ' ELSE QRMDOCNO END AS QRMDOCNO, CASE WHEN QRMSAPNO IS NULL THEN ' ' ELSE QRMSAPNO END AS QRMSAPNO, TRA.QRMCODE,\n"
                + "CASE WHEN (SELECT TOP 1 QRMDESC FROM QRMMAS WHERE QRMCODE = TRA.QRMCODE) IS NULL THEN\n"
                + "(SELECT TOP 1 SAPDESC FROM SAPMAS WHERE SAPMAT = TRA.QRMCODE AND SAPPLANT = TRA.QRMPLANT AND SAPVAL = TRA.QRMVAL) ELSE\n"
                + "(SELECT TOP 1 QRMDESC FROM QRMMAS WHERE QRMCODE = TRA.QRMCODE) END as NAME,\n"
                + "QRMID, QRMQTY as QTY, QRMBUN,\n"
                + "(SELECT TOP 1 QRMPACKTYPE FROM QRMMAS MS WHERE QRMCODE = TRA.QRMCODE) as PACKAGE,\n"
                + "QRMROLL, QRMUSER, (SELECT USERS FROM MSSUSER WHERE USERID = QRMUSER) as USERNAME\n"
                + "FROM QRMTRA TRA\n"
                + "WHERE (QRMTRDT BETWEEN '" + StranDate + "' AND '" + FtranDate + "')\n"
                + QRMWHSE
                + QRMPLANT
                + QRMDOCNO
                + QRMSAPNO
                + QRMCODE
                + QRMUSER
                + "AND (QRMMVT = '201' OR QRMMVT = '261' OR QRMMVT = '601')\n"
                + "ORDER BY QRMWHSE, QRMPLANT, QRMTRDT, QRMDOCNO, QRMSAPNO, QRMCODE";
//        String sql = "SELECT * FROM QRMTRA "
//                + "WHERE (QRMTRDT BETWEEN '" + StranDate + "' AND '" + FtranDate + "') "
//                + QRMWHSE
//                + QRMPLANT
//                + QRMDOCNO
//                + QRMSAPNO
//                + QRMCODE
//                + QRMUSER
//                + "AND (QRMMVT = '201' OR QRMMVT = '261' OR QRMMVT = '601') "
//                + "ORDER BY QRMWHSE, QRMPLANT, QRMTRDT, QRMDOCNO, QRMSAPNO, QRMCODE ";
//        String sql = "SELECT * FROM QRMTRA "
//                + "WHERE QRMRUNNO BETWEEN '2011' AND '2114' "
//                + "ORDER BY QRMWHSE, QRMPLANT, QRMTRDT, QRMDOCNO, QRMSAPNO, QRMCODE ";
//                + " WHERE (QRMTRDT BETWEEN '2018-09-01' AND '2018-11-30')"
//                + " AND (QRMWHSE BETWEEN 'TWC' AND 'WH3')"
//                + " AND (QRMPLANT BETWEEN '1000' AND '1050')"
//                + " AND (QRMMVT = '201' OR QRMMVT = '261' OR QRMMVT = '601')";
//        System.out.println(sql);
        ResultSet result = null;
        try {

            PreparedStatement ps = connect.prepareStatement(sql);
            result = ps.executeQuery();

//            while (result.next()) {
//
//                QRMTRA_3 p = new QRMTRA_3();
//
//                WarehouseDao dao4 = new WarehouseDao();
//
//                p.setWH(result.getString("QRMWHSE") + " " + dao4.findWHname(result.getString("QRMWHSE")));
//                p.setPlant(result.getString("QRMPLANT"));
//
//                String trdDB = "";
//                if (result.getString("QRMTRDT") == null) {
//                    trdDB = "-";
//                } else {
//                    trdDB = result.getString("QRMTRDT");
//                }
//
//                String trDate = trdDB.substring(8, 10) + "-" + trdDB.substring(5, 7) + "-" + trdDB.substring(0, 4);
//                p.setTranDate(trDate);
//
//                if (result.getString("QRMDOCNO") == null) {
//                    p.setPo("-");
//                } else {
//                    p.setPo(result.getString("QRMDOCNO"));
//                }
//
//                if (result.getString("QRMSAPNO") == null) {
//                    p.setTax("-");
//                } else {
//                    p.setTax(result.getString("QRMSAPNO"));
//                }
//
//                p.setMatCode(result.getString("QRMCODE"));
//                p.setID(result.getString("QRMID"));
//
//                String qty = result.getString("QRMQTY");
//                qty = qty.substring(0, qty.length() - 3);
//                p.setQty(qty);
//                p.setUm(result.getString("QRMBUN"));
//                p.setNo(result.getString("QRMROLL"));
//
//                QRMMASDao dao1 = new QRMMASDao();
//                QRMTRA_2 mas = dao1.findDESC(result.getString("QRMID"));
//
//                if ((mas.getName() == null) || (mas.getPack() == null)) {
//                    QRMMASDao dao3 = new QRMMASDao();
//                    QRMTRA_2 mas2 = dao3.findByCode(result.getString("QRMCODE"));
//
//                    if ((mas2.getName() == null) || (mas2.getPack() == null)) {
//                        SAPMAS_2Dao dao5 = new SAPMAS_2Dao();
//                        QRMTRA_2 mas3 = dao5.findSAPDesc(result.getString("QRMCODE"),
//                                result.getString("QRMPLANT"), result.getString("QRMVAL"));
//                        if (mas3.getName() == null) {
//                            p.setName("-");
//                        } else {
//                            p.setName(mas3.getName());
//                        }
//                    } else {
//                        p.setName(mas2.getName());
//                        p.setPack(mas2.getPack());
//                    }
//
//                } else {
//                    p.setName(mas.getName());
//                    p.setPack(mas.getPack());
//                }
//
//                UserDao dao2 = new UserDao();
//                UserAuth ua = dao2.findByUid(result.getString("QRMUSER"));
//
//                String name = ua.getName();
//                String[] parts = name.split(" ");
//                String Fname = parts[0];
//                String Lname = parts[1];
//
//                p.setUser(result.getString("QRMUSER") + " : " + Fname);
//
//                WHList.add(p);
//
//            }
//
//            connect.close();
        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

}
