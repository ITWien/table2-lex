/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.dao;

import com.twc.wms.database.database;
import com.twc.wms.entity.QDESTYP;
import com.twc.wms.entity.Qdest;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import com.twc.wms.database.MSSQLDbConnectionPool;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author nutthawoot.noo
 */
public class QRMMATSTSDao {

    public List<Qdest> findAll() {

        List<Qdest> QdestList = new ArrayList<Qdest>();

        String sql = "SELECT * FROM QRMMATSTS ";

        try {

            Connection conn = null;
            Statement stmt = null;  // Or PreparedStatement if needed
            ResultSet result = null;
            try {
                conn = MSSQLDbConnectionPool.getConnection();
                stmt = conn.createStatement();
                result = stmt.executeQuery(sql);

                while (result.next()) {

                    Qdest p = new Qdest();

                    p.setCode(result.getString("STSCODE"));
                    p.setDesc(result.getString("STSDESC"));

                    QdestList.add(p);
                }

            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                // Always make sure result sets and statements are closed,
                // and the connection is returned to the pool
                if (result != null) {
                    try {
                        result.close();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                    result = null;
                }
                if (stmt != null) {
                    try {
                        stmt.close();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                    stmt = null;
                }
                if (conn != null) {
                    try {
                        conn.close();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                    conn = null;
                }
            }

        } catch (Exception e) {

            e.printStackTrace();

        }

        return QdestList;

    }

    public String check(String code) {
        String STRRETURN = null;
        try {
            String sql = "Select * FROM [RMShipment].[dbo].[QRMMATSTS] where [STSCODE] = '" + code + "'";

            Connection conn = null;
            Statement stmt = null;  // Or PreparedStatement if needed
            ResultSet result = null;
            try {
                conn = MSSQLDbConnectionPool.getConnection();
                stmt = conn.createStatement();
                result = stmt.executeQuery(sql);

                if (result.next()) {
                    STRRETURN = "f";
                } else {
                    STRRETURN = "t";
                }

            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                // Always make sure result sets and statements are closed,
                // and the connection is returned to the pool
                if (result != null) {
                    try {
                        result.close();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                    result = null;
                }
                if (stmt != null) {
                    try {
                        stmt.close();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                    stmt = null;
                }
                if (conn != null) {
                    try {
                        conn.close();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                    conn = null;
                }
            }
        } catch (Exception exp) {
            exp.printStackTrace();
        }
        return STRRETURN;
    }

    public boolean add(String code, String desc, String uid) {

        boolean resultb = false;

        String sql = "INSERT INTO QRMMATSTS"
                + " (STSCOM, STSCODE, STSDESC, STSEDT, STSCDT, STSUSER)"
                + " VALUES('TWC', '" + code + "', '" + desc + "', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '" + uid + "')";

//        System.out.println(sql);
        try {

            Connection conn = null;
            Statement stmt = null;  // Or PreparedStatement if needed
            ResultSet result = null;

            try {

                conn = MSSQLDbConnectionPool.getConnection();

                PreparedStatement ps = conn.prepareStatement(sql);

                int record = ps.executeUpdate();

                if (record >= 1) {

                    resultb = true;

                } else {

                    resultb = false;

                }

            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                // Always make sure result sets and statements are closed,
                // and the connection is returned to the pool
                if (result != null) {
                    try {
                        result.close();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                    result = null;
                }
                if (stmt != null) {
                    try {
                        stmt.close();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                    stmt = null;
                }
                if (conn != null) {
                    try {
                        conn.close();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                    conn = null;
                }
            }

        } catch (Exception e) {

            e.printStackTrace();

        }

        return resultb;

    }

    public boolean edit(String code, String desc, String uid) {

        boolean resultb = false;

        String sql = "UPDATE QRMMATSTS SET STSDESC = '" + desc + "', STSEDT = CURRENT_TIMESTAMP"
                + ", STSUSER = '" + uid + "'"
                + " WHERE STSCODE = '" + code + "'";

        try {

            Connection conn = null;
            Statement stmt = null;  // Or PreparedStatement if needed
            ResultSet result = null;

            try {

                conn = MSSQLDbConnectionPool.getConnection();

                PreparedStatement ps = conn.prepareStatement(sql);

                int record = ps.executeUpdate();

                if (record >= 1) {

                    resultb = true;

                } else {

                    resultb = false;

                }

            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                // Always make sure result sets and statements are closed,
                // and the connection is returned to the pool
                if (result != null) {
                    try {
                        result.close();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                    result = null;
                }
                if (stmt != null) {
                    try {
                        stmt.close();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                    stmt = null;
                }
                if (conn != null) {
                    try {
                        conn.close();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                    conn = null;
                }
            }

        } catch (Exception e) {

            e.printStackTrace();

        }

        return resultb;

    }

    public void delete(String code) {

        String sql = "DELETE FROM QRMMATSTS WHERE STSCODE = '" + code + "'";

        try {
            Connection conn = null;
            Statement stmt = null;  // Or PreparedStatement if needed
            ResultSet result = null;

            try {

                conn = MSSQLDbConnectionPool.getConnection();

                PreparedStatement ps = conn.prepareStatement(sql);

                ps.executeUpdate();

            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                // Always make sure result sets and statements are closed,
                // and the connection is returned to the pool
                if (result != null) {
                    try {
                        result.close();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                    result = null;
                }
                if (stmt != null) {
                    try {
                        stmt.close();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                    stmt = null;
                }
                if (conn != null) {
                    try {
                        conn.close();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                    conn = null;
                }
            }

        } catch (Exception e) {

            e.printStackTrace();

        }

    }

}
