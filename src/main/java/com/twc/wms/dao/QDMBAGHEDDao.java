/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.dao;

import com.twc.wms.database.database;
import com.twc.wms.entity.QDESTYP;
import com.twc.wms.entity.QDMBAG;
import com.twc.wms.entity.QDMBAGHED;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 *
 * @author nutthawoot.noo
 */
public class QDMBAGHEDDao extends database {

    public boolean addHed(String code, String desc, String unit, String uid) {

        boolean result = false;

        if (uid != null) {
            if (uid.equals("null")) {
                uid = "";
            }
        }

        String sql = "INSERT INTO [QDMBAGHED]"
                + " ([QDMBCOM]\n"
                + "      ,[QDMBSTYLE]\n"
                + "      ,[QDMBDESC]\n"
                + "      ,[QDMBUNIT]\n"
                + "      ,[QDMBX]\n"
                + "      ,[QDMBEDT]\n"
                + "      ,[QDMBCDT]\n"
                + "      ,[QDMBUSER])"
                + " VALUES('TWC'"
                + ",'" + code + "'"
                + ",'" + desc + "'"
                + ",'" + unit + "'"
                + ",'0'"
                + ",CURRENT_TIMESTAMP"
                + ",CURRENT_TIMESTAMP"
                + ",'" + uid + "')";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean updateBagHed(String code, String desc, String unit, String equa, String x, String y, String uid) {

        boolean result = false;

        if (uid.equals("null")) {
            uid = "";
        }

        String sql = "UPDATE [QDMBAGHED]\n"
                + "SET [QDMBDESC] = '" + desc + "'\n"
                + "      ,[QDMBUNIT] = '" + unit + "'\n"
                + "      ,[QDMBEQUA] = '" + equa + "'\n"
                + "      ,[QDMBX] = '" + x + "'\n"
                + "      ,[QDMBY] = '" + y + "'\n"
                + "      ,[QDMBEDT] = CURRENT_TIMESTAMP\n"
                + "      ,[QDMBUSER] = '" + uid + "'\n"
                + "WHERE [QDMBSTYLE] = '" + code + "'";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public String checkHed(String style) {
        PreparedStatement ps = null;
        ResultSet rs = null;
        String STRRETURN = null;
        try {
            String check = "SELECT [QDMBSTYLE]\n"
                    + "      ,[QDMBDESC]\n"
                    + "      ,[QDMBUNIT]\n"
                    + "      ,[QDMBEQUA]\n"
                    + "      ,FORMAT([QDMBX],'0.0000') AS [QDMBX]\n"
                    + "      ,FORMAT([QDMBY],'0.0000')  AS [QDMBY]\n"
                    + "  FROM [RMShipment].[dbo].[QDMBAGHED]\n"
                    + "  WHERE [QDMBSTYLE] = '" + style + "'";
            ps = connect.prepareStatement(check);
            rs = ps.executeQuery();
            if (rs.next()) {
                STRRETURN = "f";
            } else {
                STRRETURN = "t";
            }
            connect.close();

        } catch (Exception exp) {
            exp.printStackTrace();
        }
        return STRRETURN;
    }

    public QDMBAGHED findQRMMAS(String id) {

        QDMBAGHED p = new QDMBAGHED();

        String sql = "SELECT LEFT(QRMCODE,8) AS [QRMCODE]\n"
                + "      ,REPLACE(QRMDESC,'#','[sharp]') AS [QRMDESC]\n"
                + "      ,format([QRMQTY],'#0.00') as [QRMQTY]\n"
                + "      ,[QRMBUN]\n"
                + "  FROM [RMShipment].[dbo].[QRMMAS]\n"
                + "  where [QRMID]='" + id + "'";
//        System.out.println(sql); 

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                p.setQDMBSTYLE(result.getString("QRMCODE"));
                p.setQDMBDESC(result.getString("QRMDESC"));
                p.setQDMBUNIT(result.getString("QRMBUN"));
                p.setQTY(result.getString("QRMQTY"));

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return p;

    }

    public QDMBAGHED findByStyle(String style) {

        QDMBAGHED p = new QDMBAGHED();

        String sql = "SELECT [QDMBSTYLE]\n"
                + "      ,[QDMBDESC]\n"
                + "      ,[QDMBUNIT]\n"
                + "      ,[QDMBEQUA]\n"
                + "      ,FORMAT([QDMBX],'0.000') AS [QDMBX]\n"
                + "      ,FORMAT([QDMBY],'0.0000')  AS [QDMBY]\n"
                + "  FROM [RMShipment].[dbo].[QDMBAGHED]\n"
                + "  WHERE [QDMBSTYLE] = '" + style + "'";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                p.setQDMBSTYLE(result.getString("QDMBSTYLE"));
                p.setQDMBDESC(result.getString("QDMBDESC"));
                p.setQDMBUNIT(result.getString("QDMBUNIT"));
                p.setQDMBEQUA(result.getString("QDMBEQUA"));
                p.setQDMBX(result.getString("QDMBX"));
                p.setQDMBY(result.getString("QDMBY"));

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return p;

    }

    public boolean add(String size, String desc, String inWidth, String inLength, String uid) {

        boolean result = false;

        if (uid.equals("null")) {
            uid = "";
        }

        String sql = "INSERT INTO [QDMBAG]"
                + " ([QDMCOM]\n"
                + "      ,[QDMSIZE]\n"
                + "      ,[QDMDESC]\n"
                + "      ,[QDMINWIDTH]\n"
                + "      ,[QDMINLENGTH]\n"
                + "      ,[QDMCMWIDTH]\n"
                + "      ,[QDMCMLENGTH]\n"
                + "      ,[QDMEDT]\n"
                + "      ,[QDMCDT]\n"
                + "      ,[QDMUSER])"
                + " VALUES('TWC'"
                + ",'" + size + "'"
                + ",'" + desc + "'"
                + "," + inWidth
                + "," + inLength
                + "," + inWidth + "*2.54"
                + "," + inLength + "*2.54"
                + ",CURRENT_TIMESTAMP"
                + ",CURRENT_TIMESTAMP"
                + ",'" + uid + "')";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean edit(String size, String desc, String inWidth, String inLength, String uid) {

        boolean result = false;

        if (uid.equals("null")) {
            uid = "";
        }

        String sql = "UPDATE [QDMBAG]\n"
                + "SET [QDMDESC] = '" + desc + "'"
                + "      ,[QDMINWIDTH] = " + inWidth
                + "      ,[QDMINLENGTH] = " + inLength
                + "      ,[QDMCMWIDTH] = " + inWidth + "*2.54"
                + "      ,[QDMCMLENGTH] = " + inLength + "*2.54"
                + "      ,[QDMEDT] = CURRENT_TIMESTAMP\n"
                + "      ,[QDMUSER] = '" + uid + "'"
                + "WHERE QDMSIZE = '" + size + "'";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public void delete(String size) {

        String sql = "DELETE FROM QDMBAG "
                + "WHERE QDMSIZE = '" + size + "'";

        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            ps.executeUpdate();

        } catch (Exception e) {

            e.printStackTrace();

        }

    }
}
