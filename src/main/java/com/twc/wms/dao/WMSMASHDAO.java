/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.dao;

import com.twc.wms.database.database;

import java.sql.*;

import java.util.ArrayList;

import java.util.List;

import com.twc.wms.entity.MSSMATN;
import com.twc.wms.entity.SAPMAS;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import com.twc.wms.database.MSSQLDbConnectionPool;
import com.twc.wms.entity.WMSMASH;

/**
 *
 * @author nutthawoot.noo
 */
public class WMSMASHDAO extends database {

    public String getMaxGroup(String POFG) {

        String is = "";

        String sql = "select case when GRPNO != '' then MAX(GRPNO) else 'Data not found' end as GRPNO "
                + "from WMSMASH where POFG='" + POFG + "' group by GRPNO";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                if (result.getString("GRPNO") != null) {
                    is = result.getString("GRPNO");
                }

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return is;

    }

    public List<WMSMASH> getEmptyGroup(String POFG) {

        List<WMSMASH> UAList = new ArrayList<WMSMASH>();

        String sql = "select * from WMSMASH where POFG='" + POFG + "' and GRPNO=''";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                WMSMASH h = new WMSMASH();
                h.setVBELN(result.getString("VBELN"));
                h.setAUDAT(result.getString("AUDAT"));
                h.setBSTKD(result.getString("BSTKD"));
                h.setPOFG(result.getString("POFG"));
                UAList.add(h);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public String check(WMSMASH h) {
        PreparedStatement ps = null;
        ResultSet rs = null;
        String STRRETURN = null;
        try {
            String check = "SELECT * FROM WMSMASH WHERE VBELN='" + h.getVBELN() + "' AND POFG='" + h.getPOFG() + "'";
            ps = connect.prepareStatement(check);
            rs = ps.executeQuery();
            if (rs.next()) {
                STRRETURN = "f";
            } else {
                STRRETURN = "t";
            }
            connect.close();

        } catch (Exception exp) {
            exp.printStackTrace();
        }
        return STRRETURN;
    }

    public boolean add(WMSMASH h) {

        boolean result = false;

        String sql = "INSERT INTO WMSMASH"
                + " (LINETYP, VBELN, AUDAT, AUART, VTWEG, BWART, KUNNR, NAME1, NAME2, "
                + "ADDR1, ADDR2, BSTKD, SUBMI, MATLOT, TAXNO, BRANCH01, DATUM, STYLE, "
                + "COLOR, LOT, AMTFG, POFG, GRPNO)"
                + " VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ps.setString(1, h.getLINETYP());
            ps.setString(2, h.getVBELN());
            ps.setString(3, h.getAUDAT());
            ps.setString(4, h.getAUART());
            ps.setString(5, h.getVTWEG());
            ps.setString(6, h.getBWART());
            ps.setString(7, h.getKUNNR());
            ps.setString(8, h.getNAME1());
            ps.setString(9, h.getNAME2());
            ps.setString(10, h.getADDR1());
            ps.setString(11, h.getADDR2());
            ps.setString(12, h.getBSTKD());
            ps.setString(13, h.getSUBMI());
            ps.setString(14, h.getMATLOT());
            ps.setString(15, h.getTAXNO());
            ps.setString(16, h.getBRANCH01());
            ps.setString(17, h.getDATUM());
            ps.setString(18, h.getSTYLE());
            ps.setString(19, h.getCOLOR());
            ps.setString(20, h.getLOT());
            ps.setString(21, h.getAMTFG());
            ps.setString(22, h.getPOFG());
            ps.setString(23, h.getGRPNO());

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public MSSMATN findByUid(String id) {

        MSSMATN ua = new MSSMATN();

        String sql = "SELECT * FROM MSSMATN WHERE LGPBE = ?";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ps.setString(1, id);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                ua.setUid(result.getString("LGPBE"));
                ua.setName(result.getString("MATCNAME"));
                ua.setWarehouse(result.getString("MSWHSE"));

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return ua;

    }

    public boolean updateGroupSet(String GRPNO, String POFG, String WHS) {

        boolean result = false;

        String sql = "update WMSMASH set GRPNO='" + GRPNO + "', MWHSE='" + WHS + "' where POFG='" + POFG + "' and GRPNO=''";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public void delete(String uid) {

        String sql = "DELETE FROM MSSMATN WHERE LGPBE = ?";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ps.setString(1, uid);

            ps.executeUpdate();

        } catch (Exception e) {

            e.printStackTrace();

        }

    }
}
