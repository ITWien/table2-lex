/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.dao;

import com.twc.wms.database.database;

import java.sql.*;

import java.util.ArrayList;

import java.util.List;

import com.twc.wms.entity.XMSWIDETAIL;
import com.twc.wms.entity.SAPMAS;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import com.twc.wms.database.MSSQLDbConnectionPool;
import com.twc.wms.entity.XMSWIDETAIL;
import java.text.DecimalFormat;

/**
 *
 * @author nutthawoot.noo
 */
public class MSS210Dao extends database {

    public ResultSet SQLGetData(String POFG, String GRPNO) throws SQLException {

        String sql = "SELECT DISTINCT LINEUP.SWTRUCK, A.* FROM ( SELECT STMT.[GRP/NO], STMT.CUSTOMER_DESC, STMT.[C/NO], SUM(STMT.NW) AS NW, MAX(STMT.GW) AS GW, MAX(STMT.WD) AS WD, MAX(STMT.LEN) AS LEN, MAX(STMT.HG) AS HG FROM ( SELECT MASH.POFG AS [CUSTOMER], MASH.NAME1 AS [CUSTOMER_DESC], QR1.*, LINE.SWDMLWD AS [WD], LINE.SWDMLEN AS [LEN], LINE.SWDMHG AS [HG], LINE.SWDMUNIT AS [UNIT] FROM ( SELECT DISTINCT POFG, GRPNO, NAME1 FROM MSSMASH ) MASH INNER JOIN ( SELECT A.SWGRPST AS [GRP/NO], A.SWCARNO AS [C/NO], SUBSTRING(A.SWITEM, 0, 9) AS [MAT/NO], SUBSTRING(A.SWITEM, 9, 12) AS [COLOR], ARKTX AS [DESC], A.SWLINE AS [LINE], A.SWITUNIT AS [U/M], A.SWQUANTY AS [PICKUP], A.SWNWEIGHT AS [NW], B.GW AS [GW] FROM SWGLINE A LEFT JOIN MSSMASM ON A.SWITEM = MATNR COLLATE Thai_CI_AS LEFT JOIN ( SELECT SWIVNO, SWCARNO, SUM(SWGWEIGHT) AS GW FROM SWGLINE GROUP BY SWIVNO, SWCARNO ) B ON A.SWIVNO = B.SWIVNO AND A.SWCARNO = B.SWCARNO LEFT JOIN SWGHEAD C ON A.SWGRPST = C.SWGRPST AND A.SWCARNO = C.SWCARNO WHERE A.SWGRPST = '" + GRPNO + "' AND A.SWLINE != 0 ) AS QR1 ON QR1.[GRP/NO] = MASH.GRPNO COLLATE Thai_CI_AS AND MASH.POFG = '" + POFG + "' INNER JOIN SWGLINE LINE ON LINE.SWCARNO = QR1.[C/NO] AND LINE.SWLINE = 0 AND LINE.SWIVNO = '" + GRPNO + "' ) STMT GROUP BY STMT.[GRP/NO], STMT.CUSTOMER_DESC, STMT.[C/NO] ) A LEFT JOIN SWGLINEUP LINEUP ON A.[GRP/NO] = LINEUP.SWGRPST AND A.[C/NO] = LINEUP.SWCARNO";
//        System.out.println(sql);

        ResultSet result = null;

        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            result = ps.executeQuery();
        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }
}
