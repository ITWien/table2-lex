/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.dao;

import com.twc.wms.database.database;

import java.sql.*;

import java.util.ArrayList;

import java.util.List;

import com.twc.wms.entity.XMSBRDETAIL;
import com.twc.wms.entity.SAPMAS;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import com.twc.wms.database.MSSQLDbConnectionPool;
import com.twc.wms.entity.XMSBRDETAIL;
import java.text.DecimalFormat;

/**
 *
 * @author nutthawoot.noo
 */
public class XMSBRDETAILDao extends database {

    DecimalFormat formatDou = new DecimalFormat("#,###,###,##0");

    public ResultSet findForPrintXMS600QNO(String qno) throws SQLException {

        String sql = "DECLARE @QNO DECIMAL(18, 0) = '" + qno + "'\n"
                + "SELECT [XMSBRHQNO]\n"
                + "      ,[XMSBRHLICENNO]\n"
                + "	  ,FORMAT([XMSBRHSPDT],'d/M/yyyy') AS [XMSBRHSPDT]\n"
                + "	  ,CASE WHEN [XMSBRHDEST] = 'KPC' THEN '��Թ������'\n"
                + "	   WHEN [XMSBRHDEST] = 'WSC' THEN '��������Ҫ�'\n"
                + "	   WHEN [XMSBRHDEST] = 'WLC' THEN '�����Ӿٹ'\n"
                + "	   WHEN [XMSBRHDEST] = 'MWC' THEN '���¹��������' ELSE [XMSBRHDEST] END AS [XMSBRHDEST]\n"
                + "	  ,[XMSBRHROUND]\n"
                + "	  ,[XMSBRHDRIVER]\n"
                + "	  ,[XMSBRHFOLLOWER]\n"
                + "	  ,[XMSBRHDOCNO]\n"
                + "      ,[XMSBRDLINE]\n"
                + "      ,[XMSBRDDEST] AS DEST\n"
                + "      ,[XMSBRDDESTN] AS DESTN\n"
                + "      ,[XMSBRDDRIVER] AS DRIVER\n"
                + "      ,[XMSBRDFOLLOWER] AS FOLLOWER\n"
                + "      ,[XMSBRDBAG1] AS BAG_WH1\n"
                + "      ,[XMSBRDROLL1] AS ROLL_WH1\n"
                + "      ,[XMSBRDBOX1] AS BOX_WH1\n"
                + "      ,[XMSBRDPCS1] AS PCS_WH1\n"
                + "      ,[XMSBRDTOT1] AS TOT_WH1\n"
                + "      ,[XMSBRDBAG2] AS BAG_WH2\n"
                + "      ,[XMSBRDROLL2] AS ROLL_WH2\n"
                + "      ,[XMSBRDBOX2] AS BOX_WH2\n"
                + "      ,[XMSBRDPCS2] AS PCS_WH2\n"
                + "      ,[XMSBRDTOT2] AS TOT_WH2\n"
                + "      ,[XMSBRDBAG3] AS BAG_WH3\n"
                + "      ,[XMSBRDROLL3] AS ROLL_WH3\n"
                + "      ,[XMSBRDBOX3] AS BOX_WH3\n"
                + "      ,[XMSBRDPCS3] AS PCS_WH3\n"
                + "      ,[XMSBRDTOT3] AS TOT_WH3\n"
                + "      ,[XMSBRDBAG] AS BAG_TOTWH\n"
                + "      ,[XMSBRDROLL] AS ROLL_TOTWH\n"
                + "      ,[XMSBRDBOX] AS BOX_TOTWH\n"
                + "      ,[XMSBRDPCS] AS PCS_TOTWH\n"
                + "      ,[XMSBRDTOT] AS TOT_TOTWH\n"
                + "  FROM [RMShipment].[dbo].[XMSBRDETAIL]\n"
                + "  LEFT JOIN [XMSBRHEAD] ON [XMSBRHEAD].[XMSBRHQNO] = [XMSBRDETAIL].[XMSBRDQNO]\n"
                + "  WHERE [XMSBRDQNO] = @QNO\n"
                + "  ORDER BY XMSBRDLINE";
//        System.out.println(sql);

        ResultSet result = null;

        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            result = ps.executeQuery();
        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public List<XMSBRDETAIL> findByQno(String qno) {

        List<XMSBRDETAIL> UAList = new ArrayList<XMSBRDETAIL>();

        String sql = "SELECT [XMSBRDQNO]\n"
                + "      ,[XMSBRDLINE]\n"
                + "      ,[XMSBRDDEST] AS DEST\n"
                + "      ,[XMSBRDDESTN] AS DESTN\n"
                + "      ,[XMSBRDDRIVER] AS DRIVER\n"
                + "      ,[XMSBRDFOLLOWER] AS FOLLOWER\n"
                + "      ,[XMSBRDBAG1] AS BAG_WH1\n"
                + "      ,[XMSBRDROLL1] AS ROLL_WH1\n"
                + "      ,[XMSBRDBOX1] AS BOX_WH1\n"
                + "      ,[XMSBRDPCS1] AS PCS_WH1\n"
                + "      ,[XMSBRDTOT1] AS TOT_WH1\n"
                + "      ,[XMSBRDBAG2] AS BAG_WH2\n"
                + "      ,[XMSBRDROLL2] AS ROLL_WH2\n"
                + "      ,[XMSBRDBOX2] AS BOX_WH2\n"
                + "      ,[XMSBRDPCS2] AS PCS_WH2\n"
                + "      ,[XMSBRDTOT2] AS TOT_WH2\n"
                + "      ,[XMSBRDBAG3] AS BAG_WH3\n"
                + "      ,[XMSBRDROLL3] AS ROLL_WH3\n"
                + "      ,[XMSBRDBOX3] AS BOX_WH3\n"
                + "      ,[XMSBRDPCS3] AS PCS_WH3\n"
                + "      ,[XMSBRDTOT3] AS TOT_WH3\n"
                + "      ,[XMSBRDBAG] AS BAG_TOTWH\n"
                + "      ,[XMSBRDROLL] AS ROLL_TOTWH\n"
                + "      ,[XMSBRDBOX] AS BOX_TOTWH\n"
                + "      ,[XMSBRDPCS] AS PCS_TOTWH\n"
                + "      ,[XMSBRDTOT] AS TOT_TOTWH\n"
                + "  FROM [RMShipment].[dbo].[XMSBRDETAIL]\n"
                + "  WHERE [XMSBRDQNO] = '" + qno + "'\n"
                + "  ORDER BY XMSBRDLINE";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                XMSBRDETAIL p = new XMSBRDETAIL();

                p.setXMSBRDDEST(result.getString("DEST"));

                if (!result.getString("DESTN").equals("���")) {
                    if (!result.getString("DESTN").equals("��ż�ҹ")) {
                        if (!result.getString("DESTN").equals("����")) {
                            String destn = result.getString("DESTN");
                            if (result.getString("DESTN").equals("WKC")) {
                                destn = "���顺Թ���";
                            } else if (result.getString("DESTN").equals("PKC")) {
                                destn = "�ѷ�ҡ�Թ���";
                            } else if (result.getString("DESTN").equals("WSC")) {
                                destn = "��������Ҫ�";
                            } else if (result.getString("DESTN").equals("WLC")) {
                                destn = "�����Ӿٹ";
                            } else if (result.getString("DESTN").equals("MWC")) {
                                destn = "���¹��������";
                            }
                            p.setXMSBRDDESTN("<b>��� " + destn + "</b>");
                            p.setBorderTop("border-top-color: #000;");
                        } else {
                            p.setXMSBRDDESTN(result.getString("DESTN"));
                        }
                    } else {
                        p.setXMSBRDDESTN(result.getString("DESTN"));
                    }
                } else {
                    p.setXMSBRDDESTN(result.getString("DESTN"));
                }

                p.setXMSBRDDRIVER(result.getString("DRIVER"));
                p.setXMSBRDFOLLOWER(result.getString("FOLLOWER"));
                p.setXMSBRDBAG1(((result.getString("BAG_WH1") == null) || result.getString("BAG_WH1").trim().equals("0")) ? "-" : formatDou.format(Double.parseDouble(result.getString("BAG_WH1"))));
                p.setXMSBRDROLL1(((result.getString("ROLL_WH1") == null) || result.getString("ROLL_WH1").trim().equals("0")) ? "-" : formatDou.format(Double.parseDouble(result.getString("ROLL_WH1"))));
                p.setXMSBRDBOX1(((result.getString("BOX_WH1") == null) || result.getString("BOX_WH1").trim().equals("0")) ? "-" : formatDou.format(Double.parseDouble(result.getString("BOX_WH1"))));
                p.setXMSBRDPCS1(((result.getString("PCS_WH1") == null) || result.getString("PCS_WH1").trim().equals("0")) ? "-" : formatDou.format(Double.parseDouble(result.getString("PCS_WH1"))));
                p.setXMSBRDTOT1(((result.getString("TOT_WH1") == null) || result.getString("TOT_WH1").trim().equals("0")) ? "0" : result.getString("TOT_WH1"));
                p.setXMSBRDBAG2(((result.getString("BAG_WH2") == null) || result.getString("BAG_WH2").trim().equals("0")) ? "-" : formatDou.format(Double.parseDouble(result.getString("BAG_WH2"))));
                p.setXMSBRDROLL2(((result.getString("ROLL_WH2") == null) || result.getString("ROLL_WH2").trim().equals("0")) ? "-" : formatDou.format(Double.parseDouble(result.getString("ROLL_WH2"))));
                p.setXMSBRDBOX2(((result.getString("BOX_WH2") == null) || result.getString("BOX_WH2").trim().equals("0")) ? "-" : formatDou.format(Double.parseDouble(result.getString("BOX_WH2"))));
                p.setXMSBRDPCS2(((result.getString("PCS_WH2") == null) || result.getString("PCS_WH2").trim().equals("0")) ? "-" : formatDou.format(Double.parseDouble(result.getString("PCS_WH2"))));
                p.setXMSBRDTOT2(((result.getString("TOT_WH2") == null) || result.getString("TOT_WH2").trim().equals("0")) ? "0" : result.getString("TOT_WH2"));
                p.setXMSBRDBAG3(((result.getString("BAG_WH3") == null) || result.getString("BAG_WH3").trim().equals("0")) ? "-" : formatDou.format(Double.parseDouble(result.getString("BAG_WH3"))));
                p.setXMSBRDROLL3(((result.getString("ROLL_WH3") == null) || result.getString("ROLL_WH3").trim().equals("0")) ? "-" : formatDou.format(Double.parseDouble(result.getString("ROLL_WH3"))));
                p.setXMSBRDBOX3(((result.getString("BOX_WH3") == null) || result.getString("BOX_WH3").trim().equals("0")) ? "-" : formatDou.format(Double.parseDouble(result.getString("BOX_WH3"))));
                p.setXMSBRDPCS3(((result.getString("PCS_WH3") == null) || result.getString("PCS_WH3").trim().equals("0")) ? "-" : formatDou.format(Double.parseDouble(result.getString("PCS_WH3"))));
                p.setXMSBRDTOT3(((result.getString("TOT_WH3") == null) || result.getString("TOT_WH3").trim().equals("0")) ? "0" : result.getString("TOT_WH3"));
                p.setXMSBRDBAG(((result.getString("BAG_TOTWH") == null) || result.getString("BAG_TOTWH").trim().equals("0")) ? "-" : formatDou.format(Double.parseDouble(result.getString("BAG_TOTWH"))));
                p.setXMSBRDROLL(((result.getString("ROLL_TOTWH") == null) || result.getString("ROLL_TOTWH").trim().equals("0")) ? "-" : formatDou.format(Double.parseDouble(result.getString("ROLL_TOTWH"))));
                p.setXMSBRDBOX(((result.getString("BOX_TOTWH") == null) || result.getString("BOX_TOTWH").trim().equals("0")) ? "-" : formatDou.format(Double.parseDouble(result.getString("BOX_TOTWH"))));
                p.setXMSBRDPCS(((result.getString("PCS_TOTWH") == null) || result.getString("PCS_TOTWH").trim().equals("0")) ? "-" : formatDou.format(Double.parseDouble(result.getString("PCS_TOTWH"))));
                p.setXMSBRDTOT(((result.getString("TOT_TOTWH") == null) || result.getString("TOT_TOTWH").trim().equals("0")) ? "0" : result.getString("TOT_TOTWH"));

                UAList.add(p);

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public List<XMSBRDETAIL> findFromOT(String lino, String shipDate, String dest, String dest2, String round) {

        List<XMSBRDETAIL> UAList = new ArrayList<XMSBRDETAIL>();

        String sql = "DECLARE @LINO NVARCHAR(50) = '" + lino + "'\n"
                + "DECLARE @DEST NVARCHAR(50) = '" + dest + "'\n"
                + "DECLARE @DEST2 NVARCHAR(50) = '" + dest2 + "'\n"
                + "DECLARE @SPDT NVARCHAR(50) = '" + shipDate + "'\n"
                + "DECLARE @ROUND NVARCHAR(50) = '" + round + "'\n"
                + "DECLARE @DRIVER NVARCHAR(50) = ''\n"
                + "DECLARE @FOLLOWER NVARCHAR(50) = ''\n"
                + "SET @DRIVER = (SELECT TOP 1 TDRIVER FROM (SELECT [XMSOTHWHS] AS TWHS\n"
                + "      ,[XMSOTHSPDT] AS TSPDT\n"
                + "      ,[XMSOTHDEST] AS TDEST\n"
                + "      ,[XMSOTHROUND] AS TROUND\n"
                + "      ,[XMSOTHTYPE] AS TTYPE\n"
                + "      ,[XMSOTHLICENNO] AS TLINO\n"
                + "      ,[XMSOTHDRIVER] AS TDRIVER\n"
                + "      ,[XMSOTHFOLLOWER] AS TFOLLOWER\n"
                + "      ,[XMSOTHBAG] AS TBAG\n"
                + "      ,[XMSOTHROLL] AS TROLL\n"
                + "      ,[XMSOTHBOX] AS TBOX\n"
                + "      ,[XMSOTHPCS] AS TPCS\n"
                + "  FROM [RMShipment].[dbo].[XMSOTHEAD]\n"
                + "  WHERE XMSOTHLICENNO = @LINO\n"
                + "  AND (XMSOTHDEST = @DEST OR XMSOTHDEST = @DEST2)\n"
                + "  AND FORMAT(XMSOTHSPDT,'yyyy-MM-dd') = @SPDT\n"
                + "  AND XMSOTHROUND = @ROUND)TMP)\n"
                + "SET @FOLLOWER = (SELECT TOP 1 TFOLLOWER FROM (SELECT [XMSOTHWHS] AS TWHS\n"
                + "      ,[XMSOTHSPDT] AS TSPDT\n"
                + "      ,[XMSOTHDEST] AS TDEST\n"
                + "      ,[XMSOTHROUND] AS TROUND\n"
                + "      ,[XMSOTHTYPE] AS TTYPE\n"
                + "      ,[XMSOTHLICENNO] AS TLINO\n"
                + "      ,[XMSOTHDRIVER] AS TDRIVER\n"
                + "      ,[XMSOTHFOLLOWER] AS TFOLLOWER\n"
                + "      ,[XMSOTHBAG] AS TBAG\n"
                + "      ,[XMSOTHROLL] AS TROLL\n"
                + "      ,[XMSOTHBOX] AS TBOX\n"
                + "      ,[XMSOTHPCS] AS TPCS\n"
                + "  FROM [RMShipment].[dbo].[XMSOTHEAD]\n"
                + "  WHERE XMSOTHLICENNO = @LINO\n"
                + "  AND (XMSOTHDEST = @DEST OR XMSOTHDEST = @DEST2)\n"
                + "  AND FORMAT(XMSOTHSPDT,'yyyy-MM-dd') = @SPDT\n"
                + "  AND XMSOTHROUND = @ROUND)TMP)\n"
                + "\n"
                + "SELECT @DEST AS DEST, (SELECT TOP 1 [QDEDESC] FROM [QDEST] WHERE [QDECOD] = @DEST) AS DESTN\n"
                + ",@DRIVER AS DRIVER, @FOLLOWER AS FOLLOWER\n"
                + ",SUM(CASE WHEN TWHS = 'WH1' THEN TBAG ELSE 0 END) AS BAG_WH1\n"
                + ",SUM(CASE WHEN TWHS = 'WH1' THEN TROLL ELSE 0 END) AS ROLL_WH1\n"
                + ",SUM(CASE WHEN TWHS = 'WH1' THEN TBOX ELSE 0 END) AS BOX_WH1\n"
                + ",SUM(CASE WHEN TWHS = 'WH1' THEN TPCS ELSE 0 END) AS PCS_WH1\n"
                + ",SUM(CASE WHEN TWHS = 'WH1' THEN (TBAG+TROLL+TBOX+TPCS) ELSE 0 END) AS TOT_WH1\n"
                + ",SUM(CASE WHEN TWHS = 'WH2' THEN TBAG ELSE 0 END) AS BAG_WH2\n"
                + ",SUM(CASE WHEN TWHS = 'WH2' THEN TROLL ELSE 0 END) AS ROLL_WH2\n"
                + ",SUM(CASE WHEN TWHS = 'WH2' THEN TBOX ELSE 0 END) AS BOX_WH2\n"
                + ",SUM(CASE WHEN TWHS = 'WH2' THEN TPCS ELSE 0 END) AS PCS_WH2\n"
                + ",SUM(CASE WHEN TWHS = 'WH2' THEN (TBAG+TROLL+TBOX+TPCS) ELSE 0 END) AS TOT_WH2\n"
                + ",SUM(CASE WHEN TWHS = 'WH3' THEN TBAG ELSE 0 END) AS BAG_WH3\n"
                + ",SUM(CASE WHEN TWHS = 'WH3' THEN TROLL ELSE 0 END) AS ROLL_WH3\n"
                + ",SUM(CASE WHEN TWHS = 'WH3' THEN TBOX ELSE 0 END) AS BOX_WH3\n"
                + ",SUM(CASE WHEN TWHS = 'WH3' THEN TPCS ELSE 0 END) AS PCS_WH3\n"
                + ",SUM(CASE WHEN TWHS = 'WH3' THEN (TBAG+TROLL+TBOX+TPCS) ELSE 0 END) AS TOT_WH3\n"
                + ",SUM(TBAG) AS BAG_TOTWH\n"
                + ",SUM(TROLL) AS ROLL_TOTWH\n"
                + ",SUM(TBOX) AS BOX_TOTWH\n"
                + ",SUM(TPCS) AS PCS_TOTWH\n"
                + ",SUM(TBAG+TROLL+TBOX+TPCS) AS TOT_TOTWH\n"
                + "FROM (SELECT [XMSOTHWHS] AS TWHS\n"
                + "      ,[XMSOTHSPDT] AS TSPDT\n"
                + "      ,[XMSOTHDEST] AS TDEST\n"
                + "      ,[XMSOTHROUND] AS TROUND\n"
                + "      ,[XMSOTHTYPE] AS TTYPE\n"
                + "      ,[XMSOTHLICENNO] AS TLINO\n"
                + "      ,[XMSOTHDRIVER] AS TDRIVER\n"
                + "      ,[XMSOTHFOLLOWER] AS TFOLLOWER\n"
                + "      ,[XMSOTHBAG] AS TBAG\n"
                + "      ,[XMSOTHROLL] AS TROLL\n"
                + "      ,[XMSOTHBOX] AS TBOX\n"
                + "      ,[XMSOTHPCS] AS TPCS\n"
                + "  FROM [RMShipment].[dbo].[XMSOTHEAD]\n"
                + "  WHERE XMSOTHLICENNO = @LINO\n"
                + "  AND (XMSOTHDEST = @DEST OR XMSOTHDEST = @DEST2)\n"
                + "  AND FORMAT(XMSOTHSPDT,'yyyy-MM-dd') = @SPDT\n"
                + "  AND XMSOTHROUND = @ROUND)TMP\n"
                + "UNION ALL\n"
                + "SELECT @DEST AS DEST, '���' AS DESTN\n"
                + ",@DRIVER AS DRIVER, @FOLLOWER AS FOLLOWER\n"
                + ",SUM(CASE WHEN TWHS = 'WH1' THEN TBAG ELSE 0 END) AS BAG_WH1\n"
                + ",SUM(CASE WHEN TWHS = 'WH1' THEN TROLL ELSE 0 END) AS ROLL_WH1\n"
                + ",SUM(CASE WHEN TWHS = 'WH1' THEN TBOX ELSE 0 END) AS BOX_WH1\n"
                + ",SUM(CASE WHEN TWHS = 'WH1' THEN TPCS ELSE 0 END) AS PCS_WH1\n"
                + ",SUM(CASE WHEN TWHS = 'WH1' THEN (TBAG+TROLL+TBOX+TPCS) ELSE 0 END) AS TOT_WH1\n"
                + ",SUM(CASE WHEN TWHS = 'WH2' THEN TBAG ELSE 0 END) AS BAG_WH2\n"
                + ",SUM(CASE WHEN TWHS = 'WH2' THEN TROLL ELSE 0 END) AS ROLL_WH2\n"
                + ",SUM(CASE WHEN TWHS = 'WH2' THEN TBOX ELSE 0 END) AS BOX_WH2\n"
                + ",SUM(CASE WHEN TWHS = 'WH2' THEN TPCS ELSE 0 END) AS PCS_WH2\n"
                + ",SUM(CASE WHEN TWHS = 'WH2' THEN (TBAG+TROLL+TBOX+TPCS) ELSE 0 END) AS TOT_WH2\n"
                + ",SUM(CASE WHEN TWHS = 'WH3' THEN TBAG ELSE 0 END) AS BAG_WH3\n"
                + ",SUM(CASE WHEN TWHS = 'WH3' THEN TROLL ELSE 0 END) AS ROLL_WH3\n"
                + ",SUM(CASE WHEN TWHS = 'WH3' THEN TBOX ELSE 0 END) AS BOX_WH3\n"
                + ",SUM(CASE WHEN TWHS = 'WH3' THEN TPCS ELSE 0 END) AS PCS_WH3\n"
                + ",SUM(CASE WHEN TWHS = 'WH3' THEN (TBAG+TROLL+TBOX+TPCS) ELSE 0 END) AS TOT_WH3\n"
                + ",SUM(TBAG) AS BAG_TOTWH\n"
                + ",SUM(TROLL) AS ROLL_TOTWH\n"
                + ",SUM(TBOX) AS BOX_TOTWH\n"
                + ",SUM(TPCS) AS PCS_TOTWH\n"
                + ",SUM(TBAG+TROLL+TBOX+TPCS) AS TOT_TOTWH\n"
                + "FROM (SELECT [XMSOTHWHS] AS TWHS\n"
                + "      ,[XMSOTHSPDT] AS TSPDT\n"
                + "      ,[XMSOTHDEST] AS TDEST\n"
                + "      ,[XMSOTHROUND] AS TROUND\n"
                + "      ,[XMSOTHTYPE] AS TTYPE\n"
                + "      ,[XMSOTHLICENNO] AS TLINO\n"
                + "      ,[XMSOTHDRIVER] AS TDRIVER\n"
                + "      ,[XMSOTHFOLLOWER] AS TFOLLOWER\n"
                + "      ,[XMSOTHBAG] AS TBAG\n"
                + "      ,[XMSOTHROLL] AS TROLL\n"
                + "      ,[XMSOTHBOX] AS TBOX\n"
                + "      ,[XMSOTHPCS] AS TPCS\n"
                + "  FROM [RMShipment].[dbo].[XMSOTHEAD]\n"
                + "  WHERE XMSOTHLICENNO = @LINO\n"
                + "  AND (XMSOTHDEST = @DEST OR XMSOTHDEST = @DEST2)\n"
                + "  AND FORMAT(XMSOTHSPDT,'yyyy-MM-dd') = @SPDT\n"
                + "  AND XMSOTHROUND = @ROUND)TMP WHERE TTYPE = '�ѵ�شԺ'\n"
                + "UNION ALL\n"
                + "SELECT @DEST AS DEST, '��ż�ҹ' AS DESTN\n"
                + ",@DRIVER AS DRIVER, @FOLLOWER AS FOLLOWER\n"
                + ",SUM(CASE WHEN TWHS = 'WH1' THEN TBAG ELSE 0 END) AS BAG_WH1\n"
                + ",SUM(CASE WHEN TWHS = 'WH1' THEN TROLL ELSE 0 END) AS ROLL_WH1\n"
                + ",SUM(CASE WHEN TWHS = 'WH1' THEN TBOX ELSE 0 END) AS BOX_WH1\n"
                + ",SUM(CASE WHEN TWHS = 'WH1' THEN TPCS ELSE 0 END) AS PCS_WH1\n"
                + ",SUM(CASE WHEN TWHS = 'WH1' THEN (TBAG+TROLL+TBOX+TPCS) ELSE 0 END) AS TOT_WH1\n"
                + ",SUM(CASE WHEN TWHS = 'WH2' THEN TBAG ELSE 0 END) AS BAG_WH2\n"
                + ",SUM(CASE WHEN TWHS = 'WH2' THEN TROLL ELSE 0 END) AS ROLL_WH2\n"
                + ",SUM(CASE WHEN TWHS = 'WH2' THEN TBOX ELSE 0 END) AS BOX_WH2\n"
                + ",SUM(CASE WHEN TWHS = 'WH2' THEN TPCS ELSE 0 END) AS PCS_WH2\n"
                + ",SUM(CASE WHEN TWHS = 'WH2' THEN (TBAG+TROLL+TBOX+TPCS) ELSE 0 END) AS TOT_WH2\n"
                + ",SUM(CASE WHEN TWHS = 'WH3' THEN TBAG ELSE 0 END) AS BAG_WH3\n"
                + ",SUM(CASE WHEN TWHS = 'WH3' THEN TROLL ELSE 0 END) AS ROLL_WH3\n"
                + ",SUM(CASE WHEN TWHS = 'WH3' THEN TBOX ELSE 0 END) AS BOX_WH3\n"
                + ",SUM(CASE WHEN TWHS = 'WH3' THEN TPCS ELSE 0 END) AS PCS_WH3\n"
                + ",SUM(CASE WHEN TWHS = 'WH3' THEN (TBAG+TROLL+TBOX+TPCS) ELSE 0 END) AS TOT_WH3\n"
                + ",SUM(TBAG) AS BAG_TOTWH\n"
                + ",SUM(TROLL) AS ROLL_TOTWH\n"
                + ",SUM(TBOX) AS BOX_TOTWH\n"
                + ",SUM(TPCS) AS PCS_TOTWH\n"
                + ",SUM(TBAG+TROLL+TBOX+TPCS) AS TOT_TOTWH\n"
                + "FROM (SELECT [XMSOTHWHS] AS TWHS\n"
                + "      ,[XMSOTHSPDT] AS TSPDT\n"
                + "      ,[XMSOTHDEST] AS TDEST\n"
                + "      ,[XMSOTHROUND] AS TROUND\n"
                + "      ,[XMSOTHTYPE] AS TTYPE\n"
                + "      ,[XMSOTHLICENNO] AS TLINO\n"
                + "      ,[XMSOTHDRIVER] AS TDRIVER\n"
                + "      ,[XMSOTHFOLLOWER] AS TFOLLOWER\n"
                + "      ,[XMSOTHBAG] AS TBAG\n"
                + "      ,[XMSOTHROLL] AS TROLL\n"
                + "      ,[XMSOTHBOX] AS TBOX\n"
                + "      ,[XMSOTHPCS] AS TPCS\n"
                + "  FROM [RMShipment].[dbo].[XMSOTHEAD]\n"
                + "  WHERE XMSOTHLICENNO = @LINO\n"
                + "  AND (XMSOTHDEST = @DEST OR XMSOTHDEST = @DEST2)\n"
                + "  AND FORMAT(XMSOTHSPDT,'yyyy-MM-dd') = @SPDT\n"
                + "  AND XMSOTHROUND = @ROUND)TMP WHERE TTYPE = '��ż�ҹ'\n"
                + "UNION ALL\n"
                + "SELECT @DEST AS DEST, '����' AS DESTN\n"
                + ",@DRIVER AS DRIVER, @FOLLOWER AS FOLLOWER\n"
                + ",SUM(CASE WHEN TWHS = 'WH1' THEN TBAG ELSE 0 END) AS BAG_WH1\n"
                + ",SUM(CASE WHEN TWHS = 'WH1' THEN TROLL ELSE 0 END) AS ROLL_WH1\n"
                + ",SUM(CASE WHEN TWHS = 'WH1' THEN TBOX ELSE 0 END) AS BOX_WH1\n"
                + ",SUM(CASE WHEN TWHS = 'WH1' THEN TPCS ELSE 0 END) AS PCS_WH1\n"
                + ",SUM(CASE WHEN TWHS = 'WH1' THEN (TBAG+TROLL+TBOX+TPCS) ELSE 0 END) AS TOT_WH1\n"
                + ",SUM(CASE WHEN TWHS = 'WH2' THEN TBAG ELSE 0 END) AS BAG_WH2\n"
                + ",SUM(CASE WHEN TWHS = 'WH2' THEN TROLL ELSE 0 END) AS ROLL_WH2\n"
                + ",SUM(CASE WHEN TWHS = 'WH2' THEN TBOX ELSE 0 END) AS BOX_WH2\n"
                + ",SUM(CASE WHEN TWHS = 'WH2' THEN TPCS ELSE 0 END) AS PCS_WH2\n"
                + ",SUM(CASE WHEN TWHS = 'WH2' THEN (TBAG+TROLL+TBOX+TPCS) ELSE 0 END) AS TOT_WH2\n"
                + ",SUM(CASE WHEN TWHS = 'WH3' THEN TBAG ELSE 0 END) AS BAG_WH3\n"
                + ",SUM(CASE WHEN TWHS = 'WH3' THEN TROLL ELSE 0 END) AS ROLL_WH3\n"
                + ",SUM(CASE WHEN TWHS = 'WH3' THEN TBOX ELSE 0 END) AS BOX_WH3\n"
                + ",SUM(CASE WHEN TWHS = 'WH3' THEN TPCS ELSE 0 END) AS PCS_WH3\n"
                + ",SUM(CASE WHEN TWHS = 'WH3' THEN (TBAG+TROLL+TBOX+TPCS) ELSE 0 END) AS TOT_WH3\n"
                + ",SUM(TBAG) AS BAG_TOTWH\n"
                + ",SUM(TROLL) AS ROLL_TOTWH\n"
                + ",SUM(TBOX) AS BOX_TOTWH\n"
                + ",SUM(TPCS) AS PCS_TOTWH\n"
                + ",SUM(TBAG+TROLL+TBOX+TPCS) AS TOT_TOTWH\n"
                + "FROM (SELECT [XMSOTHWHS] AS TWHS\n"
                + "      ,[XMSOTHSPDT] AS TSPDT\n"
                + "      ,[XMSOTHDEST] AS TDEST\n"
                + "      ,[XMSOTHROUND] AS TROUND\n"
                + "      ,[XMSOTHTYPE] AS TTYPE\n"
                + "      ,[XMSOTHLICENNO] AS TLINO\n"
                + "      ,[XMSOTHDRIVER] AS TDRIVER\n"
                + "      ,[XMSOTHFOLLOWER] AS TFOLLOWER\n"
                + "      ,[XMSOTHBAG] AS TBAG\n"
                + "      ,[XMSOTHROLL] AS TROLL\n"
                + "      ,[XMSOTHBOX] AS TBOX\n"
                + "      ,[XMSOTHPCS] AS TPCS\n"
                + "  FROM [RMShipment].[dbo].[XMSOTHEAD]\n"
                + "  WHERE XMSOTHLICENNO = @LINO\n"
                + "  AND (XMSOTHDEST = @DEST OR XMSOTHDEST = @DEST2)\n"
                + "  AND FORMAT(XMSOTHSPDT,'yyyy-MM-dd') = @SPDT\n"
                + "  AND XMSOTHROUND = @ROUND)TMP WHERE TTYPE = '����'";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                XMSBRDETAIL p = new XMSBRDETAIL();

                p.setXMSBRDDEST(result.getString("DEST"));

                if (!result.getString("DESTN").equals("���")) {
                    if (!result.getString("DESTN").equals("��ż�ҹ")) {
                        if (!result.getString("DESTN").equals("����")) {
                            String destn = result.getString("DESTN");
                            if (result.getString("DESTN").equals("WKC")) {
                                destn = "���顺Թ���";
                            } else if (result.getString("DESTN").equals("PKC")) {
                                destn = "�ѷ�ҡ�Թ���";
                            } else if (result.getString("DESTN").equals("WSC")) {
                                destn = "��������Ҫ�";
                            } else if (result.getString("DESTN").equals("WLC")) {
                                destn = "�����Ӿٹ";
                            } else if (result.getString("DESTN").equals("MWC")) {
                                destn = "���¹��������";
                            }
                            p.setXMSBRDDESTN("<b>��� " + destn + "</b>");
                            p.setBorderTop("border-top-color: #000;");
                        } else {
                            p.setXMSBRDDESTN(result.getString("DESTN"));
                        }
                    } else {
                        p.setXMSBRDDESTN(result.getString("DESTN"));
                    }
                } else {
                    p.setXMSBRDDESTN(result.getString("DESTN"));
                }

                p.setXMSBRDDRIVER(result.getString("DRIVER"));
                p.setXMSBRDFOLLOWER(result.getString("FOLLOWER"));
                p.setXMSBRDBAG1(((result.getString("BAG_WH1") == null) || result.getString("BAG_WH1").trim().equals("0")) ? "-" : formatDou.format(Double.parseDouble(result.getString("BAG_WH1"))));
                p.setXMSBRDROLL1(((result.getString("ROLL_WH1") == null) || result.getString("ROLL_WH1").trim().equals("0")) ? "-" : formatDou.format(Double.parseDouble(result.getString("ROLL_WH1"))));
                p.setXMSBRDBOX1(((result.getString("BOX_WH1") == null) || result.getString("BOX_WH1").trim().equals("0")) ? "-" : formatDou.format(Double.parseDouble(result.getString("BOX_WH1"))));
                p.setXMSBRDPCS1(((result.getString("PCS_WH1") == null) || result.getString("PCS_WH1").trim().equals("0")) ? "-" : formatDou.format(Double.parseDouble(result.getString("PCS_WH1"))));
                p.setXMSBRDTOT1(((result.getString("TOT_WH1") == null) || result.getString("TOT_WH1").trim().equals("0")) ? "0" : result.getString("TOT_WH1"));
                p.setXMSBRDBAG2(((result.getString("BAG_WH2") == null) || result.getString("BAG_WH2").trim().equals("0")) ? "-" : formatDou.format(Double.parseDouble(result.getString("BAG_WH2"))));
                p.setXMSBRDROLL2(((result.getString("ROLL_WH2") == null) || result.getString("ROLL_WH2").trim().equals("0")) ? "-" : formatDou.format(Double.parseDouble(result.getString("ROLL_WH2"))));
                p.setXMSBRDBOX2(((result.getString("BOX_WH2") == null) || result.getString("BOX_WH2").trim().equals("0")) ? "-" : formatDou.format(Double.parseDouble(result.getString("BOX_WH2"))));
                p.setXMSBRDPCS2(((result.getString("PCS_WH2") == null) || result.getString("PCS_WH2").trim().equals("0")) ? "-" : formatDou.format(Double.parseDouble(result.getString("PCS_WH2"))));
                p.setXMSBRDTOT2(((result.getString("TOT_WH2") == null) || result.getString("TOT_WH2").trim().equals("0")) ? "0" : result.getString("TOT_WH2"));
                p.setXMSBRDBAG3(((result.getString("BAG_WH3") == null) || result.getString("BAG_WH3").trim().equals("0")) ? "-" : formatDou.format(Double.parseDouble(result.getString("BAG_WH3"))));
                p.setXMSBRDROLL3(((result.getString("ROLL_WH3") == null) || result.getString("ROLL_WH3").trim().equals("0")) ? "-" : formatDou.format(Double.parseDouble(result.getString("ROLL_WH3"))));
                p.setXMSBRDBOX3(((result.getString("BOX_WH3") == null) || result.getString("BOX_WH3").trim().equals("0")) ? "-" : formatDou.format(Double.parseDouble(result.getString("BOX_WH3"))));
                p.setXMSBRDPCS3(((result.getString("PCS_WH3") == null) || result.getString("PCS_WH3").trim().equals("0")) ? "-" : formatDou.format(Double.parseDouble(result.getString("PCS_WH3"))));
                p.setXMSBRDTOT3(((result.getString("TOT_WH3") == null) || result.getString("TOT_WH3").trim().equals("0")) ? "0" : result.getString("TOT_WH3"));
                p.setXMSBRDBAG(((result.getString("BAG_TOTWH") == null) || result.getString("BAG_TOTWH").trim().equals("0")) ? "-" : formatDou.format(Double.parseDouble(result.getString("BAG_TOTWH"))));
                p.setXMSBRDROLL(((result.getString("ROLL_TOTWH") == null) || result.getString("ROLL_TOTWH").trim().equals("0")) ? "-" : formatDou.format(Double.parseDouble(result.getString("ROLL_TOTWH"))));
                p.setXMSBRDBOX(((result.getString("BOX_TOTWH") == null) || result.getString("BOX_TOTWH").trim().equals("0")) ? "-" : formatDou.format(Double.parseDouble(result.getString("BOX_TOTWH"))));
                p.setXMSBRDPCS(((result.getString("PCS_TOTWH") == null) || result.getString("PCS_TOTWH").trim().equals("0")) ? "-" : formatDou.format(Double.parseDouble(result.getString("PCS_TOTWH"))));
                p.setXMSBRDTOT(((result.getString("TOT_TOTWH") == null) || result.getString("TOT_TOTWH").trim().equals("0")) ? "0" : result.getString("TOT_TOTWH"));

                UAList.add(p);

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public boolean add(XMSBRDETAIL detail) {

        boolean result = false;

        String sql = "INSERT INTO [XMSBRDETAIL] "
                + "([XMSBRDCOM]\n"
                + "      ,[XMSBRDQNO]\n"
                + "      ,[XMSBRDLINE]\n"
                + "      ,[XMSBRDDEST]\n"
                + "      ,[XMSBRDDESTN]\n"
                + "      ,[XMSBRDDRIVER]\n"
                + "      ,[XMSBRDFOLLOWER]\n"
                + "      ,[XMSBRDBAG1]\n"
                + "      ,[XMSBRDROLL1]\n"
                + "      ,[XMSBRDBOX1]\n"
                + "      ,[XMSBRDPCS1]\n"
                + "      ,[XMSBRDTOT1]\n"
                + "      ,[XMSBRDBAG2]\n"
                + "      ,[XMSBRDROLL2]\n"
                + "      ,[XMSBRDBOX2]\n"
                + "      ,[XMSBRDPCS2]\n"
                + "      ,[XMSBRDTOT2]\n"
                + "      ,[XMSBRDBAG3]\n"
                + "      ,[XMSBRDROLL3]\n"
                + "      ,[XMSBRDBOX3]\n"
                + "      ,[XMSBRDPCS3]\n"
                + "      ,[XMSBRDTOT3]\n"
                + "      ,[XMSBRDBAG]\n"
                + "      ,[XMSBRDROLL]\n"
                + "      ,[XMSBRDBOX]\n"
                + "      ,[XMSBRDPCS]\n"
                + "      ,[XMSBRDTOT]\n"
                + "      ,[XMSBRDEDT]\n"
                + "      ,[XMSBRDCDT]\n"
                + "      ,[XMSBRDUSER]) "
                + "VALUES('TWC'"
                + ",'" + detail.getXMSBRDQNO() + "'"
                + ",'" + detail.getXMSBRDLINE() + "'"
                + ",'" + detail.getXMSBRDDEST() + "'"
                + ",'" + detail.getXMSBRDDESTN() + "'"
                + ",'" + detail.getXMSBRDDRIVER() + "'"
                + ",'" + detail.getXMSBRDFOLLOWER() + "'"
                + ",'" + detail.getXMSBRDBAG1() + "'"
                + ",'" + detail.getXMSBRDROLL1() + "'"
                + ",'" + detail.getXMSBRDBOX1() + "'"
                + ",'" + detail.getXMSBRDPCS1() + "'"
                + ",'" + detail.getXMSBRDTOT1() + "'"
                + ",'" + detail.getXMSBRDBAG2() + "'"
                + ",'" + detail.getXMSBRDROLL2() + "'"
                + ",'" + detail.getXMSBRDBOX2() + "'"
                + ",'" + detail.getXMSBRDPCS2() + "'"
                + ",'" + detail.getXMSBRDTOT2() + "'"
                + ",'" + detail.getXMSBRDBAG3() + "'"
                + ",'" + detail.getXMSBRDROLL3() + "'"
                + ",'" + detail.getXMSBRDBOX3() + "'"
                + ",'" + detail.getXMSBRDPCS3() + "'"
                + ",'" + detail.getXMSBRDTOT3() + "'"
                + ",'" + detail.getXMSBRDBAG() + "'"
                + ",'" + detail.getXMSBRDROLL() + "'"
                + ",'" + detail.getXMSBRDBOX() + "'"
                + ",'" + detail.getXMSBRDPCS() + "'"
                + ",'" + detail.getXMSBRDTOT() + "'"
                + ",CURRENT_TIMESTAMP"
                + ",CURRENT_TIMESTAMP"
                + ",'" + detail.getXMSBRDUSER() + "') ";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public void delete(String qno) {

        String sql = "DELETE FROM XMSBRDETAIL WHERE XMSBRDQNO = '" + qno + "'";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ps.executeUpdate();

        } catch (Exception e) {

            e.printStackTrace();

        }

    }
}
