/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.dao;

import com.twc.wms.database.MSSQLDbConnectionPool;
import com.twc.wms.database.database;
import java.sql.*;

import java.util.ArrayList;

import java.util.List;

import com.twc.wms.entity.PGMU;
import com.twc.wms.entity.WMS960;

import java.util.Calendar;

/**
 *
 * @author nutthawoot.noo
 */
public class QUSMTCTRLDao extends database {

    public List<WMS960> findAll960Print(String USERID, String MATCT, String PLANT) {

        List<WMS960> UAList = new ArrayList<WMS960>();

        String sql = "SELECT '" + USERID + "' AS QUSUSERID, (SELECT TOP 1 USERS FROM MSSUSER WHERE USERID = '" + USERID + "') AS USERS\n"
                + ",MATCNAME\n"
                + ",*\n"
                + "FROM(\n"
                + "SELECT QRMWHSE AS QWHCOD, QWHNAME AS QWHNAME, QRMPLANT AS PLANT, MATCTRL AS MATCT, QRMCODE AS MATCO, SAPDESC AS MATDESC\n"
                + ",FORMAT(SUM(CASE WHEN QRMSTS IN ('1','2','3') THEN QRMQTY ELSE 0 END),'#0.00') AS QTY_123\n"
                + ",FORMAT(SUM(CASE WHEN QRMSTS = '1' THEN QRMQTY ELSE 0 END),'#0.00') AS QTY_1\n"
                + ",FORMAT(SUM(CASE WHEN QRMSTS = '2' THEN QRMQTY ELSE 0 END),'#0.00') AS QTY_2\n"
                + ",FORMAT(SUM(CASE WHEN QRMSTS = '3' THEN QRMQTY ELSE 0 END),'#0.00') AS QTY_3\n"
                + "FROM(\n"
                + "SELECT QRMWHSE, QWHNAME, QRMPLANT\n"
                + ",(SELECT TOP 1 SAPMCTRL \n"
                + "         FROM SAPMAS WHERE SAPMAT = QRMCODE\n"
                + "		 AND SAPPLANT = QRMPLANT\n"
                + "		 AND SAPVAL = QRMVAL) AS MATCTRL\n"
                + ", QRMCODE\n"
                + ",(SELECT TOP 1 SAPDESC \n"
                + "         FROM SAPMAS WHERE SAPMAT = QRMCODE\n"
                + "		 AND SAPPLANT = QRMPLANT\n"
                + "		 AND SAPVAL = QRMVAL) AS SAPDESC\n"
                + ",QRMSTS, QRMQTY\n"
                + "FROM QRMMAS\n"
                + "LEFT JOIN QWHMAS ON QWHMAS.QWHCOD = QRMMAS.QRMWHSE\n"
                + "WHERE QRMWHSE NOT IN ('','TWC')\n"
                + ")TWC1000\n"
                + "WHERE MATCTRL NOT IN ('')\n"
                + "AND QRMCODE NOT IN ('')\n"
                + "AND QRMPLANT = '" + PLANT + "'\n"
                + "AND MATCTRL = '" + MATCT + "'\n"
                + "GROUP BY QRMWHSE, QWHNAME, QRMPLANT, MATCTRL, QRMCODE, SAPDESC\n"
                + ")PART4\n"
                + "LEFT JOIN MSSMATN ON MSSMATN.LGPBE = PART4.MATCT\n"
                + "WHERE QWHCOD = MSWHSE\n"
                + "--END PART 4\n"
                + "ORDER BY QWHCOD, QWHNAME, PLANT, MATCT, MATCO, MATDESC";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                WMS960 p = new WMS960();

                p.setMatCo(result.getString("MATCO"));
                p.setDesc(result.getString("MATDESC"));

                p.setSts123(result.getString("QTY_123"));
                p.setSts1(result.getString("QTY_1"));
                p.setSts2(result.getString("QTY_2"));
                p.setSts3(result.getString("QTY_3"));

                p.setUserN(result.getString("USERS"));
                p.setMatCtN(result.getString("MATCNAME"));

                UAList.add(p);

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public List<WMS960> findAll960RM(String USERID, String WHS, String MATCT, String PLANT, String RM, String ID) {

        List<WMS960> UAList = new ArrayList<WMS960>();

        String sql = "SELECT '" + USERID + "' AS QUSUSERID, (SELECT TOP 1 USERS FROM MSSUSER WHERE USERID = '" + USERID + "') AS USERS\n"
                + ",MATCNAME\n"
                + ",*\n"
                + "FROM(\n"
                + "SELECT QRMWHSE AS QWHCOD, QWHNAME AS QWHNAME, QRMPLANT AS PLANT, MATCTRL AS MATCT, RM, QRMCODE AS MATCO, SAPDESC AS MATDESC\n"
                + ",FORMAT(SUM(CASE WHEN QRMSTS IN ('1','2','3','4') THEN QRMQTY ELSE 0 END),'#,#0.00') AS QTY_1234\n"
                + ",FORMAT(SUM(CASE WHEN QRMSTS = '1' THEN QRMQTY ELSE 0 END),'#,#0.00') AS QTY_1\n"
                + ",FORMAT(SUM(CASE WHEN QRMSTS = '2' THEN QRMQTY ELSE 0 END),'#,#0.00') AS QTY_2\n"
                + ",FORMAT(SUM(CASE WHEN QRMSTS = '3' THEN QRMQTY ELSE 0 END),'#,#0.00') AS QTY_3\n"
                + ",FORMAT(SUM(CASE WHEN QRMSTS = '4' THEN QRMQTY ELSE 0 END),'#,#0.00') AS QTY_4\n"
                + ",FORMAT(SUM(CASE WHEN QRMSTS = 'P' THEN QRMQTY ELSE 0 END),'#,#0.00') AS QTY_P\n"
                + "FROM(\n"
                + "SELECT QRMWHSE, QWHNAME, QRMPLANT\n"
                + ",(SELECT TOP 1 SAPMCTRL \n"
                + "         FROM SAPMAS WHERE SAPMAT = QRMCODE\n"
                + "		 AND SAPPLANT = QRMPLANT\n"
                + "		 AND SAPVAL = QRMVAL) AS MATCTRL\n"
                + ",LEFT(QRMCODE,8) AS RM, QRMCODE\n"
                + ",(SELECT TOP 1 SAPDESC \n"
                + "         FROM SAPMAS WHERE SAPMAT = QRMCODE\n"
                + "		 AND SAPPLANT = QRMPLANT\n"
                + "		 AND SAPVAL = QRMVAL) AS SAPDESC\n"
                + ",QRMSTS, QRMQTY\n"
                + "FROM QRMMAS\n"
                + "LEFT JOIN QWHMAS ON QWHMAS.QWHCOD = QRMMAS.QRMWHSE\n"
                + "WHERE QRMWHSE NOT IN ('','TWC')\n"
                + ")TWC1000\n"
                + "WHERE MATCTRL NOT IN ('')\n"
                + "AND RM NOT IN ('')\n"
                + "AND QRMCODE NOT IN ('')\n"
                + "AND QRMWHSE = '" + WHS + "'\n"
                + "AND QRMPLANT = '" + PLANT + "'\n"
                + "AND MATCTRL = '" + MATCT + "'\n"
                + "AND RM = '" + RM + "'\n"
                + "GROUP BY QRMWHSE, QWHNAME, QRMPLANT, MATCTRL, RM, QRMCODE, SAPDESC\n"
                + ")PART4\n"
                + "LEFT JOIN MSSMATN ON MSSMATN.LGPBE = PART4.MATCT\n"
                + "WHERE QWHCOD = MSWHSE\n"
                + "--END PART 4\n"
                + "ORDER BY QWHCOD, QWHNAME, PLANT, MATCT, RM, MATCO, MATDESC";
//        if (RM.trim().equals("2UND1477")) {
//            System.out.println(sql);
//        }

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            String wh = "";
            String matc = "";
            String user = "";

            while (result.next()) {

                WMS960 p = new WMS960();

                p.setDT_RowId(ID);

//                if (user.equals(result.getString("QUSUSERID"))) {
                p.setUser("<b style=\"opacity: 0;\">" + result.getString("QUSUSERID") + " : " + result.getString("USERS") + "</b>");
//                    user = result.getString("QUSUSERID");
//                } else {
//                    p.setUser(result.getString("QUSUSERID") + " : " + result.getString("USERS"));
//                    user = result.getString("QUSUSERID");
//                }

//                if (wh.equals(result.getString("WHS"))) {
                p.setWh("<b style=\"opacity: 0;\">" + result.getString("QWHCOD") + " : " + result.getString("QWHNAME") + "</b>");
//                    wh = result.getString("WHS");
//                } else {
//                    p.setWh(result.getString("WHS") + " : " + result.getString("QWHNAME"));
//                    wh = result.getString("WHS");
//                }

//                if (matc.equals(result.getString("QUSMTCTRL"))) {
                p.setMatCt("<b style=\"opacity: 0;\">" + result.getString("MATCT") + " : " + result.getString("MATCNAME") + "</b>");
//                    matc = result.getString("QUSMTCTRL");
//                } else {
//                    p.setMatCt(result.getString("QUSMTCTRL") + " : " + result.getString("MATCNAME"));
//                    matc = result.getString("QUSMTCTRL");
//                }

                p.setPlant("<b style=\"opacity: 0;\">" + result.getString("PLANT") + "</b>");
                p.setRm("<b style=\"opacity: 0;\">" + result.getString("RM") + "</b>");

                p.setUserN(result.getString("QUSUSERID"));
                p.setWhN(result.getString("QWHCOD"));
                p.setMatCtN(result.getString("MATCT"));
                p.setPlantN(result.getString("PLANT"));
                p.setRmN(result.getString("RM"));
                p.setMatCoN(result.getString("MATCO"));
                p.setDescN(result.getString("MATDESC"));

                p.setMatCo(result.getString("MATCO"));
                p.setDesc(result.getString("MATDESC"));

                if (result.getString("PLANT").equals("TOTAL")) {
                    p.setPlant("<b>" + result.getString("PLANT") + "</b>");
                    p.setSts123("<b>" + result.getString("QTY_1234") + "</b>");
                    p.setSts1("<b>" + result.getString("QTY_1") + "</b>");
                    p.setSts2("<b>" + result.getString("QTY_2") + "</b>");
                    p.setSts3("<b>" + result.getString("QTY_3") + "</b>");
                    p.setSts4("<b>" + result.getString("QTY_4") + "</b>");
                    p.setStsP("<b>" + result.getString("QTY_P") + "</b>");
                } else {
//                    p.setPlant(result.getString("PLANT"));
                    p.setSts123(result.getString("QTY_1234"));
                    p.setSts1(result.getString("QTY_1"));
                    p.setSts2(result.getString("QTY_2"));
                    p.setSts3(result.getString("QTY_3"));
                    p.setSts4(result.getString("QTY_4"));
                    p.setStsP(result.getString("QTY_P"));
                }

                UAList.add(p);

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public List<WMS960> findAll960Plant(String USERID, String WHS, String MATCT, String PLANT, String ID) {

        List<WMS960> UAList = new ArrayList<WMS960>();

        String sql = "SELECT '" + USERID + "' AS QUSUSERID, (SELECT TOP 1 USERS FROM MSSUSER WHERE USERID = '" + USERID + "') AS USERS\n"
                + ",MATCNAME\n"
                + ",*\n"
                + "FROM(\n"
                + "SELECT QRMWHSE AS QWHCOD, QWHNAME AS QWHNAME, QRMPLANT AS PLANT, MATCTRL AS MATCT, RM,'' AS MATCO,'' AS MATDESC\n"
                + ",FORMAT(SUM(CASE WHEN QRMSTS IN ('1','2','3','4') THEN QRMQTY ELSE 0 END),'#,#0.00') AS QTY_1234\n"
                + ",FORMAT(SUM(CASE WHEN QRMSTS = '1' THEN QRMQTY ELSE 0 END),'#,#0.00') AS QTY_1\n"
                + ",FORMAT(SUM(CASE WHEN QRMSTS = '2' THEN QRMQTY ELSE 0 END),'#,#0.00') AS QTY_2\n"
                + ",FORMAT(SUM(CASE WHEN QRMSTS = '3' THEN QRMQTY ELSE 0 END),'#,#0.00') AS QTY_3\n"
                + ",FORMAT(SUM(CASE WHEN QRMSTS = '4' THEN QRMQTY ELSE 0 END),'#,#0.00') AS QTY_4\n"
                + ",FORMAT(SUM(CASE WHEN QRMSTS = 'P' THEN QRMQTY ELSE 0 END),'#,#0.00') AS QTY_P\n"
                + "FROM(\n"
                + "SELECT QRMWHSE, QWHNAME, QRMPLANT\n"
                + ",(SELECT TOP 1 SAPMCTRL \n"
                + "         FROM SAPMAS WHERE SAPMAT = QRMCODE\n"
                + "		 AND SAPPLANT = QRMPLANT\n"
                + "		 AND SAPVAL = QRMVAL) AS MATCTRL\n"
                + ",LEFT(QRMCODE,8) AS RM, QRMCODE\n"
                + ",(SELECT TOP 1 SAPDESC \n"
                + "         FROM SAPMAS WHERE SAPMAT = QRMCODE\n"
                + "		 AND SAPPLANT = QRMPLANT\n"
                + "		 AND SAPVAL = QRMVAL) AS SAPDESC\n"
                + ",QRMSTS, QRMQTY\n"
                + "FROM QRMMAS\n"
                + "LEFT JOIN QWHMAS ON QWHMAS.QWHCOD = QRMMAS.QRMWHSE\n"
                + "WHERE QRMWHSE NOT IN ('','TWC')\n"
                + ")TWC1000\n"
                + "WHERE MATCTRL NOT IN ('')\n"
                + "AND QRMWHSE = '" + WHS + "'\n"
                + "AND QRMPLANT = '" + PLANT + "'\n"
                + "AND MATCTRL = '" + MATCT + "'\n"
                + "GROUP BY QRMWHSE, QWHNAME, QRMPLANT, MATCTRL, RM\n"
                + ")PART3\n"
                + "LEFT JOIN MSSMATN ON MSSMATN.LGPBE = PART3.MATCT\n"
                + "WHERE QWHCOD = MSWHSE\n"
                + "--END PART 3\n"
                + "ORDER BY QWHCOD, QWHNAME, PLANT, MATCT, RM, MATCO, MATDESC";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            String wh = "";
            String matc = "";
            String user = "";

            while (result.next()) {

                WMS960 p = new WMS960();

                p.setDT_RowId(ID);

//                if (user.equals(result.getString("QUSUSERID"))) {
                p.setUser("<b style=\"opacity: 0;\">" + result.getString("QUSUSERID") + " : " + result.getString("USERS") + "</b>");
//                    user = result.getString("QUSUSERID");
//                } else {
//                    p.setUser(result.getString("QUSUSERID") + " : " + result.getString("USERS"));
//                    user = result.getString("QUSUSERID");
//                }

//                if (wh.equals(result.getString("WHS"))) {
                p.setWh("<b style=\"opacity: 0;\">" + result.getString("QWHCOD") + " : " + result.getString("QWHNAME") + "</b>");
//                    wh = result.getString("WHS");
//                } else {
//                    p.setWh(result.getString("WHS") + " : " + result.getString("QWHNAME"));
//                    wh = result.getString("WHS");
//                }

//                if (matc.equals(result.getString("QUSMTCTRL"))) {
                p.setMatCt("<b style=\"opacity: 0;\">" + result.getString("MATCT") + " : " + result.getString("MATCNAME") + "</b>");
//                    matc = result.getString("QUSMTCTRL");
//                } else {
//                    p.setMatCt(result.getString("QUSMTCTRL") + " : " + result.getString("MATCNAME"));
//                    matc = result.getString("QUSMTCTRL");
//                }

                p.setPlant("<b style=\"opacity: 0;\">" + result.getString("PLANT") + "</b>");

                p.setUserN(result.getString("QUSUSERID"));
                p.setWhN(result.getString("QWHCOD"));
                p.setMatCtN(result.getString("MATCT"));
                p.setPlantN(result.getString("PLANT"));
                p.setRmN(result.getString("RM"));
//                p.setMatCoN(result.getString("MATCO"));
//                p.setDescN(result.getString("MATDESC"));

                p.setRm(result.getString("RM"));
                p.setMatCo("");
                p.setDesc("");

                if (result.getString("PLANT").equals("TOTAL")) {
                    p.setPlant("<b>" + result.getString("PLANT") + "</b>");
                    p.setSts123("<b>" + result.getString("QTY_1234") + "</b>");
                    p.setSts1("<b>" + result.getString("QTY_1") + "</b>");
                    p.setSts2("<b>" + result.getString("QTY_2") + "</b>");
                    p.setSts3("<b>" + result.getString("QTY_3") + "</b>");
                    p.setSts4("<b>" + result.getString("QTY_4") + "</b>");
                    p.setStsP("<b>" + result.getString("QTY_P") + "</b>");
                } else {
//                    p.setPlant(result.getString("PLANT"));
                    p.setSts123(result.getString("QTY_1234"));
                    p.setSts1(result.getString("QTY_1"));
                    p.setSts2(result.getString("QTY_2"));
                    p.setSts3(result.getString("QTY_3"));
                    p.setSts4(result.getString("QTY_4"));
                    p.setStsP(result.getString("QTY_P"));
                }

                UAList.add(p);

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public List<WMS960> findAll960(String USERID) {

        List<WMS960> UAList = new ArrayList<WMS960>();

        String sql = "DECLARE @USER NVARCHAR(10) = '" + USERID + "'\n"
                + "SELECT *, 'TOTAL' AS PLANT\n"
                + ",FORMAT(ISNULL(CASE WHEN WHS = 'TWC' \n"
                + "       THEN (SELECT SUM(QRMQTY) FROM QRMMAS LEFT JOIN MSSMATN ON MSSMATN.LGPBE = (SELECT TOP 1 SAPMCTRL FROM SAPMAS WHERE SAPMAT = QRMCODE AND SAPPLANT = QRMPLANT AND SAPVAL = QRMVAL)\n"
                + "            WHERE QRMSTS IN ('1','2','3','4') AND QRMWHSE IS NOT NULL AND QRMWHSE != 'TWC' AND QRMPLANT IN ('1000','1050') AND QRMWHSE = MSSMATN.MSWHSE AND LGPBE = QUSMTCTRL)\n"
                + "       ELSE (SELECT SUM(QRMQTY) FROM QRMMAS LEFT JOIN MSSMATN ON MSSMATN.LGPBE = (SELECT TOP 1 SAPMCTRL FROM SAPMAS WHERE SAPMAT = QRMCODE AND SAPPLANT = QRMPLANT AND SAPVAL = QRMVAL)\n"
                + "            WHERE QRMSTS IN ('1','2','3','4') AND QRMWHSE = WHS AND QRMPLANT IN ('1000','1050') AND QRMWHSE = MSWHSE AND LGPBE = QUSMTCTRL)\n"
                + "       END,0),'#,#0.00') AS QTY_1234\n"
                + ",FORMAT(ISNULL(CASE WHEN WHS = 'TWC' \n"
                + "       THEN (SELECT SUM(QRMQTY) FROM QRMMAS LEFT JOIN MSSMATN ON MSSMATN.LGPBE = (SELECT TOP 1 SAPMCTRL FROM SAPMAS WHERE SAPMAT = QRMCODE AND SAPPLANT = QRMPLANT AND SAPVAL = QRMVAL)\n"
                + "            WHERE QRMSTS IN ('1') AND QRMWHSE IS NOT NULL AND QRMWHSE != 'TWC' AND QRMPLANT IN ('1000','1050') AND QRMWHSE = MSSMATN.MSWHSE AND LGPBE = QUSMTCTRL)\n"
                + "       ELSE (SELECT SUM(QRMQTY) FROM QRMMAS LEFT JOIN MSSMATN ON MSSMATN.LGPBE = (SELECT TOP 1 SAPMCTRL FROM SAPMAS WHERE SAPMAT = QRMCODE AND SAPPLANT = QRMPLANT AND SAPVAL = QRMVAL)\n"
                + "            WHERE QRMSTS IN ('1') AND QRMWHSE = WHS AND QRMPLANT IN ('1000','1050') AND QRMWHSE = MSWHSE AND LGPBE = QUSMTCTRL)\n"
                + "       END,0),'#,#0.00') AS QTY_1\n"
                + ",FORMAT(ISNULL(CASE WHEN WHS = 'TWC' \n"
                + "       THEN (SELECT SUM(QRMQTY) FROM QRMMAS LEFT JOIN MSSMATN ON MSSMATN.LGPBE = (SELECT TOP 1 SAPMCTRL FROM SAPMAS WHERE SAPMAT = QRMCODE AND SAPPLANT = QRMPLANT AND SAPVAL = QRMVAL)\n"
                + "            WHERE QRMSTS IN ('2') AND QRMWHSE IS NOT NULL AND QRMWHSE != 'TWC' AND QRMPLANT IN ('1000','1050') AND QRMWHSE = MSSMATN.MSWHSE AND LGPBE = QUSMTCTRL)\n"
                + "       ELSE (SELECT SUM(QRMQTY) FROM QRMMAS LEFT JOIN MSSMATN ON MSSMATN.LGPBE = (SELECT TOP 1 SAPMCTRL FROM SAPMAS WHERE SAPMAT = QRMCODE AND SAPPLANT = QRMPLANT AND SAPVAL = QRMVAL)\n"
                + "            WHERE QRMSTS IN ('2') AND QRMWHSE = WHS AND QRMPLANT IN ('1000','1050') AND QRMWHSE = MSWHSE AND LGPBE = QUSMTCTRL)\n"
                + "       END,0),'#,#0.00') AS QTY_2\n"
                + ",FORMAT(ISNULL(CASE WHEN WHS = 'TWC' \n"
                + "       THEN (SELECT SUM(QRMQTY) FROM QRMMAS LEFT JOIN MSSMATN ON MSSMATN.LGPBE = (SELECT TOP 1 SAPMCTRL FROM SAPMAS WHERE SAPMAT = QRMCODE AND SAPPLANT = QRMPLANT AND SAPVAL = QRMVAL)\n"
                + "            WHERE QRMSTS IN ('3') AND QRMWHSE IS NOT NULL AND QRMWHSE != 'TWC' AND QRMPLANT IN ('1000','1050') AND QRMWHSE = MSSMATN.MSWHSE AND LGPBE = QUSMTCTRL)\n"
                + "       ELSE (SELECT SUM(QRMQTY) FROM QRMMAS LEFT JOIN MSSMATN ON MSSMATN.LGPBE = (SELECT TOP 1 SAPMCTRL FROM SAPMAS WHERE SAPMAT = QRMCODE AND SAPPLANT = QRMPLANT AND SAPVAL = QRMVAL)\n"
                + "            WHERE QRMSTS IN ('3') AND QRMWHSE = WHS AND QRMPLANT IN ('1000','1050') AND QRMWHSE = MSWHSE AND LGPBE = QUSMTCTRL)\n"
                + "       END,0),'#,#0.00') AS QTY_3\n"
                + ",FORMAT(ISNULL(CASE WHEN WHS = 'TWC' \n"
                + "       THEN (SELECT SUM(QRMQTY) FROM QRMMAS LEFT JOIN MSSMATN ON MSSMATN.LGPBE = (SELECT TOP 1 SAPMCTRL FROM SAPMAS WHERE SAPMAT = QRMCODE AND SAPPLANT = QRMPLANT AND SAPVAL = QRMVAL)\n"
                + "            WHERE QRMSTS IN ('4') AND QRMWHSE IS NOT NULL AND QRMWHSE != 'TWC' AND QRMPLANT IN ('1000','1050') AND QRMWHSE = MSSMATN.MSWHSE AND LGPBE = QUSMTCTRL)\n"
                + "       ELSE (SELECT SUM(QRMQTY) FROM QRMMAS LEFT JOIN MSSMATN ON MSSMATN.LGPBE = (SELECT TOP 1 SAPMCTRL FROM SAPMAS WHERE SAPMAT = QRMCODE AND SAPPLANT = QRMPLANT AND SAPVAL = QRMVAL)\n"
                + "            WHERE QRMSTS IN ('4') AND QRMWHSE = WHS AND QRMPLANT IN ('1000','1050') AND QRMWHSE = MSWHSE AND LGPBE = QUSMTCTRL)\n"
                + "       END,0),'#,#0.00') AS QTY_4\n"
                + ",FORMAT(ISNULL(CASE WHEN WHS = 'TWC' \n"
                + "       THEN (SELECT SUM(QRMQTY) FROM QRMMAS LEFT JOIN MSSMATN ON MSSMATN.LGPBE = (SELECT TOP 1 SAPMCTRL FROM SAPMAS WHERE SAPMAT = QRMCODE AND SAPPLANT = QRMPLANT AND SAPVAL = QRMVAL)\n"
                + "            WHERE QRMSTS IN ('P') AND QRMWHSE IS NOT NULL AND QRMWHSE != 'TWC' AND QRMPLANT IN ('1000','1050') AND QRMWHSE = MSSMATN.MSWHSE AND LGPBE = QUSMTCTRL)\n"
                + "       ELSE (SELECT SUM(QRMQTY) FROM QRMMAS LEFT JOIN MSSMATN ON MSSMATN.LGPBE = (SELECT TOP 1 SAPMCTRL FROM SAPMAS WHERE SAPMAT = QRMCODE AND SAPPLANT = QRMPLANT AND SAPVAL = QRMVAL)\n"
                + "            WHERE QRMSTS IN ('P') AND QRMWHSE = WHS AND QRMPLANT IN ('1000','1050') AND QRMWHSE = MSWHSE AND LGPBE = QUSMTCTRL)\n"
                + "       END,0),'#,#0.00') AS QTY_P\n"
                + "FROM(\n"
                + "SELECT QUSUSERID, USERS, MSSMATN.MSWHSE AS WHS, QWHNAME, QUSMTCTRL, MATCNAME\n"
                + "FROM QUSMTCTRL\n"
                + "LEFT JOIN MSSMATN ON MSSMATN.LGPBE = QUSMTCTRL.QUSMTCTRL\n"
                + "LEFT JOIN MSSUSER ON MSSUSER.USERID = QUSMTCTRL.QUSUSERID\n"
                + "LEFT JOIN QWHMAS ON QWHMAS.QWHCOD = MSSMATN.MSWHSE\n"
                + "WHERE QUSUSERID = @USER\n"
                + ")TMP\n"
                + "UNION ALL\n"
                + "SELECT *, '1000' AS PLANT\n"
                + ",FORMAT(ISNULL(CASE WHEN WHS = 'TWC' \n"
                + "       THEN (SELECT SUM(QRMQTY) FROM QRMMAS LEFT JOIN MSSMATN ON MSSMATN.LGPBE = (SELECT TOP 1 SAPMCTRL FROM SAPMAS WHERE SAPMAT = QRMCODE AND SAPPLANT = QRMPLANT AND SAPVAL = QRMVAL)\n"
                + "            WHERE QRMSTS IN ('1','2','3','4') AND QRMWHSE IS NOT NULL AND QRMWHSE != 'TWC' AND QRMPLANT IN ('1000') AND QRMWHSE = MSSMATN.MSWHSE AND LGPBE = QUSMTCTRL)\n"
                + "       ELSE (SELECT SUM(QRMQTY) FROM QRMMAS LEFT JOIN MSSMATN ON MSSMATN.LGPBE = (SELECT TOP 1 SAPMCTRL FROM SAPMAS WHERE SAPMAT = QRMCODE AND SAPPLANT = QRMPLANT AND SAPVAL = QRMVAL)\n"
                + "            WHERE QRMSTS IN ('1','2','3','4') AND QRMWHSE = WHS AND QRMPLANT IN ('1000') AND QRMWHSE = MSWHSE AND LGPBE = QUSMTCTRL)\n"
                + "       END,0),'#,#0.00') AS QTY_1234\n"
                + ",FORMAT(ISNULL(CASE WHEN WHS = 'TWC' \n"
                + "       THEN (SELECT SUM(QRMQTY) FROM QRMMAS LEFT JOIN MSSMATN ON MSSMATN.LGPBE = (SELECT TOP 1 SAPMCTRL FROM SAPMAS WHERE SAPMAT = QRMCODE AND SAPPLANT = QRMPLANT AND SAPVAL = QRMVAL)\n"
                + "            WHERE QRMSTS IN ('1') AND QRMWHSE IS NOT NULL AND QRMWHSE != 'TWC' AND QRMPLANT IN ('1000') AND QRMWHSE = MSSMATN.MSWHSE AND LGPBE = QUSMTCTRL)\n"
                + "       ELSE (SELECT SUM(QRMQTY) FROM QRMMAS LEFT JOIN MSSMATN ON MSSMATN.LGPBE = (SELECT TOP 1 SAPMCTRL FROM SAPMAS WHERE SAPMAT = QRMCODE AND SAPPLANT = QRMPLANT AND SAPVAL = QRMVAL)\n"
                + "            WHERE QRMSTS IN ('1') AND QRMWHSE = WHS AND QRMPLANT IN ('1000') AND QRMWHSE = MSWHSE AND LGPBE = QUSMTCTRL)\n"
                + "       END,0),'#,#0.00') AS QTY_1\n"
                + ",FORMAT(ISNULL(CASE WHEN WHS = 'TWC' \n"
                + "       THEN (SELECT SUM(QRMQTY) FROM QRMMAS LEFT JOIN MSSMATN ON MSSMATN.LGPBE = (SELECT TOP 1 SAPMCTRL FROM SAPMAS WHERE SAPMAT = QRMCODE AND SAPPLANT = QRMPLANT AND SAPVAL = QRMVAL)\n"
                + "            WHERE QRMSTS IN ('2') AND QRMWHSE IS NOT NULL AND QRMWHSE != 'TWC' AND QRMPLANT IN ('1000') AND QRMWHSE = MSSMATN.MSWHSE AND LGPBE = QUSMTCTRL)\n"
                + "       ELSE (SELECT SUM(QRMQTY) FROM QRMMAS LEFT JOIN MSSMATN ON MSSMATN.LGPBE = (SELECT TOP 1 SAPMCTRL FROM SAPMAS WHERE SAPMAT = QRMCODE AND SAPPLANT = QRMPLANT AND SAPVAL = QRMVAL)\n"
                + "            WHERE QRMSTS IN ('2') AND QRMWHSE = WHS AND QRMPLANT IN ('1000') AND QRMWHSE = MSWHSE AND LGPBE = QUSMTCTRL)\n"
                + "       END,0),'#,#0.00') AS QTY_2\n"
                + ",FORMAT(ISNULL(CASE WHEN WHS = 'TWC' \n"
                + "       THEN (SELECT SUM(QRMQTY) FROM QRMMAS LEFT JOIN MSSMATN ON MSSMATN.LGPBE = (SELECT TOP 1 SAPMCTRL FROM SAPMAS WHERE SAPMAT = QRMCODE AND SAPPLANT = QRMPLANT AND SAPVAL = QRMVAL)\n"
                + "            WHERE QRMSTS IN ('3') AND QRMWHSE IS NOT NULL AND QRMWHSE != 'TWC' AND QRMPLANT IN ('1000') AND QRMWHSE = MSSMATN.MSWHSE AND LGPBE = QUSMTCTRL)\n"
                + "       ELSE (SELECT SUM(QRMQTY) FROM QRMMAS LEFT JOIN MSSMATN ON MSSMATN.LGPBE = (SELECT TOP 1 SAPMCTRL FROM SAPMAS WHERE SAPMAT = QRMCODE AND SAPPLANT = QRMPLANT AND SAPVAL = QRMVAL)\n"
                + "            WHERE QRMSTS IN ('3') AND QRMWHSE = WHS AND QRMPLANT IN ('1000') AND QRMWHSE = MSWHSE AND LGPBE = QUSMTCTRL)\n"
                + "       END,0),'#,#0.00') AS QTY_3\n"
                + ",FORMAT(ISNULL(CASE WHEN WHS = 'TWC' \n"
                + "       THEN (SELECT SUM(QRMQTY) FROM QRMMAS LEFT JOIN MSSMATN ON MSSMATN.LGPBE = (SELECT TOP 1 SAPMCTRL FROM SAPMAS WHERE SAPMAT = QRMCODE AND SAPPLANT = QRMPLANT AND SAPVAL = QRMVAL)\n"
                + "            WHERE QRMSTS IN ('4') AND QRMWHSE IS NOT NULL AND QRMWHSE != 'TWC' AND QRMPLANT IN ('1000') AND QRMWHSE = MSSMATN.MSWHSE AND LGPBE = QUSMTCTRL)\n"
                + "       ELSE (SELECT SUM(QRMQTY) FROM QRMMAS LEFT JOIN MSSMATN ON MSSMATN.LGPBE = (SELECT TOP 1 SAPMCTRL FROM SAPMAS WHERE SAPMAT = QRMCODE AND SAPPLANT = QRMPLANT AND SAPVAL = QRMVAL)\n"
                + "            WHERE QRMSTS IN ('4') AND QRMWHSE = WHS AND QRMPLANT IN ('1000') AND QRMWHSE = MSWHSE AND LGPBE = QUSMTCTRL)\n"
                + "       END,0),'#,#0.00') AS QTY_4\n"
                + ",FORMAT(ISNULL(CASE WHEN WHS = 'TWC' \n"
                + "       THEN (SELECT SUM(QRMQTY) FROM QRMMAS LEFT JOIN MSSMATN ON MSSMATN.LGPBE = (SELECT TOP 1 SAPMCTRL FROM SAPMAS WHERE SAPMAT = QRMCODE AND SAPPLANT = QRMPLANT AND SAPVAL = QRMVAL)\n"
                + "            WHERE QRMSTS IN ('P') AND QRMWHSE IS NOT NULL AND QRMWHSE != 'TWC' AND QRMPLANT IN ('1000') AND QRMWHSE = MSSMATN.MSWHSE AND LGPBE = QUSMTCTRL)\n"
                + "       ELSE (SELECT SUM(QRMQTY) FROM QRMMAS LEFT JOIN MSSMATN ON MSSMATN.LGPBE = (SELECT TOP 1 SAPMCTRL FROM SAPMAS WHERE SAPMAT = QRMCODE AND SAPPLANT = QRMPLANT AND SAPVAL = QRMVAL)\n"
                + "            WHERE QRMSTS IN ('P') AND QRMWHSE = WHS AND QRMPLANT IN ('1000') AND QRMWHSE = MSWHSE AND LGPBE = QUSMTCTRL)\n"
                + "       END,0),'#,#0.00') AS QTY_P\n"
                + "FROM(\n"
                + "SELECT QUSUSERID, USERS, MSSMATN.MSWHSE AS WHS, QWHNAME, QUSMTCTRL, MATCNAME\n"
                + "FROM QUSMTCTRL\n"
                + "LEFT JOIN MSSMATN ON MSSMATN.LGPBE = QUSMTCTRL.QUSMTCTRL\n"
                + "LEFT JOIN MSSUSER ON MSSUSER.USERID = QUSMTCTRL.QUSUSERID\n"
                + "LEFT JOIN QWHMAS ON QWHMAS.QWHCOD = MSSMATN.MSWHSE\n"
                + "WHERE QUSUSERID = @USER\n"
                + ")TMP2\n"
                + "UNION ALL\n"
                + "SELECT *, '1050' AS PLANT\n"
                + ",FORMAT(ISNULL(CASE WHEN WHS = 'TWC' \n"
                + "       THEN (SELECT SUM(QRMQTY) FROM QRMMAS LEFT JOIN MSSMATN ON MSSMATN.LGPBE = (SELECT TOP 1 SAPMCTRL FROM SAPMAS WHERE SAPMAT = QRMCODE AND SAPPLANT = QRMPLANT AND SAPVAL = QRMVAL)\n"
                + "            WHERE QRMSTS IN ('1','2','3','4') AND QRMWHSE IS NOT NULL AND QRMWHSE != 'TWC' AND QRMPLANT IN ('1050') AND QRMWHSE = MSSMATN.MSWHSE AND LGPBE = QUSMTCTRL)\n"
                + "       ELSE (SELECT SUM(QRMQTY) FROM QRMMAS LEFT JOIN MSSMATN ON MSSMATN.LGPBE = (SELECT TOP 1 SAPMCTRL FROM SAPMAS WHERE SAPMAT = QRMCODE AND SAPPLANT = QRMPLANT AND SAPVAL = QRMVAL)\n"
                + "            WHERE QRMSTS IN ('1','2','3','4') AND QRMWHSE = WHS AND QRMPLANT IN ('1050') AND QRMWHSE = MSWHSE AND LGPBE = QUSMTCTRL)\n"
                + "       END,0),'#,#0.00') AS QTY_1234\n"
                + ",FORMAT(ISNULL(CASE WHEN WHS = 'TWC' \n"
                + "       THEN (SELECT SUM(QRMQTY) FROM QRMMAS LEFT JOIN MSSMATN ON MSSMATN.LGPBE = (SELECT TOP 1 SAPMCTRL FROM SAPMAS WHERE SAPMAT = QRMCODE AND SAPPLANT = QRMPLANT AND SAPVAL = QRMVAL)\n"
                + "            WHERE QRMSTS IN ('1') AND QRMWHSE IS NOT NULL AND QRMWHSE != 'TWC' AND QRMPLANT IN ('1050') AND QRMWHSE = MSSMATN.MSWHSE AND LGPBE = QUSMTCTRL)\n"
                + "       ELSE (SELECT SUM(QRMQTY) FROM QRMMAS LEFT JOIN MSSMATN ON MSSMATN.LGPBE = (SELECT TOP 1 SAPMCTRL FROM SAPMAS WHERE SAPMAT = QRMCODE AND SAPPLANT = QRMPLANT AND SAPVAL = QRMVAL)\n"
                + "            WHERE QRMSTS IN ('1') AND QRMWHSE = WHS AND QRMPLANT IN ('1050') AND QRMWHSE = MSWHSE AND LGPBE = QUSMTCTRL)\n"
                + "       END,0),'#,#0.00') AS QTY_1\n"
                + ",FORMAT(ISNULL(CASE WHEN WHS = 'TWC' \n"
                + "       THEN (SELECT SUM(QRMQTY) FROM QRMMAS LEFT JOIN MSSMATN ON MSSMATN.LGPBE = (SELECT TOP 1 SAPMCTRL FROM SAPMAS WHERE SAPMAT = QRMCODE AND SAPPLANT = QRMPLANT AND SAPVAL = QRMVAL)\n"
                + "            WHERE QRMSTS IN ('2') AND QRMWHSE IS NOT NULL AND QRMWHSE != 'TWC' AND QRMPLANT IN ('1050') AND QRMWHSE = MSSMATN.MSWHSE AND LGPBE = QUSMTCTRL)\n"
                + "       ELSE (SELECT SUM(QRMQTY) FROM QRMMAS LEFT JOIN MSSMATN ON MSSMATN.LGPBE = (SELECT TOP 1 SAPMCTRL FROM SAPMAS WHERE SAPMAT = QRMCODE AND SAPPLANT = QRMPLANT AND SAPVAL = QRMVAL)\n"
                + "            WHERE QRMSTS IN ('2') AND QRMWHSE = WHS AND QRMPLANT IN ('1050') AND QRMWHSE = MSWHSE AND LGPBE = QUSMTCTRL)\n"
                + "       END,0),'#,#0.00') AS QTY_2\n"
                + ",FORMAT(ISNULL(CASE WHEN WHS = 'TWC' \n"
                + "       THEN (SELECT SUM(QRMQTY) FROM QRMMAS LEFT JOIN MSSMATN ON MSSMATN.LGPBE = (SELECT TOP 1 SAPMCTRL FROM SAPMAS WHERE SAPMAT = QRMCODE AND SAPPLANT = QRMPLANT AND SAPVAL = QRMVAL)\n"
                + "            WHERE QRMSTS IN ('3') AND QRMWHSE IS NOT NULL AND QRMWHSE != 'TWC' AND QRMPLANT IN ('1050') AND QRMWHSE = MSSMATN.MSWHSE AND LGPBE = QUSMTCTRL)\n"
                + "       ELSE (SELECT SUM(QRMQTY) FROM QRMMAS LEFT JOIN MSSMATN ON MSSMATN.LGPBE = (SELECT TOP 1 SAPMCTRL FROM SAPMAS WHERE SAPMAT = QRMCODE AND SAPPLANT = QRMPLANT AND SAPVAL = QRMVAL)\n"
                + "            WHERE QRMSTS IN ('3') AND QRMWHSE = WHS AND QRMPLANT IN ('1050') AND QRMWHSE = MSWHSE AND LGPBE = QUSMTCTRL)\n"
                + "       END,0),'#,#0.00') AS QTY_3\n"
                + ",FORMAT(ISNULL(CASE WHEN WHS = 'TWC' \n"
                + "       THEN (SELECT SUM(QRMQTY) FROM QRMMAS LEFT JOIN MSSMATN ON MSSMATN.LGPBE = (SELECT TOP 1 SAPMCTRL FROM SAPMAS WHERE SAPMAT = QRMCODE AND SAPPLANT = QRMPLANT AND SAPVAL = QRMVAL)\n"
                + "            WHERE QRMSTS IN ('4') AND QRMWHSE IS NOT NULL AND QRMWHSE != 'TWC' AND QRMPLANT IN ('1050') AND QRMWHSE = MSSMATN.MSWHSE AND LGPBE = QUSMTCTRL)\n"
                + "       ELSE (SELECT SUM(QRMQTY) FROM QRMMAS LEFT JOIN MSSMATN ON MSSMATN.LGPBE = (SELECT TOP 1 SAPMCTRL FROM SAPMAS WHERE SAPMAT = QRMCODE AND SAPPLANT = QRMPLANT AND SAPVAL = QRMVAL)\n"
                + "            WHERE QRMSTS IN ('4') AND QRMWHSE = WHS AND QRMPLANT IN ('1050') AND QRMWHSE = MSWHSE AND LGPBE = QUSMTCTRL)\n"
                + "       END,0),'#,#0.00') AS QTY_4\n"
                + ",FORMAT(ISNULL(CASE WHEN WHS = 'TWC' \n"
                + "       THEN (SELECT SUM(QRMQTY) FROM QRMMAS LEFT JOIN MSSMATN ON MSSMATN.LGPBE = (SELECT TOP 1 SAPMCTRL FROM SAPMAS WHERE SAPMAT = QRMCODE AND SAPPLANT = QRMPLANT AND SAPVAL = QRMVAL)\n"
                + "            WHERE QRMSTS IN ('P') AND QRMWHSE IS NOT NULL AND QRMWHSE != 'TWC' AND QRMPLANT IN ('1050') AND QRMWHSE = MSSMATN.MSWHSE AND LGPBE = QUSMTCTRL)\n"
                + "       ELSE (SELECT SUM(QRMQTY) FROM QRMMAS LEFT JOIN MSSMATN ON MSSMATN.LGPBE = (SELECT TOP 1 SAPMCTRL FROM SAPMAS WHERE SAPMAT = QRMCODE AND SAPPLANT = QRMPLANT AND SAPVAL = QRMVAL)\n"
                + "            WHERE QRMSTS IN ('P') AND QRMWHSE = WHS AND QRMPLANT IN ('1050') AND QRMWHSE = MSWHSE AND LGPBE = QUSMTCTRL)\n"
                + "       END,0),'#,#0.00') AS QTY_P\n"
                + "FROM(\n"
                + "SELECT QUSUSERID, USERS, MSSMATN.MSWHSE AS WHS, QWHNAME, QUSMTCTRL, MATCNAME\n"
                + "FROM QUSMTCTRL\n"
                + "LEFT JOIN MSSMATN ON MSSMATN.LGPBE = QUSMTCTRL.QUSMTCTRL\n"
                + "LEFT JOIN MSSUSER ON MSSUSER.USERID = QUSMTCTRL.QUSUSERID\n"
                + "LEFT JOIN QWHMAS ON QWHMAS.QWHCOD = MSSMATN.MSWHSE\n"
                + "WHERE QUSUSERID = @USER\n"
                + ")TMP3\n"
                + "ORDER BY QUSUSERID, WHS, QUSMTCTRL, PLANT";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            String wh = "";
            String matc = "";
            String user = "";

            while (result.next()) {

                WMS960 p = new WMS960();

                p.setDT_RowId("HeadFirst");

                if (user.equals(result.getString("QUSUSERID"))) {
                    p.setUser("<b style=\"opacity: 0;\">" + result.getString("QUSUSERID") + " : " + result.getString("USERS") + "</b>");
                    user = result.getString("QUSUSERID");
                } else {
                    p.setUser(result.getString("QUSUSERID") + " : " + result.getString("USERS"));
                    user = result.getString("QUSUSERID");
                }

                if (wh.equals(result.getString("WHS"))) {
                    p.setWh("<b style=\"opacity: 0;\">" + result.getString("WHS") + " : " + result.getString("QWHNAME") + "</b>");
                    wh = result.getString("WHS");
                } else {
                    p.setWh(result.getString("WHS") + " : " + result.getString("QWHNAME"));
                    wh = result.getString("WHS");
                }

                if (matc.equals(result.getString("QUSMTCTRL"))) {
                    p.setMatCt("<b style=\"opacity: 0;\">" + result.getString("QUSMTCTRL") + " : " + result.getString("MATCNAME") + "</b>");
                    matc = result.getString("QUSMTCTRL");
                } else {
                    p.setMatCt(result.getString("QUSMTCTRL") + " : " + result.getString("MATCNAME"));
                    matc = result.getString("QUSMTCTRL");
                }

                p.setUserN(result.getString("QUSUSERID"));
                p.setWhN(result.getString("WHS"));
                p.setMatCtN(result.getString("QUSMTCTRL"));
                p.setPlantN(result.getString("PLANT"));
//                p.setRmN(result.getString("RM"));
//                p.setMatCoN(result.getString("MATCO"));
//                p.setDescN(result.getString("MATDESC"));

                p.setRm("");
                p.setMatCo("");
                p.setDesc("");

                if (result.getString("PLANT").equals("TOTAL")) {
                    p.setPlant("<b>" + result.getString("PLANT") + "</b>");
                    p.setSts123("<b>" + result.getString("QTY_1234") + "</b>");
                    p.setSts1("<b>" + result.getString("QTY_1") + "</b>");
                    p.setSts2("<b>" + result.getString("QTY_2") + "</b>");
                    p.setSts3("<b>" + result.getString("QTY_3") + "</b>");
                    p.setSts4("<b>" + result.getString("QTY_4") + "</b>");
                    p.setStsP("<b>" + result.getString("QTY_P") + "</b>");
                } else {
                    p.setPlant(result.getString("PLANT"));
                    p.setSts123(result.getString("QTY_1234"));
                    p.setSts1(result.getString("QTY_1"));
                    p.setSts2(result.getString("QTY_2"));
                    p.setSts3(result.getString("QTY_3"));
                    p.setSts4(result.getString("QTY_4"));
                    p.setStsP(result.getString("QTY_P"));
                }

                UAList.add(p);

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public List<PGMU> findAll(String uid) {

        List<PGMU> UAList = new ArrayList<PGMU>();

        String sql = "SELECT * FROM QUSMTCTRL\n"
                + "LEFT JOIN MSSMATN ON MSSMATN.LGPBE = QUSMTCTRL.QUSMTCTRL\n"
                + "WHERE QUSUSERID = ? ";

        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            ps.setString(1, uid);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                PGMU p = new PGMU();

                p.setUid(result.getString("QUSMTCTRL"));

                p.setName(result.getString("MATCNAME"));

                UAList.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public String check(String uid, String pid) {
        PreparedStatement ps = null;
        ResultSet rs = null;
        String STRRETURN = null;
        try {
            String check = "Select * FROM [RMShipment].[dbo].[QUSMTCTRL] "
                    + "where [QUSUSERID] = '" + uid + "' AND [QUSMTCTRL] = '" + pid + "'";
//            System.out.println(check);
            ps = connect.prepareStatement(check);
            rs = ps.executeQuery();
            if (rs.next()) {
                STRRETURN = "f";
            } else {
                STRRETURN = "t";
            }
            connect.close();
        } catch (Exception exp) {
            exp.printStackTrace();
        }
        return STRRETURN;
    }

    public boolean add(PGMU ua, String uid) {

        boolean result = false;

        String sql = "INSERT INTO QUSMTCTRL"
                + " (QUSUSERID, QUSMTCTRL, QUSEDT, QUSCDT, QUSUSER)"
                + " VALUES('" + ua.getUid() + "', '" + ua.getName() + "', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '" + uid + "')";
//        System.out.println(sql);
        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public void delete(String uid, String pid) {

        String sql = "DELETE FROM QUSMTCTRL "
                + "WHERE QUSUSERID = ? AND QUSMTCTRL = ?";

        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            ps.setString(1, uid);
            ps.setString(2, pid);

            ps.executeUpdate();

        } catch (Exception e) {

            e.printStackTrace();

        }

    }
}
