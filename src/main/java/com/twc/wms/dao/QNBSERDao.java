/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.dao;

import com.twc.wms.database.database;

import java.sql.*;

import java.util.ArrayList;

import java.util.List;

import com.twc.wms.entity.PGMU;
import com.twc.wms.entity.QIDETAIL;
import java.text.DecimalFormat;

import java.util.Calendar;

/**
 *
 * @author nutthawoot.noo
 */
public class QNBSERDao extends database {

    public boolean addLastXMS800(String wh) {

        boolean result = false;

        String sql = "UPDATE [QNBSER]\n"
                + "SET [QNBLAST] = [QNBLAST]+1\n"
                + "  WHERE [QNBTYPE] = REPLACE('" + wh + "','WH','W')\n"
                + "  AND [QNBGROUP] = FORMAT(CURRENT_TIMESTAMP,'MM')\n"
                + "  AND [QNBYEAR] = FORMAT(CURRENT_TIMESTAMP,'yyyy')";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean addLastXMS600() {

        boolean result = false;

        String sql = "UPDATE [QNBSER]\n"
                + "SET [QNBLAST] = [QNBLAST]+1\n"
                + "  WHERE [QNBTYPE] = 'LB'\n"
                + "  AND [QNBGROUP] = FORMAT(CURRENT_TIMESTAMP,'MM')\n"
                + "  AND [QNBYEAR] = FORMAT(CURRENT_TIMESTAMP,'yyyy')";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean addLastXMS400(String wh) {

        boolean result = false;

        String sql = "UPDATE [QNBSER]\n"
                + "SET [QNBLAST] = [QNBLAST]+1\n"
                + "  WHERE [QNBTYPE] = REPLACE('" + wh + "','WH','L')\n"
                + "  AND [QNBGROUP] = FORMAT(CURRENT_TIMESTAMP,'MM')\n"
                + "  AND [QNBYEAR] = FORMAT(CURRENT_TIMESTAMP,'yyyy')";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean editCurrent(String runno, String group, String year) {

        boolean result = false;

        String sql = "UPDATE QNBSER "
                + "SET QNBLAST = '" + runno + "' "
                + "WHERE QNBTYPE = 'LG' "
                + "AND QNBGROUP = '" + group + "' "
                + "AND QNBYEAR = '" + year + "' ";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

}
