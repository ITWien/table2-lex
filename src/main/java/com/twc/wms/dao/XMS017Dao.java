/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.dao;

import com.twc.wms.database.database;
import java.sql.*;

import java.util.ArrayList;

import java.util.List;

import com.twc.wms.entity.QRMMAS;
import com.twc.wms.entity.QSHEAD;

/**
 *
 * @author nutthawoot.noo
 */
public class XMS017Dao extends database {

    public List<QSHEAD> findByWhTranDate(String wh, String tranDateFrom, String tranDateTo) {

        List<QSHEAD> QSHEADList = new ArrayList<QSHEAD>();

        String sql = "SELECT [QSHWHS]\n"
                + "      ,[QSHTRDT]\n"
                + "      ,format([QSHTRDT],'yyyyMMdd') as [TRDT]\n"
                + "      ,[QSHGRID]\n"
                + "      ,isnull([QSHMTCTRL],'') as MATCTRL\n"
                + "      ,isnull(QSHQUEUE,'') as [QSHQUEUE]\n"
                + "      ,isnull([QSHMTCTRL]+' : '+(select top 1 [MATCNAME] from [MSSMATN] where [MSWHSE]=[QSHWHS] and [LGPBE]=[QSHMTCTRL]),'') as [QSHMTCTRL]\n"
                + "      ,isnull([QSHUSER]+' : '+(select top 1 [USERS] from [MSSUSER] where [USERID]=[QSHUSER]),'') as [QSHUSER]\n"
                + "      ,QSHUSER as [UID]\n"
                + "      ,(SELECT TOP 1\n"
                + "	     STUFF((SELECT distinct ':' +  (select top 1 QRMPARENT from QRMMAS where QRMID=[QSDID])\n"
                + "	            FROM [QSDETAIL]\n"
                + "                where [QSDGPID] = [QSHGRID]\n"
                + "	     FOR XML PATH('')), 1, 1, '')) as PIDS\n"
                + "  FROM [RMShipment].[dbo].[QSHEAD]\n"
                + "  WHERE [QSHWHS] = '" + wh + "'\n"
                + "  AND format([QSHTRDT],'yyyy-MM-dd') BETWEEN '" + tranDateFrom + "' AND '" + tranDateTo + "'";

        if (wh == null) {
            sql = "SELECT TOP 0 [QSHWHS]\n"
                    + "      ,[QSHTRDT]\n"
                    + "      ,[QSHGRID]\n"
                    + "      ,[QSHMTCTRL]\n"
                    + "      ,[QSHQUEUE]\n"
                    + "      ,[QSHUSER]\n"
                    + "  FROM [RMShipment].[dbo].[QSHEAD]";
        }
//        System.out.println(sql);
        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                QSHEAD p = new QSHEAD();

                p.setQSHWHS(result.getString("QSHWHS"));
                p.setQSHTRDT(result.getString("QSHTRDT"));
                p.setQSHGRID(result.getString("QSHGRID"));
                p.setQSHMTCTRL(result.getString("QSHMTCTRL"));
                p.setQSHQUEUE(result.getString("QSHQUEUE"));
                p.setQSHUSER(result.getString("QSHUSER"));

                String option = "";

                option = "<a style=\" width: 100%; cursor: pointer;\" onclick=\"toWMSdisplay('" + result.getString("QSHGRID") + "');\">\n"
                        + "<i class=\"fa fa-laptop\" style=\"font-size:30px;\"></i>\n"
                        + "</a>&nbsp;&nbsp;&nbsp;"
                        + "<a style=\" width: 100%; cursor: pointer;\" onclick=\"print('" + result.getString("QSHGRID") + "', '" + result.getString("TRDT") + "', '" + result.getString("MATCTRL") + "', '" + result.getString("UID") + "', '" + result.getString("PIDS") + "');\">\n"
                        + "<i class=\"fa fa-print\" style=\"font-size:30px;\"></i>\n"
                        + "</a>";

                p.setOption(option);

                QSHEADList.add(p);

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return QSHEADList;

    }

    public boolean insertParentQRMTRA(String id, String cdt, String remainQty, String parentPackType, String uid) {

        boolean result = false;

        String sql = "INSERT INTO QRMTRA \n"
                + "([QRMCOM]\n"
                + "      ,[QRMWHSE]\n"
                + "      ,[QRMCODE]\n"
                + "      ,[QRMVAL]\n"
                + "      ,[QRMPLANT]\n"
                + "      ,[QRMSTORAGE]\n"
                + "      ,[QRMID]\n"
                + "      ,[QRMROLL]\n"
                + "      ,[QRMTRDT]\n"
                + "      ,[QRMTRTM]\n"
                + "      ,[QRMMVT]\n"
                + "      ,[QRMTRT]\n"
                + "      ,[QRMDOCNO]\n"
                + "      ,[QRMSAPNO]\n"
                + "      ,[QRMQTY]\n"
                + "      ,[QRMBUN]\n"
                + "      ,[QRMCDT]\n"
                + "      ,[QRMUSER])\n"
                + "SELECT [QRMCOM]\n"
                + "      ,[QRMWHSE]\n"
                + "      ,[QRMCODE]\n"
                + "      ,[QRMVAL]\n"
                + "      ,[QRMPLANT]\n"
                + "      ,[QRMSTORAGE]\n"
                + "      ,[QRMID]\n"
                + "      ,[QRMROLL]\n"
                + "	  ,format(current_timestamp,'yyyy-MM-dd')\n"
                + "	  ,format(current_timestamp,'HH:mm:ss.fff')\n"
                + "	  ,'902'\n"
                + "	  ,'SPOT'\n"
                + "	  ,null\n"
                + "	  ,null\n"
                + "      ," + remainQty + " - [QRMQTY]\n"
                + "      ,[QRMBUN]\n"
                + "	  ,current_timestamp\n"
                + "      ,'" + uid + "'\n"
                + "  FROM [RMShipment].[dbo].[QRMMAS]\n"
                + "  where qrmid='" + id + "'\n"
                + "  and [QRMCDT]='" + cdt + "'";

        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean updateParentQRMMAS(String id, String cdt, String remainQty, String parentPackType, String uid) {

        boolean result = false;

        String sql = "UPDATE [RMShipment].[dbo].[QRMMAS]\n"
                + "SET QRMQTY = '" + remainQty + "'\n"
                + "   ,QRMALQTY = '" + remainQty + "'\n"
                + "   ,QRMPACKTYPE = '" + parentPackType + "'\n"
                + "   ,QRMEDT = CURRENT_TIMESTAMP\n"
                + "   ,QRMUSER = '" + uid + "'\n"
                + "  WHERE QRMID = '" + id + "'\n"
                + "  AND QRMCDT = '" + cdt + "'";

        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean insertQRMTRA(String id, String cdt, String qrmroll, String qrmqty, String qrmpacktype, String qrmid, String uid) {

        boolean result = false;

        String sql = "INSERT INTO QRMTRA \n"
                + "([QRMCOM]\n"
                + "      ,[QRMWHSE]\n"
                + "      ,[QRMCODE]\n"
                + "      ,[QRMVAL]\n"
                + "      ,[QRMPLANT]\n"
                + "      ,[QRMSTORAGE]\n"
                + "      ,[QRMID]\n"
                + "      ,[QRMROLL]\n"
                + "      ,[QRMTRDT]\n"
                + "      ,[QRMTRTM]\n"
                + "      ,[QRMMVT]\n"
                + "      ,[QRMTRT]\n"
                + "      ,[QRMDOCNO]\n"
                + "      ,[QRMSAPNO]\n"
                + "      ,[QRMQTY]\n"
                + "      ,[QRMBUN]\n"
                + "      ,[QRMCDT]\n"
                + "      ,[QRMUSER])\n"
                + "SELECT [QRMCOM]\n"
                + "      ,[QRMWHSE]\n"
                + "      ,[QRMCODE]\n"
                + "      ,[QRMVAL]\n"
                + "      ,[QRMPLANT]\n"
                + "      ,[QRMSTORAGE]\n"
                + "      ,'" + qrmid + "'\n"
                + "      ,'" + qrmroll + "'\n"
                + "	  ,format(current_timestamp,'yyyy-MM-dd')\n"
                + "	  ,format(current_timestamp,'HH:mm:ss.fff')\n"
                + "	  ,'901'\n"
                + "	  ,'SPIN'\n"
                + "	  ,null\n"
                + "	  ,null\n"
                + "      ,'" + qrmqty + "'\n"
                + "      ,[QRMBUN]\n"
                + "	  ,current_timestamp\n"
                + "      ,'" + uid + "'\n"
                + "  FROM [RMShipment].[dbo].[QRMMAS]\n"
                + "  where qrmid='" + id + "'\n"
                + "  and [QRMCDT]='" + cdt + "'";

        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean insertQRMMAS(String id, String cdt, String qrmroll, String qrmqty, String qrmpacktype, String qrmid, String uid) {

        boolean result = false;

        String sql = "INSERT INTO QRMMAS \n"
                + "([QRMCOM]\n"
                + "      ,[QRMID]\n"
                + "      ,[QRMCODE]\n"
                + "      ,[QRMVAL]\n"
                + "      ,[QRMROLL]\n"
                + "      ,[QRMIDTEMP]\n"
                + "      ,[QRMDESC]\n"
                + "      ,[QRMPO]\n"
                + "      ,[QRMTAX]\n"
                + "      ,[QRMPACKTYPE]\n"
                + "      ,[QRMQTY]\n"
                + "      ,[QRMALQTY]\n"
                + "      ,[QRMBUN]\n"
                + "      ,[QRMAUN]\n"
                + "      ,[QRMCVFAC]\n"
                + "      ,[QRMCVFQTY]\n"
                + "      ,[QRMSTS]\n"
                + "      ,[QRMPARENT]\n"
                + "      ,[QRMPDDATE]\n"
                + "      ,[QRMISDATE]\n"
                + "      ,[QRMRCDATE]\n"
                + "      ,[QRMTFDATE]\n"
                + "      ,[QRMLOTNO]\n"
                + "      ,[QRMDYNO]\n"
                + "      ,[QRMQIS]\n"
                + "      ,[QRMWHSE]\n"
                + "      ,[QRMZONE]\n"
                + "      ,[QRMPALLET]\n"
                + "      ,[QRMRACKNO]\n"
                + "      ,[QRMSIDE]\n"
                + "      ,[QRMROW]\n"
                + "      ,[QRMCOL]\n"
                + "      ,[QRMLOC]\n"
                + "      ,[QRMPLANT]\n"
                + "      ,[QRMSTORAGE]\n"
                + "      ,[QRMBARCODE]\n"
                + "      ,[QRMSUPCODE]\n"
                + "      ,[QRMEDT]\n"
                + "      ,[QRMCDT]\n"
                + "      ,[QRMUSER])\n"
                + "SELECT [QRMCOM]\n"
                + "      ,'" + qrmid + "'\n"
                + "      ,[QRMCODE]\n"
                + "      ,[QRMVAL]\n"
                + "      ,'" + qrmroll + "'\n"
                + "      ,[QRMIDTEMP]\n"
                + "      ,[QRMDESC]\n"
                + "      ,[QRMPO]\n"
                + "      ,[QRMTAX]\n"
                + "      ,'" + qrmpacktype + "'\n"
                + "      ,'" + qrmqty + "'\n"
                + "      ,'" + qrmqty + "'\n"
                + "      ,[QRMBUN]\n"
                + "      ,[QRMAUN]\n"
                + "      ,[QRMCVFAC]\n"
                + "      ,[QRMCVFQTY]\n"
                + "      ,[QRMSTS]\n"
                + "      ,'" + id + "'\n"
                + "      ,[QRMPDDATE]\n"
                + "      ,[QRMISDATE]\n"
                + "      ,[QRMRCDATE]\n"
                + "      ,[QRMTFDATE]\n"
                + "      ,[QRMLOTNO]\n"
                + "      ,[QRMDYNO]\n"
                + "      ,[QRMQIS]\n"
                + "      ,[QRMWHSE]\n"
                + "      ,[QRMZONE]\n"
                + "      ,[QRMPALLET]\n"
                + "      ,[QRMRACKNO]\n"
                + "      ,[QRMSIDE]\n"
                + "      ,[QRMROW]\n"
                + "      ,[QRMCOL]\n"
                + "      ,[QRMLOC]\n"
                + "      ,[QRMPLANT]\n"
                + "      ,[QRMSTORAGE]\n"
                + "      ,[QRMBARCODE]\n"
                + "      ,[QRMSUPCODE]\n"
                + "      ,current_timestamp\n"
                + "      ,current_timestamp\n"
                + "      ,'" + uid + "'\n"
                + "  FROM [RMShipment].[dbo].[QRMMAS]\n"
                + "  where qrmid='" + id + "'\n"
                + "  and [QRMCDT]='" + cdt + "'";

        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean updateGenRoll(String code, String len) {

        boolean result = false;

        String sql = "update [RMShipment].[dbo].[QGENRO]\n"
                + "  set [QGENDATE] = format(current_timestamp,'yyyy-MM-dd')\n"
                + "     ,[QGENROLL] = [QGENROLL] + " + len + "\n"
                + "  WHERE [QGENCODE] = '" + code + "'";

        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public String getGenRoll(String code) {

        String sql = "SELECT [QGENDATE]\n"
                + "      ,[QGENCODE]\n"
                + "      ,[QGENROLL]+1 AS QGENROLL\n"
                + "  FROM [RMShipment].[dbo].[QGENRO]\n"
                + "  WHERE [QGENCODE] = '" + code + "'";
//        System.out.println(sql);

        String last = "0";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                last = result.getString("QGENROLL");

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return last;

    }

    public String getDateByFormat(String format) {

        String sql = "SELECT FORMAT(CURRENT_TIMESTAMP,'" + format + "') AS [DATE]";
//        System.out.println(sql);

        String date = "";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                date = result.getString("DATE");

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return date;

    }

    public String getDate() {

        String sql = "SELECT FORMAT(CURRENT_TIMESTAMP,'yyMMdd') AS [DATE]";
//        System.out.println(sql);

        String date = "";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                date = result.getString("DATE");

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return date;

    }

    public String getMatCtrl(String code) {

        String sql = "SELECT TOP 1 [SAPMCTRL]\n"
                + "  FROM [RMShipment].[dbo].[SAPMAS]\n"
                + "  where [SAPMAT] = '" + code + "'";
//        System.out.println(sql);

        String mat = "";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                mat = result.getString("SAPMCTRL");

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return mat;

    }

    public String getLastQNBSER(String wh, String id) {

        String sql = "SELECT right(left('" + id + "',6),2) \n"
                + "       + format(CURRENT_TIMESTAMP,'yyMM')\n"
                + "       + format([QNBLAST]+1,'0000') as [LAST]\n"
                + "  FROM [RMShipment].[dbo].[QNBSER]\n"
                + "  where [QNBTYPE] = REPLACE('" + wh + "','WH','G')\n"
                + "  and [QNBGROUP] = format(CURRENT_TIMESTAMP,'MM')\n"
                + "  and [QNBYEAR] = format(CURRENT_TIMESTAMP,'yyyy')";
//        System.out.println(sql);

        String last = "0";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                last = result.getString("LAST");

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return last;

    }

    public String getGenQr(String id) {

        String sql = "SELECT [QGENDATE]\n"
                + "      ,[QGENSAPPGRP]\n"
                + "      ,cast([QGENID] as int)+1 as [QGENID]\n"
                + "  FROM [RMShipment].[dbo].[QGENQR]\n"
                + "  where [QGENSAPPGRP] = RIGHT(LEFT('" + id + "',6),2)\n"
                + "  and QGENDATE = FORMAT(CURRENT_TIMESTAMP,'yyyy-MM-dd')";
//        System.out.println(sql);

        String last = "0";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                last = result.getString("QGENID");

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return last;

    }

    public boolean updateGenQr(String id, String len) {

        boolean result = false;

        String sql = "update [RMShipment].[dbo].[QGENQR]\n"
                + "set [QGENID] = format(cast([QGENID] as int)+" + len + ",'00000')\n"
                + "  where [QGENSAPPGRP] = RIGHT(LEFT('" + id + "',6),2)\n"
                + "  and QGENDATE = FORMAT(CURRENT_TIMESTAMP,'yyyy-MM-dd')";

        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean updateQNBSER(String wh, String id) {

        boolean result = false;

        String sql = "update [RMShipment].[dbo].[QNBSER]\n"
                + "set [QNBLAST] = [QNBLAST]+1\n"
                + "    ,[QNBEDT] = CURRENT_TIMESTAMP\n"
                + "  where [QNBTYPE] = REPLACE('" + wh + "','WH','G')\n"
                + "  and [QNBGROUP] = format(CURRENT_TIMESTAMP,'MM')\n"
                + "  and [QNBYEAR] = format(CURRENT_TIMESTAMP,'yyyy')";

        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean insertQSDETAIL(String wh, String lino, String mat, String gid,
            String id, String bfQty, String afQty, String qty, String unit,
            String bfPack, String afPack, String uid) {

        boolean result = false;

        String sql = "INSERT INTO QSDETAIL ([QSDCOM]\n"
                + "      ,[QSDWHS]\n"
                + "      ,[QSDLINO]\n"
                + "      ,[QSDMAT]\n"
                + "      ,[QSDGPID]\n"
                + "      ,[QSDID]\n"
                + "      ,[QSDBFQTY]\n"
                + "      ,[QSDAFQTY]\n"
                + "      ,[QSDQTY]\n"
                + "      ,[QSDUM]\n"
                + "      ,[QSDBFPKG]\n"
                + "      ,[QSDAFPKG]\n"
                + "      ,[QSDEDT]\n"
                + "      ,[QSDCDT]\n"
                + "      ,[QSDUSER])\n"
                + "  VALUES ('TWC'"
                + ", '" + wh + "'"
                + ", '" + lino + "'"
                + ", '" + mat + "'"
                + ", '" + gid + "'"
                + ", '" + id + "'"
                + ", '" + bfQty + "'"
                + ", '" + afQty + "'"
                + ", '" + qty + "'"
                + ", '" + unit + "'"
                + ", '" + bfPack + "'"
                + ", '" + afPack + "'"
                + ", CURRENT_TIMESTAMP"
                + ", CURRENT_TIMESTAMP"
                + ", '" + uid + "')";
//        System.out.println(sql);

        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean insertQSHEAD(String gid, String wh, String mat, String uid) {

        boolean result = false;

        String sql = "INSERT INTO QSHEAD ([QSHCOM]\n"
                + "      ,[QSHWHS]\n"
                + "      ,[QSHTRDT]\n"
                + "      ,[QSHGRID]\n"
                + "      ,[QSHMTCTRL]\n"
                + "      ,[QSHQUEUE]\n"
                + "      ,[QSHSTS]\n"
                + "      ,[QSHEDT]\n"
                + "      ,[QSHCDT]\n"
                + "      ,[QSHUSER])\n"
                + "  VALUES ('TWC'"
                + ", '" + wh + "'"
                + ", CURRENT_TIMESTAMP"
                + ", '" + gid + "'"
                + ", '" + mat + "'"
                + ", null"
                + ", '2'"
                + ", CURRENT_TIMESTAMP"
                + ", CURRENT_TIMESTAMP"
                + ", '" + uid + "')";
//        System.out.println(sql);

        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean insertGenQr(String id) {

        boolean result = false;

        String sql = "INSERT INTO [QGENQR]\n"
                + "      ([QGENDATE]\n"
                + "      ,[QGENSAPPGRP]\n"
                + "      ,[QGENID])\n"
                + "  VALUES (FORMAT(CURRENT_TIMESTAMP,'yyyy-MM-dd')\n"
                + "         ,RIGHT(LEFT('" + id + "',6),2)\n"
                + "	    ,'00000')";

        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean checkLastQr(String id) {

        boolean check = false;

        String sql = "SELECT TOP 1 [QGENDATE]\n"
                + "      ,[QGENSAPPGRP]\n"
                + "      ,[QGENID]\n"
                + "  FROM [RMShipment].[dbo].[QGENQR]\n"
                + "  WHERE [QGENDATE] = FORMAT(CURRENT_TIMESTAMP,'yyyy-MM-dd')\n"
                + "  AND [QGENSAPPGRP] = RIGHT(LEFT('" + id + "',6),2)";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            if (result.next()) {

                check = true;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return check;

    }

    public QRMMAS findQSHEAD(String gid) {

        QRMMAS p = new QRMMAS();

        String sql = "SELECT [QSHGRID]\n"
                + "      ,format([QSHTRDT],'yyyyMMdd') as [QSHTRDT]\n"
                + "      ,[QSHMTCTRL]\n"
                + "  FROM [RMShipment].[dbo].[QSHEAD]\n"
                + "  WHERE [QSHGRID]='" + gid + "'";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                p.setQRMID(result.getString("QSHGRID"));
                p.setQRMCODE(result.getString("QSHTRDT"));
                p.setQRMVAL(result.getString("QSHMTCTRL"));

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return p;

    }

    public QRMMAS findMaster(String idcode, String type) {

        QRMMAS p = new QRMMAS();

        String sql = "SELECT [QRMID]\n"
                + "      ,[QRMCODE]\n"
                + "      ,[QRMVAL]\n"
                + "      ,[QRMROLL]\n"
                + "      ,[QRMIDTEMP]\n"
                + "      ,[QRMDESC]\n"
                + "      ,[QRMPO]\n"
                + "      ,[QRMTAX]\n"
                + "      ,[QRMPACKTYPE]\n"
                + "      ,format(QRMQTY,'#0.000') as [QRMQTY]\n"
                + "      ,format(QRMALQTY,'#0.000') as [QRMALQTY]\n"
                + "      ,[QRMBUN]\n"
                + "      ,[QRMAUN]\n"
                + "      ,format(QRMCVFAC,'#0.000') as [QRMCVFAC]\n"
                + "      ,format(QRMCVFQTY,'#0.000') as [QRMCVFQTY]\n"
                + "      ,[QRMSTS]\n"
                + "      ,[QRMPARENT]\n"
                + "      ,[QRMPDDATE]\n"
                + "      ,[QRMISDATE]\n"
                + "      ,[QRMRCDATE]\n"
                + "      ,[QRMTFDATE]\n"
                + "      ,[QRMLOTNO]\n"
                + "      ,[QRMDYNO]\n"
                + "      ,[QRMQIS]\n"
                + "      ,[QRMWHSE]\n"
                + "      ,[QRMZONE]\n"
                + "      ,[QRMPALLET]\n"
                + "      ,[QRMRACKNO]\n"
                + "      ,[QRMSIDE]\n"
                + "      ,[QRMROW]\n"
                + "      ,[QRMCOL]\n"
                + "      ,[QRMLOC]\n"
                + "      ,[QRMPLANT]\n"
                + "      ,[QRMSTORAGE]\n"
                + "      ,[QRMBARCODE]\n"
                + "      ,[QRMSUPCODE]\n"
                + "      ,[QRMEDT]\n"
                + "      ,[QRMCDT]\n"
                + "      ,[QRMUSER]\n"
                + "      ,(SELECT TOP 1 [SAPMCTRL]+' : '+[SAPMCTRLDESC] FROM [SAPMAS] WHERE [SAPMAT] = [QRMCODE]) AS [MATCTRL]\n"
                + "FROM QRMMAS \n"
                + "WHERE " + type + " = '" + idcode + "' \n";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                p.setQRMID(result.getString("QRMID"));
                p.setQRMCODE(result.getString("QRMCODE"));
                p.setQRMVAL(result.getString("QRMVAL"));
                p.setQRMROLL(result.getString("QRMROLL"));
                p.setQRMIDTEMP(result.getString("QRMIDTEMP"));
                p.setQRMDESC(result.getString("QRMDESC"));
                p.setQRMPO(result.getString("QRMPO"));
                p.setQRMTAX(result.getString("QRMTAX"));
                p.setQRMPACKTYPE(result.getString("QRMPACKTYPE"));
                p.setQRMQTY(result.getString("QRMQTY"));
                p.setQRMALQTY(result.getString("QRMALQTY"));
                p.setQRMBUN(result.getString("QRMBUN"));
                p.setQRMAUN(result.getString("QRMAUN"));
                p.setQRMCVFAC(result.getString("QRMCVFAC"));
                p.setQRMCVFQTY(result.getString("QRMCVFQTY"));
                p.setQRMSTS(result.getString("QRMSTS"));
                p.setQRMPARENT(result.getString("QRMPARENT"));
                p.setQRMPDDATE(result.getString("QRMPDDATE"));
                p.setQRMISDATE(result.getString("QRMISDATE"));
                p.setQRMRCDATE(result.getString("QRMRCDATE"));
                p.setQRMTFDATE(result.getString("QRMTFDATE"));
                p.setQRMLOTNO(result.getString("QRMLOTNO"));
                p.setQRMDYNO(result.getString("QRMDYNO"));
                p.setQRMQIS(result.getString("QRMQIS"));
                p.setQRMWHSE(result.getString("QRMWHSE"));
                p.setQRMZONE(result.getString("QRMZONE"));
                p.setQRMPALLET(result.getString("QRMPALLET"));
                p.setQRMRACKNO(result.getString("QRMRACKNO"));
                p.setQRMSIDE(result.getString("QRMSIDE"));
                p.setQRMROW(result.getString("QRMROW"));
                p.setQRMCOL(result.getString("QRMCOL"));
                p.setQRMLOC(result.getString("QRMLOC"));
                p.setQRMPLANT(result.getString("QRMPLANT"));
                p.setQRMSTORAGE(result.getString("QRMSTORAGE"));
                p.setQRMBARCODE(result.getString("QRMBARCODE"));
                p.setQRMSUPCODE(result.getString("QRMSUPCODE"));
                p.setQRMEDT(result.getString("QRMEDT"));
                p.setQRMCDT(result.getString("QRMCDT"));
                p.setQRMUSER(result.getString("QRMUSER"));
                p.setMATCTRL(result.getString("MATCTRL"));

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return p;

    }

}
