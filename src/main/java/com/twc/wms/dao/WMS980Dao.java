/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.dao;

import com.twc.wms.database.database;

import java.sql.*;

import java.util.ArrayList;

import java.util.List;

import com.twc.wms.entity.WMS980;
import com.twc.wms.entity.SAPMAS;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import com.twc.wms.database.MSSQLDbConnectionPool;
import com.twc.wms.entity.Qdest;

/**
 *
 * @author nutthawoot.noo
 */
public class WMS980Dao extends database {

    public List<WMS980> findDetail(String[] selectCheck, String whd, String detail, String plant) {

        List<WMS980> objList = new ArrayList<WMS980>();

        String ohDateList = "";
        String ohDateListYM = "";
        String ohDateListY = "";
        if (selectCheck != null) {
            for (int i = 0; i < selectCheck.length; i++) {
                if (i == 0) {
                    ohDateList += "'" + selectCheck[i] + "'";
                    ohDateListYM += "'" + selectCheck[i].substring(0, 7) + "'";
                    ohDateListY += "'" + selectCheck[i].substring(0, 4) + "'";
                } else {
                    ohDateList += ",'" + selectCheck[i] + "'";
                    ohDateListYM += ",'" + selectCheck[i].substring(0, 7) + "'";
                    ohDateListY += ",'" + selectCheck[i].substring(0, 4) + "'";
                }
            }
        } else {
            ohDateList = "''";
            ohDateListYM = "''";
            ohDateListY = "''";
        }

        String WHD = "";
        if (whd.equals("All")) {
            WHD = "--";
        }

        String PNT = "";
        if (plant.equals("ALL")) {
            PNT = "--";
        }

        String DETAIL = "";
        String DETAIL2 = "";
        if (detail == null) {
            DETAIL = "";
            DETAIL2 = "";
        } else {
            DETAIL = "--";
            DETAIL2 = "TOP 0";
        }

        String sql = "DECLARE @ALL NVARCHAR(10) = '" + whd + "'\n"
                + "DECLARE @PLANT NVARCHAR(10) = '" + plant + "'\n"
                + "SELECT WH,DEPT,PRODUCT,SUBNAME,NAME AS GRPNAME, SUBNAME AS NAME\n"
                + ",(SELECT ISNULL(COUNT(DISTINCT [QRMCODE]),0)\n"
                + "  FROM [QRMTRA] JOIN SAPMAS ON QRMCODE=SAPMAT AND QRMPLANT = SAPPLANT AND QRMVAL = SAPVAL\n"
                + "  WHERE [QRMWHSE] = WH\n"
                + "  " + PNT + "AND [QRMPLANT] = @PLANT\n"
                + "  AND SAPPGRP = PRODUCT\n"
                + "  AND FORMAT([QRMTRDT],'yyyy-MM') IN (" + ohDateListYM + ")) AS MVTSKU\n"
                + ",(SELECT ISNULL(SUM(SAPTOTAL * SAPAVG),0)\n"
                + "FROM [SAPONH] \n"
                + "WHERE [SAPWHS] = WH \n"
                + "AND LEFT([SAPMAT],2) = PRODUCT \n"
                + "AND FORMAT([SAPTRDT],'####-##-##') IN (" + ohDateList + ")) AS AMOUNT\n"
                + ",(SELECT ISNULL(COUNT(DISTINCT [QRMID]+[QRMCODE]),0)\n"
                + "  FROM [QRMMAS] JOIN SAPMAS ON QRMCODE=SAPMAT AND QRMPLANT = SAPPLANT AND QRMVAL = SAPVAL\n"
                + "  WHERE [QRMSTS] = '2'\n"
                + "  AND [QRMWHSE] = WH\n"
                + "  " + PNT + "AND [QRMPLANT] = @PLANT\n"
                + "  AND SAPPGRP = PRODUCT\n"
                + "  --AND FORMAT([QRMCDT],'yyyy-MM') IN (" + ohDateListYM + ")\n) AS PACKPCS\n"
                + ",(SELECT ISNULL(COUNT([CHKID]),0)\n"
                + "  FROM [TABCHK]\n"
                + "  WHERE [CHKWH] = WH\n"
                + "  AND LEFT([CHKMATCODE],2) = PRODUCT\n"
                + "  AND FORMAT([CHKONHDATE],'yyyy-MM-dd') IN (" + ohDateList + ")) AS PACKPCS2\n"
                + ",(SELECT ISNULL(SUM(CHKQTY * SAPAVG),0)\n"
                + "FROM(\n"
                + "SELECT [CHKMATCODE], [CHKONHAND], SUM([CHKQTY]) AS CHKQTY\n"
                + "FROM [TABCHK] \n"
                + "WHERE [CHKWH] = WH \n"
                + "AND LEFT([CHKMATCODE],2) = PRODUCT\n"
                + "AND FORMAT([CHKONHDATE],'yyyy-MM-dd') IN (" + ohDateList + ") \n"
                + "GROUP BY [CHKMATCODE],[CHKONHAND]\n"
                + ")PP\n"
                + "full join SAPONH on SAPMAT = [CHKMATCODE] and FORMAT([SAPTRDT],'####-##-##') IN (" + ohDateList + ")\n"
                + "WHERE [CHKONHAND] != [CHKQTY]) AS AMOUNT2\n"
                + ",OH_SKU,CK_SKU\n"
                + ",CAST(((CAST(CK_SKU AS DECIMAL(18,6))/CAST((CASE WHEN OH_SKU = 0 THEN 1 ELSE OH_SKU END) AS DECIMAL(18,6)))*100) AS DECIMAL(18,3)) AS CK_SKU_PER\n"
                + ",COL_SKU\n"
                + ",CAST(((CAST(COL_SKU AS DECIMAL(18,6))/CAST((CASE WHEN OH_SKU = 0 THEN 1 ELSE OH_SKU END) AS DECIMAL(18,6)))*100) AS DECIMAL(18,3)) AS COL_SKU_PER\n"
                + ",(SELECT COUNT(*)\n"
                + "FROM(\n"
                + "SELECT [CHKMATCODE], [CHKONHAND], SUM([CHKQTY]) AS CHKQTY\n"
                + "FROM [TABCHK] \n"
                + "WHERE [CHKWH] = WH \n"
                + "AND LEFT([CHKMATCODE],2) = PRODUCT\n"
                + "AND FORMAT([CHKONHDATE],'yyyy-MM-dd') IN (" + ohDateList + ") \n"
                + "GROUP BY [CHKMATCODE],[CHKONHAND]\n"
                + ")PP\n"
                + "WHERE [CHKONHAND] != [CHKQTY]) AS DIFF_SKU\n"
                + ",CAST(((CAST((SELECT COUNT(*)\n"
                + "FROM(\n"
                + "SELECT [CHKMATCODE], [CHKONHAND], SUM([CHKQTY]) AS CHKQTY\n"
                + "FROM [TABCHK] \n"
                + "WHERE [CHKWH] = WH \n"
                + "AND LEFT([CHKMATCODE],2) = PRODUCT\n"
                + "AND FORMAT([CHKONHDATE],'yyyy-MM-dd') IN (" + ohDateList + ") \n"
                + "GROUP BY [CHKMATCODE],[CHKONHAND]\n"
                + ")PP\n"
                + "WHERE [CHKONHAND] != [CHKQTY]) AS DECIMAL(18,6))/CAST((CASE WHEN OH_SKU = 0 THEN 1 ELSE OH_SKU END) AS DECIMAL(18,6)))*100) AS DECIMAL(18,3)) AS DIFF_SKU_PER\n"
                + "FROM(\n"
                + "SELECT WH,DEPT,PRODUCT,SUBNAME,NAME\n"
                + ",(SELECT CASE WHEN COUNT(SAPMAT+SAPVAL) = 0 THEN 0 ELSE COUNT(SAPMAT+SAPVAL) END\n"
                + "  FROM [RMShipment].[dbo].[SAPONH]\n"
                + "  where [SAPWHS]=WH\n"
                + "      and FORMAT([SAPTRDT],'####-##-##') IN (" + ohDateList + ")\n"
                + "      AND LEFT(SAPMAT,2) = PRODUCT ) AS OH_SKU\n"
                + ",(SELECT count(distinct CHKMATCODE)\n"
                + "FROM [TABCHK] \n"
                + "WHERE [CHKWH] = WH\n"
                + "AND LEFT([CHKMATCODE],2) = PRODUCT AND FORMAT([CHKONHDATE],'yyyy-MM-dd') IN (" + ohDateList + ")) AS CK_SKU\n"
                + ",(SELECT COUNT(*)\n"
                + "FROM(\n"
                + "SELECT [CHKMATCODE], [CHKONHAND], SUM([CHKQTY]) AS CHKQTY\n"
                + "FROM [TABCHK] \n"
                + "WHERE [CHKWH] = WH \n"
                + "AND LEFT([CHKMATCODE],2) = PRODUCT\n"
                + "AND FORMAT([CHKONHDATE],'yyyy-MM-dd') IN (" + ohDateList + ") \n"
                + "GROUP BY [CHKMATCODE],[CHKONHAND]\n"
                + ")PP\n"
                + "WHERE [CHKONHAND] = [CHKQTY]) AS COL_SKU\n"
                + "FROM(\n"
                + "SELECT [CHKWH] AS WH\n"
                + ",(SELECT TOP 1 [QWHDEPT] FROM [QWHSTR] WHERE [QWHCOD] = [CHKWH] AND [QWHMGRP] = [CHKMATCODE]) AS DEPT\n"
                + ",[CHKMATCODE] AS PRODUCT\n"
                + ",(SELECT TOP 1 [QWHSUBPROD] FROM [QWHSTR] WHERE [QWHCOD] = [CHKWH] AND [QWHMGRP] = [CHKMATCODE]) AS SUBNAME\n"
                + ",(SELECT TOP 1 [QWHPROD] FROM [QWHSTR] WHERE [QWHCOD] = [CHKWH] AND [QWHMGRP] = [CHKMATCODE]) AS NAME\n"
                + "FROM(\n"
                + "SELECT DISTINCT [CHKWH],LEFT([CHKMATCODE],2) AS [CHKMATCODE]\n"
                + "FROM [TABCHK]\n"
                + "WHERE FORMAT([CHKONHDATE],'yyyy-MM-dd') IN (" + ohDateList + ")\n"
                + "" + PNT + "AND [CHKPLANT] = @PLANT \n"
                + DETAIL + "AND LEFT([CHKMATCODE],1) NOT IN ('3','4')\n"
                + ")TMP)TMP2)TMP3\n"
                + WHD + "WHERE (WH = @ALL OR DEPT = @ALL)\n"
                + "UNION ALL--///////////////////////////////////////////\n"
                + "SELECT " + DETAIL2 + " WH,DEPT,PRODUCT+'*' AS PRODUCT,SUBNAME,NAME AS GRPNAME, NAME\n"
                + ",(SELECT ISNULL(COUNT(DISTINCT [QRMCODE]),0)\n"
                + "  FROM [QRMTRA] JOIN SAPMAS ON QRMCODE=SAPMAT AND QRMPLANT = SAPPLANT AND QRMVAL = SAPVAL\n"
                + "  WHERE [QRMWHSE] = WH\n"
                + "  " + PNT + "AND [QRMPLANT] = @PLANT\n"
                + "  AND SAPPGRP = PRODUCT\n"
                + "  AND FORMAT([QRMTRDT],'yyyy-MM') IN (" + ohDateListYM + ")) AS MVTSKU\n"
                + ",(SELECT ISNULL(SUM(SAPTOTAL * SAPAVG),0)\n"
                + "FROM [SAPONH] \n"
                + "WHERE [SAPWHS] = WH \n"
                + "AND LEFT([SAPMAT],2) = PRODUCT \n"
                + "AND FORMAT([SAPTRDT],'####-##-##') IN (" + ohDateList + ")) AS AMOUNT\n"
                + ",(SELECT ISNULL(COUNT(DISTINCT [QRMID]+[QRMCODE]),0)\n"
                + "  FROM [QRMMAS] JOIN SAPMAS ON QRMCODE=SAPMAT AND QRMPLANT = SAPPLANT AND QRMVAL = SAPVAL\n"
                + "  WHERE [QRMSTS] = '2'\n"
                + "  AND [QRMWHSE] = WH\n"
                + "  " + PNT + "AND [QRMPLANT] = @PLANT\n"
                + "  AND SAPPGRP = PRODUCT\n"
                + "  --AND FORMAT([QRMCDT],'yyyy-MM') IN (" + ohDateListYM + ")\n) AS PACKPCS\n"
                + ",(SELECT ISNULL(COUNT([CHKID]),0)\n"
                + "  FROM [TABCHK]\n"
                + "  WHERE [CHKWH] = WH\n"
                + "  AND LEFT([CHKMATCODE],1) = PRODUCT\n"
                + "  AND FORMAT([CHKONHDATE],'yyyy-MM-dd') IN (" + ohDateList + ")) AS PACKPCS2\n"
                + ",(SELECT ISNULL(SUM(CHKQTY * SAPAVG),0)\n"
                + "FROM(\n"
                + "SELECT [CHKMATCODE], [CHKONHAND], SUM([CHKQTY]) AS CHKQTY\n"
                + "FROM [TABCHK] \n"
                + "WHERE [CHKWH] = WH \n"
                + "AND LEFT([CHKMATCODE],1) = PRODUCT\n"
                + "AND FORMAT([CHKONHDATE],'yyyy-MM-dd') IN (" + ohDateList + ") \n"
                + "GROUP BY [CHKMATCODE],[CHKONHAND]\n"
                + ")PP\n"
                + "full join SAPONH on SAPMAT = [CHKMATCODE] and FORMAT([SAPTRDT],'####-##-##') IN (" + ohDateList + ")\n"
                + "WHERE [CHKONHAND] != [CHKQTY]) AS AMOUNT2\n"
                + ",OH_SKU,CK_SKU\n"
                + ",CAST(((CAST(CK_SKU AS DECIMAL(18,6))/CAST((CASE WHEN OH_SKU = 0 THEN 1 ELSE OH_SKU END) AS DECIMAL(18,6)))*100) AS DECIMAL(18,3)) AS CK_SKU_PER\n"
                + ",COL_SKU\n"
                + ",CAST(((CAST(COL_SKU AS DECIMAL(18,6))/CAST((CASE WHEN OH_SKU = 0 THEN 1 ELSE OH_SKU END) AS DECIMAL(18,6)))*100) AS DECIMAL(18,3)) AS COL_SKU_PER\n"
                + ",(SELECT COUNT(*)\n"
                + "FROM(\n"
                + "SELECT [CHKMATCODE], [CHKONHAND], SUM([CHKQTY]) AS CHKQTY\n"
                + "FROM [TABCHK] \n"
                + "WHERE [CHKWH] = WH \n"
                + "AND LEFT([CHKMATCODE],1) = PRODUCT\n"
                + "AND FORMAT([CHKONHDATE],'yyyy-MM-dd') IN (" + ohDateList + ") \n"
                + "GROUP BY [CHKMATCODE],[CHKONHAND]\n"
                + ")PP\n"
                + "WHERE [CHKONHAND] != [CHKQTY]) AS DIFF_SKU\n"
                + ",CAST(((CAST((SELECT COUNT(*)\n"
                + "FROM(\n"
                + "SELECT [CHKMATCODE], [CHKONHAND], SUM([CHKQTY]) AS CHKQTY\n"
                + "FROM [TABCHK] \n"
                + "WHERE [CHKWH] = WH \n"
                + "AND LEFT([CHKMATCODE],1) = PRODUCT\n"
                + "AND FORMAT([CHKONHDATE],'yyyy-MM-dd') IN (" + ohDateList + ") \n"
                + "GROUP BY [CHKMATCODE],[CHKONHAND]\n"
                + ")PP\n"
                + "WHERE [CHKONHAND] != [CHKQTY]) AS DECIMAL(18,6))/CAST((CASE WHEN OH_SKU = 0 THEN 1 ELSE OH_SKU END) AS DECIMAL(18,6)))*100) AS DECIMAL(18,3)) AS DIFF_SKU_PER\n"
                + "FROM(\n"
                + "SELECT WH,DEPT,PRODUCT,SUBNAME,NAME\n"
                + ",(SELECT CASE WHEN COUNT(SAPMAT+SAPVAL) = 0 THEN 0 ELSE COUNT(SAPMAT+SAPVAL) END\n"
                + "  FROM [RMShipment].[dbo].[SAPONH]\n"
                + "  where [SAPWHS]=WH\n"
                + "      and FORMAT([SAPTRDT],'####-##-##') IN (" + ohDateList + ")\n"
                + "      AND LEFT(SAPMAT,1) = PRODUCT ) AS OH_SKU\n"
                + ",(SELECT count(distinct CHKMATCODE)\n"
                + "FROM [TABCHK] \n"
                + "WHERE [CHKWH] = WH\n"
                + "AND LEFT([CHKMATCODE],1) = PRODUCT AND FORMAT([CHKONHDATE],'yyyy-MM-dd') IN (" + ohDateList + ")) AS CK_SKU\n"
                + ",(SELECT COUNT(*)\n"
                + "FROM(\n"
                + "SELECT [CHKMATCODE], [CHKONHAND], SUM([CHKQTY]) AS CHKQTY\n"
                + "FROM [TABCHK] \n"
                + "WHERE [CHKWH] = WH \n"
                + "AND LEFT([CHKMATCODE],1) = PRODUCT\n"
                + "AND FORMAT([CHKONHDATE],'yyyy-MM-dd') IN (" + ohDateList + ") \n"
                + "GROUP BY [CHKMATCODE],[CHKONHAND]\n"
                + ")PP\n"
                + "WHERE [CHKONHAND] = [CHKQTY]) AS COL_SKU\n"
                + "FROM(\n"
                + "SELECT [CHKWH] AS WH\n"
                + ",(SELECT TOP 1 [QWHDEPT] FROM [QWHSTR] WHERE [QWHCOD] = [CHKWH] AND LEFT([QWHMGRP],1) = [CHKMATCODE]) AS DEPT\n"
                + ",[CHKMATCODE] AS PRODUCT\n"
                + ",(SELECT TOP 1 [QWHSUBPROD] FROM [QWHSTR] WHERE [QWHCOD] = [CHKWH] AND LEFT([QWHMGRP],1) = [CHKMATCODE]) AS SUBNAME\n"
                + ",(SELECT TOP 1 [QWHPROD] FROM [QWHSTR] WHERE [QWHCOD] = [CHKWH] AND LEFT([QWHMGRP],1) = [CHKMATCODE]) AS NAME\n"
                + "FROM(\n"
                + "SELECT DISTINCT [CHKWH],LEFT([CHKMATCODE],1) AS [CHKMATCODE]\n"
                + "FROM [TABCHK]\n"
                + "WHERE FORMAT([CHKONHDATE],'yyyy-MM-dd') IN (" + ohDateList + ")\n"
                + "" + PNT + "AND [CHKPLANT] = @PLANT \n"
                + "AND LEFT([CHKMATCODE],1) IN ('3','4')\n"
                + ")TMP)TMP2)TMP3\n"
                + WHD + "WHERE (WH = @ALL OR DEPT = @ALL)\n"
                + "ORDER BY WH,DEPT,PRODUCT";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                WMS980 p = new WMS980();

                p.setWH(result.getString("WH"));
                p.setDEPT(result.getString("DEPT"));
                p.setPRODUCT(result.getString("PRODUCT"));
                p.setNAME(result.getString("NAME"));
                p.setOH_SKU(result.getString("OH_SKU"));
                p.setCK_SKU(result.getString("CK_SKU"));
                p.setCK_SKU_PER(result.getString("CK_SKU_PER"));
                p.setCOL_SKU(result.getString("COL_SKU"));
                p.setCOL_SKU_PER(result.getString("COL_SKU_PER"));
                p.setDIFF_SKU(result.getString("DIFF_SKU"));
                p.setDIFF_SKU_PER(result.getString("DIFF_SKU_PER"));
                p.setAMOUNT(result.getString("AMOUNT"));
                p.setPACKPCS(result.getString("PACKPCS"));
                p.setAMOUNT2(result.getString("AMOUNT2"));
                p.setPACKPCS2(result.getString("PACKPCS2"));
                p.setMVTSKU(result.getString("MVTSKU"));

                objList.add(p);
            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return objList;

    }

    public List<WMS980> findDept() {

        List<WMS980> objList = new ArrayList<WMS980>();

        String sql = "SELECT DISTINCT [QWHDEPT]\n"
                + "  FROM [RMShipment].[dbo].[QWHSTR]\n"
                + "  ORDER BY [QWHDEPT]";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                WMS980 p = new WMS980();

                p.setDEPT(result.getString("QWHDEPT"));

                objList.add(p);
            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return objList;

    }

    public List<WMS980> findWH() {

        List<WMS980> objList = new ArrayList<WMS980>();

        String sql = "SELECT DISTINCT [QWHCOD]\n"
                + "  FROM [RMShipment].[dbo].[QWHSTR]\n"
                + "  ORDER BY [QWHCOD]";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                WMS980 p = new WMS980();

                p.setWH(result.getString("QWHCOD"));

                objList.add(p);
            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return objList;

    }

    public List<WMS980> findOHDate() {

        List<WMS980> objList = new ArrayList<WMS980>();

        String sql = "SELECT DISTINCT FORMAT([QTCOHDT],'yyyy-MM-dd') AS [QTCOHDT]\n"
                + "  FROM [RMShipment].[dbo].[QTCKSTK]\n"
                + "  ORDER BY [QTCOHDT] DESC";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                WMS980 p = new WMS980();

                p.setOH_SKU(result.getString("QTCOHDT"));

                objList.add(p);
            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return objList;

    }
}
