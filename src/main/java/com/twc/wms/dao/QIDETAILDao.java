/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.dao;

import com.twc.wms.database.MSSQLDbConnectionPool;
import com.twc.wms.database.database;

import java.sql.*;

import java.util.ArrayList;

import java.util.List;

import com.twc.wms.entity.MSSMATN;

import com.twc.wms.entity.QIDETAIL;
import com.twc.wms.entity.UserAuth;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;

/**
 *
 * @author nutthawoot.noo
 */
public class QIDETAILDao extends database {

    public String findPLANT(String qno) {

        String p = "";

        String sql = "SELECT TOP 1 QIDID, \n"
                + "(select top 1 QRMPLANT from QRMMAS where QRMID = QIDID) as PLANT\n"
                + "FROM QIDETAIL\n"
                + "WHERE QIDQNO = '" + qno + "'";

        ResultSet result = null;

        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            result = ps.executeQuery();

            while (result.next()) {

                p = result.getString("PLANT");

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return p;

    }

    public ResultSet findForPrintWMS340(String[] id, String start) throws SQLException {

        StringBuilder list = new StringBuilder();
        for (int i = 0; i < id.length; i++) {
            if (!id[i].split("-")[3].equals(" ")) {
                if (i == id.length - 1) {
                    list.append("SELECT ").append(id[i].split("-")[3]).append(" as QIDRUNNO, '").append(id[i].split("-")[2]).append("' as QIDSHDT ORDER BY QIDRUNNO ");
                } else {
                    list.append("SELECT ").append(id[i].split("-")[3]).append(" as QIDRUNNO, '").append(id[i].split("-")[2]).append("' as QIDSHDT UNION ALL \n");
                }
            }
        }

        StringBuilder dummy = new StringBuilder();
        if (start != null && !start.equals("")) {
            int st = Integer.parseInt(start);

            for (int i = 1; i < st; i++) {
                dummy.append("SELECT null as QIDRUNNO, null as QIDSHDT UNION ALL \n");
            }
        }

        String sql = dummy + "\n" + list;
        ResultSet result = null;

        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            result = ps.executeQuery();
        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public ResultSet findForPrintWMS350(String[] id, String wh, String dest, String shipDate) throws SQLException {

        StringBuilder list = new StringBuilder();
        for (int i = 0; i < id.length; i++) {
            if (i == 0) {
                if (id[i].split("-")[6].equals("null")) {
                    list.append("'0'");
                }
            }
            if (!id[i].split("-")[6].equals("null")) {
                if (!id[i].split("-")[3].equals(" ")) {
                    if (i == 0) {
                        list.append("'").append(id[i].split("-")[3]).append("'");
                    } else {
                        list.append(",'").append(id[i].split("-")[3]).append("'");
                    }
                }
            }
        }

//        String sql = "SELECT QIDROUND, MATCTRL, QIDMAT, QIDDESC, QIDPACKTYPE, \n"
//                + "CASE WHEN BAG = 1 THEN COUNT(*) ELSE 0 END AS BAG, \n"
//                + "CASE WHEN ROLL = 1 THEN COUNT(*) ELSE 0 END AS ROLL, \n"
//                + "CASE WHEN BOX = 1 THEN COUNT(*) ELSE 0 END AS BOX, \n"
//                + "QIDQNO, QIDRUNNO, QIDGPQR, MVT, QIDOCNO\n"
//                + "FROM(\n"
//                + "SELECT QIDROUND,\n"
//                + "       (select top 1 QIHMTCTRL from QIHEAD where QIHQNO = QIDQNO) AS MATCTRL,\n"
//                + "       QIDMAT, (select top 1 QRMDESC from QRMMAS where QRMCODE = QIDMAT) AS QIDDESC,\n"
//                + "	   QIDPACKTYPE,\n"
//                + "	   CASE WHEN QIDPACKTYPE = 'BAG' THEN 1 WHEN QIDPACKTYPE = 'PACK' THEN 1 ELSE 0 END AS BAG,\n"
//                + "	   CASE WHEN QIDPACKTYPE = 'ROLL' THEN 1 ELSE 0 END AS ROLL,\n"
//                + "	   CASE WHEN QIDPACKTYPE = 'BOX' THEN 1 ELSE 0 END AS BOX,\n"
//                + "       QIDQNO, QIDRUNNO, QIDGPQR,\n"
//                + "	   (select top 1 QIHMVT from QIHEAD where QIHQNO = QIDQNO) AS MVT,\n"
//                + "	   QIDOCNO\n"
//                + "FROM QIDETAIL\n"
//                + "WHERE (select top 1 QIHDEST from QIHEAD where QIHQNO = QIDQNO) = '" + dest + "'\n"
//                + "AND QIDWHS = '" + wh + "'\n"
//                + "AND QIDSHDT = '" + shipDate + "'\n"
//                + "AND QIDRUNNO IN (" + list + ")\n"
//                + ") TMP\n"
//                + "GROUP BY QIDROUND, MATCTRL, QIDMAT, QIDDESC, QIDPACKTYPE, BAG, ROLL, BOX, QIDQNO, QIDRUNNO, QIDGPQR, MVT, QIDOCNO\n"
//                + "ORDER BY QIDROUND, MATCTRL, QIDGPQR, QIDMAT, QIDQNO, MVT, QIDOCNO ";
        String sql = "SELECT QIDROUND, MATCTRL, QIDMAT, QIDDESC, QIDPACKTYPE, \n"
                + "CASE WHEN BAG = 1 THEN COUNT(*) ELSE 0 END AS BAG, \n"
                + "CASE WHEN ROLL = 1 THEN COUNT(*) ELSE 0 END AS ROLL, \n"
                + "CASE WHEN BOX = 1 THEN COUNT(*) ELSE 0 END AS BOX, \n"
                + "QIDQNO, \n"
                + "(SELECT TOP 1 QIDRUNNO \n"
                + "FROM QIDETAIL DD\n"
                + "LEFT JOIN QIHEAD HH ON HH.QIHQNO = DD.QIDQNO\n"
                + "LEFT JOIN QRMMAS MM ON MM.QRMCODE = DD.QIDMAT\n"
                + "WHERE QIDROUND = TMP.QIDROUND\n"
                + "AND HH.QIHMTCTRL = TMP.MATCTRL\n"
                + "AND QIDMAT = TMP.QIDMAT\n"
                + "AND MM.QRMDESC = TMP.QIDDESC\n"
                + "AND QIDPACKTYPE = TMP.QIDPACKTYPE\n"
                + "AND QIDQNO = TMP.QIDQNO\n"
                + "AND HH.QIHMVT = TMP.MVT\n"
                + ") AS QIDRUNNO, \n"
                + "QIDGPQR, MVT, QIDOCNO\n"
                + "FROM(\n"
                + "SELECT QIDROUND,\n"
                + "       (select top 1 QIHMTCTRL from QIHEAD where QIHQNO = QIDQNO) AS MATCTRL,\n"
                + "       QIDMAT, (select top 1 QRMDESC from QRMMAS where QRMCODE = QIDMAT) AS QIDDESC,\n"
                + "	   QIDPACKTYPE,\n"
                + "	   CASE WHEN QIDPACKTYPE = 'BAG' THEN 1 WHEN QIDPACKTYPE = 'PACK' THEN 1 ELSE 0 END AS BAG,\n"
                + "	   CASE WHEN QIDPACKTYPE = 'ROLL' THEN 1 ELSE 0 END AS ROLL,\n"
                + "	   CASE WHEN QIDPACKTYPE = 'BOX' THEN 1 ELSE 0 END AS BOX,\n"
                + "       QIDQNO, QIDGPQR,\n"
                + "	   (select top 1 QIHMVT from QIHEAD where QIHQNO = QIDQNO) AS MVT, \n"
                + "	   QIDOCNO\n"
                + "FROM QIDETAIL\n"
                + "WHERE (select top 1 QIHDEST from QIHEAD where QIHQNO = QIDQNO) = '" + dest + "'\n"
                + "AND QIDWHS = '" + wh + "'\n"
                + "AND QIDSHDT = '" + shipDate + "'\n"
                + "AND QIDRUNNO IN (" + list + ")\n"
                + ") TMP\n"
                + "GROUP BY QIDROUND, MATCTRL, QIDMAT, QIDDESC, QIDPACKTYPE, BAG, ROLL, BOX, QIDQNO, QIDGPQR, MVT, QIDOCNO\n"
                + "ORDER BY QIDROUND, MATCTRL, QIDGPQR, QIDMAT, QIDQNO, MVT, QIDOCNO";
//        System.out.println(sql);
        ResultSet result = null;

        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            result = ps.executeQuery();
        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public ResultSet findForPrintWMS350X2(String wh, String dest, String shipDate, String round) throws SQLException {

        String sql = "SELECT DTRO, QIDSHDT, QIDROUND, MATCTRL, QIDMAT, QIDDESC, \n"
                + "CASE WHEN CNT = 1 THEN (CASE WHEN QIDGPQR IS NULL THEN SUM(BAG) ELSE (CASE WHEN SUM(BAG) > 1 THEN 1 ELSE SUM(BAG) END) END) WHEN CNT != 1 AND QIDGPQR IS NULL THEN SUM(BAG) ELSE 0 END AS BAG, \n"
                + "CASE WHEN CNT = 1 THEN (CASE WHEN QIDGPQR IS NULL THEN SUM(ROLL) ELSE (CASE WHEN SUM(ROLL) > 1 THEN 1 ELSE SUM(ROLL) END) END) WHEN CNT != 1 AND QIDGPQR IS NULL THEN SUM(ROLL) ELSE 0 END AS ROLL, \n"
                + "CASE WHEN CNT = 1 THEN (CASE WHEN QIDGPQR IS NULL THEN SUM(BOX) ELSE (CASE WHEN SUM(BOX) > 1 THEN 1 ELSE SUM(BOX) END) END) WHEN CNT != 1 AND QIDGPQR IS NULL THEN SUM(BOX) ELSE 0 END AS BOX,\n"
                + "QIDQNO, QIDGPQR, MVT, TOTD, QIDOCNO, DOCLEN, QIDCDT, QIDID, CNT\n"
                + "FROM(\n"
                + "SELECT QIDSHDT +'-'+ QIDROUND AS DTRO, QIDSHDT, QIDROUND, MATCTRL, QIDMAT, QIDDESC, \n"
                + "BAG, ROLL, BOX, QIDQNO, QIDGPQR, MVT, TOTD, QIDOCNO, DOCLEN, QIDCDT, QIDID, CNT\n"
                + "FROM(\n"
                + "SELECT FORMAT(QIDSHDT,'yyyy-MM-dd') AS QIDSHDT, QIDROUND,\n"
                + "       CASE WHEN QIDMTCTRL = '' OR QIDMTCTRL IS NULL THEN (select top 1 [SAPMCTRL] from [SAPMAS] where [SAPMAT] = QIDMAT) ELSE QIDMTCTRL END AS MATCTRL,\n"
                + "       QIDMAT, (select top 1 QRMDESC from QRMMAS where QRMCODE = QIDMAT) AS QIDDESC,\n"
                + "	   CASE WHEN QIDPACKTYPE = 'BAG' THEN 1 WHEN QIDPACKTYPE = 'PACK' THEN 1 ELSE 0 END AS BAG,\n"
                + "	   CASE WHEN QIDPACKTYPE = 'ROLL' THEN 1 ELSE 0 END AS ROLL,\n"
                + "	   CASE WHEN QIDPACKTYPE = 'BOX' THEN 1 ELSE 0 END AS BOX,\n"
                + "       QIDQNO, QIDGPQR, \n"
                + "	   (select top 1 QIHMVT from QIHEAD where QIHQNO = QIDQNO) AS MVT,\n"
                + "	   (select top 1 QIHTOTD from QIHEAD where QIHQNO = QIDQNO) AS TOTD,\n"
                + "	   QIDOCNO,LEN(QIDOCNO) AS DOCLEN, QIDCDT, CASE WHEN QIDGPQR IS NULL THEN QIDID ELSE QIDGPQR END as QIDID\n"
                + "       ,ROW_NUMBER() \n"
                + "	   OVER(PARTITION BY (CASE WHEN QIDGPQR IS NULL THEN QIDID ELSE QIDGPQR END) ORDER BY QIDSHDT, QIDROUND, QIDQNO, QIDCDT, (select top 1 QIHMTCTRL from QIHEAD where QIHQNO = QIDQNO), QIDGPQR, QIDMAT, (select top 1 QIHMVT from QIHEAD where QIHQNO = QIDQNO), QIDOCNO, LEN(QIDOCNO)) as CNT\n"
                + "FROM QIDETAIL\n"
                + "WHERE (select top 1 QIHDEST from QIHEAD where QIHQNO = QIDQNO) = '" + dest + "'\n"
                + "AND QIDWHS = '" + wh + "'\n"
                + "AND QIDSHDT = '" + shipDate + "'\n"
                + "AND QIDROUND = '" + round + "'\n"
                + ") TMP\n"
                + ")TMP2\n"
                + "WHERE DTRO is not null\n"
                + "GROUP BY DTRO, QIDSHDT, QIDROUND, MATCTRL, QIDMAT, QIDDESC, QIDQNO, QIDGPQR, MVT, TOTD, QIDOCNO, DOCLEN, QIDCDT, QIDID, CNT\n"
                + "ORDER BY QIDSHDT, QIDROUND, QIDQNO, QIDCDT, MATCTRL, QIDGPQR, QIDMAT, MVT, QIDOCNO, DOCLEN";

//        System.out.println(sql);
        ResultSet result = null;

        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            result = ps.executeQuery();
        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public ResultSet findForPrintWMS350X(String[] id, String wh, String dest) throws SQLException {

        List<String> shdtList = new ArrayList<String>();
        StringBuilder list = new StringBuilder();
        for (int i = 0; i < id.length; i++) {
//            if (i == 0) {
//                if (id[i].split("-")[6].equals("null")) {
//                    list.append("'0'");
//                }
//            }
//            if (!id[i].split("-")[6].equals("null")) {
            if (!id[i].split("-")[0].equals(" ")) {
                if (i == 0) {
                    list.append("'").append(id[i].split("-")[0]).append("'");
                } else {
                    list.append(",'").append(id[i].split("-")[0]).append("'");
                }
            }
//            }

            String tmp = id[i].split("-")[2].split(" : ")[1];
            tmp = tmp.replace("/", "-");
            tmp = tmp.split("-")[2] + "-" + tmp.split("-")[1] + "-" + tmp.split("-")[0];
            if (!shdtList.contains(tmp)) {
                shdtList.add(tmp);
            }
        }

        StringBuilder list2 = new StringBuilder();
        for (int i = 0; i < shdtList.size(); i++) {
            if (i == 0) {
                list2.append("'").append(shdtList.get(i)).append("'");
            } else {
                list2.append(",'").append(shdtList.get(i)).append("'");
            }
        }

//        String sql = "SELECT QIDSHDT +'-'+ QIDROUND AS DTRO, QIDSHDT, QIDROUND, MATCTRL, QIDMAT, QIDDESC, QIDPACKTYPE, \n"
//                + "CASE WHEN BAG = 1 THEN COUNT(*) ELSE 0 END AS BAG, \n"
//                + "CASE WHEN ROLL = 1 THEN COUNT(*) ELSE 0 END AS ROLL, \n"
//                + "CASE WHEN BOX = 1 THEN COUNT(*) ELSE 0 END AS BOX, \n"
//                + "QIDQNO, \n"
//                + "(SELECT TOP 1 QIDRUNNO \n"
//                + "FROM QIDETAIL DD\n"
//                + "LEFT JOIN QIHEAD HH ON HH.QIHQNO = DD.QIDQNO\n"
//                + "LEFT JOIN QRMMAS MM ON MM.QRMCODE = DD.QIDMAT\n"
//                + "WHERE QIDROUND = TMP.QIDROUND\n"
//                + "AND HH.QIHMTCTRL = TMP.MATCTRL\n"
//                + "AND QIDMAT = TMP.QIDMAT\n"
//                + "AND MM.QRMDESC = TMP.QIDDESC\n"
//                + "AND QIDPACKTYPE = TMP.QIDPACKTYPE\n"
//                + "AND QIDQNO = TMP.QIDQNO\n"
//                + "AND HH.QIHMVT = TMP.MVT\n"
//                + ") AS QIDRUNNO, \n"
//                + "QIDGPQR, MVT, TOTD, QIDOCNO,DOCLEN\n"
//                + "FROM(\n"
//                + "SELECT FORMAT(QIDSHDT,'yyyy-MM-dd') AS QIDSHDT, QIDROUND,\n"
//                + "       (select top 1 QIHMTCTRL from QIHEAD where QIHQNO = QIDQNO) AS MATCTRL,\n"
//                + "       QIDMAT, (select top 1 QRMDESC from QRMMAS where QRMCODE = QIDMAT) AS QIDDESC,\n"
//                + "	   QIDPACKTYPE,\n"
//                + "	   CASE WHEN QIDPACKTYPE = 'BAG' THEN 1 WHEN QIDPACKTYPE = 'PACK' THEN 1 ELSE 0 END AS BAG,\n"
//                + "	   CASE WHEN QIDPACKTYPE = 'ROLL' THEN 1 ELSE 0 END AS ROLL,\n"
//                + "	   CASE WHEN QIDPACKTYPE = 'BOX' THEN 1 ELSE 0 END AS BOX,\n"
//                + "       QIDQNO, QIDGPQR,\n"
//                + "	   (select top 1 QIHMVT from QIHEAD where QIHQNO = QIDQNO) AS MVT, \n"
//                + "        (select top 1 QIHTOTD from QIHEAD where QIHQNO = QIDQNO) AS TOTD, \n"
//                + "	   QIDOCNO,LEN(QIDOCNO) AS DOCLEN\n"
//                + "FROM QIDETAIL\n"
//                + "WHERE (select top 1 QIHDEST from QIHEAD where QIHQNO = QIDQNO) = '" + dest + "'\n"
//                + "AND QIDWHS = '" + wh + "'\n"
//                //                + "AND QIDSHDT = '" + shipDate + "'\n"
//                + "AND QIDID IN (" + list + ")\n"
//                + ") TMP\n"
//                + "GROUP BY QIDSHDT, QIDROUND, MATCTRL, QIDMAT, QIDDESC, QIDPACKTYPE, BAG, ROLL, BOX, QIDQNO, QIDGPQR, MVT, TOTD, QIDOCNO,DOCLEN\n"
//                + "ORDER BY QIDSHDT, QIDROUND, MATCTRL, QIDGPQR, QIDMAT, QIDQNO, MVT, QIDOCNO,DOCLEN";
        String sql = "SELECT DTRO, QIDSHDT, QIDROUND, MATCTRL, QIDMAT, QIDDESC, \n"
                + "CASE WHEN CNT = 1 THEN (CASE WHEN QIDGPQR IS NULL THEN SUM(BAG) ELSE (CASE WHEN SUM(BAG) > 1 THEN 1 ELSE SUM(BAG) END) END) WHEN CNT != 1 AND QIDGPQR IS NULL THEN SUM(BAG) ELSE 0 END AS BAG, \n"
                + "CASE WHEN CNT = 1 THEN (CASE WHEN QIDGPQR IS NULL THEN SUM(ROLL) ELSE (CASE WHEN SUM(ROLL) > 1 THEN 1 ELSE SUM(ROLL) END) END) WHEN CNT != 1 AND QIDGPQR IS NULL THEN SUM(ROLL) ELSE 0 END AS ROLL, \n"
                + "CASE WHEN CNT = 1 THEN (CASE WHEN QIDGPQR IS NULL THEN SUM(BOX) ELSE (CASE WHEN SUM(BOX) > 1 THEN 1 ELSE SUM(BOX) END) END) WHEN CNT != 1 AND QIDGPQR IS NULL THEN SUM(BOX) ELSE 0 END AS BOX,\n"
                + "QIDQNO, QIDGPQR, MVT, TOTD, QIDOCNO, DOCLEN, QIDCDT, QIDID, CNT\n"
                + "FROM(\n"
                + "SELECT QIDSHDT +'-'+ QIDROUND AS DTRO, QIDSHDT, QIDROUND, MATCTRL, QIDMAT, QIDDESC, \n"
                + "BAG, ROLL, BOX, QIDQNO, QIDGPQR, MVT, TOTD, QIDOCNO, DOCLEN, QIDCDT, QIDID, CNT\n"
                + "FROM(\n"
                + "SELECT FORMAT(QIDSHDT,'yyyy-MM-dd') AS QIDSHDT, QIDROUND,\n"
                + "       CASE WHEN QIDMTCTRL = '' OR QIDMTCTRL IS NULL THEN (select top 1 [SAPMCTRL] from [SAPMAS] where [SAPMAT] = QIDMAT) ELSE QIDMTCTRL END AS MATCTRL,\n"
                //                + "       (select top 1 QIHMTCTRL from QIHEAD where QIHQNO = QIDQNO) AS MATCTRL,\n"
                + "       QIDMAT, (select top 1 QRMDESC from QRMMAS where QRMCODE = QIDMAT) AS QIDDESC,\n"
                + "	   CASE WHEN QIDPACKTYPE = 'BAG' THEN 1 WHEN QIDPACKTYPE = 'PACK' THEN 1 ELSE 0 END AS BAG,\n"
                + "	   CASE WHEN QIDPACKTYPE = 'ROLL' THEN 1 ELSE 0 END AS ROLL,\n"
                + "	   CASE WHEN QIDPACKTYPE = 'BOX' THEN 1 ELSE 0 END AS BOX,\n"
                + "       QIDQNO, QIDGPQR, \n"
                + "	   (select top 1 QIHMVT from QIHEAD where QIHQNO = QIDQNO) AS MVT,\n"
                + "	   (select top 1 QIHTOTD from QIHEAD where QIHQNO = QIDQNO) AS TOTD,\n"
                + "	   QIDOCNO,LEN(QIDOCNO) AS DOCLEN, QIDCDT, CASE WHEN QIDGPQR IS NULL THEN QIDID ELSE QIDGPQR END as QIDID\n"
                + "       ,ROW_NUMBER() \n"
                + "	   OVER(PARTITION BY (CASE WHEN QIDGPQR IS NULL THEN QIDID ELSE QIDGPQR END) ORDER BY QIDSHDT, QIDROUND, QIDQNO, QIDCDT, (select top 1 QIHMTCTRL from QIHEAD where QIHQNO = QIDQNO), QIDGPQR, QIDMAT, (select top 1 QIHMVT from QIHEAD where QIHQNO = QIDQNO), QIDOCNO, LEN(QIDOCNO)) as CNT\n"
                + "FROM QIDETAIL\n"
                + "WHERE (select top 1 QIHDEST from QIHEAD where QIHQNO = QIDQNO) = '" + dest + "'\n"
                + "AND QIDWHS = '" + wh + "'\n"
                + "AND QIDSHDT IN (" + list2 + ")\n"
                + "AND (QIDID IN (" + list + ") OR QIDGPQR IN (" + list + "))\n"
                + ") TMP\n"
                + ")TMP2\n"
                + "WHERE DTRO is not null\n"
                + "GROUP BY DTRO, QIDSHDT, QIDROUND, MATCTRL, QIDMAT, QIDDESC, QIDQNO, QIDGPQR, MVT, TOTD, QIDOCNO, DOCLEN, QIDCDT, QIDID, CNT\n"
                + "ORDER BY QIDSHDT, QIDROUND, QIDQNO, QIDCDT, MATCTRL, QIDGPQR, QIDMAT, MVT, QIDOCNO, DOCLEN";

//        System.out.println(sql);
        ResultSet result = null;

        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            result = ps.executeQuery();
        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public ResultSet findForPrint2WMS350(String[] id, String wh, String dest, String shipDate) throws SQLException {

        StringBuilder list = new StringBuilder();
        for (int i = 0; i < id.length; i++) {
            if (i == 0) {
                if (id[i].split("-")[6].equals("null")) {
                    list.append("'0'");
                }
            }
            if (!id[i].split("-")[6].equals("null")) {
                if (!id[i].split("-")[3].equals(" ")) {
                    if (i == 0) {
                        list.append("'").append(id[i].split("-")[3]).append("'");
                    } else {
                        list.append(",'").append(id[i].split("-")[3]).append("'");
                    }
                }
            }
        }

        String sql = "SELECT QIDROUND, MATCTRL, QIDMAT, QIDDESC, QIDPACKTYPE,\n"
                + "CASE WHEN BAG = 1 THEN COUNT(*) ELSE 0 END AS BAG,\n"
                + "CASE WHEN ROLL = 1 THEN COUNT(*) ELSE 0 END AS ROLL,\n"
                + "CASE WHEN BOX = 1 THEN COUNT(*) ELSE 0 END AS BOX,\n"
                + "QIDQNO,\n"
                + "(SELECT TOP 1 QIDRUNNO\n"
                + "FROM QIDETAIL DD\n"
                + "LEFT JOIN QIHEAD HH ON HH.QIHQNO = DD.QIDQNO\n"
                + "LEFT JOIN QRMMAS MM ON MM.QRMCODE = DD.QIDMAT\n"
                + "WHERE QIDROUND = TMP.QIDROUND\n"
                + "AND HH.QIHMTCTRL = TMP.MATCTRL\n"
                + "AND QIDMAT = TMP.QIDMAT\n"
                + "AND MM.QRMDESC = TMP.QIDDESC\n"
                + "AND QIDPACKTYPE = TMP.QIDPACKTYPE\n"
                + "AND QIDQNO = TMP.QIDQNO\n"
                + "AND HH.QIHMVT = TMP.MVT\n"
                + ") AS QIDRUNNO,\n"
                + "QIDGPQR, MVT, QIDOCNO,\n"
                + "(\n"
                + "SELECT MIN(QIDRUNNO)\n"
                + "FROM QIDETAIL DD\n"
                + "LEFT JOIN QIHEAD HH ON HH.QIHQNO = DD.QIDQNO\n"
                + "WHERE QIDROUND = TMP.QIDROUND\n"
                + "AND HH.QIHMTCTRL = TMP.MATCTRL\n"
                + "AND QIDMAT = TMP.QIDMAT\n"
                + "AND QIDPACKTYPE = TMP.QIDPACKTYPE\n"
                + "AND QIDQNO = TMP.QIDQNO\n"
                + "AND HH.QIHMVT = TMP.MVT\n"
                + "AND HH.QIHDEST = '" + dest + "'\n"
                + "AND QIDWHS = '" + wh + "'\n"
                + "AND QIDSHDT = '" + shipDate + "'\n"
                + ") AS MINRUN,\n"
                + "(\n"
                + "SELECT MAX(QIDRUNNO)\n"
                + "FROM QIDETAIL DD\n"
                + "LEFT JOIN QIHEAD HH ON HH.QIHQNO = DD.QIDQNO\n"
                + "WHERE QIDROUND = TMP.QIDROUND\n"
                + "AND HH.QIHMTCTRL = TMP.MATCTRL\n"
                + "AND QIDMAT = TMP.QIDMAT\n"
                + "AND QIDPACKTYPE = TMP.QIDPACKTYPE\n"
                + "AND QIDQNO = TMP.QIDQNO\n"
                + "AND HH.QIHMVT = TMP.MVT\n"
                + "AND HH.QIHDEST = '" + dest + "'\n"
                + "AND QIDWHS = '" + wh + "'\n"
                + "AND QIDSHDT = '" + shipDate + "'\n"
                + ") AS MAXRUN\n"
                + "FROM(\n"
                + "SELECT QIDROUND,\n"
                + "       (select top 1 QIHMTCTRL from QIHEAD where QIHQNO = QIDQNO) AS MATCTRL,\n"
                + "       QIDMAT, (select top 1 QRMDESC from QRMMAS where QRMCODE = QIDMAT) AS QIDDESC,\n"
                + "	   QIDPACKTYPE,\n"
                + "	   CASE WHEN QIDPACKTYPE = 'BAG' THEN 1 WHEN QIDPACKTYPE = 'PACK' THEN 1 ELSE 0 END AS BAG,\n"
                + "	   CASE WHEN QIDPACKTYPE = 'ROLL' THEN 1 ELSE 0 END AS ROLL,\n"
                + "	   CASE WHEN QIDPACKTYPE = 'BOX' THEN 1 ELSE 0 END AS BOX,\n"
                + "       QIDQNO, QIDGPQR,\n"
                + "	   (select top 1 QIHMVT from QIHEAD where QIHQNO = QIDQNO) AS MVT,\n"
                + "	   QIDOCNO\n"
                + "FROM QIDETAIL\n"
                + "WHERE (select top 1 QIHDEST from QIHEAD where QIHQNO = QIDQNO) = '" + dest + "'\n"
                + "AND QIDWHS = '" + wh + "'\n"
                + "AND QIDSHDT = '" + shipDate + "'\n"
                + "AND QIDRUNNO IN (" + list + ")\n"
                + ") TMP\n"
                + "GROUP BY QIDROUND, MATCTRL, QIDMAT, QIDDESC, QIDPACKTYPE, BAG, ROLL, BOX, QIDQNO, QIDGPQR, MVT, QIDOCNO\n"
                + "ORDER BY QIDROUND, QIDRUNNO, MATCTRL, QIDGPQR, QIDMAT, QIDQNO, MVT, QIDOCNO ";
//        System.out.println(sql);
        ResultSet result = null;

        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            result = ps.executeQuery();
        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public ResultSet findForPrint2WMS350X(String[] id, String wh, String dest, String shipDate) throws SQLException {

        StringBuilder list = new StringBuilder();
        for (int i = 0; i < id.length; i++) {
            if (i == 0) {
                if (id[i].split("-")[6].equals("null")) {
                    list.append("'0'");
                }
            }
            if (!id[i].split("-")[6].equals("null")) {
                if (!id[i].split("-")[3].equals(" ")) {
                    if (i == 0) {
                        list.append("'").append(id[i].split("-")[3]).append("'");
                    } else {
                        list.append(",'").append(id[i].split("-")[3]).append("'");
                    }
                }
            }
        }

        String sql = "SELECT QIDSHDT +'-'+ QIDROUND AS DTRO, QIDSHDT, QIDROUND, MATCTRL, QIDMAT, QIDDESC, QIDPACKTYPE,\n"
                + "CASE WHEN BAG = 1 THEN COUNT(*) ELSE 0 END AS BAG,\n"
                + "CASE WHEN ROLL = 1 THEN COUNT(*) ELSE 0 END AS ROLL,\n"
                + "CASE WHEN BOX = 1 THEN COUNT(*) ELSE 0 END AS BOX,\n"
                + "QIDQNO,\n"
                + "(SELECT TOP 1 QIDRUNNO\n"
                + "FROM QIDETAIL DD\n"
                + "LEFT JOIN QIHEAD HH ON HH.QIHQNO = DD.QIDQNO\n"
                + "LEFT JOIN QRMMAS MM ON MM.QRMCODE = DD.QIDMAT\n"
                + "WHERE QIDROUND = TMP.QIDROUND\n"
                + "AND HH.QIHMTCTRL = TMP.MATCTRL\n"
                + "AND QIDMAT = TMP.QIDMAT\n"
                + "AND MM.QRMDESC = TMP.QIDDESC\n"
                + "AND QIDPACKTYPE = TMP.QIDPACKTYPE\n"
                + "AND QIDQNO = TMP.QIDQNO\n"
                + "AND HH.QIHMVT = TMP.MVT\n"
                + ") AS QIDRUNNO,\n"
                + "QIDGPQR, MVT, QIDOCNO,\n"
                + "(\n"
                + "SELECT MIN(QIDRUNNO)\n"
                + "FROM QIDETAIL DD\n"
                + "LEFT JOIN QIHEAD HH ON HH.QIHQNO = DD.QIDQNO\n"
                + "WHERE QIDROUND = TMP.QIDROUND\n"
                + "AND HH.QIHMTCTRL = TMP.MATCTRL\n"
                + "AND QIDMAT = TMP.QIDMAT\n"
                + "AND QIDPACKTYPE = TMP.QIDPACKTYPE\n"
                + "AND QIDQNO = TMP.QIDQNO\n"
                + "AND HH.QIHMVT = TMP.MVT\n"
                + "AND HH.QIHDEST = '" + dest + "'\n"
                + "AND QIDWHS = '" + wh + "'\n"
                //                + "AND QIDSHDT = '" + shipDate + "'\n"
                + ") AS MINRUN,\n"
                + "(\n"
                + "SELECT MAX(QIDRUNNO)\n"
                + "FROM QIDETAIL DD\n"
                + "LEFT JOIN QIHEAD HH ON HH.QIHQNO = DD.QIDQNO\n"
                + "WHERE QIDROUND = TMP.QIDROUND\n"
                + "AND HH.QIHMTCTRL = TMP.MATCTRL\n"
                + "AND QIDMAT = TMP.QIDMAT\n"
                + "AND QIDPACKTYPE = TMP.QIDPACKTYPE\n"
                + "AND QIDQNO = TMP.QIDQNO\n"
                + "AND HH.QIHMVT = TMP.MVT\n"
                + "AND HH.QIHDEST = '" + dest + "'\n"
                + "AND QIDWHS = '" + wh + "'\n"
                //                + "AND QIDSHDT = '" + shipDate + "'\n"
                + ") AS MAXRUN\n"
                + "FROM(\n"
                + "SELECT FORMAT(QIDSHDT,'yyyy-MM-dd') AS QIDSHDT, QIDROUND,\n"
                + "       (select top 1 QIHMTCTRL from QIHEAD where QIHQNO = QIDQNO) AS MATCTRL,\n"
                + "       QIDMAT, (select top 1 QRMDESC from QRMMAS where QRMCODE = QIDMAT) AS QIDDESC,\n"
                + "	   QIDPACKTYPE,\n"
                + "	   CASE WHEN QIDPACKTYPE = 'BAG' THEN 1 WHEN QIDPACKTYPE = 'PACK' THEN 1 ELSE 0 END AS BAG,\n"
                + "	   CASE WHEN QIDPACKTYPE = 'ROLL' THEN 1 ELSE 0 END AS ROLL,\n"
                + "	   CASE WHEN QIDPACKTYPE = 'BOX' THEN 1 ELSE 0 END AS BOX,\n"
                + "       QIDQNO, QIDGPQR,\n"
                + "	   (select top 1 QIHMVT from QIHEAD where QIHQNO = QIDQNO) AS MVT,\n"
                + "	   QIDOCNO\n"
                + "FROM QIDETAIL\n"
                + "WHERE (select top 1 QIHDEST from QIHEAD where QIHQNO = QIDQNO) = '" + dest + "'\n"
                + "AND QIDWHS = '" + wh + "'\n"
                //                + "AND QIDSHDT = '" + shipDate + "'\n"
                + "AND QIDRUNNO IN (" + list + ")\n"
                + ") TMP\n"
                + "GROUP BY QIDSHDT, QIDROUND, MATCTRL, QIDMAT, QIDDESC, QIDPACKTYPE, BAG, ROLL, BOX, QIDQNO, QIDGPQR, MVT, QIDOCNO\n"
                + "ORDER BY QIDSHDT, QIDROUND, QIDRUNNO, MATCTRL, QIDGPQR, QIDMAT, QIDQNO, MVT, QIDOCNO ";
//        System.out.println(sql);
        ResultSet result = null;

        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            result = ps.executeQuery();
        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public List<QIDETAIL> findRUNNO(String id) {

        List<QIDETAIL> UAList = new ArrayList<QIDETAIL>();

        String where = "";

        String[] parts = id.split("-");
        id = parts[0];
        String qno = parts[1];

        if (id.length() < 17) {
            where = "QIDGPQR";
        } else if (id.length() == 17) {
            where = "QIDID";
        }

        String sql = "SELECT QIDRUNNO, QIDWHS, "
                + "	SUBSTRING(CONVERT(VARCHAR(25), QIDSHDT, 126), 1, 10) as QIDSHDT "
                + "FROM QIDETAIL "
                + "WHERE " + where + " = '" + id + "' "
                + "AND QIDQNO = '" + qno + "' ";

        ResultSet result = null;

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            result = ps.executeQuery();

            while (result.next()) {

                QIDETAIL p = new QIDETAIL();

                String runno = "";
                if (result.getString("QIDRUNNO") != null) {
                    runno = result.getString("QIDRUNNO");
                }

                p.setRunno(runno);
                p.setWh(result.getString("QIDWHS"));

                String ship = "";
                if (result.getString("QIDSHDT") != null) {
                    ship = result.getString("QIDSHDT");
                    String[] dt = ship.split("-");
                    ship = dt[2] + "/" + dt[1] + "/" + dt[0];
                }

                p.setShipmentDate(ship);

                UAList.add(p);

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public List<QIDETAIL> findAll(String wh, String qno) {

        List<QIDETAIL> UAList = new ArrayList<QIDETAIL>();

        DecimalFormat formatDou = new DecimalFormat("#,##0.000");

        String sql = "SELECT QIDLINE, QIDWHS, QIDQNO, QIDMAT, (select top 1 QRMDESC from QRMMAS where QRMCODE = QIDMAT) as QIDDESC, QIDGPQR, QIDID, QIDQTY, QIDBUN, "
                + "QIDPACKTYPE, QIDREMARK, QIDSTS + ' : ' + QRMSTS.STSDESC as QIDSTS, QIDSTS as STS "
                + "FROM QIDETAIL "
                + "LEFT JOIN QRMSTS "
                + "ON QIDETAIL.QIDSTS = QRMSTS.STSCODE "
                + "WHERE QIDQNO = '" + qno + "' "
                + "AND QIDWHS = '" + wh + "' "
                + "AND QIDSTS != ' ' "
                + "AND (QIDSTS = '1' OR QIDSTS = '0') "
                + "ORDER BY QIDLINE ";

//        System.out.println(sql);
        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            String oldGID = "";

            while (result.next()) {

                QIDETAIL p = new QIDETAIL();

                p.setWh(result.getString("QIDWHS"));
                p.setQno(result.getString("QIDQNO"));
                p.setNo(result.getString("QIDLINE"));
                p.setMatc(result.getString("QIDMAT"));
                p.setDesc(result.getString("QIDDESC"));
                p.setGID(result.getString("QIDGPQR"));
                p.setId(result.getString("QIDID"));
                p.setStatus(result.getString("QIDSTS"));
                p.setSts(result.getString("STS"));
                p.setUm(result.getString("QIDBUN"));
                p.setPack(result.getString("QIDPACKTYPE"));
                p.setRemark(result.getString("QIDREMARK"));

                double tot = Double.parseDouble(result.getString("QIDQTY"));
                String tqt = formatDou.format(tot);

                p.setQty(tqt);

                String ck = "";
                if (result.getString("QIDGPQR") == null) {
                    if (result.getString("STS").equals("1") || result.getString("STS").equals("2") || result.getString("STS").equals("8") || result.getString("STS").equals("9")) {
                        ck = "<input style=\"width: 30px; height: 30px;\" type=\"checkbox\" name=\"selectCk\" value=\"" + result.getString("QIDID") + "-" + result.getString("STS") + "-" + result.getString("QIDLINE") + "\">";
                    }
                } else if (result.getString("QIDGPQR").trim().equals("")) {
                    if (result.getString("STS").equals("1") || result.getString("STS").equals("2") || result.getString("STS").equals("8") || result.getString("STS").equals("9")) {
                        ck = "<input style=\"width: 30px; height: 30px;\" type=\"checkbox\" name=\"selectCk\" value=\"" + result.getString("QIDID") + "-" + result.getString("STS") + "-" + result.getString("QIDLINE") + "\">";
                    }
                } else {
                    if (oldGID.equals("")) {
                        oldGID = result.getString("QIDGPQR");
                        if (result.getString("STS").equals("1") || result.getString("STS").equals("2") || result.getString("STS").equals("8") || result.getString("STS").equals("9")) {
                            ck = "<input style=\"width: 30px; height: 30px;\" type=\"checkbox\" name=\"selectCk\" value=\"" + result.getString("QIDGPQR") + "-" + result.getString("STS") + "-" + result.getString("QIDLINE") + "\">";
                        }
                    } else {
                        if (!result.getString("QIDGPQR").equals(oldGID)) {
                            oldGID = result.getString("QIDGPQR");
                            if (result.getString("STS").equals("1") || result.getString("STS").equals("2") || result.getString("STS").equals("8") || result.getString("STS").equals("9")) {
                                ck = "<input style=\"width: 30px; height: 30px;\" type=\"checkbox\" name=\"selectCk\" value=\"" + result.getString("QIDGPQR") + "-" + result.getString("STS") + "-" + result.getString("QIDLINE") + "\">";
                            }
                        }
                    }
                }

                p.setCkBox(ck);

                UAList.add(p);

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public List<QIDETAIL> findAll940(String ym, String sortBy, String data, String wh, String sts) {

        List<QIDETAIL> UAList = new ArrayList<QIDETAIL>();

        String sort = "";
        if (sortBy.equals("user")) {
            sort = "AND QIDUSER = '" + data + "' ";
        } else if (sortBy.equals("mat")) {
            sort = "AND (SELECT TOP 1 QIHMTCTRL FROM QIHEAD WHERE QIHQNO = QIDQNO) = '" + data + "' ";
        } else if (sortBy.equals("dest")) {
            sort = "AND (SELECT TOP 1 QIHDEST FROM QIHEAD WHERE QIHQNO = QIDQNO) = '" + data + "' ";
        } else if (sortBy.equals("app")) {
            sort = "AND QIDAPUSR = '" + data + "' ";
        } else if (sortBy.equals("doc")) {
            if (data.equals("No")) {
                sort = "AND QIDOCNO IS NULL ";
            } else {
                sort = "AND QIDOCNO = '" + data + "' ";
            }
        } else if (sortBy.equals("mvt")) {
            sort = "AND QIHMVT = '" + data + "' ";
        }

        String status = "";
        if (sts != null) {
            if (!sts.trim().equals("")) {
                status = "AND QIDSTS = '" + sts + "' ";
            }
        }

        String sql = "SELECT DISTINCT QIDQNO, SUBSTRING(CONVERT(VARCHAR(25), QIHTRDT, 126), 1, 10) as QIHTRDT, "
                + "          QIHMVT, QDEST.QDEDESC as QIHDEST, SUBSTRING(MSSUSER.USERS,0,CHARINDEX(' ',MSSUSER.USERS,0)) as QIHUSER, "
                + "	     QIHSET, QIHPDGP, QIHMTCTRL "
                + "FROM QIDETAIL DE "
                + "LEFT JOIN QIHEAD HD ON HD.QIHQNO = DE.QIDQNO "
                + "LEFT JOIN QDEST ON HD.QIHDEST = QDEST.QDECOD "
                + "LEFT JOIN MSSUSER ON HD.QIHUSER = MSSUSER.USERID "
                + "WHERE CONVERT(CHAR(8), QIDTRDT, 112) like '" + ym + "%' "
                + "AND QIDWHS = '" + wh + "' "
                + status
                + sort
                + "ORDER BY QIDQNO DESC ";

//        System.out.println(sql);
        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                QIDETAIL p = new QIDETAIL();

                p.setQno(result.getString("QIDQNO"));
                p.setShipmentDate(result.getString("QIHTRDT"));
                p.setMvt(result.getString("QIHMVT"));
                p.setDesc(result.getString("QIHDEST"));
                p.setUm(result.getString("QIHUSER"));
                p.setStatus(result.getString("QIHSET"));
                p.setGID(result.getString("QIHPDGP"));
                p.setMatc(result.getString("QIHMTCTRL"));

                List<QIDETAIL> subList = findAllSub940(result.getString("QIDQNO"));

                p.setSubQNO(subList);

                UAList.add(p);
            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public List<QIDETAIL> findAllSub940(String qno) {

        List<QIDETAIL> UAList = new ArrayList<QIDETAIL>();

        String sql = "SELECT QIDQNO, QIDMAT, QIDID, QIDSTS "
                + "FROM QIDETAIL "
                + "WHERE QIDQNO = '" + qno + "' ";

//        System.out.println(sql);
        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                QIDETAIL p = new QIDETAIL();

                p.setQno(result.getString("QIDQNO"));
                p.setMatc(result.getString("QIDMAT"));
                p.setId(result.getString("QIDID"));

                String STS = "";
                if (result.getString("QIDSTS").equals("0")) {
                    STS = "<td align=\"center\"><button title=\"New Queue no.\" id=\"sts0\" disabled><i class=\"fa fa-file-o\" style=\"font-size:20px;\"></i></button></td>\n"
                            + "<td align=\"left\">New Queue</td>\n"
                            + "<td align=\"center\"></td>\n"
                            + "<td align=\"center\"></td>\n"
                            + "<td align=\"center\"></td>\n"
                            + "<td align=\"center\"></td>\n"
                            + "<td align=\"center\"></td> \n"
                            + "<td align=\"center\"></td>\n"
                            + "<td align=\"center\"></td>\n";
                } else if (result.getString("QIDSTS").equals("1")) {
                    STS = "<td align=\"center\"></td>\n"
                            + "<td align=\"center\"><button title=\"Requested\" id=\"sts1\" disabled><i class=\"fa fa-user-circle-o\" style=\"font-size:20px;\"></i></button></td>\n"
                            + "<td align=\"left\">Requested</td>\n"
                            + "<td align=\"center\"></td>\n"
                            + "<td align=\"center\"></td>\n"
                            + "<td align=\"center\"></td>\n"
                            + "<td align=\"center\"></td> \n"
                            + "<td align=\"center\"></td>\n"
                            + "<td align=\"center\"></td>\n";
                } else if (result.getString("QIDSTS").equals("2")) {
                    STS = "<td align=\"center\"></td>\n"
                            + "<td align=\"center\"></td>\n"
                            + "<td align=\"center\"></td>\n"
                            + "<td align=\"center\"><button title=\"Approved\" id=\"sts2\" disabled><i class=\"fa fa-check-circle-o\" style=\"font-size:20px;\"></i></button></td>\n"
                            + "<td align=\"left\">Approved</td>\n"
                            + "<td align=\"center\"></td>\n"
                            + "<td align=\"center\"></td> \n"
                            + "<td align=\"center\"></td>\n"
                            + "<td align=\"center\"></td>\n";
                } else if (result.getString("QIDSTS").equals("5")) {
                    STS = "<td align=\"center\"></td>\n"
                            + "<td align=\"center\"></td>\n"
                            + "<td align=\"center\"></td>\n"
                            + "<td align=\"center\"></td>\n"
                            + "<td align=\"center\"></td>\n"
                            + "<td align=\"center\"><button title=\"Received\" id=\"sts5\" disabled><i class=\"fa fa-archive\" style=\"font-size:20px;\"></i></button></td>\n"
                            + "<td align=\"left\">Received</td> \n"
                            + "<td align=\"center\"></td>\n"
                            + "<td align=\"center\"></td>\n";
                } else if (result.getString("QIDSTS").equals("6")) {
                    STS = "<td align=\"center\"></td>\n"
                            + "<td align=\"center\"></td>\n"
                            + "<td align=\"center\"></td>\n"
                            + "<td align=\"center\"></td>\n"
                            + "<td align=\"center\"></td>\n"
                            + "<td align=\"center\"></td>\n"
                            + "<td align=\"center\"><button title=\"Transported\" id=\"sts6\" disabled><i class=\"fa fa-truck\" style=\"font-size:20px;\"></i></button></td> \n"
                            + "<td align=\"left\">Transported</td>\n"
                            + "<td align=\"center\"></td>\n";
                } else if (result.getString("QIDSTS").equals("7")) {
                    STS = "<td align=\"center\"></td>\n"
                            + "<td align=\"center\"></td>\n"
                            + "<td align=\"center\"></td>\n"
                            + "<td align=\"center\"></td>\n"
                            + "<td align=\"center\"></td>\n"
                            + "<td align=\"center\"></td>\n"
                            + "<td align=\"center\"></td> \n"
                            + "<td align=\"center\"><button title=\"DEST Received\" id=\"sts7\" disabled><i class=\"fa fa-home\" style=\"font-size:20px;\"></i></button></td>\n"
                            + "<td align=\"left\">DEST Received</td>\n";
                } else if (result.getString("QIDSTS").equals("8")) {
                    STS = "<td align=\"center\"></td>\n"
                            + "<td align=\"center\"></td>\n"
                            + "<td align=\"center\"><button title=\"Cancelled\" id=\"sts8\" disabled><i class=\"fa fa-times-circle-o\" style=\"font-size:20px;\"></i></button></td>\n"
                            + "<td align=\"left\">Cancelled</td>\n"
                            + "<td align=\"center\"></td>\n"
                            + "<td align=\"center\"></td>\n"
                            + "<td align=\"center\"></td> \n"
                            + "<td align=\"center\"></td>\n"
                            + "<td align=\"center\"></td>\n";
                } else if (result.getString("QIDSTS").equals("9")) {
                    STS = "<td align=\"center\"></td>\n"
                            + "<td align=\"center\"></td>\n"
                            + "<td align=\"center\"></td>\n"
                            + "<td align=\"center\"></td>\n"
                            + "<td align=\"center\"><button title=\"Rejected\" id=\"sts9\" disabled><i class=\"fa fa-user-times\" style=\"font-size:20px;\"></i></button></td>\n"
                            + "<td align=\"left\">Rejected</td>\n"
                            + "<td align=\"center\"></td> \n"
                            + "<td align=\"center\"></td>\n"
                            + "<td align=\"center\"></td>\n";
                } else if (result.getString("QIDSTS").equals("C")) {
                    STS = "<td align=\"center\"></td>\n"
                            + "<td align=\"center\"></td>\n"
                            + "<td align=\"center\"></td>\n"
                            + "<td align=\"center\"></td>\n"
                            + "<td align=\"center\"></td>\n"
                            + "<td align=\"center\"></td>\n"
                            + "<td align=\"center\"></td> \n"
                            + "<td align=\"right\">COMPLETED</td> \n"
                            + "<td align=\"center\"><button title=\"Closed Queue no.\" id=\"stsC\" disabled><i class=\"fa fa-check-square\" style=\"font-size:20px;\"></i></button></td>";
                } else if (result.getString("QIDSTS").equals("D")) {
                    STS = "<td align=\"center\"></td>\n"
                            + "<td align=\"center\"></td>\n"
                            + "<td align=\"center\"></td>\n"
                            + "<td align=\"center\"></td>\n"
                            + "<td align=\"center\"></td>\n"
                            + "<td align=\"center\"></td>\n"
                            + "<td align=\"center\"></td> \n"
                            + "<td align=\"center\"><button title=\"DEST Returned\" id=\"stsD\" disabled><i class=\"fa fa-reply\" style=\"font-size:20px;\"></i></button></td> \n"
                            + "<td align=\"left\">DEST Returned</td>";
                } else if (result.getString("QIDSTS").equals("H")) {
                    STS = "<td align=\"center\"></td>\n"
                            + "<td align=\"center\"></td>\n"
                            + "<td align=\"center\"></td>\n"
                            + "<td align=\"center\"></td>\n"
                            + "<td align=\"center\"></td>\n"
                            + "<td align=\"center\"></td>\n"
                            + "<td align=\"center\"><button title=\"LG Hold\" id=\"stsH\" disabled><i class=\"fa fa-hand-paper-o\" style=\"font-size:20px;\"></i></button></td> \n"
                            + "<td align=\"left\">LG Hold</td> \n"
                            + "<td align=\"center\"></td>";
                } else if (result.getString("QIDSTS").equals("R")) {
                    STS = "<td align=\"center\"><button title=\"LG Returned\" id=\"stsR\" disabled><i class=\"fa fa-reply\" style=\"font-size:20px;\"></i></button></td>\n"
                            + "<td align=\"left\">LG Returned</td>\n"
                            + "<td align=\"center\"></td>\n"
                            + "<td align=\"center\"></td>\n"
                            + "<td align=\"center\"></td>\n"
                            + "<td align=\"center\"></td>\n"
                            + "<td align=\"center\"></td> \n"
                            + "<td align=\"center\"></td> \n"
                            + "<td align=\"center\"></td>";
                }

                p.setStatus(STS);

                UAList.add(p);
            }

//            connect.close();
        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public List<QIDETAIL> findUser940(String ym, String wh) {

        List<QIDETAIL> UAList = new ArrayList<QIDETAIL>();

        String sql = "SELECT DISTINCT QIDUSER, USERS\n"
                + "FROM QIDETAIL\n"
                + "LEFT JOIN MSSUSER ON QIDETAIL.QIDUSER = MSSUSER.USERID "
                + "WHERE CONVERT(CHAR(8), QIDTRDT, 112) like '" + ym + "%' "
                + "AND QIDWHS = '" + wh + "' "
                + "ORDER BY QIDUSER ";

//        System.out.println(sql);
        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                QIDETAIL p = new QIDETAIL();

                p.setId(result.getString("QIDUSER"));
                p.setDesc(result.getString("USERS"));

                UAList.add(p);

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public List<QIDETAIL> findMat350X(String wh, String dest) {

        List<QIDETAIL> UAList = new ArrayList<QIDETAIL>();

        String sql = "SELECT DISTINCT QIDWHS, QIHDEST\n"
                + ",CASE WHEN QIDMTCTRL = '' OR QIDMTCTRL IS NULL THEN (select top 1 [SAPMCTRL] from [SAPMAS] where [SAPMAT] = QIDMAT) ELSE QIDMTCTRL END AS MATCTRL\n"
                + ",MATCNAME\n"
                + "FROM QIDETAIL\n"
                + "LEFT JOIN QIHEAD ON QIHQNO = QIDQNO\n"
                + "LEFT JOIN MSSMATN ON LGPBE = (CASE WHEN QIDMTCTRL = '' OR QIDMTCTRL IS NULL THEN (select top 1 [SAPMCTRL] from [SAPMAS] where [SAPMAT] = QIDMAT) ELSE QIDMTCTRL END)\n"
                + "WHERE QIDWHS = '" + wh + "'\n"
                + "AND QIHDEST = '" + dest + "'\n"
                + "ORDER BY MATCTRL";

//        System.out.println(sql);
        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                QIDETAIL p = new QIDETAIL();

                p.setId(result.getString("MATCTRL"));
                p.setDesc(result.getString("MATCNAME"));

                UAList.add(p);

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public List<QIDETAIL> findMat940(String ym, String wh) {

        List<QIDETAIL> UAList = new ArrayList<QIDETAIL>();

        String sql = "SELECT DISTINCT QIHMTCTRL as MATCT, MATCNAME\n"
                + "FROM QIDETAIL \n"
                + "left join QIHEAD HD on QIDETAIL.QIDQNO = HD.QIHQNO\n"
                + "LEFT JOIN MSSMATN ON HD.QIHMTCTRL = MSSMATN.LGPBE "
                + "WHERE CONVERT(CHAR(8), QIDTRDT, 112) like '" + ym + "%' "
                + "AND QIDWHS = '" + wh + "' "
                + "ORDER BY MATCT ";

//        System.out.println(sql);
        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                QIDETAIL p = new QIDETAIL();

                p.setId(result.getString("MATCT"));
                p.setDesc(result.getString("MATCNAME"));

                UAList.add(p);

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public List<QIDETAIL> findDest940(String ym, String wh) {

        List<QIDETAIL> UAList = new ArrayList<QIDETAIL>();

        String sql = "SELECT DISTINCT QIHDEST as DEST, QDEDESC\n"
                + "FROM QIDETAIL \n"
                + "LEFT JOIN QIHEAD HD ON QIDETAIL.QIDQNO = HD.QIHQNO \n"
                + "LEFT JOIN QDEST ON HD.QIHDEST = QDEST.QDECOD \n"
                + "WHERE CONVERT(CHAR(8), QIDTRDT, 112) like '" + ym + "%' "
                + "AND QIDWHS = '" + wh + "' "
                + "ORDER BY DEST ";

//        System.out.println(sql);
        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                QIDETAIL p = new QIDETAIL();

                p.setId(result.getString("DEST"));
                p.setDesc(result.getString("QDEDESC"));

                UAList.add(p);

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public List<QIDETAIL> findApp940(String ym, String wh) {

        List<QIDETAIL> UAList = new ArrayList<QIDETAIL>();

        String sql = "SELECT DISTINCT QIDAPUSR, USERS\n"
                + "FROM QIDETAIL\n"
                + "LEFT JOIN MSSUSER ON QIDETAIL.QIDAPUSR = MSSUSER.USERID \n"
                + "WHERE QIDAPUSR != 'null'\n"
                + "AND CONVERT(CHAR(8), QIDTRDT, 112) like '" + ym + "%' "
                + "AND QIDWHS = '" + wh + "' "
                + "ORDER BY QIDAPUSR ";

//        System.out.println(sql);
        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                QIDETAIL p = new QIDETAIL();

                p.setId(result.getString("QIDAPUSR"));
                p.setDesc(result.getString("USERS"));

                UAList.add(p);

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public List<QIDETAIL> findDoc940(String ym, String wh) {

        List<QIDETAIL> UAList = new ArrayList<QIDETAIL>();

        String sql = "SELECT DISTINCT QIDOCNO\n"
                + "FROM QIDETAIL\n"
                + "WHERE CONVERT(CHAR(8), QIDTRDT, 112) like '" + ym + "%' "
                + "AND QIDWHS = '" + wh + "' "
                + "ORDER BY QIDOCNO ";

//        System.out.println(sql);
        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                QIDETAIL p = new QIDETAIL();

                if (result.getString("QIDOCNO") == null) {
                    p.setId("No Document Number");
                } else {
                    p.setId(result.getString("QIDOCNO"));
                }

                UAList.add(p);

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public List<QIDETAIL> findMvt940(String ym, String wh) {

        List<QIDETAIL> UAList = new ArrayList<QIDETAIL>();

        String sql = "SELECT DISTINCT QIHMVT, QMVDESC\n"
                + "FROM QIDETAIL\n"
                + "LEFT JOIN QIHEAD HD ON HD.QIHQNO = QIDETAIL.QIDQNO\n"
                + "LEFT JOIN QMVT MVT ON MVT.QMVMVT = HD.QIHMVT "
                + "WHERE CONVERT(CHAR(8), QIDTRDT, 112) like '" + ym + "%' "
                + "AND QIDWHS = '" + wh + "' "
                + "ORDER BY QIHMVT ";

//        System.out.println(sql);
        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                QIDETAIL p = new QIDETAIL();

                p.setId(result.getString("QIHMVT"));
                p.setDesc(result.getString("QMVDESC"));

                UAList.add(p);

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public List<QIDETAIL> findAllWMS311(String wh, String qno) {

        List<QIDETAIL> UAList = new ArrayList<QIDETAIL>();

        DecimalFormat formatDou = new DecimalFormat("#,##0.000");

        String sql = "SELECT QIDLINE, QIDWHS, QIDQNO, QIDMAT, (select top 1 QRMDESC from QRMMAS where QRMCODE = QIDMAT) as QIDDESC, QIDGPQR, QIDID, QIDQTY, QIDBUN, "
                + "QIDPACKTYPE, QIDREMARK, QIDSTS + ' : ' + QRMSTS.STSDESC as QIDSTS, QIDSTS as STS "
                + "FROM QIDETAIL "
                + "LEFT JOIN QRMSTS "
                + "ON QIDETAIL.QIDSTS = QRMSTS.STSCODE "
                + "WHERE QIDQNO = '" + qno + "' "
                + "AND QIDWHS = '" + wh + "' "
                + "AND QIDSTS != ' ' "
                + "AND (QIDSTS = 'R') "
                + "ORDER BY QIDLINE ";

//        System.out.println(sql);
        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            String oldGID = "";

            while (result.next()) {

                QIDETAIL p = new QIDETAIL();

                p.setWh(result.getString("QIDWHS"));
                p.setQno(result.getString("QIDQNO"));
                p.setNo(result.getString("QIDLINE"));
                p.setMatc(result.getString("QIDMAT"));
                p.setDesc(result.getString("QIDDESC"));
                p.setGID(result.getString("QIDGPQR"));
                p.setId(result.getString("QIDID"));
                p.setStatus(result.getString("QIDSTS"));
                p.setSts(result.getString("STS"));
                p.setUm(result.getString("QIDBUN"));
                p.setPack(result.getString("QIDPACKTYPE"));
                p.setRemark(result.getString("QIDREMARK"));

                double tot = Double.parseDouble(result.getString("QIDQTY"));
                String tqt = formatDou.format(tot);

                p.setQty(tqt);

                String ck = "";
                if (result.getString("QIDGPQR") == null) {
                    if (result.getString("STS").equals("R")) {
                        ck = "<input type=\"checkbox\" name=\"selectCk\" value=\"" + result.getString("QIDID") + "-" + result.getString("STS") + "-" + result.getString("QIDLINE") + "\">";
                    }
                } else if (result.getString("QIDGPQR").trim().equals("")) {
                    if (result.getString("STS").equals("R")) {
                        ck = "<input type=\"checkbox\" name=\"selectCk\" value=\"" + result.getString("QIDID") + "-" + result.getString("STS") + "-" + result.getString("QIDLINE") + "\">";
                    }
                } else {
                    if (oldGID.equals("")) {
                        oldGID = result.getString("QIDGPQR");
                        if (result.getString("STS").equals("R")) {
                            ck = "<input type=\"checkbox\" name=\"selectCk\" value=\"" + result.getString("QIDGPQR") + "-" + result.getString("STS") + "-" + result.getString("QIDLINE") + "\">";
                        }
                    }
                }

                p.setCkBox(ck);

                UAList.add(p);

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public List<QIDETAIL> findDetailDest(String wh, String dest, String mat) {

        List<QIDETAIL> UAList = new ArrayList<QIDETAIL>();

        DecimalFormat formatDou = new DecimalFormat("#,##0.00");

        String sql = "SELECT QIDWHS, (select top 1 QIHDEST from QIHEAD where QIHQNO = QIDQNO) AS DEST, "
                + "       (select top 1 QIHMTCTRL from QIHEAD where QIHQNO = QIDQNO) AS MATCTRL, "
                + "       QIDQNO, QIDMAT, (select top 1 QRMDESC from QRMMAS where QRMCODE = QIDMAT) as QIDDESC, "
                + "       QIDQTY, QIDBUN, QIDPACKTYPE "
                + "FROM QIDETAIL "
                + "WHERE (select top 1 QIHDEST from QIHEAD where QIHQNO = QIDQNO) = '" + dest + "' "
                + "AND (select top 1 QIHMTCTRL from QIHEAD where QIHQNO = QIDQNO) = '" + mat + "' "
                + "AND QIDWHS = '" + wh + "' "
                + "AND QIDSTS = '2' "
                + "ORDER BY MATCTRL, QIDMAT, QIDPACKTYPE ";

        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            String mc = "";
            String desc = "";
            double qty = 0;
            String um = "";
            int qp = 0;
            String pack = "";

            while (result.next()) {

                if (mc.equals("")) {
                    mc = result.getString("QIDMAT");
                    desc = result.getString("QIDDESC");
                    qty += Double.parseDouble(result.getString("QIDQTY"));
                    um = result.getString("QIDBUN");
                    qp += 1;
                    pack = result.getString("QIDPACKTYPE");
                } else {
                    if (result.getString("QIDMAT").trim().equals(mc)) {
                        if (result.getString("QIDPACKTYPE").trim().equals(pack)) {
                            mc = result.getString("QIDMAT");
                            desc = result.getString("QIDDESC");
                            qty += Double.parseDouble(result.getString("QIDQTY"));
                            um = result.getString("QIDBUN");
                            qp += 1;
                            pack = result.getString("QIDPACKTYPE");
                        } else {
                            QIDETAIL p = new QIDETAIL();
                            p.setNo(Integer.toString(qp)); // Q'pack
                            p.setMatc(mc);
                            p.setDesc(desc);
                            p.setUm(um);
                            p.setPack(pack);
                            String tqt = formatDou.format(qty);
                            p.setQty(tqt);
                            UAList.add(p);

                            mc = result.getString("QIDMAT");
                            desc = result.getString("QIDDESC");
                            qty = 0;
                            qty += Double.parseDouble(result.getString("QIDQTY"));
                            um = result.getString("QIDBUN");
                            qp = 0;
                            qp += 1;
                            pack = result.getString("QIDPACKTYPE");
                        }
                    } else {
                        QIDETAIL p = new QIDETAIL();
                        p.setNo(Integer.toString(qp)); // Q'pack
                        p.setMatc(mc);
                        p.setDesc(desc);
                        p.setUm(um);
                        p.setPack(pack);
                        String tqt = formatDou.format(qty);
                        p.setQty(tqt);
                        UAList.add(p);

                        mc = result.getString("QIDMAT");
                        desc = result.getString("QIDDESC");
                        qty = 0;
                        qty += Double.parseDouble(result.getString("QIDQTY"));
                        um = result.getString("QIDBUN");
                        qp = 0;
                        qp += 1;
                        pack = result.getString("QIDPACKTYPE");
                    }
                }

            }

            if (!mc.equals("")) {
                QIDETAIL p = new QIDETAIL();
                p.setNo(Integer.toString(qp)); // Q'pack
                p.setMatc(mc);
                p.setDesc(desc);
                p.setUm(um);
                p.setPack(pack);
                String tqt = formatDou.format(qty);
                p.setQty(tqt);
                UAList.add(p);
            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public List<QIDETAIL> findDetailDest340(String wh, String dest, String mat) {

        List<QIDETAIL> UAList = new ArrayList<QIDETAIL>();

        DecimalFormat formatDou = new DecimalFormat("#,##0.00");

        String sql = "SELECT QIDWHS, (select top 1 QIHDEST from QIHEAD where QIHQNO = QIDQNO) AS DEST, "
                + "       (select top 1 QIHMTCTRL from QIHEAD where QIHQNO = QIDQNO) AS MATCTRL, "
                + "       QIDQNO, QIDMAT, (select top 1 QRMDESC from QRMMAS where QRMCODE = QIDMAT) AS QIDDESC, "
                + "       QIDGPQR, QIDID, "
                + "       QIDQTY, QIDBUN, QIDPACKTYPE, QIDSTS + ' : ' + STSDESC AS QIDSTS, QIDSTS AS STS, "
                + "       (select top 1 QIHMVT from QIHEAD where QIHQNO = QIDQNO) AS MVT, "
                + "       (select top 1 QMVDESC from QMVT where QMVMVT = (select top 1 QIHMVT from QIHEAD where QIHQNO = QIDQNO)) AS MVTN, "
                + "       QIDSHDT, QIDLINE, "
                + "       QIDWHS + ' : ' + "
                + "	  SUBSTRING(CONVERT(VARCHAR(25), QIDSHDT, 126), 9, 2) + '/' + "
                + "	  SUBSTRING(CONVERT(VARCHAR(25), QIDSHDT, 126), 6, 2) + '/' + "
                + "	  SUBSTRING(CONVERT(VARCHAR(25), QIDSHDT, 126), 1, 4) as SHIPDATE, "
                + "       CASE WHEN cast(QIDRUNNO as nvarchar) IS NULL THEN ' ' ELSE cast(QIDRUNNO as nvarchar) END AS QIDRUNNO "
                + "FROM QIDETAIL "
                + "LEFT JOIN QRMSTS "
                + "ON QIDETAIL.QIDSTS = QRMSTS.STSCODE "
                + "WHERE (select top 1 QIHDEST from QIHEAD where QIHQNO = QIDQNO) = '" + dest + "' "
                + "AND (select top 1 QIHMTCTRL from QIHEAD where QIHQNO = QIDQNO) = '" + mat + "' "
                + "AND QIDWHS = '" + wh + "' "
                + "AND (QIDSTS = '2' OR QIDSTS = '5') "
                + "ORDER BY MATCTRL, QIDMAT, QIDPACKTYPE ";

        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            String mc = "";
            String desc = "";
            String gqr = "";
            String id = "";
            double qty = 0;
            String um = "";
            int qp = 0;
            String pack = "";
            String mvt = "";
            double sumqty = 0;
            double summ3 = 0;
            int sumbox = 0;
            int sumbag = 0;
            int sumroll = 0;
            String oldGID = "";

            while (result.next()) {

                mc = result.getString("QIDMAT");
                desc = result.getString("QIDDESC");
                if (result.getString("QIDGPQR") == null) {
                    gqr = "<b style=\"opacity: 0.0;\">1</b>";
                } else if (result.getString("QIDGPQR").trim().equals("")) {
                    gqr = "<b style=\"opacity: 0.0;\">1</b>";
                } else {
                    gqr = result.getString("QIDGPQR");
                }
                id = result.getString("QIDID");
                qty += Double.parseDouble(result.getString("QIDQTY"));
                um = result.getString("QIDBUN");
                qp += 1;
                pack = result.getString("QIDPACKTYPE");
                mvt = result.getString("MVT");

                QIDETAIL p = new QIDETAIL();
                p.setNo(Integer.toString(qp)); // Q'pack
                p.setMatc(mc);
                p.setDesc(desc);
                p.setGID(gqr);
                p.setId(id);
                p.setUm(um);
                p.setPack(pack);

                String shd = "";
                if (result.getString("QIDSHDT") != null) {
                    shd = result.getString("QIDSHDT");
                }
                String[] shdat = shd.split(" ");
                p.setShipmentDate(shdat[0]);

                p.setSts(result.getString("QIDSTS"));
                p.setMvt(mvt);
                p.setQno(result.getString("QIDQNO"));
                p.setMvtn(result.getString("MVTN"));
                p.setM3(formatDou.format(0));
                String tqt = formatDou.format(qty);
                p.setQty(tqt);
                sumqty += qty;
                summ3 += 0;

                String ck = "";
                String run = "";
                String runno = "";
                if (result.getString("QIDRUNNO") != null && !result.getString("QIDRUNNO").equals(" ")) {
                    runno = result.getString("QIDRUNNO");
                }
                if (result.getString("QIDGPQR") == null) {
                    ck = "<input style=\"width: 30px; height: 30px;\" type=\"checkbox\" id=\"selectCk-" + result.getString("QIDID") + "\" name=\"selectCk-" + mat + "\" value=\"" + result.getString("QIDID") + "-" + result.getString("QIDQNO") + "-" + result.getString("SHIPDATE") + "-" + result.getString("QIDRUNNO") + "-" + result.getString("QIDLINE") + "-" + result.getString("STS") + "\">";
                    run = "<input type=\"text\" name=\"running-" + mat + "\" value=\"" + runno + "\" style=\"width: 100px; text-align: center;\" disabled>";

                    if (pack.equals("BOX")) {
                        p.setBox(Integer.toString(qp));
                        sumbox += qp;
                    } else if (pack.equals("BAG") || pack.equals("PACK")) {
                        p.setBag(Integer.toString(qp));
                        sumbag += qp;
                    } else if (pack.equals("ROLL")) {
                        p.setRoll(Integer.toString(qp));
                        sumroll += qp;
                    }
                } else if (result.getString("QIDGPQR").trim().equals("")) {
                    ck = "<input style=\"width: 30px; height: 30px;\" type=\"checkbox\" id=\"selectCk-" + result.getString("QIDID") + "\" name=\"selectCk-" + mat + "\" value=\"" + result.getString("QIDID") + "-" + result.getString("QIDQNO") + "-" + result.getString("SHIPDATE") + "-" + result.getString("QIDRUNNO") + "-" + result.getString("QIDLINE") + "-" + result.getString("STS") + "\">";
                    run = "<input type=\"text\" name=\"running-" + mat + "\" value=\"" + runno + "\" style=\"width: 100px; text-align: center;\" disabled>";

                    if (pack.equals("BOX")) {
                        p.setBox(Integer.toString(qp));
                        sumbox += qp;
                    } else if (pack.equals("BAG") || pack.equals("PACK")) {
                        p.setBag(Integer.toString(qp));
                        sumbag += qp;
                    } else if (pack.equals("ROLL")) {
                        p.setRoll(Integer.toString(qp));
                        sumroll += qp;
                    }
                } else {
                    if (oldGID.equals("")) {
                        oldGID = result.getString("QIDGPQR");
                        ck = "<input style=\"width: 30px; height: 30px;\" type=\"checkbox\" id=\"selectCk-" + result.getString("QIDGPQR") + "\" name=\"selectCk-" + mat + "\" value=\"" + result.getString("QIDGPQR") + "-" + result.getString("QIDQNO") + "-" + result.getString("SHIPDATE") + "-" + result.getString("QIDRUNNO") + "-" + result.getString("QIDLINE") + "-" + result.getString("STS") + "\">";
                        run = "<input type=\"text\" name=\"running-" + mat + "\" value=\"" + runno + "\" style=\"width: 100px; text-align: center;\" disabled>";

                        if (pack.equals("BOX")) {
                            p.setBox(Integer.toString(qp));
                            sumbox += qp;
                        } else if (pack.equals("BAG") || pack.equals("PACK")) {
                            p.setBag(Integer.toString(qp));
                            sumbag += qp;
                        } else if (pack.equals("ROLL")) {
                            p.setRoll(Integer.toString(qp));
                            sumroll += qp;
                        }
                    } else {
                        if (!result.getString("QIDGPQR").equals(oldGID)) {
                            oldGID = result.getString("QIDGPQR");
                            ck = "<input style=\"width: 30px; height: 30px;\" type=\"checkbox\" id=\"selectCk-" + result.getString("QIDGPQR") + "\" name=\"selectCk-" + mat + "\" value=\"" + result.getString("QIDGPQR") + "-" + result.getString("QIDQNO") + "-" + result.getString("SHIPDATE") + "-" + result.getString("QIDRUNNO") + "-" + result.getString("QIDLINE") + "-" + result.getString("STS") + "\">";
                            run = "<input type=\"text\" name=\"running-" + mat + "\" value=\"" + runno + "\" style=\"width: 100px; text-align: center;\" disabled>";

                            if (pack.equals("BOX")) {
                                p.setBox(Integer.toString(qp));
                                sumbox += qp;
                            } else if (pack.equals("BAG") || pack.equals("PACK")) {
                                p.setBag(Integer.toString(qp));
                                sumbag += qp;
                            } else if (pack.equals("ROLL")) {
                                p.setRoll(Integer.toString(qp));
                                sumroll += qp;
                            }
                        }
                    }
                }

                p.setCkBox(ck);
                p.setRunno(run);

                UAList.add(p);

                qty = 0;
                qp = 0;

            }

            QIDETAIL p = new QIDETAIL();
            p.setGID("<b style=\"opacity: 0.0;\">0</b>");
            p.setMatc("<b style=\"opacity: 0.0;\">0</b>");
            p.setDesc("<b style=\"opacity: 0.0;\">0</b>");
            p.setId("<b>TOTAL</b>");
            p.setBox("<b>" + Integer.toString(sumbox) + "</b>");
            p.setBag("<b>" + Integer.toString(sumbag) + "</b>");
            p.setRoll("<b>" + Integer.toString(sumroll) + "</b>");
            p.setM3("<b>" + formatDou.format(summ3) + "</b>");
            p.setQty("<b>" + formatDou.format(sumqty) + "</b>");
            UAList.add(p);

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public List<QIDETAIL> findDetailDest341(String wh, String dest, String mat) {

        List<QIDETAIL> UAList = new ArrayList<QIDETAIL>();

        DecimalFormat formatDou = new DecimalFormat("#,##0.00");

        String sql = "SELECT QIDWHS, (select top 1 QIHDEST from QIHEAD where QIHQNO = QIDQNO) AS DEST, "
                + "       (select top 1 QIHMTCTRL from QIHEAD where QIHQNO = QIDQNO) AS MATCTRL, "
                + "       QIDQNO, QIDMAT, (select top 1 QRMDESC from QRMMAS where QRMCODE = QIDMAT) AS QIDDESC, "
                + "       QIDGPQR, QIDID, "
                + "       QIDQTY, QIDBUN, QIDPACKTYPE, QIDSTS + ' : ' + STSDESC AS QIDSTS, QIDSTS AS STS, "
                + "       (select top 1 QIHMVT from QIHEAD where QIHQNO = QIDQNO) AS MVT, "
                + "       (select top 1 QMVDESC from QMVT where QMVMVT = (select top 1 QIHMVT from QIHEAD where QIHQNO = QIDQNO)) AS MVTN, "
                + "       QIDSHDT, QIDLINE, "
                + "       QIDWHS + ' : ' + "
                + "	  SUBSTRING(CONVERT(VARCHAR(25), QIDSHDT, 126), 9, 2) + '/' + "
                + "	  SUBSTRING(CONVERT(VARCHAR(25), QIDSHDT, 126), 6, 2) + '/' + "
                + "	  SUBSTRING(CONVERT(VARCHAR(25), QIDSHDT, 126), 1, 4) as SHIPDATE, "
                + "       CASE WHEN cast(QIDRUNNO as nvarchar) IS NULL THEN ' ' ELSE cast(QIDRUNNO as nvarchar) END AS QIDRUNNO "
                + "FROM QIDETAIL "
                + "LEFT JOIN QRMSTS "
                + "ON QIDETAIL.QIDSTS = QRMSTS.STSCODE "
                + "WHERE (select top 1 QIHDEST from QIHEAD where QIHQNO = QIDQNO) = '" + dest + "' "
                + "AND (select top 1 QIHMTCTRL from QIHEAD where QIHQNO = QIDQNO) = '" + mat + "' "
                + "AND QIDWHS = '" + wh + "' "
                + "AND (QIDSTS = '5' OR QIDSTS = '6' OR QIDSTS = '7') "
                + "ORDER BY MATCTRL, QIDMAT, QIDPACKTYPE ";

        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            String mc = "";
            String desc = "";
            String gqr = "";
            String id = "";
            double qty = 0;
            String um = "";
            int qp = 0;
            String pack = "";
            String mvt = "";
            double sumqty = 0;
            double summ3 = 0;
            int sumbox = 0;
            int sumbag = 0;
            int sumroll = 0;
            String oldGID = "";

            while (result.next()) {

                mc = result.getString("QIDMAT");
                desc = result.getString("QIDDESC");
                if (result.getString("QIDGPQR") == null) {
                    gqr = "<b style=\"opacity: 0.0;\">1</b>";
                } else if (result.getString("QIDGPQR").trim().equals("")) {
                    gqr = "<b style=\"opacity: 0.0;\">1</b>";
                } else {
                    gqr = result.getString("QIDGPQR");
                }
                id = result.getString("QIDID");
                qty += Double.parseDouble(result.getString("QIDQTY"));
                um = result.getString("QIDBUN");
                qp += 1;
                pack = result.getString("QIDPACKTYPE");
                mvt = result.getString("MVT");

                QIDETAIL p = new QIDETAIL();
                p.setNo(Integer.toString(qp)); // Q'pack
                p.setMatc(mc);
                p.setDesc(desc);
                p.setGID(gqr);
                p.setId(id);
                p.setUm(um);
                p.setPack(pack);

                String shd = "";
                if (result.getString("QIDSHDT") != null) {
                    shd = result.getString("QIDSHDT");
                }
                String[] shdat = shd.split(" ");
                p.setShipmentDate(shdat[0]);

                p.setSts(result.getString("QIDSTS"));
                p.setMvt(mvt);
                p.setQno(result.getString("QIDQNO"));
                p.setMvtn(result.getString("MVTN"));
                p.setM3(formatDou.format(0));
                String tqt = formatDou.format(qty);
                p.setQty(tqt);
                sumqty += qty;
                summ3 += 0;

                String ck = "";
                String run = "";
                String runno = "";
                if (result.getString("QIDRUNNO") != null && !result.getString("QIDRUNNO").equals(" ")) {
                    runno = result.getString("QIDRUNNO");
                }
                if (result.getString("QIDGPQR") == null) {
                    ck = "<input style=\"width: 30px; height: 30px;\" type=\"checkbox\" id=\"selectCk-" + result.getString("QIDID") + "\" name=\"selectCk-" + mat + "\" value=\"" + result.getString("QIDID") + "-" + result.getString("QIDQNO") + "-" + result.getString("SHIPDATE") + "-" + result.getString("QIDRUNNO") + "-" + result.getString("QIDLINE") + "-" + result.getString("STS") + "\">";
                    run = "<input type=\"text\" name=\"running-" + mat + "\" value=\"" + runno + "\" style=\"width: 100px; text-align: center;\" disabled>";

                    if (pack.equals("BOX")) {
                        p.setBox(Integer.toString(qp));
                        sumbox += qp;
                    } else if (pack.equals("BAG") || pack.equals("PACK")) {
                        p.setBag(Integer.toString(qp));
                        sumbag += qp;
                    } else if (pack.equals("ROLL")) {
                        p.setRoll(Integer.toString(qp));
                        sumroll += qp;
                    }
                } else if (result.getString("QIDGPQR").trim().equals("")) {
                    ck = "<input style=\"width: 30px; height: 30px;\" type=\"checkbox\" id=\"selectCk-" + result.getString("QIDID") + "\" name=\"selectCk-" + mat + "\" value=\"" + result.getString("QIDID") + "-" + result.getString("QIDQNO") + "-" + result.getString("SHIPDATE") + "-" + result.getString("QIDRUNNO") + "-" + result.getString("QIDLINE") + "-" + result.getString("STS") + "\">";
                    run = "<input type=\"text\" name=\"running-" + mat + "\" value=\"" + runno + "\" style=\"width: 100px; text-align: center;\" disabled>";

                    if (pack.equals("BOX")) {
                        p.setBox(Integer.toString(qp));
                        sumbox += qp;
                    } else if (pack.equals("BAG") || pack.equals("PACK")) {
                        p.setBag(Integer.toString(qp));
                        sumbag += qp;
                    } else if (pack.equals("ROLL")) {
                        p.setRoll(Integer.toString(qp));
                        sumroll += qp;
                    }
                } else {
                    if (oldGID.equals("")) {
                        oldGID = result.getString("QIDGPQR");
                        ck = "<input style=\"width: 30px; height: 30px;\" type=\"checkbox\" id=\"selectCk-" + result.getString("QIDGPQR") + "\" name=\"selectCk-" + mat + "\" value=\"" + result.getString("QIDGPQR") + "-" + result.getString("QIDQNO") + "-" + result.getString("SHIPDATE") + "-" + result.getString("QIDRUNNO") + "-" + result.getString("QIDLINE") + "-" + result.getString("STS") + "\">";
                        run = "<input type=\"text\" name=\"running-" + mat + "\" value=\"" + runno + "\" style=\"width: 100px; text-align: center;\" disabled>";

                        if (pack.equals("BOX")) {
                            p.setBox(Integer.toString(qp));
                            sumbox += qp;
                        } else if (pack.equals("BAG") || pack.equals("PACK")) {
                            p.setBag(Integer.toString(qp));
                            sumbag += qp;
                        } else if (pack.equals("ROLL")) {
                            p.setRoll(Integer.toString(qp));
                            sumroll += qp;
                        }
                    } else {
                        if (!result.getString("QIDGPQR").equals(oldGID)) {
                            oldGID = result.getString("QIDGPQR");
                            ck = "<input style=\"width: 30px; height: 30px;\" type=\"checkbox\" id=\"selectCk-" + result.getString("QIDGPQR") + "\" name=\"selectCk-" + mat + "\" value=\"" + result.getString("QIDGPQR") + "-" + result.getString("QIDQNO") + "-" + result.getString("SHIPDATE") + "-" + result.getString("QIDRUNNO") + "-" + result.getString("QIDLINE") + "-" + result.getString("STS") + "\">";
                            run = "<input type=\"text\" name=\"running-" + mat + "\" value=\"" + runno + "\" style=\"width: 100px; text-align: center;\" disabled>";

                            if (pack.equals("BOX")) {
                                p.setBox(Integer.toString(qp));
                                sumbox += qp;
                            } else if (pack.equals("BAG") || pack.equals("PACK")) {
                                p.setBag(Integer.toString(qp));
                                sumbag += qp;
                            } else if (pack.equals("ROLL")) {
                                p.setRoll(Integer.toString(qp));
                                sumroll += qp;
                            }
                        }
                    }
                }

                p.setCkBox(ck);
                p.setRunno(run);

                UAList.add(p);

                qty = 0;
                qp = 0;

            }

            QIDETAIL p = new QIDETAIL();
            p.setGID("<b style=\"opacity: 0.0;\">0</b>");
            p.setMatc("<b style=\"opacity: 0.0;\">0</b>");
            p.setDesc("<b style=\"opacity: 0.0;\">0</b>");
            p.setId("<b>TOTAL</b>");
            p.setBox("<b>" + Integer.toString(sumbox) + "</b>");
            p.setBag("<b>" + Integer.toString(sumbag) + "</b>");
            p.setRoll("<b>" + Integer.toString(sumroll) + "</b>");
            p.setM3("<b>" + formatDou.format(summ3) + "</b>");
            p.setQty("<b>" + formatDou.format(sumqty) + "</b>");
            UAList.add(p);

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public List<QIDETAIL> findDetail350(String wh, String dest, String shipDate) {

        List<QIDETAIL> UAList = new ArrayList<QIDETAIL>();

        String WH = "";
        String DEST = "";
        String SHIP = "";

        if (wh != null && !wh.equals("")) {
            WH = "AND QIDWHS = '" + wh + "' \n";
        }

        if (dest != null && !dest.equals("")) {
            DEST = "AND (select top 1 QIHDEST from QIHEAD where QIHQNO = QIDQNO) = '" + dest + "' \n";
        }

        if (shipDate != null && !shipDate.equals("")) {
            SHIP = "AND QIDSHDT = '" + shipDate + "' \n";
        }
        DecimalFormat formatDou = new DecimalFormat("#,##0.00");

        String sql = "SELECT QIDWHS, (select top 1 QIHDEST from QIHEAD where QIHQNO = QIDQNO) AS DEST, \n"
                + "       (select top 1 QIHMTCTRL from QIHEAD where QIHQNO = QIDQNO) AS MATCTRL, \n"
                + "       QIDQNO, QIDMAT, (select top 1 QRMDESC from QRMMAS where QRMCODE = QIDMAT) AS QIDDESC, \n"
                + "       QIDGPQR, QIDID, \n"
                + "       QIDQTY, QIDBUN, QIDPACKTYPE, QIDSTS + ' : ' + STSDESC AS QIDSTS, QIDSTS AS STS, \n"
                + "       (select top 1 QIHMVT from QIHEAD where QIHQNO = QIDQNO) AS MVT, \n"
                + "       (select top 1 QMVDESC from QMVT where QMVMVT = (select top 1 QIHMVT from QIHEAD where QIHQNO = QIDQNO)) AS MVTN, \n"
                + "       QIDSHDT, QIDLINE, \n"
                + "       QIDWHS + ' : ' + \n"
                + "	  SUBSTRING(CONVERT(VARCHAR(25), QIDSHDT, 126), 9, 2) + '/' + \n"
                + "	  SUBSTRING(CONVERT(VARCHAR(25), QIDSHDT, 126), 6, 2) + '/' + \n"
                + "	  SUBSTRING(CONVERT(VARCHAR(25), QIDSHDT, 126), 1, 4) as SHIPDATE, \n"
                + "       CASE WHEN cast(QIDRUNNO as nvarchar) IS NULL THEN ' ' ELSE cast(QIDRUNNO as nvarchar) END AS QIDRUNNO, \n"
                + "       QIDROUND \n"
                + "FROM QIDETAIL \n"
                + "INNER JOIN QRMSTS \n"
                + "ON QIDETAIL.QIDSTS = QRMSTS.STSCODE \n"
                + "WHERE QIDQNO != ' ' \n"
                + WH
                + DEST
                + SHIP
                + "AND (QIDSTS = '5' OR QIDSTS = '6' OR QIDSTS = 'R' OR QIDSTS = 'H') \n"
                + "ORDER BY MATCTRL, QIDMAT, QIDPACKTYPE \n";
//        System.out.println(sql);

        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            String mc = "";
            String desc = "";
            String gqr = "";
            String id = "";
            double qty = 0;
            String um = "";
            int qp = 0;
            String pack = "";
            String mvt = "";
            double sumqty = 0;
            double summ3 = 0;
            int sumbox = 0;
            int sumbag = 0;
            int sumroll = 0;
            String oldGID = "";

            while (result.next()) {

                mc = result.getString("QIDMAT");
                desc = result.getString("QIDDESC");
                if (result.getString("QIDGPQR") == null) {
                    gqr = "<b style=\"opacity: 0.0;\">1</b>";
                } else if (result.getString("QIDGPQR").trim().equals("")) {
                    gqr = "<b style=\"opacity: 0.0;\">1</b>";
                } else {
                    gqr = result.getString("QIDGPQR");
                }
                id = result.getString("QIDID");
                qty += Double.parseDouble(result.getString("QIDQTY"));
                um = result.getString("QIDBUN");
                qp += 1;
                pack = result.getString("QIDPACKTYPE");
                mvt = result.getString("MVT");

                QIDETAIL p = new QIDETAIL();
                p.setNo(Integer.toString(qp)); // Q'pack
                p.setMatc(mc);
                p.setDesc(desc);
                p.setGID(gqr);
                p.setId(id);
                p.setUm(um);
                p.setPack(pack);

                p.setSd(result.getString("SHIPDATE") != null ? result.getString("SHIPDATE").split(" : ")[1] : "");

                p.setSts(result.getString("QIDSTS"));
                p.setMvt(mvt);
                p.setQno(result.getString("QIDQNO"));
                p.setMvtn(result.getString("MVTN"));
                p.setM3(formatDou.format(0));
                String tqt = formatDou.format(qty);
                p.setQty(tqt);
                sumqty += qty;
                summ3 += 0;

                String ck = "";
                String round = "";
                String runno = "";
                if (result.getString("QIDRUNNO") != null && !result.getString("QIDRUNNO").equals(" ")) {
                    runno = result.getString("QIDRUNNO");
                }
                String rnd = "";
                if (result.getString("QIDROUND") != null && !result.getString("QIDROUND").equals(" ")) {
                    rnd = result.getString("QIDROUND");
                }
                p.setShipmentDate(runno);
                if (result.getString("QIDGPQR") == null) {
                    ck = "<input style=\"width: 30px; height: 30px;\" type=\"checkbox\" id=\"selectCk-" + result.getString("QIDID") + "\" name=\"selectCk\" value=\"" + result.getString("QIDID") + "-" + result.getString("QIDQNO") + "-" + result.getString("SHIPDATE") + "-" + result.getString("QIDRUNNO") + "-" + result.getString("QIDLINE") + "-" + result.getString("STS") + "-" + result.getString("QIDROUND") + "\">";
                    round = "<input type=\"text\" name=\"round\" value=\"" + rnd + "\" style=\"width: 100px; text-align: center;\" disabled>";

                    if (pack.equals("BOX")) {
                        p.setBox(Integer.toString(qp));
                        sumbox += qp;
                    } else if (pack.equals("BAG") || pack.equals("PACK")) {
                        p.setBag(Integer.toString(qp));
                        sumbag += qp;
                    } else if (pack.equals("ROLL")) {
                        p.setRoll(Integer.toString(qp));
                        sumroll += qp;
                    }
                } else if (result.getString("QIDGPQR").trim().equals("")) {
                    ck = "<input style=\"width: 30px; height: 30px;\" type=\"checkbox\" id=\"selectCk-" + result.getString("QIDID") + "\" name=\"selectCk\" value=\"" + result.getString("QIDID") + "-" + result.getString("QIDQNO") + "-" + result.getString("SHIPDATE") + "-" + result.getString("QIDRUNNO") + "-" + result.getString("QIDLINE") + "-" + result.getString("STS") + "-" + result.getString("QIDROUND") + "\">";
                    round = "<input type=\"text\" name=\"round\" value=\"" + rnd + "\" style=\"width: 100px; text-align: center;\" disabled>";

                    if (pack.equals("BOX")) {
                        p.setBox(Integer.toString(qp));
                        sumbox += qp;
                    } else if (pack.equals("BAG") || pack.equals("PACK")) {
                        p.setBag(Integer.toString(qp));
                        sumbag += qp;
                    } else if (pack.equals("ROLL")) {
                        p.setRoll(Integer.toString(qp));
                        sumroll += qp;
                    }
                } else {
                    if (oldGID.equals("")) {
                        oldGID = result.getString("QIDGPQR");
                        ck = "<input style=\"width: 30px; height: 30px;\" type=\"checkbox\" id=\"selectCk-" + result.getString("QIDGPQR") + "\" name=\"selectCk\" value=\"" + result.getString("QIDGPQR") + "-" + result.getString("QIDQNO") + "-" + result.getString("SHIPDATE") + "-" + result.getString("QIDRUNNO") + "-" + result.getString("QIDLINE") + "-" + result.getString("STS") + "-" + result.getString("QIDROUND") + "\">";
                        round = "<input type=\"text\" name=\"round\" value=\"" + rnd + "\" style=\"width: 100px; text-align: center;\" disabled>";

                        if (pack.equals("BOX")) {
                            p.setBox(Integer.toString(qp));
                            sumbox += qp;
                        } else if (pack.equals("BAG") || pack.equals("PACK")) {
                            p.setBag(Integer.toString(qp));
                            sumbag += qp;
                        } else if (pack.equals("ROLL")) {
                            p.setRoll(Integer.toString(qp));
                            sumroll += qp;
                        }
                    } else {
                        if (!result.getString("QIDGPQR").equals(oldGID)) {
                            oldGID = result.getString("QIDGPQR");
                            ck = "<input style=\"width: 30px; height: 30px;\" type=\"checkbox\" id=\"selectCk-" + result.getString("QIDGPQR") + "\" name=\"selectCk\" value=\"" + result.getString("QIDGPQR") + "-" + result.getString("QIDQNO") + "-" + result.getString("SHIPDATE") + "-" + result.getString("QIDRUNNO") + "-" + result.getString("QIDLINE") + "-" + result.getString("STS") + "-" + result.getString("QIDROUND") + "\">";
                            round = "<input type=\"text\" name=\"round\" value=\"" + rnd + "\" style=\"width: 100px; text-align: center;\" disabled>";

                            if (pack.equals("BOX")) {
                                p.setBox(Integer.toString(qp));
                                sumbox += qp;
                            } else if (pack.equals("BAG") || pack.equals("PACK")) {
                                p.setBag(Integer.toString(qp));
                                sumbag += qp;
                            } else if (pack.equals("ROLL")) {
                                p.setRoll(Integer.toString(qp));
                                sumroll += qp;
                            }
                        }
                    }
                }

                p.setCkBox(ck);
                p.setRunno(round);

                UAList.add(p);

                qty = 0;
                qp = 0;

            }

            QIDETAIL p = new QIDETAIL();
            p.setGID("<b style=\"opacity: 0.0;\">0</b>");
            p.setMatc("<b style=\"opacity: 0.0;\">0</b>");
            p.setDesc("<b style=\"opacity: 0.0;\">0</b>");
            p.setId("<b>TOTAL</b>");
            p.setBox("<b>" + Integer.toString(sumbox) + "</b>");
            p.setBag("<b>" + Integer.toString(sumbag) + "</b>");
            p.setRoll("<b>" + Integer.toString(sumroll) + "</b>");
            p.setM3("<b>" + formatDou.format(summ3) + "</b>");
            p.setQty("<b>" + formatDou.format(sumqty) + "</b>");
            UAList.add(p);

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public List<QIDETAIL> findDetail350X(String wh, String dest, String shipDate, String sts, String mat, String MY) {

        List<QIDETAIL> UAList = new ArrayList<QIDETAIL>();

        String WH = "";
        String DEST = "";
        String SHIP = "";
        String MAT = "";

        if (wh != null && !wh.equals("")) {
            WH = "AND QIDWHS = '" + wh + "' \n";
        }

        if (!mat.equalsIgnoreCase("TOTAL") && !mat.trim().equals("All")) {
            MAT = "AND (CASE WHEN QIDMTCTRL = '' OR QIDMTCTRL IS NULL THEN (select top 1 [SAPMCTRL] from [SAPMAS] where [SAPMAT] = QIDMAT) ELSE QIDMTCTRL END) = '" + mat + "'\n";
        }

        if (dest != null && !dest.equals("") && !dest.equalsIgnoreCase("TOTAL")) {
            DEST = "AND (select top 1 QIHDEST from QIHEAD where QIHQNO = QIDQNO) = '" + dest + "' \n";
        }

//        if (shipDate != null && !shipDate.equals("")) {
//            SHIP = "AND QIDSHDT = '" + shipDate + "' \n";
//        }
        if (sts.equals("6") && MY != null) {
//            SHIP = "AND FORMAT(QIDSHDT,'yyyy-MM') = LEFT('" + MY + "',7) \n";
            SHIP = "AND YEAR(QIDTRDT) = '" + MY.split("-")[0] + "' AND MONTH(QIDTRDT) = '" + MY.split("-")[1] + "'";
        } else if ((sts.equals("2") || sts.equals("5")) && MY != null) {
            SHIP = "AND YEAR(QIDTRDT) = '" + MY.split("-")[0] + "' AND MONTH(QIDTRDT) = '" + MY.split("-")[1] + "'";
        }
        DecimalFormat formatDou = new DecimalFormat("#,##0.00");

        String sql = "SELECT QIDWHS, (select top 1 QIHDEST from QIHEAD where QIHQNO = QIDQNO) AS DEST, \n"
                + "       (CASE WHEN QIDMTCTRL = '' OR QIDMTCTRL IS NULL THEN (select top 1 [SAPMCTRL] from [SAPMAS] where [SAPMAT] = QIDMAT) ELSE QIDMTCTRL END) AS MATCTRL, \n"
                + "       QIDQNO, QIDMAT, (select top 1 QRMDESC from QRMMAS where QRMCODE = QIDMAT) AS QIDDESC, \n"
                + "       QIDGPQR, QIDID, \n"
                + "       QIDQTY, QIDBUN, QIDPACKTYPE, QIDSTS + ' : ' + STSDESC AS QIDSTS, QIDSTS AS STS, \n"
                + "       (select top 1 QIHMVT from QIHEAD where QIHQNO = QIDQNO) AS MVT, \n"
                + "       (select top 1 QMVDESC from QMVT where QMVMVT = (select top 1 QIHMVT from QIHEAD where QIHQNO = QIDQNO)) AS MVTN, \n"
                + "       QIDSHDT, QIDLINE, \n"
                + "       QIDWHS + ' : ' + \n"
                + "	  SUBSTRING(CONVERT(VARCHAR(25), QIDSHDT, 126), 9, 2) + '/' + \n"
                + "	  SUBSTRING(CONVERT(VARCHAR(25), QIDSHDT, 126), 6, 2) + '/' + \n"
                + "	  SUBSTRING(CONVERT(VARCHAR(25), QIDSHDT, 126), 1, 4) as SHIPDATE, \n"
                + "       CASE WHEN cast(QIDRUNNO as nvarchar) IS NULL THEN ' ' ELSE cast(QIDRUNNO as nvarchar) END AS QIDRUNNO, \n"
                + "       QIDROUND \n"
                + "FROM QIDETAIL \n"
                + "INNER JOIN QRMSTS \n"
                + "ON QIDETAIL.QIDSTS = QRMSTS.STSCODE \n"
                + "WHERE QIDQNO != ' ' \n"
                + WH
                + DEST
                + SHIP
                + "AND (QIDSTS = '" + sts + "') AND QIDRUNNO is null AND QIDQTY is not null \n"
                + MAT
                + "ORDER BY MATCTRL, QIDMAT, QIDPACKTYPE \n";
//        System.out.println(sql);

        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            String mc = "";
            String desc = "";
            String gqr = "";
            String id = "";
            double qty = 0;
            String um = "";
            int qp = 0;
            String pack = "";
            String mvt = "";
            double sumqty = 0;
            double summ3 = 0;
            int sumbox = 0;
            int sumbag = 0;
            int sumroll = 0;
            String oldGID = "";

            while (result.next()) {

                mc = result.getString("QIDMAT");
                desc = result.getString("QIDDESC");
                if (result.getString("QIDGPQR") == null) {
                    gqr = "<b style=\"opacity: 0.0;\">1</b>";
                } else if (result.getString("QIDGPQR").trim().equals("")) {
                    gqr = "<b style=\"opacity: 0.0;\">1</b>";
                } else {
                    gqr = result.getString("QIDGPQR");
                }
                id = result.getString("QIDID");
                qty += Double.parseDouble(result.getString("QIDQTY"));
                um = result.getString("QIDBUN");
                qp += 1;
                pack = result.getString("QIDPACKTYPE");
                mvt = result.getString("MVT");

                QIDETAIL p = new QIDETAIL();
                p.setNo(Integer.toString(qp)); // Q'pack
                p.setMatc(mc);
                p.setDesc(desc);
                p.setGID(gqr);
                p.setId(id);
                p.setUm(um);
                p.setPack(pack);

                p.setSd(result.getString("SHIPDATE") != null ? result.getString("SHIPDATE").split(" : ")[1] : "");

                p.setSts(result.getString("QIDSTS"));
                p.setMvt(mvt);
                p.setQno(result.getString("QIDQNO"));
                p.setMvtn(result.getString("MVTN"));
                p.setM3(formatDou.format(0));
                String tqt = formatDou.format(qty);
                p.setQty(tqt);
                sumqty += qty;
                summ3 += 0;

                String ck = "";
                String round = "";
                String runno = "";
                if (result.getString("QIDRUNNO") != null && !result.getString("QIDRUNNO").equals(" ")) {
                    runno = result.getString("QIDRUNNO");
                }
                String rnd = "";
                if (result.getString("QIDROUND") != null && !result.getString("QIDROUND").equals(" ")) {
                    rnd = result.getString("QIDROUND");
                }
                p.setShipmentDate(runno);
                if (result.getString("QIDGPQR") == null) {
                    ck = "<input style=\"width: 30px; height: 30px;\" type=\"checkbox\" id=\"selectCk-" + result.getString("QIDID") + "\" name=\"selectCk\" value=\"" + result.getString("QIDID") + "-" + result.getString("QIDQNO") + "-" + result.getString("SHIPDATE") + "-" + result.getString("QIDRUNNO") + "-" + result.getString("QIDLINE") + "-" + result.getString("STS") + "-" + result.getString("QIDROUND") + "\">";
                    round = "<input type=\"text\" name=\"round\" value=\"" + rnd + "\" style=\"width: 100px; text-align: center;\" disabled><b style=\"opacity: 0;\">" + rnd + "</b>";

                    if (pack.equals("BOX")) {
                        p.setBox(Integer.toString(qp));
                        sumbox += qp;
                    } else if (pack.equals("BAG") || pack.equals("PACK")) {
                        p.setBag(Integer.toString(qp));
                        sumbag += qp;
                    } else if (pack.equals("ROLL")) {
                        p.setRoll(Integer.toString(qp));
                        sumroll += qp;
                    }
                } else if (result.getString("QIDGPQR").trim().equals("")) {
                    ck = "<input style=\"width: 30px; height: 30px;\" type=\"checkbox\" id=\"selectCk-" + result.getString("QIDID") + "\" name=\"selectCk\" value=\"" + result.getString("QIDID") + "-" + result.getString("QIDQNO") + "-" + result.getString("SHIPDATE") + "-" + result.getString("QIDRUNNO") + "-" + result.getString("QIDLINE") + "-" + result.getString("STS") + "-" + result.getString("QIDROUND") + "\">";
                    round = "<input type=\"text\" name=\"round\" value=\"" + rnd + "\" style=\"width: 100px; text-align: center;\" disabled><b style=\"opacity: 0;\">" + rnd + "</b>";

                    if (pack.equals("BOX")) {
                        p.setBox(Integer.toString(qp));
                        sumbox += qp;
                    } else if (pack.equals("BAG") || pack.equals("PACK")) {
                        p.setBag(Integer.toString(qp));
                        sumbag += qp;
                    } else if (pack.equals("ROLL")) {
                        p.setRoll(Integer.toString(qp));
                        sumroll += qp;
                    }
                } else {
                    if (oldGID.equals("")) {
                        oldGID = result.getString("QIDGPQR");
                        ck = "<input style=\"width: 30px; height: 30px;\" type=\"checkbox\" id=\"selectCk-" + result.getString("QIDGPQR") + "\" name=\"selectCk\" value=\"" + result.getString("QIDGPQR") + "-" + result.getString("QIDQNO") + "-" + result.getString("SHIPDATE") + "-" + result.getString("QIDRUNNO") + "-" + result.getString("QIDLINE") + "-" + result.getString("STS") + "-" + result.getString("QIDROUND") + "\">";
                        round = "<input type=\"text\" name=\"round\" value=\"" + rnd + "\" style=\"width: 100px; text-align: center;\" disabled><b style=\"opacity: 0;\">" + rnd + "</b>";

                        if (pack.equals("BOX")) {
                            p.setBox(Integer.toString(qp));
                            sumbox += qp;
                        } else if (pack.equals("BAG") || pack.equals("PACK")) {
                            p.setBag(Integer.toString(qp));
                            sumbag += qp;
                        } else if (pack.equals("ROLL")) {
                            p.setRoll(Integer.toString(qp));
                            sumroll += qp;
                        }
                    } else {
                        if (!result.getString("QIDGPQR").equals(oldGID)) {
                            oldGID = result.getString("QIDGPQR");
                            ck = "<input style=\"width: 30px; height: 30px;\" type=\"checkbox\" id=\"selectCk-" + result.getString("QIDGPQR") + "\" name=\"selectCk\" value=\"" + result.getString("QIDGPQR") + "-" + result.getString("QIDQNO") + "-" + result.getString("SHIPDATE") + "-" + result.getString("QIDRUNNO") + "-" + result.getString("QIDLINE") + "-" + result.getString("STS") + "-" + result.getString("QIDROUND") + "\">";
                            round = "<input type=\"text\" name=\"round\" value=\"" + rnd + "\" style=\"width: 100px; text-align: center;\" disabled><b style=\"opacity: 0;\">" + rnd + "</b>";

                            if (pack.equals("BOX")) {
                                p.setBox(Integer.toString(qp));
                                sumbox += qp;
                            } else if (pack.equals("BAG") || pack.equals("PACK")) {
                                p.setBag(Integer.toString(qp));
                                sumbag += qp;
                            } else if (pack.equals("ROLL")) {
                                p.setRoll(Integer.toString(qp));
                                sumroll += qp;
                            }
                        }
                    }
                }

                p.setCkBox(ck);
                p.setRunno(round);

                UAList.add(p);

                qty = 0;
                qp = 0;

            }

            QIDETAIL p = new QIDETAIL();
            p.setGID("<b style=\"opacity: 0.0;\">0</b>");
            p.setMatc("<b style=\"opacity: 0.0;\">0</b>");
            p.setDesc("<b style=\"opacity: 0.0;\">0</b>");
            p.setId("<b>TOTAL</b>");
            p.setBox("<b>" + Integer.toString(sumbox) + "</b>");
            p.setBag("<b>" + Integer.toString(sumbag) + "</b>");
            p.setRoll("<b>" + Integer.toString(sumroll) + "</b>");
            p.setM3("<b>" + formatDou.format(summ3) + "</b>");
            p.setQty("<b>" + formatDou.format(sumqty) + "</b>");
            UAList.add(p);

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public List<QIDETAIL> findDetail360(String wh, String dest, String shipDate, String rn) {

        List<QIDETAIL> UAList = new ArrayList<QIDETAIL>();

        String WH = "";
        String DEST = "";
        String SHIP = "";
        String ROUND = "";

        if (wh != null && !wh.equals("")) {
            WH = "AND QIDWHS = '" + wh + "' ";
        }

        if (dest != null && !dest.equals("")) {
            DEST = "AND (select top 1 QIHDEST from QIHEAD where QIHQNO = QIDQNO) = '" + dest + "' ";
        }

        if (shipDate != null && !shipDate.equals("")) {
            SHIP = "AND QIDSHDT = '" + shipDate + "' ";
        }

        if (rn != null && !rn.equals("")) {
            ROUND = "AND QIDROUND = '" + rn + "' ";
        }

        DecimalFormat formatDou = new DecimalFormat("#,##0.00");

        String sql = "SELECT QIDWHS, (select top 1 QIHDEST from QIHEAD where QIHQNO = QIDQNO) AS DEST, "
                + "       (select top 1 QIHMTCTRL from QIHEAD where QIHQNO = QIDQNO) AS MATCTRL, "
                + "       QIDQNO, QIDMAT, (select top 1 QRMDESC from QRMMAS where QRMCODE = QIDMAT) AS QIDDESC, "
                + "       QIDGPQR, QIDID, "
                + "       QIDQTY, QIDBUN, QIDPACKTYPE, QIDSTS + ' : ' + STSDESC AS QIDSTS, QIDSTS AS STS, "
                + "       (select top 1 QIHMVT from QIHEAD where QIHQNO = QIDQNO) AS MVT, "
                + "       (select top 1 QMVDESC from QMVT where QMVMVT = (select top 1 QIHMVT from QIHEAD where QIHQNO = QIDQNO)) AS MVTN, "
                + "       QIDSHDT, QIDLINE, "
                + "       QIDWHS + ' : ' + "
                + "	  SUBSTRING(CONVERT(VARCHAR(25), QIDSHDT, 126), 9, 2) + '/' + "
                + "	  SUBSTRING(CONVERT(VARCHAR(25), QIDSHDT, 126), 6, 2) + '/' + "
                + "	  SUBSTRING(CONVERT(VARCHAR(25), QIDSHDT, 126), 1, 4) as SHIPDATE, "
                + "       CASE WHEN cast(QIDRUNNO as nvarchar) IS NULL THEN ' ' ELSE cast(QIDRUNNO as nvarchar) END AS QIDRUNNO, "
                + "       QIDROUND, QIDOCNO, QIDTPUSR + ' : ' + SUBSTRING(MSSUSER.USERS,0,CHARINDEX(' ',MSSUSER.USERS,0)) as QIDTPUSR "
                + "FROM QIDETAIL "
                + "INNER JOIN QRMSTS "
                + "ON QIDETAIL.QIDSTS = QRMSTS.STSCODE "
                + "INNER JOIN MSSUSER ON QIDETAIL.QIDTPUSR = MSSUSER.USERID "
                + "WHERE QIDQNO != ' ' "
                + WH
                + DEST
                + SHIP
                + ROUND
                + "AND (QIDSTS = '6' OR QIDSTS = '7' OR QIDSTS = 'D') "
                + "ORDER BY MATCTRL, QIDMAT, QIDPACKTYPE ";
//        System.out.println(sql);

        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            String mc = "";
            String desc = "";
            String gqr = "";
            String id = "";
            double qty = 0;
            String um = "";
            int qp = 0;
            String pack = "";
            String mvt = "";
            double sumqty = 0;
            double summ3 = 0;
            int sumbox = 0;
            int sumbag = 0;
            int sumroll = 0;
            String oldGID = "";

            while (result.next()) {

                mc = result.getString("QIDMAT");
                desc = result.getString("QIDDESC");
                if (result.getString("QIDGPQR") == null) {
                    gqr = "<b style=\"opacity: 0.0;\">1</b>";
                } else if (result.getString("QIDGPQR").trim().equals("")) {
                    gqr = "<b style=\"opacity: 0.0;\">1</b>";
                } else {
                    gqr = result.getString("QIDGPQR");
                }
                id = result.getString("QIDID");
                qty += Double.parseDouble(result.getString("QIDQTY"));
                um = result.getString("QIDBUN");
                qp += 1;
                pack = result.getString("QIDPACKTYPE");
                mvt = result.getString("MVT");

                QIDETAIL p = new QIDETAIL();
                p.setNo(Integer.toString(qp)); // Q'pack
                p.setMatc(mc);
                p.setDesc(desc);
                p.setGID(gqr);
                p.setId(id);
                p.setUm(um);
                p.setPack(pack);

                p.setSts(result.getString("QIDSTS"));
                p.setDocno(result.getString("QIDOCNO"));
                p.setTransporter(result.getString("QIDTPUSR"));
                p.setMvt(mvt);
                p.setQno(result.getString("QIDQNO"));
                p.setMvtn(result.getString("MVTN"));
                p.setM3(formatDou.format(0));
                String tqt = formatDou.format(qty);
                p.setQty(tqt);
                sumqty += qty;
                summ3 += 0;

                String ck = "";
                String round = "";
                String runno = "";
                if (result.getString("QIDRUNNO") != null && !result.getString("QIDRUNNO").equals(" ")) {
                    runno = result.getString("QIDRUNNO");
                }
                String rnd = "";
                if (result.getString("QIDROUND") != null && !result.getString("QIDROUND").equals(" ")) {
                    rnd = result.getString("QIDROUND");
                }
                p.setShipmentDate(runno);
                if (result.getString("QIDGPQR") == null) {
                    ck = "<input style=\"width: 30px; height: 30px;\" type=\"checkbox\" id=\"selectCk-" + result.getString("QIDID") + "\" name=\"selectCk\" value=\"" + result.getString("QIDID") + "-" + result.getString("QIDQNO") + "-" + result.getString("SHIPDATE") + "-" + result.getString("QIDRUNNO") + "-" + result.getString("QIDLINE") + "-" + result.getString("STS") + "-" + result.getString("QIDROUND") + "\" checked>";
                    round = "<input type=\"text\" name=\"round\" value=\"" + rnd + "\" style=\"width: 100px; text-align: center;\" disabled>";

                    if (pack.equals("BOX")) {
                        p.setBox(Integer.toString(qp));
                        sumbox += qp;
                    } else if (pack.equals("BAG") || pack.equals("PACK")) {
                        p.setBag(Integer.toString(qp));
                        sumbag += qp;
                    } else if (pack.equals("ROLL")) {
                        p.setRoll(Integer.toString(qp));
                        sumroll += qp;
                    }
                } else if (result.getString("QIDGPQR").trim().equals("")) {
                    ck = "<input style=\"width: 30px; height: 30px;\" type=\"checkbox\" id=\"selectCk-" + result.getString("QIDID") + "\" name=\"selectCk\" value=\"" + result.getString("QIDID") + "-" + result.getString("QIDQNO") + "-" + result.getString("SHIPDATE") + "-" + result.getString("QIDRUNNO") + "-" + result.getString("QIDLINE") + "-" + result.getString("STS") + "-" + result.getString("QIDROUND") + "\" checked>";
                    round = "<input type=\"text\" name=\"round\" value=\"" + rnd + "\" style=\"width: 100px; text-align: center;\" disabled>";

                    if (pack.equals("BOX")) {
                        p.setBox(Integer.toString(qp));
                        sumbox += qp;
                    } else if (pack.equals("BAG") || pack.equals("PACK")) {
                        p.setBag(Integer.toString(qp));
                        sumbag += qp;
                    } else if (pack.equals("ROLL")) {
                        p.setRoll(Integer.toString(qp));
                        sumroll += qp;
                    }
                } else {
                    if (oldGID.equals("")) {
                        oldGID = result.getString("QIDGPQR");
                        ck = "<input style=\"width: 30px; height: 30px;\" type=\"checkbox\" id=\"selectCk-" + result.getString("QIDGPQR") + "\" name=\"selectCk\" value=\"" + result.getString("QIDGPQR") + "-" + result.getString("QIDQNO") + "-" + result.getString("SHIPDATE") + "-" + result.getString("QIDRUNNO") + "-" + result.getString("QIDLINE") + "-" + result.getString("STS") + "-" + result.getString("QIDROUND") + "\" checked>";
                        round = "<input type=\"text\" name=\"round\" value=\"" + rnd + "\" style=\"width: 100px; text-align: center;\" disabled>";

                        if (pack.equals("BOX")) {
                            p.setBox(Integer.toString(qp));
                            sumbox += qp;
                        } else if (pack.equals("BAG") || pack.equals("PACK")) {
                            p.setBag(Integer.toString(qp));
                            sumbag += qp;
                        } else if (pack.equals("ROLL")) {
                            p.setRoll(Integer.toString(qp));
                            sumroll += qp;
                        }
                    } else {
                        if (!result.getString("QIDGPQR").equals(oldGID)) {
                            oldGID = result.getString("QIDGPQR");
                            ck = "<input style=\"width: 30px; height: 30px;\" type=\"checkbox\" id=\"selectCk-" + result.getString("QIDGPQR") + "\" name=\"selectCk\" value=\"" + result.getString("QIDGPQR") + "-" + result.getString("QIDQNO") + "-" + result.getString("SHIPDATE") + "-" + result.getString("QIDRUNNO") + "-" + result.getString("QIDLINE") + "-" + result.getString("STS") + "-" + result.getString("QIDROUND") + "\" checked>";
                            round = "<input type=\"text\" name=\"round\" value=\"" + rnd + "\" style=\"width: 100px; text-align: center;\" disabled>";

                            if (pack.equals("BOX")) {
                                p.setBox(Integer.toString(qp));
                                sumbox += qp;
                            } else if (pack.equals("BAG") || pack.equals("PACK")) {
                                p.setBag(Integer.toString(qp));
                                sumbag += qp;
                            } else if (pack.equals("ROLL")) {
                                p.setRoll(Integer.toString(qp));
                                sumroll += qp;
                            }
                        }
                    }
                }

                p.setCkBox(ck);
                p.setRunno(round);

                UAList.add(p);

                qty = 0;
                qp = 0;

            }

            QIDETAIL p = new QIDETAIL();
            p.setGID("<b style=\"opacity: 0.0;\">0</b>");
            p.setMatc("<b style=\"opacity: 0.0;\">0</b>");
            p.setDesc("<b style=\"opacity: 0.0;\">0</b>");
            p.setId("<b>TOTAL</b>");
            p.setBox("<b>" + Integer.toString(sumbox) + "</b>");
            p.setBag("<b>" + Integer.toString(sumbag) + "</b>");
            p.setRoll("<b>" + Integer.toString(sumroll) + "</b>");
            p.setM3("<b>" + formatDou.format(summ3) + "</b>");
            p.setQty("<b>" + formatDou.format(sumqty) + "</b>");
            UAList.add(p);

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public List<QIDETAIL> findDetailMat340(String wh, String dest, String mat) {

        List<QIDETAIL> UAList = new ArrayList<QIDETAIL>();

        DecimalFormat formatDou = new DecimalFormat("#,##0.00");

        String sql = "SELECT QIDWHS, (select top 1 QIHDEST from QIHEAD where QIHQNO = QIDQNO) AS DEST, "
                + "       (select top 1 QIHMTCTRL from QIHEAD where QIHQNO = QIDQNO) AS MATCTRL, "
                + "       QIDQNO, QIDMAT, (select top 1 QRMDESC from QRMMAS where QRMCODE = QIDMAT) AS QIDDESC, "
                + "       QIDGPQR, QIDID, "
                + "       QIDQTY, QIDBUN, QIDPACKTYPE, QIDSTS + ' : ' + STSDESC AS QIDSTS, QIDSTS AS STS, "
                + "       (select top 1 QIHMVT from QIHEAD where QIHQNO = QIDQNO) AS MVT, "
                + "       (select top 1 QMVDESC from QMVT where QMVMVT = (select top 1 QIHMVT from QIHEAD where QIHQNO = QIDQNO)) AS MVTN, "
                + "       QIDSHDT, QIDLINE, "
                + "       QIDWHS + ' : ' + "
                + "	  SUBSTRING(CONVERT(VARCHAR(25), QIDSHDT, 126), 9, 2) + '/' + "
                + "	  SUBSTRING(CONVERT(VARCHAR(25), QIDSHDT, 126), 6, 2) + '/' + "
                + "	  SUBSTRING(CONVERT(VARCHAR(25), QIDSHDT, 126), 1, 4) as SHIPDATE, "
                + "       CASE WHEN cast(QIDRUNNO as nvarchar) IS NULL THEN ' ' ELSE cast(QIDRUNNO as nvarchar) END AS QIDRUNNO "
                + "FROM QIDETAIL "
                + "INNER JOIN QRMSTS "
                + "ON QIDETAIL.QIDSTS = QRMSTS.STSCODE "
                + "WHERE (select top 1 QIHDEST from QIHEAD where QIHQNO = QIDQNO) = '" + dest + "' "
                + "AND (select top 1 QIHMTCTRL from QIHEAD where QIHQNO = QIDQNO) = '" + mat + "' "
                + "AND QIDWHS = '" + wh + "' "
                + "AND (QIDSTS = '2' OR QIDSTS = '5') "
                + "ORDER BY MATCTRL, QIDMAT, QIDPACKTYPE ";

        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            String mc = "";
            String desc = "";
            String gqr = "";
            String id = "";
            double qty = 0;
            String um = "";
            int qp = 0;
            String pack = "";
            String mvt = "";
            double sumqty = 0;
            double summ3 = 0;
            int sumbox = 0;
            int sumbag = 0;
            int sumroll = 0;
            String oldGID = "";

            while (result.next()) {

                mc = result.getString("QIDMAT");
                desc = result.getString("QIDDESC");
                if (result.getString("QIDGPQR") == null) {
                    gqr = "<b style=\"opacity: 0.0;\">1</b>";
                } else if (result.getString("QIDGPQR").trim().equals("")) {
                    gqr = "<b style=\"opacity: 0.0;\">1</b>";
                } else {
                    gqr = result.getString("QIDGPQR");
                }
                id = result.getString("QIDID");
                qty += Double.parseDouble(result.getString("QIDQTY"));
                um = result.getString("QIDBUN");
                qp += 1;
                pack = result.getString("QIDPACKTYPE");
                mvt = result.getString("MVT");

                QIDETAIL p = new QIDETAIL();
                p.setNo(Integer.toString(qp)); // Q'pack
                p.setMatc(mc);
                p.setDesc(desc);
                p.setGID(gqr);
                p.setId(id);
                p.setUm(um);
                p.setPack(pack);

                String shd = "";
                if (result.getString("QIDSHDT") != null) {
                    shd = result.getString("QIDSHDT");
                }
                String[] shdat = shd.split(" ");
                p.setShipmentDate(shdat[0]);

                p.setSts(result.getString("QIDSTS"));
                p.setMvt(mvt);
                p.setQno(result.getString("QIDQNO"));
                p.setMvtn(result.getString("MVTN"));
                p.setM3(formatDou.format(0));
                String tqt = formatDou.format(qty);
                p.setQty(tqt);
                sumqty += qty;
                summ3 += 0;

                String ck = "";
                String run = "";
                String runno = "";
                if (result.getString("QIDRUNNO") != null && !result.getString("QIDRUNNO").equals(" ")) {
                    runno = result.getString("QIDRUNNO");
                }
                if (result.getString("QIDGPQR") == null) {
                    ck = "<input style=\"width: 30px; height: 30px;\" type=\"checkbox\" id=\"selectCk-" + result.getString("QIDID") + "\" name=\"selectCk-" + dest + "\" value=\"" + result.getString("QIDID") + "-" + result.getString("QIDQNO") + "-" + result.getString("SHIPDATE") + "-" + result.getString("QIDRUNNO") + "-" + result.getString("QIDLINE") + "-" + result.getString("STS") + "\">";
                    run = "<input type=\"text\" name=\"running-" + dest + "\" value=\"" + runno + "\" style=\"width: 100px; text-align: center;\" disabled>";

                    if (pack.equals("BOX")) {
                        p.setBox(Integer.toString(qp));
                        sumbox += qp;
                    } else if (pack.equals("BAG") || pack.equals("PACK")) {
                        p.setBag(Integer.toString(qp));
                        sumbag += qp;
                    } else if (pack.equals("ROLL")) {
                        p.setRoll(Integer.toString(qp));
                        sumroll += qp;
                    }
                } else if (result.getString("QIDGPQR").trim().equals("")) {
                    ck = "<input style=\"width: 30px; height: 30px;\" type=\"checkbox\" id=\"selectCk-" + result.getString("QIDID") + "\" name=\"selectCk-" + dest + "\" value=\"" + result.getString("QIDID") + "-" + result.getString("QIDQNO") + "-" + result.getString("SHIPDATE") + "-" + result.getString("QIDRUNNO") + "-" + result.getString("QIDLINE") + "-" + result.getString("STS") + "\">";
                    run = "<input type=\"text\" name=\"running-" + dest + "\" value=\"" + runno + "\" style=\"width: 100px; text-align: center;\" disabled>";

                    if (pack.equals("BOX")) {
                        p.setBox(Integer.toString(qp));
                        sumbox += qp;
                    } else if (pack.equals("BAG") || pack.equals("PACK")) {
                        p.setBag(Integer.toString(qp));
                        sumbag += qp;
                    } else if (pack.equals("ROLL")) {
                        p.setRoll(Integer.toString(qp));
                        sumroll += qp;
                    }
                } else {
                    if (oldGID.equals("")) {
                        oldGID = result.getString("QIDGPQR");
                        ck = "<input style=\"width: 30px; height: 30px;\" type=\"checkbox\" id=\"selectCk-" + result.getString("QIDGPQR") + "\" name=\"selectCk-" + dest + "\" value=\"" + result.getString("QIDGPQR") + "-" + result.getString("QIDQNO") + "-" + result.getString("SHIPDATE") + "-" + result.getString("QIDRUNNO") + "-" + result.getString("QIDLINE") + "-" + result.getString("STS") + "\">";
                        run = "<input type=\"text\" name=\"running-" + dest + "\" value=\"" + runno + "\" style=\"width: 100px; text-align: center;\" disabled>";

                        if (pack.equals("BOX")) {
                            p.setBox(Integer.toString(qp));
                            sumbox += qp;
                        } else if (pack.equals("BAG") || pack.equals("PACK")) {
                            p.setBag(Integer.toString(qp));
                            sumbag += qp;
                        } else if (pack.equals("ROLL")) {
                            p.setRoll(Integer.toString(qp));
                            sumroll += qp;
                        }
                    } else {
                        if (!result.getString("QIDGPQR").equals(oldGID)) {
                            oldGID = result.getString("QIDGPQR");
                            ck = "<input style=\"width: 30px; height: 30px;\" type=\"checkbox\" id=\"selectCk-" + result.getString("QIDGPQR") + "\" name=\"selectCk-" + dest + "\" value=\"" + result.getString("QIDGPQR") + "-" + result.getString("QIDQNO") + "-" + result.getString("SHIPDATE") + "-" + result.getString("QIDRUNNO") + "-" + result.getString("QIDLINE") + "-" + result.getString("STS") + "\">";
                            run = "<input type=\"text\" name=\"running-" + dest + "\" value=\"" + runno + "\" style=\"width: 100px; text-align: center;\" disabled>";

                            if (pack.equals("BOX")) {
                                p.setBox(Integer.toString(qp));
                                sumbox += qp;
                            } else if (pack.equals("BAG") || pack.equals("PACK")) {
                                p.setBag(Integer.toString(qp));
                                sumbag += qp;
                            } else if (pack.equals("ROLL")) {
                                p.setRoll(Integer.toString(qp));
                                sumroll += qp;
                            }
                        }
                    }
                }

                p.setCkBox(ck);
                p.setRunno(run);

                UAList.add(p);

                qty = 0;
                qp = 0;

            }

            QIDETAIL p = new QIDETAIL();
            p.setGID("<b style=\"opacity: 0.0;\">0</b>");
            p.setMatc("<b style=\"opacity: 0.0;\">0</b>");
            p.setDesc("<b style=\"opacity: 0.0;\">0</b>");
            p.setId("<b>TOTAL</b>");
            p.setBox("<b>" + Integer.toString(sumbox) + "</b>");
            p.setBag("<b>" + Integer.toString(sumbag) + "</b>");
            p.setRoll("<b>" + Integer.toString(sumroll) + "</b>");
            p.setM3("<b>" + formatDou.format(summ3) + "</b>");
            p.setQty("<b>" + formatDou.format(sumqty) + "</b>");
            UAList.add(p);

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public List<QIDETAIL> findDetailMat341(String wh, String dest, String mat) {

        List<QIDETAIL> UAList = new ArrayList<QIDETAIL>();

        DecimalFormat formatDou = new DecimalFormat("#,##0.00");

        String sql = "SELECT QIDWHS, (select top 1 QIHDEST from QIHEAD where QIHQNO = QIDQNO) AS DEST, "
                + "       (select top 1 QIHMTCTRL from QIHEAD where QIHQNO = QIDQNO) AS MATCTRL, "
                + "       QIDQNO, QIDMAT, (select top 1 QRMDESC from QRMMAS where QRMCODE = QIDMAT) AS QIDDESC, "
                + "       QIDGPQR, QIDID, "
                + "       QIDQTY, QIDBUN, QIDPACKTYPE, QIDSTS + ' : ' + STSDESC AS QIDSTS, QIDSTS AS STS, "
                + "       (select top 1 QIHMVT from QIHEAD where QIHQNO = QIDQNO) AS MVT, "
                + "       (select top 1 QMVDESC from QMVT where QMVMVT = (select top 1 QIHMVT from QIHEAD where QIHQNO = QIDQNO)) AS MVTN, "
                + "       QIDSHDT, QIDLINE, "
                + "       QIDWHS + ' : ' + "
                + "	  SUBSTRING(CONVERT(VARCHAR(25), QIDSHDT, 126), 9, 2) + '/' + "
                + "	  SUBSTRING(CONVERT(VARCHAR(25), QIDSHDT, 126), 6, 2) + '/' + "
                + "	  SUBSTRING(CONVERT(VARCHAR(25), QIDSHDT, 126), 1, 4) as SHIPDATE, "
                + "       CASE WHEN cast(QIDRUNNO as nvarchar) IS NULL THEN ' ' ELSE cast(QIDRUNNO as nvarchar) END AS QIDRUNNO "
                + "FROM QIDETAIL "
                + "INNER JOIN QRMSTS "
                + "ON QIDETAIL.QIDSTS = QRMSTS.STSCODE "
                + "WHERE (select top 1 QIHDEST from QIHEAD where QIHQNO = QIDQNO) = '" + dest + "' "
                + "AND (select top 1 QIHMTCTRL from QIHEAD where QIHQNO = QIDQNO) = '" + mat + "' "
                + "AND QIDWHS = '" + wh + "' "
                + "AND (QIDSTS = '5' OR QIDSTS = '6' OR QIDSTS = '7') "
                + "ORDER BY MATCTRL, QIDMAT, QIDPACKTYPE ";

        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            String mc = "";
            String desc = "";
            String gqr = "";
            String id = "";
            double qty = 0;
            String um = "";
            int qp = 0;
            String pack = "";
            String mvt = "";
            double sumqty = 0;
            double summ3 = 0;
            int sumbox = 0;
            int sumbag = 0;
            int sumroll = 0;
            String oldGID = "";

            while (result.next()) {

                mc = result.getString("QIDMAT");
                desc = result.getString("QIDDESC");
                gqr = result.getString("QIDGPQR");
                if (result.getString("QIDGPQR") == null) {
                    gqr = "<b style=\"opacity: 0.0;\">1</b>";
                } else if (result.getString("QIDGPQR").trim().equals("")) {
                    gqr = "<b style=\"opacity: 0.0;\">1</b>";
                } else {
                    gqr = result.getString("QIDGPQR");
                }
                id = result.getString("QIDID");
                qty += Double.parseDouble(result.getString("QIDQTY"));
                um = result.getString("QIDBUN");
                qp += 1;
                pack = result.getString("QIDPACKTYPE");
                mvt = result.getString("MVT");

                QIDETAIL p = new QIDETAIL();
                p.setNo(Integer.toString(qp)); // Q'pack
                p.setMatc(mc);
                p.setDesc(desc);
                p.setGID(gqr);
                p.setId(id);
                p.setUm(um);
                p.setPack(pack);

                String shd = "";
                if (result.getString("QIDSHDT") != null) {
                    shd = result.getString("QIDSHDT");
                }
                String[] shdat = shd.split(" ");
                p.setShipmentDate(shdat[0]);

                p.setSts(result.getString("QIDSTS"));
                p.setMvt(mvt);
                p.setQno(result.getString("QIDQNO"));
                p.setMvtn(result.getString("MVTN"));
                p.setM3(formatDou.format(0));
                String tqt = formatDou.format(qty);
                p.setQty(tqt);
                sumqty += qty;
                summ3 += 0;

                String ck = "";
                String run = "";
                String runno = "";
                if (result.getString("QIDRUNNO") != null && !result.getString("QIDRUNNO").equals(" ")) {
                    runno = result.getString("QIDRUNNO");
                }
                if (result.getString("QIDGPQR") == null) {
                    ck = "<input style=\"width: 30px; height: 30px;\" style=\"width: 30px; height: 30px;\" type=\"checkbox\" id=\"selectCk-" + result.getString("QIDID") + "\" name=\"selectCk-" + dest + "\" value=\"" + result.getString("QIDID") + "-" + result.getString("QIDQNO") + "-" + result.getString("SHIPDATE") + "-" + result.getString("QIDRUNNO") + "-" + result.getString("QIDLINE") + "-" + result.getString("STS") + "\">";
                    run = "<input type=\"text\" name=\"running-" + dest + "\" value=\"" + runno + "\" style=\"width: 100px; text-align: center;\" disabled>";

                    if (pack.equals("BOX")) {
                        p.setBox(Integer.toString(qp));
                        sumbox += qp;
                    } else if (pack.equals("BAG") || pack.equals("PACK")) {
                        p.setBag(Integer.toString(qp));
                        sumbag += qp;
                    } else if (pack.equals("ROLL")) {
                        p.setRoll(Integer.toString(qp));
                        sumroll += qp;
                    }
                } else if (result.getString("QIDGPQR").trim().equals("")) {
                    ck = "<input style=\"width: 30px; height: 30px;\" style=\"width: 30px; height: 30px;\" type=\"checkbox\" id=\"selectCk-" + result.getString("QIDID") + "\" name=\"selectCk-" + dest + "\" value=\"" + result.getString("QIDID") + "-" + result.getString("QIDQNO") + "-" + result.getString("SHIPDATE") + "-" + result.getString("QIDRUNNO") + "-" + result.getString("QIDLINE") + "-" + result.getString("STS") + "\">";
                    run = "<input type=\"text\" name=\"running-" + dest + "\" value=\"" + runno + "\" style=\"width: 100px; text-align: center;\" disabled>";

                    if (pack.equals("BOX")) {
                        p.setBox(Integer.toString(qp));
                        sumbox += qp;
                    } else if (pack.equals("BAG") || pack.equals("PACK")) {
                        p.setBag(Integer.toString(qp));
                        sumbag += qp;
                    } else if (pack.equals("ROLL")) {
                        p.setRoll(Integer.toString(qp));
                        sumroll += qp;
                    }
                } else {
                    if (oldGID.equals("")) {
                        oldGID = result.getString("QIDGPQR");
                        ck = "<input style=\"width: 30px; height: 30px;\" style=\"width: 30px; height: 30px;\" type=\"checkbox\" id=\"selectCk-" + result.getString("QIDGPQR") + "\" name=\"selectCk-" + dest + "\" value=\"" + result.getString("QIDGPQR") + "-" + result.getString("QIDQNO") + "-" + result.getString("SHIPDATE") + "-" + result.getString("QIDRUNNO") + "-" + result.getString("QIDLINE") + "-" + result.getString("STS") + "\">";
                        run = "<input type=\"text\" name=\"running-" + dest + "\" value=\"" + runno + "\" style=\"width: 100px; text-align: center;\" disabled>";

                        if (pack.equals("BOX")) {
                            p.setBox(Integer.toString(qp));
                            sumbox += qp;
                        } else if (pack.equals("BAG") || pack.equals("PACK")) {
                            p.setBag(Integer.toString(qp));
                            sumbag += qp;
                        } else if (pack.equals("ROLL")) {
                            p.setRoll(Integer.toString(qp));
                            sumroll += qp;
                        }
                    } else {
                        if (!result.getString("QIDGPQR").equals(oldGID)) {
                            oldGID = result.getString("QIDGPQR");
                            ck = "<input style=\"width: 30px; height: 30px;\" style=\"width: 30px; height: 30px;\" type=\"checkbox\" id=\"selectCk-" + result.getString("QIDGPQR") + "\" name=\"selectCk-" + dest + "\" value=\"" + result.getString("QIDGPQR") + "-" + result.getString("QIDQNO") + "-" + result.getString("SHIPDATE") + "-" + result.getString("QIDRUNNO") + "-" + result.getString("QIDLINE") + "-" + result.getString("STS") + "\">";
                            run = "<input type=\"text\" name=\"running-" + dest + "\" value=\"" + runno + "\" style=\"width: 100px; text-align: center;\" disabled>";

                            if (pack.equals("BOX")) {
                                p.setBox(Integer.toString(qp));
                                sumbox += qp;
                            } else if (pack.equals("BAG") || pack.equals("PACK")) {
                                p.setBag(Integer.toString(qp));
                                sumbag += qp;
                            } else if (pack.equals("ROLL")) {
                                p.setRoll(Integer.toString(qp));
                                sumroll += qp;
                            }
                        }
                    }
                }

                p.setCkBox(ck);
                p.setRunno(run);

                UAList.add(p);

                qty = 0;
                qp = 0;

            }

            QIDETAIL p = new QIDETAIL();
            p.setGID("<b style=\"opacity: 0.0;\">0</b>");
            p.setMatc("<b style=\"opacity: 0.0;\">0</b>");
            p.setDesc("<b style=\"opacity: 0.0;\">0</b>");
            p.setId("<b>TOTAL</b>");
            p.setBox("<b>" + Integer.toString(sumbox) + "</b>");
            p.setBag("<b>" + Integer.toString(sumbag) + "</b>");
            p.setRoll("<b>" + Integer.toString(sumroll) + "</b>");
            p.setM3("<b>" + formatDou.format(summ3) + "</b>");
            p.setQty("<b>" + formatDou.format(sumqty) + "</b>");
            UAList.add(p);

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public boolean reject(String id, String qno, String user) {

        boolean result = false;

        String where = "";
        String LINE = "";

        String[] parts = id.split("-");
        id = parts[0];
        String line = parts[2];

        if (id.length() < 17) {
            where = "QIDGPQR";
        } else if (id.length() == 17) {
            where = "QIDID";
            LINE = "AND QIDLINE = '" + line + "' ";
        }

        String sql = "UPDATE QIDETAIL "
                + "SET QIDSTS = '8', QIDAPUSR = '" + user + "', QIDAPDT = CURRENT_TIMESTAMP, QIDEDT = CURRENT_TIMESTAMP "
                + "WHERE " + where + " = '" + id + "' "
                + "AND QIDQNO = '" + qno + "' "
                + LINE;

        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean receive(String id, String user) {

        boolean result = false;

        String where = "";

        String[] parts = id.split("-");
        id = parts[0];
        String qno = parts[1];

        if (id.length() < 17) {
            where = "QIDGPQR";
        } else if (id.length() == 17) {
            where = "QIDID";
        }

        String sql = "UPDATE QIDETAIL "
                + "SET QIDSTS = '5', QIDRCUSR = '" + user + "', QIDRCDT = CURRENT_TIMESTAMP, QIDEDT = CURRENT_TIMESTAMP "
                + "WHERE " + where + " = '" + id + "' "
                + "AND QIDQNO = '" + qno + "' ";

        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean transport(String id, String user) {

        boolean result = false;

        String where = "";

        String[] parts = id.split("-");
        id = parts[0];
        String qno = parts[1];

        if (id.length() < 17) {
            where = "QIDGPQR";
        } else if (id.length() == 17) {
            where = "QIDID";
        }

        String sql = "UPDATE QIDETAIL "
                + "SET QIDSTS = '6', QIDTPUSR = '" + user + "', QIDTPDT = CURRENT_TIMESTAMP, QIDEDT = CURRENT_TIMESTAMP "
                + "WHERE " + where + " = '" + id + "' "
                + "AND QIDQNO = '" + qno + "' ";

        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean receiveWMS360(String id, String user) {

        boolean result = false;

        String where = "";

        String[] parts = id.split("-");
        id = parts[0];
        String qno = parts[1];

        if (id.length() < 17) {
            where = "QIDGPQR";
        } else if (id.length() == 17) {
            where = "QIDID";
        }

        String sql = "UPDATE QIDETAIL "
                + "SET QIDSTS = '7', QIDDTUSR = '" + user + "', QIDDTDT = CURRENT_TIMESTAMP, QIDEDT = CURRENT_TIMESTAMP "
                + "WHERE " + where + " = '" + id + "' "
                + "AND QIDQNO = '" + qno + "' ";

        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean round(String id, String user, String round) {

        boolean result = false;

        String where = "";

        String[] parts = id.split("-");
        id = parts[0];
        String qno = parts[1];

        if (id.length() < 17) {
            where = "QIDGPQR";
        } else if (id.length() == 17) {
            where = "QIDID";
        }

        String sql = "UPDATE QIDETAIL "
                + "SET QIDROUND = '" + round + "', QIDEDT = CURRENT_TIMESTAMP "
                + "WHERE " + where + " = '" + id + "' "
                + "AND QIDQNO = '" + qno + "' ";

        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean roundX(String id, String user, String round, String sd) {

        boolean result = false;

        String where = "";

        String[] parts = id.split("-");
        id = parts[0];
        String qno = parts[1];

        if (id.length() < 17) {
            where = "QIDGPQR";
        } else if (id.length() == 17) {
            where = "QIDID";
        }

        String sql = "UPDATE QIDETAIL "
                + "SET QIDROUND = '" + round + "', QIDSHDT = '" + sd + "', QIDEDT = CURRENT_TIMESTAMP "
                + "WHERE " + where + " = '" + id + "' "
                + "AND QIDQNO = '" + qno + "' "
                + "AND QIDSTS = '6'";
//        System.out.println(sql);

        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean cancelWMS350(String id, String user) {

        boolean result = false;

        String where = "";

        String[] parts = id.split("-");
        id = parts[0];
        String qno = parts[1];

        if (id.length() < 17) {
            where = "QIDGPQR";
        } else if (id.length() == 17) {
            where = "QIDID";
        }

        String sql = "UPDATE QIDETAIL "
                + "SET QIDSTS = 'R', QIDTPUSR = '" + user + "', QIDTPDT = CURRENT_TIMESTAMP, QIDEDT = CURRENT_TIMESTAMP "
                + "WHERE " + where + " = '" + id + "' "
                + "AND QIDQNO = '" + qno + "' ";

        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean HoldWMS350(String id, String user) {

        boolean result = false;

        String where = "";

        String[] parts = id.split("-");
        id = parts[0];
        String qno = parts[1];

        if (id.length() < 17) {
            where = "QIDGPQR";
        } else if (id.length() == 17) {
            where = "QIDID";
        }

        String sql = "UPDATE QIDETAIL "
                + "SET QIDSTS = 'H', QIDTPUSR = '" + user + "', QIDTPDT = CURRENT_TIMESTAMP, QIDEDT = CURRENT_TIMESTAMP "
                + "WHERE " + where + " = '" + id + "' "
                + "AND QIDQNO = '" + qno + "' ";

        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean returnWMS360(String id, String user) {

        boolean result = false;

        String where = "";

        String[] parts = id.split("-");
        id = parts[0];
        String qno = parts[1];

        if (id.length() < 17) {
            where = "QIDGPQR";
        } else if (id.length() == 17) {
            where = "QIDID";
        }

        String sql = "UPDATE QIDETAIL "
                + "SET QIDSTS = 'D', QIDDTUSR = '" + user + "', QIDDTDT = CURRENT_TIMESTAMP, QIDEDT = CURRENT_TIMESTAMP "
                + "WHERE " + where + " = '" + id + "' "
                + "AND QIDQNO = '" + qno + "' ";

        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean cancel(String id, String user, String remark) {

        boolean result = false;

        String where = "";

        String[] parts = id.split("-");
        id = parts[0];
        String qno = parts[1];

        if (id.length() < 17) {
            where = "QIDGPQR";
        } else if (id.length() == 17) {
            where = "QIDID";
        }

        String sql = "UPDATE QIDETAIL "
                + "SET QIDSTS = '9', QIDRCUSR = '" + user + "', QIDRCDT = CURRENT_TIMESTAMP, QIDEDT = CURRENT_TIMESTAMP,"
                + "QIDREMARK = '" + remark + "' "
                + "WHERE " + where + " = '" + id + "' "
                + "AND QIDQNO = '" + qno + "' ";

        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public String check(String id) {
        String p = "";
        String where = "";
        String LINE = "";

        String[] parts = id.split("-");
        id = parts[0];
        String qno = parts[1];
        String line = parts[4];

        if (id.length() < 17) {
            where = "QIDGPQR";
        } else if (id.length() == 17) {
            where = "QIDID";
            LINE = "AND QIDLINE = '" + line + "' ";
        }

        String sql = "SELECT QIDRUNNO "
                + "FROM QIDETAIL "
                + "WHERE " + where + " = '" + id + "' "
                + "AND QIDQNO = '" + qno + "' "
                + LINE;

        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                p = result.getString("QIDRUNNO");

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return p;
    }

    public String[] getSTS(String wh, String qno) {
        String[] p = new String[2];

        String sql = "SELECT MAX(QIDSTS) as MAXS, MIN(QIDSTS) as MINS\n"
                + "from(\n"
                + "SELECT QIDSTS FROM QIDETAIL\n"
                + "  where QIDWHS = '" + wh + "'\n"
                + "  and QIDQNO in ('" + qno + "')\n"
                + ") TMP";
//        System.out.println(sql);

        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                p[0] = result.getString("MAXS");
                p[1] = result.getString("MINS");

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return p;
    }

    public String getTQY(String wh, String qno) {
        String p = "";

        String sql = "SELECT SUM(QIDQTY) AS TOTAL FROM QIDETAIL\n"
                + "  where QIDWHS = '" + wh + "'\n"
                + "  and QIDQNO in ('" + qno + "')\n"
                + "  and (QIDSTS = '2' OR QIDSTS = '5' OR QIDSTS = '6' OR QIDSTS = 'C' OR QIDSTS = '7') ";
//        System.out.println(sql);

        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                p = result.getString("TOTAL");

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return p;
    }

    public boolean runno(String id, String user, String runno, String shdt) {

        boolean result = false;

        String where = "";
        String LINE = "";

        String[] parts = id.split("-");
        id = parts[0];
        String qno = parts[1];
        String line = parts[4];

        if (id.length() < 17) {
            where = "QIDGPQR";
        } else if (id.length() == 17) {
            where = "QIDID";
            LINE = "AND QIDLINE = '" + line + "' ";
        }

        String sql = "UPDATE QIDETAIL "
                + "SET QIDSHUSR = '" + user + "', QIDSHDT = '" + shdt + "', QIDEDT = CURRENT_TIMESTAMP,"
                + "QIDRUNNO = '" + runno + "' "
                + "WHERE " + where + " = '" + id + "' "
                + "AND QIDQNO = '" + qno + "' "
                + LINE;

        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean runnoX(String id, String user, String runno) {

        boolean result = false;

        String where = "";
        String LINE = "";

        String[] parts = id.split("-");
        id = parts[0];
        String qno = parts[1];
        String line = parts[4];

        if (id.length() < 17) {
            where = "QIDGPQR";
        } else if (id.length() == 17) {
            where = "QIDID";
            LINE = "AND QIDLINE = '" + line + "' ";
        }

        String sql = "UPDATE QIDETAIL "
                + "SET QIDSHUSR = '" + user + "', QIDEDT = CURRENT_TIMESTAMP,"
                + "QIDRUNNO = '" + runno + "' "
                + "WHERE " + where + " = '" + id + "' "
                + "AND QIDQNO = '" + qno + "' "
                + LINE;

        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public String[] findNumberSerie(String id) {

        String[] p = new String[3];
        String where = "";
        String LINE = "";

        String[] parts = id.split("-");
        id = parts[0];
        String qno = parts[1];
        String line = parts[4];

        if (id.length() < 17) {
            where = "QIDGPQR";
        } else if (id.length() == 17) {
            where = "QIDID";
            LINE = "AND QIDLINE = '" + line + "' ";
        }

        String sql = "SELECT TOP 1 QIDQNO, SUBSTRING(CONVERT(VARCHAR(25), QIDTRDT, 126), 1, 4) as YEARS "
                + "      ,SUBSTRING(QIDQNO, 1, 2) as GROUPS "
                + "      ,QIDID "
                + "      ,QIDGPQR "
                + "	  ,(SELECT QNBLAST FROM QNBSER WHERE  QNBTYPE = 'LG' AND QNBGROUP = SUBSTRING(QIDQNO, 1, 2)  "
                + "	  AND QNBYEAR = SUBSTRING(CONVERT(VARCHAR(25), QIDTRDT, 126), 1, 4)) as CURRENTS "
                + "FROM QIDETAIL "
                + "WHERE " + where + " = '" + id + "' "
                + "AND QIDQNO = '" + qno + "' "
                + LINE;

        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                p[0] = result.getString("YEARS");
                p[1] = result.getString("GROUPS");
                p[2] = result.getString("CURRENTS");

            }

        } catch (Exception e) {

            e.printStackTrace();

        }

        return p;

    }

    public List<String> UngroupQRID(String id) {

        List<String> UAList = new ArrayList<String>();

        String[] parts = id.split("-");
        id = parts[0];

        String sql = "SELECT QIDID, QIDQNO, QIDLINE\n"
                + "FROM QIDETAIL\n"
                + "WHERE QIDGPQR = '" + id + "' ";

        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                String p = result.getString("QIDID").trim() + "-"
                        + result.getString("QIDQNO").trim() + "-"
                        + result.getString("QIDLINE").trim();
                UAList.add(p);

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public boolean approve(String id, String user, String qno) {

        boolean result = false;

        String where = "";
        String LINE = "";

        String[] parts = id.split("-");
        id = parts[0];
        String line = parts[2];

        if (id.length() < 17) {
            where = "QIDGPQR";
        } else if (id.length() == 17) {
            where = "QIDID";
            LINE = "AND QIDLINE = '" + line + "' ";
        }

        String sql = "UPDATE QIDETAIL "
                + "SET QIDSTS = '2', QIDAPUSR = '" + user + "', QIDAPDT = CURRENT_TIMESTAMP, QIDEDT = CURRENT_TIMESTAMP "
                + "WHERE " + where + " = '" + id + "' "
                + "AND QIDQNO = '" + qno + "' "
                + LINE;

        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean approveMVT301and311(String id, String user, String qno) {

        boolean result = false;

        String where = "";
        String LINE = "";

        String[] parts = id.split("-");
        id = parts[0];
        String line = parts[2];

        if (id.length() < 17) {
            where = "QIDGPQR";
        } else if (id.length() == 17) {
            where = "QIDID";
            LINE = "AND QIDLINE = '" + line + "' ";
        }

        String sql = "UPDATE QIDETAIL "
                + "SET QIDSTS = 'C', QIDAPUSR = '" + user + "', QIDAPDT = CURRENT_TIMESTAMP, QIDEDT = CURRENT_TIMESTAMP "
                + "WHERE " + where + " = '" + id + "' "
                + "AND QIDQNO = '" + qno + "' "
                + LINE;

        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean release(String id, String qno, String user) {

        boolean result = false;

        String where = "";
        String LINE = "";

        String[] parts = id.split("-");
        id = parts[0];
        String line = parts[2];

        if (id.length() < 17) {
            where = "QIDGPQR";
        } else if (id.length() == 17) {
            where = "QIDID";
            LINE = "AND QIDLINE = '" + line + "' ";
        }

        String sql = "UPDATE QIDETAIL "
                + "SET QIDSTS = '0', QIDAPUSR = '" + user + "', QIDAPDT = CURRENT_TIMESTAMP, QIDEDT = CURRENT_TIMESTAMP "
                + "WHERE " + where + " = '" + id + "' "
                + "AND QIDSTS = '9' "
                + "AND QIDQNO = '" + qno + "' "
                + LINE;

        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean rcQI2To5(String sts, String qid, String qno, String line) {

        boolean result = false;

        String sql = "UPDATE QIDETAIL SET QIDSTS = '" + sts + "' WHERE QIDID = '" + qid + "' AND QIDQNO = '" + qno + "' AND QIDLINE = '" + line + "' ";

//        System.out.println("" + sql);
        try {

            PreparedStatement ps = connect.prepareStatement(sql);
//            PreparedStatement ps = null;

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public static String padLeftZeros(String str, int n) {
        return String.format("%1$" + n + "s", str).replace(' ', '0');
    }

}
