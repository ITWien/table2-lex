/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.dao;

import com.twc.wms.database.database;
import com.twc.wms.entity.QDESTYP;
import com.twc.wms.entity.Qdest;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 *
 * @author nutthawoot.noo
 */
public class QMVTDao extends database {

    public List<Qdest> findAll() {

        List<Qdest> QdestList = new ArrayList<Qdest>();

        String sql = "SELECT QMVMVT, QMVDESC "
                + "FROM QMVT "
                + "ORDER BY QMVMVT ";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                Qdest p = new Qdest();

                p.setCode(result.getString("QMVMVT"));

                p.setDesc(result.getString("QMVDESC"));

                QdestList.add(p);

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return QdestList;

    }
}
