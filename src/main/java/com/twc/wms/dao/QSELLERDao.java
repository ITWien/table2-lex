/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.dao;

import com.twc.wms.database.database;
import com.twc.wms.entity.QSELLER;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author nutthawoot.noo
 */
public class QSELLERDao extends database {

    public List<QSELLER> findByWHS(String wh) {

        List<QSELLER> QSELLERList = new ArrayList<QSELLER>();

        String sql = "SELECT [QSCODE]\n"
                + "      ,[QSNAME]\n"
                + "      ,isnull([QSMTYP1],'') as [QSMTYP1]\n"
                + "      ,isnull([QSMTYP2],'') as [QSMTYP2]\n"
                + "      ,isnull([QSMTYP3],'') as [QSMTYP3]\n"
                + "      ,isnull([QSMTYP4],'') as [QSMTYP4]\n"
                + "      ,isnull([QSMTYP5],'') as [QSMTYP5]\n"
                + "      ,isnull([QSMTYP6],'') as [QSMTYP6]\n"
                + "      ,isnull([QSMTYP7],'') as [QSMTYP7]\n"
                + "      ,isnull([QSMTYP8],'') as [QSMTYP8]\n"
                + "      ,isnull([QSMTYP9],'') as [QSMTYP9]\n"
                + "      ,isnull([QSMTYPA],'') as [QSMTYPA]\n"
                + "  FROM [RMShipment].[dbo].[QSELLER]\n"
                + "  WHERE [QSWHSE] = '" + wh + "'";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                QSELLER p = new QSELLER();

                p.setCode(result.getString("QSCODE"));

                p.setName(result.getString("QSNAME"));

                String t = "";

                if (!result.getString("QSMTYP1").trim().equals("")) {
                    t += (result.getString("QSMTYP1").trim());
                }
                if (!result.getString("QSMTYP2").trim().equals("")) {
                    t += (", " + result.getString("QSMTYP2").trim());
                }
                if (!result.getString("QSMTYP3").trim().equals("")) {
                    t += (", " + result.getString("QSMTYP3").trim());
                }
                if (!result.getString("QSMTYP4").trim().equals("")) {
                    t += (", " + result.getString("QSMTYP4").trim());
                }
                if (!result.getString("QSMTYP5").trim().equals("")) {
                    t += (", " + result.getString("QSMTYP5").trim());
                }
                if (!result.getString("QSMTYP6").trim().equals("")) {
                    t += (", " + result.getString("QSMTYP6").trim());
                }
                if (!result.getString("QSMTYP7").trim().equals("")) {
                    t += (", " + result.getString("QSMTYP7").trim());
                }
                if (!result.getString("QSMTYP8").trim().equals("")) {
                    t += (", " + result.getString("QSMTYP8").trim());
                }
                if (!result.getString("QSMTYP9").trim().equals("")) {
                    t += (", " + result.getString("QSMTYP9").trim());
                }
                if (!result.getString("QSMTYPA").trim().equals("")) {
                    t += (", " + result.getString("QSMTYPA").trim());
                }

                p.setType(t);

                QSELLERList.add(p);

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return QSELLERList;

    }
}
