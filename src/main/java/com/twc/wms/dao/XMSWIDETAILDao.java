/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.dao;

import com.twc.wms.database.database;

import java.sql.*;

import java.util.ArrayList;

import java.util.List;

import com.twc.wms.entity.XMSWIDETAIL;
import com.twc.wms.entity.SAPMAS;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import com.twc.wms.database.MSSQLDbConnectionPool;
import com.twc.wms.entity.XMSWIDETAIL;
import java.text.DecimalFormat;

/**
 *
 * @author nutthawoot.noo
 */
public class XMSWIDETAILDao extends database {

    DecimalFormat formatDou = new DecimalFormat("#,###,###,##0");

    public ResultSet findForPrintXMS800QNO(String qno) throws SQLException {

        String sql = "DECLARE @QNO DECIMAL(18, 0) = '" + qno + "'\n"
                + "SELECT [XMSWIHQNO]\n"
                + "      ,[XMSWIHLICENNO]\n"
                + "	  ,FORMAT([XMSWIHSPDT],'d/M/yyyy') AS [XMSWIHSPDT]\n"
                + "	  ,[XMSWIHWHS] AS WHS\n"
                + "	  ,[QWHNAME] AS WHSN\n"
                + "	  ,[XMSWIHROUND]\n"
                + "	  ,[XMSWIHDRIVER]\n"
                + "	  ,[XMSWIHFOLLOWER]\n"
                + "	  ,[XMSWIHDOCNO]\n"
                + "      ,[XMSWIDLINE]\n"
                + "      ,[XMSWIDDEST] AS DEST\n"
                + "      ,[XMSWIDDESTN] AS DESTN\n"
                + "      ,[XMSWIDDRIVER] AS DRIVER\n"
                + "      ,[XMSWIDFOLLOWER] AS FOLLOWER\n"
                + "      ,[XMSWIDBAG] AS BAG_TOTWH\n"
                + "      ,[XMSWIDROLL] AS ROLL_TOTWH\n"
                + "      ,[XMSWIDBOX] AS BOX_TOTWH\n"
                + "      ,[XMSWIDPCS] AS PCS_TOTWH\n"
                + "      ,[XMSWIDTOT] AS TOT_TOTWH\n"
                + "  FROM [RMShipment].[dbo].[XMSWIDETAIL]\n"
                + "  LEFT JOIN [XMSWIHEAD] ON [XMSWIHEAD].[XMSWIHQNO] = [XMSWIDETAIL].[XMSWIDQNO]\n"
                + "  LEFT JOIN [QWHMAS] ON [QWHMAS].[QWHCOD] = [XMSWIHEAD].[XMSWIHWHS]\n"
                + "  WHERE [XMSWIDQNO] = @QNO\n"
                + "  ORDER BY XMSWIDLINE";
//        System.out.println(sql);

        ResultSet result = null;

        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            result = ps.executeQuery();
        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public List<XMSWIDETAIL> findByQno(String qno) {

        List<XMSWIDETAIL> UAList = new ArrayList<XMSWIDETAIL>();

        String sql = "SELECT [XMSWIDQNO]\n"
                + "      ,[XMSWIDLINE]\n"
                + "      ,[XMSWIDDEST] AS DEST\n"
                + "      ,[XMSWIDDESTN] AS DESTN\n"
                + "      ,[XMSWIDDRIVER] AS DRIVER\n"
                + "      ,[XMSWIDFOLLOWER] AS FOLLOWER\n"
                + "      ,[XMSWIDBAG] AS BAG_TOTWH\n"
                + "      ,[XMSWIDROLL] AS ROLL_TOTWH\n"
                + "      ,[XMSWIDBOX] AS BOX_TOTWH\n"
                + "      ,[XMSWIDPCS] AS PCS_TOTWH\n"
                + "      ,[XMSWIDTOT] AS TOT_TOTWH\n"
                + "  FROM [RMShipment].[dbo].[XMSWIDETAIL]\n"
                + "  WHERE [XMSWIDQNO] = '" + qno + "'\n"
                + "  ORDER BY XMSWIDLINE";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                XMSWIDETAIL p = new XMSWIDETAIL();

                p.setXMSWIDDEST(result.getString("DEST"));
                p.setXMSWIDDESTN(result.getString("DESTN"));
                p.setXMSWIDDRIVER(result.getString("DRIVER"));
                p.setXMSWIDFOLLOWER(result.getString("FOLLOWER"));
                p.setXMSWIDBAG(((result.getString("BAG_TOTWH") == null) || result.getString("BAG_TOTWH").trim().equals("0")) ? "-" : formatDou.format(Double.parseDouble(result.getString("BAG_TOTWH"))));
                p.setXMSWIDROLL(((result.getString("ROLL_TOTWH") == null) || result.getString("ROLL_TOTWH").trim().equals("0")) ? "-" : formatDou.format(Double.parseDouble(result.getString("ROLL_TOTWH"))));
                p.setXMSWIDBOX(((result.getString("BOX_TOTWH") == null) || result.getString("BOX_TOTWH").trim().equals("0")) ? "-" : formatDou.format(Double.parseDouble(result.getString("BOX_TOTWH"))));
                p.setXMSWIDPCS(((result.getString("PCS_TOTWH") == null) || result.getString("PCS_TOTWH").trim().equals("0")) ? "-" : formatDou.format(Double.parseDouble(result.getString("PCS_TOTWH"))));
                p.setXMSWIDTOT(((result.getString("TOT_TOTWH") == null) || result.getString("TOT_TOTWH").trim().equals("0")) ? "0" : formatDou.format(Double.parseDouble(result.getString("TOT_TOTWH"))));

                UAList.add(p);

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public List<XMSWIDETAIL> findFromOT(String lino, String shipDate, String wh, String round) {

        List<XMSWIDETAIL> UAList = new ArrayList<XMSWIDETAIL>();

        String sql = "DECLARE @LINO NVARCHAR(50) = '" + lino + "'\n"
                + "DECLARE @WHS NVARCHAR(50) = '" + wh + "'\n"
                + "DECLARE @SPDT NVARCHAR(50) = '" + shipDate + "'\n"
                + "DECLARE @ROUND NVARCHAR(50) = '" + round + "'\n"
                + "SELECT DEST, [QDETYPE]\n"
                + ", (SELECT TOP 1 [QDEDESC] FROM [QDEST] WHERE [QDECOD] = DEST) AS DESTN\n"
                + ",DRIVER, FOLLOWER, BAG_TOTWH, ROLL_TOTWH, BOX_TOTWH, PCS_TOTWH\n"
                + ",(BAG_TOTWH+ROLL_TOTWH+BOX_TOTWH+PCS_TOTWH) AS TOT_TOTWH\n"
                + "FROM(\n"
                + "SELECT [XMSOTHDEST] AS DEST\n"
                + "      ,[XMSOTHDRIVER] AS DRIVER\n"
                + "      ,[XMSOTHFOLLOWER] AS FOLLOWER\n"
                + "      ,SUM([XMSOTHBAG]) AS BAG_TOTWH\n"
                + "      ,SUM([XMSOTHROLL]) AS ROLL_TOTWH\n"
                + "      ,SUM([XMSOTHBOX]) AS BOX_TOTWH\n"
                + "      ,SUM([XMSOTHPCS]) AS PCS_TOTWH\n"
                + "  FROM [RMShipment].[dbo].[XMSOTHEAD]\n"
                + "  WHERE [XMSOTHWHS] = @WHS\n"
                + "  AND [XMSOTHSPDT] = @SPDT\n"
                + "  AND [XMSOTHROUND] = @ROUND\n"
                + "  AND [XMSOTHLICENNO] = @LINO\n"
                + "  GROUP BY [XMSOTHDEST]\n"
                + "      ,[XMSOTHLICENNO]\n"
                + "      ,[XMSOTHDRIVER]\n"
                + "      ,[XMSOTHFOLLOWER]\n"
                + ")TMP \n"
                + "LEFT JOIN [QDEST] ON [QDEST].[QDECOD] = TMP.DEST\n"
                + "WHERE [QDETYPE] = '1'\n"
                + "ORDER BY DEST";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                XMSWIDETAIL p = new XMSWIDETAIL();

                p.setXMSWIDDEST(result.getString("DEST"));
                p.setXMSWIDDESTN(result.getString("DESTN"));
                p.setXMSWIDDRIVER(result.getString("DRIVER"));
                p.setXMSWIDFOLLOWER(result.getString("FOLLOWER"));
                p.setXMSWIDBAG(((result.getString("BAG_TOTWH") == null) || result.getString("BAG_TOTWH").trim().equals("0")) ? "-" : formatDou.format(Double.parseDouble(result.getString("BAG_TOTWH"))));
                p.setXMSWIDROLL(((result.getString("ROLL_TOTWH") == null) || result.getString("ROLL_TOTWH").trim().equals("0")) ? "-" : formatDou.format(Double.parseDouble(result.getString("ROLL_TOTWH"))));
                p.setXMSWIDBOX(((result.getString("BOX_TOTWH") == null) || result.getString("BOX_TOTWH").trim().equals("0")) ? "-" : formatDou.format(Double.parseDouble(result.getString("BOX_TOTWH"))));
                p.setXMSWIDPCS(((result.getString("PCS_TOTWH") == null) || result.getString("PCS_TOTWH").trim().equals("0")) ? "-" : formatDou.format(Double.parseDouble(result.getString("PCS_TOTWH"))));
                p.setXMSWIDTOT(((result.getString("TOT_TOTWH") == null) || result.getString("TOT_TOTWH").trim().equals("0")) ? "0" : formatDou.format(Double.parseDouble(result.getString("TOT_TOTWH"))));

                UAList.add(p);

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public boolean add(XMSWIDETAIL detail) {

        boolean result = false;

        String sql = "INSERT INTO [XMSWIDETAIL] "
                + "([XMSWIDCOM]\n"
                + "      ,[XMSWIDQNO]\n"
                + "      ,[XMSWIDLINE]\n"
                + "      ,[XMSWIDDEST]\n"
                + "      ,[XMSWIDDESTN]\n"
                + "      ,[XMSWIDDRIVER]\n"
                + "      ,[XMSWIDFOLLOWER]\n"
                + "      ,[XMSWIDBAG]\n"
                + "      ,[XMSWIDROLL]\n"
                + "      ,[XMSWIDBOX]\n"
                + "      ,[XMSWIDPCS]\n"
                + "      ,[XMSWIDTOT]\n"
                + "      ,[XMSWIDEDT]\n"
                + "      ,[XMSWIDCDT]\n"
                + "      ,[XMSWIDUSER]) "
                + "VALUES('TWC'"
                + ",'" + detail.getXMSWIDQNO() + "'"
                + ",'" + detail.getXMSWIDLINE() + "'"
                + ",'" + detail.getXMSWIDDEST() + "'"
                + ",'" + detail.getXMSWIDDESTN() + "'"
                + ",'" + detail.getXMSWIDDRIVER() + "'"
                + ",'" + detail.getXMSWIDFOLLOWER() + "'"
                + ",'" + detail.getXMSWIDBAG() + "'"
                + ",'" + detail.getXMSWIDROLL() + "'"
                + ",'" + detail.getXMSWIDBOX() + "'"
                + ",'" + detail.getXMSWIDPCS() + "'"
                + ",'" + detail.getXMSWIDTOT() + "'"
                + ",CURRENT_TIMESTAMP"
                + ",CURRENT_TIMESTAMP"
                + ",'" + detail.getXMSWIDUSER() + "') ";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public void delete(String qno) {

        String sql = "DELETE FROM XMSWIDETAIL WHERE XMSWIDQNO = '" + qno + "'";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ps.executeUpdate();

        } catch (Exception e) {

            e.printStackTrace();

        }

    }
}
