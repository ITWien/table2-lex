/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.dao;

import com.twc.wms.database.database;
import com.twc.wms.entity.QDESTYP;
import com.twc.wms.entity.QDMBAG;
import com.twc.wms.entity.QDMROLLDET;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 *
 * @author nutthawoot.noo
 */
public class QDMROLLDETDao extends database {

    public String checkDet(String id) {
        PreparedStatement ps = null;
        ResultSet rs = null;
        String STRRETURN = null;
        try {
            String check = "SELECT [QDMRCOM]\n"
                    + "      ,[QDMRSTYLE]\n"
                    + "      ,[QDMRLINO]\n"
                    + "      ,[QDMRLENGTH1]\n"
                    + "      ,[QDMRLENGTH2]\n"
                    + "      ,[QDMRHEIGHT]\n"
                    + "      ,[QDMREDT]\n"
                    + "      ,[QDMRCDT]\n"
                    + "      ,[QDMRUSER]\n"
                    + "      ,[QDMRID]\n"
                    + "  FROM [RMShipment].[dbo].[QDMROLDET]\n"
                    + "  WHERE [QDMRID] = '" + id + "'";
//            System.out.println(check);
            ps = connect.prepareStatement(check);
            rs = ps.executeQuery();
            if (rs.next()) {
                STRRETURN = "f";
            } else {
                STRRETURN = "t";
            }
            connect.close();

        } catch (Exception exp) {
            exp.printStackTrace();
        }
        return STRRETURN;
    }

    public boolean updateRollDet(String code, String line, String length, String height, String uid) {

        boolean result = false;

        if (uid.equals("null")) {
            uid = "";
        }

        String length2 = length.replace("NaN", "0") + "*100";

        String sql = "UPDATE [RMShipment].[dbo].[QDMROLDET]\n"
                + "  SET [QDMRLENGTH1] = '" + length.replace("NaN", "0") + "'\n"
                + "      ,[QDMRLENGTH2] = " + length2 + "\n"
                + "      ,[QDMRHEIGHT] = '" + height.replace("NaN", "0") + "'\n"
                + "      ,[QDMREDT] = CURRENT_TIMESTAMP\n"
                + "      ,[QDMRUSER] = '" + uid + "'\n"
                + "  WHERE [QDMRSTYLE] = '" + code + "'\n"
                + "  AND [QDMRLINO] = '" + line + "'";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public List<QDMROLLDET> findByStyle(String style) {

        List<QDMROLLDET> QDMROLLDETList = new ArrayList<QDMROLLDET>();

        String sql = "SELECT [QDMRSTYLE]\n"
                + "      ,[QDMRLINO]\n"
                + "      ,[QDMRLENGTH1]\n"
                + "      ,[QDMRLENGTH2]\n"
                + "      ,[QDMRHEIGHT]\n"
                + "  FROM [RMShipment].[dbo].[QDMROLDET]\n"
                + "  WHERE [QDMRSTYLE] = '" + style + "'";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                QDMROLLDET p = new QDMROLLDET();

                p.setQDMRSTYLE(result.getString("QDMRSTYLE"));
                p.setQDMRLINO(result.getString("QDMRLINO"));
                p.setQDMRLENGTH1("<input type=\"text\" id=\"" + result.getString("QDMRLINO") + "-detLength1\" name=\"detLength1\" onkeyup=\"$('#" + result.getString("QDMRLINO") + "-detLength2').val(parseFloat($(this).val())*100);\" style=\"width:100px; text-align:right;\" value=\"" + result.getString("QDMRLENGTH1") + "\">");
                p.setQDMRLENGTH2("<input type=\"text\" id=\"" + result.getString("QDMRLINO") + "-detLength2\" name=\"detLength2\" onkeyup=\"$('#" + result.getString("QDMRLINO") + "-detLength1').val(parseFloat($(this).val())/100);\" style=\"width:100px; text-align:right;\" value=\"" + result.getString("QDMRLENGTH2") + "\">");
                p.setQDMRHEIGHT("<input type=\"text\" id=\"" + result.getString("QDMRLINO") + "-detHeightRoll\" name=\"detHeightRoll\" style=\"width:100px; text-align:right;\" value=\"" + (result.getString("QDMRHEIGHT").equals("0.00") ? "" : result.getString("QDMRHEIGHT")) + "\">");

                p.setOption("<a title=\"Delete\" style=\" cursor: pointer;\" onclick=\"DeleteDetRoll('" + result.getString("QDMRSTYLE") + "','" + result.getString("QDMRLINO") + "');\"><i class=\"fa fa-times-circle-o\" aria-hidden=\"true\" style=\"font-size:25px; color: #e51e00;\"></i></a>");

                QDMROLLDETList.add(p);

            }
//
//            if (QDMROLLDETList.isEmpty()) {
//                QDMROLLDET p = new QDMROLLDET();
//
//                p.setQDMRSTYLE("1");
//                p.setQDMRLINO("");
//                p.setQDMRQTY("");
//                p.setQDMRHEIGHT("");
//
//                QDMROLLDETList.add(p);
//            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return QDMROLLDETList;

    }

    public boolean addDet(String code, String length, String uid, String qrmid) {

        boolean result = false;

        if (uid.equals("null")) {
            uid = "";
        }

        String sql = "INSERT INTO [QDMROLDET]"
                + " ([QDMRCOM]\n"
                + "      ,[QDMRSTYLE]\n"
                + "      ,[QDMRLINO]\n"
                + "      ,[QDMRLENGTH1]\n"
                + "      ,[QDMRLENGTH2]\n"
                + "      ,[QDMRHEIGHT]\n"
                + "      ,[QDMREDT]\n"
                + "      ,[QDMRCDT]\n"
                + "      ,[QDMRUSER],[QDMRID])"
                + " VALUES('TWC'"
                + ",'" + code + "'"
                + ",(SELECT ISNULL(MAX(QDMRLINO),0)+1 FROM QDMROLDET WHERE QDMRSTYLE = '" + code + "')"
                + ",'" + length + "'"
                + "," + length + " * 100"
                + ",'0'"
                + ",CURRENT_TIMESTAMP"
                + ",CURRENT_TIMESTAMP"
                + ",'" + uid + "','" + qrmid + "')";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean add(String size, String desc, String inWidth, String inLength, String uid) {

        boolean result = false;

        if (uid.equals("null")) {
            uid = "";
        }

        String sql = "INSERT INTO [QDMROLL]"
                + " ([QDMCOM]\n"
                + "      ,[QDMSIZE]\n"
                + "      ,[QDMDESC]\n"
                + "      ,[QDMINWIDTH]\n"
                + "      ,[QDMINLENGTH]\n"
                + "      ,[QDMCMWIDTH]\n"
                + "      ,[QDMCMLENGTH]\n"
                + "      ,[QDMEDT]\n"
                + "      ,[QDMCDT]\n"
                + "      ,[QDMUSER])"
                + " VALUES('TWC'"
                + ",'" + size + "'"
                + ",'" + desc + "'"
                + "," + inWidth
                + "," + inLength
                + "," + inWidth + "*2.54"
                + "," + inLength + "*2.54"
                + ",CURRENT_TIMESTAMP"
                + ",CURRENT_TIMESTAMP"
                + ",'" + uid + "')";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean edit(String size, String desc, String inWidth, String inLength, String uid) {

        boolean result = false;

        if (uid.equals("null")) {
            uid = "";
        }

        String sql = "UPDATE [QDMROLL]\n"
                + "SET [QDMDESC] = '" + desc + "'"
                + "      ,[QDMINWIDTH] = " + inWidth
                + "      ,[QDMINLENGTH] = " + inLength
                + "      ,[QDMCMWIDTH] = " + inWidth + "*2.54"
                + "      ,[QDMCMLENGTH] = " + inLength + "*2.54"
                + "      ,[QDMEDT] = CURRENT_TIMESTAMP\n"
                + "      ,[QDMUSER] = '" + uid + "'"
                + "WHERE QDMSIZE = '" + size + "'";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public void deleteDet(String code, String line) {

        String sql = "DELETE FROM [QDMROLDET] "
                + "WHERE [QDMRSTYLE] = '" + code + "'\n"
                + "  AND [QDMRLINO] = '" + line + "'";

        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            ps.executeUpdate();

        } catch (Exception e) {

            e.printStackTrace();

        }

    }

    public void delete(String size) {

        String sql = "DELETE FROM QDMROLL "
                + "WHERE QDMSIZE = '" + size + "'";

        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            ps.executeUpdate();

        } catch (Exception e) {

            e.printStackTrace();

        }

    }
}
