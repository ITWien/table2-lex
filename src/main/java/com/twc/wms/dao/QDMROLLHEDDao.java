/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.dao;

import com.twc.wms.database.database;
import com.twc.wms.entity.QDESTYP;
import com.twc.wms.entity.QDMBAG;
import com.twc.wms.entity.QDMROLLHED;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 *
 * @author nutthawoot.noo
 */
public class QDMROLLHEDDao extends database {

    public boolean addHed(String code, String desc, String unit, String uid) {

        boolean result = false;

        if (uid != null) {
            if (uid.equals("null")) {
                uid = "";
            }
        }

        String sql = "INSERT INTO [QDMROLHED]"
                + " ([QDMRCOM]\n"
                + "      ,[QDMRSTYLE]\n"
                + "      ,[QDMRDESC]\n"
                + "      ,[QDMRUNIT]\n"
                + "      ,[QDMRX]\n"
                + "      ,[QDMREDT]\n"
                + "      ,[QDMRCDT]\n"
                + "      ,[QDMRUSER])"
                + " VALUES('TWC'"
                + ",'" + code + "'"
                + ",'" + desc + "'"
                + ",'" + unit + "'"
                + ",'0'"
                + ",CURRENT_TIMESTAMP"
                + ",CURRENT_TIMESTAMP"
                + ",'" + uid + "')";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean updateRollHed(String code, String desc, String unit, String equa, String x, String y, String uid) {

        boolean result = false;

        if (uid.equals("null")) {
            uid = "";
        }

        String sql = "UPDATE [QDMROLHED]\n"
                + "SET [QDMRDESC] = '" + desc + "'\n"
                + "      ,[QDMRUNIT] = '" + unit + "'\n"
                + "      ,[QDMREQUA] = '" + equa + "'\n"
                + "      ,[QDMRX] = '" + x + "'\n"
                + "      ,[QDMRY] = '" + y + "'\n"
                + "      ,[QDMREDT] = CURRENT_TIMESTAMP\n"
                + "      ,[QDMRUSER] = '" + uid + "'\n"
                + "WHERE [QDMRSTYLE] = '" + code + "'";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public String checkHed(String style) {
        PreparedStatement ps = null;
        ResultSet rs = null;
        String STRRETURN = null;
        try {
            String check = "SELECT [QDMRSTYLE]\n"
                    + "      ,[QDMRDESC]\n"
                    + "      ,[QDMRUNIT]\n"
                    + "      ,[QDMREQUA]\n"
                    + "      ,FORMAT([QDMRX],'0.0000') AS [QDMRX]\n"
                    + "      ,FORMAT([QDMRY],'0.0000')  AS [QDMRY]\n"
                    + "  FROM [RMShipment].[dbo].[QDMROLHED]\n"
                    + "  WHERE [QDMRSTYLE] = '" + style + "'";
//            System.out.println(check);
            ps = connect.prepareStatement(check);
            rs = ps.executeQuery();
            if (rs.next()) {
                STRRETURN = "f";
            } else {
                STRRETURN = "t";
            }
            connect.close();

        } catch (Exception exp) {
            exp.printStackTrace();
        }
        return STRRETURN;
    }

    public QDMROLLHED findQRMMAS(String id) {

        QDMROLLHED p = new QDMROLLHED();

        String sql = "SELECT LEFT(QRMCODE,8) AS [QRMCODE]\n"
                + "      ,REPLACE(QRMDESC,'#','[sharp]') AS [QRMDESC]\n"
                + "      ,format([QRMQTY],'#0.00') as [QRMQTY]\n"
                + "      ,[QRMBUN]\n"
                + "  FROM [RMShipment].[dbo].[QRMMAS]\n"
                + "  where [QRMID]='" + id + "'";
//        System.out.println(sql); 

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                p.setQDMRSTYLE(result.getString("QRMCODE"));
                p.setQDMRDESC(result.getString("QRMDESC"));
                p.setQDMRUNIT(result.getString("QRMBUN"));
                p.setQTY(result.getString("QRMQTY"));

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return p;

    }

    public QDMROLLHED findByStyle(String style) {

        QDMROLLHED p = new QDMROLLHED();

        String sql = "SELECT [QDMRSTYLE]\n"
                + "      ,[QDMRDESC]\n"
                + "      ,[QDMRUNIT]\n"
                + "      ,[QDMREQUA]\n"
                + "      ,FORMAT([QDMRX],'0.000') AS [QDMRX]\n"
                + "      ,FORMAT([QDMRY],'0.0000')  AS [QDMRY]\n"
                + "  FROM [RMShipment].[dbo].[QDMROLHED]\n"
                + "  WHERE [QDMRSTYLE] = '" + style + "'";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                p.setQDMRSTYLE(result.getString("QDMRSTYLE"));
                p.setQDMRDESC(result.getString("QDMRDESC"));
                p.setQDMRUNIT(result.getString("QDMRUNIT"));
                p.setQDMREQUA(result.getString("QDMREQUA"));
                p.setQDMRX(result.getString("QDMRX"));
                p.setQDMRY(result.getString("QDMRY"));

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return p;

    }

    public boolean add(String size, String desc, String inWidth, String inLength, String uid) {

        boolean result = false;

        if (uid.equals("null")) {
            uid = "";
        }

        String sql = "INSERT INTO [QDMROLL]"
                + " ([QDMCOM]\n"
                + "      ,[QDMSIZE]\n"
                + "      ,[QDMDESC]\n"
                + "      ,[QDMINWIDTH]\n"
                + "      ,[QDMINLENGTH]\n"
                + "      ,[QDMCMWIDTH]\n"
                + "      ,[QDMCMLENGTH]\n"
                + "      ,[QDMEDT]\n"
                + "      ,[QDMCDT]\n"
                + "      ,[QDMUSER])"
                + " VALUES('TWC'"
                + ",'" + size + "'"
                + ",'" + desc + "'"
                + "," + inWidth
                + "," + inLength
                + "," + inWidth + "*2.54"
                + "," + inLength + "*2.54"
                + ",CURRENT_TIMESTAMP"
                + ",CURRENT_TIMESTAMP"
                + ",'" + uid + "')";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean edit(String size, String desc, String inWidth, String inLength, String uid) {

        boolean result = false;

        if (uid.equals("null")) {
            uid = "";
        }

        String sql = "UPDATE [QDMROLL]\n"
                + "SET [QDMDESC] = '" + desc + "'"
                + "      ,[QDMINWIDTH] = " + inWidth
                + "      ,[QDMINLENGTH] = " + inLength
                + "      ,[QDMCMWIDTH] = " + inWidth + "*2.54"
                + "      ,[QDMCMLENGTH] = " + inLength + "*2.54"
                + "      ,[QDMEDT] = CURRENT_TIMESTAMP\n"
                + "      ,[QDMUSER] = '" + uid + "'"
                + "WHERE QDMSIZE = '" + size + "'";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public void delete(String size) {

        String sql = "DELETE FROM QDMROLL "
                + "WHERE QDMSIZE = '" + size + "'";

        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            ps.executeUpdate();

        } catch (Exception e) {

            e.printStackTrace();

        }

    }
}
