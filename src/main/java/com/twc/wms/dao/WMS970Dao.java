/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.dao;

import com.twc.wms.database.database;

import java.sql.*;

import java.util.ArrayList;

import java.util.List;

import com.twc.wms.entity.WMS970;
import com.twc.wms.entity.SAPMAS;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import com.twc.wms.database.MSSQLDbConnectionPool;
import com.twc.wms.entity.Qdest;

/**
 *
 * @author nutthawoot.noo
 */
public class WMS970Dao extends database {

    public List<WMS970> findDimension(String rm) {

        List<WMS970> objList = new ArrayList<WMS970>();

        String sql = "SELECT [QDMMAIN]\n"
                + "      ,[QDMSTYLE]\n"
                + "      ,[QDMWIDTH]\n"
                + "      ,[QDMLENGTH]\n"
                + "      ,[QDMHEIGHT]\n"
                + "      ,[QDMDIAMETER]\n"
                + "  FROM [RMShipment].[dbo].[QDMMAS]\n"
                + "  WHERE QDMSTYLE like '%" + rm + "%'\n"
                + "  ORDER BY [QDMSTYLE],[QDMMAIN]";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                WMS970 p = new WMS970();

                p.setRM(result.getString("QDMSTYLE"));
                p.setPack(result.getString("QDMMAIN"));
                p.setWidth(result.getString("QDMWIDTH"));
                p.setLength(result.getString("QDMLENGTH"));
                p.setHeight(result.getString("QDMHEIGHT"));
                p.setDiameter(result.getString("QDMDIAMETER"));

                objList.add(p);
            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return objList;

    }

    public List<WMS970> findQty(String rm, String plant) {

        List<WMS970> objList = new ArrayList<WMS970>();

        String sql = "DECLARE @RM NVARCHAR(8) = '" + rm + "'\n"
                + "  DECLARE @PLANT NVARCHAR(4) = '" + plant + "'\n"
                + "  SELECT LEFT([QRMCODE],8) AS RM, REPLACE([QRMCODE],@RM,'') AS COL, '' AS CODE\n"
                + "  ,(SELECT FORMAT(ISNULL(COUNT([QRMQTY]),0),'#,#0')\n"
                + "  FROM [RMShipment].[dbo].[QRMMAS]\n"
                + "  WHERE LEFT([QRMCODE],8) = @RM AND QRMSTS IN ('2')\n"
                + "  AND QRMPLANT = @PLANT AND ([QRMPACKTYPE] = 'BAG' OR [QRMPACKTYPE] = 'PACK')) AS BAG\n"
                + "  ,(SELECT FORMAT(ISNULL(COUNT([QRMQTY]),0),'#,#0')\n"
                + "  FROM [RMShipment].[dbo].[QRMMAS]\n"
                + "  WHERE LEFT([QRMCODE],8) = @RM AND QRMSTS IN ('2')\n"
                + "  AND QRMPLANT = @PLANT AND [QRMPACKTYPE] = 'ROLL') AS ROLL\n"
                + "  ,(SELECT FORMAT(ISNULL(COUNT([QRMQTY]),0),'#,#0')\n"
                + "  FROM [RMShipment].[dbo].[QRMMAS]\n"
                + "  WHERE LEFT([QRMCODE],8) = @RM AND QRMSTS IN ('2')\n"
                + "  AND QRMPLANT = @PLANT AND [QRMPACKTYPE] = 'BOX') AS BOX\n"
                + "  ,(SELECT FORMAT(ISNULL(COUNT([QRMQTY]),0),'#,#0')\n"
                + "  FROM [RMShipment].[dbo].[QRMMAS]\n"
                + "  WHERE LEFT([QRMCODE],8) = @RM AND QRMSTS IN ('2')\n"
                + "  AND QRMPLANT = @PLANT) AS TOTAL\n"
                + "  ,(SELECT FORMAT(ISNULL(SUM([QRMQTY]),0),'#,#0.00')\n"
                + "  FROM [RMShipment].[dbo].[QRMMAS]\n"
                + "  WHERE LEFT([QRMCODE],8) = @RM AND QRMSTS IN ('2')\n"
                + "  AND QRMPLANT = @PLANT) AS QTY\n"
                + "  ,(SELECT TOP 1 [QRMBUN]\n"
                + "  FROM [RMShipment].[dbo].[QRMMAS]\n"
                + "  WHERE LEFT([QRMCODE],8) = @RM AND QRMSTS IN ('2')\n"
                + "  AND QRMPLANT = @PLANT) AS UNIT\n"
                + "  FROM(\n"
                + "  SELECT DISTINCT LEFT([QRMCODE],8) AS [QRMCODE]\n"
                + "  FROM [RMShipment].[dbo].[QRMMAS]\n"
                + "  WHERE LEFT([QRMCODE],8) = @RM AND QRMSTS IN ('2')\n"
                + "  AND QRMPLANT = @PLANT\n"
                + "  )TMP\n"
                + "  UNION ALL\n"
                + "  SELECT LEFT([QRMCODE],8) AS RM, REPLACE([QRMCODE],@RM,'') AS COL, [QRMCODE] AS CODE\n"
                + "  ,(SELECT FORMAT(ISNULL(COUNT([QRMQTY]),0),'#,#0')\n"
                + "  FROM [RMShipment].[dbo].[QRMMAS]\n"
                + "  WHERE LEFT([QRMCODE],8) = @RM AND QRMSTS IN ('2')\n"
                + "  AND QRMPLANT = @PLANT AND [QRMCODE] = TMP.[QRMCODE] AND ([QRMPACKTYPE] = 'BAG' OR [QRMPACKTYPE] = 'PACK')) AS BAG\n"
                + "  ,(SELECT FORMAT(ISNULL(COUNT([QRMQTY]),0),'#,#0')\n"
                + "  FROM [RMShipment].[dbo].[QRMMAS]\n"
                + "  WHERE LEFT([QRMCODE],8) = @RM AND QRMSTS IN ('2')\n"
                + "  AND QRMPLANT = @PLANT AND [QRMCODE] = TMP.[QRMCODE] AND [QRMPACKTYPE] = 'ROLL') AS ROLL\n"
                + "  ,(SELECT FORMAT(ISNULL(COUNT([QRMQTY]),0),'#,#0')\n"
                + "  FROM [RMShipment].[dbo].[QRMMAS]\n"
                + "  WHERE LEFT([QRMCODE],8) = @RM AND QRMSTS IN ('2')\n"
                + "  AND QRMPLANT = @PLANT AND [QRMCODE] = TMP.[QRMCODE] AND [QRMPACKTYPE] = 'BOX') AS BOX\n"
                + "  ,(SELECT FORMAT(ISNULL(COUNT([QRMQTY]),0),'#,#0')\n"
                + "  FROM [RMShipment].[dbo].[QRMMAS]\n"
                + "  WHERE LEFT([QRMCODE],8) = @RM AND QRMSTS IN ('2')\n"
                + "  AND QRMPLANT = @PLANT AND [QRMCODE] = TMP.[QRMCODE]) AS TOTAL\n"
                + "  ,(SELECT FORMAT(ISNULL(SUM([QRMQTY]),0),'#,#0.00')\n"
                + "  FROM [RMShipment].[dbo].[QRMMAS]\n"
                + "  WHERE LEFT([QRMCODE],8) = @RM AND QRMSTS IN ('2')\n"
                + "  AND QRMPLANT = @PLANT AND [QRMCODE] = TMP.[QRMCODE]) AS QTY\n"
                + "  ,(SELECT TOP 1 [QRMBUN]\n"
                + "  FROM [RMShipment].[dbo].[QRMMAS]\n"
                + "  WHERE LEFT([QRMCODE],8) = @RM AND QRMSTS IN ('2')\n"
                + "  AND QRMPLANT = @PLANT AND [QRMCODE] = TMP.[QRMCODE]) AS UNIT\n"
                + "  FROM(\n"
                + "  SELECT DISTINCT [QRMCODE]\n"
                + "  FROM [RMShipment].[dbo].[QRMMAS]\n"
                + "  WHERE LEFT([QRMCODE],8) = @RM AND QRMSTS IN ('2')\n"
                + "  AND QRMPLANT = @PLANT\n"
                + "  )TMP\n"
                + "  ORDER BY CODE";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                WMS970 p = new WMS970();

                p.setRM(result.getString("RM"));
                p.setCOL(result.getString("COL"));
                p.setCODE(result.getString("CODE"));
                p.setBAG(result.getString("BAG"));
                p.setROLL(result.getString("ROLL"));
                p.setBOX(result.getString("BOX"));
                p.setTOTAL(result.getString("TOTAL"));
                p.setQTY(result.getString("QTY"));
                p.setUNIT(result.getString("UNIT"));

                objList.add(p);
            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return objList;

    }

    public List<WMS970> findQtyDet(String rm, String plant, String col) {

        List<WMS970> objList = new ArrayList<WMS970>();

        String sql = "DECLARE @RM NVARCHAR(8) = '" + rm + "'\n"
                + "  DECLARE @PLANT NVARCHAR(4) = '" + plant + "'\n"
                + "  SELECT LEFT([QRMCODE],8) AS RM, REPLACE([QRMCODE],@RM,'') AS COL\n"
                + "  ,[QRMCODE] AS CODE, QRMPACKTYPE AS PACKTYPE, [QRMLOC] AS LOCATION, [QRMBUN] AS UNIT\n"
                + "  ,[QRMDESC] AS DESCS, (SELECT TOP 1 [SAPMGRPDESC] FROM [SAPMAS] WHERE SAPMAT = [QRMCODE]) AS GRPDESCS\n"
                + "  ,FORMAT([QRMQTY],'#,#0.00') AS QTY, QRMID AS ID\n"
                + "  FROM [RMShipment].[dbo].[QRMMAS]\n"
                + "  WHERE LEFT([QRMCODE],8) = @RM\n"
                + "  AND REPLACE([QRMCODE],@RM,'') = '" + col + "'\n"
                + "  AND QRMPLANT = @PLANT AND QRMSTS IN ('2')\n"
                + "  ORDER BY QRMID";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                WMS970 p = new WMS970();

                p.setRM(result.getString("RM"));
                p.setCOL(result.getString("COL"));
                p.setCODE(result.getString("CODE"));
                p.setQTY(result.getString("QTY"));
                p.setID(result.getString("ID"));
                p.setPACKTYPE(result.getString("PACKTYPE"));
                p.setLOCATION(result.getString("LOCATION"));
                p.setUNIT(result.getString("UNIT"));
                p.setDESCS(result.getString("DESCS"));
                p.setGRPDESCS(result.getString("GRPDESCS"));

                objList.add(p);
            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return objList;

    }
}
