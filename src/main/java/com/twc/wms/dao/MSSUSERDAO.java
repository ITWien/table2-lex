/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.dao;

import com.twc.wms.database.database;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author nutthawoot.noo
 */
public class MSSUSERDAO extends database {

    public String getUser(String USERNAME, String PASSWORD) {

        String USER = null;
        // HttpServletRequest request=null;

        try {

            PreparedStatement statement = connect.prepareStatement("select * from MSSUSER where  USERID= ?  AND PASSWORD = ? AND MSLEVEL>6 ");
            statement.setString(1, USERNAME);
            statement.setString(2, PASSWORD);
            ResultSet set = statement.executeQuery();

            if (set.next()) {
                USER = set.getString("USERS");
            }
            set.close();
            statement.close();

        } catch (SQLException err) {
            err.printStackTrace();
        }

        return USER;

    }

    public String[] getUserAll(String USERNAME, String PASSWORD) {

        int count = 0;
        String[] USERGRP = new String[1];

        try {

            PreparedStatement statement = connect.prepareStatement("select * from MSSUSER where  USERID= ?  AND PASSWORD = ?  AND MSLEVEL>6 ");
            statement.setString(1, USERNAME);
            statement.setString(2, PASSWORD);
            ResultSet set = statement.executeQuery();

            while (set.next()) {
                //  request.setAttribute("MSLEVEL",set.getString("MSLEVEL"));
                //	request.setAttribute("MSWHSE",set.getString("MSWHSE"));
                //	request.setAttribute("MSPICTURE",set.getString("MSPICTURE"));
                USERGRP[count] = set.getString("MSWHSE") + "@" + set.getString("MSLEVEL") + "@" + set.getString("MSPICTURE");
                //System.out.println("whse="+set.getString("MSWHSE"));

                count++;

            }
            set.close();
            statement.close();

        } catch (SQLException err) {
            err.printStackTrace();
        }

        return USERGRP;

    }

}
