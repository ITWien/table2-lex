/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.dao;

import com.twc.wms.database.database;

import java.sql.*;

import java.util.ArrayList;

import java.util.List;

import com.twc.wms.entity.MSSMATN;
import com.twc.wms.entity.SAPMAS;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import com.twc.wms.database.MSSQLDbConnectionPool;

/**
 *
 * @author nutthawoot.noo
 */
public class MSSMATNDao extends database {

    public List<MSSMATN> findAllByUser(String uid) {

        List<MSSMATN> UAList = new ArrayList<MSSMATN>();

        String sql = "SELECT * \n"
                + "FROM(\n"
                + "SELECT * \n"
                + ",case when (select top 1 isnull(MSWHSE,'') from MSSUSER where USERID = '" + uid + "') = 'TWC' \n"
                + "then MSWHSE \n"
                + "else (select top 1 isnull(MSWHSE,'') from MSSUSER where USERID = '" + uid + "') end AS WHS\n"
                + "FROM MSSMATN \n"
                + "INNER JOIN QWHMAS ON MSSMATN.MSWHSE=QWHMAS.QWHCOD \n"
                + ")TMP\n"
                + "WHERE WHS = MSWHSE\n"
                + "ORDER BY LGPBE";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                MSSMATN p = new MSSMATN();

                p.setUid(result.getString("LGPBE"));

                p.setName(result.getString("MATCNAME"));

                p.setWarehouse(result.getString("MSWHSE") + " : " + result.getString("QWHNAME"));

                UAList.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public List<MSSMATN> findAll() {

        List<MSSMATN> UAList = new ArrayList<MSSMATN>();

        String sql = "SELECT * FROM MSSMATN "
                + "INNER JOIN QWHMAS ON MSSMATN.MSWHSE=QWHMAS.QWHCOD "
                + "ORDER BY LGPBE ";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                MSSMATN p = new MSSMATN();

                p.setUid(result.getString("LGPBE"));

                p.setName(result.getString("MATCNAME"));

                p.setWarehouse(result.getString("MSWHSE") + " : " + result.getString("QWHNAME"));

                UAList.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public String check(String id) {
        PreparedStatement ps = null;
        ResultSet rs = null;
        String STRRETURN = null;
        try {
            String check = "Select * FROM [RMShipment].[dbo].[MSSMATN] where [LGPBE] = '" + id + "'";
            ps = connect.prepareStatement(check);
            rs = ps.executeQuery();
            if (rs.next()) {
                STRRETURN = "f";
            } else {
                STRRETURN = "t";
            }
            connect.close();

        } catch (Exception exp) {
            exp.printStackTrace();
        }
        return STRRETURN;
    }

    public boolean add(MSSMATN ua) {

        boolean result = false;

        String sql = "INSERT INTO MSSMATN"
                + " (MSCONO, MSWHSE, LGPBE, MATCNAME)"
                + " VALUES(?, ?, ?, ?)";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ps.setString(1, "TWC");

            ps.setString(2, ua.getWarehouse());

            ps.setString(3, ua.getUid());

            ps.setString(4, ua.getName());

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public MSSMATN findByUid(String id) {

        MSSMATN ua = new MSSMATN();

        String sql = "SELECT * FROM MSSMATN WHERE LGPBE = ?";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ps.setString(1, id);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                ua.setUid(result.getString("LGPBE"));
                ua.setName(result.getString("MATCNAME"));
                ua.setWarehouse(result.getString("MSWHSE"));

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return ua;

    }

    public boolean edit(MSSMATN ua) {

        boolean result = false;

        String sql = "UPDATE MSSMATN "
                + "SET MSWHSE = ?, MATCNAME = ?"
                + " WHERE LGPBE = ?";
        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ps.setString(3, ua.getUid());
            ps.setString(1, ua.getWarehouse());
            ps.setString(2, ua.getName());

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public void delete(String uid) {

        String sql = "DELETE FROM MSSMATN WHERE LGPBE = ?";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ps.setString(1, uid);

            ps.executeUpdate();

        } catch (Exception e) {

            e.printStackTrace();

        }

    }
}
