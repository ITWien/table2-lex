/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.dao;

import com.twc.wms.database.database;

import java.sql.*;

import java.util.ArrayList;

import java.util.List;

import com.twc.wms.entity.WMS970;
import com.twc.wms.entity.SAPMAS;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import com.twc.wms.database.MSSQLDbConnectionPool;
import com.twc.wms.entity.Qdest;
import com.twc.wms.entity.SAPONH;
import com.twc.wms.entity.TOT930;
import com.twc.wms.entity.TOT930TB;

/**
 *
 * @author nutthawoot.noo
 */
public class WMS935Dao extends database {

    public List<SAPONH> findOnhandDate(String plant) {

        List<SAPONH> objList = new ArrayList<SAPONH>();

        String sql = "SELECT DISTINCT SAPTRDT\n"
                + "      ,SUBSTRING(CONVERT(varchar,SAPTRDT), 7, 2)+'/'\n"
                + "	   +SUBSTRING(CONVERT(varchar,SAPTRDT), 5, 2)+'/'\n"
                + "	   +SUBSTRING(CONVERT(varchar,SAPTRDT), 1, 4) AS ONHANDDATE \n"
                + "FROM SAPONH\n"
                + "WHERE SAPPLANT = '" + plant + "'\n"
                + "ORDER BY SAPTRDT DESC";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                SAPONH p = new SAPONH();

                p.setSAPTRDT(result.getString("SAPTRDT"));
                p.setONHANDDATE(result.getString("ONHANDDATE"));

                objList.add(p);
            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return objList;

    }

    public List<TOT930> findTOT930(String oh1000, String oh1050, String pt1000, String pt1050, String wh, String whTWC) {

        List<TOT930> objList = new ArrayList<TOT930>();

        String WH = "";

        if (whTWC.equals("TWC")) {
            WH = "--";
        }

        String sql = "DECLARE @OHDT1000 NVARCHAR(10) = '" + oh1000 + "'\n"
                + "DECLARE @OHDT1050 NVARCHAR(10) = '" + oh1050 + "'\n"
                + "DECLARE @PLNT1000 NVARCHAR(10) = '" + pt1000 + "'\n"
                + "DECLARE @PLNT1050 NVARCHAR(10) = '" + pt1050 + "'\n"
                + "DECLARE @WH NVARCHAR(10) = '" + whTWC + "'\n"
                + "SELECT TWHSE +' : '+ (select [QWHNAME] from [QWHMAS] where [QWHCOD] = TWHSE) as TWHSE\n"
                + "   ,TMTCL\n"
                + "   ,(select format(isnull(sum(TSKU),0),'#,#0') from TOT930 SB\n"
                + "	    where SB.TOHDT IN (@OHDT1000,@OHDT1050)\n"
                + "          and SB.TPLANT IN (@PLNT1000,@PLNT1050)\n"
                + "               " + WH + "and SB.TWHSE IN (@WH)\n"
                + "		  and SB.TWHSE = BS.TWHSE\n"
                + "		  and SB.TMTCL = BS.TMTCL\n"
                + "		  and SB.TPLANT = '1000') as SKU_1000\n"
                + "   ,(select format(isnull(sum(TAMT),0),'#,#0.00') from TOT930 SB\n"
                + "	    where SB.TOHDT IN (@OHDT1000,@OHDT1050)\n"
                + "          and SB.TPLANT IN (@PLNT1000,@PLNT1050)\n"
                + "		  " + WH + "and SB.TWHSE IN (@WH)\n"
                + "		  and SB.TWHSE = BS.TWHSE\n"
                + "		  and SB.TMTCL = BS.TMTCL\n"
                + "		  and SB.TPLANT = '1000') as AMT_1000\n"
                + "   ,(select format(isnull(sum(TSKU),0),'#,#0') from TOT930 SB\n"
                + "	    where SB.TOHDT IN (@OHDT1000,@OHDT1050)\n"
                + "          and SB.TPLANT IN (@PLNT1000,@PLNT1050)\n"
                + "		  " + WH + "and SB.TWHSE IN (@WH)\n"
                + "		  and SB.TWHSE = BS.TWHSE\n"
                + "		  and SB.TMTCL = BS.TMTCL  \n"
                + "		  and SB.TPLANT = '1050') as SKU_1050\n"
                + "   ,(select format(isnull(sum(TAMT),0),'#,#0.00') from TOT930 SB\n"
                + "	    where SB.TOHDT IN (@OHDT1000,@OHDT1050)\n"
                + "          and SB.TPLANT IN (@PLNT1000,@PLNT1050)\n"
                + "		  " + WH + "and SB.TWHSE IN (@WH)\n"
                + "		  and SB.TWHSE = BS.TWHSE\n"
                + "		  and SB.TMTCL = BS.TMTCL\n"
                + "		  and SB.TPLANT = '1050') as AMT_1050\n"
                + "   ,(select format(isnull(sum(TSKU),0),'#,#0') from TOT930 SB\n"
                + "	    where SB.TOHDT IN (@OHDT1000,@OHDT1050)\n"
                + "          and SB.TPLANT IN (@PLNT1000,@PLNT1050)\n"
                + "		  " + WH + "and SB.TWHSE IN (@WH)\n"
                + "		  and SB.TWHSE = BS.TWHSE\n"
                + "		  and SB.TMTCL = BS.TMTCL) as SKU_TOT\n"
                + "   ,(select format(isnull(sum(TAMT),0),'#,#0.00') from TOT930 SB\n"
                + "	    where SB.TOHDT IN (@OHDT1000,@OHDT1050)\n"
                + "          and SB.TPLANT IN (@PLNT1000,@PLNT1050)\n"
                + "		  " + WH + "and SB.TWHSE IN (@WH)\n"
                + "		  and SB.TWHSE = BS.TWHSE\n"
                + "		  and SB.TMTCL = BS.TMTCL) as AMT_TOT\n"
                + "\n"
                + "   ,(select format(isnull(sum(TSKU),0),'#,#0') from TOT930 SB\n"
                + "	    where SB.TOHDT IN (@OHDT1000,@OHDT1050)\n"
                + "          and SB.TPLANT IN (@PLNT1000,@PLNT1050)\n"
                + "		  " + WH + "and SB.TWHSE IN (@WH)\n"
                + "		  and SB.TWHSE = BS.TWHSE\n"
                + "		  and SB.TPLANT = '1000') as T1000_SKU\n"
                + "   ,(select format(isnull(sum(TAMT),0),'#,#0.00') from TOT930 SB\n"
                + "	    where SB.TOHDT IN (@OHDT1000,@OHDT1050)\n"
                + "          and SB.TPLANT IN (@PLNT1000,@PLNT1050)\n"
                + "		  " + WH + "and SB.TWHSE IN (@WH)\n"
                + "		  and SB.TWHSE = BS.TWHSE\n"
                + "		  and SB.TPLANT = '1000') as T1000_AMT\n"
                + "   ,(select format(isnull(sum(TSKU),0),'#,#0') from TOT930 SB\n"
                + "	    where SB.TOHDT IN (@OHDT1000,@OHDT1050)\n"
                + "          and SB.TPLANT IN (@PLNT1000,@PLNT1050)\n"
                + "		  " + WH + "and SB.TWHSE IN (@WH)\n"
                + "		  and SB.TWHSE = BS.TWHSE\n"
                + "		  and SB.TPLANT = '1050') as T1050_SKU\n"
                + "   ,(select format(isnull(sum(TAMT),0),'#,#0.00') from TOT930 SB\n"
                + "	    where SB.TOHDT IN (@OHDT1000,@OHDT1050)\n"
                + "          and SB.TPLANT IN (@PLNT1000,@PLNT1050)\n"
                + "		  " + WH + "and SB.TWHSE IN (@WH)\n"
                + "		  and SB.TWHSE = BS.TWHSE\n"
                + "		  and SB.TPLANT = '1050') as T1050_AMT\n"
                + "   ,(select format(isnull(sum(TSKU),0),'#,#0') from TOT930 SB\n"
                + "	    where SB.TOHDT IN (@OHDT1000,@OHDT1050)\n"
                + "          and SB.TPLANT IN (@PLNT1000,@PLNT1050)\n"
                + "		  " + WH + "and SB.TWHSE IN (@WH)\n"
                + "		  and SB.TWHSE = BS.TWHSE) as TTOT_SKU\n"
                + "   ,(select format(isnull(sum(TAMT),0),'#,#0.00') from TOT930 SB\n"
                + "	    where SB.TOHDT IN (@OHDT1000,@OHDT1050)\n"
                + "          and SB.TPLANT IN (@PLNT1000,@PLNT1050)\n"
                + "		  " + WH + "and SB.TWHSE IN (@WH)\n"
                + "		  and SB.TWHSE = BS.TWHSE) as TTOT_AMT\n"
                + "\n"
                + "   ,(select format(isnull(sum(TSKU),0),'#,#0') from TOT930 SB\n"
                + "	    where SB.TOHDT IN (@OHDT1000,@OHDT1050)\n"
                + "          and SB.TPLANT IN (@PLNT1000,@PLNT1050)\n"
                + "		  " + WH + "and SB.TWHSE IN (@WH)\n"
                + "		  and SB.TPLANT = '1000') as GT1000_SKU\n"
                + "   ,(select format(isnull(sum(TAMT),0),'#,#0.00') from TOT930 SB\n"
                + "	    where SB.TOHDT IN (@OHDT1000,@OHDT1050)\n"
                + "          and SB.TPLANT IN (@PLNT1000,@PLNT1050)\n"
                + "		  " + WH + "and SB.TWHSE IN (@WH)\n"
                + "		  and SB.TPLANT = '1000') as GT1000_AMT\n"
                + "   ,(select format(isnull(sum(TSKU),0),'#,#0') from TOT930 SB\n"
                + "	    where SB.TOHDT IN (@OHDT1000,@OHDT1050)\n"
                + "          and SB.TPLANT IN (@PLNT1000,@PLNT1050)\n"
                + "		  " + WH + "and SB.TWHSE IN (@WH)\n"
                + "		  and SB.TPLANT = '1050') as GT1050_SKU\n"
                + "   ,(select format(isnull(sum(TAMT),0),'#,#0.00') from TOT930 SB\n"
                + "	    where SB.TOHDT IN (@OHDT1000,@OHDT1050)\n"
                + "          and SB.TPLANT IN (@PLNT1000,@PLNT1050)\n"
                + "		  " + WH + "and SB.TWHSE IN (@WH)\n"
                + "		  and SB.TPLANT = '1050') as GT1050_AMT\n"
                + "   ,(select format(isnull(sum(TSKU),0),'#,#0') from TOT930 SB\n"
                + "        where SB.TOHDT IN (@OHDT1000,@OHDT1050)\n"
                + "          and SB.TPLANT IN (@PLNT1000,@PLNT1050)\n"
                + "		  " + WH + "and SB.TWHSE IN (@WH)\n"
                + "		  ) as GTTOT_SKU\n"
                + "   ,(select format(isnull(sum(TAMT),0),'#,#0.00') from TOT930 SB\n"
                + "        where SB.TOHDT IN (@OHDT1000,@OHDT1050)\n"
                + "          and SB.TPLANT IN (@PLNT1000,@PLNT1050)\n"
                + "		  " + WH + "and SB.TWHSE IN (@WH)\n"
                + "		  ) as GTTOT_AMT\n"
                + "FROM(\n"
                + "SELECT DISTINCT TWHSE\n"
                + "      ,TMTCL\n"
                + "  FROM TOT930\n"
                + "  where TOHDT IN (@OHDT1000,@OHDT1050)\n"
                + "    and TPLANT IN (@PLNT1000,@PLNT1050)\n"
                + "    " + WH + "and TWHSE IN (@WH)\n"
                + "  ) BS\n"
                + "  order by TWHSE, TMTCL";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                String whs = result.getString("TWHSE").split(" : ")[0].trim();
                if (wh.equals(whs)) {

                    TOT930 p = new TOT930();

                    p.setTWHSE(result.getString("TWHSE"));
                    p.setTMTCL(result.getString("TMTCL"));
                    p.setSKU_1000(result.getString("SKU_1000"));
                    p.setAMT_1000(result.getString("AMT_1000"));
                    p.setSKU_1050(result.getString("SKU_1050"));
                    p.setAMT_1050(result.getString("AMT_1050"));
                    p.setSKU_TOT(result.getString("SKU_TOT"));
                    p.setAMT_TOT(result.getString("AMT_TOT"));
                    p.setT1000_SKU(result.getString("T1000_SKU"));
                    p.setT1000_AMT(result.getString("T1000_AMT"));
                    p.setT1050_SKU(result.getString("T1050_SKU"));
                    p.setT1050_AMT(result.getString("T1050_AMT"));
                    p.setTTOT_SKU(result.getString("TTOT_SKU"));
                    p.setTTOT_AMT(result.getString("TTOT_AMT"));
                    p.setGT1000_SKU(result.getString("GT1000_SKU"));
                    p.setGT1000_AMT(result.getString("GT1000_AMT"));
                    p.setGT1050_SKU(result.getString("GT1050_SKU"));
                    p.setGT1050_AMT(result.getString("GT1050_AMT"));
                    p.setGTTOT_SKU(result.getString("GTTOT_SKU"));
                    p.setGTTOT_AMT(result.getString("GTTOT_AMT"));

                    objList.add(p);

                }
            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return objList;

    }

    public List<TOT930TB> findWHTOT930(String oh1000, String oh1050, String pt1000, String pt1050, String wh) {

        List<TOT930TB> objList = new ArrayList<TOT930TB>();

        String WH = "";

        if (wh.equals("TWC")) {
            WH = "--";
        }

        String sql = "DECLARE @OHDT1000 NVARCHAR(10) = '" + oh1000 + "'\n"
                + "DECLARE @OHDT1050 NVARCHAR(10) = '" + oh1050 + "'\n"
                + "DECLARE @PLNT1000 NVARCHAR(10) = '" + pt1000 + "'\n"
                + "DECLARE @PLNT1050 NVARCHAR(10) = '" + pt1050 + "'\n"
                + "DECLARE @WH NVARCHAR(10) = '" + wh + "'\n"
                + "SELECT DISTINCT TWHSE\n"
                + "  FROM TOT930\n"
                + "  where TOHDT IN (@OHDT1000,@OHDT1050)\n"
                + "    and TPLANT IN (@PLNT1000,@PLNT1050)\n"
                + "    " + WH + "and TWHSE IN (@WH)\n"
                + "  order by TWHSE";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                TOT930TB p = new TOT930TB();

                p.setWH(result.getString("TWHSE"));

                objList.add(p);
            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return objList;

    }
}
