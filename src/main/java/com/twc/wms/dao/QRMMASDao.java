/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.dao;

import com.twc.wms.database.database;
import com.twc.wms.entity.PALMOVE;
import java.sql.*;

import java.util.ArrayList;

import java.util.List;

import com.twc.wms.entity.QRMTRA_2;

/**
 *
 * @author nutthawoot.noo
 */
public class QRMMASDao extends database {

    public boolean UpdateUser(String qrid, String uid) {

        boolean result = false;

        String sql = "UPDATE QRMMAS "
                + "SET QRMUSER = '" + uid + "' "
                + "WHERE QRMID = '" + qrid + "' ";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public List<PALMOVE> findPallet(String from) {

        List<PALMOVE> QsetList = new ArrayList<PALMOVE>();

        String where = "";
        if (from.length() == 17) {
            where = "WHERE QRMID = '" + from + "' ";
        } else if (from.length() < 17) {
            where = "WHERE QRMPALLET = '" + from + "' ";
        }

        String sql = "SELECT QRMID, QRMCODE, QRMDESC, QRMPALLET\n"
                + "FROM QRMMAS \n"
                + where;

//        System.out.println(sql); 
        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                PALMOVE p = new PALMOVE();

                p.setNo(result.getString("QRMID"));
                p.setId(result.getString("QRMCODE"));
                p.setCode(result.getString("QRMDESC"));
                p.setDesc(result.getString("QRMPALLET"));

                QsetList.add(p);

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return QsetList;

    }

    public boolean UpdatePallet(String id, String to) {

        boolean result = false;

        String sql = "UPDATE QRMMAS "
                + "SET QRMPALLET = '" + to + "' "
                + "WHERE QRMID = '" + id + "' ";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public String CheckApprove(String id) {

        String p = "";

        String[] parts = id.split("-");
        id = parts[0];

        String sql = "SELECT QRMID, QRMSTS\n"
                + "FROM QRMMAS\n"
                + "WHERE QRMID = '" + id + "' ";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                p = result.getString("QRMSTS").trim();

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return p;

    }

    public boolean UpdateStatus(String id, String sts) {

        boolean result = false;

        String sql = "UPDATE QRMMAS "
                + "SET QRMSTS = '" + sts + "' "
                + "WHERE QRMID = '" + id + "' ";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean UpdateStatusMVT301(String id, String sts, String plant) {

        boolean result = false;

        String sql = "UPDATE QRMMAS "
                + "SET QRMSTS = '" + sts + "', QRMPLANT = '" + plant + "' "
                + "WHERE QRMID = '" + id + "' ";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean UpdateStatusMVT311(String id, String sts) {

        boolean result = false;

        String sql = "UPDATE QRMMAS "
                + "SET QRMSTS = '" + sts + "', QRMVAL = '03' "
                + "WHERE QRMID = '" + id + "' ";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean UpdateStatusMVT541(String id, String sts) {

        boolean result = false;

        String sql = "UPDATE QRMMAS "
                + "SET QRMSTS = '" + sts + "' "
                + "WHERE QRMID = '" + id + "' ";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public QRMTRA_2 findDESC(String id) {

        QRMTRA_2 p = new QRMTRA_2();

        String sql = "SELECT * FROM QRMMAS "
                + "WHERE QRMID = ? ";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ps.setString(1, id);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                p.setName(result.getString("QRMDESC"));
                p.setPack(result.getString("QRMPACKTYPE"));
                p.setTax(result.getString("QRMPARENT"));

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return p;

    }

    public QRMTRA_2 findByCode(String code) {

        QRMTRA_2 p = new QRMTRA_2();

        String sql = "SELECT * FROM QRMMAS "
                + "WHERE QRMCODE = ? ";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ps.setString(1, code);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                p.setName(result.getString("QRMDESC"));
                p.setPack(result.getString("QRMPACKTYPE"));

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return p;

    }

    public List<QRMTRA_2> selectByMatCode(String matCode) {

        List<QRMTRA_2> QMList = new ArrayList<QRMTRA_2>();

        String sql = "SELECT * FROM QRMMAS "
                + "WHERE QRMCODE = ? ";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ps.setString(1, matCode);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                QRMTRA_2 p = new QRMTRA_2();

                p.setMatCode(result.getString("QRMCODE"));
                p.setName(result.getString("QRMDESC"));
                p.setPack(result.getString("QRMPACKTYPE"));
                p.setTax(result.getString("QRMPARENT"));

                QMList.add(p);

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return QMList;

    }

}
