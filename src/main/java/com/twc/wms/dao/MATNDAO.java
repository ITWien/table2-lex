/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.dao;

import com.twc.wms.database.database;
import com.twc.wms.entity.MPDATAD;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author nutthawoot.noo
 */
public class MATNDAO extends database {

    public void add(MPDATAD MPDATAD) {

        try {
            PreparedStatement statement = connect.prepareStatement("insert into MSSMATN(LGPBE, MATCNAME) values (?, ?)");
            statement.setString(1, MPDATAD.getLGPBE());
            statement.setString(2, MPDATAD.getMATCNAME());
            statement.executeUpdate();
        } catch (SQLException err) {
            err.printStackTrace();
        }

    }

    public void update(MPDATAD MPDATAD) {

        try {
            PreparedStatement statement = connect.prepareStatement("update MSSMATN set MATCNAME=? where LGPBE=?");
            statement.setString(1, MPDATAD.getMATCNAME());
            statement.setString(2, MPDATAD.getLGPBE());
            statement.executeUpdate();
        } catch (SQLException err) {
            err.printStackTrace();
        }

    }

    public boolean checkDuplicate(MPDATAD MPDATAD) {

        boolean status = false;

        try {
            PreparedStatement statement = connect.prepareStatement("select * from MSSMATN where LGPBE=?");
            statement.setString(1, MPDATAD.getLGPBE());
            ResultSet set = statement.executeQuery();

            if (set.next()) {
                status = true;
            }

        } catch (SQLException err) {
            status = true;
            err.printStackTrace();
        }

        return status;

    }

    public String getMatName(String LGPBE) {

        String name = "";

        try {
            PreparedStatement statement = connect.prepareStatement("select MATCNAME from MSSMATN where LGPBE = ?");
            statement.setString(1, LGPBE);
            ResultSet set = statement.executeQuery();

            if (set.next()) {
                name = set.getString("MATCNAME");
            }

        } catch (SQLException err) {
            err.printStackTrace();
        }

        return name;
    }
}
