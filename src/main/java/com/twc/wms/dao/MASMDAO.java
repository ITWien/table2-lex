/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.dao;

import com.twc.wms.database.database;
import com.twc.wms.entity.MPDATAD;
import com.twc.wms.entity.MPDATAS;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author nutthawoot.noo
 */
public class MASMDAO extends database {

    public void add(MPDATAD MPDATAD) {

        try {
            PreparedStatement statement = connect.prepareStatement("insert into MSSMASM(MATNR, ARKTX, LGPBE, MATWID, MATCOM, UNR01, UNR03, VRKME, PRICE) values (?, ?, ?, ?, ?, ?, ?, ?, ?)");
            statement.setString(1, MPDATAD.getMATNR());
            statement.setString(2, MPDATAD.getARKTX());
            statement.setString(3, MPDATAD.getLGPBE());
            statement.setString(4, MPDATAD.getMATWID());
            statement.setString(5, MPDATAD.getMATCOM());
            statement.setFloat(6, MPDATAD.getUNR01());
            statement.setFloat(7, MPDATAD.getUNR03());
            statement.setString(8, MPDATAD.getVRKME());
            statement.setFloat(9, MPDATAD.getPRICE());
            statement.executeUpdate();
            statement.close();
        } catch (SQLException err) {
            err.printStackTrace();
        }

    }

    public void update(MPDATAD MPDATAD) {

        try {
            PreparedStatement statement = connect.prepareStatement("update MSSMASM set ARKTX=?, LGPBE=?, MATWID=?, MATCOM=?, UNR01=?, UNR03=?, VRKME=?, PRICE=? where MATNR=?");
            //PreparedStatement statement = connect.prepareStatement("update MSSMASM set ARKTX=?, LGPBE=?, MATWID=?, MATCOM=?, UNR01=?, UNR03=?, VRKME=?, PRICE=?,ALLOCATE=0 where MATNR=?");
            statement.setString(1, MPDATAD.getARKTX());
            statement.setString(2, MPDATAD.getLGPBE());
            statement.setString(3, MPDATAD.getMATWID());
            statement.setString(4, MPDATAD.getMATCOM());
            statement.setFloat(5, MPDATAD.getUNR01());
            statement.setFloat(6, MPDATAD.getUNR03());
            statement.setString(7, MPDATAD.getVRKME());
            statement.setFloat(8, MPDATAD.getPRICE());
            statement.setString(9, MPDATAD.getMATNR());
            statement.executeUpdate();
            statement.close();
        } catch (SQLException err) {
            err.printStackTrace();
        }

    }

    public boolean checkDuplicate(MPDATAD MPDATAD) {

        boolean status = false;

        try {
            PreparedStatement statement = connect.prepareStatement("select * from MSSMASM where MATNR=?");
            statement.setString(1, MPDATAD.getMATNR());
            ResultSet set = statement.executeQuery();

            if (set.next()) {
                status = true;
            }
            set.close();
            statement.close();

        } catch (SQLException err) {
            status = true;
            err.printStackTrace();
        }

        return status;

    }

    public List<MPDATAD> getOnHand(String POFG, String LGPBE, String GRPNO, String ISSUENO, String CHECK) {

        List<MPDATAD> listMPDATAD = new ArrayList<MPDATAD>();
        String CHECKSTATUS = "";

        try {

            PreparedStatement statement = null;

            if (CHECK.equals("1")) {

                //statement = connect.prepareStatement("select * from ( select A.MATNR, A.ARKTX, SUM(B.KWMENG) AS KWMENG, A.UNR01, A.UNR03 from MSSMASM A left join MSSMASD B on A.MATNR = B.MATNR left join MSSMASH C on B.VBELN = C.VBELN where C.POFG = ? and A.LGPBE = ? and C.GRPNO = ? and B.ISSUENO = ? group by A.MATNR, A.ARKTX, A.UNR01, A.UNR03 ) QR1 WHERE ( QR1.UNR01 + QR1.UNR03 - QR1.KWMENG) < 0 ");
                statement = connect.prepareStatement(" select * from ( select A.MATNR, A.ARKTX, SUM(B.KWMENG) AS KWMENG, (A.UNR01-A.ALLOCATE) as UNR01 , A.UNR03 from MSSMASM A left join MSSMASD B on A.MATNR = B.MATNR left join MSSMASH C on B.VBELN = C.VBELN where C.POFG = ? and A.LGPBE = ? and C.GRPNO = ? and B.ISSUENO = ? group by A.MATNR, A.ARKTX, A.UNR01,A.ALLOCATE, A.UNR03 ) QR1 WHERE ( QR1.UNR01 + QR1.UNR03 - QR1.KWMENG) < 0 ");

            } else {

                //statement = connect.prepareStatement("select A.MATNR, A.ARKTX, SUM(B.KWMENG) AS KWMENG, A.UNR01, A.UNR03 from MSSMASM A left join MSSMASD B on A.MATNR = B.MATNR left join MSSMASH C on B.VBELN = C.VBELN where C.POFG = ? and A.LGPBE = ? and C.GRPNO = ? and B.ISSUENO = ? group by A.MATNR, A.ARKTX, A.UNR01, A.UNR03");
                statement = connect.prepareStatement("select A.MATNR, A.ARKTX, SUM(B.KWMENG) AS KWMENG, (A.UNR01-A.ALLOCATE) as UNR01, A.UNR03 from MSSMASM A left join MSSMASD B on A.MATNR = B.MATNR left join MSSMASH C on B.VBELN = C.VBELN where C.POFG = ? and A.LGPBE = ? and C.GRPNO = ? and B.ISSUENO = ? group by A.MATNR, A.ARKTX, A.UNR01,A.ALLOCATE, A.UNR03");

            }

            statement.setString(1, POFG);
            statement.setString(2, LGPBE);
            statement.setString(3, GRPNO);
            statement.setString(4, ISSUENO);
            ResultSet set = statement.executeQuery();

            while (set.next()) {
                MPDATAD MPDATAD = new MPDATAD();
                MPDATAD.setMATNR(set.getString("MATNR"));
                MPDATAD.setARKTX(set.getString("ARKTX"));
                MPDATAD.setKWMENG(set.getFloat("KWMENG"));
                MPDATAD.setUNR01(set.getFloat("UNR01"));
                MPDATAD.setUNR03(set.getFloat("UNR03"));
                MPDATAD.setDIFF((set.getFloat("UNR01") + set.getFloat("UNR03") - set.getFloat("KWMENG")));
                listMPDATAD.add(MPDATAD);
            }
            set.close();
            statement.close();

        } catch (SQLException err) {
            err.printStackTrace();
        }

        return listMPDATAD;

    }

    public List<MPDATAD> getIssue(String POFG, String LGPBE, String GRPNO, String ISSUENO) {

        List<MPDATAD> listMPDATAD = new ArrayList<MPDATAD>();

        try {

            //old  PreparedStatement statement = connect.prepareStatement("select A.MATNR, A.ARKTX, A.VRKME, SUM(B.KWMENG) AS KWMENG, A.UNR01, A.UNR03, CASE WHEN D.ISSUETOPICK is not null THEN D.ISSUETOPICK ELSE 0 END AS ISSUETOPICK, CASE WHEN E.STATUS IS NOT NULL THEN E.STATUS ELSE 0 END AS STATUS , F.STATUSDESC from MSSMASM A left join MSSMASD B on A.MATNR = B.MATNR left join MSSMASH C on B.VBELN = C.VBELN LEFT JOIN MSSTOPK D ON C.GRPNO = D.GRPNO AND B.ISSUENO = D.ISSUENO AND A.MATNR = D.MATNR left join MSSSTAT E on C.GRPNO = E.GRPNO and B.ISSUENO = E.ISSUENO and A.MATNR = E.MATNR left join MSSSTAD F on CASE WHEN E.STATUS IS NOT NULL THEN E.STATUS ELSE 0 END = F.STATUS where C.POFG = ? and A.LGPBE = ? and C.GRPNO = ? and B.ISSUENO = ? group by A.MATNR, A.ARKTX, A.VRKME, A.UNR01, A.UNR03, D.ISSUETOPICK, E.STATUS, F.STATUSDESC");
            PreparedStatement statement = connect.prepareStatement("SELECT [ISDITNO] as MATNR\n"
                    + "      ,[SAPDESC] as ARKTX\n"
                    + "	  ,[ISDUNIT] as VRKME\n"
                    + "	  ,[ISDRQQTY] as KWMENG\n"
                    + "	  ,[ISDUNR01] as UNR01\n"
                    + "	  ,0.000 as ALLOCATE\n"
                    + "	  ,[ISDUNR03] as UNR03\n"
                    + "	  ,(select top 1 isnull([ISSUETOPICK],0)\n"
                    + "     from [ISMTOPK] where [GRPNO] = '" + GRPNO + "'\n"
                    + "     and [ISSUENO] = '" + ISSUENO + "'\n"
                    + "     and [MATNR] = [ISDITNO]) AS ISSUETOPICK \n"
                    + "	   ,[STSIN] AS [STATUS]\n"
                    + "    ,[STATUSDESC] as STATUSDESC,ONHSAP,ISDDEST,(SELECT TOP 1 QDEDESC FROM [QDEST] WHERE QDECOD = ISDDEST) AS DESTDESC\n"
                    + "FROM(\n"
                    + "SELECT [ISDITNO],[ISDUNIT]\n"
                    + "	  ,(SELECT TOP 1 [ISDUNR01] FROM [ISMMASD] DET2 WHERE DET2.ISDITNO = DET.ISDITNO) AS [ISDUNR01]\n"
                    + "	  ,(SELECT TOP 1 [ISDUNR03] FROM [ISMMASD] DET2 WHERE DET2.ISDITNO = DET.ISDITNO) AS [ISDUNR03]\n"
                    + "   ,(SELECT SUM(QRMQTY) FROM QRMMAS WHERE QRMCODE = DET.ISDITNO  collate SQL_Latin1_General_CP1_CI_AS AND QRMSTS = '2') AS ONHSAP\n"
                    + "	  ,SUM([ISDRQQTY]) as [ISDRQQTY]\n"
                    + "	  ,STS.[STATUS] AS STSIN,DET.ISDDEST\n"
                    + "  FROM [ISMMASD] DET\n"
                    + "  left join [ISMMASH] hh on hh.[ISHORD] = DET.[ISDORD] \n"
                    + "                        and hh.[ISHSEQ] = DET.[ISDSEQ]\n"
                    + "  LEFT JOIN [ISMSTAT] STS ON [GRPNO] = '" + GRPNO + "' AND [MATNR] = [ISDITNO]\n"
                    + "\n"
                    + "  where hh.[ISHPOFG] = ?\n"
                    + "  and DET.[ISDMTCTRL] = ?\n"
                    + "  and DET.[ISDJBNO] = ?\n"
                    + "  and DET.[ISDSEQ] = ?\n"
                    + "  AND DET.[ISDQNO] is not null\n"
                    + "  AND DET.[ISDSTS] >= '5'\n"
                    + "  AND DET.[ISDSINO] = '0'\n"
                    + "  GROUP BY [ISDITNO],[ISDUNIT],STS.[STATUS],[ISDUNR01],[ISDUNR03],DET.ISDDEST\n"
                    + ")TMP\n"
                    + "LEFT JOIN [SAPMAS] ON [SAPMAT]=[ISDITNO] collate SQL_Latin1_General_CP1_CI_AS\n"
                    + "LEFT JOIN [ISMSTAD] ON [STATUS]=[STSIN]\n"
                    + "GROUP BY [ISDITNO],[SAPDESC],[ISDUNR01],[ISDUNR03],[ISDRQQTY],[ISDUNIT],[STSIN],[STATUSDESC],ONHSAP,ISDDEST\n"
                    + "ORDER BY [ISDITNO],[ISDUNIT]");

            statement.setString(1, POFG);
            statement.setString(2, LGPBE);
            statement.setString(3, GRPNO);
            statement.setString(4, ISSUENO);
            ResultSet set = statement.executeQuery();

            while (set.next()) {
                MPDATAD MPDATAD = new MPDATAD();
                MPDATAD.setMATNR(set.getString("MATNR"));
                MPDATAD.setARKTX(set.getString("ARKTX"));
                MPDATAD.setVRKME(set.getString("VRKME"));
                MPDATAD.setKWMENG(set.getFloat("KWMENG"));
                MPDATAD.setISSUETOPICK(set.getFloat("ISSUETOPICK"));
                MPDATAD.setREMAIN(set.getFloat("KWMENG") - set.getFloat("ISSUETOPICK"));
                MPDATAD.setDIFF(((set.getFloat("UNR01") + set.getFloat("UNR03") - set.getFloat("ALLOCATE")) - set.getFloat("KWMENG")));
                MPDATAD.setONHAND(set.getFloat("ONHSAP"));
//                MPDATAD.setONHAND(set.getFloat("UNR01") + set.getFloat("UNR03") - set.getFloat("ALLOCATE"));
                MPDATAD.setPERCENT((set.getFloat("ISSUETOPICK") - set.getFloat("KWMENG")) / set.getFloat("KWMENG"));
                MPDATAD.setSTATUS(set.getString("STATUS"));
                MPDATAD.setSTATUSDESC(set.getString("STATUSDESC"));
                MPDATAD.setTYPE(set.getString("DESTDESC"));
                listMPDATAD.add(MPDATAD);
            }
            set.close();
            statement.close();

        } catch (SQLException err) {
            err.printStackTrace();
        }

        return listMPDATAD;

    }

    public boolean checkDuplicateStatus(String GRPNO, String ISSUENO, String MATNR) {

        boolean status = false;

        try {
            PreparedStatement statement = connect.prepareStatement("select STATUS from ISMSTAT where GRPNO=? and ISSUENO=? and MATNR=?");
            statement.setString(1, GRPNO);
            statement.setString(2, ISSUENO);
            statement.setString(3, MATNR);
            ResultSet set = statement.executeQuery();

            if (set.next()) {
                status = true;
            }
            set.close();
            statement.close();

        } catch (SQLException err) {
            status = true;
            err.printStackTrace();
        }

        return status;

    }

    public boolean checkDuplicateStatusName(String GRPNO, String ISSUENO, String MATNR) {

        boolean status = false;

        try {
            PreparedStatement statement = connect.prepareStatement("select ANAME from MSSSTAN where GRPNO=? and ISSUENO=? and MATNR=?");
            statement.setString(1, GRPNO);
            statement.setString(2, ISSUENO);
            statement.setString(3, MATNR);
            ResultSet set = statement.executeQuery();

            if (set.next()) {
                status = true;
            }
            set.close();
            statement.close();

        } catch (SQLException err) {
            status = true;
            err.printStackTrace();
        }

        return status;

    }

    public int checkStatus(String GRPNO, String ISSUENO, String MATNR) {

        int status = 0;

        try {
            PreparedStatement statement = connect.prepareStatement("select STATUS from ISMSTAT where GRPNO=? and ISSUENO=? and MATNR=?");
            statement.setString(1, GRPNO);
            statement.setString(2, ISSUENO);
            statement.setString(3, MATNR);
            ResultSet set = statement.executeQuery();

            if (set.next()) {
                status = set.getInt("STATUS");
            }
            set.close();
            statement.close();

        } catch (SQLException err) {
            err.printStackTrace();
        }

        return status;

    }

    public void addStatus(String GRPNO, String ISSUENO, String MATNR, int STATUS) {

        try {
            PreparedStatement statement = connect.prepareStatement("insert into ISMSTAT(GRPNO, ISSUENO, MATNR, STATUS) values (?, ?, ?, ?)");
            statement.setString(1, GRPNO);
            statement.setString(2, ISSUENO);
            statement.setString(3, MATNR);
            statement.setInt(4, STATUS);
            statement.executeUpdate();
            statement.close();
        } catch (SQLException err) {
            err.printStackTrace();
        }

    }

    public void addStatusName(String GRPNO, String ISSUENO, String MATNR, String NAME) {

        try {
            PreparedStatement statement = connect.prepareStatement("insert into MSSSTAN(GRPNO, ISSUENO, MATNR, ANAME) values (?, ?, ?, ?)");
            statement.setString(1, GRPNO);
            statement.setString(2, ISSUENO);
            statement.setString(3, MATNR);
            statement.setString(4, NAME);
            statement.executeUpdate();
        } catch (SQLException err) {
            err.printStackTrace();
        }

    }

    public void updateStatus(String GRPNO, String ISSUENO, String MATNR, int STATUS) {

        try {
            PreparedStatement statement = connect.prepareStatement("update ISMSTAT set STATUS=? where GRPNO=? and ISSUENO=? and MATNR=?");
            statement.setInt(1, STATUS);
            statement.setString(2, GRPNO);
            statement.setString(3, ISSUENO);
            statement.setString(4, MATNR);
            statement.executeUpdate();

            //NEW MOD
            // PreparedStatement statement = connect.prepareStatement("UPDATE MSSPACK SET CHKUP=4 WHERE STATUS=1 AND GRPNO=? and ISSUENO=? and MATNR=?");
            // statement.setInt(1, STATUS);
            // statement.setString(2, GRPNO);
            // statement.setString(3, ISSUENO);
            // statement.setString(4, MATNR);
            // statement.executeUpdate();
        } catch (SQLException err) {
            err.printStackTrace();
        }

    }

    public void updateStatusAName(String GRPNO, String ISSUENO, String MATNR, String NAME) {

        try {
            PreparedStatement statement = connect.prepareStatement("update MSSSTAN set ANAME=? where GRPNO=? and ISSUENO=? and MATNR=?");
            statement.setString(1, NAME);
            statement.setString(2, GRPNO);
            statement.setString(3, ISSUENO);
            statement.setString(4, MATNR);
            statement.executeUpdate();
        } catch (SQLException err) {
            err.printStackTrace();
        }

    }

    public void updateStatusRName(String GRPNO, String ISSUENO, String MATNR, String NAME) {

        try {
            PreparedStatement statement = connect.prepareStatement("update MSSSTAN set RNAME=? where GRPNO=? and ISSUENO=? and MATNR=?");
            statement.setString(1, NAME);
            statement.setString(2, GRPNO);
            statement.setString(3, ISSUENO);
            statement.setString(4, MATNR);
            statement.executeUpdate();
        } catch (SQLException err) {
            err.printStackTrace();
        }

    }

    public List<MPDATAD> getListApproved(String POFG, String VIEW, String GRPNO, String ISSUENO) {

        List<MPDATAD> listMPDATAD = new ArrayList<MPDATAD>();
        String where = "";

        try {

            if (VIEW.equals("1")) {
                where = " where round ((ISSUETOPICK - A.KWMENG)/A.KWMENG,2) > 0.05 \n";
            } else if (VIEW.equals("2")) {
                where = " where round ((ISSUETOPICK - A.KWMENG)/A.KWMENG,2) <= 0.05 \n";
            }

            PreparedStatement statement = connect.prepareStatement("select * from (\n"
                    + "SELECT [ISDMTCTRL] as LGPBE\n"
                    + "      ,[ISDITNO] as MATNR\n"
                    + "      ,[SAPDESC] as ARKTX\n"
                    + "	  ,[ISDUNIT] as VRKME\n"
                    + "	  ,[ISDRQQTY] as KWMENG\n"
                    + "	  ,[ISDUNR01] as UNR01\n"
                    + "	  ,0.000 as ALLOCATE\n"
                    + "	  ,[ISDUNR03] as UNR03\n"
                    + "	  ,(select top 1 isnull([ISSUETOPICK],0)\n"
                    + "     from [ISMTOPK] where [GRPNO] = ?\n"
                    + "     and [ISSUENO] = ?\n"
                    + "     and [MATNR] = [ISDITNO]) AS ISSUETOPICK \n"
                    + "	  ,CASE WHEN [ISDSTAD] IS NOT NULL \n"
                    + "		THEN [ISDSTAD] ELSE 0 END AS STATUS \n"
                    + "      ,[STATUSDESC] as STATUSDESC\n"
                    + "FROM(\n"
                    + "SELECT [ISDMTCTRL],[ISDITNO],[ISDUNIT]/*,[ISDSTAD]*/,[STATUS] AS ISDSTAD \n"
                    + "	  ,SUM([ISDUNR01]) as [ISDUNR01]\n"
                    + "	  ,SUM([ISDUNR03]) as [ISDUNR03]\n"
                    + "	  ,SUM([ISDRQQTY]) as [ISDRQQTY]\n"
                    + "  FROM [ISMMASD] DET\n"
                    + "  left join [ISMMASH] hh on hh.[ISHORD] = DET.[ISDORD] \n"
                    + "                        and hh.[ISHSEQ] = DET.[ISDSEQ]\n"
                    + "  LEFT JOIN [ISMSTAT] STAT ON DET.[ISDITNO]= STAT.[MATNR] AND [GRPNO] = ?\n"
                    + "  where hh.[ISHPOFG] = ?\n"
                    + "  and DET.[ISDJBNO] = ?\n"
                    + "  and DET.[ISDSEQ] = ?\n"
                    + "  AND DET.[ISDQNO] is not null\n"
                    //                    + "  AND DET.[ISDSTS] = '3'\n" Aip Edit
                    + "  AND DET.[ISDSTS] >= '5'\n"
                    + "  AND DET.[ISDSTSTMP] is not null\n"
                    + "  AND DET.[ISDSINO] = '0'\n"
                    + "  GROUP BY [ISDMTCTRL],[ISDITNO],[ISDUNIT],[ISDSTAD],[ISDUNR01],[ISDUNR03],[STATUS]\n"
                    + ")TMP\n"
                    + "LEFT JOIN [SAPMAS] ON [SAPMAT]=[ISDITNO] collate SQL_Latin1_General_CP1_CI_AS\n"
                    + "LEFT JOIN [ISMSTAD] ON [STATUS]=[ISDSTAD]\n"
                    + "GROUP BY [ISDMTCTRL],[ISDITNO],[SAPDESC],[ISDUNR01],[ISDUNR03],[ISDRQQTY],[ISDUNIT],[ISDSTAD],[STATUSDESC]\n"
                    + ") A\n"
                    + where
                    + "ORDER BY LGPBE,MATNR,VRKME");

            statement.setString(1, GRPNO);
            statement.setString(2, ISSUENO);
            statement.setString(3, GRPNO);
            statement.setString(4, POFG);
            statement.setString(5, GRPNO);
            statement.setString(6, ISSUENO);
            ResultSet set = statement.executeQuery();

            while (set.next()) {
                MPDATAD MPDATAD = new MPDATAD();
                MPDATAD.setLGPBE(set.getString("LGPBE"));
                MPDATAD.setMATNR(set.getString("MATNR"));
                MPDATAD.setARKTX(set.getString("ARKTX"));
                MPDATAD.setVRKME(set.getString("VRKME"));
                MPDATAD.setKWMENG(set.getFloat("KWMENG"));
                MPDATAD.setISSUETOPICK(set.getFloat("ISSUETOPICK"));
                MPDATAD.setREMAIN(set.getFloat("KWMENG") - set.getFloat("ISSUETOPICK"));
                MPDATAD.setDIFF((set.getFloat("UNR01") + set.getFloat("UNR03") - set.getFloat("KWMENG")));
                MPDATAD.setONHAND(set.getFloat("UNR01") + set.getFloat("UNR03"));
                MPDATAD.setPERCENT((set.getFloat("ISSUETOPICK") - set.getFloat("KWMENG")) / set.getFloat("KWMENG"));
                MPDATAD.setSTATUS(set.getString("STATUS"));
                MPDATAD.setSTATUSDESC(set.getString("STATUSDESC"));
                listMPDATAD.add(MPDATAD);
            }
            set.close();
            statement.close();

        } catch (SQLException err) {
            err.printStackTrace();
        }

        return listMPDATAD;

    }

    public List<MPDATAD> getListReceived(String POFG, String VIEW, String GRPNO, String ISSUENO) {

        List<MPDATAD> listMPDATAD = new ArrayList<MPDATAD>();

        try {

            PreparedStatement statement = connect.prepareStatement("select * from (select A.LGPBE, A.MATNR, A.ARKTX, A.VRKME, CASE WHEN D.ISSUETOPICK is not null THEN D.ISSUETOPICK ELSE 0 END AS ISSUETOPICK, CASE WHEN E.STATUS IS NOT NULL THEN E.STATUS ELSE 0 END AS STATUS , F.STATUSDESC, G.BOX, H.BAG, I.ROLL from MSSMASM A left join MSSMASD B on A.MATNR = B.MATNR left join MSSMASH C on B.VBELN = C.VBELN LEFT JOIN MSSTOPK D ON C.GRPNO = D.GRPNO AND B.ISSUENO = D.ISSUENO AND A.MATNR = D.MATNR left join MSSSTAT E on C.GRPNO = E.GRPNO and B.ISSUENO = E.ISSUENO and A.MATNR = E.MATNR left join MSSSTAD F on CASE WHEN E.STATUS IS NOT NULL THEN E.STATUS ELSE 0 END = F.STATUS LEFT JOIN (SELECT GRPNO, ISSUENO, MATNR, COUNT(NO) AS BOX FROM MSSPACK WHERE TYPE = 'BOX' AND GRPNO = ? AND ISSUENO = ? GROUP BY GRPNO, ISSUENO, MATNR ) G ON G.GRPNO = C.GRPNO AND G.ISSUENO = B.ISSUENO AND G.MATNR = A.MATNR LEFT JOIN (SELECT GRPNO, ISSUENO, MATNR, COUNT(NO) AS BAG FROM MSSPACK WHERE TYPE = 'BAG' AND GRPNO = ? AND ISSUENO = ? GROUP BY GRPNO, ISSUENO, MATNR ) H ON H.GRPNO = C.GRPNO AND H.ISSUENO = B.ISSUENO AND H.MATNR = A.MATNR LEFT JOIN (SELECT GRPNO, ISSUENO, MATNR, COUNT(NO) AS ROLL FROM MSSPACK WHERE TYPE = 'ROLL' AND GRPNO = ? AND ISSUENO = ? GROUP BY GRPNO, ISSUENO, MATNR ) I ON I.GRPNO = C.GRPNO AND I.ISSUENO = B.ISSUENO AND I.MATNR = A.MATNR where C.POFG = ? and C.GRPNO = ? and B.ISSUENO = ? group by A.LGPBE, A.MATNR, A.ARKTX, A.VRKME, A.UNR01, A.UNR03, D.ISSUETOPICK, E.STATUS, F.STATUSDESC, G.BOX, H.BAG, I.ROLL ) A");
            statement.setString(1, GRPNO);
            statement.setString(2, ISSUENO);
            statement.setString(3, GRPNO);
            statement.setString(4, ISSUENO);
            statement.setString(5, GRPNO);
            statement.setString(6, ISSUENO);
            statement.setString(7, POFG);
            statement.setString(8, GRPNO);
            statement.setString(9, ISSUENO);
            ResultSet set = statement.executeQuery();

            while (set.next()) {
                MPDATAD MPDATAD = new MPDATAD();
                MPDATAD.setLGPBE(set.getString("LGPBE"));
                MPDATAD.setMATNR(set.getString("MATNR"));
                MPDATAD.setARKTX(set.getString("ARKTX"));
                MPDATAD.setVRKME(set.getString("VRKME"));
                MPDATAD.setISSUETOPICK(set.getFloat("ISSUETOPICK"));
                MPDATAD.setSTATUS(set.getString("STATUS"));
                MPDATAD.setSTATUSDESC(set.getString("STATUSDESC"));
                MPDATAD.setBOX(set.getInt("BOX"));
                MPDATAD.setBAG(set.getInt("Bag"));
                MPDATAD.setROLL(set.getInt("ROLL"));
                listMPDATAD.add(MPDATAD);
            }
            set.close();
            statement.close();

        } catch (SQLException err) {
            err.printStackTrace();
        }

        return listMPDATAD;

    }

    public List<MPDATAD> getListNotReceive(String GRPNO, String ISSUENO, String customWhere) {

        List<MPDATAD> listMPDATAD = new ArrayList<MPDATAD>();

        try {

            PreparedStatement statement = connect.prepareStatement("SELECT B.LGPBE, A.MATNR, B.ARKTX, B.VRKME, SUM(D.KWMENG) AS KWMENG, VALUE, TYPE, NO FROM MSSPACK A LEFT JOIN MSSMASM B ON A.MATNR = B.MATNR LEFT JOIN MSSMASH C ON A.GRPNO = C.GRPNO LEFT JOIN MSSMASD D ON A.ISSUENO = D.ISSUENO AND A.MATNR = D.MATNR AND C.VBELN = D.VBELN WHERE A.GRPNO = ? AND A.ISSUENO = ? AND A.STATUS = 0 " + customWhere + " GROUP BY B.LGPBE, A.MATNR, B.ARKTX, B.VRKME, VALUE, TYPE, NO ORDER BY B.LGPBE, A.MATNR, A.NO");
            statement.setString(1, GRPNO);
            statement.setString(2, ISSUENO);
            ResultSet set = statement.executeQuery();

            while (set.next()) {
                MPDATAD MPDATAD = new MPDATAD();
                MPDATAD.setLGPBE(set.getString("LGPBE"));
                MPDATAD.setMATNR(set.getString("MATNR"));
                MPDATAD.setARKTX(set.getString("ARKTX"));
                MPDATAD.setVRKME(set.getString("VRKME"));
                MPDATAD.setKWMENG(set.getFloat("KWMENG"));
                MPDATAD.setISSUETOPICK(set.getFloat("VALUE"));
                MPDATAD.setTYPE(set.getString("TYPE"));
                MPDATAD.setNO(set.getInt("NO"));
                listMPDATAD.add(MPDATAD);
            }
            set.close();
            statement.close();

        } catch (SQLException err) {
            err.printStackTrace();
        }

        return listMPDATAD;

    }

    public List<MPDATAD> getListAccepted(String GRPNO, String ISSUENO, String customWhere) {

        List<MPDATAD> listMPDATAD = new ArrayList<MPDATAD>();

        try {

            PreparedStatement statement = connect.prepareStatement("SELECT B.LGPBE, A.MATNR, B.ARKTX, B.VRKME, SUM(D.KWMENG) AS KWMENG, VALUE, TYPE, NO FROM MSSPACK A LEFT JOIN MSSMASM B ON A.MATNR = B.MATNR LEFT JOIN MSSMASH C ON A.GRPNO = C.GRPNO LEFT JOIN MSSMASD D ON A.ISSUENO = D.ISSUENO AND A.MATNR = D.MATNR AND C.VBELN = D.VBELN WHERE A.GRPNO = ? AND A.ISSUENO = ? AND A.STATUS = 1 " + customWhere + " GROUP BY B.LGPBE, A.MATNR, B.ARKTX, B.VRKME, VALUE, TYPE, NO ORDER BY B.LGPBE, A.MATNR, A.NO");
            statement.setString(1, GRPNO);
            statement.setString(2, ISSUENO);
            ResultSet set = statement.executeQuery();

            while (set.next()) {
                MPDATAD MPDATAD = new MPDATAD();
                MPDATAD.setLGPBE(set.getString("LGPBE"));
                MPDATAD.setMATNR(set.getString("MATNR"));
                MPDATAD.setARKTX(set.getString("ARKTX"));
                MPDATAD.setVRKME(set.getString("VRKME"));
                MPDATAD.setKWMENG(set.getFloat("KWMENG"));
                MPDATAD.setISSUETOPICK(set.getFloat("VALUE"));
                MPDATAD.setTYPE(set.getString("TYPE"));
                MPDATAD.setNO(set.getInt("NO"));
                listMPDATAD.add(MPDATAD);
            }
            set.close();
            statement.close();

        } catch (SQLException err) {
            err.printStackTrace();
        }

        return listMPDATAD;

    }

    public void updateReceive(int STATUS, String GRPNO, String ISSUENO, String MATNR, int NO) {

        try {
            PreparedStatement statement = connect.prepareStatement("update MSSPACK set STATUS=? where GRPNO=? and ISSUENO=? and MATNR=? and NO=?");
            statement.setInt(1, STATUS);
            statement.setString(2, GRPNO);
            statement.setString(3, ISSUENO);
            statement.setString(4, MATNR);
            statement.setInt(5, NO);
            statement.executeUpdate();
            statement.close();
        } catch (SQLException err) {
            err.printStackTrace();
        }

    }

    public void updateReceiveID(int STATUS, String GRPNO, String ISSUENO, String MATNR, int NO, String MATID) {

        try {
            PreparedStatement statement = connect.prepareStatement("UPDATE MSSPACK SET STATUS=? WHERE GRPNO=? AND ISSUENO=? AND MATID=? ");
            statement.setInt(1, STATUS);
            statement.setString(2, GRPNO);
            statement.setString(3, ISSUENO);
            statement.setString(4, MATID);
            //statement.setInt(5, NO);
            statement.executeUpdate();
            statement.close();
        } catch (SQLException err) {
            err.printStackTrace();
        }

    }

    public void deleteAllocate(String GRPNO, String ISSUENO, String MATNR) {

        try {
            PreparedStatement statement = connect.prepareStatement(" DELETE MSSALLOC WHERE GRPNO=? AND ISSUENO=? AND MATNR=? ");
            statement.setString(1, GRPNO);
            statement.setString(2, ISSUENO);
            statement.setString(3, MATNR);
            //statement.setInt(4, STATUS);
            //statement.setInt(5, NO);
            statement.executeUpdate();
            statement.close();
        } catch (SQLException err) {
            err.printStackTrace();
        }

    }

    public void InsertAllocate(String GRPNO, String ISSUENO, String MATNR, int STATUS) {

        try {
            PreparedStatement statement = connect.prepareStatement(" INSERT INTO MSSALLOC(MSCUSNO,GRPNO,MATNR,ISSUENO,MSALLOC,MSPERSON,MSDATE,MSTIME) SELECT (SUBSTRING(C.GRPNO,1,3)),C.GRPNO,C.MATNR,C.ISSUENO ,C.SVALUE ,D.LGPBE ,CONVERT(DECIMAL(8,0),REPLACE(CONVERT(VARCHAR(8), GETDATE(), 112), ':', '')) ,CONVERT(DECIMAL(8,0),REPLACE(CONVERT(VARCHAR(8),GETDATE(), 108), ':', '')) FROM MSSMASM D INNER JOIN (SELECT A.GRPNO,A.ISSUENO,A.MATNR,MIN(B.STATUS) AS STATUS,SUM(A.VALUE)  as SVALUE FROM MSSPACK A INNER JOIN MSSSTAT B ON A.GRPNO=B.GRPNO AND A.MATNR=B.MATNR  AND A.ISSUENO=B.ISSUENO WHERE A.GRPNO =? AND A.ISSUENO=? AND  A.MATNR=?  AND B.STATUS=?  GROUP BY A.GRPNO,A.ISSUENO,A.MATNR)C ON D.MATNR=C.MATNR ");
            statement.setString(1, GRPNO);
            statement.setString(2, ISSUENO);
            statement.setString(3, MATNR);
            statement.setInt(4, STATUS);
            //statement.setInt(5, NO);
            statement.executeUpdate();
            statement.close();
            //System.out.println("UPDATE MSSMASM SET ALLOCATE=ALLOCATE+VALUE FROM MSSPACK A INNER JOIN MSSMASM B ON A.MATNR=B.MATNR  WHERE A.GRPNO="+GRPNO+"  AND A.ISSUENO="+ISSUENO+"  AND A.STATUS="+STATUS+"  AND A.MATNR="+MATNR+"");
        } catch (SQLException err) {
            err.printStackTrace();
        }

    }

    public void updateAllocateGrop(String GRPNO, String ISSUENO, String MATNR) {

        try {
            PreparedStatement statement = connect.prepareStatement(" UPDATE MSSALLOC SET MSALLOC=0 WHERE  GRPNO=? AND ISSUENO=? AND MATNR=?  ");
            statement.setString(1, GRPNO);
            statement.setString(2, ISSUENO);
            statement.setString(3, MATNR);
            // statement.setString(4, MATNR);
            //statement.setInt(5, NO);
            statement.executeUpdate();
            statement.close();
            //System.out.println("UPDATE MSSMASM SET ALLOCATE=ALLOCATE+VALUE FROM MSSPACK A INNER JOIN MSSMASM B ON A.MATNR=B.MATNR  WHERE A.GRPNO="+GRPNO+"  AND A.ISSUENO="+ISSUENO+"  AND A.STATUS="+STATUS+"  AND A.MATNR="+MATNR+"");
        } catch (SQLException err) {
            err.printStackTrace();
        }

    }

    public void updateAllocate(String GRPNO, String ISSUENO, String MATNR, int STATUS) {

        try {
            PreparedStatement statement = connect.prepareStatement(" UPDATE MSSMASM SET ALLOCATE=MSALLOC FROM MSSMASM D INNER JOIN  MSSALLOC A ON D.MATNR=A.MATNR WHERE A.GRPNO =? AND A.ISSUENO=? AND A.MATNR=? ");
            statement.setString(1, GRPNO);
            statement.setString(2, ISSUENO);
            statement.setString(3, MATNR);
            //statement.setInt(4, STATUS);
            //statement.setInt(5, NO);
            statement.executeUpdate();
            statement.close();
            //System.out.println("UPDATE MSSMASM SET ALLOCATE=ALLOCATE+VALUE FROM MSSPACK A INNER JOIN MSSMASM B ON A.MATNR=B.MATNR  WHERE A.GRPNO="+GRPNO+"  AND A.ISSUENO="+ISSUENO+"  AND A.STATUS="+STATUS+"  AND A.MATNR="+MATNR+"");
        } catch (SQLException err) {
            err.printStackTrace();
        }

    }

    public int getTotal(String GRPNO, String ISSUENO, int STATUS, String TYPE, String customWhere) {

        int total = 0;

        try {

            PreparedStatement statement = connect.prepareStatement("SELECT COUNT(NO) AS COUNT FROM MSSPACK A LEFT JOIN MSSMASM B ON A.MATNR = B.MATNR WHERE GRPNO = ? AND ISSUENO = ? AND STATUS = ? AND TYPE = ? " + customWhere);
            statement.setString(1, GRPNO);
            statement.setString(2, ISSUENO);
            statement.setInt(3, STATUS);
            statement.setString(4, TYPE);
            ResultSet set = statement.executeQuery();

            while (set.next()) {
                total = set.getInt("COUNT");
            }
            set.close();
            statement.close();

        } catch (SQLException err) {
            err.printStackTrace();
        }

        return total;

    }

    public List<MPDATAS> getListUpdateStatus(String GRPNO, String ISSUENO, String customWhere) {

        List<MPDATAS> listMPDATAS = new ArrayList<MPDATAS>();

        try {

            PreparedStatement statement = connect.prepareStatement("SELECT A.GRPNO, A.ISSUENO, A.MATNR, MIN(A.STATUS) AS STATUS FROM MSSPACK A LEFT JOIN MSSMASM B ON A.MATNR = B.MATNR where A.GRPNO = ? AND A.ISSUENO = ? " + customWhere + " GROUP BY A.GRPNO, A.ISSUENO, A.MATNR");
            statement.setString(1, GRPNO);
            statement.setString(2, ISSUENO);
            ResultSet set = statement.executeQuery();

            while (set.next()) {
                MPDATAS MPDATAS = new MPDATAS();
                MPDATAS.setSWITEM(set.getString("MATNR"));
                MPDATAS.setSWGRPST(set.getString("GRPNO"));
                MPDATAS.setSWRUNSQ(set.getInt("ISSUENO"));
                MPDATAS.setSWCHECK(set.getInt("STATUS"));
                listMPDATAS.add(MPDATAS);
            }
            set.close();
            statement.close();

        } catch (SQLException err) {
            err.printStackTrace();
        }

        return listMPDATAS;

    }

    public void disconnect() {

        //  throws Exception
        try {
            //if(statement != null)statement.close();
            if (connect != null) {
                connect.close();
            }

        } catch (SQLException err) {
            err.printStackTrace();

        }

    }

}
