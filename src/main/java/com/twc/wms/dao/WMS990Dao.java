/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.dao;

import com.twc.wms.database.database;

import java.sql.*;

import java.util.ArrayList;

import java.util.List;

import com.twc.wms.entity.WMS990;
import com.twc.wms.entity.SAPMAS;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import com.twc.wms.database.MSSQLDbConnectionPool;
import com.twc.wms.entity.Qdest;

/**
 *
 * @author nutthawoot.noo
 */
public class WMS990Dao extends database {

    public List<WMS990> getMatcodeList(String dateText, String whd, String plant, String matctrl) {

        List<WMS990> objList = new ArrayList<WMS990>();

        String sql = "DECLARE @WHD NVARCHAR(10) = '" + whd + "'\n"
                + "SELECT DISTINCT [QRMCODE] AS MATCODE, SAPMGRPDESC AS DESCT        \n"
                + "FROM(\n"
                + "SELECT (SELECT TOP 1 [QWHDEPT] FROM [QWHSTR] \n"
                + "        WHERE [QWHCOD] = QRMWHSE AND [QWHMGRP] = LEFT(QRMCODE,2)) AS DEPT\n"
                + "       ,*\n"
                + "  FROM [QRMTRA] JOIN SAPMAS ON QRMCODE=SAPMAT AND QRMPLANT = SAPPLANT AND QRMVAL = SAPVAL\n"
                + "  WHERE [QRMPLANT] = '" + plant + "'\n"
                + "  AND SAPMCTRL = '" + matctrl + "'\n"
                + "  AND FORMAT([QRMTRDT],'yyyy-MM') IN (" + dateText + ")\n"
                + ")TMP\n"
                + "WHERE (QRMWHSE = @WHD OR DEPT = @WHD)\n"
                + "ORDER BY MATCODE";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                WMS990 p = new WMS990();

                p.setNAME(result.getString("MATCODE"));
                p.setSUBNAME(result.getString("DESCT"));

                objList.add(p);
            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return objList;

    }

    public List<WMS990> findDetail(String[] selectCheck, String whd, String detail, String plant) {

        List<WMS990> objList = new ArrayList<WMS990>();

        String ohDateList = "";
        String ohDateListYM = "";
        String ohDateListY = "";
        if (selectCheck != null) {
            for (int i = 0; i < selectCheck.length; i++) {
                if (i == 0) {
                    ohDateList += "'" + selectCheck[i] + "'";
                    ohDateListYM += "'" + selectCheck[i].substring(0, 7) + "'";
                    ohDateListY += "'" + selectCheck[i].substring(0, 4) + "'";
                } else {
                    ohDateList += ",'" + selectCheck[i] + "'";
                    ohDateListYM += ",'" + selectCheck[i].substring(0, 7) + "'";
                    ohDateListY += ",'" + selectCheck[i].substring(0, 4) + "'";
                }
            }
        } else {
            ohDateList = "''";
            ohDateListYM = "''";
            ohDateListY = "''";
        }

        String WHD = "";
        if (whd.equals("All")) {
            WHD = "--";
        }

        String PNT = "";
        if (plant.equals("ALL")) {
            PNT = "--";
        }

        String DETAIL = "";
        String DETAIL2 = "";
        if (detail == null) {
            DETAIL = "";
            DETAIL2 = "";
        } else {
            DETAIL = "--";
            DETAIL2 = "TOP 0";
        }

        String sql = "DECLARE @ALL NVARCHAR(10) = '" + whd + "'\n"
                + "DECLARE @PLANT NVARCHAR(10) = '" + plant + "'\n"
                + "SELECT WH,DEPT,PRODUCT,MATCTRL,[MATCNAME]\n"
                + ",(SELECT ISNULL(COUNT(DISTINCT [QRMCODE]),0)\n"
                + "  FROM [QRMTRA] JOIN SAPMAS ON QRMCODE=SAPMAT AND QRMPLANT = SAPPLANT AND QRMVAL = SAPVAL\n"
                + "  WHERE [QRMWHSE] = WH\n"
                + "  " + PNT + "AND [QRMPLANT] = @PLANT\n"
                + "  AND SAPMCTRL = MATCTRL\n"
                + "  AND FORMAT([QRMTRDT],'yyyy-MM') IN (" + ohDateListYM + ")) AS MVTSKU\n"
                + ",(SELECT ISNULL(SUM(SAPTOTAL * SAPAVG),0)\n"
                + "FROM [SAPONH] \n"
                + "WHERE [SAPWHS] = WH \n"
                + "AND [SAPMTCTRL] = MATCTRL \n"
                + "AND FORMAT([SAPTRDT],'####-##-##') IN (" + ohDateList + ")) AS AMOUNT\n"
                + ",(SELECT ISNULL(COUNT(DISTINCT [QRMID]+[QRMCODE]),0)\n"
                + "  FROM [QRMMAS] JOIN SAPMAS ON QRMCODE=SAPMAT AND QRMPLANT = SAPPLANT AND QRMVAL = SAPVAL\n"
                + "  WHERE [QRMSTS] = '2'\n"
                + "  AND [QRMWHSE] = WH\n"
                + "  " + PNT + "AND [QRMPLANT] = @PLANT\n"
                //                + "  AND SAPPGRP like '%'+PRODUCT+'%'\n"
                + "  AND SAPMCTRL = MATCTRL\n"
                + "  --AND FORMAT([QRMCDT],'yyyy-MM') IN (" + ohDateListYM + ")\n) AS PACKPCS\n"
                + ",(SELECT ISNULL(COUNT([CHKID]),0)\n"
                + "  FROM [TABCHK]\n"
                + "  WHERE [CHKWH] = WH\n"
                + "  AND [CHKMATCTRL] = MATCTRL\n"
                + "  AND FORMAT([CHKONHDATE],'yyyy-MM-dd') IN (" + ohDateList + ")) AS PACKPCS2\n"
                + ",(SELECT ISNULL(SUM(CHKQTY * SAPAVG),0)\n"
                + "FROM(\n"
                + "SELECT [CHKMATCODE], [CHKONHAND], SUM([CHKQTY]) AS CHKQTY\n"
                + "FROM [TABCHK] \n"
                + "WHERE [CHKWH] = WH \n"
                + "AND [CHKMATCTRL] = MATCTRL\n"
                + "AND FORMAT([CHKONHDATE],'yyyy-MM-dd') IN (" + ohDateList + ") \n"
                + "GROUP BY [CHKMATCODE],[CHKONHAND]\n"
                + ")PP\n"
                + "full join SAPONH on SAPMAT = [CHKMATCODE] and FORMAT([SAPTRDT],'####-##-##') IN (" + ohDateList + ")\n"
                + "WHERE [CHKONHAND] != [CHKQTY]) AS AMOUNT2\n"
                + ",OH_SKU,CK_SKU\n"
                + ",CAST(((CAST(CK_SKU AS DECIMAL(18,6))/CAST((CASE WHEN OH_SKU = 0 THEN 1 ELSE OH_SKU END) AS DECIMAL(18,6)))*100) AS DECIMAL(18,3)) AS CK_SKU_PER\n"
                + ",COL_SKU\n"
                + ",CAST(((CAST(COL_SKU AS DECIMAL(18,6))/CAST((CASE WHEN OH_SKU = 0 THEN 1 ELSE OH_SKU END) AS DECIMAL(18,6)))*100) AS DECIMAL(18,3)) AS COL_SKU_PER\n"
                + ",(SELECT COUNT(*)\n"
                + "FROM(\n"
                + "SELECT [CHKMATCODE], [CHKONHAND], SUM([CHKQTY]) AS CHKQTY\n"
                + "FROM [TABCHK] \n"
                + "WHERE [CHKWH] = WH \n"
                + "AND [CHKMATCTRL] = MATCTRL\n"
                + "AND FORMAT([CHKONHDATE],'yyyy-MM-dd') IN (" + ohDateList + ") \n"
                + "GROUP BY [CHKMATCODE],[CHKONHAND]\n"
                + ")PP\n"
                + "WHERE [CHKONHAND] != [CHKQTY]) AS DIFF_SKU\n"
                + ",CAST(((CAST((SELECT COUNT(*)\n"
                + "FROM(\n"
                + "SELECT [CHKMATCODE], [CHKONHAND], SUM([CHKQTY]) AS CHKQTY\n"
                + "FROM [TABCHK] \n"
                + "WHERE [CHKWH] = WH \n"
                + "AND [CHKMATCTRL] = MATCTRL\n"
                + "AND FORMAT([CHKONHDATE],'yyyy-MM-dd') IN (" + ohDateList + ") \n"
                + "GROUP BY [CHKMATCODE],[CHKONHAND]\n"
                + ")PP\n"
                + "WHERE [CHKONHAND] != [CHKQTY]) AS DECIMAL(18,6))/CAST((CASE WHEN OH_SKU = 0 THEN 1 ELSE OH_SKU END) AS DECIMAL(18,6)))*100) AS DECIMAL(18,3)) AS DIFF_SKU_PER\n"
                + "FROM(\n"
                + "SELECT WH,DEPT,PRODUCT,MATCTRL\n"
                + ",(SELECT CASE WHEN COUNT(SAPMAT+SAPVAL) = 0 THEN 0 ELSE COUNT(SAPMAT+SAPVAL) END\n"
                + "  FROM [RMShipment].[dbo].[SAPONH]\n"
                + "  where [SAPWHS]=WH\n"
                + "      and FORMAT([SAPTRDT],'####-##-##') IN (" + ohDateList + ")\n"
                + "      and [SAPMTCTRL]=MATCTRL) AS OH_SKU\n"
                + ",(SELECT count(distinct CHKMATCODE)\n"
                + "FROM [TABCHK] \n"
                + "WHERE [CHKWH] = WH\n"
                + "AND [CHKMATCTRL] = MATCTRL AND FORMAT([CHKONHDATE],'yyyy-MM-dd') IN (" + ohDateList + ")) AS CK_SKU\n"
                + ",(SELECT COUNT(*)\n"
                + "FROM(\n"
                + "SELECT [CHKMATCODE], [CHKONHAND], SUM([CHKQTY]) AS CHKQTY\n"
                + "FROM [TABCHK] \n"
                + "WHERE [CHKWH] = WH \n"
                + "AND [CHKMATCTRL] = MATCTRL\n"
                + "AND FORMAT([CHKONHDATE],'yyyy-MM-dd') IN (" + ohDateList + ") \n"
                + "GROUP BY [CHKMATCODE],[CHKONHAND]\n"
                + ")PP\n"
                + "WHERE [CHKONHAND] = [CHKQTY]) AS COL_SKU\n"
                + "FROM(\n"
                + "SELECT [CHKWH] AS WH\n"
                + ",(SELECT TOP 1 [QWHDEPT] FROM [QWHSTR] WHERE [QWHCOD] = [CHKWH] AND [QWHMGRP] = [CHKMATCODE]) AS DEPT\n"
                + ",[CHKMATCODE] AS PRODUCT\n"
                + ",[CHKMATCTRL] AS MATCTRL\n"
                + "FROM(\n"
                + "SELECT DISTINCT [MSWHSE] AS [CHKWH]\n"
                + ",(SELECT TOP 1 LEFT([CHKMATCODE],2)\n"
                + "  FROM [RMShipment].[dbo].[TABCHK]\n"
                + "  WHERE [CHKWH] = [MSWHSE]\n"
                + "  AND [CHKMATCTRL] = [LGPBE]) AS [CHKMATCODE]\n"
                + ",[LGPBE] as [CHKMATCTRL]\n"
                + "FROM [MSSMATN]\n"
                + "UNION ALL \n"
                + "SELECT DISTINCT [MSWHSE],'',''\n"
                + "FROM [MSSMATN]\n"
                + ")TMP)TMP2)TMP3\n"
                + "LEFT JOIN [MSSMATN] ON [MSSMATN].[MSWHSE] = WH AND [MSSMATN].[LGPBE] = MATCTRL\n"
                + "WHERE (WH = @ALL OR DEPT = @ALL)\n"
                + "--AND (PRODUCT is not null AND DEPT is not null)\n"
                + "ORDER BY WH,DEPT,PRODUCT,MATCTRL";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                WMS990 p = new WMS990();

                p.setWH(result.getString("WH"));
                p.setDEPT(result.getString("DEPT"));
                p.setPRODUCT(result.getString("MATCTRL"));
                p.setNAME(result.getString("MATCNAME"));
                p.setOH_SKU(result.getString("OH_SKU"));
                p.setCK_SKU(result.getString("CK_SKU"));
                p.setCK_SKU_PER(result.getString("CK_SKU_PER"));
                p.setCOL_SKU(result.getString("COL_SKU"));
                p.setCOL_SKU_PER(result.getString("COL_SKU_PER"));
                p.setDIFF_SKU(result.getString("DIFF_SKU"));
                p.setDIFF_SKU_PER(result.getString("DIFF_SKU_PER"));
                p.setAMOUNT(result.getString("AMOUNT").replace("0E-9", "0.000").replace("0E-12", "0.000"));
                p.setPACKPCS(result.getString("PACKPCS"));
                p.setAMOUNT2(result.getString("AMOUNT2"));
                p.setPACKPCS2(result.getString("PACKPCS2"));
                p.setMVTSKU(result.getString("MVTSKU"));

                objList.add(p);
            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return objList;

    }

    public List<WMS990> findDept() {

        List<WMS990> objList = new ArrayList<WMS990>();

        String sql = "SELECT DISTINCT [QWHDEPT]\n"
                + "  FROM [RMShipment].[dbo].[QWHSTR]\n"
                + "  ORDER BY [QWHDEPT]";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                WMS990 p = new WMS990();

                p.setDEPT(result.getString("QWHDEPT"));

                objList.add(p);
            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return objList;

    }

    public List<WMS990> findWH() {

        List<WMS990> objList = new ArrayList<WMS990>();

        String sql = "SELECT DISTINCT [QWHCOD]\n"
                + "  FROM [RMShipment].[dbo].[QWHSTR]\n"
                + "  ORDER BY [QWHCOD]";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                WMS990 p = new WMS990();

                p.setWH(result.getString("QWHCOD"));

                objList.add(p);
            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return objList;

    }

    public List<WMS990> findOHDate() {

        List<WMS990> objList = new ArrayList<WMS990>();

        String sql = "SELECT DISTINCT FORMAT([QTCOHDT],'yyyy-MM-dd') AS [QTCOHDT]\n"
                + "  FROM [RMShipment].[dbo].[QTCKSTK]\n"
                + "  ORDER BY [QTCOHDT] DESC";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                WMS990 p = new WMS990();

                p.setOH_SKU(result.getString("QTCOHDT"));

                objList.add(p);
            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return objList;

    }
}
