/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.dao;

import com.twc.wms.database.database;
import com.twc.wms.entity.ISMMASD;
import com.twc.wms.entity.ISMMASH;
import com.twc.wms.entity.WH;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author nutthawoot.noo
 */
public class ISM200Dao extends database {

    public List<ISMMASD> getDetailPrepare(String styleLotList, String seq) {

        List<ISMMASD> UAList = new ArrayList<ISMMASD>();

        String seqparam = "";

        if (seq != null) {
            if (!seq.equals("")) {
                seqparam = " AND [ISDSEQ] IN (" + seq + ") ";
            }
        }

        String sql = "SELECT isnull([ISDITNO],'') as [ISDITNO]\n"
                + "       ,isnull([SAPDESC],'') as [ISDDESC]\n"
                + "	  ,isnull(format((isnull([ISDUNR01],0)+isnull([ISDUNR03],0)),'#,#0.00'),0) as [ISDSAPONH]\n"
                + "	  ,isnull((SELECT format(sum([QRMQTY]),'#,#0.00')\n"
                + "	    FROM [QRMMAS]\n"
                + "	    where [QRMCODE]=[ISDITNO] collate SQL_Latin1_General_CP1_CI_AS\n"
                + "	    and [QRMSTS]='2'),0) as [ISDWMSONH]\n"
                + "	  ,isnull(format([ISDRQQTY],'#,#0.00'),0) as [ISDRQQTY]\n"
                + "	  ,isnull([ISDMTCTRL],'') as [ISDMTCTRL]\n"
                + "	  ,isnull([ISDUNIT],'') as [ISDUNIT]\n"
                + "FROM(\n"
                + "SELECT [ISDITNO],[ISDMTCTRL],[ISDUNIT]\n"
                + "	  ,(SELECT TOP 1 [ISDUNR01] FROM [ISMMASD] DET2 WHERE DET2.ISDITNO = DET.ISDITNO) AS [ISDUNR01]\n"
                + "	  ,(SELECT TOP 1 [ISDUNR03] FROM [ISMMASD] DET2 WHERE DET2.ISDITNO = DET.ISDITNO) AS [ISDUNR03]\n"
                + "	  ,SUM([ISDRQQTY]) as [ISDRQQTY]\n"
                + "  FROM [ISMMASD] DET\n"
                + "  WHERE [ISDORD] IN (\n"
                + "        SELECT [ISHORD]\n"
                + "        FROM [ISMMASH]\n"
                + "        WHERE isnull([ISHSTYLE],'')\n"
                + "             +isnull([ISHCOLOR],'')\n"
                + "	         +'-'+isnull([ISHLOT],'') IN (" + styleLotList + "))\n"
                + "  AND [ISDSTS] IN ('2','4')\n"
                + "  AND [ISDSINO] = '0' " + seqparam + " \n"
                + "  GROUP BY [ISDITNO],[ISDMTCTRL],[ISDUNIT],[ISDUNR01],[ISDUNR03]\n"
                + ")TMP\n"
                + "LEFT JOIN [SAPMAS] ON [SAPMAT]=[ISDITNO] collate SQL_Latin1_General_CP1_CI_AS\n"
                + "GROUP BY [ISDITNO],[SAPDESC],[ISDUNR01],[ISDUNR03],[ISDRQQTY],[ISDMTCTRL],[ISDUNIT]\n"
                + "ORDER BY [ISDMTCTRL],[ISDITNO],[ISDUNIT]";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                ISMMASD p = new ISMMASD();

                p.setISDITNO(result.getString("ISDITNO"));
                p.setISDDESC(result.getString("ISDDESC"));
                p.setISDSAPONH(result.getString("ISDSAPONH"));
                p.setISDWMSONH(result.getString("ISDWMSONH"));
                p.setISDRQQTY(result.getString("ISDRQQTY"));
                p.setISDMTCTRL(result.getString("ISDMTCTRL"));
                p.setISDUNIT(result.getString("ISDUNIT"));

                UAList.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public List<ISMMASD> getDetailSummary(String jobNo, String wh) {

        List<ISMMASD> UAList = new ArrayList<ISMMASD>();

        //---------------------------------this NEW method---------------------------
        Statement stmtCre = null;

        String sqlCreate = "DECLARE @JBNO NVARCHAR(9) = '" + jobNo + "'\n"
                + "DECLARE @WHS NVARCHAR(9) = '" + wh + "'\n"
                + "DECLARE @SINO NVARCHAR(9) = '0'\n"
                + "\n"
                + "CREATE TABLE #tempISM200\n"
                + "(\n"
                + "	ISMITNO NVARCHAR(18) COLLATE Thai_CI_AS,\n"
                + "	ISMWMSONH DECIMAL(16, 6)\n"
                + ")\n"
                + "\n"
                + "INSERT INTO #tempISM200 \n"
                + "SELECT F1.[ISDITNO],SUM([QRMQTY]) FROM (\n"
                + "	SELECT DISTINCT DETIN.[ISDITNO]\n"
                + "	FROM [ISMMASD] DETIN\n"
                + "	WHERE DETIN.[ISDORD] IN (SELECT DISTINCT [ISHORD] FROM [ISMMASH] WHERE [ISHJBNO] = @JBNO and [ISHWHNO] = @WHS) \n"
                + "	AND ISDJBNO = @JBNO AND [ISDSTS] IN (4,5) AND [ISDSINO] = @SINO\n"
                + ") F1\n"
                + "LEFT JOIN QRMMAS ON [QRMSTS]='2' AND [QRMCODE] = F1.[ISDITNO] collate SQL_Latin1_General_CP1_CI_AS\n"
                + "GROUP BY F1.[ISDITNO]\n";

        try {
//            System.out.println(sqlCreate);
            stmtCre = connect.createStatement();
            stmtCre.executeUpdate(sqlCreate);
        } catch (SQLException ex) {
            Logger.getLogger(WMSPRTDao.class.getName()).log(Level.SEVERE, null, ex);
        }

        String sql = "DECLARE @JBNO NVARCHAR(9) = '" + jobNo + "'\n"
                + "DECLARE @WHS NVARCHAR(9) = '" + wh + "'\n"
                + "DECLARE @SINO NVARCHAR(9) = '0'\n"
                + "\n"
                + "SELECT isnull([ISDORD],'') as [ISDORD]\n"
                + "      ,isnull([ISDLINO],'') as [ISDLINO]\n"
                + "      ,isnull([ISDSINO],'') as [ISDSINO]\n"
                + "      ,isnull([ISDITNO],'') as [ISDITNO]\n"
                + "      ,isnull([SAPDESC],'') as [ISDDESC]\n"
                + "	  ,isnull(format((isnull([ISDUNR01],0)+isnull([ISDUNR03],0)),'#,#0.00'),0) as [ISDSAPONH]\n"
                + "\n"
                + "	  ,(SELECT TOP 1 format(ISNULL(ISMWMSONH,0),'#,#0.00') FROM #tempISM200 WHERE ISMITNO collate SQL_Latin1_General_CP1_CI_AS = ISDITNO) AS [ISDWMSONH]\n"
                + "\n"
                + "	  ,isnull(format([ISDRQQTY],'#,#0.00'),0) as [ISDRQQTY]\n"
                + "	  ,isnull([ISDMTCTRL],'') as [ISDMTCTRL]\n"
                + "	  ,(select top 1 isnull([MATCNAME],'') from [MSSMATN] where [LGPBE]=[ISDMTCTRL]) as MAT_NAME\n"
                + "	  ,isnull([ISDUSER],'') as [ISDUSER]\n"
                + "	  ,(select top 1 isnull(substring([USERS],0,charindex(' ',[USERS])),'') from [MSSUSER] where [USERID]=[ISDUSER]) as USR_NAME\n"
                + "	  ,isnull([ISDUNIT],'') as [ISDUNIT]\n"
                + "	  ,isnull([ISDQNO],'') as [ISDQNO]\n"
                + "FROM(\n"
                + "SELECT [ISDORD],[ISDLINO],[ISDSINO],[ISDITNO],[ISDMTCTRL],[ISDUSER],[ISDUNIT],[ISDQNO]\n"
                + "	  ,(SELECT TOP 1 [ISDUNR01] FROM [ISMMASD] DET2 WHERE DET2.ISDITNO = DET.ISDITNO) AS [ISDUNR01]\n"
                + "	  ,(SELECT TOP 1 [ISDUNR03] FROM [ISMMASD] DET2 WHERE DET2.ISDITNO = DET.ISDITNO) AS [ISDUNR03]\n"
                + "	  ,SUM([ISDRQQTY]) as [ISDRQQTY]\n"
                + "  FROM [ISMMASD] DET\n"
                + "  WHERE [ISDORD] IN (\n"
                + "        SELECT [ISHORD]\n"
                + "        FROM [ISMMASH]\n"
                + "        WHERE [ISHJBNO] = @JBNO and [ISHWHNO] = @WHS) AND ISDJBNO = @JBNO \n"
                + "  AND [ISDSTS] IN (4,5)\n"
                + "  AND [ISDSINO] = @SINO\n"
                + "  GROUP BY [ISDORD],[ISDLINO],[ISDSINO],[ISDITNO],[ISDMTCTRL],[ISDUSER],[ISDUNIT],[ISDQNO],[ISDUNR01],[ISDUNR03]\n"
                + ")TMP\n"
                + "LEFT JOIN [SAPMAS] ON [SAPMAT]=[ISDITNO] collate SQL_Latin1_General_CP1_CI_AS\n"
                + "GROUP BY [ISDORD],[ISDLINO],[ISDSINO],[ISDITNO],[SAPDESC],[ISDUNR01],[ISDUNR03],[ISDRQQTY],[ISDMTCTRL],[ISDUSER],[ISDUNIT],[ISDQNO]\n"
                + "ORDER BY [ISDMTCTRL],[ISDITNO],[ISDUNIT]\n"
                + "\n"
                + "drop table #tempISM200";

        //---------------------------------this NEW method end---------------------------
        //---------------------------------this OLD method---------------------------
//        String sql = "SELECT isnull([ISDORD],'') as [ISDORD]\n"
//                + "      ,isnull([ISDLINO],'') as [ISDLINO]\n"
//                + "      ,isnull([ISDSINO],'') as [ISDSINO]\n"
//                + "      ,isnull([ISDITNO],'') as [ISDITNO]\n"
//                + "      ,isnull([SAPDESC],'') as [ISDDESC]\n"
//                + "	  ,isnull(format((isnull([ISDUNR01],0)+isnull([ISDUNR03],0)),'#,#0.00'),0) as [ISDSAPONH]\n"
//                + "	  ,isnull((SELECT format(sum([QRMQTY]),'#,#0.00')\n"
//                + "	    FROM [QRMMAS]\n"
//                + "	    where [QRMCODE]=[ISDITNO] collate SQL_Latin1_General_CP1_CI_AS\n"
//                + "	    and [QRMSTS]='2'),0) as [ISDWMSONH]\n"
//                + "	  ,isnull(format([ISDRQQTY],'#,#0.00'),0) as [ISDRQQTY]\n"
//                + "	  ,isnull([ISDMTCTRL],'') as [ISDMTCTRL]\n"
//                + "	  ,(select top 1 isnull([MATCNAME],'') from [MSSMATN] where [LGPBE]=[ISDMTCTRL]) as MAT_NAME\n"
//                + "	  ,isnull([ISDUSER],'') as [ISDUSER]\n"
//                + "	  ,(select top 1 isnull(substring([USERS],0,charindex(' ',[USERS])),'') from [MSSUSER] where [USERID]=[ISDUSER]) as USR_NAME\n"
//                + "	  ,isnull([ISDUNIT],'') as [ISDUNIT]\n"
//                + "	  ,isnull([ISDQNO],'') as [ISDQNO]\n"
//                + "FROM(\n"
//                + "SELECT [ISDORD],[ISDLINO],[ISDSINO],[ISDITNO],[ISDMTCTRL],[ISDUSER],[ISDUNIT],[ISDQNO]\n"
//                + "	  ,(SELECT TOP 1 [ISDUNR01] FROM [ISMMASD] DET2 WHERE DET2.ISDITNO = DET.ISDITNO) AS [ISDUNR01]\n"
//                + "	  ,(SELECT TOP 1 [ISDUNR03] FROM [ISMMASD] DET2 WHERE DET2.ISDITNO = DET.ISDITNO) AS [ISDUNR03]\n"
//                + "	  ,SUM([ISDRQQTY]) as [ISDRQQTY]\n"
//                + "  FROM [ISMMASD] DET\n"
//                + "  WHERE [ISDORD] IN (\n"
//                + "        SELECT [ISHORD]\n"
//                + "        FROM [ISMMASH]\n"
//                + "        WHERE [ISHJBNO] = '" + jobNo + "' and [ISHWHNO] = '" + wh + "') AND ISDJBNO = '" + jobNo + "' \n"
//                + "  AND [ISDSTS] IN (4,5)\n"
//                + "  AND [ISDSINO] = '0'\n"
//                + "  GROUP BY [ISDORD],[ISDLINO],[ISDSINO],[ISDITNO],[ISDMTCTRL],[ISDUSER],[ISDUNIT],[ISDQNO],[ISDUNR01],[ISDUNR03]\n"
//                + ")TMP\n"
//                + "LEFT JOIN [SAPMAS] ON [SAPMAT]=[ISDITNO] collate SQL_Latin1_General_CP1_CI_AS\n"
//                + "GROUP BY [ISDORD],[ISDLINO],[ISDSINO],[ISDITNO],[SAPDESC],[ISDUNR01],[ISDUNR03],[ISDRQQTY],[ISDMTCTRL],[ISDUSER],[ISDUNIT],[ISDQNO]\n"
//                + "ORDER BY [ISDMTCTRL],[ISDITNO],[ISDUNIT]";
//        System.out.println(sql);
        //---------------------------------this OLD method end---------------------------
        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                ISMMASD p = new ISMMASD();

                p.setISDORD(result.getString("ISDORD"));
                p.setISDLINO(result.getString("ISDLINO"));
                p.setISDSINO(result.getString("ISDSINO"));
                p.setISDITNO(result.getString("ISDITNO"));
                p.setISDDESC(result.getString("ISDDESC"));
                p.setISDSAPONH(result.getString("ISDSAPONH"));
                p.setISDWMSONH(result.getString("ISDWMSONH"));
                p.setISDRQQTY(result.getString("ISDRQQTY"));
                p.setISDMTCTRL(result.getString("ISDMTCTRL"));
                p.setISDUNIT(result.getString("ISDUNIT"));
                p.setMAT_NAME(result.getString("MAT_NAME"));
                p.setISDUSER(result.getString("ISDUSER"));
                p.setUSR_NAME(result.getString("USR_NAME"));
                p.setISDQNO(result.getString("ISDQNO"));

                UAList.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public List<ISMMASH> getHead(String wh, String pdg) {

        List<ISMMASH> UAList = new ArrayList<ISMMASH>();

        String sql = "SELECT [ISHMVT]\n"
                + ",isnull(right([ISHTRDT],2)\n"
                + "+'/'+left(right([ISHTRDT],4),2)\n"
                + "+'/'+left([ISHTRDT],4),'') as [ISHTRDT]\n"
                + ",isnull([ISHORD],'') as [ISHORD]\n"
                + ",isnull([ISHSEQ],'') as [ISHSEQ]\n"
                + ",isnull(cast([ISHCUNO] as int),'') as [ISHCUNO]\n"
                + ",isnull([ISHCUNM1],'') as [ISHCUNM1]\n"
                + ",isnull([ISHSUBMI],'') as [ISHSUBMI]\n"
                + ",isnull([ISHPOFG],'') as [ISHPOFG]\n"
                + ",isnull(format([ISHJBNO],''),'') as [ISHJBNO]\n"
                + ",isnull([ISHSTYLE]+[ISHCOLOR],'') as [ISHMATCODE]\n"
                + ",isnull([ISHSTYLE],'') as [ISHSTYLE]\n"
                + ",isnull([ISHCOLOR],'') as [ISHCOLOR]\n"
                + ",isnull([ISHWHNO],'') as [ISHWHNO]\n"
                + ",isnull('#'+[ISHLOT],'') as [ISHLOT]\n"
                + ",isnull(format(cast([ISHAMTFG] as decimal(16,6)),'#,#0'),'') as [ISHAMTFG]\n"
                + ",isnull(format([ISHNOR] ,'#,#0'),'0') as [ISHNOR]\n"
                + ",isnull(format((select count(*)\n"
                + "  from(\n"
                + "  SELECT [ISDORD],[ISDLINO],[ISDSINO],[ISDITNO],[ISDMTCTRL],[ISDUSER],[ISDUNIT],[ISDQNO]\n"
                + "  FROM [ISMMASD] DET\n"
                + "  WHERE [ISDORD] IN (\n"
                + "        SELECT [ISHORD]\n"
                + "        FROM [ISMMASH] h1\n"
                + "        WHERE h1.[ISHJBNO] = h0.[ISHJBNO] and h1.[ISHWHNO] = h0.[ISHWHNO])\n"
                + "  AND [ISDSTS] = '3'\n"
                + "  AND [ISDSINO] = '0'\n"
                + "  GROUP BY [ISDORD],[ISDLINO],[ISDSINO],[ISDITNO],[ISDMTCTRL],[ISDUSER],[ISDUNIT],[ISDQNO]\n"
                + ")tmpp),'#,#0'),'') as [ISHCNTTOT]\n"
                + ",isnull(case when [ISHLSTS] = 0 then '-o' else '' end,'') as [LSTSI]\n"
                + ",isnull(case when [ISHHSTS] = 0 then '-o' else '' end,'') as [HSTSI]\n"
                + ",isnull([ISHLSTS],'') as [LSTS]\n"
                + ",isnull([ISHHSTS],'') as [HSTS]\n"
                + ",isnull([ISHLSTS]+' = '+(select top 1 [ISSDESC] from [ISMSTS] where [ISSSTS] = [ISHLSTS]),'') as [ISHLSTS]\n"
                + ",isnull([ISHHSTS]+' = '+(select top 1 [ISSDESC] from [ISMSTS] where [ISSSTS] = [ISHHSTS]),'') as [ISHHSTS]\n"
                + ",isnull(right([ISHRQDT],2)\n"
                + "+'/'+left(right([ISHRQDT],4),2)\n"
                + "+'/'+left([ISHRQDT],4),'') as [ISHRQDT]\n"
                //                + ",isnull([ISMMASH].[ISHDEST] COLLATE SQL_Latin1_General_CP1_CI_AS +' : '+(select top 1 [QDEDESC] from [QDEST] where [QDEST].[QDECOD] = [ISMMASH].[ISHDEST] COLLATE SQL_Latin1_General_CP1_CI_AS),'') as [ISHDEST]\n"
                + ",isnull((select top 1 [QDEDESC] from [QDEST] where [QDEST].[QDECOD] = h0.[ISHDEST] COLLATE SQL_Latin1_General_CP1_CI_AS),'') as [ISHDEST]\n"
                + ",isnull(right([ISHAPDT],2)\n"
                + "+'/'+left(right([ISHAPDT],4),2)\n"
                + "+'/'+left([ISHAPDT],4),'') as [ISHAPDT]\n"
                + ",isnull(right([ISHRCDT],2)\n"
                + "+'/'+left(right([ISHRCDT],4),2)\n"
                + "+'/'+left([ISHRCDT],4),'') as [ISHRCDT]\n"
                + ",isnull([ISHAPUSR]+' : '+(select top 1 SUBSTRING([USERS],0,CHARINDEX(' ', [USERS])) from [MSSUSER] where [USERID] = [ISHAPUSR]),'') as [ISHAPUSR]\n"
                + ",isnull([ISHUSER]+' : '+(select top 1 SUBSTRING([USERS],0,CHARINDEX(' ', [USERS])) from [MSSUSER] where [USERID] = [ISHUSER]),'') as [ISHUSER]\n"
                + "FROM [ISMMASH] h0\n"
                + "WHERE [ISHLSTS] in ('2','3','4','5','6','7','8','9')\n"
                + "AND [ISHWHNO] = '" + wh + "'\n"
                + "AND [ISHPGRP] = '" + pdg + "'\n"
                + "ORDER BY [ISHORD] DESC";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                ISMMASH p = new ISMMASH();

                p.setISHMVT(result.getString("ISHMVT"));
                p.setISHTRDT(result.getString("ISHTRDT"));
                p.setISHORD(result.getString("ISHORD"));
                p.setISHSEQ(result.getString("ISHSEQ"));
                p.setISHCUNO(result.getString("ISHCUNO"));
                p.setISHCUNM1(result.getString("ISHCUNM1"));
                p.setISHSUBMI(result.getString("ISHSUBMI"));
                p.setISHJBNO(result.getString("ISHJBNO"));
                p.setISHMATCODE(result.getString("ISHMATCODE"));
                p.setISHSTYLE(result.getString("ISHSTYLE"));
                p.setISHCOLOR(result.getString("ISHCOLOR"));
                p.setISHLOT(result.getString("ISHLOT"));
                p.setISHAMTFG(result.getString("ISHAMTFG"));
                p.setISHNOR(result.getString("ISHNOR"));
                p.setISHCNTTOT(result.getString("ISHCNTTOT"));
                p.setLSTSI(result.getString("LSTSI"));
                p.setHSTSI(result.getString("HSTSI"));
                p.setLSTS(result.getString("LSTS"));
                p.setHSTS(result.getString("HSTS"));
                p.setISHLSTS(result.getString("ISHLSTS"));
                p.setISHHSTS(result.getString("ISHHSTS"));
                p.setISHRQDT(result.getString("ISHRQDT"));
                p.setISHDEST(result.getString("ISHDEST"));
                p.setISHAPDT(result.getString("ISHAPDT"));
                p.setISHAPUSR(result.getString("ISHAPUSR"));
                p.setISHUSER(result.getString("ISHUSER"));
                p.setISHRCDT(result.getString("ISHRCDT"));
                p.setISHPOFG(result.getString("ISHPOFG"));
                p.setISHWHNO(result.getString("ISHWHNO"));

                UAList.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public ISMMASH getHeadSummary(String jobNo) {

        ISMMASH hd = new ISMMASH();

        String sql = "SELECT top 1 \n"
                + "       isnull(format([ISHP1DT],'####-##-##'),'') as [ISHP1DT]\n"
                + "	 ,isnull(format([ISHA1DT],'####-##-##'),'') as [ISHA1DT]\n"
                + "	 ,isnull(format([ISHT1DT],'####-##-##'),'') as [ISHT1DT]\n"
                + "  FROM [RMShipment].[dbo].[ISMMASH]\n"
                + "  where [ISHJBNO] = '" + jobNo + "'";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                hd.setISHP1DT(result.getString("ISHP1DT"));
                hd.setISHA1DT(result.getString("ISHA1DT"));
                hd.setISHT1DT(result.getString("ISHT1DT"));

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return hd;

    }

    public List<ISMMASH> getHeadByMatcode(String matCode, String styleLotList) {

        List<ISMMASH> UAList = new ArrayList<ISMMASH>();

        String sql = "SELECT [ISHMVT]\n"
                + ",isnull(right([ISHTRDT],2)\n"
                + "+'/'+left(right([ISHTRDT],4),2)\n"
                + "+'/'+left([ISHTRDT],4),'') as [ISHTRDT]\n"
                + ",isnull([ISHORD],'') as [ISHORD]\n"
                + ",isnull(cast([ISHCUNO] as int),'') as [ISHCUNO]\n"
                + ",isnull([ISHCUNM1],'') as [ISHCUNM1]\n"
                + ",isnull([ISHSUBMI],'') as [ISHSUBMI]\n"
                + ",isnull([ISHSTYLE]+[ISHCOLOR],'') as [ISHMATCODE]\n"
                + ",isnull([ISHSTYLE],'') as [ISHSTYLE]\n"
                + ",isnull([ISHCOLOR],'') as [ISHCOLOR]\n"
                + ",isnull('#'+[ISHLOT],'') as [ISHLOT]\n"
                + ",isnull(format(cast([ISHAMTFG] as decimal(16,6)),'#,#0.00'),'') as [ISHAMTFG]\n"
                + "FROM [ISMMASH]\n"
                + "WHERE [ISHORD] IN (SELECT [ISDORD]\n"
                + "FROM [ISMMASD]\n"
                + "WHERE [ISDITNO] = '" + matCode + "')\n"
                + "AND [ISHLSTS] = '4'\n"
                + "AND isnull([ISHSTYLE],'')\n"
                + "   +isnull([ISHCOLOR],'')\n"
                + "   +'-'+isnull([ISHLOT],'') IN (" + styleLotList + ")\n"
                + "ORDER BY [ISHORD] DESC";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                ISMMASH p = new ISMMASH();

                p.setISHMVT(result.getString("ISHMVT"));
                p.setISHTRDT(result.getString("ISHTRDT"));
                p.setISHORD(result.getString("ISHORD"));
                p.setISHCUNO(result.getString("ISHCUNO"));
                p.setISHCUNM1(result.getString("ISHCUNM1"));
                p.setISHSUBMI(result.getString("ISHSUBMI"));
                p.setISHMATCODE(result.getString("ISHMATCODE"));
                p.setISHSTYLE(result.getString("ISHSTYLE"));
                p.setISHCOLOR(result.getString("ISHCOLOR"));
                p.setISHLOT(result.getString("ISHLOT"));
                p.setISHAMTFG(result.getString("ISHAMTFG"));

                UAList.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public List<ISMMASH> getHeadByMatcodeJobNo(String matCode, String jobNo) {

        List<ISMMASH> UAList = new ArrayList<ISMMASH>();

        String sql = "SELECT [ISHMVT]\n"
                + ",isnull(right([ISHTRDT],2)\n"
                + "+'/'+left(right([ISHTRDT],4),2)\n"
                + "+'/'+left([ISHTRDT],4),'') as [ISHTRDT]\n"
                + ",isnull([ISHORD],'') as [ISHORD]\n"
                + ",isnull(cast([ISHCUNO] as int),'') as [ISHCUNO]\n"
                + ",isnull([ISHCUNM1],'') as [ISHCUNM1]\n"
                + ",isnull([ISHSUBMI],'') as [ISHSUBMI]\n"
                + ",isnull([ISHSTYLE]+[ISHCOLOR],'') as [ISHMATCODE]\n"
                + ",isnull([ISHSTYLE],'') as [ISHSTYLE]\n"
                + ",isnull([ISHCOLOR],'') as [ISHCOLOR]\n"
                + ",isnull('#'+[ISHLOT],'') as [ISHLOT]\n"
                + ",isnull(format(cast([ISHAMTFG] as decimal(16,6)),'#,#0.00'),'') as [ISHAMTFG]\n"
                + "FROM [ISMMASH]\n"
                + "WHERE [ISHORD] IN (SELECT [ISDORD]\n"
                + "                  FROM [ISMMASD]\n"
                + "		     WHERE [ISDITNO] = '" + matCode + "')\n"
                + "--AND [ISHLSTS] IN (5)\n"
                + "AND [ISHJBNO] = '" + jobNo + "'\n"
                + "ORDER BY [ISHORD] DESC";

        try {
//            System.out.println("" + sql);

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                ISMMASH p = new ISMMASH();

                p.setISHMVT(result.getString("ISHMVT"));
                p.setISHTRDT(result.getString("ISHTRDT"));
                p.setISHORD(result.getString("ISHORD"));
                p.setISHCUNO(result.getString("ISHCUNO"));
                p.setISHCUNM1(result.getString("ISHCUNM1"));
                p.setISHSUBMI(result.getString("ISHSUBMI"));
                p.setISHMATCODE(result.getString("ISHMATCODE"));
                p.setISHSTYLE(result.getString("ISHSTYLE"));
                p.setISHCOLOR(result.getString("ISHCOLOR"));
                p.setISHLOT(result.getString("ISHLOT"));
                p.setISHAMTFG(result.getString("ISHAMTFG"));

                UAList.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public List<ISMMASH> getCardData(String[] oANDs) {
//    public List<ISMMASH> getCardData(String[] order, String[] seq, String[] oANDs) {

//        String orderList = "";
//        String seqList = "";
        String orderAND = "";
        String ANDseq = "";

//        if (order != null) {
//            for (int i = 0; i < order.length; i++) {
//                if (i == 0) {
//                    orderList += "'" + order[i] + "'";
//                } else {
//                    orderList += ",'" + order[i] + "'";
//                }
//            }
//            orderList = "and [ISHORD] in (" + orderList + ")";
//        }
//        if (seq != null) {
//            for (int i = 0; i < seq.length; i++) {
//                if (i == 0) {
//                    seqList += "'" + seq[i] + "'";
//                } else {
//                    seqList += ",'" + seq[i] + "'";
//                }
//            }
//            seqList = " and [ISHSEQ] in (" + seqList + ")";
//        }
        String os = "";

        if (oANDs != null) {

            os = " AND ( ";

            for (int i = 0; i < oANDs.length; i++) {
                if (i == 0) {
                    os += " ([ISHORD] = '" + oANDs[i].split("_")[0] + "' AND [ISHSEQ] = '" + oANDs[i].split("_")[1] + "' ) ";
//                    orderAND += "'" + oANDs[i].split("_")[0] + "'";
//                    ANDseq += "'" + oANDs[i].split("_")[1] + "'";
                } else {
                    os += " OR ([ISHORD] = '" + oANDs[i].split("_")[0] + "' AND [ISHSEQ] = '" + oANDs[i].split("_")[1] + "' ) ";
//                    orderAND += ",'" + oANDs[i].split("_")[0] + "'";
//                    ANDseq += ",'" + oANDs[i].split("_")[1] + "'";
                }
            }
//            orderAND = " and [ISHORD] in (" + orderAND + ")";
//            ANDseq = " and [ISHSEQ] in (" + ANDseq + ")";

            os += " )";

        }

        List<ISMMASH> UAList = new ArrayList<ISMMASH>();

        String sql = "SELECT tmp.[ISHPOFG] as DEST\n"
                + "      ,tmp.[ISHCUNM1] as DESTNAME\n"
                + "	  ,(select format(count(distinct [ISHORD]),'#,#0')\n"
                + "	    from [ISMMASH] hd \n"
                + "		where hd.[ISHPOFG]=tmp.[ISHPOFG]\n"
                + "		and [ISHLSTS]= 4 and [ISHJBNO] is null " + os + ") as [ORDER]\n"
                + "	  ,(0) as PCS\n"
                + "	  ,(SELECT format(count(distinct [ISDITNO]),'#,#0')\n"
                + "        FROM [ISMMASD]\n"
                + "        where [ISDORD] in ((select [ISHORD]\n"
                + "	    from [ISMMASH] hd \n"
                + "		where hd.[ISHPOFG]=tmp.[ISHPOFG]\n"
                + "		and [ISHLSTS] = 4 and [ISHJBNO] is null " + os + ")) " + os.replace("ISHSEQ", "ISDSEQ").replace("ISHORD", "ISDORD") + ") as SKU\n"
                + "	  ,(select isnull(right(min([ISHRQDT]),2)\n"
                + "       +'/'+left(right(min([ISHRQDT]),4),2)\n"
                + "       +'/'+left(min([ISHRQDT]),4),'')\n"
                + "	    from [ISMMASH] hd \n"
                + "		where hd.[ISHPOFG]=tmp.[ISHPOFG]\n"
                + "		and [ISHLSTS] = 4 and [ISHJBNO] is null " + os + ") as NEARDATE\n"
                + "FROM(\n"
                + "SELECT distinct [ISHPOFG]\n"
                + "      ,[ISHCUNM1]\n"
                + "  FROM [ISMMASH]\n"
                + "  WHERE [ISHLSTS] = '4' and [ISHJBNO] is null " + os + "\n"
                + ")tmp\n"
                + "order by [ISHPOFG]";

//        String sql = "SELECT tmp.[ISHPOFG] as DEST\n"
//                + "      ,tmp.[ISHCUNM1] as DESTNAME\n"
//                + "	  ,(select format(count(distinct [ISHORD]),'#,#0')\n"
//                + "	    from [ISMMASH] hd \n"
//                + "		where hd.[ISHPOFG]=tmp.[ISHPOFG]\n"
//                + "		and [ISHLSTS]= 4 and [ISHJBNO] is null " + orderAND + ANDseq + ") as [ORDER]\n"
//                + "	  ,(0) as PCS\n"
//                + "	  ,(SELECT format(count(distinct [ISDITNO]),'#,#0')\n"
//                + "        FROM [ISMMASD]\n"
//                + "        where [ISDORD] in ((select [ISHORD]\n"
//                + "	    from [ISMMASH] hd \n"
//                + "		where hd.[ISHPOFG]=tmp.[ISHPOFG]\n"
//                + "		and [ISHLSTS] = 4 and [ISHJBNO] is null " + orderAND + ANDseq + ")) " + ANDseq.replace("ISHSEQ", "ISDSEQ") + ") as SKU\n"
//                + "	  ,(select isnull(right(min([ISHRQDT]),2)\n"
//                + "       +'/'+left(right(min([ISHRQDT]),4),2)\n"
//                + "       +'/'+left(min([ISHRQDT]),4),'')\n"
//                + "	    from [ISMMASH] hd \n"
//                + "		where hd.[ISHPOFG]=tmp.[ISHPOFG]\n"
//                + "		and [ISHLSTS] = 4 and [ISHJBNO] is null " + orderAND + ANDseq + ") as NEARDATE\n"
//                + "FROM(\n"
//                + "SELECT distinct [ISHPOFG]\n"
//                + "      ,[ISHCUNM1]\n"
//                + "  FROM [ISMMASH]\n"
//                + "  WHERE [ISHLSTS] = '4' and [ISHJBNO] is null " + orderAND + ANDseq + "\n"
//                + ")tmp\n"
//                + "order by [ISHPOFG]";
//        System.out.println(sql);
        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                ISMMASH p = new ISMMASH();

                p.setDEST(result.getString("DEST"));
                p.setDESTNAME(result.getString("DESTNAME"));
                p.setORDER(result.getString("ORDER"));
                p.setPCS(result.getString("PCS"));
                p.setSKU(result.getString("SKU"));
                p.setNEARDATE(result.getString("NEARDATE"));

                UAList.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public List<ISMMASH> getCardStyleLotData(String dest, String[] oANDs) {

//        String orderList = "";
//        String seqList = "";
        String os = "";

        if (oANDs != null) {

            os = " AND ( ";

            for (int i = 0; i < oANDs.length; i++) {
                if (i == 0) {
                    os += " ([ISHORD] = '" + oANDs[i].split("_")[0] + "' AND [ISHSEQ] = '" + oANDs[i].split("_")[1] + "' ) ";
                } else {
                    os += " OR ([ISHORD] = '" + oANDs[i].split("_")[0] + "' AND [ISHSEQ] = '" + oANDs[i].split("_")[1] + "' ) ";
                }
            }

            os += " )";

        }

//        if (order != null) {
//
//            for (int i = 0; i < order.length; i++) {
//
//                if (i == 0) {
//                    orderList += "'" + order[i] + "'";
//                } else {
//                    orderList += ",'" + order[i] + "'";
//                }
//            }
//            orderList = "and [ISHORD] in (" + orderList + ")";
//        }
//        if (seq != null) {
//            for (int i = 0; i < seq.length; i++) {
//                if (i == 0) {
//                    seqList += "'" + seq[i] + "'";
//                } else {
//                    seqList += ",'" + seq[i] + "'";
//                }
//            }
//            seqList = "and [ISHSEQ] in (" + seqList + ")";
//        }
        List<ISMMASH> UAList = new ArrayList<ISMMASH>();

        String sql = "SELECT DISTINCT STYLE_LOT_ID,STYLE_LOT_TEXT,ISHWHNO\n"
                + "FROM(\n"
                + "SELECT isnull([ISHSTYLE],'')+isnull([ISHCOLOR],'')+'-'+isnull([ISHLOT],'') as STYLE_LOT_ID\n"
                + "      ,isnull([ISHSTYLE],'')+isnull([ISHCOLOR],'')+'#'+isnull([ISHLOT],'') as STYLE_LOT_TEXT\n"
                + "      ,isnull([ISHWHNO],'') as [ISHWHNO]\n"
                + "FROM [ISMMASH]\n"
                + "WHERE [ISHPOFG] = '" + dest + "'\n"
                + "AND [ISHLSTS] = 4 and [ISHJBNO] is null " + os + "\n"
                + ")TMP\n"
                + "ORDER BY STYLE_LOT_ID,STYLE_LOT_TEXT";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                ISMMASH p = new ISMMASH();

                p.setSTYLE_LOT_ID(result.getString("STYLE_LOT_ID"));
                p.setSTYLE_LOT_TEXT(result.getString("STYLE_LOT_TEXT"));
                p.setISHWHNO(result.getString("ISHWHNO"));

                UAList.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public String getJobNo(String wh) {

        String type = "";

        if (wh.equals("WH1")) {
            type = "S1";
        } else if (wh.equals("WH2")) {
            type = "S2";
        } else if (wh.equals("WH3")) {
            type = "S3";
        }

        String p = "";

        String sql = "SELECT top 1 format(CURRENT_TIMESTAMP,'yyyyMM')+format([QNBLAST]+1,'000') as LAST\n"
                + "  FROM [RMShipment].[dbo].[QNBSER]\n"
                + "  where [QNBTYPE]='" + type + "'\n"
                + "  and [QNBGROUP]=format(CURRENT_TIMESTAMP,'MM')\n"
                + "  and [QNBYEAR]=format(CURRENT_TIMESTAMP,'yyyy')\n";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                p = result.getString("LAST");

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return p;

    }

    public boolean updateJobNo(String wh) {

        String type = "";

        if (wh.equals("WH1")) {
            type = "S1";
        } else if (wh.equals("WH2")) {
            type = "S2";
        } else if (wh.equals("WH3")) {
            type = "S3";
        }

        boolean result = false;

        String sql = "UPDATE [RMShipment].[dbo].[QNBSER]\n"
                + "set [QNBLAST]=[QNBLAST]+1\n"
                + "  where [QNBTYPE]='" + type + "'\n"
                + "  and [QNBGROUP]=format(CURRENT_TIMESTAMP,'MM')\n"
                + "  and [QNBYEAR]=format(CURRENT_TIMESTAMP,'yyyy')\n";
        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public List<String> getQno(int cnt, String wh) {

        String type = "";

        if (wh.equals("WH1")) {
            type = "Z1";
        } else if (wh.equals("WH2")) {
            type = "Z2";
        } else if (wh.equals("WH3")) {
            type = "Z3";
        }

        List<String> qnoList = new ArrayList<String>();

        String sql = "SELECT top 1 format(CURRENT_TIMESTAMP,'yyMM')+format([QNBLAST]+1,'00000') as LAST\n"
                + "  FROM [RMShipment].[dbo].[QNBSER]\n"
                + "  where [QNBTYPE]='" + type + "'\n"
                + "  and [QNBGROUP]=format(CURRENT_TIMESTAMP,'MM')\n"
                + "  and [QNBYEAR]=format(CURRENT_TIMESTAMP,'yyyy')\n";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            int qno = 0;

            while (result.next()) {

                qno = result.getInt("LAST");

            }

            for (int i = 0; i < cnt; i++) {
                qnoList.add(Integer.toString(qno + i));
            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return qnoList;

    }

    public boolean updateQno(String cnt, String wh) {

        String type = "";

        if (wh.equals("WH1")) {
            type = "Z1";
        } else if (wh.equals("WH2")) {
            type = "Z2";
        } else if (wh.equals("WH3")) {
            type = "Z3";
        }

        boolean result = false;

        String sql = "UPDATE [RMShipment].[dbo].[QNBSER]\n"
                + "set [QNBLAST]=[QNBLAST]+" + cnt + "\n"
                + "  where [QNBTYPE]='" + type + "'\n"
                + "  and [QNBGROUP]=format(CURRENT_TIMESTAMP,'MM')\n"
                + "  and [QNBYEAR]=format(CURRENT_TIMESTAMP,'yyyy')\n";
        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean setJobNo(String styleLotList, String jobNo, String seq) {

        boolean result = false;

        String seqparamH = "";
        String seqparamD = "";

        if (seq != null) {
            if (!seq.equals("")) {
                seqparamH = " AND ISHSEQ IN (" + seq + ") ";
                seqparamD = " AND ISDSEQ IN (" + seq + ") ";
            }
        }

        String sql = "UPDATE [ISMMASH]\n"
                + "SET [ISHJBNO] = '" + jobNo + "'\n"
                + "WHERE isnull([ISHSTYLE],'')\n"
                + "     +isnull([ISHCOLOR],'')\n"
                + "     +'-'+isnull([ISHLOT],'') IN (" + styleLotList + ") " + seqparamH + " \n"
                + "\n"
                + "UPDATE [ISMMASD]\n"
                + "SET [ISDJBNO] = '" + jobNo + "'\n"
                + "WHERE [ISDORD] IN (\n"
                + "   SELECT [ISHORD]\n"
                + "   FROM [ISMMASH]\n"
                + "   WHERE isnull([ISHSTYLE],'')\n"
                + "        +isnull([ISHCOLOR],'')\n"
                + "        +'-'+isnull([ISHLOT],'') IN (" + styleLotList + ")) " + seqparamD + " ";
        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean updateReceived(String orderNo, String uid, String seq) {

        boolean result = false;

        String sql = "UPDATE [ISMMASH] "
                + "SET [ISHLSTS] = '4' "
                + "   ,[ISHHSTS] = '4' "
                + "   ,[ISHRCUSR] = '" + uid + "' "
                + "   ,[ISHRCDT] = format(current_timestamp,'yyyyMMdd') "
                + "   ,[ISHRCTM] = format(current_timestamp,'HHmmss') "
                + "   ,[ISHEDT] = format(current_timestamp,'yyyyMMdd') "
                + "   ,[ISHETM] = format(current_timestamp,'HHmmss') "
                + " WHERE [ISHORD] = ? AND [ISHSEQ] = ?";
        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ps.setString(1, orderNo);
            ps.setString(2, seq);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean updateReceivedAllDet(String orderNo, String uid, String seq) {

        boolean result = false;

        String sql = "UPDATE [ISMMASD] "
                + "SET [ISDSTS] = '4' "
                + "   ,[ISDSTAD] = '1' "
                + "   ,[ISDRCUSER] = '" + uid + "' "
                + "   ,[ISDRCDT] = format(current_timestamp,'yyyyMMdd') "
                + "   ,[ISDRCTM] = format(current_timestamp,'HHmmss') "
                + "   ,[ISDEDT] = format(current_timestamp,'yyyyMMdd') "
                + "   ,[ISDETM] = format(current_timestamp,'HHmmss') "
                + " WHERE [ISDORD] = ? AND [ISDSEQ] = ? ";
        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ps.setString(1, orderNo);
            ps.setString(2, seq);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean updateCancel(String orderNo) {

        boolean result = false;

        String sql = "UPDATE [ISMMASH] "
                + "SET [ISHLSTS] = '9' "
                + "   ,[ISHHSTS] = '9' "
                + "   ,[ISHEDT] = format(current_timestamp,'yyyyMMdd') "
                + "   ,[ISHETM] = format(current_timestamp,'HHmmss') "
                + " WHERE [ISHORD] = ? ";
        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ps.setString(1, orderNo);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean updateCancelAllDet(String orderNo) {

        boolean result = false;

        String sql = "UPDATE [ISMMASD] "
                + "SET [ISDSTS] = '9' "
                + "   ,[ISDSTAD] = null "
                + "   ,[ISDEDT] = format(current_timestamp,'yyyyMMdd') "
                + "   ,[ISDETM] = format(current_timestamp,'HHmmss') "
                + " WHERE [ISDORD] = ? ";
        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ps.setString(1, orderNo);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean updateQnoList(List<ISMMASD> qnoList) {

        boolean result = false;

        String sql = "";

        for (int i = 0; i < qnoList.size(); i++) {
            sql += "UPDATE [ISMMASD] "
                    + "SET [ISDQNO] = '" + qnoList.get(i).getISDQNO() + "' \n"
                    + "WHERE [ISDORD] = '" + qnoList.get(i).getISDORD() + "' "
                    + "AND [ISDLINO] = '" + qnoList.get(i).getISDLINO() + "' "
                    + "AND [ISDSINO] = '" + qnoList.get(i).getISDSINO() + "' \n";
        }
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean updateHeadDate(String jobNo, ISMMASH hd) {

        boolean result = false;

        String sql = "UPDATE [ISMMASH] "
                + "SET [ISHP1DT] = '" + hd.getISHP1DT() + "' "
                + "   ,[ISHA1DT] = '" + hd.getISHA1DT() + "' "
                + "   ,[ISHT1DT] = '" + hd.getISHT1DT() + "' "
                + " WHERE [ISHJBNO] = '" + jobNo + "' ";
        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean updateSTS4To5Head(String jbno, String sts) {

        boolean result = false;

        String sql = "UPDATE ISMMASH SET \n"
                + "	ISHLSTS = ?,ISHHSTS = ?\n"
                + "	,[ISHEDT] = format(current_timestamp,'yyyyMMdd')\n"
                + "	,[ISHETM] = format(current_timestamp,'HHmmss')\n"
                + "	WHERE ISHJBNO = ?";

        try {

//            PreparedStatement ps = null;
            PreparedStatement ps = connect.prepareStatement(sql);
            ps.setString(1, sts);
            ps.setString(2, sts);
            ps.setString(3, jbno);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean updateSTS4To5Detail(String jbno, String sts) {

        boolean result = false;

        String sql = "UPDATE ISMMASD SET \n"
                + "	ISDSTS = ?\n"
                + "	,[ISDEDT] = format(current_timestamp,'yyyyMMdd')\n"
                + "	,[ISDETM] = format(current_timestamp,'HHmmss')\n"
                + "	WHERE ISDJBNO = ?";

        try {

//            PreparedStatement ps = null;
            PreparedStatement ps = connect.prepareStatement(sql);
            ps.setString(1, sts);
            ps.setString(2, jbno);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean updateBackwardH(String orderNo, String uid, String seq) {

        SimpleDateFormat formatter = new SimpleDateFormat("HHmmss");
        Date date = new Date(System.currentTimeMillis());
        String tm = formatter.format(date);

        boolean result = false;

        String sql = "UPDATE [ISMMASH] "
                + "SET [ISHLSTS] = '0' "
                + "   ,[ISHHSTS] = '0' "
                + "   ,[ISHREMARK] = null "
                + "   ,[ISHRQDT] = null"
                + "   ,[ISHRQTM] = null"
                + "   ,[ISHAPDT] = null"
                + "   ,[ISHAPTM] = null"
                + "   ,[ISHAPUSR] = null"
                + "   ,[ISHRCDT] = null"
                + "   ,[ISHRCTM] = null"
                + "   ,[ISHRCUSR] = null"
                + "   ,[ISHJBNO] = null"
                + "   ,[ISHP1DT] = null"
                + "   ,[ISHA1DT] = null"
                + "   ,[ISHT1DT] = null"
                //                + "   ,[ISHEUSR] = ? "
                + "   ,[ISHEDT] = format(current_timestamp,'yyyyMMdd') "
                + "   ,[ISHETM] = '" + tm + "' "
                + " WHERE [ISHORD] = ? AND [ISHSEQ] = ?";
//        System.out.println("" + sql); 
        try {
            PreparedStatement ps = connect.prepareStatement(sql);

//            ps.setString(1, uid);
            ps.setString(1, orderNo);
            ps.setString(2, seq);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean updateBackWardAllDet(String orderNo, String uid, String seq) {

        SimpleDateFormat formatter = new SimpleDateFormat("HHmmss");
        Date date = new Date(System.currentTimeMillis());
        String tm = formatter.format(date);

        boolean result = false;

        String sql = "UPDATE [ISMMASD] "
                + "SET [ISDSTS] = '0' "
                + "   ,[ISDREMARK] = null "
                + "   ,[ISDRQDT] = null"
                + "   ,[ISDRQTM] = null"
                + "   ,[ISDAPDT] = null"
                + "   ,[ISDAPTM] = null"
                + "   ,[ISDAPUSER] = null"
                + "   ,[ISDRCDT] = null"
                + "   ,[ISDRCTM] = null"
                + "   ,[ISDRCUSER] = null"
                + "   ,[ISDJBNO] = null"
                + "   ,[ISDQNO] = null"
                //                + "   ,[ISDEUSR] = ? "
                + "   ,[ISDEDT] = format(current_timestamp,'yyyyMMdd') "
                + "   ,[ISDETM] = '" + tm + "' "
                + " WHERE [ISDORD] = ? AND [ISDSEQ] = ?";
//        System.out.println("" + sql);
        try {
            PreparedStatement ps = connect.prepareStatement(sql);

//            ps.setString(1, uid);
            ps.setString(1, orderNo);
            ps.setString(2, seq);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public List<ISMMASH> getISMMASHDataTop1(String order, String seq) {

        List<ISMMASH> UAList = new ArrayList<ISMMASH>();

        String sql = "SELECT TOP 1 * FROM [RMShipment].[dbo].[ISMMASH] WHERE ISHORD = ? AND ISHSEQ = ?";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);
            ps.setString(1, order);
            ps.setString(2, seq);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                ISMMASH p = new ISMMASH();

                p.setISHMVT(result.getString("ISHMVT"));
                p.setISHTRDT(result.getString("ISHTRDT"));
                p.setISHORD(result.getString("ISHORD"));
                p.setISHCUNO(result.getString("ISHCUNO"));
                p.setISHCUNM1(result.getString("ISHCUNM1"));
                p.setISHSUBMI(result.getString("ISHSUBMI"));
                p.setISHSTYLE(result.getString("ISHSTYLE"));
                p.setISHCOLOR(result.getString("ISHCOLOR"));
                p.setISHLOT(result.getString("ISHLOT"));
                p.setISHAMTFG(result.getString("ISHAMTFG"));
                p.setISHRPUSR(result.getString("ISHRPUSR"));

                UAList.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public List<ISMMASD> getCloseOD(String appDate) {

        List<ISMMASD> UAList = new ArrayList<ISMMASD>();

        String sql = "SELECT MA.[ISDCONO],MA.[ISDORD],MA.[ISDSEQ],MA.[ISDLINO],MA.[ISDSINO],MA.[ISDITNO],MA.[ISDPLANT]\n"
                + ",ISNULL(MA.[ISDVT],'') AS ISDVT,MA.[ISDSTRG],MA.[ISDUNIT],MA.[ISDPRICE],MA.[ISDUNR01],MA.[ISDUNR03],MA.[ISDMTCTRL]\n"
                + ",MA.[ISDDEST],MA.[ISDRQDT],MA.[ISDRQTM],MA.[ISDAPDT],MA.[ISDAPTM],MA.[ISDAPUSER],MA.[ISDRCDT],MA.[ISDRCTM]\n"
                + ",MA.[ISDRCUSER],MA.[ISDPKQTY],MA.[ISDPKDT],MA.[ISDPKTM],MA.[ISDPKUSER],MA.[ISDTPQTY],MA.[ISDTPDT],MA.[ISDTPTM],MA.[ISDTPUSER]\n"
                + ",MA.[ISDSTS],MA.[ISDJBNO],MA.[ISDQNO],MA.[ISDREMARK],MA.[ISDISUNO],MA.[ISDTEMP],MA.[ISDEDT],MA.[ISDETM],MA.[ISDEUSR]\n"
                + ",MA.[ISDCDT],MA.[ISDCTM],MA.[ISDUSER],MA.[ISDSTAD],MA.[ISDSTSTMP],MA.[ISDREMAIN],ISNULL((SELECT TOP 1 [SAPDESC] FROM [SAPMAS] WHERE [SAPMAT]=MA.[ISDITNO] collate SQL_Latin1_General_CP1_CI_AS),'') AS [ISDDESC] \n"
                + ",ISNULL(FORMAT((ISNULL(MA.[ISDUNR01],0)+ISNULL(MA.[ISDUNR03],0)),'#,#0.00'),0) AS [ISDSAPONH] \n"
                + ",ISNULL((SELECT FORMAT(SUM([QRMQTY]),'#,#0.00') FROM [QRMMAS] WHERE [QRMCODE]=MA.[ISDITNO] collate SQL_Latin1_General_CP1_CI_AS and [QRMSTS]='2'),0) AS [ISDWMSONH] \n"
                + ",ISNULL(FORMAT(MA.[ISDRQQTY],'#,#0.00'),0) AS [ISDRQQTY]\n"
                + ",(SELECT TOP 1 DET.ISDSINO FROM [RMShipment].[dbo].[ISMMASD] DET WHERE DET.ISDORD = MA.ISDORD AND DET.ISDSEQ = MA.ISDSEQ AND DET.ISDLINO = MA.ISDLINO AND DET.ISDREMAIN = MA.ISDREMAIN  AND LEFT(DET.ISDITNO, 2) = LEFT(MA.ISDITNO, 2) AND DET.ISDSTS = MA.ISDSTS /*AND (ISNULL(DET.ISDRQQTY,0)-ISNULL(DET.ISDPKQTY,0)) = (ISNULL(MA.ISDRQQTY,0)-ISNULL(MA.ISDPKQTY,0))*/ ORDER BY DET.ISDSINO DESC) AS CHILD\n"
                + "FROM [RMShipment].[dbo].[ISMMASD] MA\n"
                + "WHERE MA.ISDAPDT = '" + appDate.replaceAll("-", "") + "' AND MA.ISDSTS <= '4'\n"
                + "ORDER BY ISDORD,ISDSEQ,ISDLINO,ISDSINO";

//        System.out.println(sql);
        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                ISMMASD p = new ISMMASD();

                p.setISDCONO(result.getString("ISDCONO"));
                p.setISDORD(result.getString("ISDORD"));
                p.setISDSEQ(result.getString("ISDSEQ"));
                p.setISDLINO(result.getString("ISDLINO"));
                p.setISDSINO(result.getString("ISDSINO"));
                p.setISDITNO(result.getString("ISDITNO"));
                p.setISDPLANT(result.getString("ISDPLANT"));
                p.setISDVT(result.getString("ISDVT"));
                p.setISDSTRG(result.getString("ISDSTRG"));
                p.setISDRQQTY(result.getString("ISDRQQTY"));
                p.setISDUNIT(result.getString("ISDUNIT"));
                p.setISDPRICE(result.getString("ISDPRICE"));
                p.setISDUNR01(result.getString("ISDUNR01"));
                p.setISDUNR03(result.getString("ISDUNR03"));
                p.setISDMTCTRL(result.getString("ISDMTCTRL"));
                p.setISDDEST(result.getString("ISDDEST"));
                p.setISDRQDT(result.getString("ISDRQDT"));
                p.setISDRQTM(result.getString("ISDRQTM"));
                p.setISDAPDT(result.getString("ISDAPDT"));
                p.setISDAPTM(result.getString("ISDAPTM"));
                p.setISDAPUSER(result.getString("ISDAPUSER"));
                p.setISDRCDT(result.getString("ISDRCDT"));
                p.setISDRCTM(result.getString("ISDRCTM"));
                p.setISDRCUSER(result.getString("ISDRCUSER"));
                p.setISDPKQTY(result.getString("ISDPKQTY"));
                p.setISDPKDT(result.getString("ISDPKDT"));
                p.setISDPKTM(result.getString("ISDPKTM"));
                p.setISDPKUSER(result.getString("ISDPKUSER"));
                p.setISDTPQTY(result.getString("ISDTPQTY"));
                p.setISDTPDT(result.getString("ISDTPDT"));
                p.setISDTPTM(result.getString("ISDTPTM"));
                p.setISDTPUSER(result.getString("ISDTPUSER"));
                p.setISDSTS(result.getString("ISDSTS"));
                p.setISDJBNO(result.getString("ISDJBNO"));
                p.setISDQNO(result.getString("ISDQNO"));
                p.setISDREMARK(result.getString("ISDREMARK"));
                p.setISDISUNO(result.getString("ISDISUNO"));
                p.setISDTEMP(result.getString("ISDTEMP"));
                p.setISDEDT(result.getString("ISDEDT"));
                p.setISDETM(result.getString("ISDETM"));
                p.setISDEUSR(result.getString("ISDEUSR"));
                p.setISDCDT(result.getString("ISDCDT"));
                p.setISDCTM(result.getString("ISDCTM"));
                p.setISDUSER(result.getString("ISDUSER"));
                p.setISDSTAD(result.getString("ISDSTAD"));
                p.setISDSTSTMP(result.getString("ISDSTSTMP"));
                p.setISDREMAIN(result.getString("ISDREMAIN"));
                p.setISDDESC(result.getString("ISDDESC").trim());
                p.setISDSAPONH(result.getString("ISDSAPONH"));
                p.setISDWMSONH(result.getString("ISDWMSONH"));
                p.setFAM(result.getString("CHILD"));

                UAList.add(p);

            }

            result.close();
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public boolean updateCloseOrderSts(String order, String seq, String line, String sino) {

        boolean result = false;
        String sqlD = "UPDATE [RMShipment].[dbo].[ISMMASD] SET ISDSTS = 'C' WHERE ISDORD = '" + order + "' AND ISDSEQ = '" + seq + "' AND ISDLINO = '" + line + "' AND ISDSINO = '" + sino + "' ";

        try {
            Statement stmt = connect.createStatement();
            int record = stmt.executeUpdate(sqlD);
            if (record >= 1) {
                result = true;
            } else {
                result = false;
            }

            stmt.close();
            connect.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;

    }

    public boolean updateCloseOrderStsHeadLow(String order, String seq) {

        boolean result = false;
        String sqlH = "UPDATE [RMShipment].[dbo].[ISMMASH] SET ISHLSTS = 'C' WHERE ISHORD = '" + order + "' AND ISHSEQ = '" + seq + "' ";

        try {
            Statement stmt = connect.createStatement();
            int record = stmt.executeUpdate(sqlH);
            if (record >= 1) {
                result = true;
            } else {
                result = false;
            }

            stmt.close();
            connect.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;

    }

    public boolean updateCloseOrderStsHeadHigh(String order, String seq) {

        boolean result = false;
        String sqlH = "UPDATE [RMShipment].[dbo].[ISMMASH] SET ISHHSTS = 'C' WHERE ISHORD = '" + order + "' AND ISHSEQ = '" + seq + "' ";

        try {
            Statement stmt = connect.createStatement();
            int record = stmt.executeUpdate(sqlH);
            if (record >= 1) {
                result = true;
            } else {
                result = false;
            }

            stmt.close();
            connect.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;

    }

    public List<ISMMASD> getMasD_C_Sts(String orderno, String seq) {

        List<ISMMASD> UAList = new ArrayList<ISMMASD>();

        String sql = "SELECT * FROM [RMShipment].[dbo].[ISMMASD] \n"
                + "WHERE ISDORD = '" + orderno + "' AND ISDSEQ = '" + seq + "' AND ISDSTS != 'C' ";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                ISMMASD p = new ISMMASD();

                p.setISDCONO(result.getString("ISDCONO"));
                p.setISDORD(result.getString("ISDORD"));
                p.setISDSEQ(result.getString("ISDSEQ"));
                p.setISDLINO(result.getString("ISDLINO"));
                p.setISDSINO(result.getString("ISDSINO"));
                p.setISDITNO(result.getString("ISDITNO"));
                p.setISDPLANT(result.getString("ISDPLANT"));
                p.setISDVT(result.getString("ISDVT"));
                p.setISDSTRG(result.getString("ISDSTRG"));
                p.setISDRQQTY(result.getString("ISDRQQTY"));
                p.setISDUNIT(result.getString("ISDUNIT"));
                p.setISDPRICE(result.getString("ISDPRICE"));
                p.setISDUNR01(result.getString("ISDUNR01"));
                p.setISDUNR03(result.getString("ISDUNR03"));
                p.setISDMTCTRL(result.getString("ISDMTCTRL"));
                p.setISDDEST(result.getString("ISDDEST"));
                p.setISDRQDT(result.getString("ISDRQDT"));
                p.setISDRQTM(result.getString("ISDRQTM"));
                p.setISDAPDT(result.getString("ISDAPDT"));
                p.setISDAPTM(result.getString("ISDAPTM"));
                p.setISDAPUSER(result.getString("ISDAPUSER"));
                p.setISDRCDT(result.getString("ISDRCDT"));
                p.setISDRCTM(result.getString("ISDRCTM"));
                p.setISDRCUSER(result.getString("ISDRCUSER"));
                p.setISDPKQTY(result.getString("ISDPKQTY"));
                p.setISDPKDT(result.getString("ISDPKDT"));
                p.setISDPKTM(result.getString("ISDPKTM"));
                p.setISDPKUSER(result.getString("ISDPKUSER"));
                p.setISDTPQTY(result.getString("ISDTPQTY"));
                p.setISDTPDT(result.getString("ISDTPDT"));
                p.setISDTPTM(result.getString("ISDTPTM"));
                p.setISDTPUSER(result.getString("ISDTPUSER"));
                p.setISDSTS(result.getString("ISDSTS"));
                p.setISDJBNO(result.getString("ISDJBNO"));
                p.setISDQNO(result.getString("ISDQNO"));
                p.setISDREMARK(result.getString("ISDREMARK"));
                p.setISDISUNO(result.getString("ISDISUNO"));
                p.setISDTEMP(result.getString("ISDTEMP"));
                p.setISDEDT(result.getString("ISDEDT"));
                p.setISDETM(result.getString("ISDETM"));
                p.setISDEUSR(result.getString("ISDEUSR"));
                p.setISDCDT(result.getString("ISDCDT"));
                p.setISDCTM(result.getString("ISDCTM"));
                p.setISDUSER(result.getString("ISDUSER"));
                p.setISDSTAD(result.getString("ISDSTAD"));
                p.setISDSTSTMP(result.getString("ISDSTSTMP"));
                p.setISDREMAIN(result.getString("ISDREMAIN"));
                p.setISDDESC(result.getString("ISDDESC").trim());
                p.setISDSAPONH(result.getString("ISDSAPONH"));
                p.setISDWMSONH(result.getString("ISDWMSONH"));
                p.setFAM(result.getString("CHILD"));

                UAList.add(p);

            }

            result.close();
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

}
