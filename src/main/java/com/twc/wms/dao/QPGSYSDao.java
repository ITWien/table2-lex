/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.dao;

import com.twc.wms.database.database;
import com.twc.wms.entity.QPGTYPE;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author nutthawoot.noo
 */
public class QPGSYSDao extends database {

    public QPGSYSDao() {
    }

    public List<QPGTYPE> selectPGSYS() {
        List<QPGTYPE> WHList = new ArrayList();

        String sql = "SELECT QPGSMNU, QPGSYS + ' - ' + QPGSMNUNAME AS QPGSYS FROM QPGSYS";

        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {
                QPGTYPE p = new QPGTYPE();

                p.setCode(result.getString("QPGSMNU"));

                p.setName(result.getString("QPGSYS"));

                WHList.add(p);
            }

            connect.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return WHList;
    }

    public String findPGSname(String id) {
        String pgtname = "";

        String sql = "SELECT QPGSMNU, QPGSYS + ' - ' + QPGSMNUNAME AS QPGSYS FROM QPGSYS WHERE QPGSMNU = ?";

        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            ps.setString(1, id);

            ResultSet result = ps.executeQuery();

            while (result.next()) {
                pgtname = result.getString("QPGSYS");
            }

            connect.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return pgtname;
    }
}
