/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.entity;

/**
 *
 * @author nutthawoot.noo
 */
public class WH {

    private String uid;
    private String name;
    private String warehouse;

    public WH() {

    }

    public WH(String uid, String name, String warehouse) {

        this.uid = uid;
        this.name = name;
        this.warehouse = warehouse;
    }

    public String getUid() {

        return uid;

    }
    
    public String getName() {

        return name;

    }
    
    public String getWarehouse() {

        return warehouse;

    }

    public void setUid(String uid) {

        this.uid = uid;

    }
    
    public void setName(String name) {

        this.name = name;

    }
    
    public void setWarehouse(String warehouse) {

        this.warehouse = warehouse;

    }

}
