/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.entity;

/**
 *
 * @author 93176
 */
public class ISMSDWD {

    private String ISDCONO;
    private String ISDDWQNO;
    private String ISDORD;
    private String ISDSEQ;
    private String ISDLINO;
    private String ISDITNO;
    private String ISDPLANT;
    private String ISDVT;
    private String ISDSTRG;
    private String ISDRQQTY;
    private String ISDUNIT;
    private String ISDPRICE;
    private String ISDUNR01;
    private String ISDUNR03;
    private String ISDMTCTRL;
    private String ISDDEST;
    private String ISDREMK;
    private String Date;
    private String ISDDESC;
    private String ISDSAPONH;
    private String ISDWMSONH;
    private String ISDSTS;
    private String ISDEUSR;

    private String ISDSINO;
    private String ISDBAT;
    private String ISDMGRP;
    private String ISDSTEXT;
    private String ISDITCAT;
    private String ISDRSRJ;
    private String ISDRSRJT;
    private String ISDDLQTY;
    private String ISDBUNIT;
    private String ISDCON1;
    private String ISDCON2;
    private String ISDNETV;
    private String ISDCUR;
    private String ISDEXC;
    private String ISDPFCT;
    private String ISDACGP;
    private String ISDODT;
    private String ISDUDT;
    private String ISDRQDT;
    private String ISDDLS;
    private String ISDODLS;
    private String ISDCRDT;
    private String ISDCRTM;
    private String ISDSUSER;
    private String ISDEDT;
    private String ISDETM;
    private String ISDCDT;
    private String ISDCTM;
    private String ISDUSER;
    private String STYLE;
    private String COLOR;
    private String LOT;
    private String ISDPURG;

    public ISMSDWD() {
    }

    public ISMSDWD(String ISDCONO, String ISDDWQNO, String ISDORD, String ISDSEQ, String ISDLINO, String ISDITNO, String ISDPLANT, String ISDVT, String ISDSTRG, String ISDRQQTY, String ISDUNIT, String ISDPRICE, String ISDUNR01, String ISDUNR03, String ISDMTCTRL, String ISDDEST, String ISDREMK, String Date, String ISDDESC, String ISDSAPONH, String ISDWMSONH, String ISDSTS, String ISDEUSR, String ISDSINO, String ISDBAT, String ISDMGRP, String ISDSTEXT, String ISDITCAT, String ISDRSRJ, String ISDRSRJT, String ISDDLQTY, String ISDBUNIT, String ISDCON1, String ISDCON2, String ISDNETV, String ISDCUR, String ISDEXC, String ISDPFCT, String ISDACGP, String ISDODT, String ISDUDT, String ISDRQDT, String ISDDLS, String ISDODLS, String ISDCRDT, String ISDCRTM, String ISDSUSER, String ISDEDT, String ISDETM, String ISDCDT, String ISDCTM, String ISDUSER, String STYLE, String COLOR, String LOT, String ISDPURG) {
        this.ISDCONO = ISDCONO;
        this.ISDDWQNO = ISDDWQNO;
        this.ISDORD = ISDORD;
        this.ISDSEQ = ISDSEQ;
        this.ISDLINO = ISDLINO;
        this.ISDITNO = ISDITNO;
        this.ISDPLANT = ISDPLANT;
        this.ISDVT = ISDVT;
        this.ISDSTRG = ISDSTRG;
        this.ISDRQQTY = ISDRQQTY;
        this.ISDUNIT = ISDUNIT;
        this.ISDPRICE = ISDPRICE;
        this.ISDUNR01 = ISDUNR01;
        this.ISDUNR03 = ISDUNR03;
        this.ISDMTCTRL = ISDMTCTRL;
        this.ISDDEST = ISDDEST;
        this.ISDREMK = ISDREMK;
        this.Date = Date;
        this.ISDDESC = ISDDESC;
        this.ISDSAPONH = ISDSAPONH;
        this.ISDWMSONH = ISDWMSONH;
        this.ISDSTS = ISDSTS;
        this.ISDEUSR = ISDEUSR;
        this.ISDSINO = ISDSINO;
        this.ISDBAT = ISDBAT;
        this.ISDMGRP = ISDMGRP;
        this.ISDSTEXT = ISDSTEXT;
        this.ISDITCAT = ISDITCAT;
        this.ISDRSRJ = ISDRSRJ;
        this.ISDRSRJT = ISDRSRJT;
        this.ISDDLQTY = ISDDLQTY;
        this.ISDBUNIT = ISDBUNIT;
        this.ISDCON1 = ISDCON1;
        this.ISDCON2 = ISDCON2;
        this.ISDNETV = ISDNETV;
        this.ISDCUR = ISDCUR;
        this.ISDEXC = ISDEXC;
        this.ISDPFCT = ISDPFCT;
        this.ISDACGP = ISDACGP;
        this.ISDODT = ISDODT;
        this.ISDUDT = ISDUDT;
        this.ISDRQDT = ISDRQDT;
        this.ISDDLS = ISDDLS;
        this.ISDODLS = ISDODLS;
        this.ISDCRDT = ISDCRDT;
        this.ISDCRTM = ISDCRTM;
        this.ISDSUSER = ISDSUSER;
        this.ISDEDT = ISDEDT;
        this.ISDETM = ISDETM;
        this.ISDCDT = ISDCDT;
        this.ISDCTM = ISDCTM;
        this.ISDUSER = ISDUSER;
        this.STYLE = STYLE;
        this.COLOR = COLOR;
        this.LOT = LOT;
        this.ISDPURG = ISDPURG;
    }

    public String getISDCONO() {
        return ISDCONO;
    }

    public void setISDCONO(String ISDCONO) {
        this.ISDCONO = ISDCONO;
    }

    public String getISDDWQNO() {
        return ISDDWQNO;
    }

    public void setISDDWQNO(String ISDDWQNO) {
        this.ISDDWQNO = ISDDWQNO;
    }

    public String getISDORD() {
        return ISDORD;
    }

    public void setISDORD(String ISDORD) {
        this.ISDORD = ISDORD;
    }

    public String getISDSEQ() {
        return ISDSEQ;
    }

    public void setISDSEQ(String ISDSEQ) {
        this.ISDSEQ = ISDSEQ;
    }

    public String getISDLINO() {
        return ISDLINO;
    }

    public void setISDLINO(String ISDLINO) {
        this.ISDLINO = ISDLINO;
    }

    public String getISDITNO() {
        return ISDITNO;
    }

    public void setISDITNO(String ISDITNO) {
        this.ISDITNO = ISDITNO;
    }

    public String getISDPLANT() {
        return ISDPLANT;
    }

    public void setISDPLANT(String ISDPLANT) {
        this.ISDPLANT = ISDPLANT;
    }

    public String getISDVT() {
        return ISDVT;
    }

    public void setISDVT(String ISDVT) {
        this.ISDVT = ISDVT;
    }

    public String getISDSTRG() {
        return ISDSTRG;
    }

    public void setISDSTRG(String ISDSTRG) {
        this.ISDSTRG = ISDSTRG;
    }

    public String getISDRQQTY() {
        return ISDRQQTY;
    }

    public void setISDRQQTY(String ISDRQQTY) {
        this.ISDRQQTY = ISDRQQTY;
    }

    public String getISDUNIT() {
        return ISDUNIT;
    }

    public void setISDUNIT(String ISDUNIT) {
        this.ISDUNIT = ISDUNIT;
    }

    public String getISDPRICE() {
        return ISDPRICE;
    }

    public void setISDPRICE(String ISDPRICE) {
        this.ISDPRICE = ISDPRICE;
    }

    public String getISDUNR01() {
        return ISDUNR01;
    }

    public void setISDUNR01(String ISDUNR01) {
        this.ISDUNR01 = ISDUNR01;
    }

    public String getISDUNR03() {
        return ISDUNR03;
    }

    public void setISDUNR03(String ISDUNR03) {
        this.ISDUNR03 = ISDUNR03;
    }

    public String getISDMTCTRL() {
        return ISDMTCTRL;
    }

    public void setISDMTCTRL(String ISDMTCTRL) {
        this.ISDMTCTRL = ISDMTCTRL;
    }

    public String getISDDEST() {
        return ISDDEST;
    }

    public void setISDDEST(String ISDDEST) {
        this.ISDDEST = ISDDEST;
    }

    public String getISDREMK() {
        return ISDREMK;
    }

    public void setISDREMK(String ISDREMK) {
        this.ISDREMK = ISDREMK;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String Date) {
        this.Date = Date;
    }

    public String getISDDESC() {
        return ISDDESC;
    }

    public void setISDDESC(String ISDDESC) {
        this.ISDDESC = ISDDESC;
    }

    public String getISDSAPONH() {
        return ISDSAPONH;
    }

    public void setISDSAPONH(String ISDSAPONH) {
        this.ISDSAPONH = ISDSAPONH;
    }

    public String getISDWMSONH() {
        return ISDWMSONH;
    }

    public void setISDWMSONH(String ISDWMSONH) {
        this.ISDWMSONH = ISDWMSONH;
    }

    public String getISDSTS() {
        return ISDSTS;
    }

    public void setISDSTS(String ISDSTS) {
        this.ISDSTS = ISDSTS;
    }

    public String getISDEUSR() {
        return ISDEUSR;
    }

    public void setISDEUSR(String ISDEUSR) {
        this.ISDEUSR = ISDEUSR;
    }

    public String getISDSINO() {
        return ISDSINO;
    }

    public void setISDSINO(String ISDSINO) {
        this.ISDSINO = ISDSINO;
    }

    public String getISDBAT() {
        return ISDBAT;
    }

    public void setISDBAT(String ISDBAT) {
        this.ISDBAT = ISDBAT;
    }

    public String getISDMGRP() {
        return ISDMGRP;
    }

    public void setISDMGRP(String ISDMGRP) {
        this.ISDMGRP = ISDMGRP;
    }

    public String getISDSTEXT() {
        return ISDSTEXT;
    }

    public void setISDSTEXT(String ISDSTEXT) {
        this.ISDSTEXT = ISDSTEXT;
    }

    public String getISDITCAT() {
        return ISDITCAT;
    }

    public void setISDITCAT(String ISDITCAT) {
        this.ISDITCAT = ISDITCAT;
    }

    public String getISDRSRJ() {
        return ISDRSRJ;
    }

    public void setISDRSRJ(String ISDRSRJ) {
        this.ISDRSRJ = ISDRSRJ;
    }

    public String getISDRSRJT() {
        return ISDRSRJT;
    }

    public void setISDRSRJT(String ISDRSRJT) {
        this.ISDRSRJT = ISDRSRJT;
    }

    public String getISDDLQTY() {
        return ISDDLQTY;
    }

    public void setISDDLQTY(String ISDDLQTY) {
        this.ISDDLQTY = ISDDLQTY;
    }

    public String getISDBUNIT() {
        return ISDBUNIT;
    }

    public void setISDBUNIT(String ISDBUNIT) {
        this.ISDBUNIT = ISDBUNIT;
    }

    public String getISDCON1() {
        return ISDCON1;
    }

    public void setISDCON1(String ISDCON1) {
        this.ISDCON1 = ISDCON1;
    }

    public String getISDCON2() {
        return ISDCON2;
    }

    public void setISDCON2(String ISDCON2) {
        this.ISDCON2 = ISDCON2;
    }

    public String getISDNETV() {
        return ISDNETV;
    }

    public void setISDNETV(String ISDNETV) {
        this.ISDNETV = ISDNETV;
    }

    public String getISDCUR() {
        return ISDCUR;
    }

    public void setISDCUR(String ISDCUR) {
        this.ISDCUR = ISDCUR;
    }

    public String getISDEXC() {
        return ISDEXC;
    }

    public void setISDEXC(String ISDEXC) {
        this.ISDEXC = ISDEXC;
    }

    public String getISDPFCT() {
        return ISDPFCT;
    }

    public void setISDPFCT(String ISDPFCT) {
        this.ISDPFCT = ISDPFCT;
    }

    public String getISDACGP() {
        return ISDACGP;
    }

    public void setISDACGP(String ISDACGP) {
        this.ISDACGP = ISDACGP;
    }

    public String getISDODT() {
        return ISDODT;
    }

    public void setISDODT(String ISDODT) {
        this.ISDODT = ISDODT;
    }

    public String getISDUDT() {
        return ISDUDT;
    }

    public void setISDUDT(String ISDUDT) {
        this.ISDUDT = ISDUDT;
    }

    public String getISDRQDT() {
        return ISDRQDT;
    }

    public void setISDRQDT(String ISDRQDT) {
        this.ISDRQDT = ISDRQDT;
    }

    public String getISDDLS() {
        return ISDDLS;
    }

    public void setISDDLS(String ISDDLS) {
        this.ISDDLS = ISDDLS;
    }

    public String getISDODLS() {
        return ISDODLS;
    }

    public void setISDODLS(String ISDODLS) {
        this.ISDODLS = ISDODLS;
    }

    public String getISDCRDT() {
        return ISDCRDT;
    }

    public void setISDCRDT(String ISDCRDT) {
        this.ISDCRDT = ISDCRDT;
    }

    public String getISDCRTM() {
        return ISDCRTM;
    }

    public void setISDCRTM(String ISDCRTM) {
        this.ISDCRTM = ISDCRTM;
    }

    public String getISDSUSER() {
        return ISDSUSER;
    }

    public void setISDSUSER(String ISDSUSER) {
        this.ISDSUSER = ISDSUSER;
    }

    public String getISDEDT() {
        return ISDEDT;
    }

    public void setISDEDT(String ISDEDT) {
        this.ISDEDT = ISDEDT;
    }

    public String getISDETM() {
        return ISDETM;
    }

    public void setISDETM(String ISDETM) {
        this.ISDETM = ISDETM;
    }

    public String getISDCDT() {
        return ISDCDT;
    }

    public void setISDCDT(String ISDCDT) {
        this.ISDCDT = ISDCDT;
    }

    public String getISDCTM() {
        return ISDCTM;
    }

    public void setISDCTM(String ISDCTM) {
        this.ISDCTM = ISDCTM;
    }

    public String getISDUSER() {
        return ISDUSER;
    }

    public void setISDUSER(String ISDUSER) {
        this.ISDUSER = ISDUSER;
    }

    public String getSTYLE() {
        return STYLE;
    }

    public void setSTYLE(String STYLE) {
        this.STYLE = STYLE;
    }

    public String getCOLOR() {
        return COLOR;
    }

    public void setCOLOR(String COLOR) {
        this.COLOR = COLOR;
    }

    public String getLOT() {
        return LOT;
    }

    public void setLOT(String LOT) {
        this.LOT = LOT;
    }

    public String getISDPURG() {
        return ISDPURG;
    }

    public void setISDPURG(String ISDPURG) {
        this.ISDPURG = ISDPURG;
    }

}
