/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.entity;

import java.util.List;

/**
 *
 * @author nutthawoot.noo
 */
public class TOT930TB {

    private String WH;
    private List<TOT930> DATALIST;

    public TOT930TB() {

    }

    public String getWH() {
        return WH;
    }

    public void setWH(String WH) {
        this.WH = WH;
    }

    public List<TOT930> getDATALIST() {
        return DATALIST;
    }

    public void setDATALIST(List<TOT930> DATALIST) {
        this.DATALIST = DATALIST;
    }

}
