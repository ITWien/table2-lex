/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.twc.wms.entity;

/**
 *
 * @author 93176
 */
public class ISMBILLD {

    private String ISDCONO;
    private String VBELN;
    private String POSNR;
    private String UEPOS;
    private Double FKIMG;
    private String VRKME;
    private Double UMVKZ;
    private Double UMVKN;
    private String MEINS;
    private Double SMENG;
    private Double FKLMG;
    private Double LMENG;
    private Double NTGEW;
    private Double BRGEW;
    private String GEWEI;
    private String PRSDT;
    private String FBUDA;
    private Double KURSK;
    private Double NETWR;
    private String VBELV;
    private String POSNV;
    private String VGBEL;
    private String VGPOS;
    private String VGTYP;
    private String AUBEL;
    private String AUPOS;
    private String AUREF;
    private String MATNR;
    private String ARKTX;
    private String PMATN;
    private String CHARG;
    private String MATKL;
    private String PSTYV;
    private String POSAR;
    private String PRODH;
    private String VSTEL;
    private String SPART;
    private String POSPA;
    private String WERKS;
    private String ALAND;
    private String TAXM1;
    private String KTGRM;
    private String VKGRP;
    private String VKBUR;
    private String SPARA;
    private String ERNAM;
    private String ERDAT;
    private String ERZET;
    private String BWTAR;
    private String LGORT;
    private Double WAVWR;
    private Double STCUR;
    private String PRCTR;
    private String KVGR1;
    private String KVGR2;
    private String KVGR3;
    private String KVGR4;
    private String KVGR5;
    private Double BONBA;
    private String KOKRS;
    private Double MWSBP;
    private String PO_NUMBER;
    private String PO_DATE;
    private String YOUR_REFERENCE;
    private String PO_ITEM;
    private String CUST_MAT;
    private String CUST_CODE;
    private Double KBETR;
    private String ISDEDT;
    private String ISDETM;
    private String ISDCDT;
    private String ISDCTM;
    private String ISDUSER;
    private String SALESORDER;

    public ISMBILLD() {
    }

    public ISMBILLD(String ISDCONO, String VBELN, String POSNR, String UEPOS, Double FKIMG, String VRKME, Double UMVKZ, Double UMVKN, String MEINS, Double SMENG, Double FKLMG, Double LMENG, Double NTGEW, Double BRGEW, String GEWEI, String PRSDT, String FBUDA, Double KURSK, Double NETWR, String VBELV, String POSNV, String VGBEL, String VGPOS, String VGTYP, String AUBEL, String AUPOS, String AUREF, String MATNR, String ARKTX, String PMATN, String CHARG, String MATKL, String PSTYV, String POSAR, String PRODH, String VSTEL, String SPART, String POSPA, String WERKS, String ALAND, String TAXM1, String KTGRM, String VKGRP, String VKBUR, String SPARA, String ERNAM, String ERDAT, String ERZET, String BWTAR, String LGORT, Double WAVWR, Double STCUR, String PRCTR, String KVGR1, String KVGR2, String KVGR3, String KVGR4, String KVGR5, Double BONBA, String KOKRS, Double MWSBP, String PO_NUMBER, String PO_DATE, String YOUR_REFERENCE, String PO_ITEM, String CUST_MAT, String CUST_CODE, Double KBETR, String ISDEDT, String ISDETM, String ISDCDT, String ISDCTM, String ISDUSER, String SALESORDER) {
        this.ISDCONO = ISDCONO;
        this.VBELN = VBELN;
        this.POSNR = POSNR;
        this.UEPOS = UEPOS;
        this.FKIMG = FKIMG;
        this.VRKME = VRKME;
        this.UMVKZ = UMVKZ;
        this.UMVKN = UMVKN;
        this.MEINS = MEINS;
        this.SMENG = SMENG;
        this.FKLMG = FKLMG;
        this.LMENG = LMENG;
        this.NTGEW = NTGEW;
        this.BRGEW = BRGEW;
        this.GEWEI = GEWEI;
        this.PRSDT = PRSDT;
        this.FBUDA = FBUDA;
        this.KURSK = KURSK;
        this.NETWR = NETWR;
        this.VBELV = VBELV;
        this.POSNV = POSNV;
        this.VGBEL = VGBEL;
        this.VGPOS = VGPOS;
        this.VGTYP = VGTYP;
        this.AUBEL = AUBEL;
        this.AUPOS = AUPOS;
        this.AUREF = AUREF;
        this.MATNR = MATNR;
        this.ARKTX = ARKTX;
        this.PMATN = PMATN;
        this.CHARG = CHARG;
        this.MATKL = MATKL;
        this.PSTYV = PSTYV;
        this.POSAR = POSAR;
        this.PRODH = PRODH;
        this.VSTEL = VSTEL;
        this.SPART = SPART;
        this.POSPA = POSPA;
        this.WERKS = WERKS;
        this.ALAND = ALAND;
        this.TAXM1 = TAXM1;
        this.KTGRM = KTGRM;
        this.VKGRP = VKGRP;
        this.VKBUR = VKBUR;
        this.SPARA = SPARA;
        this.ERNAM = ERNAM;
        this.ERDAT = ERDAT;
        this.ERZET = ERZET;
        this.BWTAR = BWTAR;
        this.LGORT = LGORT;
        this.WAVWR = WAVWR;
        this.STCUR = STCUR;
        this.PRCTR = PRCTR;
        this.KVGR1 = KVGR1;
        this.KVGR2 = KVGR2;
        this.KVGR3 = KVGR3;
        this.KVGR4 = KVGR4;
        this.KVGR5 = KVGR5;
        this.BONBA = BONBA;
        this.KOKRS = KOKRS;
        this.MWSBP = MWSBP;
        this.PO_NUMBER = PO_NUMBER;
        this.PO_DATE = PO_DATE;
        this.YOUR_REFERENCE = YOUR_REFERENCE;
        this.PO_ITEM = PO_ITEM;
        this.CUST_MAT = CUST_MAT;
        this.CUST_CODE = CUST_CODE;
        this.KBETR = KBETR;
        this.ISDEDT = ISDEDT;
        this.ISDETM = ISDETM;
        this.ISDCDT = ISDCDT;
        this.ISDCTM = ISDCTM;
        this.ISDUSER = ISDUSER;
        this.SALESORDER = SALESORDER;
    }

    public String getISDCONO() {
        return ISDCONO;
    }

    public void setISDCONO(String ISDCONO) {
        this.ISDCONO = ISDCONO;
    }

    public String getVBELN() {
        return VBELN;
    }

    public void setVBELN(String VBELN) {
        this.VBELN = VBELN;
    }

    public String getPOSNR() {
        return POSNR;
    }

    public void setPOSNR(String POSNR) {
        this.POSNR = POSNR;
    }

    public String getUEPOS() {
        return UEPOS;
    }

    public void setUEPOS(String UEPOS) {
        this.UEPOS = UEPOS;
    }

    public Double getFKIMG() {
        return FKIMG;
    }

    public void setFKIMG(Double FKIMG) {
        this.FKIMG = FKIMG;
    }

    public String getVRKME() {
        return VRKME;
    }

    public void setVRKME(String VRKME) {
        this.VRKME = VRKME;
    }

    public Double getUMVKZ() {
        return UMVKZ;
    }

    public void setUMVKZ(Double UMVKZ) {
        this.UMVKZ = UMVKZ;
    }

    public Double getUMVKN() {
        return UMVKN;
    }

    public void setUMVKN(Double UMVKN) {
        this.UMVKN = UMVKN;
    }

    public String getMEINS() {
        return MEINS;
    }

    public void setMEINS(String MEINS) {
        this.MEINS = MEINS;
    }

    public Double getSMENG() {
        return SMENG;
    }

    public void setSMENG(Double SMENG) {
        this.SMENG = SMENG;
    }

    public Double getFKLMG() {
        return FKLMG;
    }

    public void setFKLMG(Double FKLMG) {
        this.FKLMG = FKLMG;
    }

    public Double getLMENG() {
        return LMENG;
    }

    public void setLMENG(Double LMENG) {
        this.LMENG = LMENG;
    }

    public Double getNTGEW() {
        return NTGEW;
    }

    public void setNTGEW(Double NTGEW) {
        this.NTGEW = NTGEW;
    }

    public Double getBRGEW() {
        return BRGEW;
    }

    public void setBRGEW(Double BRGEW) {
        this.BRGEW = BRGEW;
    }

    public String getGEWEI() {
        return GEWEI;
    }

    public void setGEWEI(String GEWEI) {
        this.GEWEI = GEWEI;
    }

    public String getPRSDT() {
        return PRSDT;
    }

    public void setPRSDT(String PRSDT) {
        this.PRSDT = PRSDT;
    }

    public String getFBUDA() {
        return FBUDA;
    }

    public void setFBUDA(String FBUDA) {
        this.FBUDA = FBUDA;
    }

    public Double getKURSK() {
        return KURSK;
    }

    public void setKURSK(Double KURSK) {
        this.KURSK = KURSK;
    }

    public Double getNETWR() {
        return NETWR;
    }

    public void setNETWR(Double NETWR) {
        this.NETWR = NETWR;
    }

    public String getVBELV() {
        return VBELV;
    }

    public void setVBELV(String VBELV) {
        this.VBELV = VBELV;
    }

    public String getPOSNV() {
        return POSNV;
    }

    public void setPOSNV(String POSNV) {
        this.POSNV = POSNV;
    }

    public String getVGBEL() {
        return VGBEL;
    }

    public void setVGBEL(String VGBEL) {
        this.VGBEL = VGBEL;
    }

    public String getVGPOS() {
        return VGPOS;
    }

    public void setVGPOS(String VGPOS) {
        this.VGPOS = VGPOS;
    }

    public String getVGTYP() {
        return VGTYP;
    }

    public void setVGTYP(String VGTYP) {
        this.VGTYP = VGTYP;
    }

    public String getAUBEL() {
        return AUBEL;
    }

    public void setAUBEL(String AUBEL) {
        this.AUBEL = AUBEL;
    }

    public String getAUPOS() {
        return AUPOS;
    }

    public void setAUPOS(String AUPOS) {
        this.AUPOS = AUPOS;
    }

    public String getAUREF() {
        return AUREF;
    }

    public void setAUREF(String AUREF) {
        this.AUREF = AUREF;
    }

    public String getMATNR() {
        return MATNR;
    }

    public void setMATNR(String MATNR) {
        this.MATNR = MATNR;
    }

    public String getARKTX() {
        return ARKTX;
    }

    public void setARKTX(String ARKTX) {
        this.ARKTX = ARKTX;
    }

    public String getPMATN() {
        return PMATN;
    }

    public void setPMATN(String PMATN) {
        this.PMATN = PMATN;
    }

    public String getCHARG() {
        return CHARG;
    }

    public void setCHARG(String CHARG) {
        this.CHARG = CHARG;
    }

    public String getMATKL() {
        return MATKL;
    }

    public void setMATKL(String MATKL) {
        this.MATKL = MATKL;
    }

    public String getPSTYV() {
        return PSTYV;
    }

    public void setPSTYV(String PSTYV) {
        this.PSTYV = PSTYV;
    }

    public String getPOSAR() {
        return POSAR;
    }

    public void setPOSAR(String POSAR) {
        this.POSAR = POSAR;
    }

    public String getPRODH() {
        return PRODH;
    }

    public void setPRODH(String PRODH) {
        this.PRODH = PRODH;
    }

    public String getVSTEL() {
        return VSTEL;
    }

    public void setVSTEL(String VSTEL) {
        this.VSTEL = VSTEL;
    }

    public String getSPART() {
        return SPART;
    }

    public void setSPART(String SPART) {
        this.SPART = SPART;
    }

    public String getPOSPA() {
        return POSPA;
    }

    public void setPOSPA(String POSPA) {
        this.POSPA = POSPA;
    }

    public String getWERKS() {
        return WERKS;
    }

    public void setWERKS(String WERKS) {
        this.WERKS = WERKS;
    }

    public String getALAND() {
        return ALAND;
    }

    public void setALAND(String ALAND) {
        this.ALAND = ALAND;
    }

    public String getTAXM1() {
        return TAXM1;
    }

    public void setTAXM1(String TAXM1) {
        this.TAXM1 = TAXM1;
    }

    public String getKTGRM() {
        return KTGRM;
    }

    public void setKTGRM(String KTGRM) {
        this.KTGRM = KTGRM;
    }

    public String getVKGRP() {
        return VKGRP;
    }

    public void setVKGRP(String VKGRP) {
        this.VKGRP = VKGRP;
    }

    public String getVKBUR() {
        return VKBUR;
    }

    public void setVKBUR(String VKBUR) {
        this.VKBUR = VKBUR;
    }

    public String getSPARA() {
        return SPARA;
    }

    public void setSPARA(String SPARA) {
        this.SPARA = SPARA;
    }

    public String getERNAM() {
        return ERNAM;
    }

    public void setERNAM(String ERNAM) {
        this.ERNAM = ERNAM;
    }

    public String getERDAT() {
        return ERDAT;
    }

    public void setERDAT(String ERDAT) {
        this.ERDAT = ERDAT;
    }

    public String getERZET() {
        return ERZET;
    }

    public void setERZET(String ERZET) {
        this.ERZET = ERZET;
    }

    public String getBWTAR() {
        return BWTAR;
    }

    public void setBWTAR(String BWTAR) {
        this.BWTAR = BWTAR;
    }

    public String getLGORT() {
        return LGORT;
    }

    public void setLGORT(String LGORT) {
        this.LGORT = LGORT;
    }

    public Double getWAVWR() {
        return WAVWR;
    }

    public void setWAVWR(Double WAVWR) {
        this.WAVWR = WAVWR;
    }

    public Double getSTCUR() {
        return STCUR;
    }

    public void setSTCUR(Double STCUR) {
        this.STCUR = STCUR;
    }

    public String getPRCTR() {
        return PRCTR;
    }

    public void setPRCTR(String PRCTR) {
        this.PRCTR = PRCTR;
    }

    public String getKVGR1() {
        return KVGR1;
    }

    public void setKVGR1(String KVGR1) {
        this.KVGR1 = KVGR1;
    }

    public String getKVGR2() {
        return KVGR2;
    }

    public void setKVGR2(String KVGR2) {
        this.KVGR2 = KVGR2;
    }

    public String getKVGR3() {
        return KVGR3;
    }

    public void setKVGR3(String KVGR3) {
        this.KVGR3 = KVGR3;
    }

    public String getKVGR4() {
        return KVGR4;
    }

    public void setKVGR4(String KVGR4) {
        this.KVGR4 = KVGR4;
    }

    public String getKVGR5() {
        return KVGR5;
    }

    public void setKVGR5(String KVGR5) {
        this.KVGR5 = KVGR5;
    }

    public Double getBONBA() {
        return BONBA;
    }

    public void setBONBA(Double BONBA) {
        this.BONBA = BONBA;
    }

    public String getKOKRS() {
        return KOKRS;
    }

    public void setKOKRS(String KOKRS) {
        this.KOKRS = KOKRS;
    }

    public Double getMWSBP() {
        return MWSBP;
    }

    public void setMWSBP(Double MWSBP) {
        this.MWSBP = MWSBP;
    }

    public String getPO_NUMBER() {
        return PO_NUMBER;
    }

    public void setPO_NUMBER(String PO_NUMBER) {
        this.PO_NUMBER = PO_NUMBER;
    }

    public String getPO_DATE() {
        return PO_DATE;
    }

    public void setPO_DATE(String PO_DATE) {
        this.PO_DATE = PO_DATE;
    }

    public String getYOUR_REFERENCE() {
        return YOUR_REFERENCE;
    }

    public void setYOUR_REFERENCE(String YOUR_REFERENCE) {
        this.YOUR_REFERENCE = YOUR_REFERENCE;
    }

    public String getPO_ITEM() {
        return PO_ITEM;
    }

    public void setPO_ITEM(String PO_ITEM) {
        this.PO_ITEM = PO_ITEM;
    }

    public String getCUST_MAT() {
        return CUST_MAT;
    }

    public void setCUST_MAT(String CUST_MAT) {
        this.CUST_MAT = CUST_MAT;
    }

    public String getCUST_CODE() {
        return CUST_CODE;
    }

    public void setCUST_CODE(String CUST_CODE) {
        this.CUST_CODE = CUST_CODE;
    }

    public Double getKBETR() {
        return KBETR;
    }

    public void setKBETR(Double KBETR) {
        this.KBETR = KBETR;
    }

    public String getISDEDT() {
        return ISDEDT;
    }

    public void setISDEDT(String ISDEDT) {
        this.ISDEDT = ISDEDT;
    }

    public String getISDETM() {
        return ISDETM;
    }

    public void setISDETM(String ISDETM) {
        this.ISDETM = ISDETM;
    }

    public String getISDCDT() {
        return ISDCDT;
    }

    public void setISDCDT(String ISDCDT) {
        this.ISDCDT = ISDCDT;
    }

    public String getISDCTM() {
        return ISDCTM;
    }

    public void setISDCTM(String ISDCTM) {
        this.ISDCTM = ISDCTM;
    }

    public String getISDUSER() {
        return ISDUSER;
    }

    public void setISDUSER(String ISDUSER) {
        this.ISDUSER = ISDUSER;
    }

    public String getSALESORDER() {
        return SALESORDER;
    }

    public void setSALESORDER(String SALESORDER) {
        this.SALESORDER = SALESORDER;
    }

}
