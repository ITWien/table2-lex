/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.entity;

import java.util.ArrayList;
import com.twc.wms.entity.QIDETAIL;
import java.util.List;

/**
 *
 * @author nutthawoot.noo
 */
public class WMS950 {

    private String wh;
    private String whN;
    private String plant;
    private String plantN;
    private String matCt;
    private String matCtN;
    private String rm;
    private String rmN;
    private String matCo;
    private String matCoN;
    private String desc;
    private String descN;
    private String sts123;
    private String sts456;
    private String sts1;
    private String sts2;
    private String sts3;
    private String sts4;
    private String sts5;
    private String sts6;
    private String sts7;
    private String stsO;
    private String stsP;
    private String isSub;
    private String DT_RowId;
    private List<WMS950> detList = new ArrayList<WMS950>();

    public WMS950() {

    }

    public String getDT_RowId() {
        return DT_RowId;
    }

    public void setDT_RowId(String DT_RowId) {
        this.DT_RowId = DT_RowId;
    }

    public String getIsSub() {
        return isSub;
    }

    public void setIsSub(String isSub) {
        this.isSub = isSub;
    }

    public String getPlantN() {
        return plantN;
    }

    public void setPlantN(String plantN) {
        this.plantN = plantN;
    }

    public String getMatCtN() {
        return matCtN;
    }

    public void setMatCtN(String matCtN) {
        this.matCtN = matCtN;
    }

    public String getRmN() {
        return rmN;
    }

    public void setRmN(String rmN) {
        this.rmN = rmN;
    }

    public String getMatCoN() {
        return matCoN;
    }

    public void setMatCoN(String matCoN) {
        this.matCoN = matCoN;
    }

    public String getDescN() {
        return descN;
    }

    public void setDescN(String descN) {
        this.descN = descN;
    }

    public String getWhN() {
        return whN;
    }

    public void setWhN(String whN) {
        this.whN = whN;
    }

    public String getSts123() {
        return sts123;
    }

    public void setSts123(String sts123) {
        this.sts123 = sts123;
    }

    public String getSts456() {
        return sts456;
    }

    public void setSts456(String sts456) {
        this.sts456 = sts456;
    }

    public String getWh() {
        return wh;
    }

    public void setWh(String wh) {
        this.wh = wh;
    }

    public String getPlant() {
        return plant;
    }

    public void setPlant(String plant) {
        this.plant = plant;
    }

    public String getMatCt() {
        return matCt;
    }

    public void setMatCt(String matCt) {
        this.matCt = matCt;
    }

    public String getRm() {
        return rm;
    }

    public void setRm(String rm) {
        this.rm = rm;
    }

    public String getMatCo() {
        return matCo;
    }

    public void setMatCo(String matCo) {
        this.matCo = matCo;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getSts1() {
        return sts1;
    }

    public void setSts1(String sts1) {
        this.sts1 = sts1;
    }

    public String getSts2() {
        return sts2;
    }

    public void setSts2(String sts2) {
        this.sts2 = sts2;
    }

    public String getSts3() {
        return sts3;
    }

    public void setSts3(String sts3) {
        this.sts3 = sts3;
    }

    public String getSts4() {
        return sts4;
    }

    public void setSts4(String sts4) {
        this.sts4 = sts4;
    }

    public String getSts5() {
        return sts5;
    }

    public void setSts5(String sts5) {
        this.sts5 = sts5;
    }

    public String getSts6() {
        return sts6;
    }

    public void setSts6(String sts6) {
        this.sts6 = sts6;
    }

    public String getSts7() {
        return sts7;
    }

    public void setSts7(String sts7) {
        this.sts7 = sts7;
    }

    public String getStsO() {
        return stsO;
    }

    public void setStsO(String stsO) {
        this.stsO = stsO;
    }

    public String getStsP() {
        return stsP;
    }

    public void setStsP(String stsP) {
        this.stsP = stsP;
    }

    public List<WMS950> getDetList() {
        return detList;
    }

    public void setDetList(List<WMS950> detList) {
        this.detList = detList;
    }

}
