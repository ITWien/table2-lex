/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.twc.wms.entity;

/**
 *
 * @author 93176
 */
public class ISMTOPK {

    private String GRPNO;
    private int ISSUENO;
    private String MATNR;
    private Double ISSUETOPICK;

    public ISMTOPK() {
    }

    public ISMTOPK(String GRPNO, int ISSUENO, String MATNR, Double ISSUETOPICK) {
        this.GRPNO = GRPNO;
        this.ISSUENO = ISSUENO;
        this.MATNR = MATNR;
        this.ISSUETOPICK = ISSUETOPICK;
    }

    public String getGRPNO() {
        return GRPNO;
    }

    public void setGRPNO(String GRPNO) {
        this.GRPNO = GRPNO;
    }

    public int getISSUENO() {
        return ISSUENO;
    }

    public void setISSUENO(int ISSUENO) {
        this.ISSUENO = ISSUENO;
    }

    public String getMATNR() {
        return MATNR;
    }

    public void setMATNR(String MATNR) {
        this.MATNR = MATNR;
    }

    public Double getISSUETOPICK() {
        return ISSUETOPICK;
    }

    public void setISSUETOPICK(Double ISSUETOPICK) {
        this.ISSUETOPICK = ISSUETOPICK;
    }

}
