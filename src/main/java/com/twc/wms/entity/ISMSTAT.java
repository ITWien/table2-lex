/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.twc.wms.entity;

/**
 *
 * @author 93176
 */
public class ISMSTAT {

    private String GRPNO;
    private int ISSUENO;
    private String MATNR;
    private int STATUS;

    public ISMSTAT() {
    }

    public ISMSTAT(String GRPNO, int ISSUENO, String MATNR, int STATUS) {
        this.GRPNO = GRPNO;
        this.ISSUENO = ISSUENO;
        this.MATNR = MATNR;
        this.STATUS = STATUS;
    }

    public String getGRPNO() {
        return GRPNO;
    }

    public void setGRPNO(String GRPNO) {
        this.GRPNO = GRPNO;
    }

    public int getISSUENO() {
        return ISSUENO;
    }

    public void setISSUENO(int ISSUENO) {
        this.ISSUENO = ISSUENO;
    }

    public String getMATNR() {
        return MATNR;
    }

    public void setMATNR(String MATNR) {
        this.MATNR = MATNR;
    }

    public int getSTATUS() {
        return STATUS;
    }

    public void setSTATUS(int STATUS) {
        this.STATUS = STATUS;
    }

}
