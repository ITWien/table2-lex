/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.entity;

/**
 *
 * @author nutthawoot.noo
 */
public class SAPMAS {

    private String matcode;
    private String desc;
    private String plant;
    private String vtype;
    private String mtype;
    private String um;
    private String mgroup;
    private String mgdesc;
    private String mc;
    private String mcname;
    private String pgroup;
    private String pgname;
    private String location;

    public SAPMAS() {

    }

    public SAPMAS(String matcode, String desc, String plant,
            String vtype, String mtype, String um,
            String mgroup, String mgdesc, String mc,
            String mcname, String pgroup, String pgname, String location) {

        this.matcode = matcode;
        this.desc = desc;
        this.plant = plant;
        this.vtype = vtype;
        this.mtype = mtype;
        this.um = um;
        this.mgroup = mgroup;
        this.mgdesc = mgdesc;
        this.mc = mc;
        this.mcname = mcname;
        this.pgroup = pgroup;
        this.pgname = pgname;
        this.location = location;
    }

    public String getMatcode() {

        return matcode;

    }

    public void setMatcode(String matcode) {

        this.matcode = matcode;

    }

    public String getDesc() {

        return desc;

    }

    public void setDesc(String desc) {

        this.desc = desc;

    }

    public String getPlant() {

        return plant;

    }

    public void setPlant(String plant) {

        this.plant = plant;

    }

    public String getVtype() {

        return vtype;

    }

    public void setVtype(String vtype) {

        this.vtype = vtype;

    }

    public String getMtype() {

        return mtype;

    }

    public void setMtype(String mtype) {

        this.mtype = mtype;

    }

    public String getUm() {

        return um;

    }

    public void setUm(String um) {

        this.um = um;

    }

    public String getMgroup() {

        return mgroup;

    }

    public void setMgroup(String mgroup) {

        this.mgroup = mgroup;

    }

    public String getMgdesc() {

        return mgdesc;

    }

    public void setMgdesc(String mgdesc) {

        this.mgdesc = mgdesc;

    }

    public String getMatCtrl() {

        return mc;

    }

    public void setMatCtrl(String mc) {

        this.mc = mc;

    }

    public String getMatCtrlName() {

        return mcname;

    }

    public void setMatCtrlName(String mcname) {

        this.mcname = mcname;

    }
    
    public String getPgroup() {

        return pgroup;

    }

    public void setPgroup(String pgroup) {

        this.pgroup = pgroup;

    }
    
    public String getPgroupName() {

        return pgname;

    }

    public void setPgroupName(String pgname) {

        this.pgname = pgname;

    }
    
    public String getLocation() {

        return location;

    }

    public void setLocation(String location) {

        this.location = location;

    }
}
