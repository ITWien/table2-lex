/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.entity;

import java.util.ArrayList;
import com.twc.wms.entity.QIDETAIL;
import java.util.List;

/**
 *
 * @author nutthawoot.noo
 */
public class WMS990 {

    private String WH;
    private String DEPT;
    private String PRODUCT;
    private String SUBNAME;
    private String NAME;
    private String OH_SKU;
    private String CK_SKU;
    private String CK_SKU_PER;
    private String COL_SKU;
    private String COL_SKU_PER;
    private String DIFF_SKU;
    private String DIFF_SKU_PER;
    private String AMOUNT;
    private String PACKPCS;
    private String AMOUNT2;
    private String PACKPCS2;
    private String MVTSKU;

    public WMS990() {

    }

    public String getMVTSKU() {
        return MVTSKU;
    }

    public void setMVTSKU(String MVTSKU) {
        this.MVTSKU = MVTSKU;
    }

    public String getAMOUNT() {
        return AMOUNT;
    }

    public void setAMOUNT(String AMOUNT) {
        this.AMOUNT = AMOUNT;
    }

    public String getPACKPCS() {
        return PACKPCS;
    }

    public void setPACKPCS(String PACKPCS) {
        this.PACKPCS = PACKPCS;
    }

    public String getAMOUNT2() {
        return AMOUNT2;
    }

    public void setAMOUNT2(String AMOUNT2) {
        this.AMOUNT2 = AMOUNT2;
    }

    public String getPACKPCS2() {
        return PACKPCS2;
    }

    public void setPACKPCS2(String PACKPCS2) {
        this.PACKPCS2 = PACKPCS2;
    }

    public String getWH() {
        return WH;
    }

    public void setWH(String WH) {
        this.WH = WH;
    }

    public String getDEPT() {
        return DEPT;
    }

    public void setDEPT(String DEPT) {
        this.DEPT = DEPT;
    }

    public String getPRODUCT() {
        return PRODUCT;
    }

    public void setPRODUCT(String PRODUCT) {
        this.PRODUCT = PRODUCT;
    }

    public String getSUBNAME() {
        return SUBNAME;
    }

    public void setSUBNAME(String SUBNAME) {
        this.SUBNAME = SUBNAME;
    }

    public String getNAME() {
        return NAME;
    }

    public void setNAME(String NAME) {
        this.NAME = NAME;
    }

    public String getOH_SKU() {
        return OH_SKU;
    }

    public void setOH_SKU(String OH_SKU) {
        this.OH_SKU = OH_SKU;
    }

    public String getCK_SKU() {
        return CK_SKU;
    }

    public void setCK_SKU(String CK_SKU) {
        this.CK_SKU = CK_SKU;
    }

    public String getCK_SKU_PER() {
        return CK_SKU_PER;
    }

    public void setCK_SKU_PER(String CK_SKU_PER) {
        this.CK_SKU_PER = CK_SKU_PER;
    }

    public String getCOL_SKU() {
        return COL_SKU;
    }

    public void setCOL_SKU(String COL_SKU) {
        this.COL_SKU = COL_SKU;
    }

    public String getCOL_SKU_PER() {
        return COL_SKU_PER;
    }

    public void setCOL_SKU_PER(String COL_SKU_PER) {
        this.COL_SKU_PER = COL_SKU_PER;
    }

    public String getDIFF_SKU() {
        return DIFF_SKU;
    }

    public void setDIFF_SKU(String DIFF_SKU) {
        this.DIFF_SKU = DIFF_SKU;
    }

    public String getDIFF_SKU_PER() {
        return DIFF_SKU_PER;
    }

    public void setDIFF_SKU_PER(String DIFF_SKU_PER) {
        this.DIFF_SKU_PER = DIFF_SKU_PER;
    }
}
