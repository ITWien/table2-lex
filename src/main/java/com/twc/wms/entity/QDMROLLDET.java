/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.entity;

/**
 *
 * @author nutthawoot.noo
 */
public class QDMROLLDET {

    private String QDMRSTYLE;
    private String QDMRLINO;
    private String QDMRLENGTH1;
    private String QDMRLENGTH2;
    private String QDMRQTY;
    private String QDMRHEIGHT;
    private String option;

    public QDMROLLDET() {

    }

    public String getQDMRSTYLE() {
        return QDMRSTYLE;
    }

    public void setQDMRSTYLE(String QDMRSTYLE) {
        this.QDMRSTYLE = QDMRSTYLE;
    }

    public String getQDMRLINO() {
        return QDMRLINO;
    }

    public void setQDMRLINO(String QDMRLINO) {
        this.QDMRLINO = QDMRLINO;
    }

    public String getQDMRLENGTH1() {
        return QDMRLENGTH1;
    }

    public void setQDMRLENGTH1(String QDMRLENGTH1) {
        this.QDMRLENGTH1 = QDMRLENGTH1;
    }

    public String getQDMRLENGTH2() {
        return QDMRLENGTH2;
    }

    public void setQDMRLENGTH2(String QDMRLENGTH2) {
        this.QDMRLENGTH2 = QDMRLENGTH2;
    }

    public String getQDMRQTY() {
        return QDMRQTY;
    }

    public void setQDMRQTY(String QDMRQTY) {
        this.QDMRQTY = QDMRQTY;
    }

    public String getQDMRHEIGHT() {
        return QDMRHEIGHT;
    }

    public void setQDMRHEIGHT(String QDMRHEIGHT) {
        this.QDMRHEIGHT = QDMRHEIGHT;
    }

    public String getOption() {
        return option;
    }

    public void setOption(String option) {
        this.option = option;
    }

}
