/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.entity;

/**
 *
 * @author nutthawoot.noo
 */
public class UserAuth {

    private String uid;
    private String password;
    private String name;
    private String level;
    private String warehouse;
    private String department;
    private String menuset;
    private String path;

    public UserAuth() {

    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getPath() {
        return path;
    }

    public UserAuth(String uid, String password, String name, String level, String warehouse, String department, String menuset) {

        this.uid = uid;
        this.password = password;
        this.name = name;
        this.level = level;
        this.warehouse = warehouse;
        this.department = department;
        this.menuset = menuset;
    }

    public UserAuth(String uid, String password) {

        this.uid = uid;
        this.password = password;
    }

    public String getUid() {

        return uid;

    }

    public String getPassword() {

        return password;

    }

    public String getName() {

        return name;

    }

    public String getLevel() {

        return level;

    }

    public String getWarehouse() {

        return warehouse;

    }

    public String getDepartment() {

        return department;

    }

    public String getMenuset() {

        return menuset;

    }

    public void setUid(String uid) {

        this.uid = uid;

    }

    public void setPassword(String password) {

        this.password = password;

    }

    public void setName(String name) {

        this.name = name;

    }

    public void setLevel(String level) {

        this.level = level;

    }

    public void setWarehouse(String warehouse) {

        this.warehouse = warehouse;

    }

    public void setDepartment(String department) {

        this.department = department;

    }

    public void setMenuset(String menuset) {

        this.menuset = menuset;

    }

}
