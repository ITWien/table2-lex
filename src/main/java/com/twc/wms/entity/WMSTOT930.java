/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.entity;

import java.util.ArrayList;
import com.twc.wms.entity.QIDETAIL;
import java.util.List;

/**
 *
 * @author nutthawoot.noo
 */
public class WMSTOT930 {

    private String TCOM;
    private String TRUNNO;
    private String TOHDT;
    private String TPLANT;
    private String TWHSE;
    private String TMTCL;
    private String TSKU;
    private String TPCK;
    private String TPER;
    private String TAMT;
    private String TCOSKU;
    private String TCOPCK;
    private String TCOPER;
    private String TCOAMT;
    private String TCRSKU;
    private String TCRPCK;
    private String TCRPER;
    private String TCRAMT;
    private String TICSKU;
    private String TICPCK;
    private String TICPER;
    private String TICAMT;
    private String TDMSKU;
    private String TDMPCK;
    private String TDMPER;
    private String TDMAMT;
    private String TDPSKU;
    private String TDPPCK;
    private String TDPPER;
    private String TDPAMT;
    private String TCDT;
    private String TUSER;

    public WMSTOT930() {

    }

    public String getTCOM() {
        return TCOM;
    }

    public void setTCOM(String TCOM) {
        this.TCOM = TCOM;
    }

    public String getTRUNNO() {
        return TRUNNO;
    }

    public void setTRUNNO(String TRUNNO) {
        this.TRUNNO = TRUNNO;
    }

    public String getTOHDT() {
        return TOHDT;
    }

    public void setTOHDT(String TOHDT) {
        this.TOHDT = TOHDT;
    }

    public String getTPLANT() {
        return TPLANT;
    }

    public void setTPLANT(String TPLANT) {
        this.TPLANT = TPLANT;
    }

    public String getTWHSE() {
        return TWHSE;
    }

    public void setTWHSE(String TWHSE) {
        this.TWHSE = TWHSE;
    }

    public String getTMTCL() {
        return TMTCL;
    }

    public void setTMTCL(String TMTCL) {
        this.TMTCL = TMTCL;
    }

    public String getTSKU() {
        return TSKU;
    }

    public void setTSKU(String TSKU) {
        this.TSKU = TSKU;
    }

    public String getTPCK() {
        return TPCK;
    }

    public void setTPCK(String TPCK) {
        this.TPCK = TPCK;
    }

    public String getTPER() {
        return TPER;
    }

    public void setTPER(String TPER) {
        this.TPER = TPER;
    }

    public String getTAMT() {
        return TAMT;
    }

    public void setTAMT(String TAMT) {
        this.TAMT = TAMT;
    }

    public String getTCOSKU() {
        return TCOSKU;
    }

    public void setTCOSKU(String TCOSKU) {
        this.TCOSKU = TCOSKU;
    }

    public String getTCOPCK() {
        return TCOPCK;
    }

    public void setTCOPCK(String TCOPCK) {
        this.TCOPCK = TCOPCK;
    }

    public String getTCOPER() {
        return TCOPER;
    }

    public void setTCOPER(String TCOPER) {
        this.TCOPER = TCOPER;
    }

    public String getTCOAMT() {
        return TCOAMT;
    }

    public void setTCOAMT(String TCOAMT) {
        this.TCOAMT = TCOAMT;
    }

    public String getTCRSKU() {
        return TCRSKU;
    }

    public void setTCRSKU(String TCRSKU) {
        this.TCRSKU = TCRSKU;
    }

    public String getTCRPCK() {
        return TCRPCK;
    }

    public void setTCRPCK(String TCRPCK) {
        this.TCRPCK = TCRPCK;
    }

    public String getTCRPER() {
        return TCRPER;
    }

    public void setTCRPER(String TCRPER) {
        this.TCRPER = TCRPER;
    }

    public String getTCRAMT() {
        return TCRAMT;
    }

    public void setTCRAMT(String TCRAMT) {
        this.TCRAMT = TCRAMT;
    }

    public String getTICSKU() {
        return TICSKU;
    }

    public void setTICSKU(String TICSKU) {
        this.TICSKU = TICSKU;
    }

    public String getTICPCK() {
        return TICPCK;
    }

    public void setTICPCK(String TICPCK) {
        this.TICPCK = TICPCK;
    }

    public String getTICPER() {
        return TICPER;
    }

    public void setTICPER(String TICPER) {
        this.TICPER = TICPER;
    }

    public String getTICAMT() {
        return TICAMT;
    }

    public void setTICAMT(String TICAMT) {
        this.TICAMT = TICAMT;
    }

    public String getTDMSKU() {
        return TDMSKU;
    }

    public void setTDMSKU(String TDMSKU) {
        this.TDMSKU = TDMSKU;
    }

    public String getTDMPCK() {
        return TDMPCK;
    }

    public void setTDMPCK(String TDMPCK) {
        this.TDMPCK = TDMPCK;
    }

    public String getTDMPER() {
        return TDMPER;
    }

    public void setTDMPER(String TDMPER) {
        this.TDMPER = TDMPER;
    }

    public String getTDMAMT() {
        return TDMAMT;
    }

    public void setTDMAMT(String TDMAMT) {
        this.TDMAMT = TDMAMT;
    }

    public String getTDPSKU() {
        return TDPSKU;
    }

    public void setTDPSKU(String TDPSKU) {
        this.TDPSKU = TDPSKU;
    }

    public String getTDPPCK() {
        return TDPPCK;
    }

    public void setTDPPCK(String TDPPCK) {
        this.TDPPCK = TDPPCK;
    }

    public String getTDPPER() {
        return TDPPER;
    }

    public void setTDPPER(String TDPPER) {
        this.TDPPER = TDPPER;
    }

    public String getTDPAMT() {
        return TDPAMT;
    }

    public void setTDPAMT(String TDPAMT) {
        this.TDPAMT = TDPAMT;
    }

    public String getTCDT() {
        return TCDT;
    }

    public void setTCDT(String TCDT) {
        this.TCDT = TCDT;
    }

    public String getTUSER() {
        return TUSER;
    }

    public void setTUSER(String TUSER) {
        this.TUSER = TUSER;
    }

}
