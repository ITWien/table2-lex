/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.entity;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author nutthawoot.noo
 */
public class WMSMASD {

    private String LINETYP;
    private String VBELN;
    private String POSNR;
    private String MATNR;
    private String ARKTX;
    private String WERKS;
    private String BWTAR;
    private String LGORT;
    private String KWMENG;
    private String VRKME;
    private String PRICE;
    private String LGPBE;
    private String MATCNAME;
    private String MATGROUP;
    private String MATWID;
    private String MATCOM;
    private String UNR01;
    private String UNR03;
    private String DIFF;
    private String ISSUENO;
    private String ISSUETOPICK;
    private String REMAIN;
    private String TEMP;
    private String PERCENT;
    private String ONHAND;
    private String STATUS;
    private String STATUSDESC;
    private String BOX;
    private String BAG;
    private String ROLL;
    private String TYPE;
    private String NO;

    public WMSMASD() {

    }

    public String getLINETYP() {
        return LINETYP;
    }

    public void setLINETYP(String LINETYP) {
        this.LINETYP = LINETYP;
    }

    public String getVBELN() {
        return VBELN;
    }

    public void setVBELN(String VBELN) {
        this.VBELN = VBELN;
    }

    public String getPOSNR() {
        return POSNR;
    }

    public void setPOSNR(String POSNR) {
        this.POSNR = POSNR;
    }

    public String getMATNR() {
        return MATNR;
    }

    public void setMATNR(String MATNR) {
        this.MATNR = MATNR;
    }

    public String getARKTX() {
        return ARKTX;
    }

    public void setARKTX(String ARKTX) {
        this.ARKTX = ARKTX;
    }

    public String getWERKS() {
        return WERKS;
    }

    public void setWERKS(String WERKS) {
        this.WERKS = WERKS;
    }

    public String getBWTAR() {
        return BWTAR;
    }

    public void setBWTAR(String BWTAR) {
        this.BWTAR = BWTAR;
    }

    public String getLGORT() {
        return LGORT;
    }

    public void setLGORT(String LGORT) {
        this.LGORT = LGORT;
    }

    public String getKWMENG() {
        return KWMENG;
    }

    public void setKWMENG(String KWMENG) {
        this.KWMENG = KWMENG;
    }

    public String getVRKME() {
        return VRKME;
    }

    public void setVRKME(String VRKME) {
        this.VRKME = VRKME;
    }

    public String getPRICE() {
        return PRICE;
    }

    public void setPRICE(String PRICE) {
        this.PRICE = PRICE;
    }

    public String getLGPBE() {
        return LGPBE;
    }

    public void setLGPBE(String LGPBE) {
        this.LGPBE = LGPBE;
    }

    public String getMATCNAME() {
        return MATCNAME;
    }

    public void setMATCNAME(String MATCNAME) {
        this.MATCNAME = MATCNAME;
    }

    public String getMATGROUP() {
        return MATGROUP;
    }

    public void setMATGROUP(String MATGROUP) {
        this.MATGROUP = MATGROUP;
    }

    public String getMATWID() {
        return MATWID;
    }

    public void setMATWID(String MATWID) {
        this.MATWID = MATWID;
    }

    public String getMATCOM() {
        return MATCOM;
    }

    public void setMATCOM(String MATCOM) {
        this.MATCOM = MATCOM;
    }

    public String getUNR01() {
        return UNR01;
    }

    public void setUNR01(String UNR01) {
        this.UNR01 = UNR01;
    }

    public String getUNR03() {
        return UNR03;
    }

    public void setUNR03(String UNR03) {
        this.UNR03 = UNR03;
    }

    public String getDIFF() {
        return DIFF;
    }

    public void setDIFF(String DIFF) {
        this.DIFF = DIFF;
    }

    public String getISSUENO() {
        return ISSUENO;
    }

    public void setISSUENO(String ISSUENO) {
        this.ISSUENO = ISSUENO;
    }

    public String getISSUETOPICK() {
        return ISSUETOPICK;
    }

    public void setISSUETOPICK(String ISSUETOPICK) {
        this.ISSUETOPICK = ISSUETOPICK;
    }

    public String getREMAIN() {
        return REMAIN;
    }

    public void setREMAIN(String REMAIN) {
        this.REMAIN = REMAIN;
    }

    public String getTEMP() {
        return TEMP;
    }

    public void setTEMP(String TEMP) {
        this.TEMP = TEMP;
    }

    public String getPERCENT() {
        return PERCENT;
    }

    public void setPERCENT(String PERCENT) {
        this.PERCENT = PERCENT;
    }

    public String getONHAND() {
        return ONHAND;
    }

    public void setONHAND(String ONHAND) {
        this.ONHAND = ONHAND;
    }

    public String getSTATUS() {
        return STATUS;
    }

    public void setSTATUS(String STATUS) {
        this.STATUS = STATUS;
    }

    public String getSTATUSDESC() {
        return STATUSDESC;
    }

    public void setSTATUSDESC(String STATUSDESC) {
        this.STATUSDESC = STATUSDESC;
    }

    public String getBOX() {
        return BOX;
    }

    public void setBOX(String BOX) {
        this.BOX = BOX;
    }

    public String getBAG() {
        return BAG;
    }

    public void setBAG(String BAG) {
        this.BAG = BAG;
    }

    public String getROLL() {
        return ROLL;
    }

    public void setROLL(String ROLL) {
        this.ROLL = ROLL;
    }

    public String getTYPE() {
        return TYPE;
    }

    public void setTYPE(String TYPE) {
        this.TYPE = TYPE;
    }

    public String getNO() {
        return NO;
    }

    public void setNO(String NO) {
        this.NO = NO;
    }

}
