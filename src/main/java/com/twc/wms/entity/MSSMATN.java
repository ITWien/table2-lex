/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.entity;

import java.util.ArrayList;
import com.twc.wms.entity.QIDETAIL;
import java.util.List;

/**
 *
 * @author nutthawoot.noo
 */
public class MSSMATN {

    private String uid;
    private String name;
    private String warehouse;
    private String desc;
    private List<MSSMATN> whList = new ArrayList<MSSMATN>();
    private List<QIDETAIL> detList = new ArrayList<QIDETAIL>();
    private String tolQno;
    private String tolQty;
    private String Fname;
    private String box;

    public MSSMATN() {

    }

    public String getBox() {
        return box;
    }

    public void setBox(String box) {
        this.box = box;
    }

    public String getFname() {
        return Fname;
    }

    public void setFname(String Fname) {
        this.Fname = Fname;
    }

    public String getTolQno() {
        return tolQno;
    }

    public void setTolQno(String tolQno) {
        this.tolQno = tolQno;
    }

    public String getTolQty() {
        return tolQty;
    }

    public void setTolQty(String tolQty) {
        this.tolQty = tolQty;
    }

    public void setDetList(List<QIDETAIL> detList) {
        this.detList = detList;
    }

    public List<QIDETAIL> getDetList() {
        return detList;
    }

    public void setWhList(List<MSSMATN> whList) {
        this.whList = whList;
    }

    public List<MSSMATN> getWhList() {
        return whList;
    }

    public MSSMATN(String uid, String name, String warehouse) {

        this.uid = uid;
        this.name = name;
        this.warehouse = warehouse;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getDesc() {
        return desc;
    }

    public String getUid() {

        return uid;

    }

    public String getName() {

        return name;

    }

    public String getWarehouse() {

        return warehouse;

    }

    public void setUid(String uid) {

        this.uid = uid;

    }

    public void setName(String name) {

        this.name = name;

    }

    public void setWarehouse(String warehouse) {

        this.warehouse = warehouse;

    }

}
