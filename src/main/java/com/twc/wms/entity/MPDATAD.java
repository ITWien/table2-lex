/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.entity;

/**
 *
 * @author nutthawoot.noo
 */
public class MPDATAD {

    private String LINETYP;
    private String VBELN;
    private String POSNR;
    private String MATNR;
    private String ARKTX;
    private String WERKS;
    private String BWTAR;
    private String LGORT;
    private float KWMENG;
    private String VRKME;
    private float PRICE;
    private String LGPBE;
    private String MATCNAME;
    private String MATGROUP;
    private String MATWID;
    private String MATCOM;
    private float UNR01;
    private float UNR03;
    private float DIFF;
    private int ISSUENO;
    private float ISSUETOPICK;
    private float REMAIN;
    private float TEMP;
    private float PERCENT;
    private float ONHAND;
    private String STATUS;
    private String STATUSDESC;
    private int BOX;
    private int BAG;
    private int ROLL;
    private String TYPE;
    private int NO;

    public void setLINETYP(String LINETYP) {
        this.LINETYP = LINETYP;
    }

    public String getLINETYP() {
        return LINETYP;
    }

    public void setVBELN(String VBELN) {
        this.VBELN = VBELN;
    }

    public String getVBELN() {
        return VBELN;
    }

    public void setPOSNR(String POSNR) {
        this.POSNR = POSNR;
    }

    public String getPOSNR() {
        return POSNR;
    }

    public void setMATNR(String MATNR) {
        this.MATNR = MATNR;
    }

    public String getMATNR() {
        return MATNR;
    }

    public void setARKTX(String ARKTX) {
        this.ARKTX = ARKTX;
    }

    public String getARKTX() {
        return ARKTX;
    }

    public void setWERKS(String WERKS) {
        this.WERKS = WERKS;
    }

    public String getWERKS() {
        return WERKS;
    }

    public void setBWTAR(String BWTAR) {
        this.BWTAR = BWTAR;
    }

    public String getBWTAR() {
        return BWTAR;
    }

    public void setLGORT(String LGORT) {
        this.LGORT = LGORT;
    }

    public String getLGORT() {
        return LGORT;
    }

    public void setKWMENG(float KWMENG) {
        this.KWMENG = KWMENG;
    }

    public float getKWMENG() {
        return KWMENG;
    }

    public void setVRKME(String VRKME) {
        this.VRKME = VRKME;
    }

    public String getVRKME() {
        return VRKME;
    }

    public void setPRICE(float PRICE) {
        this.PRICE = PRICE;
    }

    public float getPRICE() {
        return PRICE;
    }

    public void setLGPBE(String LGPBE) {
        this.LGPBE = LGPBE;
    }

    public String getLGPBE() {
        return LGPBE;
    }

    public void setMATCNAME(String MATCNAME) {
        this.MATCNAME = MATCNAME;
    }

    public String getMATCNAME() {
        return MATCNAME;
    }

    public void setMATGROUP(String MATGROUP) {
        this.MATGROUP = MATGROUP;
    }

    public String getMATGROUP() {
        return MATGROUP;
    }

    public void setMATWID(String MATWID) {
        this.MATWID = MATWID;
    }

    public String getMATWID() {
        return MATWID;
    }

    public void setMATCOM(String MATCOM) {
        this.MATCOM = MATCOM;
    }

    public String getMATCOM() {
        return MATCOM;
    }

    public void setUNR01(float UNR01) {
        this.UNR01 = UNR01;
    }

    public float getUNR01() {
        return UNR01;
    }

    public void setUNR03(float UNR03) {
        this.UNR03 = UNR03;
    }

    public float getUNR03() {
        return UNR03;
    }

    public void setDIFF(float DIFF) {
        this.DIFF = DIFF;
    }

    public float getDIFF() {
        return DIFF;
    }

    public void setISSUENO(int ISSUENO) {
        this.ISSUENO = ISSUENO;
    }

    public int getISSUENO() {
        return ISSUENO;
    }

    public void setISSUETOPICK(float ISSUETOPICK) {
        this.ISSUETOPICK = ISSUETOPICK;
    }

    public float getISSUETOPICK() {
        return ISSUETOPICK;
    }

    public void setREMAIN(float REMAIN) {
        this.REMAIN = REMAIN;
    }

    public float getREMAIN() {
        return REMAIN;
    }

    public void setTEMP(float TEMP) {
        this.TEMP = TEMP;
    }

    public float getTEMP() {
        return TEMP;
    }

    public void setPERCENT(float PERCENT) {
        this.PERCENT = PERCENT;
    }

    public float getPERCENT() {
        return PERCENT;
    }

    public void setONHAND(float ONHAND) {
        this.ONHAND = ONHAND;
    }

    public float getONHAND() {
        return ONHAND;
    }

    public void setSTATUS(String STATUS) {
        this.STATUS = STATUS;
    }

    public String getSTATUS() {
        return STATUS;
    }

    public void setSTATUSDESC(String STATUSDESC) {
        this.STATUSDESC = STATUSDESC;
    }

    public String getSTATUSDESC() {
        return STATUSDESC;
    }

    public void setBOX(int BOX) {
        this.BOX = BOX;
    }

    public int getBOX() {
        return BOX;
    }

    public void setBAG(int BAG) {
        this.BAG = BAG;
    }

    public int getBAG() {
        return BAG;
    }

    public void setROLL(int ROLL) {
        this.ROLL = ROLL;
    }

    public int getROLL() {
        return ROLL;
    }

    public void setTYPE(String TYPE) {
        this.TYPE = TYPE;
    }

    public String getTYPE() {
        return TYPE;
    }

    public void setNO(int NO) {
        this.NO = NO;
    }

    public int getNO() {
        return NO;
    }

}
