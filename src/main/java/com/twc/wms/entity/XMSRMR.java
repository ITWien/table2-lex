/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.entity;

import java.util.ArrayList;
import com.twc.wms.entity.QIDETAIL;
import java.util.List;

/**
 *
 * @author nutthawoot.noo
 */
public class XMSRMR {

    private String XMSRMRMAT;
    private String XMSRMRDESC;
    private String XMSRMRQTY;
    private String XMSRMRPACK;
    private String XMSRMRPLANT;
    private String XMSRMRQRID;
    private String XMSRMRUSER;

    public XMSRMR() {

    }

    public String getXMSRMRMAT() {
        return XMSRMRMAT;
    }

    public void setXMSRMRMAT(String XMSRMRMAT) {
        this.XMSRMRMAT = XMSRMRMAT;
    }

    public String getXMSRMRDESC() {
        return XMSRMRDESC;
    }

    public void setXMSRMRDESC(String XMSRMRDESC) {
        this.XMSRMRDESC = XMSRMRDESC;
    }

    public String getXMSRMRQTY() {
        return XMSRMRQTY;
    }

    public void setXMSRMRQTY(String XMSRMRQTY) {
        this.XMSRMRQTY = XMSRMRQTY;
    }

    public String getXMSRMRPACK() {
        return XMSRMRPACK;
    }

    public void setXMSRMRPACK(String XMSRMRPACK) {
        this.XMSRMRPACK = XMSRMRPACK;
    }

    public String getXMSRMRPLANT() {
        return XMSRMRPLANT;
    }

    public void setXMSRMRPLANT(String XMSRMRPLANT) {
        this.XMSRMRPLANT = XMSRMRPLANT;
    }

    public String getXMSRMRQRID() {
        return XMSRMRQRID;
    }

    public void setXMSRMRQRID(String XMSRMRQRID) {
        this.XMSRMRQRID = XMSRMRQRID;
    }

    public String getXMSRMRUSER() {
        return XMSRMRUSER;
    }

    public void setXMSRMRUSER(String XMSRMRUSER) {
        this.XMSRMRUSER = XMSRMRUSER;
    }
}
