/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.entity;

/**
 *
 * @author nutthawoot.noo
 */
public class QRMTRA_3 {

    private String wh;
    private String plant;
    private String tranDate;
    private String po;//value
    private String tax;//parent
    private String matCode;
    private String name;
    private String qty;
    private String um;
    private int box;
    private int bag;
    private int roll;
    private String pack;//MVT
    private String nameLength;//TRT
    private String id;
    private String no;
    private String user;

    public QRMTRA_3() {

    }

    public QRMTRA_3(String wh, String plant, String tranDate, String po, String tax, String matCode, String name,
            String qty, String um, int box, int bag, int roll, String pack, String nameLength, String id,
            String no, String user) {

        this.wh = wh;
        this.plant = plant;
        this.tranDate = tranDate;
        this.po = po;
        this.tax = tax;
        this.matCode = matCode;
        this.name = name;
        this.qty = qty;
        this.um = um;
        this.box = box;
        this.bag = bag;
        this.roll = roll;
        this.pack = pack;
        this.nameLength = nameLength;
        this.id = id;
        this.no = no;
        this.user = user;

    }

    public String getNo() {

        return no;

    }

    public void setNo(String no) {

        this.no = no;

    }

    public String getUser() {

        return user;

    }

    public void setUser(String user) {

        this.user = user;

    }

    public String getID() {

        return id;

    }

    public void setID(String id) {

        this.id = id;

    }

    public String getWH() {

        return wh;

    }

    public void setWH(String wh) {

        this.wh = wh;

    }

    public String getPlant() {

        return plant;

    }

    public void setPlant(String plant) {

        this.plant = plant;

    }

    public String getTranDate() {

        return tranDate;

    }

    public void setTranDate(String tranDate) {

        this.tranDate = tranDate;

    }

    public String getPo() {

        return po;

    }

    public void setPo(String po) {

        this.po = po;

    }

    public String getTax() {

        return tax;

    }

    public void setTax(String tax) {

        this.tax = tax;

    }

    public String getMatCode() {

        return matCode;

    }

    public void setMatCode(String matCode) {

        this.matCode = matCode;

    }

    public String getName() {

        return name;

    }

    public void setName(String name) {

        this.name = name;

    }

    public String getQty() {

        return qty;

    }

    public void setQty(String qty) {

        this.qty = qty;

    }

    public String getUm() {

        return um;

    }

    public void setUm(String um) {

        this.um = um;

    }

    public int getBox() {

        return box;

    }

    public void setBox(int box) {

        this.box = box;

    }

    public int getBag() {

        return bag;

    }

    public void setBag(int bag) {

        this.bag = bag;

    }

    public int getRoll() {

        return roll;

    }

    public void setRoll(int roll) {

        this.roll = roll;

    }

    public String getPack() {

        return pack;

    }

    public void setPack(String pack) {

        this.pack = pack;

    }

    public String getNL() {

        return nameLength;

    }

    public void setNL(String nameLength) {

        this.nameLength = nameLength;

    }

}
