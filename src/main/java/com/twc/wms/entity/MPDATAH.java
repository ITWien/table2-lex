/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.entity;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 *
 * @author nutthawoot.noo
 */
public class MPDATAH {

    private String LINETYP;
    private String VBELN;
    private Date AUDAT;
    private String AUART;
    private String VTWEG;
    private String BWART;
    private String KUNNR;
    private String NAME1;
    private String NAME2;
    private String ADDR1;
    private String ADDR2;
    private String BSTKD;
    private String SUBMI;
    private String MATLOT;
    private String TAXNO;
    private String BRANCH01;
    private Date DATUM;
    private String STYLE;
    private String COLOR;
    private String LOT;
    private String AMTFG;
    private String POFG;
    private String GRPNO;

    public void setLINETYP(String LINETYP) {
        this.LINETYP = LINETYP;
    }

    public String getLINETYP() {
        return LINETYP;
    }

    public void setVBELN(String VBELN) {
        this.VBELN = VBELN;
    }

    public String getVBELN() {
        return VBELN;
    }

    public void setAUDAT(Date AUDAT) {
        this.AUDAT = AUDAT;
    }

    public Date getAUDAT() {
        return AUDAT;
    }

    public void setAUART(String AUART) {
        this.AUART = AUART;
    }

    public String getAUART() {
        return AUART;
    }

    public void setVTWEG(String VTWEG) {
        this.VTWEG = VTWEG;
    }

    public String getVTWEG() {
        return VTWEG;
    }

    public void setBWART(String BWART) {
        this.BWART = BWART;
    }

    public String getBWART() {
        return BWART;
    }

    public void setKUNNR(String KUNNR) {
        this.KUNNR = KUNNR;
    }

    public String getKUNNR() {
        return KUNNR;
    }

    public void setNAME1(String NAME1) {
        this.NAME1 = NAME1;
    }

    public String getNAME1() {
        return NAME1;
    }

    public void setNAME2(String NAME2) {
        this.NAME2 = NAME2;
    }

    public String getNAME2() {
        return NAME2;
    }

    public void setADDR1(String ADDR1) {
        this.ADDR1 = ADDR1;
    }

    public String getADDR1() {
        return ADDR1;
    }

    public void setADDR2(String ADDR2) {
        this.ADDR2 = ADDR2;
    }

    public String getADDR2() {
        return ADDR2;
    }

    public void setBSTKD(String BSTKD) {
        this.BSTKD = BSTKD;
    }

    public String getBSTKD() {
        return BSTKD;
    }

    public void setSUBMI(String SUBMI) {
        this.SUBMI = SUBMI;
    }

    public String getSUBMI() {
        return SUBMI;
    }

    public void setMATLOT(String MATLOT) {
        this.MATLOT = MATLOT;
    }

    public String getMATLOT() {
        return MATLOT;
    }

    public void setTAXNO(String TAXNO) {
        this.TAXNO = TAXNO;
    }

    public String getTAXNO() {
        return TAXNO;
    }

    public void setBRANCH01(String BRANCH01) {
        this.BRANCH01 = BRANCH01;
    }

    public String getBRANCH01() {
        return BRANCH01;
    }

    public void setDATUM(Date DATUM) {
        this.DATUM = DATUM;
    }

    public Date getDATUM() {
        return DATUM;
    }

    public String getDATUMStr() {
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US).format(DATUM.getTime());
    }

    public void setSTYLE(String STYLE) {
        this.STYLE = STYLE;
    }

    public String getSTYLE() {
        return STYLE;
    }

    public void setCOLOR(String COLOR) {
        this.COLOR = COLOR;
    }

    public String getCOLOR() {
        return COLOR;
    }

    public void setLOT(String LOT) {
        this.LOT = LOT;
    }

    public String getLOT() {
        return LOT;
    }

    public void setAMTFG(String AMTFG) {
        this.AMTFG = AMTFG;
    }

    public String getAMTFG() {
        return AMTFG;
    }

    public void setPOFG(String POFG) {
        this.POFG = POFG;
    }

    public String getPOFG() {
        return POFG;
    }

    public void setGRPNO(String GRPNO) {
        this.GRPNO = GRPNO;
    }

    public String getGRPNO() {
        return GRPNO;
    }

}
