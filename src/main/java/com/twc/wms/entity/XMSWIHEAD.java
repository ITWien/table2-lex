/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.entity;

import java.util.ArrayList;
import com.twc.wms.entity.QIDETAIL;
import java.util.List;

/**
 *
 * @author nutthawoot.noo
 */
public class XMSWIHEAD {

    private String XMSWIHQNO;
    private String XMSWIHWHS;
    private String XMSWIHSPDT;
    private String XMSWIHDEST;
    private String XMSWIHROUND;
    private String XMSWIHLICENNO;
    private String XMSWIHDRIVER;
    private String XMSWIHFOLLOWER;
    private String XMSWIHBAG;
    private String XMSWIHROLL;
    private String XMSWIHBOX;
    private String XMSWIHPCS;
    private String XMSWIHTOT;
    private String XMSWIHUSER;
    private String options;
    private String whn;
    private String destn;
    private String XMSWIHDOCNO;

    public XMSWIHEAD() {

    }

    public String getXMSWIHPCS() {
        return XMSWIHPCS;
    }

    public void setXMSWIHPCS(String XMSWIHPCS) {
        this.XMSWIHPCS = XMSWIHPCS;
    }

    public String getXMSWIHWHS() {
        return XMSWIHWHS;
    }

    public void setXMSWIHWHS(String XMSWIHWHS) {
        this.XMSWIHWHS = XMSWIHWHS;
    }

    public XMSWIHEAD(String XMSWIHQNO, String XMSWIHWHS, String XMSWIHSPDT, String XMSWIHDEST, String XMSWIHROUND, String XMSWIHLICENNO, String XMSWIHDRIVER, String XMSWIHFOLLOWER, String XMSWIHBAG, String XMSWIHROLL, String XMSWIHBOX, String XMSWIHPCS, String XMSWIHTOT, String XMSWIHUSER) {
        this.XMSWIHQNO = XMSWIHQNO;
        this.XMSWIHWHS = XMSWIHWHS;
        this.XMSWIHSPDT = XMSWIHSPDT;
        this.XMSWIHDEST = XMSWIHDEST;
        this.XMSWIHROUND = XMSWIHROUND;
        this.XMSWIHLICENNO = XMSWIHLICENNO;
        this.XMSWIHDRIVER = XMSWIHDRIVER;
        this.XMSWIHFOLLOWER = XMSWIHFOLLOWER;
        this.XMSWIHBAG = XMSWIHBAG;
        this.XMSWIHROLL = XMSWIHROLL;
        this.XMSWIHBOX = XMSWIHBOX;
        this.XMSWIHPCS = XMSWIHPCS;
        this.XMSWIHTOT = XMSWIHTOT;
        this.XMSWIHUSER = XMSWIHUSER;
    }

    public String getXMSWIHQNO() {
        return XMSWIHQNO;
    }

    public void setXMSWIHQNO(String XMSWIHQNO) {
        this.XMSWIHQNO = XMSWIHQNO;
    }

    public String getXMSWIHSPDT() {
        return XMSWIHSPDT;
    }

    public void setXMSWIHSPDT(String XMSWIHSPDT) {
        this.XMSWIHSPDT = XMSWIHSPDT;
    }

    public String getXMSWIHDEST() {
        return XMSWIHDEST;
    }

    public void setXMSWIHDEST(String XMSWIHDEST) {
        this.XMSWIHDEST = XMSWIHDEST;
    }

    public String getXMSWIHROUND() {
        return XMSWIHROUND;
    }

    public void setXMSWIHROUND(String XMSWIHROUND) {
        this.XMSWIHROUND = XMSWIHROUND;
    }

    public String getXMSWIHLICENNO() {
        return XMSWIHLICENNO;
    }

    public void setXMSWIHLICENNO(String XMSWIHLICENNO) {
        this.XMSWIHLICENNO = XMSWIHLICENNO;
    }

    public String getXMSWIHDRIVER() {
        return XMSWIHDRIVER;
    }

    public void setXMSWIHDRIVER(String XMSWIHDRIVER) {
        this.XMSWIHDRIVER = XMSWIHDRIVER;
    }

    public String getXMSWIHFOLLOWER() {
        return XMSWIHFOLLOWER;
    }

    public void setXMSWIHFOLLOWER(String XMSWIHFOLLOWER) {
        this.XMSWIHFOLLOWER = XMSWIHFOLLOWER;
    }

    public String getXMSWIHBAG() {
        return XMSWIHBAG;
    }

    public void setXMSWIHBAG(String XMSWIHBAG) {
        this.XMSWIHBAG = XMSWIHBAG;
    }

    public String getXMSWIHROLL() {
        return XMSWIHROLL;
    }

    public void setXMSWIHROLL(String XMSWIHROLL) {
        this.XMSWIHROLL = XMSWIHROLL;
    }

    public String getXMSWIHBOX() {
        return XMSWIHBOX;
    }

    public void setXMSWIHBOX(String XMSWIHBOX) {
        this.XMSWIHBOX = XMSWIHBOX;
    }

    public String getXMSWIHTOT() {
        return XMSWIHTOT;
    }

    public void setXMSWIHTOT(String XMSWIHTOT) {
        this.XMSWIHTOT = XMSWIHTOT;
    }

    public String getXMSWIHUSER() {
        return XMSWIHUSER;
    }

    public void setXMSWIHUSER(String XMSWIHUSER) {
        this.XMSWIHUSER = XMSWIHUSER;
    }

    public String getOptions() {
        return options;
    }

    public void setOptions(String options) {
        this.options = options;
    }

    public String getWhn() {
        return whn;
    }

    public void setWhn(String whn) {
        this.whn = whn;
    }

    public String getDestn() {
        return destn;
    }

    public void setDestn(String destn) {
        this.destn = destn;
    }

    public String getXMSWIHDOCNO() {
        return XMSWIHDOCNO;
    }

    public void setXMSWIHDOCNO(String XMSWIHDOCNO) {
        this.XMSWIHDOCNO = XMSWIHDOCNO;
    }
}
