/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.entity;

/**
 *
 * @author nutthawoot.noo
 */
public class Warehouse {

    private String code;
    private String name;

    public Warehouse() {

    }

    public Warehouse(String code, String name) {
        
        this.code = code;
        this.name = name;
    }
    
    public String getName() {

        return name;

    }
    
    public void setName(String name) {

        this.name = name;

    }
    
    public String getCode() {

        return code;

    }
    
    public void setCode(String code) {

        this.code = code;

    }

}
