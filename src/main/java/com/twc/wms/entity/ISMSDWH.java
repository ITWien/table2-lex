/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.entity;

/**
 *
 * @author 93176
 */
public class ISMSDWH {

    private String NUM;
    private String ISHTRDT;
    private String ISHCUNO;
    private String ISHCUNM1;
    private String ISHCUNM2;
    private String SODATE;
    private String SODATETO;
    private String SOFROM;
    private String SOTO;
    private String ISHPGRP;
    private String STATUS;
    private String ISHUSER;
    private String ISHCONO;
    private String ISHORD;
    private String Reason;
    private String ISHSEQ;
    private String ISHDWQNO;
    private String ISHUART;
    private String ISHSORG;
    private String ISHTWEG;
    private String ISHDIVI;
    private String ISHSGRP;
    private String ISHSOFF;
    private String ISHMVT;
    private String ISHCUAD1;
    private String ISHCUAD2;
    private String ISHBSTKD;
    private String ISHSUBMI;
    private String ISHMATLOT;
    private String ISHDDATE;
    private String ISHDTIME;
    private String ISHTAXNO;
    private String ISHBRANCH01;
    private String ISHSTYLE;
    private String ISHCOLOR;
    private String ISHLOT;
    private String ISHAMTFG;
    private String ISHSTYLE2;
    private String ISHNOR;
    private String ISHPOFG;
    private String ISHWHNO;
    private String ISHLSTS;
    private String ISHHSTS;
    private String ISHDEST;
    private String ISHREMK;
    private String ISHCUNO2;
    private String ISHDLDT;
    private String ISHDLS;
    private String ISHODLS;
    private String ISHCRDT;
    private String ISHCRTM;
    private String ISHSUSER;
    private String ISHEDT;
    private String ISHETM;
    private String ISHEUSR;
    private String ISHCDT;
    private String ISHCTM;

    private String ISHDATUM;
    private String ISHGRPNO;
    private String ISHREASON;

    public ISMSDWH() {
    }

    public String getNUM() {
        return NUM;
    }

    public void setNUM(String NUM) {
        this.NUM = NUM;
    }

    public String getISHTRDT() {
        return ISHTRDT;
    }

    public void setISHTRDT(String ISHTRDT) {
        this.ISHTRDT = ISHTRDT;
    }

    public String getISHCUNO() {
        return ISHCUNO;
    }

    public void setISHCUNO(String ISHCUNO) {
        this.ISHCUNO = ISHCUNO;
    }

    public String getISHCUNM1() {
        return ISHCUNM1;
    }

    public void setISHCUNM1(String ISHCUNM1) {
        this.ISHCUNM1 = ISHCUNM1;
    }

    public String getISHCUNM2() {
        return ISHCUNM2;
    }

    public void setISHCUNM2(String ISHCUNM2) {
        this.ISHCUNM2 = ISHCUNM2;
    }

    public String getSODATE() {
        return SODATE;
    }

    public void setSODATE(String SODATE) {
        this.SODATE = SODATE;
    }

    public String getSODATETO() {
        return SODATETO;
    }

    public void setSODATETO(String SODATETO) {
        this.SODATETO = SODATETO;
    }

    public String getSOFROM() {
        return SOFROM;
    }

    public void setSOFROM(String SOFROM) {
        this.SOFROM = SOFROM;
    }

    public String getSOTO() {
        return SOTO;
    }

    public void setSOTO(String SOTO) {
        this.SOTO = SOTO;
    }

    public String getISHPGRP() {
        return ISHPGRP;
    }

    public void setISHPGRP(String ISHPGRP) {
        this.ISHPGRP = ISHPGRP;
    }

    public String getSTATUS() {
        return STATUS;
    }

    public void setSTATUS(String STATUS) {
        this.STATUS = STATUS;
    }

    public String getISHUSER() {
        return ISHUSER;
    }

    public void setISHUSER(String ISHUSER) {
        this.ISHUSER = ISHUSER;
    }

    public String getISHCONO() {
        return ISHCONO;
    }

    public void setISHCONO(String ISHCONO) {
        this.ISHCONO = ISHCONO;
    }

    public String getISHORD() {
        return ISHORD;
    }

    public void setISHORD(String ISHORD) {
        this.ISHORD = ISHORD;
    }

    public String getReason() {
        return Reason;
    }

    public void setReason(String Reason) {
        this.Reason = Reason;
    }

    public String getISHSEQ() {
        return ISHSEQ;
    }

    public void setISHSEQ(String ISHSEQ) {
        this.ISHSEQ = ISHSEQ;
    }

    public String getISHDWQNO() {
        return ISHDWQNO;
    }

    public void setISHDWQNO(String ISHDWQNO) {
        this.ISHDWQNO = ISHDWQNO;
    }

    public String getISHUART() {
        return ISHUART;
    }

    public void setISHUART(String ISHUART) {
        this.ISHUART = ISHUART;
    }

    public String getISHSORG() {
        return ISHSORG;
    }

    public void setISHSORG(String ISHSORG) {
        this.ISHSORG = ISHSORG;
    }

    public String getISHTWEG() {
        return ISHTWEG;
    }

    public void setISHTWEG(String ISHTWEG) {
        this.ISHTWEG = ISHTWEG;
    }

    public String getISHDIVI() {
        return ISHDIVI;
    }

    public void setISHDIVI(String ISHDIVI) {
        this.ISHDIVI = ISHDIVI;
    }

    public String getISHSGRP() {
        return ISHSGRP;
    }

    public void setISHSGRP(String ISHSGRP) {
        this.ISHSGRP = ISHSGRP;
    }

    public String getISHSOFF() {
        return ISHSOFF;
    }

    public void setISHSOFF(String ISHSOFF) {
        this.ISHSOFF = ISHSOFF;
    }

    public String getISHMVT() {
        return ISHMVT;
    }

    public void setISHMVT(String ISHMVT) {
        this.ISHMVT = ISHMVT;
    }

    public String getISHCUAD1() {
        return ISHCUAD1;
    }

    public void setISHCUAD1(String ISHCUAD1) {
        this.ISHCUAD1 = ISHCUAD1;
    }

    public String getISHCUAD2() {
        return ISHCUAD2;
    }

    public void setISHCUAD2(String ISHCUAD2) {
        this.ISHCUAD2 = ISHCUAD2;
    }

    public String getISHBSTKD() {
        return ISHBSTKD;
    }

    public void setISHBSTKD(String ISHBSTKD) {
        this.ISHBSTKD = ISHBSTKD;
    }

    public String getISHSUBMI() {
        return ISHSUBMI;
    }

    public void setISHSUBMI(String ISHSUBMI) {
        this.ISHSUBMI = ISHSUBMI;
    }

    public String getISHMATLOT() {
        return ISHMATLOT;
    }

    public void setISHMATLOT(String ISHMATLOT) {
        this.ISHMATLOT = ISHMATLOT;
    }

    public String getISHDDATE() {
        return ISHDDATE;
    }

    public void setISHDDATE(String ISHDDATE) {
        this.ISHDDATE = ISHDDATE;
    }

    public String getISHDTIME() {
        return ISHDTIME;
    }

    public void setISHDTIME(String ISHDTIME) {
        this.ISHDTIME = ISHDTIME;
    }

    public String getISHTAXNO() {
        return ISHTAXNO;
    }

    public void setISHTAXNO(String ISHTAXNO) {
        this.ISHTAXNO = ISHTAXNO;
    }

    public String getISHBRANCH01() {
        return ISHBRANCH01;
    }

    public void setISHBRANCH01(String ISHBRANCH01) {
        this.ISHBRANCH01 = ISHBRANCH01;
    }

    public String getISHSTYLE() {
        return ISHSTYLE;
    }

    public void setISHSTYLE(String ISHSTYLE) {
        this.ISHSTYLE = ISHSTYLE;
    }

    public String getISHCOLOR() {
        return ISHCOLOR;
    }

    public void setISHCOLOR(String ISHCOLOR) {
        this.ISHCOLOR = ISHCOLOR;
    }

    public String getISHLOT() {
        return ISHLOT;
    }

    public void setISHLOT(String ISHLOT) {
        this.ISHLOT = ISHLOT;
    }

    public String getISHAMTFG() {
        return ISHAMTFG;
    }

    public void setISHAMTFG(String ISHAMTFG) {
        this.ISHAMTFG = ISHAMTFG;
    }

    public String getISHSTYLE2() {
        return ISHSTYLE2;
    }

    public void setISHSTYLE2(String ISHSTYLE2) {
        this.ISHSTYLE2 = ISHSTYLE2;
    }

    public String getISHNOR() {
        return ISHNOR;
    }

    public void setISHNOR(String ISHNOR) {
        this.ISHNOR = ISHNOR;
    }

    public String getISHPOFG() {
        return ISHPOFG;
    }

    public void setISHPOFG(String ISHPOFG) {
        this.ISHPOFG = ISHPOFG;
    }

    public String getISHWHNO() {
        return ISHWHNO;
    }

    public void setISHWHNO(String ISHWHNO) {
        this.ISHWHNO = ISHWHNO;
    }

    public String getISHLSTS() {
        return ISHLSTS;
    }

    public void setISHLSTS(String ISHLSTS) {
        this.ISHLSTS = ISHLSTS;
    }

    public String getISHHSTS() {
        return ISHHSTS;
    }

    public void setISHHSTS(String ISHHSTS) {
        this.ISHHSTS = ISHHSTS;
    }

    public String getISHDEST() {
        return ISHDEST;
    }

    public void setISHDEST(String ISHDEST) {
        this.ISHDEST = ISHDEST;
    }

    public String getISHREMK() {
        return ISHREMK;
    }

    public void setISHREMK(String ISHREMK) {
        this.ISHREMK = ISHREMK;
    }

    public String getISHCUNO2() {
        return ISHCUNO2;
    }

    public void setISHCUNO2(String ISHCUNO2) {
        this.ISHCUNO2 = ISHCUNO2;
    }

    public String getISHDLDT() {
        return ISHDLDT;
    }

    public void setISHDLDT(String ISHDLDT) {
        this.ISHDLDT = ISHDLDT;
    }

    public String getISHDLS() {
        return ISHDLS;
    }

    public void setISHDLS(String ISHDLS) {
        this.ISHDLS = ISHDLS;
    }

    public String getISHODLS() {
        return ISHODLS;
    }

    public void setISHODLS(String ISHODLS) {
        this.ISHODLS = ISHODLS;
    }

    public String getISHCRDT() {
        return ISHCRDT;
    }

    public void setISHCRDT(String ISHCRDT) {
        this.ISHCRDT = ISHCRDT;
    }

    public String getISHCRTM() {
        return ISHCRTM;
    }

    public void setISHCRTM(String ISHCRTM) {
        this.ISHCRTM = ISHCRTM;
    }

    public String getISHSUSER() {
        return ISHSUSER;
    }

    public void setISHSUSER(String ISHSUSER) {
        this.ISHSUSER = ISHSUSER;
    }

    public String getISHEDT() {
        return ISHEDT;
    }

    public void setISHEDT(String ISHEDT) {
        this.ISHEDT = ISHEDT;
    }

    public String getISHETM() {
        return ISHETM;
    }

    public void setISHETM(String ISHETM) {
        this.ISHETM = ISHETM;
    }

    public String getISHEUSR() {
        return ISHEUSR;
    }

    public void setISHEUSR(String ISHEUSR) {
        this.ISHEUSR = ISHEUSR;
    }

    public String getISHCDT() {
        return ISHCDT;
    }

    public void setISHCDT(String ISHCDT) {
        this.ISHCDT = ISHCDT;
    }

    public String getISHCTM() {
        return ISHCTM;
    }

    public void setISHCTM(String ISHCTM) {
        this.ISHCTM = ISHCTM;
    }

    public String getISHDATUM() {
        return ISHDATUM;
    }

    public void setISHDATUM(String ISHDATUM) {
        this.ISHDATUM = ISHDATUM;
    }

    public String getISHGRPNO() {
        return ISHGRPNO;
    }

    public void setISHGRPNO(String ISHGRPNO) {
        this.ISHGRPNO = ISHGRPNO;
    }

    public String getISHREASON() {
        return ISHREASON;
    }

    public void setISHREASON(String ISHREASON) {
        this.ISHREASON = ISHREASON;
    }

}
