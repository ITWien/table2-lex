/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.entity;

/**
 *
 * @author nutthawoot.noo
 */
public class MPDATAS {

    private int SWCARNO;
    private int SWCHECK;
    private float SWDMLWD;
    private float SWDMLEN;
    private float SWDMHG;
    private String SWDMUNIT;
    private float SWGWEIGHT;
    private String SWNTUNIT;
    private String SWGRPST;
    private String SWITEM;
    private int SWRUNSQ;
    private String SWTRUCK;

    public void setSWRUNSQ(int SWRUNSQ) {
        this.SWRUNSQ = SWRUNSQ;
    }

    public int getSWRUNSQ() {
        return SWRUNSQ;
    }

    public void setSWITEM(String SWITEM) {
        this.SWITEM = SWITEM;
    }

    public String getSWITEM() {
        return SWITEM;
    }

    public void setSWCARNO(int SWCARNO) {
        this.SWCARNO = SWCARNO;
    }

    public int getSWCARNO() {
        return SWCARNO;
    }

    public void setSWCHECK(int SWCHECK) {
        this.SWCHECK = SWCHECK;
    }

    public int getSWCHECK() {
        return SWCHECK;
    }

    public void setSWDMLWD(float SWDMLWD) {
        this.SWDMLWD = SWDMLWD;
    }

    public float getSWDMLWD() {
        return SWDMLWD;
    }

    public void setSWDMLEN(float SWDMLEN) {
        this.SWDMLEN = SWDMLEN;
    }

    public float getSWDMLEN() {
        return SWDMLEN;
    }

    public void setSWDMHG(float SWDMHG) {
        this.SWDMHG = SWDMHG;
    }

    public float getSWDMHG() {
        return SWDMHG;
    }

    public void setSWDMUNIT(String SWDMUNIT) {
        this.SWDMUNIT = SWDMUNIT;
    }

    public String getSWDMUNIT() {
        return SWDMUNIT;
    }

    public void setSWGWEIGHT(float SWGWEIGHT) {
        this.SWGWEIGHT = SWGWEIGHT;
    }

    public float getSWGWEIGHT() {
        return SWGWEIGHT;
    }

    public void setSWNTUNIT(String SWNTUNIT) {
        this.SWNTUNIT = SWNTUNIT;
    }

    public String getSWNTUNIT() {
        return SWNTUNIT;
    }

    public void setSWGRPST(String SWGRPST) {
        this.SWGRPST = SWGRPST;
    }

    public String getSWGRPST() {
        return SWGRPST;
    }

    public void setSWTRUCK(String SWTRUCK) {
        this.SWTRUCK = SWTRUCK;
    }

    public String getSWTRUCK() {
        return SWTRUCK;
    }

}
