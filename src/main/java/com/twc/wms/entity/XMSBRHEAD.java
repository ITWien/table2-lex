/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.entity;

import java.util.ArrayList;
import com.twc.wms.entity.QIDETAIL;
import java.util.List;

/**
 *
 * @author nutthawoot.noo
 */
public class XMSBRHEAD {

    private String XMSBRHQNO;
    private String XMSBRHSPDT;
    private String XMSBRHDEST;
    private String XMSBRHROUND;
    private String XMSBRHLICENNO;
    private String XMSBRHDRIVER;
    private String XMSBRHFOLLOWER;
    private String XMSBRHWH1;
    private String XMSBRHWH2;
    private String XMSBRHWH3;
    private String XMSBRHTOTWH;
    private String XMSBRHUSER;
    private String options;
    private String whn;
    private String destn;
    private String XMSBRHDOCNO;

    public XMSBRHEAD() {

    }

    public XMSBRHEAD(String XMSBRHQNO, String XMSBRHSPDT, String XMSBRHDEST, String XMSBRHROUND, String XMSBRHLICENNO, String XMSBRHDRIVER, String XMSBRHFOLLOWER, String XMSBRHWH1, String XMSBRHWH2, String XMSBRHWH3, String XMSBRHTOTWH, String XMSBRHUSER) {
        this.XMSBRHQNO = XMSBRHQNO;
        this.XMSBRHSPDT = XMSBRHSPDT;
        this.XMSBRHDEST = XMSBRHDEST;
        this.XMSBRHROUND = XMSBRHROUND;
        this.XMSBRHLICENNO = XMSBRHLICENNO;
        this.XMSBRHDRIVER = XMSBRHDRIVER;
        this.XMSBRHFOLLOWER = XMSBRHFOLLOWER;
        this.XMSBRHWH1 = XMSBRHWH1;
        this.XMSBRHWH2 = XMSBRHWH2;
        this.XMSBRHWH3 = XMSBRHWH3;
        this.XMSBRHTOTWH = XMSBRHTOTWH;
        this.XMSBRHUSER = XMSBRHUSER;
    }

    public String getXMSBRHQNO() {
        return XMSBRHQNO;
    }

    public void setXMSBRHQNO(String XMSBRHQNO) {
        this.XMSBRHQNO = XMSBRHQNO;
    }

    public String getXMSBRHSPDT() {
        return XMSBRHSPDT;
    }

    public void setXMSBRHSPDT(String XMSBRHSPDT) {
        this.XMSBRHSPDT = XMSBRHSPDT;
    }

    public String getXMSBRHDEST() {
        return XMSBRHDEST;
    }

    public void setXMSBRHDEST(String XMSBRHDEST) {
        this.XMSBRHDEST = XMSBRHDEST;
    }

    public String getXMSBRHROUND() {
        return XMSBRHROUND;
    }

    public void setXMSBRHROUND(String XMSBRHROUND) {
        this.XMSBRHROUND = XMSBRHROUND;
    }

    public String getXMSBRHLICENNO() {
        return XMSBRHLICENNO;
    }

    public void setXMSBRHLICENNO(String XMSBRHLICENNO) {
        this.XMSBRHLICENNO = XMSBRHLICENNO;
    }

    public String getXMSBRHDRIVER() {
        return XMSBRHDRIVER;
    }

    public void setXMSBRHDRIVER(String XMSBRHDRIVER) {
        this.XMSBRHDRIVER = XMSBRHDRIVER;
    }

    public String getXMSBRHFOLLOWER() {
        return XMSBRHFOLLOWER;
    }

    public void setXMSBRHFOLLOWER(String XMSBRHFOLLOWER) {
        this.XMSBRHFOLLOWER = XMSBRHFOLLOWER;
    }

    public String getXMSBRHWH1() {
        return XMSBRHWH1;
    }

    public void setXMSBRHWH1(String XMSBRHWH1) {
        this.XMSBRHWH1 = XMSBRHWH1;
    }

    public String getXMSBRHWH2() {
        return XMSBRHWH2;
    }

    public void setXMSBRHWH2(String XMSBRHWH2) {
        this.XMSBRHWH2 = XMSBRHWH2;
    }

    public String getXMSBRHWH3() {
        return XMSBRHWH3;
    }

    public void setXMSBRHWH3(String XMSBRHWH3) {
        this.XMSBRHWH3 = XMSBRHWH3;
    }

    public String getXMSBRHTOTWH() {
        return XMSBRHTOTWH;
    }

    public void setXMSBRHTOTWH(String XMSBRHTOTWH) {
        this.XMSBRHTOTWH = XMSBRHTOTWH;
    }

    public String getXMSBRHUSER() {
        return XMSBRHUSER;
    }

    public void setXMSBRHUSER(String XMSBRHUSER) {
        this.XMSBRHUSER = XMSBRHUSER;
    }

    public String getOptions() {
        return options;
    }

    public void setOptions(String options) {
        this.options = options;
    }

    public String getWhn() {
        return whn;
    }

    public void setWhn(String whn) {
        this.whn = whn;
    }

    public String getDestn() {
        return destn;
    }

    public void setDestn(String destn) {
        this.destn = destn;
    }

    public String getXMSBRHDOCNO() {
        return XMSBRHDOCNO;
    }

    public void setXMSBRHDOCNO(String XMSBRHDOCNO) {
        this.XMSBRHDOCNO = XMSBRHDOCNO;
    }
}
