/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.entity;

/**
 *
 * @author nutthawoot.noo
 */
public class QDMBAGHED {

    private String QDMBSTYLE;
    private String QDMBDESC;
    private String QDMBUNIT;
    private String QDMBEQUA;
    private String QDMBX;
    private String QDMBY;
    private String QTY;

    public QDMBAGHED() {

    }

    @Override
    public String toString() {
        return "QDMBAGHED{" + "QDMBSTYLE=" + QDMBSTYLE + ", QDMBDESC=" + QDMBDESC + ", QDMBUNIT=" + QDMBUNIT + ", QDMBEQUA=" + QDMBEQUA + ", QDMBX=" + QDMBX + ", QDMBY=" + QDMBY + ", QTY=" + QTY + '}';
    }

    public String getQTY() {
        return QTY;
    }

    public void setQTY(String QTY) {
        this.QTY = QTY;
    }

    public String getQDMBSTYLE() {
        return QDMBSTYLE;
    }

    public void setQDMBSTYLE(String QDMBSTYLE) {
        this.QDMBSTYLE = QDMBSTYLE;
    }

    public String getQDMBDESC() {
        return QDMBDESC;
    }

    public void setQDMBDESC(String QDMBDESC) {
        this.QDMBDESC = QDMBDESC;
    }

    public String getQDMBUNIT() {
        return QDMBUNIT;
    }

    public void setQDMBUNIT(String QDMBUNIT) {
        this.QDMBUNIT = QDMBUNIT;
    }

    public String getQDMBEQUA() {
        return QDMBEQUA;
    }

    public void setQDMBEQUA(String QDMBEQUA) {
        this.QDMBEQUA = QDMBEQUA;
    }

    public String getQDMBX() {
        return QDMBX;
    }

    public void setQDMBX(String QDMBX) {
        this.QDMBX = QDMBX;
    }

    public String getQDMBY() {
        return QDMBY;
    }

    public void setQDMBY(String QDMBY) {
        this.QDMBY = QDMBY;
    }

}
