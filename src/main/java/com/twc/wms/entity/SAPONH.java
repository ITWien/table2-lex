/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.entity;

/**
 *
 * @author nutthawoot.noo
 */
public class SAPONH {

    private String SAPTRDT;
    private String ONHANDDATE;

    public SAPONH() {

    }

    public String getSAPTRDT() {
        return SAPTRDT;
    }

    public void setSAPTRDT(String SAPTRDT) {
        this.SAPTRDT = SAPTRDT;
    }

    public String getONHANDDATE() {
        return ONHANDDATE;
    }

    public void setONHANDDATE(String ONHANDDATE) {
        this.ONHANDDATE = ONHANDDATE;
    }

}
