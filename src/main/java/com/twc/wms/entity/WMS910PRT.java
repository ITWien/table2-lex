/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.entity;

import java.util.ArrayList;
import com.twc.wms.entity.QIDETAIL;
import java.util.List;

/**
 *
 * @author nutthawoot.noo
 */
public class WMS910PRT {

    private String NO;
    private String SAPWHS;
    private String SAPWHSDESC;
    private String SAPMTCTRL;
    private String SAPMCTRLDESC;
    private String SAPPLANT;
    private String SAPLOCATION;
    private String SAPMAT;
    private String SAPDESC;
    private String SAPVAL;
    private String SAPBUN;
    private String SAPPUNRES;
    private String SAPQI;
    private String SAPAVG;
    private String SAPAMOUNT;
    private String SAPTOTAL;
    private String QCKSUM;
    private String DIFFMISS;
    private String DIFFOVER;
    private String AMTMISS;
    private String AMTOVER;
    private String WMS_AMT;
    private String QI;
    private String PACKPCS;
    private String PACKPCS_COR;
    private String ThisRedLine;

    public WMS910PRT() {

    }

    public String getNO() {
        return NO;
    }

    public void setNO(String NO) {
        this.NO = NO;
    }

    public String getSAPWHS() {
        return SAPWHS;
    }

    public void setSAPWHS(String SAPWHS) {
        this.SAPWHS = SAPWHS;
    }

    public String getSAPWHSDESC() {
        return SAPWHSDESC;
    }

    public void setSAPWHSDESC(String SAPWHSDESC) {
        this.SAPWHSDESC = SAPWHSDESC;
    }

    public String getSAPMTCTRL() {
        return SAPMTCTRL;
    }

    public void setSAPMTCTRL(String SAPMTCTRL) {
        this.SAPMTCTRL = SAPMTCTRL;
    }

    public String getSAPMCTRLDESC() {
        return SAPMCTRLDESC;
    }

    public void setSAPMCTRLDESC(String SAPMCTRLDESC) {
        this.SAPMCTRLDESC = SAPMCTRLDESC;
    }

    public String getSAPPLANT() {
        return SAPPLANT;
    }

    public void setSAPPLANT(String SAPPLANT) {
        this.SAPPLANT = SAPPLANT;
    }

    public String getSAPLOCATION() {
        return SAPLOCATION;
    }

    public void setSAPLOCATION(String SAPLOCATION) {
        this.SAPLOCATION = SAPLOCATION;
    }

    public String getSAPMAT() {
        return SAPMAT;
    }

    public void setSAPMAT(String SAPMAT) {
        this.SAPMAT = SAPMAT;
    }

    public String getSAPDESC() {
        return SAPDESC;
    }

    public void setSAPDESC(String SAPDESC) {
        this.SAPDESC = SAPDESC;
    }

    public String getSAPVAL() {
        return SAPVAL;
    }

    public void setSAPVAL(String SAPVAL) {
        this.SAPVAL = SAPVAL;
    }

    public String getSAPBUN() {
        return SAPBUN;
    }

    public void setSAPBUN(String SAPBUN) {
        this.SAPBUN = SAPBUN;
    }

    public String getSAPPUNRES() {
        return SAPPUNRES;
    }

    public void setSAPPUNRES(String SAPPUNRES) {
        this.SAPPUNRES = SAPPUNRES;
    }

    public String getSAPQI() {
        return SAPQI;
    }

    public void setSAPQI(String SAPQI) {
        this.SAPQI = SAPQI;
    }

    public String getSAPAVG() {
        return SAPAVG;
    }

    public void setSAPAVG(String SAPAVG) {
        this.SAPAVG = SAPAVG;
    }

    public String getSAPAMOUNT() {
        return SAPAMOUNT;
    }

    public void setSAPAMOUNT(String SAPAMOUNT) {
        this.SAPAMOUNT = SAPAMOUNT;
    }

    public String getSAPTOTAL() {
        return SAPTOTAL;
    }

    public void setSAPTOTAL(String SAPTOTAL) {
        this.SAPTOTAL = SAPTOTAL;
    }

    public String getQCKSUM() {
        return QCKSUM;
    }

    public void setQCKSUM(String QCKSUM) {
        this.QCKSUM = QCKSUM;
    }

    public String getDIFFMISS() {
        return DIFFMISS;
    }

    public void setDIFFMISS(String DIFFMISS) {
        this.DIFFMISS = DIFFMISS;
    }

    public String getDIFFOVER() {
        return DIFFOVER;
    }

    public void setDIFFOVER(String DIFFOVER) {
        this.DIFFOVER = DIFFOVER;
    }

    public String getAMTMISS() {
        return AMTMISS;
    }

    public void setAMTMISS(String AMTMISS) {
        this.AMTMISS = AMTMISS;
    }

    public String getAMTOVER() {
        return AMTOVER;
    }

    public void setAMTOVER(String AMTOVER) {
        this.AMTOVER = AMTOVER;
    }

    public String getWMS_AMT() {
        return WMS_AMT;
    }

    public void setWMS_AMT(String WMS_AMT) {
        this.WMS_AMT = WMS_AMT;
    }

    public String getQI() {
        return QI;
    }

    public void setQI(String QI) {
        this.QI = QI;
    }

    public String getPACKPCS() {
        return PACKPCS;
    }

    public void setPACKPCS(String PACKPCS) {
        this.PACKPCS = PACKPCS;
    }

    public String getPACKPCS_COR() {
        return PACKPCS_COR;
    }

    public void setPACKPCS_COR(String PACKPCS_COR) {
        this.PACKPCS_COR = PACKPCS_COR;
    }

    public String getThisRedLine() {
        return ThisRedLine;
    }

    public void setThisRedLine(String ThisRedLine) {
        this.ThisRedLine = ThisRedLine;
    }

}
