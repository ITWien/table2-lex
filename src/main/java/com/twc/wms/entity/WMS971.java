/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.entity;

import java.util.ArrayList;
import com.twc.wms.entity.QIDETAIL;
import java.util.List;

/**
 *
 * @author nutthawoot.noo
 */
public class WMS971 {

    private String CODE;
    private String BAG;
    private String ROLL;
    private String BOX;
    private String QTY;
    private String ID;
    private String LOCATION;
    private String UNIT;
    private String DESC;
    private String GRPDESC;
    private String PLANT;
    private String VAL;
    private String STS;
    private String LCUPDT;

    public WMS971() {

    }

    public String getLCUPDT() {
        return LCUPDT;
    }

    public void setLCUPDT(String LCUPDT) {
        this.LCUPDT = LCUPDT;
    }

    public String getCODE() {
        return CODE;
    }

    public void setCODE(String CODE) {
        this.CODE = CODE;
    }

    public String getBAG() {
        return BAG;
    }

    public void setBAG(String BAG) {
        this.BAG = BAG;
    }

    public String getROLL() {
        return ROLL;
    }

    public void setROLL(String ROLL) {
        this.ROLL = ROLL;
    }

    public String getBOX() {
        return BOX;
    }

    public void setBOX(String BOX) {
        this.BOX = BOX;
    }

    public String getQTY() {
        return QTY;
    }

    public void setQTY(String QTY) {
        this.QTY = QTY;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getLOCATION() {
        return LOCATION;
    }

    public void setLOCATION(String LOCATION) {
        this.LOCATION = LOCATION;
    }

    public String getUNIT() {
        return UNIT;
    }

    public void setUNIT(String UNIT) {
        this.UNIT = UNIT;
    }

    public String getDESC() {
        return DESC;
    }

    public void setDESC(String DESCS) {
        this.DESC = DESCS;
    }

    public String getGRPDESC() {
        return GRPDESC;
    }

    public void setGRPDESC(String GRPDESC) {
        this.GRPDESC = GRPDESC;
    }

    public String getPLANT() {
        return PLANT;
    }

    public void setPLANT(String PLANT) {
        this.PLANT = PLANT;
    }

    public String getVAL() {
        return VAL;
    }

    public void setVAL(String VAL) {
        this.VAL = VAL;
    }

    public String getSTS() {
        return STS;
    }

    public void setSTS(String STS) {
        this.STS = STS;
    }
}
