/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.twc.wms.entity;

/**
 *
 * @author 93176
 */
public class ISMBILLP {

    private String ISPCONO;
    private String VBELN;
    private String POSNR;
    private String PARVW;
    private String KUNNR;
    private String ADRNR;
    private String NAME1;
    private String NAME2;
    private String NAME3;
    private String NAME4;
    private String CITY1;
    private String CITY2;
    private String POST_CODE1;
    private String STREET;
    private String COUNTRY;
    private String LANGU;
    private String REGION;
    private String SORT1;
    private String TEL_NUMBER;
    private String TEL_EXTENS;
    private String FAX_NUMBER;
    private String FAX_EXTENS;
    private String EXTENSION1;
    private String EXTENSION2;
    private String ISPEDT;
    private String ISPETM;
    private String ISPCDT;
    private String ISPCTM;
    private String ISPUSER;

    public ISMBILLP() {
    }

    public ISMBILLP(String ISPCONO, String VBELN, String POSNR, String PARVW, String KUNNR, String ADRNR, String NAME1, String NAME2, String NAME3, String NAME4, String CITY1, String CITY2, String POST_CODE1, String STREET, String COUNTRY, String LANGU, String REGION, String SORT1, String TEL_NUMBER, String TEL_EXTENS, String FAX_NUMBER, String FAX_EXTENS, String EXTENSION1, String EXTENSION2, String ISPEDT, String ISPETM, String ISPCDT, String ISPCTM, String ISPUSER) {
        this.ISPCONO = ISPCONO;
        this.VBELN = VBELN;
        this.POSNR = POSNR;
        this.PARVW = PARVW;
        this.KUNNR = KUNNR;
        this.ADRNR = ADRNR;
        this.NAME1 = NAME1;
        this.NAME2 = NAME2;
        this.NAME3 = NAME3;
        this.NAME4 = NAME4;
        this.CITY1 = CITY1;
        this.CITY2 = CITY2;
        this.POST_CODE1 = POST_CODE1;
        this.STREET = STREET;
        this.COUNTRY = COUNTRY;
        this.LANGU = LANGU;
        this.REGION = REGION;
        this.SORT1 = SORT1;
        this.TEL_NUMBER = TEL_NUMBER;
        this.TEL_EXTENS = TEL_EXTENS;
        this.FAX_NUMBER = FAX_NUMBER;
        this.FAX_EXTENS = FAX_EXTENS;
        this.EXTENSION1 = EXTENSION1;
        this.EXTENSION2 = EXTENSION2;
        this.ISPEDT = ISPEDT;
        this.ISPETM = ISPETM;
        this.ISPCDT = ISPCDT;
        this.ISPCTM = ISPCTM;
        this.ISPUSER = ISPUSER;
    }

    public String getISPCONO() {
        return ISPCONO;
    }

    public void setISPCONO(String ISPCONO) {
        this.ISPCONO = ISPCONO;
    }

    public String getVBELN() {
        return VBELN;
    }

    public void setVBELN(String VBELN) {
        this.VBELN = VBELN;
    }

    public String getPOSNR() {
        return POSNR;
    }

    public void setPOSNR(String POSNR) {
        this.POSNR = POSNR;
    }

    public String getPARVW() {
        return PARVW;
    }

    public void setPARVW(String PARVW) {
        this.PARVW = PARVW;
    }

    public String getKUNNR() {
        return KUNNR;
    }

    public void setKUNNR(String KUNNR) {
        this.KUNNR = KUNNR;
    }

    public String getADRNR() {
        return ADRNR;
    }

    public void setADRNR(String ADRNR) {
        this.ADRNR = ADRNR;
    }

    public String getNAME1() {
        return NAME1;
    }

    public void setNAME1(String NAME1) {
        this.NAME1 = NAME1;
    }

    public String getNAME2() {
        return NAME2;
    }

    public void setNAME2(String NAME2) {
        this.NAME2 = NAME2;
    }

    public String getNAME3() {
        return NAME3;
    }

    public void setNAME3(String NAME3) {
        this.NAME3 = NAME3;
    }

    public String getNAME4() {
        return NAME4;
    }

    public void setNAME4(String NAME4) {
        this.NAME4 = NAME4;
    }

    public String getCITY1() {
        return CITY1;
    }

    public void setCITY1(String CITY1) {
        this.CITY1 = CITY1;
    }

    public String getCITY2() {
        return CITY2;
    }

    public void setCITY2(String CITY2) {
        this.CITY2 = CITY2;
    }

    public String getPOST_CODE1() {
        return POST_CODE1;
    }

    public void setPOST_CODE1(String POST_CODE1) {
        this.POST_CODE1 = POST_CODE1;
    }

    public String getSTREET() {
        return STREET;
    }

    public void setSTREET(String STREET) {
        this.STREET = STREET;
    }

    public String getCOUNTRY() {
        return COUNTRY;
    }

    public void setCOUNTRY(String COUNTRY) {
        this.COUNTRY = COUNTRY;
    }

    public String getLANGU() {
        return LANGU;
    }

    public void setLANGU(String LANGU) {
        this.LANGU = LANGU;
    }

    public String getREGION() {
        return REGION;
    }

    public void setREGION(String REGION) {
        this.REGION = REGION;
    }

    public String getSORT1() {
        return SORT1;
    }

    public void setSORT1(String SORT1) {
        this.SORT1 = SORT1;
    }

    public String getTEL_NUMBER() {
        return TEL_NUMBER;
    }

    public void setTEL_NUMBER(String TEL_NUMBER) {
        this.TEL_NUMBER = TEL_NUMBER;
    }

    public String getTEL_EXTENS() {
        return TEL_EXTENS;
    }

    public void setTEL_EXTENS(String TEL_EXTENS) {
        this.TEL_EXTENS = TEL_EXTENS;
    }

    public String getFAX_NUMBER() {
        return FAX_NUMBER;
    }

    public void setFAX_NUMBER(String FAX_NUMBER) {
        this.FAX_NUMBER = FAX_NUMBER;
    }

    public String getFAX_EXTENS() {
        return FAX_EXTENS;
    }

    public void setFAX_EXTENS(String FAX_EXTENS) {
        this.FAX_EXTENS = FAX_EXTENS;
    }

    public String getEXTENSION1() {
        return EXTENSION1;
    }

    public void setEXTENSION1(String EXTENSION1) {
        this.EXTENSION1 = EXTENSION1;
    }

    public String getEXTENSION2() {
        return EXTENSION2;
    }

    public void setEXTENSION2(String EXTENSION2) {
        this.EXTENSION2 = EXTENSION2;
    }

    public String getISPEDT() {
        return ISPEDT;
    }

    public void setISPEDT(String ISPEDT) {
        this.ISPEDT = ISPEDT;
    }

    public String getISPETM() {
        return ISPETM;
    }

    public void setISPETM(String ISPETM) {
        this.ISPETM = ISPETM;
    }

    public String getISPCDT() {
        return ISPCDT;
    }

    public void setISPCDT(String ISPCDT) {
        this.ISPCDT = ISPCDT;
    }

    public String getISPCTM() {
        return ISPCTM;
    }

    public void setISPCTM(String ISPCTM) {
        this.ISPCTM = ISPCTM;
    }

    public String getISPUSER() {
        return ISPUSER;
    }

    public void setISPUSER(String ISPUSER) {
        this.ISPUSER = ISPUSER;
    }

}
