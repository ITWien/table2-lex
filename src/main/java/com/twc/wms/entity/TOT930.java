/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.entity;

/**
 *
 * @author nutthawoot.noo
 */
public class TOT930 {

    private String TWHSE;
    private String TMTCL;
    private String SKU_1000;
    private String AMT_1000;
    private String SKU_1050;
    private String AMT_1050;
    private String SKU_TOT;
    private String AMT_TOT;
    private String T1000_SKU;
    private String T1000_AMT;
    private String T1050_SKU;
    private String T1050_AMT;
    private String TTOT_SKU;
    private String TTOT_AMT;
    private String GT1000_SKU;
    private String GT1000_AMT;
    private String GT1050_SKU;
    private String GT1050_AMT;
    private String GTTOT_SKU;
    private String GTTOT_AMT;

    public TOT930() {

    }

    public String getTWHSE() {
        return TWHSE;
    }

    public void setTWHSE(String TWHSE) {
        this.TWHSE = TWHSE;
    }

    public String getTMTCL() {
        return TMTCL;
    }

    public void setTMTCL(String TMTCL) {
        this.TMTCL = TMTCL;
    }

    public String getSKU_1000() {
        return SKU_1000;
    }

    public void setSKU_1000(String SKU_1000) {
        this.SKU_1000 = SKU_1000;
    }

    public String getAMT_1000() {
        return AMT_1000;
    }

    public void setAMT_1000(String AMT_1000) {
        this.AMT_1000 = AMT_1000;
    }

    public String getSKU_1050() {
        return SKU_1050;
    }

    public void setSKU_1050(String SKU_1050) {
        this.SKU_1050 = SKU_1050;
    }

    public String getAMT_1050() {
        return AMT_1050;
    }

    public void setAMT_1050(String AMT_1050) {
        this.AMT_1050 = AMT_1050;
    }

    public String getSKU_TOT() {
        return SKU_TOT;
    }

    public void setSKU_TOT(String SKU_TOT) {
        this.SKU_TOT = SKU_TOT;
    }

    public String getAMT_TOT() {
        return AMT_TOT;
    }

    public void setAMT_TOT(String AMT_TOT) {
        this.AMT_TOT = AMT_TOT;
    }

    public String getT1000_SKU() {
        return T1000_SKU;
    }

    public void setT1000_SKU(String T1000_SKU) {
        this.T1000_SKU = T1000_SKU;
    }

    public String getT1000_AMT() {
        return T1000_AMT;
    }

    public void setT1000_AMT(String T1000_AMT) {
        this.T1000_AMT = T1000_AMT;
    }

    public String getT1050_SKU() {
        return T1050_SKU;
    }

    public void setT1050_SKU(String T1050_SKU) {
        this.T1050_SKU = T1050_SKU;
    }

    public String getT1050_AMT() {
        return T1050_AMT;
    }

    public void setT1050_AMT(String T1050_AMT) {
        this.T1050_AMT = T1050_AMT;
    }

    public String getTTOT_SKU() {
        return TTOT_SKU;
    }

    public void setTTOT_SKU(String TTOT_SKU) {
        this.TTOT_SKU = TTOT_SKU;
    }

    public String getTTOT_AMT() {
        return TTOT_AMT;
    }

    public void setTTOT_AMT(String TTOT_AMT) {
        this.TTOT_AMT = TTOT_AMT;
    }

    public String getGT1000_SKU() {
        return GT1000_SKU;
    }

    public void setGT1000_SKU(String GT1000_SKU) {
        this.GT1000_SKU = GT1000_SKU;
    }

    public String getGT1000_AMT() {
        return GT1000_AMT;
    }

    public void setGT1000_AMT(String GT1000_AMT) {
        this.GT1000_AMT = GT1000_AMT;
    }

    public String getGT1050_SKU() {
        return GT1050_SKU;
    }

    public void setGT1050_SKU(String GT1050_SKU) {
        this.GT1050_SKU = GT1050_SKU;
    }

    public String getGT1050_AMT() {
        return GT1050_AMT;
    }

    public void setGT1050_AMT(String GT1050_AMT) {
        this.GT1050_AMT = GT1050_AMT;
    }

    public String getGTTOT_SKU() {
        return GTTOT_SKU;
    }

    public void setGTTOT_SKU(String GTTOT_SKU) {
        this.GTTOT_SKU = GTTOT_SKU;
    }

    public String getGTTOT_AMT() {
        return GTTOT_AMT;
    }

    public void setGTTOT_AMT(String GTTOT_AMT) {
        this.GTTOT_AMT = GTTOT_AMT;
    }

}
