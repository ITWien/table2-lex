/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.twc.wms.entity;

/**
 *
 * @author 93176
 */
public class ISMPACD {

    private String GRPNO;
    private int ISSUENO;
    private String VBELN;
    private String POSNR;
    private String MATNR;
    private int NO;
    private String TYPE;
    private Double VALUE;

    public ISMPACD() {
    }

    public ISMPACD(String GRPNO, int ISSUENO, String VBELN, String POSNR, String MATNR, int NO, String TYPE, Double VALUE) {
        this.GRPNO = GRPNO;
        this.ISSUENO = ISSUENO;
        this.VBELN = VBELN;
        this.POSNR = POSNR;
        this.MATNR = MATNR;
        this.NO = NO;
        this.TYPE = TYPE;
        this.VALUE = VALUE;
    }

    public String getGRPNO() {
        return GRPNO;
    }

    public void setGRPNO(String GRPNO) {
        this.GRPNO = GRPNO;
    }

    public int getISSUENO() {
        return ISSUENO;
    }

    public void setISSUENO(int ISSUENO) {
        this.ISSUENO = ISSUENO;
    }

    public String getVBELN() {
        return VBELN;
    }

    public void setVBELN(String VBELN) {
        this.VBELN = VBELN;
    }

    public String getPOSNR() {
        return POSNR;
    }

    public void setPOSNR(String POSNR) {
        this.POSNR = POSNR;
    }

    public String getMATNR() {
        return MATNR;
    }

    public void setMATNR(String MATNR) {
        this.MATNR = MATNR;
    }

    public int getNO() {
        return NO;
    }

    public void setNO(int NO) {
        this.NO = NO;
    }

    public String getTYPE() {
        return TYPE;
    }

    public void setTYPE(String TYPE) {
        this.TYPE = TYPE;
    }

    public Double getVALUE() {
        return VALUE;
    }

    public void setVALUE(Double VALUE) {
        this.VALUE = VALUE;
    }

}
