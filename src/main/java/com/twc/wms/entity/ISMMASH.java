/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.entity;

/**
 *
 * @author nutthawoot.noo
 */
public class ISMMASH {

    private String ISHCONO;
    private String ISHMVT;
    private String ISHTRDT;
    private String ISHUART;
    private String ISHTWEG;
    private String ISHORD;
    private String ISHCUNO;
    private String ISHCUNM1;
    private String ISHCUNM2;
    private String ISHCUAD1;
    private String ISHCUAD2;
    private String ISHBSTKD;
    private String ISHSUBMI;
    private String ISHMATLOT;
    private String ISHTAXNO;
    private String ISHBRANCH01;
    private String ISHDATUM;
    private String ISHMATCODE;
    private String ISHSTYLE;
    private String ISHSTYLE2;
    private String ISHCOLOR;
    private String ISHGRPNO;
    private String ISHPGRP;
    private String ISHLOT;
    private String ISHAMTFG;
    private String LSTSI;
    private String HSTSI;
    private String LSTS;
    private String HSTS;
    private String ISHLSTS;
    private String ISHHSTS;
    private String ISHRQDT;
    private String ISHDEST;
    private String ISHAPDT;
    private String ISHAPUSR;
    private String ISHUSER;
    private String ISHREMARK;
    private String ISHJBNO;
    private String ISHRCDT;
    private String DEST;
    private String DESTNAME;
    private String ORDER;
    private String PCS;
    private String SKU;
    private String NEARDATE;
    private String STYLE_LOT_ID;
    private String STYLE_LOT_TEXT;
    private String ISHPOFG;
    private String ISHPKDT;
    private String ISHTPDT;
    private String ISHEUSR;
    private String ISHWHNO;
    private String ISHEDT;
    private String ISHSEQ;
    private String ISHCNTTOT;
    private String ISHP1DT;
    private String ISHA1DT;
    private String ISHT1DT;
    private String ISHNOR;
    private String ISHCDT;
    private String ISHCTM;
    private String ISHRQTM;
    private String ISHAPTM;
    private String ISHRCTM;
    private String ISHRCUSR;
    private String ISHPKTM;
    private String ISHPKUSR;
    private String ISHTPTM;
    private String ISHTPUSR;
    private String ISHETM;
    private String ISHRPUSR;
    private String ISHREASON;

    public ISMMASH() {
    }

    public String getISHCONO() {
        return ISHCONO;
    }

    public void setISHCONO(String ISHCONO) {
        this.ISHCONO = ISHCONO;
    }

    public String getISHMVT() {
        return ISHMVT;
    }

    public void setISHMVT(String ISHMVT) {
        this.ISHMVT = ISHMVT;
    }

    public String getISHTRDT() {
        return ISHTRDT;
    }

    public void setISHTRDT(String ISHTRDT) {
        this.ISHTRDT = ISHTRDT;
    }

    public String getISHUART() {
        return ISHUART;
    }

    public void setISHUART(String ISHUART) {
        this.ISHUART = ISHUART;
    }

    public String getISHTWEG() {
        return ISHTWEG;
    }

    public void setISHTWEG(String ISHTWEG) {
        this.ISHTWEG = ISHTWEG;
    }

    public String getISHORD() {
        return ISHORD;
    }

    public void setISHORD(String ISHORD) {
        this.ISHORD = ISHORD;
    }

    public String getISHCUNO() {
        return ISHCUNO;
    }

    public void setISHCUNO(String ISHCUNO) {
        this.ISHCUNO = ISHCUNO;
    }

    public String getISHCUNM1() {
        return ISHCUNM1;
    }

    public void setISHCUNM1(String ISHCUNM1) {
        this.ISHCUNM1 = ISHCUNM1;
    }

    public String getISHCUNM2() {
        return ISHCUNM2;
    }

    public void setISHCUNM2(String ISHCUNM2) {
        this.ISHCUNM2 = ISHCUNM2;
    }

    public String getISHCUAD1() {
        return ISHCUAD1;
    }

    public void setISHCUAD1(String ISHCUAD1) {
        this.ISHCUAD1 = ISHCUAD1;
    }

    public String getISHCUAD2() {
        return ISHCUAD2;
    }

    public void setISHCUAD2(String ISHCUAD2) {
        this.ISHCUAD2 = ISHCUAD2;
    }

    public String getISHBSTKD() {
        return ISHBSTKD;
    }

    public void setISHBSTKD(String ISHBSTKD) {
        this.ISHBSTKD = ISHBSTKD;
    }

    public String getISHSUBMI() {
        return ISHSUBMI;
    }

    public void setISHSUBMI(String ISHSUBMI) {
        this.ISHSUBMI = ISHSUBMI;
    }

    public String getISHMATLOT() {
        return ISHMATLOT;
    }

    public void setISHMATLOT(String ISHMATLOT) {
        this.ISHMATLOT = ISHMATLOT;
    }

    public String getISHTAXNO() {
        return ISHTAXNO;
    }

    public void setISHTAXNO(String ISHTAXNO) {
        this.ISHTAXNO = ISHTAXNO;
    }

    public String getISHBRANCH01() {
        return ISHBRANCH01;
    }

    public void setISHBRANCH01(String ISHBRANCH01) {
        this.ISHBRANCH01 = ISHBRANCH01;
    }

    public String getISHDATUM() {
        return ISHDATUM;
    }

    public void setISHDATUM(String ISHDATUM) {
        this.ISHDATUM = ISHDATUM;
    }

    public String getISHMATCODE() {
        return ISHMATCODE;
    }

    public void setISHMATCODE(String ISHMATCODE) {
        this.ISHMATCODE = ISHMATCODE;
    }

    public String getISHSTYLE() {
        return ISHSTYLE;
    }

    public void setISHSTYLE(String ISHSTYLE) {
        this.ISHSTYLE = ISHSTYLE;
    }

    public String getISHSTYLE2() {
        return ISHSTYLE2;
    }

    public void setISHSTYLE2(String ISHSTYLE2) {
        this.ISHSTYLE2 = ISHSTYLE2;
    }

    public String getISHCOLOR() {
        return ISHCOLOR;
    }

    public void setISHCOLOR(String ISHCOLOR) {
        this.ISHCOLOR = ISHCOLOR;
    }

    public String getISHGRPNO() {
        return ISHGRPNO;
    }

    public void setISHGRPNO(String ISHGRPNO) {
        this.ISHGRPNO = ISHGRPNO;
    }

    public String getISHPGRP() {
        return ISHPGRP;
    }

    public void setISHPGRP(String ISHPGRP) {
        this.ISHPGRP = ISHPGRP;
    }

    public String getISHLOT() {
        return ISHLOT;
    }

    public void setISHLOT(String ISHLOT) {
        this.ISHLOT = ISHLOT;
    }

    public String getISHAMTFG() {
        return ISHAMTFG;
    }

    public void setISHAMTFG(String ISHAMTFG) {
        this.ISHAMTFG = ISHAMTFG;
    }

    public String getLSTSI() {
        return LSTSI;
    }

    public void setLSTSI(String LSTSI) {
        this.LSTSI = LSTSI;
    }

    public String getHSTSI() {
        return HSTSI;
    }

    public void setHSTSI(String HSTSI) {
        this.HSTSI = HSTSI;
    }

    public String getLSTS() {
        return LSTS;
    }

    public void setLSTS(String LSTS) {
        this.LSTS = LSTS;
    }

    public String getHSTS() {
        return HSTS;
    }

    public void setHSTS(String HSTS) {
        this.HSTS = HSTS;
    }

    public String getISHLSTS() {
        return ISHLSTS;
    }

    public void setISHLSTS(String ISHLSTS) {
        this.ISHLSTS = ISHLSTS;
    }

    public String getISHHSTS() {
        return ISHHSTS;
    }

    public void setISHHSTS(String ISHHSTS) {
        this.ISHHSTS = ISHHSTS;
    }

    public String getISHRQDT() {
        return ISHRQDT;
    }

    public void setISHRQDT(String ISHRQDT) {
        this.ISHRQDT = ISHRQDT;
    }

    public String getISHDEST() {
        return ISHDEST;
    }

    public void setISHDEST(String ISHDEST) {
        this.ISHDEST = ISHDEST;
    }

    public String getISHAPDT() {
        return ISHAPDT;
    }

    public void setISHAPDT(String ISHAPDT) {
        this.ISHAPDT = ISHAPDT;
    }

    public String getISHAPUSR() {
        return ISHAPUSR;
    }

    public void setISHAPUSR(String ISHAPUSR) {
        this.ISHAPUSR = ISHAPUSR;
    }

    public String getISHUSER() {
        return ISHUSER;
    }

    public void setISHUSER(String ISHUSER) {
        this.ISHUSER = ISHUSER;
    }

    public String getISHREMARK() {
        return ISHREMARK;
    }

    public void setISHREMARK(String ISHREMARK) {
        this.ISHREMARK = ISHREMARK;
    }

    public String getISHJBNO() {
        return ISHJBNO;
    }

    public void setISHJBNO(String ISHJBNO) {
        this.ISHJBNO = ISHJBNO;
    }

    public String getISHRCDT() {
        return ISHRCDT;
    }

    public void setISHRCDT(String ISHRCDT) {
        this.ISHRCDT = ISHRCDT;
    }

    public String getDEST() {
        return DEST;
    }

    public void setDEST(String DEST) {
        this.DEST = DEST;
    }

    public String getDESTNAME() {
        return DESTNAME;
    }

    public void setDESTNAME(String DESTNAME) {
        this.DESTNAME = DESTNAME;
    }

    public String getORDER() {
        return ORDER;
    }

    public void setORDER(String ORDER) {
        this.ORDER = ORDER;
    }

    public String getPCS() {
        return PCS;
    }

    public void setPCS(String PCS) {
        this.PCS = PCS;
    }

    public String getSKU() {
        return SKU;
    }

    public void setSKU(String SKU) {
        this.SKU = SKU;
    }

    public String getNEARDATE() {
        return NEARDATE;
    }

    public void setNEARDATE(String NEARDATE) {
        this.NEARDATE = NEARDATE;
    }

    public String getSTYLE_LOT_ID() {
        return STYLE_LOT_ID;
    }

    public void setSTYLE_LOT_ID(String STYLE_LOT_ID) {
        this.STYLE_LOT_ID = STYLE_LOT_ID;
    }

    public String getSTYLE_LOT_TEXT() {
        return STYLE_LOT_TEXT;
    }

    public void setSTYLE_LOT_TEXT(String STYLE_LOT_TEXT) {
        this.STYLE_LOT_TEXT = STYLE_LOT_TEXT;
    }

    public String getISHPOFG() {
        return ISHPOFG;
    }

    public void setISHPOFG(String ISHPOFG) {
        this.ISHPOFG = ISHPOFG;
    }

    public String getISHPKDT() {
        return ISHPKDT;
    }

    public void setISHPKDT(String ISHPKDT) {
        this.ISHPKDT = ISHPKDT;
    }

    public String getISHTPDT() {
        return ISHTPDT;
    }

    public void setISHTPDT(String ISHTPDT) {
        this.ISHTPDT = ISHTPDT;
    }

    public String getISHEUSR() {
        return ISHEUSR;
    }

    public void setISHEUSR(String ISHEUSR) {
        this.ISHEUSR = ISHEUSR;
    }

    public String getISHWHNO() {
        return ISHWHNO;
    }

    public void setISHWHNO(String ISHWHNO) {
        this.ISHWHNO = ISHWHNO;
    }

    public String getISHEDT() {
        return ISHEDT;
    }

    public void setISHEDT(String ISHEDT) {
        this.ISHEDT = ISHEDT;
    }

    public String getISHSEQ() {
        return ISHSEQ;
    }

    public void setISHSEQ(String ISHSEQ) {
        this.ISHSEQ = ISHSEQ;
    }

    public String getISHCNTTOT() {
        return ISHCNTTOT;
    }

    public void setISHCNTTOT(String ISHCNTTOT) {
        this.ISHCNTTOT = ISHCNTTOT;
    }

    public String getISHP1DT() {
        return ISHP1DT;
    }

    public void setISHP1DT(String ISHP1DT) {
        this.ISHP1DT = ISHP1DT;
    }

    public String getISHA1DT() {
        return ISHA1DT;
    }

    public void setISHA1DT(String ISHA1DT) {
        this.ISHA1DT = ISHA1DT;
    }

    public String getISHT1DT() {
        return ISHT1DT;
    }

    public void setISHT1DT(String ISHT1DT) {
        this.ISHT1DT = ISHT1DT;
    }

    public String getISHNOR() {
        return ISHNOR;
    }

    public void setISHNOR(String ISHNOR) {
        this.ISHNOR = ISHNOR;
    }

    public String getISHCDT() {
        return ISHCDT;
    }

    public void setISHCDT(String ISHCDT) {
        this.ISHCDT = ISHCDT;
    }

    public String getISHCTM() {
        return ISHCTM;
    }

    public void setISHCTM(String ISHCTM) {
        this.ISHCTM = ISHCTM;
    }

    public String getISHRQTM() {
        return ISHRQTM;
    }

    public void setISHRQTM(String ISHRQTM) {
        this.ISHRQTM = ISHRQTM;
    }

    public String getISHAPTM() {
        return ISHAPTM;
    }

    public void setISHAPTM(String ISHAPTM) {
        this.ISHAPTM = ISHAPTM;
    }

    public String getISHRCTM() {
        return ISHRCTM;
    }

    public void setISHRCTM(String ISHRCTM) {
        this.ISHRCTM = ISHRCTM;
    }

    public String getISHRCUSR() {
        return ISHRCUSR;
    }

    public void setISHRCUSR(String ISHRCUSR) {
        this.ISHRCUSR = ISHRCUSR;
    }

    public String getISHPKTM() {
        return ISHPKTM;
    }

    public void setISHPKTM(String ISHPKTM) {
        this.ISHPKTM = ISHPKTM;
    }

    public String getISHPKUSR() {
        return ISHPKUSR;
    }

    public void setISHPKUSR(String ISHPKUSR) {
        this.ISHPKUSR = ISHPKUSR;
    }

    public String getISHTPTM() {
        return ISHTPTM;
    }

    public void setISHTPTM(String ISHTPTM) {
        this.ISHTPTM = ISHTPTM;
    }

    public String getISHTPUSR() {
        return ISHTPUSR;
    }

    public void setISHTPUSR(String ISHTPUSR) {
        this.ISHTPUSR = ISHTPUSR;
    }

    public String getISHETM() {
        return ISHETM;
    }

    public void setISHETM(String ISHETM) {
        this.ISHETM = ISHETM;
    }

    public String getISHRPUSR() {
        return ISHRPUSR;
    }

    public void setISHRPUSR(String ISHRPUSR) {
        this.ISHRPUSR = ISHRPUSR;
    }

    public String getISHREASON() {
        return ISHREASON;
    }

    public void setISHREASON(String ISHREASON) {
        this.ISHREASON = ISHREASON;
    }

}
