/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.twc.wms.entity;

/**
 *
 * @author 93176
 */
public class ISMBILLH {

    private String ISHCONO;
    private String VBELN;
    private String FKART;
    private String WAERK;
    private String VKORG;
    private String VTWEG;
    private String FKDAT;
    private String INCO1;
    private String INCO2;
    private Double KURRF;
    private String ZTERM;
    private String LAND1;
    private String BUKRS;
    private String TAXK1;
    private Double NETWR;
    private String ERNAM;
    private String ERZET;
    private String ERDAT;
    private String KUNRG;
    private String KUNAG;
    private String SPART;
    private String XBLNR;
    private String ZUONR;
    private Double MWSBK;
    private String FKSTO;
    private String BUPLA;
    private String NAMRG;
    private String ADRNR;
    private String ISHEDT;
    private String ISHETM;
    private String ISHCDT;
    private String ISHCTM;
    private String ISHUSER;

    public ISMBILLH() {
    }

    public ISMBILLH(String ISHCONO, String VBELN, String FKART, String WAERK, String VKORG, String VTWEG, String FKDAT, String INCO1, String INCO2, Double KURRF, String ZTERM, String LAND1, String BUKRS, String TAXK1, Double NETWR, String ERNAM, String ERZET, String ERDAT, String KUNRG, String KUNAG, String SPART, String XBLNR, String ZUONR, Double MWSBK, String FKSTO, String BUPLA, String NAMRG, String ADRNR, String ISHEDT, String ISHETM, String ISHCDT, String ISHCTM, String ISHUSER) {
        this.ISHCONO = ISHCONO;
        this.VBELN = VBELN;
        this.FKART = FKART;
        this.WAERK = WAERK;
        this.VKORG = VKORG;
        this.VTWEG = VTWEG;
        this.FKDAT = FKDAT;
        this.INCO1 = INCO1;
        this.INCO2 = INCO2;
        this.KURRF = KURRF;
        this.ZTERM = ZTERM;
        this.LAND1 = LAND1;
        this.BUKRS = BUKRS;
        this.TAXK1 = TAXK1;
        this.NETWR = NETWR;
        this.ERNAM = ERNAM;
        this.ERZET = ERZET;
        this.ERDAT = ERDAT;
        this.KUNRG = KUNRG;
        this.KUNAG = KUNAG;
        this.SPART = SPART;
        this.XBLNR = XBLNR;
        this.ZUONR = ZUONR;
        this.MWSBK = MWSBK;
        this.FKSTO = FKSTO;
        this.BUPLA = BUPLA;
        this.NAMRG = NAMRG;
        this.ADRNR = ADRNR;
        this.ISHEDT = ISHEDT;
        this.ISHETM = ISHETM;
        this.ISHCDT = ISHCDT;
        this.ISHCTM = ISHCTM;
        this.ISHUSER = ISHUSER;
    }

    public String getISHCONO() {
        return ISHCONO;
    }

    public void setISHCONO(String ISHCONO) {
        this.ISHCONO = ISHCONO;
    }

    public String getVBELN() {
        return VBELN;
    }

    public void setVBELN(String VBELN) {
        this.VBELN = VBELN;
    }

    public String getFKART() {
        return FKART;
    }

    public void setFKART(String FKART) {
        this.FKART = FKART;
    }

    public String getWAERK() {
        return WAERK;
    }

    public void setWAERK(String WAERK) {
        this.WAERK = WAERK;
    }

    public String getVKORG() {
        return VKORG;
    }

    public void setVKORG(String VKORG) {
        this.VKORG = VKORG;
    }

    public String getVTWEG() {
        return VTWEG;
    }

    public void setVTWEG(String VTWEG) {
        this.VTWEG = VTWEG;
    }

    public String getFKDAT() {
        return FKDAT;
    }

    public void setFKDAT(String FKDAT) {
        this.FKDAT = FKDAT;
    }

    public String getINCO1() {
        return INCO1;
    }

    public void setINCO1(String INCO1) {
        this.INCO1 = INCO1;
    }

    public String getINCO2() {
        return INCO2;
    }

    public void setINCO2(String INCO2) {
        this.INCO2 = INCO2;
    }

    public Double getKURRF() {
        return KURRF;
    }

    public void setKURRF(Double KURRF) {
        this.KURRF = KURRF;
    }

    public String getZTERM() {
        return ZTERM;
    }

    public void setZTERM(String ZTERM) {
        this.ZTERM = ZTERM;
    }

    public String getLAND1() {
        return LAND1;
    }

    public void setLAND1(String LAND1) {
        this.LAND1 = LAND1;
    }

    public String getBUKRS() {
        return BUKRS;
    }

    public void setBUKRS(String BUKRS) {
        this.BUKRS = BUKRS;
    }

    public String getTAXK1() {
        return TAXK1;
    }

    public void setTAXK1(String TAXK1) {
        this.TAXK1 = TAXK1;
    }

    public Double getNETWR() {
        return NETWR;
    }

    public void setNETWR(Double NETWR) {
        this.NETWR = NETWR;
    }

    public String getERNAM() {
        return ERNAM;
    }

    public void setERNAM(String ERNAM) {
        this.ERNAM = ERNAM;
    }

    public String getERZET() {
        return ERZET;
    }

    public void setERZET(String ERZET) {
        this.ERZET = ERZET;
    }

    public String getERDAT() {
        return ERDAT;
    }

    public void setERDAT(String ERDAT) {
        this.ERDAT = ERDAT;
    }

    public String getKUNRG() {
        return KUNRG;
    }

    public void setKUNRG(String KUNRG) {
        this.KUNRG = KUNRG;
    }

    public String getKUNAG() {
        return KUNAG;
    }

    public void setKUNAG(String KUNAG) {
        this.KUNAG = KUNAG;
    }

    public String getSPART() {
        return SPART;
    }

    public void setSPART(String SPART) {
        this.SPART = SPART;
    }

    public String getXBLNR() {
        return XBLNR;
    }

    public void setXBLNR(String XBLNR) {
        this.XBLNR = XBLNR;
    }

    public String getZUONR() {
        return ZUONR;
    }

    public void setZUONR(String ZUONR) {
        this.ZUONR = ZUONR;
    }

    public Double getMWSBK() {
        return MWSBK;
    }

    public void setMWSBK(Double MWSBK) {
        this.MWSBK = MWSBK;
    }

    public String getFKSTO() {
        return FKSTO;
    }

    public void setFKSTO(String FKSTO) {
        this.FKSTO = FKSTO;
    }

    public String getBUPLA() {
        return BUPLA;
    }

    public void setBUPLA(String BUPLA) {
        this.BUPLA = BUPLA;
    }

    public String getNAMRG() {
        return NAMRG;
    }

    public void setNAMRG(String NAMRG) {
        this.NAMRG = NAMRG;
    }

    public String getADRNR() {
        return ADRNR;
    }

    public void setADRNR(String ADRNR) {
        this.ADRNR = ADRNR;
    }

    public String getISHEDT() {
        return ISHEDT;
    }

    public void setISHEDT(String ISHEDT) {
        this.ISHEDT = ISHEDT;
    }

    public String getISHETM() {
        return ISHETM;
    }

    public void setISHETM(String ISHETM) {
        this.ISHETM = ISHETM;
    }

    public String getISHCDT() {
        return ISHCDT;
    }

    public void setISHCDT(String ISHCDT) {
        this.ISHCDT = ISHCDT;
    }

    public String getISHCTM() {
        return ISHCTM;
    }

    public void setISHCTM(String ISHCTM) {
        this.ISHCTM = ISHCTM;
    }

    public String getISHUSER() {
        return ISHUSER;
    }

    public void setISHUSER(String ISHUSER) {
        this.ISHUSER = ISHUSER;
    }

}
