/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.entity;

import java.util.Date;

/**
 *
 * @author pittawat.pra
 */
public class QDESTYP {

    private String QDECOM;
    private String QDETYPE;
    private String QDEDESC;
    private Date QDEEDT;
    private Date QDECDT;
    private String QDEUSER;

    public QDESTYP(String QDECOM, String QDETYPE, String QDEDESC, Date QDEEDT, Date QDECDT, String QDEUSER) {
        this.QDECOM = QDECOM;
        this.QDETYPE = QDETYPE;
        this.QDEDESC = QDEDESC;
        this.QDEEDT = QDEEDT;
        this.QDECDT = QDECDT;
        this.QDEUSER = QDEUSER;
    }

    public QDESTYP() {

    }

    public String getQDECOM() {
        return QDECOM;
    }

    public void setQDECOM(String QDECOM) {
        this.QDECOM = QDECOM;
    }

    public String getQDETYPE() {
        return QDETYPE;
    }

    public void setQDETYPE(String QDETYPE) {
        this.QDETYPE = QDETYPE;
    }

    public String getQDEDESC() {
        return QDEDESC;
    }

    public void setQDEDESC(String QDEDESC) {
        this.QDEDESC = QDEDESC;
    }

    public Date getQDEEDT() {
        return QDEEDT;
    }

    public void setQDEEDT(Date QDEEDT) {
        this.QDEEDT = QDEEDT;
    }

    public Date getQDECDT() {
        return QDECDT;
    }

    public void setQDECDT(Date QDECDT) {
        this.QDECDT = QDECDT;
    }

    public String getQDEUSER() {
        return QDEUSER;
    }

    public void setQDEUSER(String QDEUSER) {
        this.QDEUSER = QDEUSER;
    }

    @Override
    public String toString() {
        return "QDESTYP{" + "QDECOM=" + QDECOM + ", QDETYPE=" + QDETYPE + ", QDEDESC=" + QDEDESC + ", QDEEDT=" + QDEEDT + ", QDECDT=" + QDECDT + ", QDEUSER=" + QDEUSER + '}';
    }

}
