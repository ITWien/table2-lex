/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.entity;

/**
 *
 * @author nutthawoot.noo
 */
public class QRKFEATURE {

    private String wh;
    private String Srkno;
    private String Frkno;
    private String Sside;
    private String Fside;
    private String Scolumn;
    private String Fcolumn;
    private String Srow;
    private String Frow;

    public QRKFEATURE() {

    }

    public QRKFEATURE(String wh, String Srkno, String Frkno,
            String Sside, String Fside, String Scolumn, String Fcolumn, String Srow, String Frow) {

        this.wh = wh;
        this.Srkno = Srkno;
        this.Frkno = Frkno;
        this.Sside = Sside;
        this.Fside = Fside;
        this.Scolumn = Scolumn;
        this.Fcolumn = Fcolumn;
        this.Srow = Srow;
        this.Frow = Frow;

    }

    public QRKFEATURE(String wh, String Srkno) {

        this.wh = wh;
        this.Srkno = Srkno;

    }

    public String getWH() {

        return wh;

    }

    public void setWH(String wh) {

        this.wh = wh;

    }

    public String getSrkno() {

        return Srkno;

    }

    public void setSrkno(String Srkno) {

        this.Srkno = Srkno;

    }

    public String getFrkno() {

        return Frkno;

    }

    public void setFrkno(String Frkno) {

        this.Frkno = Frkno;

    }
    
    public String getSside() {

        return Sside;

    }

    public void setSside(String Sside) {

        this.Sside = Sside;

    }

    public String getFside() {

        return Fside;

    }

    public void setFside(String Fside) {

        this.Fside = Fside;

    }
    
    public String getScolumn() {

        return Scolumn;

    }

    public void setScolumn(String Scolumn) {

        this.Scolumn = Scolumn;

    }

    public String getFcolumn() {

        return Fcolumn;

    }

    public void setFcolumn(String Fcolumn) {

        this.Fcolumn = Fcolumn;

    }
    
    public String getSrow() {

        return Srow;

    }

    public void setSrow(String Srow) {

        this.Srow = Srow;

    }

    public String getFrow() {

        return Frow;

    }

    public void setFrow(String Frow) {

        this.Frow = Frow;

    }

}
