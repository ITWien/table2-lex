/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.entity;

import java.util.ArrayList;
import com.twc.wms.entity.QIDETAIL;
import java.util.List;

/**
 *
 * @author nutthawoot.noo
 */
public class XMSWIDETAIL {

    private String XMSWIDQNO;
    private String XMSWIDLINE;
    private String XMSWIDDEST;
    private String XMSWIDDESTN;
    private String XMSWIDDRIVER;
    private String XMSWIDFOLLOWER;
    private String XMSWIDBAG;
    private String XMSWIDROLL;
    private String XMSWIDBOX;
    private String XMSWIDPCS;
    private String XMSWIDTOT;
    private String XMSWIDUSER;
    private String borderTop;

    public XMSWIDETAIL() {

    }

    public String getXMSWIDUSER() {
        return XMSWIDUSER;
    }

    public void setXMSWIDUSER(String XMSWIDUSER) {
        this.XMSWIDUSER = XMSWIDUSER;
    }

    public XMSWIDETAIL(String XMSWIDQNO, String XMSWIDLINE, String XMSWIDDEST, String XMSWIDDESTN, String XMSWIDDRIVER, String XMSWIDFOLLOWER, String XMSWIDBAG, String XMSWIDROLL, String XMSWIDBOX, String XMSWIDPCS, String XMSWIDTOT, String XMSWIDUSER) {
        this.XMSWIDQNO = XMSWIDQNO;
        this.XMSWIDLINE = XMSWIDLINE;
        this.XMSWIDDEST = XMSWIDDEST;
        this.XMSWIDDESTN = XMSWIDDESTN;
        this.XMSWIDDRIVER = XMSWIDDRIVER;
        this.XMSWIDFOLLOWER = XMSWIDFOLLOWER;
        this.XMSWIDBAG = XMSWIDBAG;
        this.XMSWIDROLL = XMSWIDROLL;
        this.XMSWIDBOX = XMSWIDBOX;
        this.XMSWIDPCS = XMSWIDPCS;
        this.XMSWIDTOT = XMSWIDTOT;
        this.XMSWIDUSER = XMSWIDUSER;
    }

    public String getBorderTop() {
        return borderTop;
    }

    public void setBorderTop(String borderTop) {
        this.borderTop = borderTop;
    }

    public String getXMSWIDQNO() {
        return XMSWIDQNO;
    }

    public void setXMSWIDQNO(String XMSWIDQNO) {
        this.XMSWIDQNO = XMSWIDQNO;
    }

    public String getXMSWIDLINE() {
        return XMSWIDLINE;
    }

    public void setXMSWIDLINE(String XMSWIDLINE) {
        this.XMSWIDLINE = XMSWIDLINE;
    }

    public String getXMSWIDDEST() {
        return XMSWIDDEST;
    }

    public void setXMSWIDDEST(String XMSWIDDEST) {
        this.XMSWIDDEST = XMSWIDDEST;
    }

    public String getXMSWIDDESTN() {
        return XMSWIDDESTN;
    }

    public void setXMSWIDDESTN(String XMSWIDDESTN) {
        this.XMSWIDDESTN = XMSWIDDESTN;
    }

    public String getXMSWIDDRIVER() {
        return XMSWIDDRIVER;
    }

    public void setXMSWIDDRIVER(String XMSWIDDRIVER) {
        this.XMSWIDDRIVER = XMSWIDDRIVER;
    }

    public String getXMSWIDFOLLOWER() {
        return XMSWIDFOLLOWER;
    }

    public void setXMSWIDFOLLOWER(String XMSWIDFOLLOWER) {
        this.XMSWIDFOLLOWER = XMSWIDFOLLOWER;
    }

    public String getXMSWIDBAG() {
        return XMSWIDBAG;
    }

    public void setXMSWIDBAG(String XMSWIDBAG) {
        this.XMSWIDBAG = XMSWIDBAG;
    }

    public String getXMSWIDROLL() {
        return XMSWIDROLL;
    }

    public void setXMSWIDROLL(String XMSWIDROLL) {
        this.XMSWIDROLL = XMSWIDROLL;
    }

    public String getXMSWIDBOX() {
        return XMSWIDBOX;
    }

    public void setXMSWIDBOX(String XMSWIDBOX) {
        this.XMSWIDBOX = XMSWIDBOX;
    }

    public String getXMSWIDPCS() {
        return XMSWIDPCS;
    }

    public void setXMSWIDPCS(String XMSWIDPCS) {
        this.XMSWIDPCS = XMSWIDPCS;
    }

    public String getXMSWIDTOT() {
        return XMSWIDTOT;
    }

    public void setXMSWIDTOT(String XMSWIDTOT) {
        this.XMSWIDTOT = XMSWIDTOT;
    }

}
