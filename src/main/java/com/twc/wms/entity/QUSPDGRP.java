/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.entity;

/**
 *
 * @author 93176
 */
public class QUSPDGRP {

    private String QUSCOM;
    private String QUSRUNNO;
    private String QUSUSERID;
    private String QUSPDGRP;
    private String QWHBGRP;
    private String QUSEDT;
    private String QUSCDT;
    private String QUSUSER;

    public QUSPDGRP() {
    }

    public QUSPDGRP(String QUSCOM, String QUSRUNNO, String QUSUSERID, String QUSPDGRP, String QWHBGRP, String QUSEDT, String QUSCDT, String QUSUSER) {
        this.QUSCOM = QUSCOM;
        this.QUSRUNNO = QUSRUNNO;
        this.QUSUSERID = QUSUSERID;
        this.QUSPDGRP = QUSPDGRP;
        this.QWHBGRP = QWHBGRP;
        this.QUSEDT = QUSEDT;
        this.QUSCDT = QUSCDT;
        this.QUSUSER = QUSUSER;
    }

    public String getQUSCOM() {
        return QUSCOM;
    }

    public void setQUSCOM(String QUSCOM) {
        this.QUSCOM = QUSCOM;
    }

    public String getQUSRUNNO() {
        return QUSRUNNO;
    }

    public void setQUSRUNNO(String QUSRUNNO) {
        this.QUSRUNNO = QUSRUNNO;
    }

    public String getQUSUSERID() {
        return QUSUSERID;
    }

    public void setQUSUSERID(String QUSUSERID) {
        this.QUSUSERID = QUSUSERID;
    }

    public String getQUSPDGRP() {
        return QUSPDGRP;
    }

    public void setQUSPDGRP(String QUSPDGRP) {
        this.QUSPDGRP = QUSPDGRP;
    }

    public String getQWHBGRP() {
        return QWHBGRP;
    }

    public void setQWHBGRP(String QWHBGRP) {
        this.QWHBGRP = QWHBGRP;
    }

    public String getQUSEDT() {
        return QUSEDT;
    }

    public void setQUSEDT(String QUSEDT) {
        this.QUSEDT = QUSEDT;
    }

    public String getQUSCDT() {
        return QUSCDT;
    }

    public void setQUSCDT(String QUSCDT) {
        this.QUSCDT = QUSCDT;
    }

    public String getQUSUSER() {
        return QUSUSER;
    }

    public void setQUSUSER(String QUSUSER) {
        this.QUSUSER = QUSUSER;
    }

}
