/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.entity;

/**
 *
 * @author nutthawoot.noo
 */
public class SAPMAS_2 {

    private String desc;
    private String mgroup;
    private String mgdesc;
    private String mc;
    private String mcname;

    public SAPMAS_2() {

    }

    public SAPMAS_2(String desc, String mtype,
            String mgroup, String mgdesc, String mc,
            String mcname) {

        this.desc = desc;
        this.mgroup = mgroup;
        this.mgdesc = mgdesc;
        this.mc = mc;
        this.mcname = mcname;
    }

    public String getDesc() {

        return desc;

    }

    public void setDesc(String desc) {

        this.desc = desc;

    }

    public String getMgroup() {

        return mgroup;

    }

    public void setMgroup(String mgroup) {

        this.mgroup = mgroup;

    }

    public String getMgdesc() {

        return mgdesc;

    }

    public void setMgdesc(String mgdesc) {

        this.mgdesc = mgdesc;

    }

    public String getMatCtrl() {

        return mc;

    }

    public void setMatCtrl(String mc) {

        this.mc = mc;

    }

    public String getMatCtrlName() {

        return mcname;

    }

    public void setMatCtrlName(String mcname) {

        this.mcname = mcname;

    }
}
