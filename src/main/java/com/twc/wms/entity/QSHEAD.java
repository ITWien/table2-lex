/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.entity;

/**
 *
 * @author nutthawoot.noo
 */
public class QSHEAD {

    private String QSHWHS;
    private String QSHTRDT;
    private String QSHGRID;
    private String QSHMTCTRL;
    private String QSHQUEUE;
    private String QSHUSER;
    private String option;

    public QSHEAD() {

    }

    public String getQSHUSER() {
        return QSHUSER;
    }

    public void setQSHUSER(String QSHUSER) {
        this.QSHUSER = QSHUSER;
    }

    public String getOption() {
        return option;
    }

    public void setOption(String option) {
        this.option = option;
    }

    public String getQSHWHS() {
        return QSHWHS;
    }

    public void setQSHWHS(String QSHWHS) {
        this.QSHWHS = QSHWHS;
    }

    public String getQSHTRDT() {
        return QSHTRDT;
    }

    public void setQSHTRDT(String QSHTRDT) {
        this.QSHTRDT = QSHTRDT;
    }

    public String getQSHGRID() {
        return QSHGRID;
    }

    public void setQSHGRID(String QSHGRID) {
        this.QSHGRID = QSHGRID;
    }

    public String getQSHMTCTRL() {
        return QSHMTCTRL;
    }

    public void setQSHMTCTRL(String QSHMTCTRL) {
        this.QSHMTCTRL = QSHMTCTRL;
    }

    public String getQSHQUEUE() {
        return QSHQUEUE;
    }

    public void setQSHQUEUE(String QSHQUEUE) {
        this.QSHQUEUE = QSHQUEUE;
    }

}
