/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.entity;

/**
 *
 * @author nutthawoot.noo
 */
public class Qdest {

    private String code;
    private String desc;
    private String name;
    private String adrs1;
    private String adrs2;
    private String adrs3;
    private String adrs4;
    private String type;
    private String wh;

    public Qdest() {

    }

    public String getWh() {
        return wh;
    }

    public void setWh(String wh) {
        this.wh = wh;
    }

    public Qdest(String code, String desc, String name, String adrs1, String adrs2, String adrs3, String adrs4) {

        this.code = code;
        this.desc = desc;
        this.name = name;
        this.adrs1 = adrs1;
        this.adrs2 = adrs2;
        this.adrs3 = adrs3;
        this.adrs4 = adrs4;

    }

    public void setType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public Qdest(String code, String desc) {

        this.code = code;
        this.desc = desc;

    }

    public String getCode() {

        return code;

    }

    public void setCode(String code) {

        this.code = code;

    }

    public String getDesc() {

        return desc;

    }

    public void setDesc(String desc) {

        this.desc = desc;

    }

    public String getName() {

        return name;

    }

    public void setName(String name) {

        this.name = name;

    }

    public String getAdrs1() {

        return adrs1;

    }

    public void setAdrs1(String adrs1) {

        this.adrs1 = adrs1;

    }

    public String getAdrs2() {

        return adrs2;

    }

    public void setAdrs2(String adrs2) {

        this.adrs2 = adrs2;

    }

    public String getAdrs3() {

        return adrs3;

    }

    public void setAdrs3(String adrs3) {

        this.adrs3 = adrs3;

    }

    public String getAdrs4() {

        return adrs4;

    }

    public void setAdrs4(String adrs4) {

        this.adrs4 = adrs4;

    }

    @Override

    public String toString() {

        return "Qdest{" + "code=" + code + ", desc=" + desc + '}';

    }

}
