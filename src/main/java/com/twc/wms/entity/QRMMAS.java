/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.entity;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author nutthawoot.noo
 */
public class QRMMAS {

    private String QRMID;
    private String QRMCODE;
    private String QRMVAL;
    private String QRMROLL;
    private String QRMIDTEMP;
    private String QRMDESC;
    private String QRMPO;
    private String QRMTAX;
    private String QRMPACKTYPE;
    private String QRMQTY;
    private String QRMALQTY;
    private String QRMBUN;
    private String QRMAUN;
    private String QRMCVFAC;
    private String QRMCVFQTY;
    private String QRMSTS;
    private String QRMPARENT;
    private String QRMPDDATE;
    private String QRMISDATE;
    private String QRMRCDATE;
    private String QRMTFDATE;
    private String QRMLOTNO;
    private String QRMDYNO;
    private String QRMQIS;
    private String QRMWHSE;
    private String QRMZONE;
    private String QRMPALLET;
    private String QRMRACKNO;
    private String QRMSIDE;
    private String QRMROW;
    private String QRMCOL;
    private String QRMLOC;
    private String QRMPLANT;
    private String QRMSTORAGE;
    private String QRMBARCODE;
    private String QRMSUPCODE;
    private String QRMEDT;
    private String QRMCDT;
    private String QRMUSER;
    private String MATCTRL;

    public QRMMAS() {

    }

    public String getMATCTRL() {
        return MATCTRL;
    }

    public void setMATCTRL(String MATCTRL) {
        this.MATCTRL = MATCTRL;
    }

    public String getQRMID() {
        return QRMID;
    }

    public void setQRMID(String QRMID) {
        this.QRMID = QRMID;
    }

    public String getQRMCODE() {
        return QRMCODE;
    }

    public void setQRMCODE(String QRMCODE) {
        this.QRMCODE = QRMCODE;
    }

    public String getQRMVAL() {
        return QRMVAL;
    }

    public void setQRMVAL(String QRMVAL) {
        this.QRMVAL = QRMVAL;
    }

    public String getQRMROLL() {
        return QRMROLL;
    }

    public void setQRMROLL(String QRMROLL) {
        this.QRMROLL = QRMROLL;
    }

    public String getQRMIDTEMP() {
        return QRMIDTEMP;
    }

    public void setQRMIDTEMP(String QRMIDTEMP) {
        this.QRMIDTEMP = QRMIDTEMP;
    }

    public String getQRMDESC() {
        return QRMDESC;
    }

    public void setQRMDESC(String QRMDESC) {
        this.QRMDESC = QRMDESC;
    }

    public String getQRMPO() {
        return QRMPO;
    }

    public void setQRMPO(String QRMPO) {
        this.QRMPO = QRMPO;
    }

    public String getQRMTAX() {
        return QRMTAX;
    }

    public void setQRMTAX(String QRMTAX) {
        this.QRMTAX = QRMTAX;
    }

    public String getQRMPACKTYPE() {
        return QRMPACKTYPE;
    }

    public void setQRMPACKTYPE(String QRMPACKTYPE) {
        this.QRMPACKTYPE = QRMPACKTYPE;
    }

    public String getQRMQTY() {
        return QRMQTY;
    }

    public void setQRMQTY(String QRMQTY) {
        this.QRMQTY = QRMQTY;
    }

    public String getQRMALQTY() {
        return QRMALQTY;
    }

    public void setQRMALQTY(String QRMALQTY) {
        this.QRMALQTY = QRMALQTY;
    }

    public String getQRMBUN() {
        return QRMBUN;
    }

    public void setQRMBUN(String QRMBUN) {
        this.QRMBUN = QRMBUN;
    }

    public String getQRMAUN() {
        return QRMAUN;
    }

    public void setQRMAUN(String QRMAUN) {
        this.QRMAUN = QRMAUN;
    }

    public String getQRMCVFAC() {
        return QRMCVFAC;
    }

    public void setQRMCVFAC(String QRMCVFAC) {
        this.QRMCVFAC = QRMCVFAC;
    }

    public String getQRMCVFQTY() {
        return QRMCVFQTY;
    }

    public void setQRMCVFQTY(String QRMCVFQTY) {
        this.QRMCVFQTY = QRMCVFQTY;
    }

    public String getQRMSTS() {
        return QRMSTS;
    }

    public void setQRMSTS(String QRMSTS) {
        this.QRMSTS = QRMSTS;
    }

    public String getQRMPARENT() {
        return QRMPARENT;
    }

    public void setQRMPARENT(String QRMPARENT) {
        this.QRMPARENT = QRMPARENT;
    }

    public String getQRMPDDATE() {
        return QRMPDDATE;
    }

    public void setQRMPDDATE(String QRMPDDATE) {
        this.QRMPDDATE = QRMPDDATE;
    }

    public String getQRMISDATE() {
        return QRMISDATE;
    }

    public void setQRMISDATE(String QRMISDATE) {
        this.QRMISDATE = QRMISDATE;
    }

    public String getQRMRCDATE() {
        return QRMRCDATE;
    }

    public void setQRMRCDATE(String QRMRCDATE) {
        this.QRMRCDATE = QRMRCDATE;
    }

    public String getQRMTFDATE() {
        return QRMTFDATE;
    }

    public void setQRMTFDATE(String QRMTFDATE) {
        this.QRMTFDATE = QRMTFDATE;
    }

    public String getQRMLOTNO() {
        return QRMLOTNO;
    }

    public void setQRMLOTNO(String QRMLOTNO) {
        this.QRMLOTNO = QRMLOTNO;
    }

    public String getQRMDYNO() {
        return QRMDYNO;
    }

    public void setQRMDYNO(String QRMDYNO) {
        this.QRMDYNO = QRMDYNO;
    }

    public String getQRMQIS() {
        return QRMQIS;
    }

    public void setQRMQIS(String QRMQIS) {
        this.QRMQIS = QRMQIS;
    }

    public String getQRMWHSE() {
        return QRMWHSE;
    }

    public void setQRMWHSE(String QRMWHSE) {
        this.QRMWHSE = QRMWHSE;
    }

    public String getQRMZONE() {
        return QRMZONE;
    }

    public void setQRMZONE(String QRMZONE) {
        this.QRMZONE = QRMZONE;
    }

    public String getQRMPALLET() {
        return QRMPALLET;
    }

    public void setQRMPALLET(String QRMPALLET) {
        this.QRMPALLET = QRMPALLET;
    }

    public String getQRMRACKNO() {
        return QRMRACKNO;
    }

    public void setQRMRACKNO(String QRMRACKNO) {
        this.QRMRACKNO = QRMRACKNO;
    }

    public String getQRMSIDE() {
        return QRMSIDE;
    }

    public void setQRMSIDE(String QRMSIDE) {
        this.QRMSIDE = QRMSIDE;
    }

    public String getQRMROW() {
        return QRMROW;
    }

    public void setQRMROW(String QRMROW) {
        this.QRMROW = QRMROW;
    }

    public String getQRMCOL() {
        return QRMCOL;
    }

    public void setQRMCOL(String QRMCOL) {
        this.QRMCOL = QRMCOL;
    }

    public String getQRMLOC() {
        return QRMLOC;
    }

    public void setQRMLOC(String QRMLOC) {
        this.QRMLOC = QRMLOC;
    }

    public String getQRMPLANT() {
        return QRMPLANT;
    }

    public void setQRMPLANT(String QRMPLANT) {
        this.QRMPLANT = QRMPLANT;
    }

    public String getQRMSTORAGE() {
        return QRMSTORAGE;
    }

    public void setQRMSTORAGE(String QRMSTORAGE) {
        this.QRMSTORAGE = QRMSTORAGE;
    }

    public String getQRMBARCODE() {
        return QRMBARCODE;
    }

    public void setQRMBARCODE(String QRMBARCODE) {
        this.QRMBARCODE = QRMBARCODE;
    }

    public String getQRMSUPCODE() {
        return QRMSUPCODE;
    }

    public void setQRMSUPCODE(String QRMSUPCODE) {
        this.QRMSUPCODE = QRMSUPCODE;
    }

    public String getQRMEDT() {
        return QRMEDT;
    }

    public void setQRMEDT(String QRMEDT) {
        this.QRMEDT = QRMEDT;
    }

    public String getQRMCDT() {
        return QRMCDT;
    }

    public void setQRMCDT(String QRMCDT) {
        this.QRMCDT = QRMCDT;
    }

    public String getQRMUSER() {
        return QRMUSER;
    }

    public void setQRMUSER(String QRMUSER) {
        this.QRMUSER = QRMUSER;
    }

}
