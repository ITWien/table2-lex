/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.entity;

/**
 *
 * @author nutthawoot.noo
 */
public class QRKDETAIL {

    private String zone;
    private String zone1;//warehouse
    private String rkno;
    private String side;
    private String column;
    private String row;

    public QRKDETAIL() {

    }

    public QRKDETAIL(String zone, String zone1, String rkno,
            String side, String column, String row) {

        this.zone = zone;
        this.zone1 = zone1;
        this.rkno = rkno;
        this.side = side;
        this.column = column;
        this.row = row;

    }

    public String getZone() {

        return zone;

    }

    public void setZone(String zone) {

        this.zone = zone;

    }
    
    public String getZone1() {

        return zone1;

    }

    public void setZone1(String zone1) {

        this.zone1 = zone1;

    }

    public String getRkno() {

        return rkno;

    }

    public void setRkno(String rkno) {

        this.rkno = rkno;

    }
    
    public String getSide() {

        return side;

    }

    public void setSide(String side) {

        this.side = side;

    }

    public String getColumn() {

        return column;

    }

    public void setColumn(String column) {

        this.column = column;

    }
    
    public String getRow() {

        return row;

    }

    public void setRow(String row) {

        this.row = row;

    }

}
