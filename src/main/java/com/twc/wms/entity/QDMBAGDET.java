/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.entity;

/**
 *
 * @author nutthawoot.noo
 */
public class QDMBAGDET {

    private String QDMBSTYLE;
    private String QDMBLINO;
    private String QDMBQTY;
    private String QDMBHEIGHT;
    private String option;

    public QDMBAGDET() {

    }

    public String getOption() {
        return option;
    }

    public void setOption(String option) {
        this.option = option;
    }

    public String getQDMBSTYLE() {
        return QDMBSTYLE;
    }

    public void setQDMBSTYLE(String QDMBSTYLE) {
        this.QDMBSTYLE = QDMBSTYLE;
    }

    public String getQDMBLINO() {
        return QDMBLINO;
    }

    public void setQDMBLINO(String QDMBLINO) {
        this.QDMBLINO = QDMBLINO;
    }

    public String getQDMBQTY() {
        return QDMBQTY;
    }

    public void setQDMBQTY(String QDMBQTY) {
        this.QDMBQTY = QDMBQTY;
    }

    public String getQDMBHEIGHT() {
        return QDMBHEIGHT;
    }

    public void setQDMBHEIGHT(String QDMBHEIGHT) {
        this.QDMBHEIGHT = QDMBHEIGHT;
    }

}
