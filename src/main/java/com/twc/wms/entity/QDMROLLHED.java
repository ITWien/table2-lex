/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.entity;

/**
 *
 * @author nutthawoot.noo
 */
public class QDMROLLHED {

    private String QDMRSTYLE;
    private String QDMRDESC;
    private String QDMRUNIT;
    private String QDMREQUA;
    private String QDMRX;
    private String QDMRY;
    private String QTY;

    public QDMROLLHED() {

    }

    public String getQDMRSTYLE() {
        return QDMRSTYLE;
    }

    public void setQDMRSTYLE(String QDMRSTYLE) {
        this.QDMRSTYLE = QDMRSTYLE;
    }

    public String getQDMRDESC() {
        return QDMRDESC;
    }

    public void setQDMRDESC(String QDMRDESC) {
        this.QDMRDESC = QDMRDESC;
    }

    public String getQDMRUNIT() {
        return QDMRUNIT;
    }

    public void setQDMRUNIT(String QDMRUNIT) {
        this.QDMRUNIT = QDMRUNIT;
    }

    public String getQDMREQUA() {
        return QDMREQUA;
    }

    public void setQDMREQUA(String QDMREQUA) {
        this.QDMREQUA = QDMREQUA;
    }

    public String getQDMRX() {
        return QDMRX;
    }

    public void setQDMRX(String QDMRX) {
        this.QDMRX = QDMRX;
    }

    public String getQDMRY() {
        return QDMRY;
    }

    public void setQDMRY(String QDMRY) {
        this.QDMRY = QDMRY;
    }

    public String getQTY() {
        return QTY;
    }

    public void setQTY(String QTY) {
        this.QTY = QTY;
    }

}
