/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.entity;

/**
 *
 * @author nutthawoot.noo
 */
public class QSET {

    private String code;
    private String total;
    private String sq1;
    private String sq2;
    private String sq3;
    private String sq4;
    private String sq5;
    private int lno;

    public QSET() {

    }

    public QSET(String code, String total, String sq1, String sq2, String sq3, String sq4, String sq5) {

        this.code = code;
        this.total = total;
        this.sq1 = sq1;
        this.sq2 = sq2;
        this.sq3 = sq3;
        this.sq4 = sq4;
        this.sq5 = sq5;

    }
    
    public QSET(String code, int lno, String sq1) {

        this.code = code;
        this.lno = lno;
        this.sq1 = sq1;

    }
    
    public QSET(String code, String total) {

        this.code = code;
        this.total = total;

    }

    public String getCode() {

        return code;

    }

    public void setCode(String code) {

        this.code = code;

    }

    public String getTotal() {

        return total;

    }

    public void setTotal(String total) {

        this.total = total;

    }
    
    public String getSq1() {

        return sq1;

    }
    
    public void setSq1(String sq1) {

        this.sq1 = sq1;

    }
    
    public String getSq2() {

        return sq2;

    }
    
    public void setSq2(String sq2) {

        this.sq2 = sq2;

    }
    
    public String getSq3() {

        return sq3;

    }
    
    public void setSq3(String sq3) {

        this.sq3 = sq3;

    }
    
    public String getSq4() {

        return sq4;

    }
    
    public void setSq4(String sq4) {

        this.sq4 = sq4;

    }
    
    public String getSq5() {

        return sq5;

    }
    
    public void setSq5(String sq5) {

        this.sq5 = sq5;

    }
    
    public int getLno() {

        return lno;

    }
    
    public void setLno(int lno) {

        this.lno = lno;

    }

}
