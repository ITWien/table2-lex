/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.entity;

import java.util.ArrayList;
import com.twc.wms.entity.QIDETAIL;
import java.util.List;

/**
 *
 * @author nutthawoot.noo
 */
public class XMSOTHEAD {

    private String XMSOTHQNO;
    private String XMSOTHWHS;
    private String XMSOTHSPDT;
    private String XMSOTHDEST;
    private String XMSOTHROUND;
    private String XMSOTHTYPE;
    private String XMSOTHLICENNO;
    private String XMSOTHDRIVER;
    private String XMSOTHFOLLOWER;
    private String XMSOTHBAG;
    private String XMSOTHROLL;
    private String XMSOTHBOX;
    private String XMSOTHPCS;
    private String XMSOTHTOTDOC;
    private String XMSOTHUSER;
    private String options;
    private String whn;
    private String destn;
    private String XMSOTHDOCNO;

    public XMSOTHEAD() {

    }

    public String getXMSOTHDOCNO() {
        return XMSOTHDOCNO;
    }

    public void setXMSOTHDOCNO(String XMSOTHDOCNO) {
        this.XMSOTHDOCNO = XMSOTHDOCNO;
    }

    public String getWhn() {
        return whn;
    }

    public void setWhn(String whn) {
        this.whn = whn;
    }

    public String getDestn() {
        return destn;
    }

    public void setDestn(String destn) {
        this.destn = destn;
    }

    public XMSOTHEAD(String XMSOTHQNO, String XMSOTHWHS, String XMSOTHSPDT, String XMSOTHDEST, String XMSOTHROUND, String XMSOTHTYPE, String XMSOTHLICENNO, String XMSOTHDRIVER, String XMSOTHFOLLOWER, String XMSOTHBAG, String XMSOTHROLL, String XMSOTHBOX, String XMSOTHPCS, String XMSOTHTOTDOC, String XMSOTHUSER) {
        this.XMSOTHQNO = XMSOTHQNO;
        this.XMSOTHWHS = XMSOTHWHS;
        this.XMSOTHSPDT = XMSOTHSPDT;
        this.XMSOTHDEST = XMSOTHDEST;
        this.XMSOTHROUND = XMSOTHROUND;
        this.XMSOTHTYPE = XMSOTHTYPE;
        this.XMSOTHLICENNO = XMSOTHLICENNO;
        this.XMSOTHDRIVER = XMSOTHDRIVER;
        this.XMSOTHFOLLOWER = XMSOTHFOLLOWER;
        this.XMSOTHBAG = XMSOTHBAG;
        this.XMSOTHROLL = XMSOTHROLL;
        this.XMSOTHBOX = XMSOTHBOX;
        this.XMSOTHPCS = XMSOTHPCS;
        this.XMSOTHTOTDOC = XMSOTHTOTDOC;
        this.XMSOTHUSER = XMSOTHUSER;
    }

    public String getOptions() {
        return options;
    }

    public void setOptions(String options) {
        this.options = options;
    }

    public String getXMSOTHUSER() {
        return XMSOTHUSER;
    }

    public void setXMSOTHUSER(String XMSOTHUSER) {
        this.XMSOTHUSER = XMSOTHUSER;
    }

    public String getXMSOTHQNO() {
        return XMSOTHQNO;
    }

    public void setXMSOTHQNO(String XMSOTHQNO) {
        this.XMSOTHQNO = XMSOTHQNO;
    }

    public String getXMSOTHWHS() {
        return XMSOTHWHS;
    }

    public void setXMSOTHWHS(String XMSOTHWHS) {
        this.XMSOTHWHS = XMSOTHWHS;
    }

    public String getXMSOTHSPDT() {
        return XMSOTHSPDT;
    }

    public void setXMSOTHSPDT(String XMSOTHSPDT) {
        this.XMSOTHSPDT = XMSOTHSPDT;
    }

    public String getXMSOTHDEST() {
        return XMSOTHDEST;
    }

    public void setXMSOTHDEST(String XMSOTHDEST) {
        this.XMSOTHDEST = XMSOTHDEST;
    }

    public String getXMSOTHROUND() {
        return XMSOTHROUND;
    }

    public void setXMSOTHROUND(String XMSOTHROUND) {
        this.XMSOTHROUND = XMSOTHROUND;
    }

    public String getXMSOTHTYPE() {
        return XMSOTHTYPE;
    }

    public void setXMSOTHTYPE(String XMSOTHTYPE) {
        this.XMSOTHTYPE = XMSOTHTYPE;
    }

    public String getXMSOTHLICENNO() {
        return XMSOTHLICENNO;
    }

    public void setXMSOTHLICENNO(String XMSOTHLICENNO) {
        this.XMSOTHLICENNO = XMSOTHLICENNO;
    }

    public String getXMSOTHDRIVER() {
        return XMSOTHDRIVER;
    }

    public void setXMSOTHDRIVER(String XMSOTHDRIVER) {
        this.XMSOTHDRIVER = XMSOTHDRIVER;
    }

    public String getXMSOTHFOLLOWER() {
        return XMSOTHFOLLOWER;
    }

    public void setXMSOTHFOLLOWER(String XMSOTHFOLLOWER) {
        this.XMSOTHFOLLOWER = XMSOTHFOLLOWER;
    }

    public String getXMSOTHBAG() {
        return XMSOTHBAG;
    }

    public void setXMSOTHBAG(String XMSOTHBAG) {
        this.XMSOTHBAG = XMSOTHBAG;
    }

    public String getXMSOTHROLL() {
        return XMSOTHROLL;
    }

    public void setXMSOTHROLL(String XMSOTHROLL) {
        this.XMSOTHROLL = XMSOTHROLL;
    }

    public String getXMSOTHBOX() {
        return XMSOTHBOX;
    }

    public void setXMSOTHBOX(String XMSOTHBOX) {
        this.XMSOTHBOX = XMSOTHBOX;
    }

    public String getXMSOTHPCS() {
        return XMSOTHPCS;
    }

    public void setXMSOTHPCS(String XMSOTHPCS) {
        this.XMSOTHPCS = XMSOTHPCS;
    }

    public String getXMSOTHTOTDOC() {
        return XMSOTHTOTDOC;
    }

    public void setXMSOTHTOTDOC(String XMSOTHTOTDOC) {
        this.XMSOTHTOTDOC = XMSOTHTOTDOC;
    }
}
