/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.entity;

import java.util.ArrayList;
import com.twc.wms.entity.QIDETAIL;
import java.util.List;

/**
 *
 * @author nutthawoot.noo
 */
public class WMS991 {

    private String WH;
    private String DEPT;
    private String PRODUCT;
    private String SUBNAME;
    private String NAME;
    private String OH_SKU1;
    private String OH_SKU2;
    private String OH_SKU3;
    private String OH_SKU4;
    private String OH_SKU5;
    private String OH_SKU6;
    private String OH_SKU7;
    private String OH_SKU8;
    private String OH_SKU9;
    private String OH_SKU10;
    private String OH_SKU11;
    private String OH_SKU12;
    private String OH_SKU;
    private String CK_SKU1;
    private String CK_SKU2;
    private String CK_SKU3;
    private String CK_SKU4;
    private String CK_SKU5;
    private String CK_SKU6;
    private String CK_SKU7;
    private String CK_SKU8;
    private String CK_SKU9;
    private String CK_SKU10;
    private String CK_SKU11;
    private String CK_SKU12;
    private String CK_SKU;
    private String CK_SKU_PER;
    private String COL_SKU1;
    private String COL_SKU2;
    private String COL_SKU3;
    private String COL_SKU4;
    private String COL_SKU5;
    private String COL_SKU6;
    private String COL_SKU7;
    private String COL_SKU8;
    private String COL_SKU9;
    private String COL_SKU10;
    private String COL_SKU11;
    private String COL_SKU12;
    private String COL_SKU;
    private String COL_SKU_PER;
    private String DIFF_SKU1;
    private String DIFF_SKU2;
    private String DIFF_SKU3;
    private String DIFF_SKU4;
    private String DIFF_SKU5;
    private String DIFF_SKU6;
    private String DIFF_SKU7;
    private String DIFF_SKU8;
    private String DIFF_SKU9;
    private String DIFF_SKU10;
    private String DIFF_SKU11;
    private String DIFF_SKU12;
    private String DIFF_SKU;
    private String DIFF_SKU1_PER;
    private String DIFF_SKU2_PER;
    private String DIFF_SKU3_PER;
    private String DIFF_SKU4_PER;
    private String DIFF_SKU5_PER;
    private String DIFF_SKU6_PER;
    private String DIFF_SKU7_PER;
    private String DIFF_SKU8_PER;
    private String DIFF_SKU9_PER;
    private String DIFF_SKU10_PER;
    private String DIFF_SKU11_PER;
    private String DIFF_SKU12_PER;
    private String DIFF_SKU_PER;
    private String AMOUNT_1;
    private String AMOUNT_2;
    private String AMOUNT_3;
    private String AMOUNT_4;
    private String AMOUNT_5;
    private String AMOUNT_6;
    private String AMOUNT_7;
    private String AMOUNT_8;
    private String AMOUNT_9;
    private String AMOUNT_10;
    private String AMOUNT_11;
    private String AMOUNT_12;
    private String AMOUNT;
    private String PACKPCS_1;
    private String PACKPCS_2;
    private String PACKPCS_3;
    private String PACKPCS_4;
    private String PACKPCS_5;
    private String PACKPCS_6;
    private String PACKPCS_7;
    private String PACKPCS_8;
    private String PACKPCS_9;
    private String PACKPCS_10;
    private String PACKPCS_11;
    private String PACKPCS_12;
    private String PACKPCS;
    private String AMOUNT2_1;
    private String AMOUNT2_2;
    private String AMOUNT2_3;
    private String AMOUNT2_4;
    private String AMOUNT2_5;
    private String AMOUNT2_6;
    private String AMOUNT2_7;
    private String AMOUNT2_8;
    private String AMOUNT2_9;
    private String AMOUNT2_10;
    private String AMOUNT2_11;
    private String AMOUNT2_12;
    private String AMOUNT2;
    private String PACKPCS2_1;
    private String PACKPCS2_2;
    private String PACKPCS2_3;
    private String PACKPCS2_4;
    private String PACKPCS2_5;
    private String PACKPCS2_6;
    private String PACKPCS2_7;
    private String PACKPCS2_8;
    private String PACKPCS2_9;
    private String PACKPCS2_10;
    private String PACKPCS2_11;
    private String PACKPCS2_12;
    private String PACKPCS2;

    public WMS991() {

    }

    public String getAMOUNT_1() {
        return AMOUNT_1;
    }

    public void setAMOUNT_1(String AMOUNT_1) {
        this.AMOUNT_1 = AMOUNT_1;
    }

    public String getAMOUNT_2() {
        return AMOUNT_2;
    }

    public void setAMOUNT_2(String AMOUNT_2) {
        this.AMOUNT_2 = AMOUNT_2;
    }

    public String getAMOUNT_3() {
        return AMOUNT_3;
    }

    public void setAMOUNT_3(String AMOUNT_3) {
        this.AMOUNT_3 = AMOUNT_3;
    }

    public String getAMOUNT_4() {
        return AMOUNT_4;
    }

    public void setAMOUNT_4(String AMOUNT_4) {
        this.AMOUNT_4 = AMOUNT_4;
    }

    public String getAMOUNT_5() {
        return AMOUNT_5;
    }

    public void setAMOUNT_5(String AMOUNT_5) {
        this.AMOUNT_5 = AMOUNT_5;
    }

    public String getAMOUNT_6() {
        return AMOUNT_6;
    }

    public void setAMOUNT_6(String AMOUNT_6) {
        this.AMOUNT_6 = AMOUNT_6;
    }

    public String getAMOUNT_7() {
        return AMOUNT_7;
    }

    public void setAMOUNT_7(String AMOUNT_7) {
        this.AMOUNT_7 = AMOUNT_7;
    }

    public String getAMOUNT_8() {
        return AMOUNT_8;
    }

    public void setAMOUNT_8(String AMOUNT_8) {
        this.AMOUNT_8 = AMOUNT_8;
    }

    public String getAMOUNT_9() {
        return AMOUNT_9;
    }

    public void setAMOUNT_9(String AMOUNT_9) {
        this.AMOUNT_9 = AMOUNT_9;
    }

    public String getAMOUNT_10() {
        return AMOUNT_10;
    }

    public void setAMOUNT_10(String AMOUNT_10) {
        this.AMOUNT_10 = AMOUNT_10;
    }

    public String getAMOUNT_11() {
        return AMOUNT_11;
    }

    public void setAMOUNT_11(String AMOUNT_11) {
        this.AMOUNT_11 = AMOUNT_11;
    }

    public String getAMOUNT_12() {
        return AMOUNT_12;
    }

    public void setAMOUNT_12(String AMOUNT_12) {
        this.AMOUNT_12 = AMOUNT_12;
    }

    public String getAMOUNT() {
        return AMOUNT;
    }

    public void setAMOUNT(String AMOUNT) {
        this.AMOUNT = AMOUNT;
    }

    public String getPACKPCS_1() {
        return PACKPCS_1;
    }

    public void setPACKPCS_1(String PACKPCS_1) {
        this.PACKPCS_1 = PACKPCS_1;
    }

    public String getPACKPCS_2() {
        return PACKPCS_2;
    }

    public void setPACKPCS_2(String PACKPCS_2) {
        this.PACKPCS_2 = PACKPCS_2;
    }

    public String getPACKPCS_3() {
        return PACKPCS_3;
    }

    public void setPACKPCS_3(String PACKPCS_3) {
        this.PACKPCS_3 = PACKPCS_3;
    }

    public String getPACKPCS_4() {
        return PACKPCS_4;
    }

    public void setPACKPCS_4(String PACKPCS_4) {
        this.PACKPCS_4 = PACKPCS_4;
    }

    public String getPACKPCS_5() {
        return PACKPCS_5;
    }

    public void setPACKPCS_5(String PACKPCS_5) {
        this.PACKPCS_5 = PACKPCS_5;
    }

    public String getPACKPCS_6() {
        return PACKPCS_6;
    }

    public void setPACKPCS_6(String PACKPCS_6) {
        this.PACKPCS_6 = PACKPCS_6;
    }

    public String getPACKPCS_7() {
        return PACKPCS_7;
    }

    public void setPACKPCS_7(String PACKPCS_7) {
        this.PACKPCS_7 = PACKPCS_7;
    }

    public String getPACKPCS_8() {
        return PACKPCS_8;
    }

    public void setPACKPCS_8(String PACKPCS_8) {
        this.PACKPCS_8 = PACKPCS_8;
    }

    public String getPACKPCS_9() {
        return PACKPCS_9;
    }

    public void setPACKPCS_9(String PACKPCS_9) {
        this.PACKPCS_9 = PACKPCS_9;
    }

    public String getPACKPCS_10() {
        return PACKPCS_10;
    }

    public void setPACKPCS_10(String PACKPCS_10) {
        this.PACKPCS_10 = PACKPCS_10;
    }

    public String getPACKPCS_11() {
        return PACKPCS_11;
    }

    public void setPACKPCS_11(String PACKPCS_11) {
        this.PACKPCS_11 = PACKPCS_11;
    }

    public String getPACKPCS_12() {
        return PACKPCS_12;
    }

    public void setPACKPCS_12(String PACKPCS_12) {
        this.PACKPCS_12 = PACKPCS_12;
    }

    public String getPACKPCS() {
        return PACKPCS;
    }

    public void setPACKPCS(String PACKPCS) {
        this.PACKPCS = PACKPCS;
    }

    public String getAMOUNT2_1() {
        return AMOUNT2_1;
    }

    public void setAMOUNT2_1(String AMOUNT2_1) {
        this.AMOUNT2_1 = AMOUNT2_1;
    }

    public String getAMOUNT2_2() {
        return AMOUNT2_2;
    }

    public void setAMOUNT2_2(String AMOUNT2_2) {
        this.AMOUNT2_2 = AMOUNT2_2;
    }

    public String getAMOUNT2_3() {
        return AMOUNT2_3;
    }

    public void setAMOUNT2_3(String AMOUNT2_3) {
        this.AMOUNT2_3 = AMOUNT2_3;
    }

    public String getAMOUNT2_4() {
        return AMOUNT2_4;
    }

    public void setAMOUNT2_4(String AMOUNT2_4) {
        this.AMOUNT2_4 = AMOUNT2_4;
    }

    public String getAMOUNT2_5() {
        return AMOUNT2_5;
    }

    public void setAMOUNT2_5(String AMOUNT2_5) {
        this.AMOUNT2_5 = AMOUNT2_5;
    }

    public String getAMOUNT2_6() {
        return AMOUNT2_6;
    }

    public void setAMOUNT2_6(String AMOUNT2_6) {
        this.AMOUNT2_6 = AMOUNT2_6;
    }

    public String getAMOUNT2_7() {
        return AMOUNT2_7;
    }

    public void setAMOUNT2_7(String AMOUNT2_7) {
        this.AMOUNT2_7 = AMOUNT2_7;
    }

    public String getAMOUNT2_8() {
        return AMOUNT2_8;
    }

    public void setAMOUNT2_8(String AMOUNT2_8) {
        this.AMOUNT2_8 = AMOUNT2_8;
    }

    public String getAMOUNT2_9() {
        return AMOUNT2_9;
    }

    public void setAMOUNT2_9(String AMOUNT2_9) {
        this.AMOUNT2_9 = AMOUNT2_9;
    }

    public String getAMOUNT2_10() {
        return AMOUNT2_10;
    }

    public void setAMOUNT2_10(String AMOUNT2_10) {
        this.AMOUNT2_10 = AMOUNT2_10;
    }

    public String getAMOUNT2_11() {
        return AMOUNT2_11;
    }

    public void setAMOUNT2_11(String AMOUNT2_11) {
        this.AMOUNT2_11 = AMOUNT2_11;
    }

    public String getAMOUNT2_12() {
        return AMOUNT2_12;
    }

    public void setAMOUNT2_12(String AMOUNT2_12) {
        this.AMOUNT2_12 = AMOUNT2_12;
    }

    public String getAMOUNT2() {
        return AMOUNT2;
    }

    public void setAMOUNT2(String AMOUNT2) {
        this.AMOUNT2 = AMOUNT2;
    }

    public String getPACKPCS2_1() {
        return PACKPCS2_1;
    }

    public void setPACKPCS2_1(String PACKPCS2_1) {
        this.PACKPCS2_1 = PACKPCS2_1;
    }

    public String getPACKPCS2_2() {
        return PACKPCS2_2;
    }

    public void setPACKPCS2_2(String PACKPCS2_2) {
        this.PACKPCS2_2 = PACKPCS2_2;
    }

    public String getPACKPCS2_3() {
        return PACKPCS2_3;
    }

    public void setPACKPCS2_3(String PACKPCS2_3) {
        this.PACKPCS2_3 = PACKPCS2_3;
    }

    public String getPACKPCS2_4() {
        return PACKPCS2_4;
    }

    public void setPACKPCS2_4(String PACKPCS2_4) {
        this.PACKPCS2_4 = PACKPCS2_4;
    }

    public String getPACKPCS2_5() {
        return PACKPCS2_5;
    }

    public void setPACKPCS2_5(String PACKPCS2_5) {
        this.PACKPCS2_5 = PACKPCS2_5;
    }

    public String getPACKPCS2_6() {
        return PACKPCS2_6;
    }

    public void setPACKPCS2_6(String PACKPCS2_6) {
        this.PACKPCS2_6 = PACKPCS2_6;
    }

    public String getPACKPCS2_7() {
        return PACKPCS2_7;
    }

    public void setPACKPCS2_7(String PACKPCS2_7) {
        this.PACKPCS2_7 = PACKPCS2_7;
    }

    public String getPACKPCS2_8() {
        return PACKPCS2_8;
    }

    public void setPACKPCS2_8(String PACKPCS2_8) {
        this.PACKPCS2_8 = PACKPCS2_8;
    }

    public String getPACKPCS2_9() {
        return PACKPCS2_9;
    }

    public void setPACKPCS2_9(String PACKPCS2_9) {
        this.PACKPCS2_9 = PACKPCS2_9;
    }

    public String getPACKPCS2_10() {
        return PACKPCS2_10;
    }

    public void setPACKPCS2_10(String PACKPCS2_10) {
        this.PACKPCS2_10 = PACKPCS2_10;
    }

    public String getPACKPCS2_11() {
        return PACKPCS2_11;
    }

    public void setPACKPCS2_11(String PACKPCS2_11) {
        this.PACKPCS2_11 = PACKPCS2_11;
    }

    public String getPACKPCS2_12() {
        return PACKPCS2_12;
    }

    public void setPACKPCS2_12(String PACKPCS2_12) {
        this.PACKPCS2_12 = PACKPCS2_12;
    }

    public String getPACKPCS2() {
        return PACKPCS2;
    }

    public void setPACKPCS2(String PACKPCS2) {
        this.PACKPCS2 = PACKPCS2;
    }

    public String getOH_SKU1() {
        return OH_SKU1;
    }

    public void setOH_SKU1(String OH_SKU1) {
        this.OH_SKU1 = OH_SKU1;
    }

    public String getOH_SKU2() {
        return OH_SKU2;
    }

    public void setOH_SKU2(String OH_SKU2) {
        this.OH_SKU2 = OH_SKU2;
    }

    public String getOH_SKU3() {
        return OH_SKU3;
    }

    public void setOH_SKU3(String OH_SKU3) {
        this.OH_SKU3 = OH_SKU3;
    }

    public String getOH_SKU4() {
        return OH_SKU4;
    }

    public void setOH_SKU4(String OH_SKU4) {
        this.OH_SKU4 = OH_SKU4;
    }

    public String getOH_SKU5() {
        return OH_SKU5;
    }

    public void setOH_SKU5(String OH_SKU5) {
        this.OH_SKU5 = OH_SKU5;
    }

    public String getOH_SKU6() {
        return OH_SKU6;
    }

    public void setOH_SKU6(String OH_SKU6) {
        this.OH_SKU6 = OH_SKU6;
    }

    public String getOH_SKU7() {
        return OH_SKU7;
    }

    public void setOH_SKU7(String OH_SKU7) {
        this.OH_SKU7 = OH_SKU7;
    }

    public String getOH_SKU8() {
        return OH_SKU8;
    }

    public void setOH_SKU8(String OH_SKU8) {
        this.OH_SKU8 = OH_SKU8;
    }

    public String getOH_SKU9() {
        return OH_SKU9;
    }

    public void setOH_SKU9(String OH_SKU9) {
        this.OH_SKU9 = OH_SKU9;
    }

    public String getOH_SKU10() {
        return OH_SKU10;
    }

    public void setOH_SKU10(String OH_SKU10) {
        this.OH_SKU10 = OH_SKU10;
    }

    public String getOH_SKU11() {
        return OH_SKU11;
    }

    public void setOH_SKU11(String OH_SKU11) {
        this.OH_SKU11 = OH_SKU11;
    }

    public String getOH_SKU12() {
        return OH_SKU12;
    }

    public void setOH_SKU12(String OH_SKU12) {
        this.OH_SKU12 = OH_SKU12;
    }

    public String getCK_SKU1() {
        return CK_SKU1;
    }

    public void setCK_SKU1(String CK_SKU1) {
        this.CK_SKU1 = CK_SKU1;
    }

    public String getCK_SKU2() {
        return CK_SKU2;
    }

    public void setCK_SKU2(String CK_SKU2) {
        this.CK_SKU2 = CK_SKU2;
    }

    public String getCK_SKU3() {
        return CK_SKU3;
    }

    public void setCK_SKU3(String CK_SKU3) {
        this.CK_SKU3 = CK_SKU3;
    }

    public String getCK_SKU4() {
        return CK_SKU4;
    }

    public void setCK_SKU4(String CK_SKU4) {
        this.CK_SKU4 = CK_SKU4;
    }

    public String getCK_SKU5() {
        return CK_SKU5;
    }

    public void setCK_SKU5(String CK_SKU5) {
        this.CK_SKU5 = CK_SKU5;
    }

    public String getCK_SKU6() {
        return CK_SKU6;
    }

    public void setCK_SKU6(String CK_SKU6) {
        this.CK_SKU6 = CK_SKU6;
    }

    public String getCK_SKU7() {
        return CK_SKU7;
    }

    public void setCK_SKU7(String CK_SKU7) {
        this.CK_SKU7 = CK_SKU7;
    }

    public String getCK_SKU8() {
        return CK_SKU8;
    }

    public void setCK_SKU8(String CK_SKU8) {
        this.CK_SKU8 = CK_SKU8;
    }

    public String getCK_SKU9() {
        return CK_SKU9;
    }

    public void setCK_SKU9(String CK_SKU9) {
        this.CK_SKU9 = CK_SKU9;
    }

    public String getCK_SKU10() {
        return CK_SKU10;
    }

    public void setCK_SKU10(String CK_SKU10) {
        this.CK_SKU10 = CK_SKU10;
    }

    public String getCK_SKU11() {
        return CK_SKU11;
    }

    public void setCK_SKU11(String CK_SKU11) {
        this.CK_SKU11 = CK_SKU11;
    }

    public String getCK_SKU12() {
        return CK_SKU12;
    }

    public void setCK_SKU12(String CK_SKU12) {
        this.CK_SKU12 = CK_SKU12;
    }

    public String getCOL_SKU1() {
        return COL_SKU1;
    }

    public void setCOL_SKU1(String COL_SKU1) {
        this.COL_SKU1 = COL_SKU1;
    }

    public String getCOL_SKU2() {
        return COL_SKU2;
    }

    public void setCOL_SKU2(String COL_SKU2) {
        this.COL_SKU2 = COL_SKU2;
    }

    public String getCOL_SKU3() {
        return COL_SKU3;
    }

    public void setCOL_SKU3(String COL_SKU3) {
        this.COL_SKU3 = COL_SKU3;
    }

    public String getCOL_SKU4() {
        return COL_SKU4;
    }

    public void setCOL_SKU4(String COL_SKU4) {
        this.COL_SKU4 = COL_SKU4;
    }

    public String getCOL_SKU5() {
        return COL_SKU5;
    }

    public void setCOL_SKU5(String COL_SKU5) {
        this.COL_SKU5 = COL_SKU5;
    }

    public String getCOL_SKU6() {
        return COL_SKU6;
    }

    public void setCOL_SKU6(String COL_SKU6) {
        this.COL_SKU6 = COL_SKU6;
    }

    public String getCOL_SKU7() {
        return COL_SKU7;
    }

    public void setCOL_SKU7(String COL_SKU7) {
        this.COL_SKU7 = COL_SKU7;
    }

    public String getCOL_SKU8() {
        return COL_SKU8;
    }

    public void setCOL_SKU8(String COL_SKU8) {
        this.COL_SKU8 = COL_SKU8;
    }

    public String getCOL_SKU9() {
        return COL_SKU9;
    }

    public void setCOL_SKU9(String COL_SKU9) {
        this.COL_SKU9 = COL_SKU9;
    }

    public String getCOL_SKU10() {
        return COL_SKU10;
    }

    public void setCOL_SKU10(String COL_SKU10) {
        this.COL_SKU10 = COL_SKU10;
    }

    public String getCOL_SKU11() {
        return COL_SKU11;
    }

    public void setCOL_SKU11(String COL_SKU11) {
        this.COL_SKU11 = COL_SKU11;
    }

    public String getCOL_SKU12() {
        return COL_SKU12;
    }

    public void setCOL_SKU12(String COL_SKU12) {
        this.COL_SKU12 = COL_SKU12;
    }

    public String getDIFF_SKU1() {
        return DIFF_SKU1;
    }

    public void setDIFF_SKU1(String DIFF_SKU1) {
        this.DIFF_SKU1 = DIFF_SKU1;
    }

    public String getDIFF_SKU2() {
        return DIFF_SKU2;
    }

    public void setDIFF_SKU2(String DIFF_SKU2) {
        this.DIFF_SKU2 = DIFF_SKU2;
    }

    public String getDIFF_SKU3() {
        return DIFF_SKU3;
    }

    public void setDIFF_SKU3(String DIFF_SKU3) {
        this.DIFF_SKU3 = DIFF_SKU3;
    }

    public String getDIFF_SKU4() {
        return DIFF_SKU4;
    }

    public void setDIFF_SKU4(String DIFF_SKU4) {
        this.DIFF_SKU4 = DIFF_SKU4;
    }

    public String getDIFF_SKU5() {
        return DIFF_SKU5;
    }

    public void setDIFF_SKU5(String DIFF_SKU5) {
        this.DIFF_SKU5 = DIFF_SKU5;
    }

    public String getDIFF_SKU6() {
        return DIFF_SKU6;
    }

    public void setDIFF_SKU6(String DIFF_SKU6) {
        this.DIFF_SKU6 = DIFF_SKU6;
    }

    public String getDIFF_SKU7() {
        return DIFF_SKU7;
    }

    public void setDIFF_SKU7(String DIFF_SKU7) {
        this.DIFF_SKU7 = DIFF_SKU7;
    }

    public String getDIFF_SKU8() {
        return DIFF_SKU8;
    }

    public void setDIFF_SKU8(String DIFF_SKU8) {
        this.DIFF_SKU8 = DIFF_SKU8;
    }

    public String getDIFF_SKU9() {
        return DIFF_SKU9;
    }

    public void setDIFF_SKU9(String DIFF_SKU9) {
        this.DIFF_SKU9 = DIFF_SKU9;
    }

    public String getDIFF_SKU10() {
        return DIFF_SKU10;
    }

    public void setDIFF_SKU10(String DIFF_SKU10) {
        this.DIFF_SKU10 = DIFF_SKU10;
    }

    public String getDIFF_SKU11() {
        return DIFF_SKU11;
    }

    public void setDIFF_SKU11(String DIFF_SKU11) {
        this.DIFF_SKU11 = DIFF_SKU11;
    }

    public String getDIFF_SKU12() {
        return DIFF_SKU12;
    }

    public void setDIFF_SKU12(String DIFF_SKU12) {
        this.DIFF_SKU12 = DIFF_SKU12;
    }

    public String getDIFF_SKU1_PER() {
        return DIFF_SKU1_PER;
    }

    public void setDIFF_SKU1_PER(String DIFF_SKU1_PER) {
        this.DIFF_SKU1_PER = DIFF_SKU1_PER;
    }

    public String getDIFF_SKU2_PER() {
        return DIFF_SKU2_PER;
    }

    public void setDIFF_SKU2_PER(String DIFF_SKU2_PER) {
        this.DIFF_SKU2_PER = DIFF_SKU2_PER;
    }

    public String getDIFF_SKU3_PER() {
        return DIFF_SKU3_PER;
    }

    public void setDIFF_SKU3_PER(String DIFF_SKU3_PER) {
        this.DIFF_SKU3_PER = DIFF_SKU3_PER;
    }

    public String getDIFF_SKU4_PER() {
        return DIFF_SKU4_PER;
    }

    public void setDIFF_SKU4_PER(String DIFF_SKU4_PER) {
        this.DIFF_SKU4_PER = DIFF_SKU4_PER;
    }

    public String getDIFF_SKU5_PER() {
        return DIFF_SKU5_PER;
    }

    public void setDIFF_SKU5_PER(String DIFF_SKU5_PER) {
        this.DIFF_SKU5_PER = DIFF_SKU5_PER;
    }

    public String getDIFF_SKU6_PER() {
        return DIFF_SKU6_PER;
    }

    public void setDIFF_SKU6_PER(String DIFF_SKU6_PER) {
        this.DIFF_SKU6_PER = DIFF_SKU6_PER;
    }

    public String getDIFF_SKU7_PER() {
        return DIFF_SKU7_PER;
    }

    public void setDIFF_SKU7_PER(String DIFF_SKU7_PER) {
        this.DIFF_SKU7_PER = DIFF_SKU7_PER;
    }

    public String getDIFF_SKU8_PER() {
        return DIFF_SKU8_PER;
    }

    public void setDIFF_SKU8_PER(String DIFF_SKU8_PER) {
        this.DIFF_SKU8_PER = DIFF_SKU8_PER;
    }

    public String getDIFF_SKU9_PER() {
        return DIFF_SKU9_PER;
    }

    public void setDIFF_SKU9_PER(String DIFF_SKU9_PER) {
        this.DIFF_SKU9_PER = DIFF_SKU9_PER;
    }

    public String getDIFF_SKU10_PER() {
        return DIFF_SKU10_PER;
    }

    public void setDIFF_SKU10_PER(String DIFF_SKU10_PER) {
        this.DIFF_SKU10_PER = DIFF_SKU10_PER;
    }

    public String getDIFF_SKU11_PER() {
        return DIFF_SKU11_PER;
    }

    public void setDIFF_SKU11_PER(String DIFF_SKU11_PER) {
        this.DIFF_SKU11_PER = DIFF_SKU11_PER;
    }

    public String getDIFF_SKU12_PER() {
        return DIFF_SKU12_PER;
    }

    public void setDIFF_SKU12_PER(String DIFF_SKU12_PER) {
        this.DIFF_SKU12_PER = DIFF_SKU12_PER;
    }

    public String getWH() {
        return WH;
    }

    public void setWH(String WH) {
        this.WH = WH;
    }

    public String getDEPT() {
        return DEPT;
    }

    public void setDEPT(String DEPT) {
        this.DEPT = DEPT;
    }

    public String getPRODUCT() {
        return PRODUCT;
    }

    public void setPRODUCT(String PRODUCT) {
        this.PRODUCT = PRODUCT;
    }

    public String getSUBNAME() {
        return SUBNAME;
    }

    public void setSUBNAME(String SUBNAME) {
        this.SUBNAME = SUBNAME;
    }

    public String getNAME() {
        return NAME;
    }

    public void setNAME(String NAME) {
        this.NAME = NAME;
    }

    public String getOH_SKU() {
        return OH_SKU;
    }

    public void setOH_SKU(String OH_SKU) {
        this.OH_SKU = OH_SKU;
    }

    public String getCK_SKU() {
        return CK_SKU;
    }

    public void setCK_SKU(String CK_SKU) {
        this.CK_SKU = CK_SKU;
    }

    public String getCK_SKU_PER() {
        return CK_SKU_PER;
    }

    public void setCK_SKU_PER(String CK_SKU_PER) {
        this.CK_SKU_PER = CK_SKU_PER;
    }

    public String getCOL_SKU() {
        return COL_SKU;
    }

    public void setCOL_SKU(String COL_SKU) {
        this.COL_SKU = COL_SKU;
    }

    public String getCOL_SKU_PER() {
        return COL_SKU_PER;
    }

    public void setCOL_SKU_PER(String COL_SKU_PER) {
        this.COL_SKU_PER = COL_SKU_PER;
    }

    public String getDIFF_SKU() {
        return DIFF_SKU;
    }

    public void setDIFF_SKU(String DIFF_SKU) {
        this.DIFF_SKU = DIFF_SKU;
    }

    public String getDIFF_SKU_PER() {
        return DIFF_SKU_PER;
    }

    public void setDIFF_SKU_PER(String DIFF_SKU_PER) {
        this.DIFF_SKU_PER = DIFF_SKU_PER;
    }
}
