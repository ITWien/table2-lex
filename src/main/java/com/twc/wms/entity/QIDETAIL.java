/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.entity;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author nutthawoot.noo
 */
public class QIDETAIL {

    private String wh;
    private String qno;
    private String no;
    private String matc;
    private String desc;
    private String id;
    private String qty;
    private String um;
    private String pack;
    private String remark;
    private String status;
    private String GID;
    private String ckBox;
    private String mvt;
    private String mvtn;
    private String box;
    private String bag;
    private String roll;
    private String m3;
    private String sts;
    private String runno;
    private String shipmentDate;
    private String docno;
    private String transporter;
    private String sd;
    private List<QIDETAIL> subQNO = new ArrayList<QIDETAIL>();

    public QIDETAIL() {

    }

    public String getSd() {
        return sd;
    }

    public void setSd(String sd) {
        this.sd = sd;
    }

    public List<QIDETAIL> getSubQNO() {
        return subQNO;
    }

    public void setSubQNO(List<QIDETAIL> subQNO) {
        this.subQNO = subQNO;
    }

    public String getDocno() {
        return docno;
    }

    public void setDocno(String docno) {
        this.docno = docno;
    }

    public String getTransporter() {
        return transporter;
    }

    public void setTransporter(String transporter) {
        this.transporter = transporter;
    }

    public void setMvtn(String mvtn) {
        this.mvtn = mvtn;
    }

    public String getMvtn() {
        return mvtn;
    }

    public void setShipmentDate(String shipmentDate) {
        this.shipmentDate = shipmentDate;
    }

    public String getShipmentDate() {
        return shipmentDate;
    }

    public void setRunno(String runno) {
        this.runno = runno;
    }

    public String getRunno() {
        return runno;
    }

    public void setSts(String sts) {
        this.sts = sts;
    }

    public String getSts() {
        return sts;
    }

    public void setM3(String m3) {
        this.m3 = m3;
    }

    public String getM3() {
        return m3;
    }

    public void setBox(String box) {
        this.box = box;
    }

    public void setBag(String bag) {
        this.bag = bag;
    }

    public void setRoll(String roll) {
        this.roll = roll;
    }

    public String getBox() {
        return box;
    }

    public String getBag() {
        return bag;
    }

    public String getRoll() {
        return roll;
    }

    public void setMvt(String mvt) {
        this.mvt = mvt;
    }

    public String getMvt() {
        return mvt;
    }

    public void setCkBox(String ckBox) {
        this.ckBox = ckBox;
    }

    public String getCkBox() {
        return ckBox;
    }

    public String getGID() {
        return GID;
    }

    public void setGID(String GID) {
        this.GID = GID;
    }

    public String getWh() {
        return wh;
    }

    public String getQno() {
        return qno;
    }

    public String getNo() {
        return no;
    }

    public String getMatc() {
        return matc;
    }

    public String getDesc() {
        return desc;
    }

    public String getId() {
        return id;
    }

    public String getQty() {
        return qty;
    }

    public String getUm() {
        return um;
    }

    public String getPack() {
        return pack;
    }

    public String getRemark() {
        return remark;
    }

    public String getStatus() {
        return status;
    }

    public void setWh(String wh) {
        this.wh = wh;
    }

    public void setQno(String qno) {
        this.qno = qno;
    }

    public void setNo(String no) {
        this.no = no;
    }

    public void setMatc(String matc) {
        this.matc = matc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public void setUm(String um) {
        this.um = um;
    }

    public void setPack(String pack) {
        this.pack = pack;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public QIDETAIL(String wh, String qno, String no, String matc, String desc, String GID, String id, String qty, String um, String pack, String remark, String status) {
        this.wh = wh;
        this.qno = qno;
        this.no = no;
        this.matc = matc;
        this.desc = desc;
        this.GID = GID;
        this.id = id;
        this.qty = qty;
        this.um = um;
        this.pack = pack;
        this.remark = remark;
        this.status = status;
    }

}
