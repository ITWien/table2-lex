/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.entity;

import java.util.ArrayList;
import com.twc.wms.entity.QIDETAIL;
import java.util.List;

/**
 *
 * @author nutthawoot.noo
 */
public class XMSBRDETAIL {

    private String XMSBRDQNO;
    private String XMSBRDLINE;
    private String XMSBRDDEST;
    private String XMSBRDDESTN;
    private String XMSBRDDRIVER;
    private String XMSBRDFOLLOWER;
    private String XMSBRDBAG1;
    private String XMSBRDROLL1;
    private String XMSBRDBOX1;
    private String XMSBRDPCS1;
    private String XMSBRDTOT1;
    private String XMSBRDBAG2;
    private String XMSBRDROLL2;
    private String XMSBRDBOX2;
    private String XMSBRDPCS2;
    private String XMSBRDTOT2;
    private String XMSBRDBAG3;
    private String XMSBRDROLL3;
    private String XMSBRDBOX3;
    private String XMSBRDPCS3;
    private String XMSBRDTOT3;
    private String XMSBRDBAG;
    private String XMSBRDROLL;
    private String XMSBRDBOX;
    private String XMSBRDPCS;
    private String XMSBRDTOT;
    private String XMSBRDUSER;
    private String borderTop;

    public XMSBRDETAIL() {

    }

    public String getXMSBRDUSER() {
        return XMSBRDUSER;
    }

    public void setXMSBRDUSER(String XMSBRDUSER) {
        this.XMSBRDUSER = XMSBRDUSER;
    }

    public XMSBRDETAIL(String XMSBRDQNO, String XMSBRDLINE, String XMSBRDDEST, String XMSBRDDESTN, String XMSBRDDRIVER, String XMSBRDFOLLOWER, String XMSBRDBAG1, String XMSBRDROLL1, String XMSBRDBOX1, String XMSBRDPCS1, String XMSBRDTOT1, String XMSBRDBAG2, String XMSBRDROLL2, String XMSBRDBOX2, String XMSBRDPCS2, String XMSBRDTOT2, String XMSBRDBAG3, String XMSBRDROLL3, String XMSBRDBOX3, String XMSBRDPCS3, String XMSBRDTOT3, String XMSBRDBAG, String XMSBRDROLL, String XMSBRDBOX, String XMSBRDPCS, String XMSBRDTOT, String XMSBRDUSER) {
        this.XMSBRDQNO = XMSBRDQNO;
        this.XMSBRDLINE = XMSBRDLINE;
        this.XMSBRDDEST = XMSBRDDEST;
        this.XMSBRDDESTN = XMSBRDDESTN;
        this.XMSBRDDRIVER = XMSBRDDRIVER;
        this.XMSBRDFOLLOWER = XMSBRDFOLLOWER;
        this.XMSBRDBAG1 = XMSBRDBAG1;
        this.XMSBRDROLL1 = XMSBRDROLL1;
        this.XMSBRDBOX1 = XMSBRDBOX1;
        this.XMSBRDPCS1 = XMSBRDPCS1;
        this.XMSBRDTOT1 = XMSBRDTOT1;
        this.XMSBRDBAG2 = XMSBRDBAG2;
        this.XMSBRDROLL2 = XMSBRDROLL2;
        this.XMSBRDBOX2 = XMSBRDBOX2;
        this.XMSBRDPCS2 = XMSBRDPCS2;
        this.XMSBRDTOT2 = XMSBRDTOT2;
        this.XMSBRDBAG3 = XMSBRDBAG3;
        this.XMSBRDROLL3 = XMSBRDROLL3;
        this.XMSBRDBOX3 = XMSBRDBOX3;
        this.XMSBRDPCS3 = XMSBRDPCS3;
        this.XMSBRDTOT3 = XMSBRDTOT3;
        this.XMSBRDBAG = XMSBRDBAG;
        this.XMSBRDROLL = XMSBRDROLL;
        this.XMSBRDBOX = XMSBRDBOX;
        this.XMSBRDPCS = XMSBRDPCS;
        this.XMSBRDTOT = XMSBRDTOT;
        this.XMSBRDUSER = XMSBRDUSER;
    }

    public String getBorderTop() {
        return borderTop;
    }

    public void setBorderTop(String borderTop) {
        this.borderTop = borderTop;
    }

    public String getXMSBRDQNO() {
        return XMSBRDQNO;
    }

    public void setXMSBRDQNO(String XMSBRDQNO) {
        this.XMSBRDQNO = XMSBRDQNO;
    }

    public String getXMSBRDLINE() {
        return XMSBRDLINE;
    }

    public void setXMSBRDLINE(String XMSBRDLINE) {
        this.XMSBRDLINE = XMSBRDLINE;
    }

    public String getXMSBRDDEST() {
        return XMSBRDDEST;
    }

    public void setXMSBRDDEST(String XMSBRDDEST) {
        this.XMSBRDDEST = XMSBRDDEST;
    }

    public String getXMSBRDDESTN() {
        return XMSBRDDESTN;
    }

    public void setXMSBRDDESTN(String XMSBRDDESTN) {
        this.XMSBRDDESTN = XMSBRDDESTN;
    }

    public String getXMSBRDDRIVER() {
        return XMSBRDDRIVER;
    }

    public void setXMSBRDDRIVER(String XMSBRDDRIVER) {
        this.XMSBRDDRIVER = XMSBRDDRIVER;
    }

    public String getXMSBRDFOLLOWER() {
        return XMSBRDFOLLOWER;
    }

    public void setXMSBRDFOLLOWER(String XMSBRDFOLLOWER) {
        this.XMSBRDFOLLOWER = XMSBRDFOLLOWER;
    }

    public String getXMSBRDBAG1() {
        return XMSBRDBAG1;
    }

    public void setXMSBRDBAG1(String XMSBRDBAG1) {
        this.XMSBRDBAG1 = XMSBRDBAG1;
    }

    public String getXMSBRDROLL1() {
        return XMSBRDROLL1;
    }

    public void setXMSBRDROLL1(String XMSBRDROLL1) {
        this.XMSBRDROLL1 = XMSBRDROLL1;
    }

    public String getXMSBRDBOX1() {
        return XMSBRDBOX1;
    }

    public void setXMSBRDBOX1(String XMSBRDBOX1) {
        this.XMSBRDBOX1 = XMSBRDBOX1;
    }

    public String getXMSBRDPCS1() {
        return XMSBRDPCS1;
    }

    public void setXMSBRDPCS1(String XMSBRDPCS1) {
        this.XMSBRDPCS1 = XMSBRDPCS1;
    }

    public String getXMSBRDTOT1() {
        return XMSBRDTOT1;
    }

    public void setXMSBRDTOT1(String XMSBRDTOT1) {
        this.XMSBRDTOT1 = XMSBRDTOT1;
    }

    public String getXMSBRDBAG2() {
        return XMSBRDBAG2;
    }

    public void setXMSBRDBAG2(String XMSBRDBAG2) {
        this.XMSBRDBAG2 = XMSBRDBAG2;
    }

    public String getXMSBRDROLL2() {
        return XMSBRDROLL2;
    }

    public void setXMSBRDROLL2(String XMSBRDROLL2) {
        this.XMSBRDROLL2 = XMSBRDROLL2;
    }

    public String getXMSBRDBOX2() {
        return XMSBRDBOX2;
    }

    public void setXMSBRDBOX2(String XMSBRDBOX2) {
        this.XMSBRDBOX2 = XMSBRDBOX2;
    }

    public String getXMSBRDPCS2() {
        return XMSBRDPCS2;
    }

    public void setXMSBRDPCS2(String XMSBRDPCS2) {
        this.XMSBRDPCS2 = XMSBRDPCS2;
    }

    public String getXMSBRDTOT2() {
        return XMSBRDTOT2;
    }

    public void setXMSBRDTOT2(String XMSBRDTOT2) {
        this.XMSBRDTOT2 = XMSBRDTOT2;
    }

    public String getXMSBRDBAG3() {
        return XMSBRDBAG3;
    }

    public void setXMSBRDBAG3(String XMSBRDBAG3) {
        this.XMSBRDBAG3 = XMSBRDBAG3;
    }

    public String getXMSBRDROLL3() {
        return XMSBRDROLL3;
    }

    public void setXMSBRDROLL3(String XMSBRDROLL3) {
        this.XMSBRDROLL3 = XMSBRDROLL3;
    }

    public String getXMSBRDBOX3() {
        return XMSBRDBOX3;
    }

    public void setXMSBRDBOX3(String XMSBRDBOX3) {
        this.XMSBRDBOX3 = XMSBRDBOX3;
    }

    public String getXMSBRDPCS3() {
        return XMSBRDPCS3;
    }

    public void setXMSBRDPCS3(String XMSBRDPCS3) {
        this.XMSBRDPCS3 = XMSBRDPCS3;
    }

    public String getXMSBRDTOT3() {
        return XMSBRDTOT3;
    }

    public void setXMSBRDTOT3(String XMSBRDTOT3) {
        this.XMSBRDTOT3 = XMSBRDTOT3;
    }

    public String getXMSBRDBAG() {
        return XMSBRDBAG;
    }

    public void setXMSBRDBAG(String XMSBRDBAG) {
        this.XMSBRDBAG = XMSBRDBAG;
    }

    public String getXMSBRDROLL() {
        return XMSBRDROLL;
    }

    public void setXMSBRDROLL(String XMSBRDROLL) {
        this.XMSBRDROLL = XMSBRDROLL;
    }

    public String getXMSBRDBOX() {
        return XMSBRDBOX;
    }

    public void setXMSBRDBOX(String XMSBRDBOX) {
        this.XMSBRDBOX = XMSBRDBOX;
    }

    public String getXMSBRDPCS() {
        return XMSBRDPCS;
    }

    public void setXMSBRDPCS(String XMSBRDPCS) {
        this.XMSBRDPCS = XMSBRDPCS;
    }

    public String getXMSBRDTOT() {
        return XMSBRDTOT;
    }

    public void setXMSBRDTOT(String XMSBRDTOT) {
        this.XMSBRDTOT = XMSBRDTOT;
    }

}
