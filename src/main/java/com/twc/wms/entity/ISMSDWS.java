/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.entity;

/**
 *
 * @author 93176
 */
public class ISMSDWS {

    private String ISSCONO;
    private String ISSMTNO;
    private String ISSPLANT;
    private String ISSSTORE;
    private String ISSBTNO;
    private String ISSMTGP;
    private String ISSSBIN;
    private String ISSMDES;
    private String ISSBUOM;
    private String ISSUNR;
    private String ISSQI;
    private String ISSBLOCK;
    private String ISSTOTQ;
    private String ISSMAVG;
    private String ISSTOTA;
    private String ISSEDT;
    private String ISSETM;
    private String ISSEUSR;
    private String ISSCDT;
    private String ISSCTM;
    private String ISSUSER;

    public ISMSDWS() {
    }

    public ISMSDWS(String ISSCONO, String ISSMTNO, String ISSPLANT, String ISSSTORE, String ISSBTNO, String ISSMTGP, String ISSSBIN, String ISSMDES, String ISSBUOM, String ISSUNR, String ISSQI, String ISSBLOCK, String ISSTOTQ, String ISSMAVG, String ISSTOTA, String ISSEDT, String ISSETM, String ISSEUSR, String ISSCDT, String ISSCTM, String ISSUSER) {
        this.ISSCONO = ISSCONO;
        this.ISSMTNO = ISSMTNO;
        this.ISSPLANT = ISSPLANT;
        this.ISSSTORE = ISSSTORE;
        this.ISSBTNO = ISSBTNO;
        this.ISSMTGP = ISSMTGP;
        this.ISSSBIN = ISSSBIN;
        this.ISSMDES = ISSMDES;
        this.ISSBUOM = ISSBUOM;
        this.ISSUNR = ISSUNR;
        this.ISSQI = ISSQI;
        this.ISSBLOCK = ISSBLOCK;
        this.ISSTOTQ = ISSTOTQ;
        this.ISSMAVG = ISSMAVG;
        this.ISSTOTA = ISSTOTA;
        this.ISSEDT = ISSEDT;
        this.ISSETM = ISSETM;
        this.ISSEUSR = ISSEUSR;
        this.ISSCDT = ISSCDT;
        this.ISSCTM = ISSCTM;
        this.ISSUSER = ISSUSER;
    }

    public String getISSCONO() {
        return ISSCONO;
    }

    public void setISSCONO(String ISSCONO) {
        this.ISSCONO = ISSCONO;
    }

    public String getISSMTNO() {
        return ISSMTNO;
    }

    public void setISSMTNO(String ISSMTNO) {
        this.ISSMTNO = ISSMTNO;
    }

    public String getISSPLANT() {
        return ISSPLANT;
    }

    public void setISSPLANT(String ISSPLANT) {
        this.ISSPLANT = ISSPLANT;
    }

    public String getISSSTORE() {
        return ISSSTORE;
    }

    public void setISSSTORE(String ISSSTORE) {
        this.ISSSTORE = ISSSTORE;
    }

    public String getISSBTNO() {
        return ISSBTNO;
    }

    public void setISSBTNO(String ISSBTNO) {
        this.ISSBTNO = ISSBTNO;
    }

    public String getISSMTGP() {
        return ISSMTGP;
    }

    public void setISSMTGP(String ISSMTGP) {
        this.ISSMTGP = ISSMTGP;
    }

    public String getISSSBIN() {
        return ISSSBIN;
    }

    public void setISSSBIN(String ISSSBIN) {
        this.ISSSBIN = ISSSBIN;
    }

    public String getISSMDES() {
        return ISSMDES;
    }

    public void setISSMDES(String ISSMDES) {
        this.ISSMDES = ISSMDES;
    }

    public String getISSBUOM() {
        return ISSBUOM;
    }

    public void setISSBUOM(String ISSBUOM) {
        this.ISSBUOM = ISSBUOM;
    }

    public String getISSUNR() {
        return ISSUNR;
    }

    public void setISSUNR(String ISSUNR) {
        this.ISSUNR = ISSUNR;
    }

    public String getISSQI() {
        return ISSQI;
    }

    public void setISSQI(String ISSQI) {
        this.ISSQI = ISSQI;
    }

    public String getISSBLOCK() {
        return ISSBLOCK;
    }

    public void setISSBLOCK(String ISSBLOCK) {
        this.ISSBLOCK = ISSBLOCK;
    }

    public String getISSTOTQ() {
        return ISSTOTQ;
    }

    public void setISSTOTQ(String ISSTOTQ) {
        this.ISSTOTQ = ISSTOTQ;
    }

    public String getISSMAVG() {
        return ISSMAVG;
    }

    public void setISSMAVG(String ISSMAVG) {
        this.ISSMAVG = ISSMAVG;
    }

    public String getISSTOTA() {
        return ISSTOTA;
    }

    public void setISSTOTA(String ISSTOTA) {
        this.ISSTOTA = ISSTOTA;
    }

    public String getISSEDT() {
        return ISSEDT;
    }

    public void setISSEDT(String ISSEDT) {
        this.ISSEDT = ISSEDT;
    }

    public String getISSETM() {
        return ISSETM;
    }

    public void setISSETM(String ISSETM) {
        this.ISSETM = ISSETM;
    }

    public String getISSEUSR() {
        return ISSEUSR;
    }

    public void setISSEUSR(String ISSEUSR) {
        this.ISSEUSR = ISSEUSR;
    }

    public String getISSCDT() {
        return ISSCDT;
    }

    public void setISSCDT(String ISSCDT) {
        this.ISSCDT = ISSCDT;
    }

    public String getISSCTM() {
        return ISSCTM;
    }

    public void setISSCTM(String ISSCTM) {
        this.ISSCTM = ISSCTM;
    }

    public String getISSUSER() {
        return ISSUSER;
    }

    public void setISSUSER(String ISSUSER) {
        this.ISSUSER = ISSUSER;
    }

}
