/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.entity;

import java.util.ArrayList;
import com.twc.wms.entity.QIDETAIL;
import java.util.List;

/**
 *
 * @author nutthawoot.noo
 */
public class XMSOTDETAIL {

    private String XMSOTDQNO;
    private String XMSOTDLINE;
    private String XMSOTDSENDER;
    private String XMSOTDDEPT;
    private String XMSOTDDESC;
    private String XMSOTDBAG;
    private String XMSOTDROLL;
    private String XMSOTDBOX;
    private String XMSOTDPCS;
    private String XMSOTDTOT;
    private String XMSOTDTOTDOC;
    private String XMSOTDPONO;
    private String XMSOTDINVNO;
    private String XMSOTDDELNO;
    private String XMSOTDAMT;
    private String XMSOTDUSER;
    private String XMSOTDREMARK;
    private String XMSOTDDOCNO;
    private String XMSOTDRECEIVER;
    private String deptn;

    public XMSOTDETAIL() {

    }

    public String getXMSOTDREMARK() {
        return XMSOTDREMARK;
    }

    public void setXMSOTDREMARK(String XMSOTDREMARK) {
        this.XMSOTDREMARK = XMSOTDREMARK;
    }

    public String getXMSOTDDOCNO() {
        return XMSOTDDOCNO;
    }

    public void setXMSOTDDOCNO(String XMSOTDDOCNO) {
        this.XMSOTDDOCNO = XMSOTDDOCNO;
    }

    public String getXMSOTDRECEIVER() {
        return XMSOTDRECEIVER;
    }

    public void setXMSOTDRECEIVER(String XMSOTDRECEIVER) {
        this.XMSOTDRECEIVER = XMSOTDRECEIVER;
    }

    public XMSOTDETAIL(String XMSOTDQNO, String XMSOTDLINE, String XMSOTDSENDER, String XMSOTDDEPT, String XMSOTDDESC, String XMSOTDBAG, String XMSOTDROLL, String XMSOTDBOX, String XMSOTDPCS, String XMSOTDTOT, String XMSOTDTOTDOC, String XMSOTDPONO, String XMSOTDINVNO, String XMSOTDDELNO, String XMSOTDAMT, String XMSOTDUSER, String XMSOTDREMARK, String XMSOTDDOCNO, String XMSOTDRECEIVER) {
        this.XMSOTDQNO = XMSOTDQNO;
        this.XMSOTDLINE = XMSOTDLINE;
        this.XMSOTDSENDER = XMSOTDSENDER;
        this.XMSOTDDEPT = XMSOTDDEPT;
        this.XMSOTDDESC = XMSOTDDESC;
        this.XMSOTDBAG = XMSOTDBAG;
        this.XMSOTDROLL = XMSOTDROLL;
        this.XMSOTDBOX = XMSOTDBOX;
        this.XMSOTDPCS = XMSOTDPCS;
        this.XMSOTDTOT = XMSOTDTOT;
        this.XMSOTDTOTDOC = XMSOTDTOTDOC;
        this.XMSOTDPONO = XMSOTDPONO;
        this.XMSOTDINVNO = XMSOTDINVNO;
        this.XMSOTDDELNO = XMSOTDDELNO;
        this.XMSOTDAMT = XMSOTDAMT;
        this.XMSOTDUSER = XMSOTDUSER;
        this.XMSOTDREMARK = XMSOTDREMARK;
        this.XMSOTDDOCNO = XMSOTDDOCNO;
        this.XMSOTDRECEIVER = XMSOTDRECEIVER;
    }

    public String getDeptn() {
        return deptn;
    }

    public void setDeptn(String deptn) {
        this.deptn = deptn;
    }

    public String getXMSOTDQNO() {
        return XMSOTDQNO;
    }

    public void setXMSOTDQNO(String XMSOTDQNO) {
        this.XMSOTDQNO = XMSOTDQNO;
    }

    public String getXMSOTDLINE() {
        return XMSOTDLINE;
    }

    public void setXMSOTDLINE(String XMSOTDLINE) {
        this.XMSOTDLINE = XMSOTDLINE;
    }

    public String getXMSOTDSENDER() {
        return XMSOTDSENDER;
    }

    public void setXMSOTDSENDER(String XMSOTDSENDER) {
        this.XMSOTDSENDER = XMSOTDSENDER;
    }

    public String getXMSOTDDEPT() {
        return XMSOTDDEPT;
    }

    public void setXMSOTDDEPT(String XMSOTDDEPT) {
        this.XMSOTDDEPT = XMSOTDDEPT;
    }

    public String getXMSOTDDESC() {
        return XMSOTDDESC;
    }

    public void setXMSOTDDESC(String XMSOTDDESC) {
        this.XMSOTDDESC = XMSOTDDESC;
    }

    public String getXMSOTDBAG() {
        return XMSOTDBAG;
    }

    public void setXMSOTDBAG(String XMSOTDBAG) {
        this.XMSOTDBAG = XMSOTDBAG;
    }

    public String getXMSOTDROLL() {
        return XMSOTDROLL;
    }

    public void setXMSOTDROLL(String XMSOTDROLL) {
        this.XMSOTDROLL = XMSOTDROLL;
    }

    public String getXMSOTDBOX() {
        return XMSOTDBOX;
    }

    public void setXMSOTDBOX(String XMSOTDBOX) {
        this.XMSOTDBOX = XMSOTDBOX;
    }

    public String getXMSOTDPCS() {
        return XMSOTDPCS;
    }

    public void setXMSOTDPCS(String XMSOTDPCS) {
        this.XMSOTDPCS = XMSOTDPCS;
    }

    public String getXMSOTDTOT() {
        return XMSOTDTOT;
    }

    public void setXMSOTDTOT(String XMSOTDTOT) {
        this.XMSOTDTOT = XMSOTDTOT;
    }

    public String getXMSOTDTOTDOC() {
        return XMSOTDTOTDOC;
    }

    public void setXMSOTDTOTDOC(String XMSOTDTOTDOC) {
        this.XMSOTDTOTDOC = XMSOTDTOTDOC;
    }

    public String getXMSOTDPONO() {
        return XMSOTDPONO;
    }

    public void setXMSOTDPONO(String XMSOTDPONO) {
        this.XMSOTDPONO = XMSOTDPONO;
    }

    public String getXMSOTDINVNO() {
        return XMSOTDINVNO;
    }

    public void setXMSOTDINVNO(String XMSOTDINVNO) {
        this.XMSOTDINVNO = XMSOTDINVNO;
    }

    public String getXMSOTDDELNO() {
        return XMSOTDDELNO;
    }

    public void setXMSOTDDELNO(String XMSOTDDELNO) {
        this.XMSOTDDELNO = XMSOTDDELNO;
    }

    public String getXMSOTDAMT() {
        return XMSOTDAMT;
    }

    public void setXMSOTDAMT(String XMSOTDAMT) {
        this.XMSOTDAMT = XMSOTDAMT;
    }

    public String getXMSOTDUSER() {
        return XMSOTDUSER;
    }

    public void setXMSOTDUSER(String XMSOTDUSER) {
        this.XMSOTDUSER = XMSOTDUSER;
    }

}
