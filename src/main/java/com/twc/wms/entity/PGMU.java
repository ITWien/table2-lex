/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.entity;

/**
 *
 * @author nutthawoot.noo
 */
public class PGMU {

    private String uid;
    private String name;

    public PGMU() {

    }

    public PGMU(String uid, String name) {

        this.uid = uid;
        this.name = name;
    }

    public String getUid() {

        return uid;

    }
    
    public String getName() {

        return name;

    }

    public void setUid(String uid) {

        this.uid = uid;

    }
    
    public void setName(String name) {

        this.name = name;

    }

}
