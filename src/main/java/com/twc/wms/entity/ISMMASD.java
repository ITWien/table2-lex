/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.entity;

/**
 *
 * @author nutthawoot.noo
 */
public class ISMMASD {

    private String ISDCONO;
    private String ISDPLANT;
    private String ISDSTRG;
    private String ISDPRICE;
    private String ISDUNR01;
    private String ISDUNR03;
    private String ISDORD;
    private String LINO;
    private String ISDLINO;
    private String ISDVT;
    private String ISDSINO;
    private String ISDITNO;
    private String ISDDESC;
    private String ISDRQQTY;
    private String ISDUNIT;
    private String ISDSAPONH;
    private String ISDWMSONH;
    private String ISDMTCTRL;
    private String ISDRQDT;
    private String ISDDEST;
    private String ISDSTS;
    private String STS;
    private String STSI;
    private String FAM;
    private String MAT_NAME;
    private String ISDUSER;
    private String USR_NAME;
    private String ISDQNO;
    private String ISDSEQ;
    private String ISDRQTM;
    private String ISDAPDT;
    private String ISDAPTM;
    private String ISDAPUSER;
    private String ISDRCDT;
    private String ISDRCTM;
    private String ISDRCUSER;
    private String ISDPKQTY;
    private String ISDPKDT;
    private String ISDPKTM;
    private String ISDPKUSER;
    private String ISDTPQTY;
    private String ISDTPDT;
    private String ISDTPTM;
    private String ISDTPUSER;
    private String ISDJBNO;
    private String ISDREMARK;
    private String ISDISUNO;
    private String ISDTEMP;
    private String ISDEDT;
    private String ISDETM;
    private String ISDEUSR;
    private String ISDCDT;
    private String ISDCTM;
    private String ISDSTAD;
    private String ISDSTSTMP;
    private String ISDREMAIN;
    private String ISDPURG;

    public ISMMASD() {

    }

    public ISMMASD(String ISDCONO, String ISDPLANT, String ISDSTRG, String ISDPRICE, String ISDUNR01, String ISDUNR03, String ISDORD, String LINO, String ISDLINO, String ISDVT, String ISDSINO, String ISDITNO, String ISDDESC, String ISDRQQTY, String ISDUNIT, String ISDSAPONH, String ISDWMSONH, String ISDMTCTRL, String ISDRQDT, String ISDDEST, String ISDSTS, String STS, String STSI, String FAM, String MAT_NAME, String ISDUSER, String USR_NAME, String ISDQNO, String ISDSEQ, String ISDRQTM, String ISDAPDT, String ISDAPTM, String ISDAPUSER, String ISDRCDT, String ISDRCTM, String ISDRCUSER, String ISDPKQTY, String ISDPKDT, String ISDPKTM, String ISDPKUSER, String ISDTPQTY, String ISDTPDT, String ISDTPTM, String ISDTPUSER, String ISDJBNO, String ISDREMARK, String ISDISUNO, String ISDTEMP, String ISDEDT, String ISDETM, String ISDEUSR, String ISDCDT, String ISDCTM, String ISDSTAD, String ISDSTSTMP, String ISDREMAIN, String ISDPURG) {
        this.ISDCONO = ISDCONO;
        this.ISDPLANT = ISDPLANT;
        this.ISDSTRG = ISDSTRG;
        this.ISDPRICE = ISDPRICE;
        this.ISDUNR01 = ISDUNR01;
        this.ISDUNR03 = ISDUNR03;
        this.ISDORD = ISDORD;
        this.LINO = LINO;
        this.ISDLINO = ISDLINO;
        this.ISDVT = ISDVT;
        this.ISDSINO = ISDSINO;
        this.ISDITNO = ISDITNO;
        this.ISDDESC = ISDDESC;
        this.ISDRQQTY = ISDRQQTY;
        this.ISDUNIT = ISDUNIT;
        this.ISDSAPONH = ISDSAPONH;
        this.ISDWMSONH = ISDWMSONH;
        this.ISDMTCTRL = ISDMTCTRL;
        this.ISDRQDT = ISDRQDT;
        this.ISDDEST = ISDDEST;
        this.ISDSTS = ISDSTS;
        this.STS = STS;
        this.STSI = STSI;
        this.FAM = FAM;
        this.MAT_NAME = MAT_NAME;
        this.ISDUSER = ISDUSER;
        this.USR_NAME = USR_NAME;
        this.ISDQNO = ISDQNO;
        this.ISDSEQ = ISDSEQ;
        this.ISDRQTM = ISDRQTM;
        this.ISDAPDT = ISDAPDT;
        this.ISDAPTM = ISDAPTM;
        this.ISDAPUSER = ISDAPUSER;
        this.ISDRCDT = ISDRCDT;
        this.ISDRCTM = ISDRCTM;
        this.ISDRCUSER = ISDRCUSER;
        this.ISDPKQTY = ISDPKQTY;
        this.ISDPKDT = ISDPKDT;
        this.ISDPKTM = ISDPKTM;
        this.ISDPKUSER = ISDPKUSER;
        this.ISDTPQTY = ISDTPQTY;
        this.ISDTPDT = ISDTPDT;
        this.ISDTPTM = ISDTPTM;
        this.ISDTPUSER = ISDTPUSER;
        this.ISDJBNO = ISDJBNO;
        this.ISDREMARK = ISDREMARK;
        this.ISDISUNO = ISDISUNO;
        this.ISDTEMP = ISDTEMP;
        this.ISDEDT = ISDEDT;
        this.ISDETM = ISDETM;
        this.ISDEUSR = ISDEUSR;
        this.ISDCDT = ISDCDT;
        this.ISDCTM = ISDCTM;
        this.ISDSTAD = ISDSTAD;
        this.ISDSTSTMP = ISDSTSTMP;
        this.ISDREMAIN = ISDREMAIN;
        this.ISDPURG = ISDPURG;
    }

    public String getISDCONO() {
        return ISDCONO;
    }

    public void setISDCONO(String ISDCONO) {
        this.ISDCONO = ISDCONO;
    }

    public String getISDPLANT() {
        return ISDPLANT;
    }

    public void setISDPLANT(String ISDPLANT) {
        this.ISDPLANT = ISDPLANT;
    }

    public String getISDSTRG() {
        return ISDSTRG;
    }

    public void setISDSTRG(String ISDSTRG) {
        this.ISDSTRG = ISDSTRG;
    }

    public String getISDPRICE() {
        return ISDPRICE;
    }

    public void setISDPRICE(String ISDPRICE) {
        this.ISDPRICE = ISDPRICE;
    }

    public String getISDUNR01() {
        return ISDUNR01;
    }

    public void setISDUNR01(String ISDUNR01) {
        this.ISDUNR01 = ISDUNR01;
    }

    public String getISDUNR03() {
        return ISDUNR03;
    }

    public void setISDUNR03(String ISDUNR03) {
        this.ISDUNR03 = ISDUNR03;
    }

    public String getISDORD() {
        return ISDORD;
    }

    public void setISDORD(String ISDORD) {
        this.ISDORD = ISDORD;
    }

    public String getLINO() {
        return LINO;
    }

    public void setLINO(String LINO) {
        this.LINO = LINO;
    }

    public String getISDLINO() {
        return ISDLINO;
    }

    public void setISDLINO(String ISDLINO) {
        this.ISDLINO = ISDLINO;
    }

    public String getISDVT() {
        return ISDVT;
    }

    public void setISDVT(String ISDVT) {
        this.ISDVT = ISDVT;
    }

    public String getISDSINO() {
        return ISDSINO;
    }

    public void setISDSINO(String ISDSINO) {
        this.ISDSINO = ISDSINO;
    }

    public String getISDITNO() {
        return ISDITNO;
    }

    public void setISDITNO(String ISDITNO) {
        this.ISDITNO = ISDITNO;
    }

    public String getISDDESC() {
        return ISDDESC;
    }

    public void setISDDESC(String ISDDESC) {
        this.ISDDESC = ISDDESC;
    }

    public String getISDRQQTY() {
        return ISDRQQTY;
    }

    public void setISDRQQTY(String ISDRQQTY) {
        this.ISDRQQTY = ISDRQQTY;
    }

    public String getISDUNIT() {
        return ISDUNIT;
    }

    public void setISDUNIT(String ISDUNIT) {
        this.ISDUNIT = ISDUNIT;
    }

    public String getISDSAPONH() {
        return ISDSAPONH;
    }

    public void setISDSAPONH(String ISDSAPONH) {
        this.ISDSAPONH = ISDSAPONH;
    }

    public String getISDWMSONH() {
        return ISDWMSONH;
    }

    public void setISDWMSONH(String ISDWMSONH) {
        this.ISDWMSONH = ISDWMSONH;
    }

    public String getISDMTCTRL() {
        return ISDMTCTRL;
    }

    public void setISDMTCTRL(String ISDMTCTRL) {
        this.ISDMTCTRL = ISDMTCTRL;
    }

    public String getISDRQDT() {
        return ISDRQDT;
    }

    public void setISDRQDT(String ISDRQDT) {
        this.ISDRQDT = ISDRQDT;
    }

    public String getISDDEST() {
        return ISDDEST;
    }

    public void setISDDEST(String ISDDEST) {
        this.ISDDEST = ISDDEST;
    }

    public String getISDSTS() {
        return ISDSTS;
    }

    public void setISDSTS(String ISDSTS) {
        this.ISDSTS = ISDSTS;
    }

    public String getSTS() {
        return STS;
    }

    public void setSTS(String STS) {
        this.STS = STS;
    }

    public String getSTSI() {
        return STSI;
    }

    public void setSTSI(String STSI) {
        this.STSI = STSI;
    }

    public String getFAM() {
        return FAM;
    }

    public void setFAM(String FAM) {
        this.FAM = FAM;
    }

    public String getMAT_NAME() {
        return MAT_NAME;
    }

    public void setMAT_NAME(String MAT_NAME) {
        this.MAT_NAME = MAT_NAME;
    }

    public String getISDUSER() {
        return ISDUSER;
    }

    public void setISDUSER(String ISDUSER) {
        this.ISDUSER = ISDUSER;
    }

    public String getUSR_NAME() {
        return USR_NAME;
    }

    public void setUSR_NAME(String USR_NAME) {
        this.USR_NAME = USR_NAME;
    }

    public String getISDQNO() {
        return ISDQNO;
    }

    public void setISDQNO(String ISDQNO) {
        this.ISDQNO = ISDQNO;
    }

    public String getISDSEQ() {
        return ISDSEQ;
    }

    public void setISDSEQ(String ISDSEQ) {
        this.ISDSEQ = ISDSEQ;
    }

    public String getISDRQTM() {
        return ISDRQTM;
    }

    public void setISDRQTM(String ISDRQTM) {
        this.ISDRQTM = ISDRQTM;
    }

    public String getISDAPDT() {
        return ISDAPDT;
    }

    public void setISDAPDT(String ISDAPDT) {
        this.ISDAPDT = ISDAPDT;
    }

    public String getISDAPTM() {
        return ISDAPTM;
    }

    public void setISDAPTM(String ISDAPTM) {
        this.ISDAPTM = ISDAPTM;
    }

    public String getISDAPUSER() {
        return ISDAPUSER;
    }

    public void setISDAPUSER(String ISDAPUSER) {
        this.ISDAPUSER = ISDAPUSER;
    }

    public String getISDRCDT() {
        return ISDRCDT;
    }

    public void setISDRCDT(String ISDRCDT) {
        this.ISDRCDT = ISDRCDT;
    }

    public String getISDRCTM() {
        return ISDRCTM;
    }

    public void setISDRCTM(String ISDRCTM) {
        this.ISDRCTM = ISDRCTM;
    }

    public String getISDRCUSER() {
        return ISDRCUSER;
    }

    public void setISDRCUSER(String ISDRCUSER) {
        this.ISDRCUSER = ISDRCUSER;
    }

    public String getISDPKQTY() {
        return ISDPKQTY;
    }

    public void setISDPKQTY(String ISDPKQTY) {
        this.ISDPKQTY = ISDPKQTY;
    }

    public String getISDPKDT() {
        return ISDPKDT;
    }

    public void setISDPKDT(String ISDPKDT) {
        this.ISDPKDT = ISDPKDT;
    }

    public String getISDPKTM() {
        return ISDPKTM;
    }

    public void setISDPKTM(String ISDPKTM) {
        this.ISDPKTM = ISDPKTM;
    }

    public String getISDPKUSER() {
        return ISDPKUSER;
    }

    public void setISDPKUSER(String ISDPKUSER) {
        this.ISDPKUSER = ISDPKUSER;
    }

    public String getISDTPQTY() {
        return ISDTPQTY;
    }

    public void setISDTPQTY(String ISDTPQTY) {
        this.ISDTPQTY = ISDTPQTY;
    }

    public String getISDTPDT() {
        return ISDTPDT;
    }

    public void setISDTPDT(String ISDTPDT) {
        this.ISDTPDT = ISDTPDT;
    }

    public String getISDTPTM() {
        return ISDTPTM;
    }

    public void setISDTPTM(String ISDTPTM) {
        this.ISDTPTM = ISDTPTM;
    }

    public String getISDTPUSER() {
        return ISDTPUSER;
    }

    public void setISDTPUSER(String ISDTPUSER) {
        this.ISDTPUSER = ISDTPUSER;
    }

    public String getISDJBNO() {
        return ISDJBNO;
    }

    public void setISDJBNO(String ISDJBNO) {
        this.ISDJBNO = ISDJBNO;
    }

    public String getISDREMARK() {
        return ISDREMARK;
    }

    public void setISDREMARK(String ISDREMARK) {
        this.ISDREMARK = ISDREMARK;
    }

    public String getISDISUNO() {
        return ISDISUNO;
    }

    public void setISDISUNO(String ISDISUNO) {
        this.ISDISUNO = ISDISUNO;
    }

    public String getISDTEMP() {
        return ISDTEMP;
    }

    public void setISDTEMP(String ISDTEMP) {
        this.ISDTEMP = ISDTEMP;
    }

    public String getISDEDT() {
        return ISDEDT;
    }

    public void setISDEDT(String ISDEDT) {
        this.ISDEDT = ISDEDT;
    }

    public String getISDETM() {
        return ISDETM;
    }

    public void setISDETM(String ISDETM) {
        this.ISDETM = ISDETM;
    }

    public String getISDEUSR() {
        return ISDEUSR;
    }

    public void setISDEUSR(String ISDEUSR) {
        this.ISDEUSR = ISDEUSR;
    }

    public String getISDCDT() {
        return ISDCDT;
    }

    public void setISDCDT(String ISDCDT) {
        this.ISDCDT = ISDCDT;
    }

    public String getISDCTM() {
        return ISDCTM;
    }

    public void setISDCTM(String ISDCTM) {
        this.ISDCTM = ISDCTM;
    }

    public String getISDSTAD() {
        return ISDSTAD;
    }

    public void setISDSTAD(String ISDSTAD) {
        this.ISDSTAD = ISDSTAD;
    }

    public String getISDSTSTMP() {
        return ISDSTSTMP;
    }

    public void setISDSTSTMP(String ISDSTSTMP) {
        this.ISDSTSTMP = ISDSTSTMP;
    }

    public String getISDREMAIN() {
        return ISDREMAIN;
    }

    public void setISDREMAIN(String ISDREMAIN) {
        this.ISDREMAIN = ISDREMAIN;
    }

    public String getISDPURG() {
        return ISDPURG;
    }

    public void setISDPURG(String ISDPURG) {
        this.ISDPURG = ISDPURG;
    }

}
