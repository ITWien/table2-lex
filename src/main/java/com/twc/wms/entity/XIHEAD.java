package com.twc.wms.entity;

/**
 *
 * @author 93176
 */
public class XIHEAD {

    private String wh;
    private String transDate;
    private String queueNo;
    private String mvt;
    private String dest;
    private String user;
    private String set;
    private String setTotal;
    private String setRT;
    private String productGroup;
    private String matCtrl;
    private String status;
    private String histatus;
    private String totalQty;
    private String editOption;
    private String whn;
    private String mvtn;
    private String ckBox;

    public XIHEAD() {
    }

    public XIHEAD(String wh, String transDate, String queueNo, String mvt, String dest, String user, String set, String setTotal, String setRT, String productGroup, String matCtrl, String status, String histatus, String totalQty, String editOption, String whn, String mvtn, String ckBox) {
        this.wh = wh;
        this.transDate = transDate;
        this.queueNo = queueNo;
        this.mvt = mvt;
        this.dest = dest;
        this.user = user;
        this.set = set;
        this.setTotal = setTotal;
        this.setRT = setRT;
        this.productGroup = productGroup;
        this.matCtrl = matCtrl;
        this.status = status;
        this.histatus = histatus;
        this.totalQty = totalQty;
        this.editOption = editOption;
        this.whn = whn;
        this.mvtn = mvtn;
        this.ckBox = ckBox;
    }

    public String getWh() {
        return wh;
    }

    public void setWh(String wh) {
        this.wh = wh;
    }

    public String getTransDate() {
        return transDate;
    }

    public void setTransDate(String transDate) {
        this.transDate = transDate;
    }

    public String getQueueNo() {
        return queueNo;
    }

    public void setQueueNo(String queueNo) {
        this.queueNo = queueNo;
    }

    public String getMvt() {
        return mvt;
    }

    public void setMvt(String mvt) {
        this.mvt = mvt;
    }

    public String getDest() {
        return dest;
    }

    public void setDest(String dest) {
        this.dest = dest;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getSet() {
        return set;
    }

    public void setSet(String set) {
        this.set = set;
    }

    public String getSetTotal() {
        return setTotal;
    }

    public void setSetTotal(String setTotal) {
        this.setTotal = setTotal;
    }

    public String getSetRT() {
        return setRT;
    }

    public void setSetRT(String setRT) {
        this.setRT = setRT;
    }

    public String getProductGroup() {
        return productGroup;
    }

    public void setProductGroup(String productGroup) {
        this.productGroup = productGroup;
    }

    public String getMatCtrl() {
        return matCtrl;
    }

    public void setMatCtrl(String matCtrl) {
        this.matCtrl = matCtrl;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getHistatus() {
        return histatus;
    }

    public void setHistatus(String histatus) {
        this.histatus = histatus;
    }

    public String getTotalQty() {
        return totalQty;
    }

    public void setTotalQty(String totalQty) {
        this.totalQty = totalQty;
    }

    public String getEditOption() {
        return editOption;
    }

    public void setEditOption(String editOption) {
        this.editOption = editOption;
    }

    public String getWhn() {
        return whn;
    }

    public void setWhn(String whn) {
        this.whn = whn;
    }

    public String getMvtn() {
        return mvtn;
    }

    public void setMvtn(String mvtn) {
        this.mvtn = mvtn;
    }

    public String getCkBox() {
        return ckBox;
    }

    public void setCkBox(String ckBox) {
        this.ckBox = ckBox;
    }

}
