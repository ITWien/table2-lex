/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.entity;

/**
 *
 * @author nutthawoot.noo
 */
public class QDMBAG {

    private String checkBax;
    private String QDMSIZE;
    private String QDMDESC;
    private String QDMINWIDTH;
    private String QDMINLENGTH;
    private String QDMCMWIDTH;
    private String QDMCMLENGTH;
    private String QDMCDT;
    private String QDMUSER;
    private String option;

    public QDMBAG() {

    }

    public String getCheckBax() {
        return checkBax;
    }

    public void setCheckBax(String checkBax) {
        this.checkBax = checkBax;
    }

    public String getOption() {
        return option;
    }

    public void setOption(String option) {
        this.option = option;
    }

    public String getQDMSIZE() {
        return QDMSIZE;
    }

    public void setQDMSIZE(String QDMSIZE) {
        this.QDMSIZE = QDMSIZE;
    }

    public String getQDMDESC() {
        return QDMDESC;
    }

    public void setQDMDESC(String QDMDESC) {
        this.QDMDESC = QDMDESC;
    }

    public String getQDMINWIDTH() {
        return QDMINWIDTH;
    }

    public void setQDMINWIDTH(String QDMINWIDTH) {
        this.QDMINWIDTH = QDMINWIDTH;
    }

    public String getQDMINLENGTH() {
        return QDMINLENGTH;
    }

    public void setQDMINLENGTH(String QDMINLENGTH) {
        this.QDMINLENGTH = QDMINLENGTH;
    }

    public String getQDMCMWIDTH() {
        return QDMCMWIDTH;
    }

    public void setQDMCMWIDTH(String QDMCMWIDTH) {
        this.QDMCMWIDTH = QDMCMWIDTH;
    }

    public String getQDMCMLENGTH() {
        return QDMCMLENGTH;
    }

    public void setQDMCMLENGTH(String QDMCMLENGTH) {
        this.QDMCMLENGTH = QDMCMLENGTH;
    }

    public String getQDMCDT() {
        return QDMCDT;
    }

    public void setQDMCDT(String QDMCDT) {
        this.QDMCDT = QDMCDT;
    }

    public String getQDMUSER() {
        return QDMUSER;
    }

    public void setQDMUSER(String QDMUSER) {
        this.QDMUSER = QDMUSER;
    }

}
