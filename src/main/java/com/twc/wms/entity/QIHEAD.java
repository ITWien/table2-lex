/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.entity;

/**
 *
 * @author nutthawoot.noo
 */
public class QIHEAD {

    private String wh;
    private String transDate;
    private String queueNo;
    private String mvt;
    private String dest;
    private String user;
    private String set;
    private String setTotal;
    private String setRT;
    private String productGroup;
    private String matCtrl;
    private String status;
    private String histatus;
    private String totalQty;
    private String editOption;
    private String whn;
    private String mvtn;
    private String ckBox;

    public String getCkBox() {
        return ckBox;
    }

    public void setCkBox(String ckBox) {
        this.ckBox = ckBox;
    }

    public void setWhn(String whn) {
        this.whn = whn;
    }

    public void setMvtn(String mvtn) {
        this.mvtn = mvtn;
    }

    public String getWhn() {
        return whn;
    }

    public String getMvtn() {
        return mvtn;
    }

    public void setHistatus(String histatus) {
        this.histatus = histatus;
    }

    public String getHistatus() {
        return histatus;
    }

    public void setSetTotal(String setTotal) {
        this.setTotal = setTotal;
    }

    public void setSetRT(String setRT) {
        this.setRT = setRT;
    }

    public String getSetTotal() {
        return setTotal;
    }

    public String getSetRT() {
        return setRT;
    }

    public void setWh(String wh) {
        this.wh = wh;
    }

    public String getWh() {
        return wh;
    }

    public void setEditOption(String editOption) {
        this.editOption = editOption;
    }

    public String getEditOption() {
        return editOption;
    }

    public void setTransDate(String transDate) {
        this.transDate = transDate;
    }

    public void setQueueNo(String queueNo) {
        this.queueNo = queueNo;
    }

    public void setMvt(String mvt) {
        this.mvt = mvt;
    }

    public void setDest(String dest) {
        this.dest = dest;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public void setSet(String set) {
        this.set = set;
    }

    public void setProductGroup(String productGroup) {
        this.productGroup = productGroup;
    }

    public void setMatCtrl(String matCtrl) {
        this.matCtrl = matCtrl;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setTotalQty(String totalQty) {
        this.totalQty = totalQty;
    }

    public String getTransDate() {
        return transDate;
    }

    public String getQueueNo() {
        return queueNo;
    }

    public String getMvt() {
        return mvt;
    }

    public String getDest() {
        return dest;
    }

    public String getUser() {
        return user;
    }

    public String getSet() {
        return set;
    }

    public String getProductGroup() {
        return productGroup;
    }

    public String getMatCtrl() {
        return matCtrl;
    }

    public String getStatus() {
        return status;
    }

    public String getTotalQty() {
        return totalQty;
    }

    public QIHEAD(String transDate, String queueNo, String mvt, String dest, String user, String set, String productGroup, String matCtrl, String status, String totalQty) {
        this.transDate = transDate;
        this.queueNo = queueNo;
        this.mvt = mvt;
        this.dest = dest;
        this.user = user;
        this.set = set;
        this.productGroup = productGroup;
        this.matCtrl = matCtrl;
        this.status = status;
        this.totalQty = totalQty;
    }

    public QIHEAD() {

    }

}
