/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.entity;

/**
 *
 * @author 93176
 */
public class QWHSTR {

    private String Code;
    private String QWHSUBPROD;

    public QWHSTR() {
    }

    public QWHSTR(String Code, String QWHSUBPROD) {
        this.Code = Code;
        this.QWHSUBPROD = QWHSUBPROD;
    }

    public String getCode() {
        return Code;
    }

    public void setCode(String Code) {
        this.Code = Code;
    }

    public String getQWHSUBPROD() {
        return QWHSUBPROD;
    }

    public void setQWHSUBPROD(String QWHSUBPROD) {
        this.QWHSUBPROD = QWHSUBPROD;
    }

}
