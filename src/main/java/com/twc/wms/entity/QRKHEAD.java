/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.entity;

/**
 *
 * @author nutthawoot.noo
 */
public class QRKHEAD {

    private String code;//wh
    private String desc;//rkno
    private String name;//zone

    public QRKHEAD() {

    }

    public QRKHEAD(String code, String desc, String name) {

        this.code = code;
        this.desc = desc;
        this.name = name;

    }
    
    public QRKHEAD(String code, String desc) {

        this.code = code;
        this.desc = desc;

    }

    public String getCode() {

        return code;

    }

    public void setCode(String code) {

        this.code = code;

    }

    public String getDesc() {

        return desc;

    }

    public void setDesc(String desc) {

        this.desc = desc;

    }
    
    public String getName() {

        return name;

    }
    
    public void setName(String name) {

        this.name = name;

    }

}
