/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.twc.wms.entity;

/**
 *
 * @author 93176
 */
public class ISMPACK {

    private String GRPNO;
    private int ISSUENO;
    private String MATNR;
    private int NO;
    private String TYPE;
    private Double VALUE;
    private int STATUS;
    private int CHKUP;
    private String MATID;
    private String MTCTRL;
    private Double CHKDATE;
    private Double CHKTIME;

    public ISMPACK() {
    }

    public ISMPACK(String GRPNO, int ISSUENO, String MATNR, int NO, String TYPE, Double VALUE, int STATUS, int CHKUP, String MATID, String MTCTRL, Double CHKDATE, Double CHKTIME) {
        this.GRPNO = GRPNO;
        this.ISSUENO = ISSUENO;
        this.MATNR = MATNR;
        this.NO = NO;
        this.TYPE = TYPE;
        this.VALUE = VALUE;
        this.STATUS = STATUS;
        this.CHKUP = CHKUP;
        this.MATID = MATID;
        this.MTCTRL = MTCTRL;
        this.CHKDATE = CHKDATE;
        this.CHKTIME = CHKTIME;
    }

    public String getGRPNO() {
        return GRPNO;
    }

    public void setGRPNO(String GRPNO) {
        this.GRPNO = GRPNO;
    }

    public int getISSUENO() {
        return ISSUENO;
    }

    public void setISSUENO(int ISSUENO) {
        this.ISSUENO = ISSUENO;
    }

    public String getMATNR() {
        return MATNR;
    }

    public void setMATNR(String MATNR) {
        this.MATNR = MATNR;
    }

    public int getNO() {
        return NO;
    }

    public void setNO(int NO) {
        this.NO = NO;
    }

    public String getTYPE() {
        return TYPE;
    }

    public void setTYPE(String TYPE) {
        this.TYPE = TYPE;
    }

    public Double getVALUE() {
        return VALUE;
    }

    public void setVALUE(Double VALUE) {
        this.VALUE = VALUE;
    }

    public int getSTATUS() {
        return STATUS;
    }

    public void setSTATUS(int STATUS) {
        this.STATUS = STATUS;
    }

    public int getCHKUP() {
        return CHKUP;
    }

    public void setCHKUP(int CHKUP) {
        this.CHKUP = CHKUP;
    }

    public String getMATID() {
        return MATID;
    }

    public void setMATID(String MATID) {
        this.MATID = MATID;
    }

    public String getMTCTRL() {
        return MTCTRL;
    }

    public void setMTCTRL(String MTCTRL) {
        this.MTCTRL = MTCTRL;
    }

    public Double getCHKDATE() {
        return CHKDATE;
    }

    public void setCHKDATE(Double CHKDATE) {
        this.CHKDATE = CHKDATE;
    }

    public Double getCHKTIME() {
        return CHKTIME;
    }

    public void setCHKTIME(Double CHKTIME) {
        this.CHKTIME = CHKTIME;
    }

}
