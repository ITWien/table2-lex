/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.entity;

/**
 *
 * @author nutthawoot.noo
 */
public class QPGMAS_2 {

    private String uid;
    private String name;
    private String type;
    private String respon;
    private String sys;

    public QPGMAS_2() {

    }

    public QPGMAS_2(String uid, String name, String type, String respon) {

        this.uid = uid;
        this.name = name;
        this.type = type;
        this.respon = respon;
    }

    public String getSys() {
        return sys;
    }

    public void setSys(String sys) {
        this.sys = sys;
    }

    public String getUid() {

        return uid;

    }

    public String getName() {

        return name;

    }

    public String getType() {

        return type;

    }

    public String getRespon() {

        return respon;

    }

    public void setUid(String uid) {

        this.uid = uid;

    }

    public void setName(String name) {

        this.name = name;

    }

    public void setType(String type) {

        this.type = type;

    }

    public void setRespon(String respon) {

        this.respon = respon;

    }

}
