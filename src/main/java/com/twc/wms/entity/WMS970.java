/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.entity;

import java.util.ArrayList;
import com.twc.wms.entity.QIDETAIL;
import java.util.List;

/**
 *
 * @author nutthawoot.noo
 */
public class WMS970 {

    private String RM;
    private String COL;
    private String CODE;
    private String BAG;
    private String ROLL;
    private String BOX;
    private String TOTAL;
    private String QTY;
    private String ID;
    private String PACKTYPE;
    private String LOCATION;
    private String UNIT;
    private String DESCS;
    private String GRPDESCS;
    private int noi;
    private List<WMS970> detList;
    private List<String> imageList;
    private String pack;
    private String width;
    private String length;
    private String height;
    private String diameter;

    public WMS970() {

    }

    public String getPack() {
        return pack;
    }

    public void setPack(String pack) {
        this.pack = pack;
    }

    public String getWidth() {
        return width;
    }

    public void setWidth(String width) {
        this.width = width;
    }

    public String getLength() {
        return length;
    }

    public void setLength(String length) {
        this.length = length;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getDiameter() {
        return diameter;
    }

    public void setDiameter(String diameter) {
        this.diameter = diameter;
    }

    public int getNoi() {
        return noi;
    }

    public void setNoi(int noi) {
        this.noi = noi;
    }

    public List<String> getImageList() {
        return imageList;
    }

    public void setImageList(List<String> imageList) {
        this.imageList = imageList;
    }

    public String getDESCS() {
        return DESCS;
    }

    public void setDESCS(String DESCS) {
        this.DESCS = DESCS;
    }

    public String getGRPDESCS() {
        return GRPDESCS;
    }

    public void setGRPDESCS(String GRPDESCS) {
        this.GRPDESCS = GRPDESCS;
    }

    public String getUNIT() {
        return UNIT;
    }

    public void setUNIT(String UNIT) {
        this.UNIT = UNIT;
    }

    public String getPACKTYPE() {
        return PACKTYPE;
    }

    public void setPACKTYPE(String PACKTYPE) {
        this.PACKTYPE = PACKTYPE;
    }

    public String getLOCATION() {
        return LOCATION;
    }

    public void setLOCATION(String LOCATION) {
        this.LOCATION = LOCATION;
    }

    public String getRM() {
        return RM;
    }

    public void setRM(String RM) {
        this.RM = RM;
    }

    public String getCOL() {
        return COL;
    }

    public void setCOL(String COL) {
        this.COL = COL;
    }

    public String getCODE() {
        return CODE;
    }

    public void setCODE(String CODE) {
        this.CODE = CODE;
    }

    public String getBAG() {
        return BAG;
    }

    public void setBAG(String BAG) {
        this.BAG = BAG;
    }

    public String getROLL() {
        return ROLL;
    }

    public void setROLL(String ROLL) {
        this.ROLL = ROLL;
    }

    public String getBOX() {
        return BOX;
    }

    public void setBOX(String BOX) {
        this.BOX = BOX;
    }

    public String getTOTAL() {
        return TOTAL;
    }

    public void setTOTAL(String TOTAL) {
        this.TOTAL = TOTAL;
    }

    public String getQTY() {
        return QTY;
    }

    public void setQTY(String QTY) {
        this.QTY = QTY;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public List<WMS970> getDetList() {
        return detList;
    }

    public void setDetList(List<WMS970> detList) {
        this.detList = detList;
    }
}
