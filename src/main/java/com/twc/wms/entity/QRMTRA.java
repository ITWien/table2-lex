/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.entity;

/**
 *
 * @author nutthawoot.noo
 */
public class QRMTRA {

    private String matcode;
    private String matcode1;
    private String matcode2;
    private String matcode3;
    private String matcode4;
    private String matcode5;
    private String matcode6;
    private String matcode7;
    private String matcode8;
    private String matcode9;
    private String matcode10;
    private String matcode11;
    private String desc;
    private String plant;
    private String vtype;
    private String mtype;
    private String um;
    private String mgroup;
    private String mgdesc;
    private String pgroup;
    private String location;

    public QRMTRA() {

    }

    public QRMTRA(String matcode, String matcode1, String matcode2, String matcode3, 
            String matcode4, String matcode5, String matcode6, String matcode7, 
            String matcode8, String matcode9, 
            String matcode10, String matcode11, String desc, String plant,
            String vtype, String mtype, String um,
            String mgroup, String mgdesc, String pgroup, String location) {

        this.matcode = matcode;
        this.matcode1 = matcode1;
        this.matcode2 = matcode2;
        this.matcode3 = matcode3;
        this.matcode4 = matcode4;
        this.matcode5 = matcode5;
        this.matcode6 = matcode6;
        this.matcode7 = matcode7;
        this.matcode8 = matcode8;
        this.matcode9 = matcode9;
        this.matcode10 = matcode10;
        this.matcode11 = matcode11;
        this.desc = desc;
        this.plant = plant;
        this.vtype = vtype;
        this.mtype = mtype;
        this.um = um;
        this.mgroup = mgroup;
        this.mgdesc = mgdesc;
        this.pgroup = pgroup;
        this.location = location;
    }

    public String getMatcode() {

        return matcode;

    }

    public void setMatcode(String matcode) {

        this.matcode = matcode;

    }
    
    public String getMatcode1() {

        return matcode1;

    }

    public void setMatcode1(String matcode1) {

        this.matcode1 = matcode1;

    }
    
    public String getMatcode2() {

        return matcode2;

    }

    public void setMatcode2(String matcode2) {

        this.matcode2 = matcode2;

    }
    
    public String getMatcode3() {

        return matcode3;

    }

    public void setMatcode3(String matcode3) {

        this.matcode3 = matcode3;

    }
    
    public String getMatcode4() {

        return matcode4;

    }

    public void setMatcode4(String matcode4) {

        this.matcode4 = matcode4;

    }
    
    public String getMatcode5() {

        return matcode5;

    }

    public void setMatcode5(String matcode5) {

        this.matcode5 = matcode5;

    }
    
    public String getMatcode6() {

        return matcode6;

    }

    public void setMatcode6(String matcode6) {

        this.matcode6 = matcode6;

    }
    
    public String getMatcode7() {

        return matcode7;

    }

    public void setMatcode7(String matcode7) {

        this.matcode7 = matcode7;

    }
    
    public String getMatcode8() {

        return matcode8;

    }

    public void setMatcode8(String matcode8) {

        this.matcode8 = matcode8;

    }
    
    public String getMatcode9() {

        return matcode9;

    }

    public void setMatcode9(String matcode9) {

        this.matcode9 = matcode9;

    }
    
    public String getMatcode10() {

        return matcode10;

    }

    public void setMatcode10(String matcode10) {

        this.matcode10 = matcode10;

    }
    
    public String getMatcode11() {

        return matcode11;

    }

    public void setMatcode11(String matcode11) {

        this.matcode11 = matcode11;

    }

    public String getDesc() {

        return desc;

    }

    public void setDesc(String desc) {

        this.desc = desc;

    }

    public String getPlant() {

        return plant;

    }

    public void setPlant(String plant) {

        this.plant = plant;

    }

    public String getVtype() {

        return vtype;

    }

    public void setVtype(String vtype) {

        this.vtype = vtype;

    }

    public String getMtype() {

        return mtype;

    }

    public void setMtype(String mtype) {

        this.mtype = mtype;

    }

    public String getUm() {

        return um;

    }

    public void setUm(String um) {

        this.um = um;

    }

    public String getMgroup() {

        return mgroup;

    }

    public void setMgroup(String mgroup) {

        this.mgroup = mgroup;

    }

    public String getMgdesc() {

        return mgdesc;

    }

    public void setMgdesc(String mgdesc) {

        this.mgdesc = mgdesc;

    }
    
    public String getPgroup() {

        return pgroup;

    }

    public void setPgroup(String pgroup) {

        this.pgroup = pgroup;

    }
    
    public String getLocation() {

        return location;

    }

    public void setLocation(String location) {

        this.location = location;

    }
}
