
package com.twc.wms.entity;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author 93176
 */
public class XIDETAIL {

    private String wh;
    private String qno;
    private String no;
    private String matc;
    private String desc;
    private String id;
    private String qty;
    private String um;
    private String pack;
    private String remark;
    private String status;
    private String GID;
    private String ckBox;
    private String mvt;
    private String mvtn;
    private String box;
    private String bag;
    private String roll;
    private String m3;
    private String sts;
    private String runno;
    private String shipmentDate;
    private String docno;
    private String transporter;
    private String sd;
    private List<QIDETAIL> subQNO = new ArrayList<QIDETAIL>();

    public XIDETAIL() {
    }

    public XIDETAIL(String wh, String qno, String no, String matc, String desc, String id, String qty, String um, String pack, String remark, String status, String GID, String ckBox, String mvt, String mvtn, String box, String bag, String roll, String m3, String sts, String runno, String shipmentDate, String docno, String transporter, String sd) {
        this.wh = wh;
        this.qno = qno;
        this.no = no;
        this.matc = matc;
        this.desc = desc;
        this.id = id;
        this.qty = qty;
        this.um = um;
        this.pack = pack;
        this.remark = remark;
        this.status = status;
        this.GID = GID;
        this.ckBox = ckBox;
        this.mvt = mvt;
        this.mvtn = mvtn;
        this.box = box;
        this.bag = bag;
        this.roll = roll;
        this.m3 = m3;
        this.sts = sts;
        this.runno = runno;
        this.shipmentDate = shipmentDate;
        this.docno = docno;
        this.transporter = transporter;
        this.sd = sd;
    }

    public String getWh() {
        return wh;
    }

    public void setWh(String wh) {
        this.wh = wh;
    }

    public String getQno() {
        return qno;
    }

    public void setQno(String qno) {
        this.qno = qno;
    }

    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }

    public String getMatc() {
        return matc;
    }

    public void setMatc(String matc) {
        this.matc = matc;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public String getUm() {
        return um;
    }

    public void setUm(String um) {
        this.um = um;
    }

    public String getPack() {
        return pack;
    }

    public void setPack(String pack) {
        this.pack = pack;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getGID() {
        return GID;
    }

    public void setGID(String GID) {
        this.GID = GID;
    }

    public String getCkBox() {
        return ckBox;
    }

    public void setCkBox(String ckBox) {
        this.ckBox = ckBox;
    }

    public String getMvt() {
        return mvt;
    }

    public void setMvt(String mvt) {
        this.mvt = mvt;
    }

    public String getMvtn() {
        return mvtn;
    }

    public void setMvtn(String mvtn) {
        this.mvtn = mvtn;
    }

    public String getBox() {
        return box;
    }

    public void setBox(String box) {
        this.box = box;
    }

    public String getBag() {
        return bag;
    }

    public void setBag(String bag) {
        this.bag = bag;
    }

    public String getRoll() {
        return roll;
    }

    public void setRoll(String roll) {
        this.roll = roll;
    }

    public String getM3() {
        return m3;
    }

    public void setM3(String m3) {
        this.m3 = m3;
    }

    public String getSts() {
        return sts;
    }

    public void setSts(String sts) {
        this.sts = sts;
    }

    public String getRunno() {
        return runno;
    }

    public void setRunno(String runno) {
        this.runno = runno;
    }

    public String getShipmentDate() {
        return shipmentDate;
    }

    public void setShipmentDate(String shipmentDate) {
        this.shipmentDate = shipmentDate;
    }

    public String getDocno() {
        return docno;
    }

    public void setDocno(String docno) {
        this.docno = docno;
    }

    public String getTransporter() {
        return transporter;
    }

    public void setTransporter(String transporter) {
        this.transporter = transporter;
    }

    public String getSd() {
        return sd;
    }

    public void setSd(String sd) {
        this.sd = sd;
    }

    public List<QIDETAIL> getSubQNO() {
        return subQNO;
    }

    public void setSubQNO(List<QIDETAIL> subQNO) {
        this.subQNO = subQNO;
    }

}
