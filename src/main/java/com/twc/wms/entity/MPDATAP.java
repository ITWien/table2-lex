/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.entity;

/**
 *
 * @author nutthawoot.noo
 */
public class MPDATAP {

    private String VBELN;
    private String STYLE;
    private String COLOR;
    private String LOT;
    private float KWMENG;
    private int NO;
    private String TYPE;
    private String MATID;  //new mod
    private float VALUE;

    public void setVBELN(String VBELN) {
        this.VBELN = VBELN;
    }

    public String getVBELN() {
        return VBELN;
    }

    public void setSTYLE(String STYLE) {
        this.STYLE = STYLE;
    }

    public String getSTYLE() {
        return STYLE;
    }

    public void setCOLOR(String COLOR) {
        this.COLOR = COLOR;
    }

    public String getCOLOR() {
        return COLOR;
    }

    public void setLOT(String LOT) {
        this.LOT = LOT;
    }

    public String getLOT() {
        return LOT;
    }

    public void setKWMENG(float KWMENG) {
        this.KWMENG = KWMENG;
    }

    public float getKWMENG() {
        return KWMENG;
    }

    public void setNO(int NO) {
        this.NO = NO;
    }

    public int getNO() {
        return NO;
    }

    public void setTYPE(String TYPE) {
        this.TYPE = TYPE;
    }

    public String getTYPE() {
        return TYPE;
    }

    //new mod
    public void setMATID(String MATID) {
        this.MATID = MATID;
    }

    public String getMATID() {
        return MATID;
    }

    public void setVALUE(float VALUE) {
        this.VALUE = VALUE;
    }

    public float getVALUE() {
        return VALUE;
    }

}
