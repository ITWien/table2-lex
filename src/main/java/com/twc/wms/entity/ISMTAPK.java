/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.twc.wms.entity;

/**
 *
 * @author 93176
 */
public class ISMTAPK {

    private String GRPNO;
    private int ISSUENO;
    private String MATNR;
    private Double ISSUEPICK;
    private String ISSUEDATE;

    public ISMTAPK() {
    }

    public ISMTAPK(String GRPNO, int ISSUENO, String MATNR, Double ISSUEPICK, String ISSUEDATE) {
        this.GRPNO = GRPNO;
        this.ISSUENO = ISSUENO;
        this.MATNR = MATNR;
        this.ISSUEPICK = ISSUEPICK;
        this.ISSUEDATE = ISSUEDATE;
    }

    public String getGRPNO() {
        return GRPNO;
    }

    public void setGRPNO(String GRPNO) {
        this.GRPNO = GRPNO;
    }

    public int getISSUENO() {
        return ISSUENO;
    }

    public void setISSUENO(int ISSUENO) {
        this.ISSUENO = ISSUENO;
    }

    public String getMATNR() {
        return MATNR;
    }

    public void setMATNR(String MATNR) {
        this.MATNR = MATNR;
    }

    public Double getISSUEPICK() {
        return ISSUEPICK;
    }

    public void setISSUEPICK(Double ISSUEPICK) {
        this.ISSUEPICK = ISSUEPICK;
    }

    public String getISSUEDATE() {
        return ISSUEDATE;
    }

    public void setISSUEDATE(String ISSUEDATE) {
        this.ISSUEDATE = ISSUEDATE;
    }

}
