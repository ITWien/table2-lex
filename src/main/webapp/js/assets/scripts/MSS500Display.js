

/**
 * MSS500Display.js
 * This script use for [MSS500 Display] Only.
 * Created @24/11/2016 , 15:09:15 by jUNG-DAi Devs.
 */

$(document).ready(function() {

    // SET DATE&TIME to Header
        // new headerDateTime.setDateTo("#date");
        new headerDateTime.setDateTimeTo("#time");

	// Hide  Menu Side 
   	$("#wrapper").css({"-webkit-transition":"all 0.0s ease"},{"-moz-transition":"all 0.0s ease"},{"-o-transition":"all 0.0s ease"},{"transition":"all 0.0s ease"});

    $("#sidebar-wrapper").css({"-webkit-transition":"all 0.0s ease"},{"-moz-transition":"all 0.0s ease"},{"-o-transition":"all 0.0s ease"},{"transition":"all 0.0s ease"});

    $("#menu-toggle").css({"display":"none"});

    $("#mHead").css({"margin-top":"11px"});

    $("#wrapper").toggleClass("toggled");

});

/**
 *  Module :: MSS500Module
 */
var app = angular.module('MSS500Module', ['ngResource']);

app.factory('MSS500Service', function($q, $http){
	var service = {
		get : function (url, config) {
			return $http.get(url , config)
		},
		get_from_controller : function ( url ) {
			return $http.get(url)
		}
	};
	return service;
}); // end factory


// ,DTColumnDefBuilder , DTColumnBuilder
app.controller('MSS500Controller', function MSS500Controller ($window, $scope, $http, $q, $resource, MSS500Service){
	$scope.test = "jUNG-DAi";
	$scope.testNum = {
		box : 2, bag: 1, roll: 1
	};
	
	  $scope.groupSetNo = {
	  	model : null,
	  	datas : [   ]
	  };

	  $scope.sortBy = {
	  	model : null,
	  	datas : [   ]
	  };
  	// console.log(jsonObject)
	if ( !isUndefinedEmptyOrNull( json ) ) {
		$scope.data2Datatables = json.customerDetail;
		console.log($scope.data2Datatables);
		$scope.sPOFG = customer;

		// if ( _.isEqual($scope.mode, "MASH.VBELN")  ) {
		// 	$scope.data2Datatables = json[0].SaleConsignDatasets;
		// }
		// else if ( _.isEqual($scope.mode, "MASD.MATNR") ) {
		// 	$scope.data2Datatables = json[0].MaterialDatasets;
		// 	// console.log($scope.data2Datatables)
		// } else if ( _.isEqual($scope.mode, "MASH.STYLE") ) {
		// 	$scope.count = 0;
		// 	$scope.data2Datatables = json[0].StyleDatasets;
			
		// 	for ( var i = 0; i < $scope.data2Datatables.length; i++) {
		// 		console.log($scope.data2Datatables[i].Style)

		// 		if ( i == 0 ) {
		// 			$scope.count = $scope.count + 1
		// 		} else {
		// 			if ( $scope.data2Datatables[i].Style != $scope.data2Datatables[i - 1].Style ) {
		// 				$scope.count = $scope.count + 1
		// 			}
		// 		}
		// 	}
		// }

	} else {
		$scope.sPOFG = customer;
	}

	// console.log($scope.data2Datatables, $scope.sPOFG, $scope.sLGPBE);

	$scope.customerName = "";


	$scope.dis = true;

	$scope.authorized = false;


	$scope.statusError = function (response, status) {
		console.log(response)
		// return alert(status + " :: Error Data!!");
	}

	$scope.GetNameDetail = function ( action, text ) {

		let config = {
			params : {
				action : action,
				text : text
			}
		}

		// $http.get('../MSS500AJAX', config)
		MSS500Service.get('../MSS500AJAX', config)
			.success(function ( response , status) {
				if ( status == 200 ) {
					switch (response.type) {
						case 'GetCustomerName':
									if ( isUndefinedEmptyOrNull(response.name) ) {

										// reset form input when customer name is empty
										$scope.customerName = "";

										// Reset Data Tables
										// $scope.data2Datatables = {};
										$scope.authorized = false;
										$scope.searchFilter = "";



									} else {
										$scope.customerName = response.name;	// customer name
									}
							break;
						default:
								return false;
							break;
					}
				} else {
					// console.log(response[0])
					return statusError(response,status)
				}
			})
				.error(function( err ) {
					console.log( err )
				});
	}; // end GetNameDetail()


	if ( !isUndefinedEmptyOrNull( $scope.sPOFG ) ) {
		$scope.GetNameDetail('GetCustomerName', $scope.sPOFG);
	}

	/**
	 * [getData description]
	 * @return {[type]} [description]
	 */
	$scope.getData = function () {
		$scope.cursorPointer = {'cursor': 'pointer'};
		$scope.authorized = true;
		$scope.searchFilter = "";

		// const url = "?";
		let config = {
			params : {
				action : "GetData",
				text : "text",
				customer : $scope.sPOFG,
			}
		};

		var url = "";

		// check input Mat-Ctrl
		if ( isUndefinedEmptyOrNull(config.params.material) ) {
			url = "?action="+config.params.action+"&customer="+config.params.customer;
		}


		// Send Redirect URL
		$window.location.href = url;

/***************
/////////// Old Ver. ///////////////////
		// MSS500Service.get()
		MSS500Service.get(url, config).then(doneCallbacks, failCallbacks);
			function doneCallbacks (res) {

				$scope.data2Datatables = res.data[0].SaleConsignDatasets
				console.log($scope.data2Datatables)
			};
			function failCallbacks (err) {
				console.log(err)
			}
***********************/

	}; // GET DATA FROM SERVER

	$scope.showDetails = function ( data ) {
		// console.log(data);
		$scope.selected_data = data
	}
  
  $scope.isSelected=function(data){
    return $scope.selected_data === data;
  }
  $scope.countStyle = 0;

  $scope.check = function (index) {
		if ( index == 0 ) {
			return true;
		} else {
			if ( $scope.data2Datatables[index].Style != $scope.data2Datatables[index - 1].Style ) {
				return true;
			}
		}
		return false;
	}


	// Plot point status tracking - system.
	$scope.statusPoint = {
		stockRm : function (data, type) {
			// console.log(data)
			let color;
			// console.log(index, data)
			if ( data.max <= 0 ) {
				if ( _.isEqual(type, "groupset") ) {
					color = 'ghostwhite'
				} else if (_.isEqual(type, "style") ) {
					color = 'white'
				}
			} else {
				if ( data.min < 3  ) {
					color = 'yellow'
				} else if ( data.min >= 3 ) {
					color = 'green'
				}
			}
			return color;
		},
		transport : function (data, type) {
			// console.log(data)
			let color;
			// console.log(index, data)
			if ( data.max <= 3 ) {
				if ( _.isEqual(type, "groupset") ) {
					color = 'ghostwhite'
				} else if (_.isEqual(type, "style") ) {
					color = 'white'
				}
			} else {
				if ( data.min < 6  ) {
					color = 'yellow'
				} else if ( data.min >= 6 ) {
					color = 'green'
				}
			}
			return color;
		}
	};

}); // end MSS500Controller

app.filter('sumOfValue', function () {
    return function (data, key) {
        // debugger;
        if (_.isUndefined(data) && _.isUndefined(key))
            return 0;        
        var sum = 0;
        
        angular.forEach(data,function(v,k){
            let t = _.isUndefined(v[key]) ? 0.0 : parseFloat(v[key]);
            // sum = sum + parseFloat(v[key]);
            sum = sum + t;
        });        
        return sum;
    }
});

app.directive('uppercaseText', function() {
  return {
    require: 'ngModel',
    link: function($scope, element, attrs, MSS500Controller) {
      var setDataToUppercase = function(inputValue) {
        if ( isUndefinedEmptyOrNull(inputValue) ) inputValue = '';
        var uppercaseSet = inputValue.toUpperCase();
        if (uppercaseSet !== inputValue) {
          MSS500Controller.$setViewValue(uppercaseSet);
          MSS500Controller.$render();
        }
        return uppercaseSet;
      }
      MSS500Controller.$parsers.push(setDataToUppercase);
      setDataToUppercase($scope[attrs.ngModel]); // setDataToUppercase() initial value
    }
  };
});  // end directive

