/**
 * MSS003Display.js
 * This script use for [MSS003 Display] Only.
 * Created @29/09/2016 , 14:15:16 by jUNG-DAi Devs.
 */

$(document).ready(function() {
	// SET DATE&TIME to Header
	// new headerDateTime.setDateTo("#date");
	new headerDateTime.setDateTimeTo("#time");

	// Hide  Menu Side 
   	$("#wrapper").css({"-webkit-transition":"all 0.0s ease"},{"-moz-transition":"all 0.0s ease"},{"-o-transition":"all 0.0s ease"},{"transition":"all 0.0s ease"});

    $("#sidebar-wrapper").css({"-webkit-transition":"all 0.0s ease"},{"-moz-transition":"all 0.0s ease"},{"-o-transition":"all 0.0s ease"},{"transition":"all 0.0s ease"});

    $("#menu-toggle").css({"display":"none"});

    $("#mHead").css({"margin-top":"11px"});

    $("#wrapper").toggleClass("toggled");

    // $('#dataTables-example').DataTable();
});


/**
*  Module :: MSS003Module
*
* Factory :: 
* 	'ui.bootstrap.datetimepicker'
* 	"ngAlertify" :: Ref. https://alertifyjs.org/
*/
var app = angular.module('MSS003Module', ['ngResource', 'datatables', 'datatables.columnfilter']);
/**
 *
 * Controller :: MSS003Controller
 */
app.controller('MSS003Controller', function($window, $scope, $resource, DTOptionsBuilder, DTColumnBuilder, DTColumnDefBuilder ){
	
	// SET DATE Default :: Current Date 
	$scope.selectedDt =  moment().format('DD-MM-YYYY');
	// Defined Options of Alertify
	// alertify.logPosition("bottom right");
	

	if ( !isUndefinedEmptyOrNull( json ) ) {
		// console.log(json)
		$scope.datasets = json
		$scope.selectedDt = dt

	} else if ( !isUndefinedEmptyOrNull(dt) ) { 
		// console.log(dt)
		$scope.selectedDt = dt
		// $window.location.href = "./DP"
	}

	$scope.getData = getData
	function getData () {
		let date = $scope.selectedDt

		if ( isUndefinedEmptyOrNull(date) ) {
			alertify.error("Please fill in the date field");
		} else {
			alertify.success("Success log message");

			// APIs for testing data :: http://localhost:8080/MSS/MSS300AJAX?action=TestMSS003&dt=03-10-2016
			// Send Redirect URL
			var url = "?dt="+date;
			$window.location.href = url;
		    
		    /*
		    $resource(URL+'/MSS/MSS003/DP?dt='+date).query().$promise.then(function(response) {
		        $scope.datasets = response;
		    });
		    */
		}
	} // end getData();

	$scope.closeWin = closeWin
	function closeWin() {
		// $window.open(location, '_self').close();
		$window.close()
	}

    $scope.dtOptions = DTOptionsBuilder.newOptions()
      	.withColumnFilter({
            aoColumns: [
	            null,
	            {
	                type: 'text',
	                bRegex: true,
	                bSmart: true
	            },
	            {
	                type: 'text',
	                bRegex: true,
	                bSmart: true
	            },
				{
	                type: 'text',
	                bRegex: true,
	                bSmart: true
	            },
	            {
	                type: 'text',
	                bRegex: true,
	                bSmart: true
	            },
	            {
	                type: 'text',
	                bRegex: true,
	                bSmart: true
	            },
	            {
	                type: 'text',
	                bRegex: true,
	                bSmart: true
	            },
	            {
	                type: 'text',
	                bRegex: true,
	                bSmart: true
	            },
	            {
	                type: 'text',
	                bRegex: true,
	                bSmart: true
	            },
	            {
	                type: 'text',
	                bRegex: true,
	                bSmart: true
	            }
            ]
        })
        .withOption('lengthMenu', [ [6, 20, 50, -1], [6, 20, 50, "All"] ])
        .withDisplayLength(-1);
});

/**
 * Directive :: dateTimePicker
 * Ref. Doc. :: https://yaplex.com/blog/bootstrap-datetimepicker-with-angularjs
 */
app.directive('dateTimePicker',  function(){
	// Runs during compile
	return {
        restrict: "A",
        require: "ngModel",
        link: function (scope, element, attrs, ngModelCtrl) {
            var parent = $(element).parent();
            var dtp = parent.datetimepicker({
                // format: "LL", // Old Ver. from ref. doc.
                format : "DD-MM-YYYY",
                // showTodayButton: true
            });
            dtp.on("dp.change", function (e) {
            	// console.log('in dateTimePicker Directive :: ', e)
            	if ( !isUndefinedEmptyOrNull(e.date) ) {
	                ngModelCtrl.$setViewValue(moment(e.date).format("DD-MM-YYYY"));
	                scope.$apply();
            	}
            });
        }
    };
});

app.directive('windowExit', function($window) {
  return {
    restrict: 'A',
    link: function(element, attrs){
       var myEvent = $window.attachEvent || $window.addEventListener,
       chkevent = $window.attachEvent ? 'onbeforeunload' : 'beforeunload'; /// make IE7, IE8 compatable

       myEvent(chkevent, function (e) { // For >=IE7, Chrome, Firefox
           var confirmationMessage = ' ';  // a space
           (e || $window.event).returnValue = "Are you sure that you'd like to close the browser?";
           return confirmationMessage;
       });
    }
  };
});
