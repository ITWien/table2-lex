/**
 * MSS107Report.js
 * This script use for [MSS107 Report] Only.
 * Created @02/09/2016 , 15:11:48 by jUNG-DAi Devs.
 */
var sidebarVisible = localStorage.getItem('wrapper');
var sidebarVisible2 = localStorage.getItem('wrapper-top');
var checkIMenu = $("#i-menu").html();
var checkToggleTop = $("#i-toggle-top").html();

$(document).ready(function() {

    // SET DATE&TIME to Header
        new headerDateTime.setDateTo("#date");
        new headerDateTime.setTimeTo("#time");

    toggleClick();
    toggleLoad();
});

function getCustomerName() {
    var POFG = $("#sPOFG").val();
    $.get('../MSS107AJAX',{a:'getCustomerName', POFG:POFG},function(responseText) {
        $('#customerName').html(responseText);
        if ( !isUndefinedEmptyOrNull(responseText) ) {
            getDropDownGroup();
        } else {
            // Reset Form all when clear input Customer
            ResetFormInput.groupNo();
            ResetFormInput.sequenceNo__1();
            ResetFormInput.sequenceNo__2();
            ResetFormInput.saleNo__1();
            ResetFormInput.saleNo__2();
        }
    });
}

function getDropDownGroup() {
    var POFG = $("#sPOFG").val(); // => MWC
    $.get('../MSS107AJAX',{a:'getDropDownGroup', POFG:POFG},function(responseText) {
        if (responseText != "null") {
            $('#dpGRPNO').html(responseText);
            $('#sGRPNO').val("${GRPNO}");

            $('#sGRPNO').change(function(event) {
                if ( !isUndefinedEmptyOrNull($(this).val()) ) {
                    // Reset Form Input
                    ResetFormInput.sequenceNo__1();
                    ResetFormInput.sequenceNo__2();
                    ResetFormInput.saleNo__1();
                    ResetFormInput.saleNo__2();
                    // open dropdownList Sequence no. #1
                    getDropDownSQ();
                } else {
                    // close dropdownList Sequence no. #1
                    ResetFormInput.sequenceNo__1();
                    // close dropdownList Sequence no. #2
                    ResetFormInput.sequenceNo__2();
                }
            });
        }
        else {
            ResetFormInput.groupNo();
        }
    });
}

function getDropDownSQ() {
    var POFG = $("#sPOFG").val();
    $.get('../MSS107AJAX',{a:'getDropDownSQ', POFG:POFG},function(responseText) {
        if (responseText != "null") {
            $('#dpSEQNO').html(responseText);
            $('#sISSUENO').val("${ISSUENO1}");   //  ${ISSUENO} มาจาก controller ไปดูไฟล์ MSS100Controller.java แล้วจะเข้าใ

            $('#sISSUENO').change(function(event) {
                // Call getDropDownSQ__2() :: is open dropdown Sequence no.#2
                !isUndefinedEmptyOrNull($(this).val()) ? getDropDownSQ__2() : ResetFormInput.sequenceNo__2();
            });
        }
        else {
            ResetFormInput.sequenceNo__1();
        }
    });
}

function getDropDownSQ__2() {
    var POFG = $("#sPOFG").val();
    var ISSUENO1 = $("#sISSUENO").val();
    $.get('../MSS107AJAX',{a:'getDropDownSQ__2', POFG:POFG, ISSUENO1:ISSUENO1},function(responseText) {
        if (responseText != "null") {
            $('#dpSEQNO__2').html(responseText);
            $('#sISSUENO__2').val("${ISSUENO2}");

            // set new width of dropdown Sequence no.#2 is 70%
            $("#sISSUENO__2").attr("style", "width:70%");

            $("#sISSUENO__2").change(function(event) {
                !isUndefinedEmptyOrNull($(this).val()) ? getDropDownSaleNo() : ResetFormInput.saleNo__1();
            });
        }
        else {
            ResetFormInput.sequenceNo__2();
        }
    });
}

function getDropDownSaleNo() {

    var POFG = $("#sPOFG").val();
    var GRPNO = $("#sGRPNO").val();
    var ISSUENO1 = $("#sISSUENO").val();
    var ISSUENO2 = $("#sISSUENO__2").val();

    $.get('../MSS107AJAX',{a:'getDropDownSaleNo', POFG:POFG, GRPNO:GRPNO, ISSUENO1:ISSUENO1, ISSUENO2:ISSUENO2},function(responseText) {
        if (responseText != "null") {
            $('#dpVBELN').html(responseText);
            $('#sVBELN').val("${sVBELN1}");

            // Call getDropDownSaleNo__2() :: is open dropdown Sequence no.#2
            $("#sVBELN").change(function(event) {
                !isUndefinedEmptyOrNull($(this).val()) ? getDropDownSaleNo__2() : ResetFormInput.saleNo__2();
            });
        }
        else {
            ResetFormInput.saleNo__1();
        }
    });
}

function getDropDownSaleNo__2() {

    var POFG = $("#sPOFG").val();
    var GRPNO = $("#sGRPNO").val();
    var ISSUENO1 = $("#sISSUENO").val();
    var ISSUENO2 = $("#sISSUENO__2").val();
    var VBELN1 = $("#sVBELN").val();

    $.get('../MSS107AJAX',{a:'getDropDownSaleNo__2', POFG:POFG, GRPNO:GRPNO, ISSUENO1:ISSUENO1, ISSUENO2:ISSUENO2, VBELN1:VBELN1},function(responseText) {
        if (responseText != "null") {
            $('#dpVBELN__2').html(responseText);
            $('#sVBELN__2').val("${sVBELN2}");

            // set new width of dropdown Sequence no.#2 is 70%
            $("#sVBELN__2").attr("style", "width:70%");
        }
        else {
            ResetFormInput.saleNo__2()
        }
    });
}

function print() {

    var POFG = $("#sPOFG").val();
    var GRPNO = $("#sGRPNO").val();
    var ISSUENO1 = $("#sISSUENO").val();
    var ISSUENO2 = $("#sISSUENO__2").val();
    var VBELN1 = $("#sVBELN").val();
    var VBELN2 = $("#sVBELN__2").val();

// || isUndefinedEmptyOrNull(VBELN1) || isUndefinedEmptyOrNull(VBELN2)

    if ( 
        isUndefinedEmptyOrNull(POFG) || 
        isUndefinedEmptyOrNull(GRPNO) || 
        isUndefinedEmptyOrNull(ISSUENO1) || 
        isUndefinedEmptyOrNull(ISSUENO2) 
    ) {
        alertify.error("Please fill out search form.");
    } 
    else if (
        !isUndefinedEmptyOrNull(VBELN1) && isUndefinedEmptyOrNull(VBELN2)
    ) {
        alertify.error("Please fill out search form.");
    }
    else {
        // window.location = "MC?a=list&POFG="+POFG+"&LGPBE="+LGPBE+"&GRPNO="+GRPNO+"&ISSUENO="+ISSUENO;
        //a=list&POFG=MWC&LGPBE=RF09&GRPNO=MWC1/16&ISSUENO=1
        // controller/GetDataCtrl.jsp?a=report&POFG=MWC&VBELN=1107600208-1107600212&GRPNO=MWC1/16&ISSUENO=1-3
        VBELN1 = isUndefinedEmptyOrNull(VBELN1) ? "" : VBELN1;
        VBELN2 = isUndefinedEmptyOrNull(VBELN2) ? "" : VBELN2
        // console.log(SERVER.PROTOCAL()+"//"+SERVER.HOSTNAME()+":"+SERVER.PORT()+"/MSS107/controller/GetDataCtrl.jsp?a=report&POFG="+POFG+"&VBELN1="+VBELN1+"&VBELN2="+VBELN2+"&GRPNO="+GRPNO+"&ISSUENO1="+ISSUENO1+"&ISSUENO2="+ISSUENO2);
        window.open(SERVER.PROTOCAL()+"//"+SERVER.HOSTNAME()+":"+SERVER.PORT()+"/MSS107/controller/GetDataCtrl.jsp?a=report&POFG="+POFG+"&VBELN1="+VBELN1+"&VBELN2="+VBELN2+"&GRPNO="+GRPNO+"&ISSUENO1="+ISSUENO1+"&ISSUENO2="+ISSUENO2)
    }
}

// Customize Function() for MSS107Report.jsp
const ResetFormInput = {
    groupNo : function () {
        return $('#dpGRPNO').html("<input class=\"form-control\" type=\"text\" disabled>");
    },
    sequenceNo__1 : function () {
        return $('#dpSEQNO').html("<input class=\"form-control\" type=\"text\" disabled>");
    },
    sequenceNo__2 : function () {
        return $('#dpSEQNO__2').html("<input class=\"form-control\" type=\"text\" disabled>");
    },
    saleNo__1 : function () {
        return $('#dpVBELN').html("<input class=\"form-control\" type=\"text\" disabled>");
    },
    saleNo__2 : function () {
        return $('#dpVBELN__2').html("<input class=\"form-control\" type=\"text\" disabled>");
    }
};