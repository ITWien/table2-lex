app.factory('MSS300Service', function($q, $http){
	var service = {
		get : function (url, config) {
			return $http.get(url , config)
		},
		get_from_controller : function ( url ) {
			return $http.get(url)
		}
	};
	return service;
}); // end factory