/**
 * MSS105Report.js
 * This script use for [MSS105 Report] Only.
 * Created @05/09/2016 , 11:29:40 by jUNG-DAi Devs.
 */
var sidebarVisible = localStorage.getItem('wrapper');
var sidebarVisible2 = localStorage.getItem('wrapper-top');
var checkIMenu = $("#i-menu").html();
var checkToggleTop = $("#i-toggle-top").html();

$(document).ready(function() {

    // SET DATE&TIME to Header
        new headerDateTime.setDateTimeTo("#time");

    toggleClick();
    toggleLoad();
});

function getCustomerName() {
    var POFG = $("#sPOFG").val();
    $.get('../MSS100AJAX',{a:'getCustomerName', POFG:POFG},function(responseText) {
        $('#customerName').html(responseText);
        if ( !isUndefinedEmptyOrNull(responseText) ) {
            getDropDownGroup();
            getDropDownSQ();
        } else {
            // Reset Form all when clear input Customer
            ResetFormInput.groupNo();
            ResetFormInput.sequenceNo();
        }
    });
}

function getMatName() {
    var LGPBE = $("#sLGPBE").val();
    $.get('../MSS100AJAX',{a:'getMatName', LGPBE:LGPBE},function(responseText) {
        $('#MatName').html(responseText);
    });
}

function getDropDownGroup() {
    var POFG = $("#sPOFG").val(); // => MWC
    $.get('../MSS100AJAX',{a:'getDropDownGroup', POFG:POFG},function(responseText) {
        if (responseText != "null") {
            $('#dpGRPNO').html(responseText);
            $('#sGRPNO').val("${GRPNO}");

            $('#sGRPNO').change(function(event) {
                if ( !isUndefinedEmptyOrNull($(this).val()) ) {
                    // Reset Form Input
                    ResetFormInput.sequenceNo();
                    // open dropdownList Sequence no.
                    getDropDownSQ();
                } else {
                    // close dropdownList Sequence no.
                    ResetFormInput.sequenceNo();
                }
            });
        }
        else {
            ResetFormInput.groupNo();
        }
    });
}

function getDropDownSQ() {
    var POFG = $("#sPOFG").val();
    $.get('../MSS100AJAX',{a:'getDropDownSQ', POFG:POFG},function(responseText) {
        if (responseText != "null") {
            $('#dpSEQNO').html(responseText);
            $('#sISSUENO').val("${ISSUENO1}");   //  ${ISSUENO} มาจาก controller ไปดูไฟล์ MSS100Controller.java แล้วจะเข้าใ
        }
        else {
            ResetFormInput.sequenceNo();
        }
    });
}

function print() {

    var POFG = $('#sPOFG').val();
    var LGPBE = $("#sLGPBE").val();
    var GRPNO = $("#sGRPNO").val();
    var ISSUENO = isUndefinedEmptyOrNull($("#sISSUENO").val()) ? "" : $("#sISSUENO").val();

    // || isUndefinedEmptyOrNull(ISSUENO)       // Removed from condition by Ji @2017-07-17
    if ( isUndefinedEmptyOrNull(POFG) || isUndefinedEmptyOrNull(GRPNO) ) {
        alertify.error("Please fill out search form.");
    } else {
        // window.location = "MC?a=list&POFG="+POFG+"&LGPBE="+LGPBE+"&GRPNO="+GRPNO+"&ISSUENO="+ISSUENO;
        //a=list&POFG=MWC&LGPBE=RF09&GRPNO=MWC1/16&ISSUENO=1
        // controller/GetDataCtrl.jsp?a=report&POFG=MWC&VBELN=1105600208-1105600212&GRPNO=MWC1/16&ISSUENO=1-3
        window.open(SERVER.PROTOCAL()+"//"+SERVER.HOSTNAME()+":"+SERVER.PORT()+"/MSS209/controller/GetDataCtrl.jsp?a=report&POFG="+POFG+"&LGPBE="+LGPBE+"&GRPNO="+GRPNO+"&ISSUENO="+ISSUENO)
    }
}

// Customize Function() for MSS105Report.jsp
const ResetFormInput = {
    groupNo : function () {
        return $('#dpGRPNO').html("<input class=\"form-control\" type=\"text\" disabled>");
    },
    matCtrl : function () {
        return '';
    },
    sequenceNo : function () {
        return $('#dpSEQNO').html("<input class=\"form-control\" type=\"text\" disabled>");
    }
};