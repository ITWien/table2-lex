

/**
 * MSS300Display.js
 * This script use for [MSS300 Display] Only.
 * Created @08/09/2016 , 09:18:51 by jUNG-DAi Devs.
 */

$(document).ready(function() {

    // SET DATE&TIME to Header
        // new headerDateTime.setDateTo("#date");
        new headerDateTime.setDateTimeTo("#time");

    // Toggle Side 
	    toggleClick();
	    toggleLoad();

    // Toggle Top
	    toggleClick2();
		toggleLoad2();
});

/**
 *  Module :: MSS300Module
 */
var app = angular.module('MSS300Module', ['ngResource']);

app.factory('MSS300Service', function($q, $http){
	var service = {
		get : function (url, config) {
			return $http.get(url , config)
		},
		get_from_controller : function ( url ) {
			return $http.get(url)
		}
	};
	return service;
}); // end factory


// ,DTColumnDefBuilder , DTColumnBuilder
app.controller('MSS300Controller', function MSS300Controller ($window, $scope, $http, $q, $resource, MSS300Service){
	$scope.test = "jUNG-DAi";
	$scope.testNum = {
		box : 2, bag: 1, roll: 1
	};
	
	  $scope.groupSetNo = {
	  	model : null,
	  	datas : [   ]
	  };

	  $scope.sortBy = {
	  	model : null,
	  	datas : [   ]
	  };
  	// console.log(jsonObject)
	if ( !isUndefinedEmptyOrNull( json ) ) {

		// $scope.data2Datatables = json[0].SaleConsignDatasets;
		$scope.sPOFG = customer;
		$scope.sLGPBE = material;
		$scope.groupSetNo.model = !isUndefinedEmptyOrNull(groupset) ? groupset : null;
		$scope.sortBy.model = !isUndefinedEmptyOrNull(sortby) ? sortby : null;
		$scope.mode = !isUndefinedEmptyOrNull(sortby) ? sortby : null;

		if ( _.isEqual($scope.mode, "MASH.VBELN")  ) {
			$scope.data2Datatables = json[0].SaleConsignDatasets;
		}
		else if ( _.isEqual($scope.mode, "MASD.MATNR") ) {
			$scope.data2Datatables = json[0].MaterialDatasets;
			// console.log($scope.data2Datatables)
		} else if ( _.isEqual($scope.mode, "MASH.STYLE") ) {
			$scope.count = 0;
			$scope.data2Datatables = json[0].StyleDatasets;
			
			for ( var i = 0; i < $scope.data2Datatables.length; i++) {
				console.log($scope.data2Datatables[i].Style)

				if ( i == 0 ) {
					$scope.count = $scope.count + 1
				} else {
					if ( $scope.data2Datatables[i].Style != $scope.data2Datatables[i - 1].Style ) {
						$scope.count = $scope.count + 1
					}
				}
			}
		}

	} else {
		$scope.sPOFG = customer;
		$scope.sLGPBE = material;
		$scope.groupSetNo.model = !isUndefinedEmptyOrNull(groupset) ? groupset : null;
		$scope.sortBy.model = !isUndefinedEmptyOrNull(sortby) ? sortby : null;
		$scope.mode = !isUndefinedEmptyOrNull(sortby) ? sortby : null;
	}

	// console.log($scope.data2Datatables, $scope.sPOFG, $scope.sLGPBE);

	$scope.customerName = "";
	$scope.materialName = "";


	$scope.dis = true;

	$scope.authorized = false;


	$scope.statusError = function (response, status) {
		console.log(response)
		// return alert(status + " :: Error Data!!");
	}

	$scope.GetNameDetail = function ( action, text ) {

		let config = {
			params : {
				action : action,
				text : text
			}
		}

		// $http.get('../MSS300AJAX', config)
		MSS300Service.get('../MSS300AJAX', config)
			.success(function ( response , status) {
				if ( status == 200 ) {
					switch (response.type) {
						case 'GetCustomerName':
									if ( isUndefinedEmptyOrNull(response.name) ) {

										// reset form input when customer name is empty
										$scope.customerName = "";

										// Reset Mat-Ctrl
										$scope.materialName = "";
										$scope.sLGPBE = "";

										// Reset Group Set no. & Sort by
										$scope.groupSetNo.model = null;
										$scope.dis = true;
										$scope.sortBy.model = null;

										// Reset Data Tables
										// $scope.data2Datatables = {};
										$scope.authorized = false;
										$scope.searchFilter = "";



									} else {
										$scope.customerName = response.name;	// customer name
										// Open Dropdown Group set no. & Sort by
										$scope.dropdownList.groupSetNo();
										// console.log($scope.groupSetNo)
										$scope.dropdownList.sortBy();
										// console.log($scope.sortBy)
									}
							break;
						case 'GetMaterialName':
								$scope.materialName = response.name;	// material name
							break;
						default:
								return false;
							break;
					}
				} else {
					// console.log(response[0])
					return statusError(response,status)
				}
			})
				.error(function( err ) {
					console.log( err )
				});
	}; // end GetNameDetail()


	if ( !isUndefinedEmptyOrNull( $scope.sPOFG ) ) {
		$scope.GetNameDetail('GetCustomerName', $scope.sPOFG);
	}

	if ( !isUndefinedEmptyOrNull( $scope.sLGPBE ) ) {
		$scope.GetNameDetail('GetMaterialName', $scope.sLGPBE);
	}

	//
	$scope.dropdownList = {
		groupSetNo : function () {
		// Set data into dropdown list Group Set No.
			let config = {
				params : {
					action : 'GetGroupSetNo',
					text : $scope.sPOFG
				}
			};

			$http.get('../MSS300AJAX', config)
				.success(function (response , status) {
					// console.log(response, status);
					if ( status === 200 ) {
						$scope.groupSetNo.datas  = response;
						// open drop down list 
							$scope.dis = false;
					}
				});
		}, 
		sortBy : function () {
			// add list to drop down
			$scope.sortBy.datas = [
	      {id: '1', name: 'Sale Consign', value: 'MASH.VBELN'},
	      {id: '2', name: 'Material', value: 'MASD.MATNR'},
	      {id: '3', name: 'Style', value: 'MASH.STYLE'}
			]
		}
	}; // end dropdownList{...}

	/**
	 * [getData description]
	 * @return {[type]} [description]
	 */
	$scope.getData = function () {
		$scope.cursorPointer = {'cursor': 'pointer'};
		$scope.authorized = true;
		$scope.searchFilter = "";

		// const url = "?";
		let config = {
			params : {
				action : "GetData",
				text : "text",
				customer : $scope.sPOFG,
				material : $scope.sLGPBE,
				groupset : $scope.groupSetNo.model,
				sortby : $scope.sortBy.model,
				sorted : $scope.sortBy.model
			}
		};

		var url = "";

		// check input Mat-Ctrl
		if ( isUndefinedEmptyOrNull(config.params.material) ) {
			url = "?action="+config.params.action+"&customer="+config.params.customer+"&groupset="+config.params.groupset+"&sortby="+config.params.sortby+"&sorted="+config.params.sorted;
		} else {
			url = "?action="+config.params.action+"&customer="+config.params.customer+"&groupset="+config.params.groupset+"&material="+config.params.material+"&sortby="+config.params.sortby+"&sorted="+config.params.sorted;
		}


		// Send Redirect URL
		$window.location.href = url;

/***************
/////////// Old Ver. ///////////////////
		// MSS300Service.get()
		MSS300Service.get(url, config).then(doneCallbacks, failCallbacks);
			function doneCallbacks (res) {

				$scope.data2Datatables = res.data[0].SaleConsignDatasets
				console.log($scope.data2Datatables)
			};
			function failCallbacks (err) {
				console.log(err)
			}
***********************/

	}; // GET DATA FROM SERVER

	$scope.showDetails = function ( data ) {
		// console.log(data);
		$scope.selected_data = data
	}
  
  $scope.isSelected=function(data){
    return $scope.selected_data === data;
  }

  $scope.showDetails2 = function ( data2 ) {
  	
  	$scope.selected_data2 = data2
  	let pickUpDetailsDataset = data2.pickUpDetails
		$scope.totalPickUpDetails = {box : 0,bag : 0,roll : 0}
		_.each(pickUpDetailsDataset, function(value, key, list){
			$scope.totalPickUpDetails.box = !isUndefinedEmptyOrNull(_.property('box')(value)) ? ($scope.totalPickUpDetails.box + 1) : $scope.totalPickUpDetails.box ;
			$scope.totalPickUpDetails.bag = !isUndefinedEmptyOrNull(_.property('bag')(value)) ? ($scope.totalPickUpDetails.bag + 1) : $scope.totalPickUpDetails.bag ;
			$scope.totalPickUpDetails.roll = !isUndefinedEmptyOrNull(_.property('roll')(value)) ? ($scope.totalPickUpDetails.roll + 1) : $scope.totalPickUpDetails.roll ;
		});
  }

  $scope.isSelected2=function(data2){
    return $scope.selected_data2 === data2;
  }
  $scope.countStyle = 0;
  $scope.check = function (index) {
		if ( index == 0 ) {
			return true;
		} else {
			if ( $scope.data2Datatables[index].Style != $scope.data2Datatables[index - 1].Style ) {
				return true;
			}
		}
		return false;
	}

}); // end MSS300Controller

app.filter('sumOfValue', function () {
    return function (data, key) {
        // debugger;
        if (_.isUndefined(data) && _.isUndefined(key))
            return 0;        
        var sum = 0;
        
        angular.forEach(data,function(v,k){
            let t = _.isUndefined(v[key]) ? 0.0 : parseFloat(v[key]);
            // sum = sum + parseFloat(v[key]);
            sum = sum + t;
        });        
        return sum;
    }
});

app.directive('uppercaseText', function() {
  return {
    require: 'ngModel',
    link: function($scope, element, attrs, MSS300Controller) {
      var setDataToUppercase = function(inputValue) {
        if ( isUndefinedEmptyOrNull(inputValue) ) inputValue = '';
        var uppercaseSet = inputValue.toUpperCase();
        if (uppercaseSet !== inputValue) {
          MSS300Controller.$setViewValue(uppercaseSet);
          MSS300Controller.$render();
        }
        return uppercaseSet;
      }
      MSS300Controller.$parsers.push(setDataToUppercase);
      setDataToUppercase($scope[attrs.ngModel]); // setDataToUppercase() initial value
    }
  };
});  // end directive

