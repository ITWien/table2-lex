
/**
 * WorkingStatusDisplay.js
 * Created @12/10/2016 , 10:52:51 by jUNG-DAi Dev.
 */
$(document).ready(function() {

    // SET DATE&TIME to Header
        // new headerDateTime.setDateTo("#date");
        new headerDateTime.setDateTimeTo("#time");

    // Toggle Side 
	    toggleClick();
	    toggleLoad();
});

var app = /**
* WorkingStatusDisplayModule
*
* Description
*/
angular.module('WorkingStatusDisplayModule', []);

app.controller('WorkingStatusDisplayController', function($scope, $http, $window, MSS300Service){
	$scope.test = "jUNG-DAi"

	  $scope.groupSetNo = {
	  	model : null,
	  	datas : [   ]
	  };

	if ( !isUndefinedEmptyOrNull( json ) ) {
		console.log(json)
		$scope.data2Datatables = json;
		$scope.sPOFG = customer;
		$scope.groupSetNo.model = !isUndefinedEmptyOrNull(groupset) ? groupset : null;
	
	} else {
		$scope.sPOFG = customer;
		$scope.groupSetNo.model = !isUndefinedEmptyOrNull(groupset) ? groupset : null;
	}
  	$scope.customerName = "";

		$scope.dis = true;



	$scope.GetNameDetail = function ( action, text ) {

		let config = {
			params : {
				action : action,
				text : text
			}
		}

		// $http.get('../MSS300AJAX', config)
		MSS300Service.get('../MSS300AJAX', config)
			.success(function ( response , status) {
				if ( status == 200 ) {
					switch (response.type) {
						case 'GetCustomerName':
									if ( isUndefinedEmptyOrNull(response.name) ) {

										// reset form input when customer name is empty
										$scope.customerName = "";

										// Reset Group Set no. & Sort by
										$scope.groupSetNo.model = null;
										$scope.dis = true;
										$scope.searchFilter = "";

									} else {
										$scope.customerName = response.name;	// customer name
										// Open Dropdown Group set no. & Sort by
										$scope.dropdownList.groupSetNo();
										// console.log($scope.groupSetNo)
									}
							break;
						default:
								return false;
							break;
					}
				} else {
					console.log(response,status)
				}
			})
				.error(function( err ) {
					console.log( err )
				});
	}; // end GetNameDetail()


	if ( !isUndefinedEmptyOrNull( $scope.sPOFG ) ) {
		$scope.GetNameDetail('GetCustomerName', $scope.sPOFG);
	}

	$scope.dropdownList = {
		groupSetNo : function () {
		// Set data into dropdown list Group Set No.
			let config = {
				params : {
					action : 'GetGroupSetNo',
					text : $scope.sPOFG
				}
			};

			$http.get('../MSS300AJAX', config)
				.success(function (response , status) {
					// console.log(response, status);
					if ( status === 200 ) {
						$scope.groupSetNo.datas  = response;
						// open drop down list 
							$scope.dis = false;
					}
				});
		}
	} // end dropdownList{...}


	/**
	 * [getData description]
	 * @return {[type]} [description]
	 */
	$scope.getData = function () {
		// const url = "?";
		let config = {
			params : {
				action : "GetData",
				text : "text",
				customer : $scope.sPOFG,
				groupset : $scope.groupSetNo.model
			}
		};

		const url = "?action="+config.params.action+"&customer="+config.params.customer+"&groupset="+config.params.groupset;

		// console.log(url);
		// Send Redirect URL
		$window.location.href = url;

	}; // GET DATA FROM SERVER

  $scope.check = function (index) {
		if ( index == 0 ) {
			return true;
		} else {
			if ( $scope.data2Datatables[index].seq != $scope.data2Datatables[index - 1].seq ) {
				return true;
			}
		}
		return false;
	}
});