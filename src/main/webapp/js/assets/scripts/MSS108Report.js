/**
 * MSS205Report.js
 * This script use for [MSS205 Report] Only.
 * Created @10/10/2016 , 13:43:13 by jUNG-DAi Dev.
 */
var sidebarVisible = localStorage.getItem('wrapper');
var sidebarVisible2 = localStorage.getItem('wrapper-top');
var checkIMenu = $("#i-menu").html();
var checkToggleTop = $("#i-toggle-top").html();

$(document).ready(function() {

    // SET DATE&TIME to Header
        new headerDateTime.setDateTimeTo("#time");

    toggleClick();
    toggleLoad();
});

function getCustomerName() {
    var POFG = $("#sPOFG").val();
    $.get('../MSS100AJAX',{a:'getCustomerName', POFG:POFG},function(responseText) {
        $('#customerName').html(responseText);
        if ( !isUndefinedEmptyOrNull(responseText) ) {
            getDropDownGroup();
            // getDropDownSQ();
        } else {
            // Reset Form all when clear input Customer
            ResetFormInput.groupNo();
            // ResetFormInput.sequenceNo();
        }
    });
}

function getDropDownGroup() {
    var POFG = $("#sPOFG").val(); // => MWC
    $.get('../MSS100AJAX',{a:'getDropDownGroup', POFG:POFG},function(responseText) {
        if (responseText != "null") {
            $('#dpGRPNO').html(responseText);
            $('#sGRPNO').val("${GRPNO}");
        }
        else {
            ResetFormInput.groupNo();
        }
    });
}

function print() {

    var POFG = $('#sPOFG').val();
    var GRPNO = $("#sGRPNO").val();

    if ( isUndefinedEmptyOrNull(POFG) || isUndefinedEmptyOrNull(GRPNO) ) {
        alertify.error("Please fill out search form.");
    } else {
        // http://localhost:8080/MSS205/controller/GetDataCtrl.jsp?a=report&POFG=MWC&GRPNO=MWC1/16
        window.open(SERVER.PROTOCAL()+"//"+SERVER.HOSTNAME()+":"+SERVER.PORT()+"/MSS108/controller/GetDataCtrl.jsp?a=report&POFG="+POFG+"&GRPNO="+GRPNO)
    }
}

// Customize Function() for MSS205Report.jsp
const ResetFormInput = {
    groupNo : function () {
        return $('#dpGRPNO').html("<input class=\"form-control\" type=\"text\" disabled>");
    }
};