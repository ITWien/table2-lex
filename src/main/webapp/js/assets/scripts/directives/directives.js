app.directive('uppercaseText', function() {
  return {
    require: 'ngModel',
    link: function($scope, element, attrs, WorkingStatusDisplayController) {
      var setDataToUppercase = function(inputValue) {
        if ( isUndefinedEmptyOrNull(inputValue) ) inputValue = '';
        var uppercaseSet = inputValue.toUpperCase();
        if (uppercaseSet !== inputValue) {
          WorkingStatusDisplayController.$setViewValue(uppercaseSet);
          WorkingStatusDisplayController.$render();
        }
        return uppercaseSet;
      }
      WorkingStatusDisplayController.$parsers.push(setDataToUppercase);
      setDataToUppercase($scope[attrs.ngModel]); // setDataToUppercase() initial value
    }
  };
});  // end directive