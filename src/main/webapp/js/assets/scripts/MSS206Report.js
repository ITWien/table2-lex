/**
 * MSS206Report.js
 * This script use for [MSS206 Report] Only.
 * Created @14/12/2016 , 15:55:51 by jUNG-DAi Dev.
 */
var sidebarVisible = localStorage.getItem('wrapper');
var sidebarVisible2 = localStorage.getItem('wrapper-top');
var checkIMenu = $("#i-menu").html();
var checkToggleTop = $("#i-toggle-top").html();

$(document).ready(function() {

    // SET DATE&TIME to Header
        new headerDateTime.setDateTimeTo("#time");

    toggleClick();
    toggleLoad();

});

function getCustomerName() {
    var POFG = $("#sPOFG").val();
    $.get('../MSS100AJAX',{a:'getCustomerName', POFG:POFG},function(responseText) {
        $('#customerName').html(responseText);
        if ( !isUndefinedEmptyOrNull(responseText) ) {
            getDropDownGroup();
            getInputDateToReport()
        } else {
            // Reset Form all when clear input Customer
            ResetFormInput.groupNo();
            ResetFormInput.dateToReport();
            ResetFormInput.cn(3);
        }
    });
}

function getDropDownGroup() {
    var POFG = $("#sPOFG").val(); // => MWC
    $.get('../MSS100AJAX',{a:'getDropDownGroup', POFG:POFG},function(responseText) {
        if (responseText != "null") {
            $('#dpGRPNO').html(responseText);
            $('#sGRPNO').val("${GRPNO}");

            $('#sGRPNO').change(function(event) {
                getDropDownCartonNoOfGroup( $(this) );
            });


        }
        else {
            ResetFormInput.groupNo();
        }
    });
}

function getInputDateToReport() {
    $('#inputDateToReport').prop('disabled', false);
}

function getDropDownCartonNoOfGroup ( groupNo ) {
    // console.log(groupNo.val());
    $.getJSON('../MSS206AJAX', {action: 'GetCartonNo', ivno: groupNo.val()}, function(json, textStatus) {
        if ( _.isEqual(textStatus, "success") && json.length >= 1) {
            // console.log(textStatus, json)
            var cnInputSize = 3;

            for (var i = 1; i <= cnInputSize; i++) {
                cn.setOptionCnFrom(i, cn.setOption(json));
                cn.setOptionCnTo(i, cn.setOption(json.reverse()));

                // Cls json to default :: not reverse
                json.reverse()
            }

        } else {
            alert("Data reseponse is empty !!!");
        }
    });

    var cn = {
        setOption : function ( dtsCartonNo ) {
            // console.log(dtsCartonNo)
            var html = "";
            // html += " <select name=\"\" id=\"\"> ";
            _.each(dtsCartonNo, function (cn) {
                html += "\t\t<option value=\""+ cn +"\">"+ cn +"<\/option>\r\n\t";
            })

            // html += " <\/select> ";
            return html;
        },
        setOptionCnFrom : function ( $indexPlusOne , optionsIsset) {

            $("#dpCARNO_From_" + $indexPlusOne).html("<select class=\"form-control\" name=\"inputDpCARNO_From_"+$indexPlusOne+"\" id=\"inputDpCARNO_From_"+$indexPlusOne+"\"> \t\t<option value=\"\">C/N["+$indexPlusOne+"] From<\/option>\r\n\t "+optionsIsset+" <\/select> ");
        },
        setOptionCnTo : function ( $indexPlusOne , optionsIsset) {

            $("#dpCARNO_To_" + $indexPlusOne).html("<select class=\"form-control\" name=\"inputDpCARNO_To_"+$indexPlusOne+"\" id=\"inputDpCARNO_To_"+$indexPlusOne+"\"> \t\t<option value=\"\">C/N["+$indexPlusOne+"] To<\/option>\r\n\t "+optionsIsset+" <\/select> ");
        }

    }
}

function print( method ) {

    var POFG = $('#sPOFG').val();
    var GRPNO = $("#sGRPNO").val();
    var dateToReport = moment($("#inputDateToReport").val(), "DD/MM/YYYY").format("ll") ;
    dateToReport = _.isEqual(dateToReport, "Invalid date") ? "" : dateToReport;

    var obj = {"cn" : [ 
        { "from" : $("#inputDpCARNO_From_1").val() , "to" : $("#inputDpCARNO_To_1").val()  },
        { "from" : $("#inputDpCARNO_From_2").val() , "to" : $("#inputDpCARNO_To_2").val()  },
        { "from" : $("#inputDpCARNO_From_3").val() , "to" : $("#inputDpCARNO_To_3").val()  }
        ] 
    };

    var cn1 = isUndefinedEmptyOrNull($("#inputDpCARNO_From_1").val()) || isUndefinedEmptyOrNull($("#inputDpCARNO_To_1").val()) ? "" : $("#inputDpCARNO_From_1").val() + "-" + $("#inputDpCARNO_To_1").val();
    var cn2 = isUndefinedEmptyOrNull($("#inputDpCARNO_From_2").val()) || isUndefinedEmptyOrNull($("#inputDpCARNO_To_2").val()) ? "" : $("#inputDpCARNO_From_2").val() + "-" + $("#inputDpCARNO_To_2").val();
    var cn3 = isUndefinedEmptyOrNull($("#inputDpCARNO_From_3").val()) || isUndefinedEmptyOrNull($("#inputDpCARNO_To_3").val()) ? "" : $("#inputDpCARNO_From_3").val() + "-" + $("#inputDpCARNO_To_3").val();

    // console.log(obj);


    if ( isUndefinedEmptyOrNull(POFG) || isUndefinedEmptyOrNull(GRPNO) || isUndefinedEmptyOrNull(dateToReport) ) {
        alertify.error("Please fill out search form.");
    } else {
        // http://localhost:8080/MSS206/controller/GetDataCtrl.jsp?a=report&POFG=MWC&GRPNO=MWC1/16
        console.log(POFG, GRPNO, dateToReport);
        console.log(method);
        
        if ( !isUndefinedEmptyOrNull(method) && _.isEqual(method, 'summary') ) {
            window.open(SERVER.PROTOCAL()+"//"+SERVER.HOSTNAME()+":"+SERVER.PORT()+"/MSS206/controller/GetDataCtrl_Summary.jsp?a=report&POFG="+POFG+"&GRPNO="+GRPNO+"&dateToReport="+dateToReport+"&cn1="+cn1+"&cn2="+cn2+"&cn3="+cn3);
        } else {
            window.open(SERVER.PROTOCAL()+"//"+SERVER.HOSTNAME()+":"+SERVER.PORT()+"/MSS206/controller/GetDataCtrl.jsp?a=report&POFG="+POFG+"&GRPNO="+GRPNO+"&dateToReport="+dateToReport+"&cn1="+cn1+"&cn2="+cn2+"&cn3="+cn3);
                    window.open(SERVER.PROTOCAL()+"//"+SERVER.HOSTNAME()+":"+SERVER.PORT()+"/MSS206/controller/GetDataCtrl_Excel.jsp?a=report&POFG="+POFG+"&GRPNO="+GRPNO+"&dateToReport="+dateToReport+"&cn1="+cn1+"&cn2="+cn2+"&cn3="+cn3);
        }
    }
}

// Customize Function() for MSS206Report.jsp
const ResetFormInput = {
    groupNo : function () {
        return $('#dpGRPNO').html("<input class=\"form-control\" type=\"text\" disabled>");
    },
    dateToReport : function () {
        return $('#inputDateToReport').prop('disabled', true);
    },
    cn : function ( cnInputSize ) {

        for (var $indexPlusOne = 1; $indexPlusOne <= cnInputSize; $indexPlusOne++) {
            $('#dpCARNO_From_'+$indexPlusOne).html("<input class=\"form-control\" type=\"text\" placeholder=\"C\/N["+$indexPlusOne+"] From\" disabled>");
            $('#dpCARNO_To_'+$indexPlusOne).html("<input class=\"form-control\" type=\"text\" placeholder=\"C\/N["+$indexPlusOne+"] To\"  disabled >");
        }

    }
};