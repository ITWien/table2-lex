<!DOCTYPE html>
<html>
    <head>
        <title>Tic Tac Toe</title>
    </head>
    <body>

        <script>
            function switchVal(id, val, sts) {
                var curVal = document.getElementById('curVal').value;

                if (val === 'E') {
                    document.getElementById(id).value = curVal;

                    if (curVal === 'X') {
                        document.getElementById(id).style.color = 'red';
                        document.getElementById('curVal').value = 'O';
                    } else {
                        document.getElementById(id).style.color = 'blue';
                        document.getElementById('curVal').value = 'X';
                    }
                }

                checkWin(id, curVal);
            }

            function checkWin(id, val) {
                var a1 = document.getElementById('A1').value;
                var b1 = document.getElementById('B1').value;
                var c1 = document.getElementById('C1').value;

                var a2 = document.getElementById('A2').value;
                var b2 = document.getElementById('B2').value;
                var c2 = document.getElementById('C2').value;

                var a3 = document.getElementById('A3').value;
                var b3 = document.getElementById('B3').value;
                var c3 = document.getElementById('C3').value;

                if (id === 'A1') {
                    if ((a1 === val && a2 === val && a3 === val)
                            || (a1 === val && b1 === val && c1 === val)
                            || (a1 === val && b2 === val && c3 === val)) {
                        alert('Winner is ' + val + ' !');
                        restart();
                    }
                } else if (id === 'C1') {
                    if ((a1 === val && b1 === val && c1 === val)
                            || (c1 === val && b2 === val && a3 === val)
                            || (c1 === val && c2 === val && c3 === val)) {
                        alert('Winner is ' + val + ' !');
                        restart();
                    }
                } else if (id === 'C3') {
                    if ((c3 === val && c2 === val && c1 === val)
                            || (c3 === val && b2 === val && a1 === val)
                            || (c3 === val && b3 === val && a3 === val)) {
                        alert('Winner is ' + val + ' !');
                        restart();
                    }
                } else if (id === 'A3') {
                    if ((a3 === val && b3 === val && c3 === val)
                            || (a3 === val && b2 === val && c1 === val)
                            || (a3 === val && a2 === val && a1 === val)) {
                        alert('Winner is ' + val + ' !');
                        restart();
                    }
                } else if (id === 'B1') {
                    if ((a1 === val && b1 === val && c1 === val)
                            || (b1 === val && b2 === val && b3 === val)) {
                        alert('Winner is ' + val + ' !');
                        restart();
                    }
                } else if (id === 'C2') {
                    if ((c1 === val && c2 === val && c3 === val)
                            || (a2 === val && b2 === val && c2 === val)) {
                        alert('Winner is ' + val + ' !');
                        restart();
                    }
                } else if (id === 'B3') {
                    if ((a3 === val && b3 === val && c3 === val)
                            || (b1 === val && b2 === val && b3 === val)) {
                        alert('Winner is ' + val + ' !');
                        restart();
                    }
                } else if (id === 'A2') {
                    if ((a1 === val && a2 === val && a3 === val)
                            || (a2 === val && b2 === val && c2 === val)) {
                        alert('Winner is ' + val + ' !');
                        restart();
                    }
                } else if (id === 'B2') {
                    if ((b1 === val && b2 === val && b3 === val)
                            || (a2 === val && b2 === val && c2 === val)
                            || (a3 === val && b2 === val && c1 === val)
                            || (a1 === val && b2 === val && c3 === val)) {
                        alert('Winner is ' + val + ' !');
                        restart();
                    }
                }
            }

            function restart() {
                var input = document.getElementsByTagName('input');
                for (var i = 0; i < input.length; i++) {
                    if (input[i].name === 'xo') {
                        input[i].value = 'E';
                        input[i].style.color = 'transparent';
                    }
                }
                document.getElementById('curVal').value = 'X';
            }
        </script>
        <style>
            img {
                width:20%;
                float:left; 
                margin:0;
            }
            /*Filter styles*/
            .saturate { filter: saturate(3); }
            .grayscale { filter: grayscale(100%); }
            .contrast { filter: contrast(160%); }
            .brightness { filter: brightness(0.25); }
            .blur { filter: blur(3px); }
            .invert { filter: invert(100%); }
            .sepia { filter: sepia(100%); }
            .huerotate { filter: hue-rotate(180deg); }
            .rss.opacity { filter: opacity(50%); }
        </style>

        <input type="hidden" id="curVal" value="X">
        <input type="button" value="Restart" onclick="restart();">
        <br>        
        <br>
        <table>
            <tr>
                <td>
                    <input type="button" id="A1" name="xo" value="E" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="B1" name="xo" value="E" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="C1" name="xo" value="E" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
            </tr>
            <tr>
                <td>
                    <input type="button" id="A2" name="xo" value="E" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="B2" name="xo" value="E" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="C2" name="xo" value="E" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
            </tr>
            <tr>
                <td>
                    <input type="button" id="A3" name="xo" value="E" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="B3" name="xo" value="E" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="C3" name="xo" value="E" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
            </tr>
        </table>

        <br><hr><br>

        <%
            String path = "";
//            path = "http://i.stack.imgur.com/OyP0g.jpg";
            path = "https://www.tate.org.uk/sites/default/files/styles/width-600/public/van_gogh_self_portrait.jpg";
        %>

        <img src="<%=path%>" title="original">
        <img src="<%=path%>" title="saturate" class="saturate">
        <img src="<%=path%>" title="grayscale" class="grayscale">
        <img src="<%=path%>" title="contrast" class="contrast">
        <img src="<%=path%>" title="brightness" class="brightness">
        <img src="<%=path%>" title="blur" class="blur">
        <img src="<%=path%>" title="invert" class="invert">
        <img src="<%=path%>" title="sepia" class="sepia">
        <img src="<%=path%>" title="huerotate" class="huerotate">
        <img src="<%=path%>" title="opacity" class="rss opacity">

    </body>
</html>
