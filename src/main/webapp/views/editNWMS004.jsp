<%@ include file="../fragments/taglibs.jsp" %>
<%@ include file="../fragments/imports.jsp" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.sql.*;" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>EDIT MODE</title>
        <!-- css :: vendors -->
        <jsp:include page = "../fragments/css.jsp" />
        <!-- additional custom :: my-style -->
        <link rel="stylesheet" href="../resources/assets/styles/myStyles.css">

        <!-- js :: vendors -->
        <jsp:include page = "../fragments/script.jsp" />
        <!-- additional custom :: my-script -->
        <script src="../resources/assets/scripts/myScripts.js" async></script>
        <script src="../resources/assets/scripts/toggleLoad.js" async></script>
        <script type="text/javascript"> /* add something code to your script */</script>
        ${sendMessage}
        <script>
            var show1 = function () {
                $('#myModal1').modal('show');
            };

            function validateForm() {
                var desc = document.forms["frm"]["description"].value;
                var plant = document.forms["frm"]["plant"].value;
                var vtype = document.forms["frm"]["Vtype"].value;
                var mtype = document.forms["frm"]["Mtype"].value;
                var um = document.forms["frm"]["um"].value;
                var mg = document.forms["frm"]["Mgroup"].value;
                var mgd = document.forms["frm"]["MgroupDesc"].value;
                var mc = document.forms["frm"]["matCtrl"].value;
                var mcd = document.forms["frm"]["MatCtrlDesc"].value;
                var pg = document.forms["frm"]["Pgroup"].value;
                var pgn = document.forms["frm"]["PgroupName"].value;
                var loc = document.forms["frm"]["location"].value;

                if (desc.trim() === "") {
                    window.setTimeout(show1, 0);
                    document.getElementById("description").style.cssText = "border: 2px solid #ff9999";
                    return false;
                } else {
                    document.getElementById("description").style.cssText = "border: 1px solid #ccc";
                }

                if (plant.trim() === "") {
                    window.setTimeout(show1, 0);
                    document.getElementById("plant").style.cssText = "border: 2px solid #ff9999";
                    return false;
                } else {
                    document.getElementById("plant").style.cssText = "border: 1px solid #ccc";
                }

                if (vtype.trim() === "") {
                    window.setTimeout(show1, 0);
                    document.getElementById("Vtype").style.cssText = "border: 2px solid #ff9999";
                    return false;
                } else {
                    document.getElementById("Vtype").style.cssText = "border: 1px solid #ccc";
                }

                if (mtype.trim() === "") {
                    window.setTimeout(show1, 0);
                    document.getElementById("Mtype").style.cssText = "border: 2px solid #ff9999";
                    return false;
                } else {
                    document.getElementById("Mtype").style.cssText = "border: 1px solid #ccc";
                }

                if (um.trim() === "") {
                    window.setTimeout(show1, 0);
                    document.getElementById("um").style.cssText = "border: 2px solid #ff9999";
                    return false;
                } else {
                    document.getElementById("um").style.cssText = "border: 1px solid #ccc";
                }

                if (mg.trim() === "") {
                    window.setTimeout(show1, 0);
                    document.getElementById("Mgroup").style.cssText = "border: 2px solid #ff9999";
                    return false;
                } else {
                    document.getElementById("Mgroup").style.cssText = "border: 1px solid #ccc";
                }

                if (mgd.trim() === "") {
                    window.setTimeout(show1, 0);
                    document.getElementById("MgroupDesc").style.cssText = "border: 2px solid #ff9999";
                    return false;
                } else {
                    document.getElementById("MgroupDesc").style.cssText = "border: 1px solid #ccc";
                }

                if (mc.trim() === "") {
                    window.setTimeout(show1, 0);
                    document.getElementById("matCtrl").style.cssText = "border: 2px solid #ff9999";
                    return false;
                } else {
                    document.getElementById("matCtrl").style.cssText = "border: 1px solid #ccc";
                }

                if (mcd.trim() === "") {
                    window.setTimeout(show1, 0);
                    document.getElementById("MatCtrlDesc").style.cssText = "border: 2px solid #ff9999";
                    return false;
                } else {
                    document.getElementById("MatCtrlDesc").style.cssText = "border: 1px solid #ccc";
                }

                if (pg.trim() === "") {
                    window.setTimeout(show1, 0);
                    document.getElementById("Pgroup").style.cssText = "border: 2px solid #ff9999";
                    return false;
                } else {
                    document.getElementById("Pgroup").style.cssText = "border: 1px solid #ccc";
                }

                if (pgn.trim() === "") {
                    window.setTimeout(show1, 0);
                    document.getElementById("PgroupName").style.cssText = "border: 2px solid #ff9999";
                    return false;
                } else {
                    document.getElementById("PgroupName").style.cssText = "border: 1px solid #ccc";
                }

                if (loc.trim() === "") {
                    window.setTimeout(show1, 0);
                    document.getElementById("location").style.cssText = "border: 2px solid #ff9999";
                    return false;
                } else {
                    document.getElementById("location").style.cssText = "border: 1px solid #ccc";
                }
            }
        </script>
        <script>
            function changeMCDesc() {
                var x = document.getElementById("matCtrl").value;

                var parts = x.split(" : ");
                var part1 = parts[0];
                var part2 = parts[1];

                var ds = part1.split(" ");
                var ds1 = ds[0];

                var as = part2.split(" ");
                var as1 = as[0];

                document.getElementById("matCtrl").value = x;
                document.getElementById("MatCtrlDesc").value = as1;
            }
        </script>
        <style>
            input[type=text], input[type=password], input[type=number], select {
                width: 100%;
                padding: 5px 5px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }

            input[type=submit] {
                width: 100%;
                background-color: #2bd14a;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=submit]:hover {
                background-color: #45a049;
            }

            input[type=button] {
                width: 100%;
                background-color: #ef604a;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=button]:hover {
                background-color: #e51e00;
            }

            button[name=ok] {
                width: 25%;
                background-color: #008cff;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            button[name=ok]:hover {
                background-color: #008cff;
            }
        </style>
    </head>    
    <body onload="document.getElementById('userid').value = sessionStorage.getItem('uid');">
        <div id="wrapper">

            <!-- /#sidebar-wrapper -->
            <%@ include file="../fragments/sidebar.jsp" %>

            <!-- Page Content -->
            <!-- nav-head-custom -->
            <%@ include file="../fragments/nav_head.jsp" %>

            <div class="container-fluid">
                <!--                <div id="wrapper-top" align="left">
                <%-- PART 2 --%>
                <div class="row">
                    <div class="col-lg-12">
                        <div id="set-height" style="height:415px;margin-top:0px;">
                            <div id="sidebar-wrapper-top" class="">
                                <b class="page-header" style="padding-left:5px;font-size:18px;">
                <%-- <hr style="margin:0px;margin-top:10px;margin-bottom:10px;"> --%>
            </b>
        </div>
    </div>
</div>
</div> -->

                <%-- PART 3 --%>
                <form action="editN" method="POST" name="frm" onsubmit="return validateForm()">
                    <input type="hidden" id="userid" name="userid">
                    <table frame="box" width="100%" bordercolor="#e0e0e0">
                        <tr>
                            <th height="50" bgcolor="#f8f8f8"></th>
                            <th height="50" bgcolor="#f8f8f8">EDIT MODE</th>
                            <th height="50" bgcolor="#f8f8f8"></th>
                            <th height="50" bgcolor="#f8f8f8"></th>
                            <th height="50" bgcolor="#f8f8f8"></th>
                            <th height="50" bgcolor="#f8f8f8"></th>
                        </tr>
                        <br>
                        <tr>
                            <td width="5%" align="left">
                            <td width="15%" align="left"><h4>Material Code :</h4></td>
                            <td width="30%" align="left" style="padding-right: 200px;"><input type="text" id="materialCode" name="materialCode" value="${MatCode}" style="background-color: #ddd" readonly></td>
                            <td width="5%" align="left">
                            <td width="15%" align="left"><h4>Material Control :</h4></td>
                            <td width="30%" align="left" style="padding-right: 200px;"><select id="matCtrl" name="matCtrl" onchange="changeMCDesc()">
                                    <option value="${MatCtrl} : ${MatCtrlDesc}" selected hidden>${MatCtrl}</option>
                                    <c:forEach items="${MCList}" var="p" varStatus="i">
                                        <option value="${p.uid} : ${p.name}">${p.uid}</option>
                                    </c:forEach>
                                </select></td>
                        </tr>
                        <tr>
                            <td width="5%" align="left">
                            <td width="15%" align="left"><h4>Description :</h4></td>
                            <td width="30%" align="left" style="padding-right: 20px;"><input type="text" id="description" name="description" maxlength="40" value="${Desc}"></td>
                            <td width="5%" align="left">
                            <td width="15%" align="left"><h4>Material Ctrl Desc. :</h4></td>
                            <td width="30%" align="left" style="padding-right: 20px;"><input type="text" id="MatCtrlDesc" name="MatCtrlDesc" maxlength="50" value="${MatCtrlDesc}" readonly></td>
                        </tr>
                        <tr>
                            <td width="5%" align="left">
                            <td width="15%" align="left"><h4>Plant :</h4></td>
                            <td width="30%" align="left" style="padding-right: 200px;"><input type="text" id="plant" name="plant" maxlength="4" value="${Plant}"></td>
                            <td width="5%" align="left">
                            <td width="15%" align="left"><h4>P Group :</h4></td>
                            <td width="30%" align="left" style="padding-right: 200px;"><input type="text" id="Pgroup" name="Pgroup" maxlength="3" value="${Pgroup}"></td>
                        </tr>
                        <tr>
                            <td width="5%" align="left">
                            <td width="15%" align="left"><h4>V type :</h4></td>
                            <td width="30%" align="left" style="padding-right: 200px;"><input type="text" id="Vtype" name="Vtype" maxlength="2" value="${Vtype}"></td>
                            <td width="5%" align="left">
                            <td width="15%" align="left"><h4>P Group Name :</h4></td>
                            <td width="30%" align="left" style="padding-right: 20px;"><input type="text" id="PgroupName" name="PgroupName" maxlength="25" value="${PgroupName}"></td>
                        </tr>
                        <tr>
                            <td width="5%" align="left">
                            <td width="15%" align="left"><h4>M type :</h4></td>
                            <td width="30%" align="left" style="padding-right: 200px;"><input type="text" id="Mtype" name="Mtype" maxlength="4" value="${Mtype}"></td>
                            <td width="5%" align="left">
                            <td width="15%" align="left"><h4>Location :</h4></td>
                            <td width="30%" align="left" style="padding-right: 200px;"><input type="text" id="location" name="location" maxlength="4" value="${Location}"></td>
                        </tr>
                        <tr>
                            <td width="5%" align="left">
                            <td width="15%" align="left"><h4>Std U/M :</h4></td>
                            <td width="30%" align="left" style="padding-right: 200px;"><input type="text" id="um" name="um" maxlength="3" value="${UM}"></td>
                            <td width="5%" align="left">
                            <td width="15%" align="left">
                            <td width="30%" align="left">
                        </tr>
                        <tr>
                            <td width="5%" align="left">
                            <td width="15%" align="left"><h4>M Group :</h4></td>
                            <td width="30%" align="left" style="padding-right: 200px;"><input type="text" id="Mgroup" name="Mgroup" maxlength="4" value="${Mgroup}"></td>
                            <td width="5%" align="left">
                            <td width="15%" align="left">
                            <td width="30%" align="left">
                        </tr>
                        <tr>
                            <td width="5%" align="left">
                            <td width="15%" align="left"><h4>M Group Desc. :</h4></td>
                            <td width="30%" align="left" style="padding-right: 20px;"><input type="text" id="MgroupDesc" name="MgroupDesc" maxlength="60" value="${MgroupDesc}"></td>
                            <td width="5%" align="left">
                            <td width="15%" align="left">
                            <td width="30%" align="left"><input style="width: 100px;" type="button" value="Cancel" onclick="window.history.back()"/>
                                <input style="width: 100px;" type="submit" value="Confirm" /></td>
                        </tr>
                    </table>
                    <center>
                        <div id="myModal1" class="modal fade" role="dialog">
                            <div class="modal-dialog modal-sm"> <!-- ???????????? modal ?????????????? ???? xs, sm, md, lg -->
                                <div class="modal-md col-md-6" style="top: 150px; left:300px; background-color: #ddd; border: 1px solid #ccc; border-radius: 4px;">
                                    <div class="modal-header">
                                        <h4 class="modal-title" align="center">Please fill in the blanks !</h4>
                                        <br>
                                        <button name="ok" type="button" class="close" data-dismiss="modal">
                                            <font color = "white">OK</font>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- devbanban.com -->
                    </center>
                    <center>
                        <div id="myModal2" class="modal fade" role="dialog">
                            <div class="modal-dialog modal-sm"> <!-- ???????????? modal ?????????????? ???? xs, sm, md, lg -->
                                <div class="modal-md col-md-6" style="top: 150px; left:300px; background-color: #ddd; border: 1px solid #ccc; border-radius: 4px;">
                                    <div class="modal-header">
                                        <h4 class="modal-title" align="center">Edit Failed !</h4>
                                        <br>
                                        <button name="ok" type="button" class="close" data-dismiss="modal">
                                            <font color = "white">OK</font>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- devbanban.com -->
                    </center>
                </form>
                <!--End Part 3-->
                <!--</div>  end #wrapper-top -->
            </div> <!-- end .container-fluid -->
        </div> <!-- end #wrapper -->


    </body>

</html>