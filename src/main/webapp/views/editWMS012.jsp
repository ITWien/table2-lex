<%@ include file="../fragments/taglibs.jsp" %>
<%@ include file="../fragments/imports.jsp" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.sql.*;" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>EDIT MODE</title>
        <!-- css :: vendors -->
        <jsp:include page = "../fragments/css.jsp" />
        <!-- additional custom :: my-style -->
        <link rel="stylesheet" href="../resources/assets/styles/myStyles.css">

        <!-- js :: vendors -->
        <jsp:include page = "../fragments/script.jsp" />
        <!-- additional custom :: my-script -->
        <script src="../resources/assets/scripts/myScripts.js" async></script>
        <script src="../resources/assets/scripts/toggleLoad.js" async></script>
        <script type="text/javascript"> /* add something code to your script */</script>
        ${sendMessage}
        <script>
            function isNumberKey(evt) {
                var charCode = (evt.which) ? evt.which : event.keyCode
                if (charCode > 31 && (charCode < 48 || charCode > 57))
                    return false;
                return true;
            }
        </script>
        <SCRIPT language="javascript">
            function add(type) {

                //Create an input type dynamically.
                var element = document.createElement("input");

                //Assign different attributes to the element.
                element.setAttribute("type", type);
                element.setAttribute("value", "");
                element.setAttribute("name", "sq");
                element.setAttribute("maxlength", "15");


                var foo = document.getElementById("fooBar");

                //Append the element in page (in span).
                foo.appendChild(element);

            }
        </SCRIPT>
        <style>
            input[type=text], input[type=password], input[type=number], select {
                width: 100%;
                padding: 5px 5px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }

            input[type=submit] {
                width: 100%;
                background-color: #2bd14a;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=submit]:hover {
                background-color: #45a049;
            }

            input[type=button] {
                width: 100%;
                background-color: #ef604a;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=button]:hover {
                background-color: #e51e00;
            }

            button[name=ok] {
                width: 25%;
                background-color: #008cff;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            button[name=ok]:hover {
                background-color: #008cff;
            }
        </style>
    </head>    
    <body onload="document.getElementById('userid').value = sessionStorage.getItem('uid');">
        <div id="wrapper">

            <!-- /#sidebar-wrapper -->
            <%@ include file="../fragments/sidebar.jsp" %>

            <!-- Page Content -->
            <!-- nav-head-custom -->
            <%@ include file="../fragments/nav_head.jsp" %>

            <div class="container-fluid">
                <!--                <div id="wrapper-top" align="left">
                <%-- PART 2 --%>
                <div class="row">
                    <div class="col-lg-12">
                        <div id="set-height" style="height:415px;margin-top:0px;">
                            <div id="sidebar-wrapper-top" class="">
                                <b class="page-header" style="padding-left:5px;font-size:18px;">
                <%-- <hr style="margin:0px;margin-top:10px;margin-bottom:10px;"> --%>
            </b>
        </div>
    </div>
</div>
</div> -->

                <%-- PART 3 --%>
                <form action="edit" method="POST" name="frm" onsubmit="return validateForm()">
                    <input type="hidden" id="userid" name="userid">
                    <table id="myTable" frame="box" width="100%" bordercolor="#e0e0e0">
                        <tbody>
                            <tr>
                                <th height="50" bgcolor="#f8f8f8">EDIT MODE</th>
                                <th height="50" bgcolor="#f8f8f8"></th>
                                <th height="50" bgcolor="#f8f8f8"></th>
                                <th height="50" bgcolor="#f8f8f8"></th>
                            </tr>
                        <br>
                        <tr>
                            <td width="15%"></td>
                            <td width="15%" align="left"><h4>Setting Code :</h4></td>
                            <td width="20%" align="left" style="padding-right: 100px;"><input type="text" id="cod" name="cod" value="${cod}" style="background-color: #ddd" readonly></td>
                            <td width="40%"></td>
                        </tr>
                        <tr>
                            <td width="15%"></td>
                            <td width="15%" align="left"><h4>Total Sequences :</h4></td>
                            <td width="20%" align="left" style="padding-right: 100px;"><input type="text" id="total" name="total" value="${total}" maxlength="3" onkeypress="return isNumberKey(event)"></td>
                            <td width="40%"></td>
                        </tr>
                        <tr style="border-bottom: 2px solid #ddd;">
                            <td height="20"></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td width="15%"></td>
                            <td width="15%" align="left"><h4>Sequence :</h4></td>
                            <td width="20%" align="left">
                                <input type="text" id="seq" name="seq" value="${seq}" maxlength="15">
                            </td>
                            <td width="40%">&nbsp;&nbsp;&nbsp;<button id="addbox" name="addbox" type="button" style="border-radius: 100%;" onclick="add(document.forms[0].element.value)"><a><i class="glyphicon glyphicon-plus-sign" style="font-size:25px;"></i></a></button></td>
                        </tr>
                        <c:forEach items="${sqList}" var="p" varStatus="i">
                            <tr>
                                <td width="15%">
                                <td width="15%">
                                <td width="20%">
                                    <input type="text" id="sq" name="sq" value="${p.sq1}" maxlength="15">
                                </td>
                                <td width="40%">
                            </tr>
                        </c:forEach>
                        <tr>
                            <td></td>
                            <td></td>
                            <td><span id="fooBar"></span></td>
                            <td></td>
                        </tr>
                        <SELECT name="element" style="display: none">
                            <OPTION value="text">Textbox</OPTION>
                        </SELECT>
                        <div align="center">
                            <tr>
                                <td></td>
                                <td></td>
                                <td width="20%" align="center"><input style="width: 100px;" type="button" value="Cancel" onclick="window.location.href = '/TABLE2/WMS012/display'"/>
                                    <input style="width: 100px;" type="submit" value="Confirm" /></td>
                                <td></td>
                            </tr>
                        </div>
                        </tbody>
                    </table>
                    <center>
                        <div id="myModal2" class="modal fade" role="dialog">
                            <div class="modal-dialog modal-sm"> <!-- ???????????? modal ?????????????? ???? xs, sm, md, lg -->
                                <div class="modal-md col-md-6" style="top: 150px; left:300px; background-color: #ddd; border: 1px solid #ccc; border-radius: 4px;">
                                    <div class="modal-header">
                                        <h4 class="modal-title" align="center">Edit Failed !</h4>
                                        <br>
                                        <button name="ok" type="button" onclick="window.location.href = '/TABLE2/WMS012/edit'">
                                            <font color = "white">OK</font>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- devbanban.com -->
                    </center>
                </form>
                <!--End Part 3-->
                <!--</div>  end #wrapper-top -->
            </div> <!-- end .container-fluid -->
        </div> <!-- end #wrapper -->


    </body>

</html>