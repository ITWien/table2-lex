<%@ include file="../fragments/taglibs.jsp" %>
<%@ include file="../fragments/imports.jsp" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.sql.*;" %>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <title>ISM</title>
        <!-- css :: vendors -->
        <jsp:include page="../fragments/css.jsp" />
        <!-- additional custom :: my-style -->
        <link rel="stylesheet" href="../resources/assets/styles/myStyles.css">
        <link rel="stylesheet" href="../resources/font/THsarabun/thsarabumnew.css">

        <!-- js :: vendors -->
        <jsp:include page="../fragments/script.jsp" />
        <!-- additional custom :: my-script -->
        <script src="../resources/assets/scripts/myScripts.js" async></script>
        <script src="../resources/assets/scripts/toggleLoad.js" async></script>
        <style>
            input[type=text],
            select {
                width: 100%;
                padding: 5px 5px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }

            input[type=date],
            input[type=number] {
                /*width: 100%;*/
                padding: 5px 5px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }

            input[type=submit] {
                width: 100%;
                background-color: #00399b;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=submit]:hover {
                background-color: #3973d6;
            }

            input[type=button] {
                width: 100%;
                background-color: #ef604a;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=button]:hover {
                background-color: #e51e00;
            }
        </style>
        <script>
            var tableOptions = {
                "paging": false,
                "lengthMenu": [[-1, 10, 25, 50, 100], ["All", 10, 25, 50, 100]],
                "bSortClasses": false,
                "ordering": false,
                "scrollY": '50vh',
//                "scrollX": true,
                "scrollCollapse": true,
                "info": false,
                "fixedColumns": true,
                "order": [[0, "asc"]],
                "columnDefs": [
                    {"targets": [3, 4, 5, 6, 7], "className": "text-right"},
                    {"targets": [0, 9, 10], "className": "text-center"},
                    {"targets": [10], "orderable": false}
                ]
            };
        </script>
        <style>
            body {
                font-family: "Sarabun", sans-serif !important;
                /*font-family: 'THSarabunNew', sans-serif;*/
            }

            .displayTable {
                width: auto;
                border: 1px solid #ccc;
                border-radius: 4px;
            }

            .displayContain {
                border: 2px solid #ccc;
                border-radius: 5px;
                padding: 20px;
                /*width: fit-content;*/
            }

            /*            td {
                            white-space: nowrap;
                            cursor: pointer;
                        }*/

            .checkData ,.checkAllData{
                width: 20px;
                height: 20px;
            }

            .sts-0{
                font-size: 20px;
                color: navy;
            }
            .sts-1{
                font-size: 20px;
                color: yellow;
            }
            .sts-2{
                font-size: 20px;
                color: green;
            }
            .sts-3{
                font-size: 20px;
                color: blue;
            }
            .sts-4{
                font-size: 20px;
                color: navy;
            }
            .sts-8{
                font-size: 20px;
                color: black;
            }

            .modal-content{
                width: 50%;
            }

            #fileFrom{
                height: 700px;
                border-radius: 10px;
                border: 5px solid #88d9f2;
                padding: 10px;
                /*text-align: center;*/
                overflow: auto;
            }

            #fileTo{
                height: 700px;
                border-radius: 10px;
                border: 5px dashed #88d9f2;
                padding: 10px;
                /*text-align: center;*/
                overflow: auto;
            }

            .styleLotIcon{
                font-size: 50px;
                color: #59b7d4;
            }

            .cardStyle{
                width: fit-content;
                text-align: center;
                padding: 10px;
                display: inline-grid;
                cursor: pointer;
                border-radius: 15px;
            }

            #fileFrom .cardStyle:hover{
                background-color: #d0ecf5;
            }
            #fileTo .cardStyle:hover{
                background-color: #f5d6d0;
            }

            .centerTb{
                text-align: center;
                padding: 7px;
            }

            .rightTb{
                text-align: right;
                padding: 7px;
            }

            .cardTb-l{
                border: 2px solid #2725a8;
                background-color: #cfdaff;
            }
            .cardTb-r{
                border: 2px solid #2725a8;
            }

            #showTable tbody tr {
                cursor: pointer;
            }

            .alarmStyle{
                background-color: #f5d6d0;
                border: 3px dashed red;
            }
        </style>
        <script>
            $(document).ready(function () {

                genCard();

                $('#showTable').DataTable(tableOptions);

                $('#backBtn').click(function () {
//                    var order = '';
//                    var seq = '';

            <%--<c:forEach var="x" items="${order}">--%>
//                    order += '&order=${x}';
            <%--</c:forEach>--%>
            <%--<c:forEach var="y" items="${seq}">--%>
//                    seq += '&seq=${y}';
            <%--</c:forEach>--%>

                    var oANDs = '';
                    let zz = '';
            <c:forEach var="z" items="${oANDs}">
                    zz = '${z}';
                    if (zz !== '' && zz !== null) {
                        oANDs += '&oANDs=' + zz;
                    }
            </c:forEach>
//                    console.log('order: ', order);
//                    console.log('seq: ', seq);

//                    console.log('sumByDest?act=back' + order + seq);

                    window.location.href = 'sumByDest?act=back' + oANDs;
//                    window.location.href = 'sumByDest?act=back' + order + seq;
                });

                $('#selectAllStyleBtn').click(function () {
                    if ($('#fileFrom .cardStyle').not('.alarmStyle').length > 0) {
                        $('#fileTo').append($('#fileFrom .cardStyle').not('.alarmStyle'));
                    } else {
                        $('#fileTo').append($('#fileFrom .cardStyle'));
                    }
                    $('#cntTotOrder').html($('#fileTo .cardStyle').length);
                });

                $('#deselectAllStyleBtn').click(function () {
                    if ($('#fileTo .cardStyle.alarmStyle').length > 0) {
                        $('#fileFrom').append($('#fileTo .cardStyle.alarmStyle'));
                    } else {
                        $('#fileFrom').append($('#fileTo .cardStyle'));
                    }
                    $('#cntTotOrder').html($('#fileTo .cardStyle').length);
                });

                $('#prepareBtn').click(function () {
                    var file = $('#fileTo .cardStyle');
                    $('.cardStyle').removeClass('alarmStyle');
                    var styleLotList = '';

                    var table = $('#showTable').DataTable();
                    table.clear().draw();
                    $('#loadIcon').removeClass('hidden');

                    $('#avgPer').html(0);
                    $('#skuTot').html(0);
                    $('#skuCheck').html(0);
                    $('#skuPer').html(0);
                    $('#qtyTot').html(0);
                    $('#qtyCheck').html(0);
                    $('#qtyPer').html(0);
                    $('#costTot').html(0);
                    $('#costCheck').html(0);
                    $('#costPer').html(0);

                    $('#skuCheck').css('color', '');
                    $('#qtyCheck').css('color', '');

                    $('#fileFrom').height(670);
                    $('#fileTo').height(670);

                    for (var i = 0; i < file.length; i++) {
                        if (i === 0) {
                            styleLotList += '\'' + file[i].id + '\'';
                        } else {
                            styleLotList += ',\'' + file[i].id + '\'';
                        }
                    }

                    if (file.length > 0) {

                        var repHashTag = styleLotList.toString().replace(/#/g, "HashTag");
                        var seq = '';
                        let zz = '';
                        let count = 0;

            <c:forEach var="z" items="${oANDs}">
                        zz = '${z}';
                        if (zz !== '' && zz !== null) {
                            if (count === 0) {
                                seq += '\'' + zz.split("_")[1] + '\'';
                            } else {
                                seq += ',\'' + zz.split("_")[1] + '\'';
                            }
                        }

                        count++;
            </c:forEach>

                        $.ajax({
                            url: "/TABLE2/ShowDataTablesISM200?mode=getDetailPrepare"
                                    + "&styleLotList=" + repHashTag + "&sseq=" + seq

                        }).done(function (result) {
                            console.log(result);

                            var skuCheck = 0;
                            var avgPer = 0.00;
                            var qtyTot = 0.00;
                            var qtyCheck = 0.00;

                            for (var i = 0; i < result.length; i++) {

                                var diff = parseFloat(result[i].ISDSAPONH.toString().replace(/,/g, '')) - parseFloat(result[i].ISDRQQTY.toString().replace(/,/g, ''));
                                var qty = parseFloat(result[i].ISDRQQTY.toString().replace(/,/g, ''));
                                var per = (parseFloat(result[i].ISDSAPONH.toString().replace(/,/g, '')) / parseFloat(result[i].ISDRQQTY.toString().replace(/,/g, ''))) * 100;
                                if (per > 100.00) {
                                    per = 100.00;
                                }
                                avgPer += per;
                                qtyTot += qty;

                                var check = '';
                                if (per >= 80.00) {
                                    check = '<i class="fa fa-check-circle-o fa-2x" aria-hidden="true" style="color: green;"></i>';
                                    skuCheck++;
                                    qtyCheck += qty;
                                } else {
                                    check = '<i class="fa fa-times-circle-o fa-2x" aria-hidden="true" style="color: red;"></i>';
                                }

                                table.row.add([
                                    (i + 1),
                                    result[i].ISDITNO,
                                    result[i].ISDDESC,
                                    result[i].ISDSAPONH,
                                    result[i].ISDWMSONH,
                                    result[i].ISDRQQTY,
                                    currencyFormat(diff, 2),
                                    currencyFormat(per, 2),
                                    result[i].ISDMTCTRL,
                                    result[i].ISDUNIT,
                                    check
                                ]).draw(false);

                            }
//                            $('[data-toggle="tooltip"]').tooltip();
                            $('#loadIcon').addClass('hidden');

                            $('#avgPer').html(currencyFormat((avgPer / result.length), 2));
                            $('#skuTot').html(currencyFormat(result.length, 0));
                            $('#skuCheck').html(currencyFormat(skuCheck, 0));
                            if (result.length !== skuCheck) {
                                $('#skuCheck').css('color', 'red');
                            }
                            $('#skuPer').html(currencyFormat(((skuCheck / result.length) * 100), 2));
//                            $('#qtyTot').html(currencyFormat(qtyTot, 2));
//                            $('#qtyCheck').html(currencyFormat(qtyCheck, 2));
//                            if (qtyTot !== qtyCheck) {
//                                $('#qtyCheck').css('color', 'red');
//                            }
//                            $('#qtyPer').html(currencyFormat(((qtyCheck / qtyTot) * 100), 2));
//                            $('#costTot').html(currencyFormat(result.length, 0));
//                            $('#costCheck').html(currencyFormat(skuCheck, 0));
//                            $('#costPer').html(currencyFormat(((skuCheck / result.length) * 100), 2));

                            $('#fileFrom').height($('#fileTable').height());
                            $('#fileTo').height($('#fileTable').height());

                        }).fail(function (jqXHR, textStatus, errorThrown) {
                            // needs to implement if it fails
                        });

                    } else {
                        $('#loadIcon').addClass('hidden');

                        $('#avgPer').html(0);
                        $('#skuTot').html(0);
                        $('#skuCheck').html(0);
                        $('#skuPer').html(0);
                        $('#qtyTot').html(0);
                        $('#qtyCheck').html(0);
                        $('#qtyPer').html(0);
                        $('#costTot').html(0);
                        $('#costCheck').html(0);
                        $('#costPer').html(0);

                        $('#skuCheck').css('color', '');
                        $('#qtyCheck').css('color', '');

                        $('#fileFrom').height(670);
                        $('#fileTo').height(670);
                    }

                });

                $('#showTable tbody').on('click', 'tr', function () {
                    var table = $('#showTable').DataTable();
                    var row = table.row(this);
                    var thisTr = this;

                    if (row.child.isShown()) {
                        row.child.hide();
                        $(this).css('background-color', '');

                    } else {

                        var matCode = row.data()[1].toString().trim();
                        var file = $('#fileTo .cardStyle');
                        var styleLotList = '';
                        for (var i = 0; i < file.length; i++) {
                            if (i === 0) {
                                styleLotList += '\'' + file[i].id + '\'';
                            } else {
                                styleLotList += ',\'' + file[i].id + '\'';
                            }
                        }

                        var repHashTag = styleLotList.toString().replace(/#/g, "HashTag");

                        $.ajax({

                            url: "/TABLE2/ShowDataTablesISM200?mode=getHeadByMatcode"
                                    + "&matCode=" + matCode
                                    + "&styleLotList=" + repHashTag

                        }).done(function (result) {
                            console.log(result);
                            row.child(format(row.data(), result)).show();
                            if (row.data()[10].toString().includes('red')) {
                                $(thisTr).css('background-color', '#f5c6c6');
                                $('#childTB-' + row.data()[1].toString().trim()).parent().css('background-color', '#f5c6c6');
                            } else {
                                $(thisTr).css('background-color', '#a0e8b1');
                                $('#childTB-' + row.data()[1].toString().trim()).parent().css('background-color', '#a0e8b1');
                            }

                        }).fail(function (jqXHR, textStatus, errorThrown) {
                            // needs to implement if it fails
                        });

                    }

                });

                $('#generateBtn').click(function () {
                    var file = $('#fileTo .cardStyle');
                    $('.cardStyle').removeClass('alarmStyle');
                    $('#loadIcon').removeClass('hidden');
                    var styleLotList = '';

                    for (var i = 0; i < file.length; i++) {
                        if (i === 0) {
                            styleLotList += '\'' + file[i].id + '\'';
                        } else {
                            styleLotList += ',\'' + file[i].id + '\'';
                        }
                    }

                    if (file.length > 0) {

                        var wh = $(file[0]).attr('wh');
                        var repHashTag = styleLotList.toString().replace(/#/g, "HashTag");

                        var seq = '';
                        let zz = '';
                        let count = 0;
                        var oANDs = '';

            <c:forEach var="z" items="${oANDs}">

                        zz = '${z}';

                        if (zz !== '' && zz !== null) {
                            oANDs += '&oANDs=' + zz;
                            if (count === 0) {
                                seq += '\'' + zz.split("_")[1] + '\'';
                            } else {
                                seq += ',\'' + zz.split("_")[1] + '\'';
                            }
                        }

                        count++;
            </c:forEach>

                        $.ajax({
                            url: "/TABLE2/ShowDataTablesISM200?mode=setJobNo"
                                    + "&styleLotList=" + repHashTag
                                    + "&wh=" + wh + "&sseq=" + seq

                        }).done(function (result) {
                            console.log(result);
                            window.location.href = "summary?dest=${dest}&jobNo=" + result + "&wh=" + wh + "&sseq=" + seq + "&oANDs=" + oANDs;

                        }).fail(function (jqXHR, textStatus, errorThrown) {
                            // needs to implement if it fails
                        });

                    } else {
                        $('#loadIcon').addClass('hidden');
                    }
                });

            });

            function allowDrop(ev) {
                ev.preventDefault();
            }

            function drag(ev) {
                ev.dataTransfer.setData("text", ev.target.id);
            }

            function drop(ev) {
                ev.preventDefault();
                var fromID = ev.dataTransfer.getData("text");
                var toID = ev.target.id;
                if ($('#' + toID).hasClass('filestore')) {
                    $('#' + toID).append($('#' + fromID));
                }

                $('#cntTotOrder').html($('#fileTo .cardStyle').length);
            }

            function genCard() {

                var oANDs = '';
                let zz = '';
            <c:forEach var="z" items="${oANDs}">
                zz = '${z}';
                if (zz !== '' && zz !== null) {
                    oANDs += '&oANDs=' + zz;
                }
            </c:forEach>

//                var order = '';
//                var seq = '';
//                var oANDs = '';
//                var xx = '';
//
            <%--<c:forEach var="x" items="${oANDs}">--%>
//                xx = '${x}';
//
//                if (xx !== null && xx !== '') {
//                    oANDs += '&oorder=' + xx.split("_")[0] + '&sseq=' + xx.split("_")[1];
//                }
            <%--</c:forEach>--%>
//                console.log('order: ', order);

            <%--<c:forEach var="x" items="${order}">--%>
//                order += '&order=${x}';
            <%--</c:forEach>--%>
            <%--<c:forEach var="y" items="${seq}">--%>
//                seq += '&seq=${y}';
            <%--</c:forEach>--%>

                $.ajax({
                    url: "/TABLE2/ShowDataTablesISM200?mode=getCardStyleLotData"
                            + "&dest=${dest}"
                            + oANDs
//                            + order
//                            + seq

                }).done(function (result) {
                    console.log(result);
                    for (var i = 0; i < result.length; i++) {
                        addCard(
                                result[i].STYLE_LOT_ID,
                                result[i].STYLE_LOT_TEXT,
                                result[i].ISHWHNO
                                );
                    }

                }).fail(function (jqXHR, textStatus, errorThrown) {
                    // needs to implement if it fails
                });

            }

            function addCard(id, text, wh) {

                var cardHtml = $('#cardTmp').html().toString();
                cardHtml = cardHtml.replace('genCardId', id);
                cardHtml = cardHtml.replace('genCardText', text);
                cardHtml = cardHtml.replace('genCardWh', wh);
                $('#fileFrom').append(cardHtml);

            }

            function selectFile(ele) {
                var fileStore = $(ele).parent().attr('id');
                if (fileStore === 'fileFrom') {
                    $('#fileTo').append($(ele));
                } else if (fileStore === 'fileTo') {
                    $('#fileFrom').append($(ele));
                }

                $('#cntTotOrder').html($('#fileTo .cardStyle').length);
            }

            function currencyFormat(num, fp) {
                var pp = Math.pow(10, fp);
                num = Math.round(num * pp) / pp;
                return num.toFixed(fp).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
            }

            function clearTable() {
                var table = $('#showTable').DataTable();
                table.clear().draw();
            }

            function format(d, res) {

                var tableText = '';

                tableText += '<table id="childTB-' + d[1].toString().trim() + '" cellpadding="5" cellspacing="0" border="0" style="padding-left:50px; width: 100%;">' +
                        '<tr style="color: purple;">' +
                        '<th>MVT</th>' +
                        '<th>Trans Dt</th>' +
                        '<th>Sale Order</th>' +
                        '<th>Customer</th>' +
                        '<th>Name</th>' +
                        '<th>Doc no.</th>' +
                        '<th>FG Style</th>' +
                        '<th>Style</th>' +
                        '<th>Color</th>' +
                        '<th>Lot</th>' +
                        '<th>Qty</th>' +
                        '</tr>';

                for (var i = 0; i < res.length; i++) {

                    tableText += '<tr onclick="alarmStyle(\'' + res[i].ISHMATCODE + '\',\'' + res[i].ISHLOT + '\');">' +
                            '<td>' + res[i].ISHMVT + '</td>' +
                            '<td>' + res[i].ISHTRDT + '</td>' +
                            '<td>' + res[i].ISHORD + '</td>' +
                            '<td>' + res[i].ISHCUNO + '</td>' +
                            '<td>' + res[i].ISHCUNM1 + '</td>' +
                            '<td>' + res[i].ISHSUBMI + '</td>' +
                            '<td>' + res[i].ISHMATCODE + '</td>' +
                            '<td>' + res[i].ISHSTYLE + '</td>' +
                            '<td>' + res[i].ISHCOLOR + '</td>' +
                            '<td>' + res[i].ISHLOT + '</td>' +
                            '<td>' + res[i].ISHAMTFG + '</td>' +
                            '</tr>';

                }

                tableText += '</table>';

                return tableText;

            }

            function alarmStyle(style, lot) {
                var styleLot = (style + lot).toString().replace('#', '-');

                if ($('#' + styleLot).hasClass('alarmStyle')) {
                    $('#' + styleLot).removeClass('alarmStyle');
                } else {
                    $('#' + styleLot).addClass('alarmStyle');
                }
            }

        </script>
    </head>

    <body onload="clearTable()">
        <div id="wrapper">

            <!-- /#sidebar-wrapper -->
            <%@ include file="../fragments/sidebar.jsp" %>

            <!-- Page Content -->
            <!-- nav-head-custom -->
            <%@ include file="../fragments/nav_head.jsp" %>

            <div class="container-fluid">
                <!--<div id="wrapper-top">-->
                <%-- PART 2 --%>
                <!--                    <div class="row">
                                                <div class="col-lg-12">
                                                    <div id="set-height" style="height:415px;margin-top:0px;">
                                                        <div id="sidebar-wrapper-top" class="">
                                                            <b class="page-header" style="padding-left:5px;font-size:18px;">
                <%-- <hr style="margin:0px;margin-top:10px;margin-bottom:10px;"> --%>
            </b>
        </div>
    </div>
    </div>
    </div> -->
                <%-- PART 3 --%>
                <!--show Qdest table-->
                <br>
                <div id="childTableTmp" class="hidden"></div>
                <div id="cardTmp" class="hidden">
                    <div class="cardStyle" id="genCardId" wh="genCardWh" draggable="true" ondragstart="drag(event)" ondblclick="selectFile(this);">
                        <i class="fa fa-file-text-o styleLotIcon" aria-hidden="true"></i>
                        <p style="margin-top: 5px;">genCardText</p>
                    </div>
                </div>
                <div class="displayContain">
                    <div style=" text-align: right;">
                        <button type="button" id="backBtn" class="btn btn-default" style="width: 150px;">Back</button>
                    </div>
                    <div class="row">
                        <div id="fileFrom" class="filestore col-sm-2" ondrop="drop(event)" ondragover="allowDrop(event)">
                            <h3 style="text-align: center;">Sales Order by Style</h3>
                            <br>
                        </div>
                        <div id="fileTo" class="filestore col-sm-2" ondrop="drop(event)" ondragover="allowDrop(event)">
                            <h3 style="text-align: center;">Selected Style</h3>
                            <br>
                        </div>
                        <div id="fileTable" class="col-sm-8">
                            <table id="showTable" class="display displayTable">
                                <thead>
                                    <tr>
                                        <th>ลำดับที่</th>
                                        <th>รหัสวัตถุดิบ</th>
                                        <th>ชื่อวัตถุดิบ</th>
                                        <th>SAP ONHAND</th>
                                        <th>WMS ONHAND</th>
                                        <th>ปริมาณที่ต้องการ</th>
                                        <th>DIFF</th>
                                        <th>%</th>
                                        <th>MTCTRL</th>
                                        <th>U/M</th>
                                        <th>Check</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                </tfoot>
                                <tbody>
                                </tbody>
                            </table>
                            <center>
                                <div id="loadIcon" class="hidden">
                                    <h3>
                                        Loading...
                                        <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
                                    </h3>
                                </div>
                            </center>
                            <table width="100%">
                                <tr>
                                    <td width="50%" style=" vertical-align: top;">
                                        <button type="button" id="prepareBtn" class="btn btn-warning" style="width: 150px;">Prepare</button>
                                        <button type="button" id="generateBtn" class="btn btn-success" style="width: 150px;">Generate</button>
                                    </td>
                                    <td width="50%" align="right">
                                        <table width="70%">
                                            <tr>
                                                <th width="25%" class="centerTb">

                                                </th>
                                                <th width="45%" class="centerTb cardTb-l">
                                                    AVG. Percent
                                                </th>
                                                <th class="centerTb cardTb-r">
                                                    <span id="avgPer">0</span> %
                                                </th>
                                            </tr>
                                        </table>
                                        <br>
                                        <table width="70%">
                                            <tr>
                                                <th width="25%" class="centerTb">

                                                </th>
                                                <th class="centerTb cardTb-l">
                                                    TOTAL
                                                </th>
                                                <th class="centerTb cardTb-l">
                                                    <i class="fa fa-check-circle-o fa-2x" aria-hidden="true" style="color: green;"></i>
                                                </th>
                                                <th class="centerTb cardTb-l">
                                                    Percent
                                                </th>
                                            </tr>
                                            <tr>
                                                <th width="25%" class="centerTb cardTb-l">
                                                    SKU
                                                </th>
                                                <th class="rightTb cardTb-r">
                                                    <span id="skuTot">0</span>
                                                </th>
                                                <th class="rightTb cardTb-r">
                                                    <span id="skuCheck">0</span>
                                                </th>
                                                <th class="rightTb cardTb-r">
                                                    <span id="skuPer">0</span>
                                                </th>
                                            </tr>
                                            <tr>
                                                <th width="25%" class="centerTb cardTb-l">
                                                    PC
                                                </th>
                                                <th class="rightTb cardTb-r">
                                                    <span id="qtyTot">0</span>
                                                </th>
                                                <th class="rightTb cardTb-r">
                                                    <span id="qtyCheck">0</span>
                                                </th>
                                                <th class="rightTb cardTb-r">
                                                    <span id="qtyPer">0</span>
                                                </th>
                                            </tr>
                                            <tr>
                                                <th width="25%" class="centerTb cardTb-l">
                                                    Cost
                                                </th>
                                                <th class="rightTb cardTb-r">
                                                    <span id="costTot">0</span>
                                                </th>
                                                <th class="rightTb cardTb-r">
                                                    <span id="costCheck">0</span>
                                                </th>
                                                <th class="rightTb cardTb-r">
                                                    <span id="costPer">0</span>
                                                </th>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-sm-2">
                            <button type="button" id="deselectAllStyleBtn" class="btn btn-danger" style="width: 150px;">Deselect All</button>
                            <button type="button" id="selectAllStyleBtn" class="btn btn-primary" style="width: 150px;">Select All</button>
                        </div>
                        <div class="col-sm-2">
                            <table width="100%">
                                <tr>
                                    <th width="55%" class="centerTb cardTb-l">
                                        TOTAL Order
                                    </th>
                                    <th class="centerTb cardTb-r">
                                        <span id="cntTotOrder">0</span>
                                    </th>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <!--**********************-->
                    <!--End Part 3-->
                    <!--</div>  end #wrapper-top-->
                </div>
                <br>
            </div> <!-- end .container-fluid -->
        </div> <!-- end #wrapper -->
    </body>

</html>