<%@ include file="../fragments/taglibs.jsp" %>
<%@ include file="../fragments/imports.jsp" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.sql.*;" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>WMS</title>
        <!-- css :: vendors -->
        <jsp:include page = "../fragments/css.jsp" />
        <!-- additional custom :: my-style -->
        <link rel="stylesheet" href="../resources/assets/styles/myStyles.css">

        <!-- js :: vendors -->
        <jsp:include page = "../fragments/script.jsp" />
        <!-- additional custom :: my-script -->
        <script src="../resources/assets/scripts/myScripts.js" async></script>
        <script src="../resources/assets/scripts/toggleLoad.js" async></script> 
        ${sendMessage}
        <style>
            input[type=text], select{
                width: 100%;
                padding: 5px 5px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }

            input[type=date], input[type=number]{
                /*width: 100%;*/
                padding: 5px 5px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }

            input[type=submit] {
                width: 100%;
                background-color: #00399b;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=submit]:hover {
                background-color: #3973d6;
            }

            input[type=button] {
                width: 100%;
                background-color: #ef604a;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=button]:hover {
                background-color: #e51e00;
            }
        </style>
        <style>
            body {font-family: Arial, Helvetica, sans-serif;}

            /* The Modal (background) */
            .modal {
                display: none; /* Hidden by default */
                position: fixed; /* Stay in place */
                z-index: 1; /* Sit on top */
                padding-top: 100px; /* Location of the box */
                left: 0;
                top: 0;
                width: 100%; /* Full width */
                height: 100%; /* Full height */
                overflow: auto; /* Enable scroll if needed */
                background-color: rgb(0,0,0); /* Fallback color */
                background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
            }

            /* Modal Content */
            .modal-content {
                background-color: #fefefe;
                margin: auto;
                padding: 20px;
                border: 1px solid #888;
                width: fit-content;
                height: 320px;
            }

            /* The Close Button */
            .close {
                color: #aaaaaa;
                float: right;
                font-size: 28px;
                font-weight: bold;
            }

            .close:hover,
            .close:focus {
                color: #000;
                text-decoration: none;
                cursor: pointer;
            }

            button[name=ok] {
                width: 25%;
                background-color: #008cff;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            button[name=ok]:hover {
                background-color: #008cff;
            }
        </style>
        <script>
            $(document).ready(function () {
                $('#showTable').DataTable({
                    "lengthMenu": [[10, 25, 50, 100], [10, 25, 50, 100]],
                    "bSortClasses": false,
                    "columnDefs": [
                        {orderable: false, targets: [10]},
//                        {orderable: false, targets: [4]},
//                        {orderable: false, targets: [5]},
//                        {"width": "10%", "targets": 9}
                    ]
                });

                // Setup - add a text input to each footer cell
                $('#showTable tfoot th').not(":eq(10)").each(function () {
                    var title = $(this).text();
                    $(this).html('<input type="text" />');
                });

                // DataTable
                var table = $('#showTable').DataTable();

                // Apply the search
                table.columns().every(function () {
                    var that = this;
                    $('input', this.footer()).on('keyup change', function () {
                        if (that.search() !== this.value) {
                            that
                                    .search(this.value)
                                    .draw();
                        }
                    });
                });
            });
        </script>
    </head>    
    <body onload="document.getElementById('userid').value = sessionStorage.getItem('uid');">
        <div id="wrapper">

            <!-- /#sidebar-wrapper -->
            <%@ include file="../fragments/sidebar.jsp" %>

            <!-- Page Content -->
            <!-- nav-head-custom -->
            <%@ include file="../fragments/nav_head.jsp" %>

            <div class="container-fluid">
                <!--<div id="wrapper-top">-->
                <%-- PART 2 --%>
                <!--                    <div class="row">
                                        <div class="col-lg-12">
                                            <div id="set-height" style="height:415px;margin-top:0px;">
                                                <div id="sidebar-wrapper-top" class="">
                                                    <b class="page-header" style="padding-left:5px;font-size:18px;">
                <%-- <hr style="margin:0px;margin-top:10px;margin-bottom:10px;"> --%>
            </b>
        </div>
    </div>
</div>
</div> -->
                <%-- PART 3 --%>
                <!--show Qdest table-->
                <form action="../resources/manual/XMS500.pdf" target="_blank">
                    <table width="100%">
                        <tr>
                            <td width="94%"></td>
                            <td width="6%" align="right"><input type="submit" value="Manual" /></td>
                        </tr>
                    </table>
                </form>
                <div style="border: 2px solid #ccc; border-radius: 5px; padding: 20px;">
                    <form id="frm" name="frm" action="create" method="post">
                        <input type="hidden" id="userid" name="userid">
                        <input type="hidden" id="uid" name="uid" value="${uid}">
                        <table width="50%">
                            <tr>
                                <td width="70%" align="right"><b style="color: #00399b; font-size: 20px;">QR ID. : </b>
                                    <input type="text" name="qr" id="qr" style="width:60%;" autofocus>
                                    <script>
                                        var input = document.getElementById("qr");
                                        input.addEventListener("keyup", function (event) {
                                            input.value = input.value.toUpperCase();
                                            if (event.keyCode === 13) {
                                                event.preventDefault();
                                                document.getElementById('frm').submit();
                                            }
                                        });
                                    </script>  
                                    &nbsp;&nbsp;
                                    <i class="fa fa-qrcode" aria-hidden="true" style="font-size:30px;"></i>
                                </td>
                            </tr>
                        </table>   
                    </form>
                    <form id="frmDet" name="frmDet" action="edit" method="post">
                        <input type="hidden" id="uid" name="uid" value="${uid}">
                        <table id="showTable" class="display" style="width:100%; border: 1px solid #ccc; border-radius: 4px;">
                            <thead>
                                <tr>
                                    <th colspan="6" style="border-color: transparent;"></th>
                                    <th colspan="3" style="text-align: center; border-color: #ccc; color: #00c907;">Return</th> 
                                </tr>
                                <tr>
                                    <th style="text-align: center;">Material Code</th>
                                    <th style="text-align: center;">Description</th>
                                    <th style="text-align: center;">Quantity</th>
                                    <th style="text-align: center;">Package Type</th>
                                    <th style="text-align: center;">Plant</th>
                                    <th style="text-align: center;">QR ID.</th>
                                    <th style="text-align: center; color: #00c907;">Quantity</th>
                                    <th style="text-align: center; color: #00c907;">Package Type</th>
                                    <th style="text-align: center; color: #00c907;">Plant</th>
                                    <th style="text-align: center;">User</th>
                                    <th style="text-align: center;">Delete</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </tfoot>
                            <tbody>
                                <c:forEach items="${RMRList}" var="x">
                                    <tr>                                        
                                        <td>${x.XMSRMRMAT}</td>
                                        <td>${x.XMSRMRDESC}</td>
                                        <td style="text-align: right;">${x.XMSRMRQTY}</td>
                                        <td style="text-align: center;">${x.XMSRMRPACK}</td>
                                        <td style="text-align: center;">${x.XMSRMRPLANT}</td>
                                        <td>${x.XMSRMRQRID}<input type="hidden" name="qrid" value="${x.XMSRMRQRID}"></td>
                                        <td><input type="text" name="qty" value="${x.XMSRMRQTY}" style="text-align: right;"></td>
                                        <td>
                                            <select id="pack" name="pack">
                                                <option value="${x.XMSRMRPACK}" selected hidden>${x.XMSRMRPACK}</option>
                                                <option value="BAG">BAG</option>
                                                <option value="ROLL">ROLL</option>
                                                <option value="BOX">BOX</option>
                                            </select>
                                        </td>
                                        <td>
                                            <select id="plant" name="plant">
                                                <option value="${x.XMSRMRPLANT}" selected hidden>${x.XMSRMRPLANT}</option>
                                                <option value="1000">1000</option>
                                                <option value="1050">1050</option>
                                            </select>
                                        </td>
                                        <td>${x.XMSRMRUSER}<input type="hidden" name="user" value="${uid}"></td>
                                        <td align="center">
                                            <a onclick="document.getElementById('myModal-4-${x.XMSRMRQRID}-${uid}').style.display = 'block';" style=" cursor: pointer;"><i class="fa fa-trash" aria-hidden="true" style="font-size:25px;"></i></a>
                                            <div id="myModal-4-${x.XMSRMRQRID}-${uid}" class="modal">
                                                <!-- Modal content -->
                                                <div class="modal-content" style=" height: 250px; width: 500px;">
                                                    <span id="span-4-${x.XMSRMRQRID}-${uid}" class="close" onclick="document.getElementById('myModal-4-${x.XMSRMRQRID}-${uid}').style.display = 'none';">&times;</span>
                                                    <br>
                                                    <p><b style="color: #00399b;"><font size="5">Do you want to delete ?</font></b></p>
                                                    <br>
                                                    <table width="100%">
                                                        <tr style=" background-color: white;">
                                                            <td align="center" width="50%">
                                                                <b><font size="4">QR ID. : ${x.XMSRMRQRID}</font></b><br><br><br>
                                                                <a class="btn btn btn-outline btn-danger" onclick="document.getElementById('myModal-4-${x.XMSRMRQRID}-${uid}').style.display = 'none';">
                                                                    <i class="fa fa-check-circle-o" style="font-size:20px;"></i> Cancel</a>  
                                                                <a class="btn btn btn-outline btn-success" onclick="window.location.href = '/TABLE2/XMS500/delete?qr=${x.XMSRMRQRID}&uid=${uid}'">
                                                                    <i class="fa fa-check-circle-o" style="font-size:20px;"></i> Confirm</a>  
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                </c:forEach>
                            </tbody>
                        </table>
                    </form>
                    <center>
                        <button type="button" class="btn btn-primary" onclick="document.getElementById('myModal-6').style.display = 'block';"><i class="fa fa-floppy-o" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Confirm</button>
                        <button type="button" class="btn btn-danger" onclick="document.getElementById('myModal-5').style.display = 'block';"><i style="font-size: 18px;" class="fa fa-times-circle-o" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Clear All</button>
                    </center>                        
                    <br><br><br>
                    <div id="myModal-5" class="modal">
                        <!-- Modal content -->
                        <div class="modal-content" style=" height: 200px; width: 500px;">
                            <span id="span-5" class="close" onclick="document.getElementById('myModal-5').style.display = 'none';">&times;</span>
                            <br>
                            <center>
                                <p><b style="color: #00399b;"><font size="5">Do you want to clear all ?</font></b></p>
                                <br>
                                <a class="btn btn btn-outline btn-danger" onclick="document.getElementById('myModal-5').style.display = 'none';">
                                    <i class="fa fa-check-circle-o" style="font-size:20px;"></i> Cancel</a> 
                                <a class="btn btn btn-outline btn-success" onclick="window.location.href = '/TABLE2/XMS500/delete?qr=all&uid=${uid}'">
                                    <i class="fa fa-check-circle-o" style="font-size:20px;"></i> Confirm</a> 
                            </center>
                        </div>
                    </div>
                    <div id="myModal-6" class="modal">
                        <!-- Modal content -->
                        <div class="modal-content" style=" height: 200px; width: 500px;">
                            <span id="span-6" class="close" onclick="document.getElementById('myModal-6').style.display = 'none';">&times;</span>
                            <br>
                            <center>
                                <p><b style="color: #00399b;"><font size="5">Do you want to confirm ?</font></b></p>
                                <br>
                                <a class="btn btn btn-outline btn-danger" onclick="document.getElementById('myModal-6').style.display = 'none';">
                                    <i class="fa fa-check-circle-o" style="font-size:20px;"></i> Cancel</a> 
                                <a class="btn btn btn-outline btn-success" onclick="document.getElementById('frmDet').submit();">
                                    <i class="fa fa-check-circle-o" style="font-size:20px;"></i> Confirm</a> 
                            </center>
                        </div>
                    </div>
                    <!--**********************-->
                    <!--End Part 3-->
                    <!--</div>  end #wrapper-top--> 
                </div> 
                <br>
            </div> <!-- end .container-fluid -->
        </div> <!-- end #wrapper -->
    </body>
</html>