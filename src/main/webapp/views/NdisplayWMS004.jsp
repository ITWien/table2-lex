<%@ include file="../fragments/taglibs.jsp" %>
<%@ include file="../fragments/imports.jsp" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.sql.*;" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>WMS</title>
        <!-- css :: vendors -->
        <jsp:include page = "../fragments/css.jsp" />
        <!-- additional custom :: my-style -->
        <link rel="stylesheet" href="../resources/assets/styles/myStyles.css">

        <!-- js :: vendors -->
        <jsp:include page = "../fragments/script.jsp" />
        <!-- additional custom :: my-script -->
        <script src="../resources/assets/scripts/myScripts.js" async></script>
        <script src="../resources/assets/scripts/toggleLoad.js" async></script>
        <script>
            function Uppercase() {
                var x = document.getElementById("searchMatCode");
                x.value = x.value.toUpperCase();

                var y = document.getElementById("searchTable");
                y.value = y.value.toUpperCase();
            }
        </script>
        <script>
            $(document).ready(function () {
                $('#showTable').DataTable({
                    "lengthMenu": [[10, 25, 50, 100], [10, 25, 50, 100]],
                    "bSortClasses": false
                });

                // Setup - add a text input to each footer cell
                $('#showTable tfoot th').not(":eq(1)").not(":eq(1)").not(":eq(1)")
                        .not(":eq(1)").not(":eq(1)").not(":eq(1)").not(":eq(1)")
                        .not(":eq(1)").each(function () {
                    var title = $(this).text();
                    $(this).html('<input type="text" id="searchTable" name="searchTable" onkeyup="Uppercase()" />');
                });

                // DataTable
                var table = $('#showTable').DataTable();

                // Apply the search
                table.columns().every(function () {
                    var that = this;

                    $('input', this.footer()).on('keyup change', function () {
                        if (that.search() !== this.value) {
                            that
                                    .search(this.value)
                                    .draw();
                        }
                    });
                });
            });
        </script>
        <style>
            input[type=text], select {
                width: 100%;
                padding: 5px 5px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }

            input[type=submit] {
                width: 100%;
                background-color: #3973d6;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=submit]:hover {
                background-color: #00399b;
            }
        </style>
    </head>    
    <body >
        <div id="wrapper">

            <!-- /#sidebar-wrapper -->
            <%@ include file="../fragments/sidebar.jsp" %>

            <!-- Page Content -->
            <!-- nav-head-custom -->
            <%@ include file="../fragments/nav_head.jsp" %>

            <div class="container-fluid">
                <!--<div id="wrapper-top">-->
                <%-- PART 2 --%>
                <!--                    <div class="row">
                                        <div class="col-lg-12">
                                            <div id="set-height" style="height:415px;margin-top:0px;">
                                                <div id="sidebar-wrapper-top" class="">
                                                    <b class="page-header" style="padding-left:5px;font-size:18px;">
                <%-- <hr style="margin:0px;margin-top:10px;margin-bottom:10px;"> --%>
            </b>
        </div>
    </div>
</div>
</div> -->
                <%-- PART 3 --%>
                <!--show Qdest table-->
                <br>
                <div style="border: 2px solid #ccc; border-radius: 5px; padding: 20px;">
                    <form action="ndisplay" method="POST">
                        <table>
                            <tr>
                                <th><h4>Material Control : </h4></th>
                                <td>
                                    <select name="matctrl">
                                        <option value="${mc}" selected hidden>${mc} : ${name}</option>
                                        <option value=" "></option>
                                        <c:forEach items="${MCList}" var="p" varStatus="i">
                                            <option value="${p.uid}">${p.uid} : ${p.name}</option>
                                        </c:forEach>
                                    </select>
                                </td>
                                <th></th>
                                <th><h4>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Material Code : </h4></th>
                                <td><input type="text" id="searchMatCode" name="searchMatCode" maxlength="18" value="${searchVal}" onkeyup="Uppercase()"/></td>
                                <td>&nbsp;&nbsp;&nbsp;<input style="width: 100px;" type="submit" value="Search" /></td>
                            </tr>
                        </table>
                    </form>
                    <table id="showTable" class="display" style="width:100%; border: 1px solid #ccc; border-radius: 4px;">
                        <thead>
                            <tr>
                                <th>Material Code</th>
                                <th>Description</th>
                                <th>Plant</th>
                                <th>V type</th>
                                <th>M type</th>
                                <th>U / M</th>
                                <th>M group</th>
                                <th>Description</th>
                                <th>Option</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>Material Code</th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                            </tr>
                        </tfoot>
                        <tbody>
                            <c:forEach items="${MCNList}" var="p" varStatus="i">
                                <tr>
                                    <td>${p.matcode}</td>
                                    <td>${p.desc}</td>
                                    <td>${p.plant}</td>
                                    <td>${p.vtype}</td>
                                    <td>${p.mtype}</td>
                                    <td>${p.um}</td>
                                    <td>${p.mgroup}</td>
                                    <td>${p.mgdesc}</td>
                                    <td>
                                        <a href="editN?uid=${p.matcode}&vt=${p.vtype}&plant=${p.plant}"><i class="glyphicon glyphicon-edit" style="font-size:15px;"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <a href="displayN?uid=${p.matcode}&vt=${p.vtype}&plant=${p.plant}"><i class="fa fa-laptop" style="font-size:20px;"></i></a>
                                    </td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>
                <!--End Part 3-->
                <br>
                <!--</div>  end #wrapper-top--> 
            </div> <!-- end .container-fluid -->
        </div> <!-- end #wrapper -->
    </body>
</html>