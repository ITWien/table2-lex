<%-- 
    Document   : displayISM200ShowOnly
    Created on : Jan 25, 2023, 8:17:55 AM
    Author     : 93176
--%>

<%@ include file="../fragments/taglibs.jsp" %>
<%@ include file="../fragments/imports.jsp" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>ISM</title>
        <!-- css :: vendors -->
        <jsp:include page="../fragments/css.jsp" />
        <!-- additional custom :: my-style -->
        <link rel="stylesheet" href="../resources/assets/styles/myStyles.css">
        <link rel="stylesheet" href="../resources/font/THsarabun/thsarabumnew.css">

        <!-- js :: vendors -->
        <jsp:include page="../fragments/script.jsp" />
        <!-- additional custom :: my-script -->
        <script src="../resources/assets/scripts/myScripts.js" async></script>
        <script src="../resources/assets/scripts/toggleLoad.js" async></script>

        <style>

            input[type=text],
            select {
                width: 100%;
                padding: 5px 5px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }

            input[type=date],
            input[type=number] {
                /*width: 100%;*/
                padding: 5px 5px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }

            input[type=submit] {
                width: 100%;
                background-color: #00399b;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=submit]:hover {
                background-color: #3973d6;
            }

            input[type=button] {
                width: 100%;
                background-color: #ef604a;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=button]:hover {
                background-color: #e51e00;
            }

            #closeBtn{
                /*width: 150px;*/
                background-color: #575656;
                border-color: #575656;
                color:white;
                /*padding: 0px;*/
            }

            #closeBtn:hover{
                /*width: 150px;*/
                background-color: #000000;
                border-color: #000000;
                color:white;
                /*padding: 0px;*/
            }

            body {
                font-family: "Sarabun", sans-serif !important;
                /*font-family: 'THSarabunNew', sans-serif;*/
            }

            .displayTable {
                width: auto;
                border: 1px solid #ccc;
                border-radius: 4px;
            }

            .displayContain {
                border: 2px solid #ccc;
                border-radius: 0px 0px 5px 5px;
                padding: 20px;
                /*width: fit-content;*/
            }

            .checkData ,.checkAllData{
                width: 20px;
                height: 20px;
            }

            .sts-0{
                font-size: 20px;
                color: navy;
            }
            .sts-1{
                font-size: 20px;
                color: yellow;
            }
            .sts-2{
                font-size: 20px;
                color: green;
            }
            .sts-3{
                font-size: 20px;
                color: blue;
            }
            .sts-4{
                font-size: 20px;
                color: pink;
            }
            .sts-5{
                font-size: 20px;
                color: navy;
            }
            .sts-6{
                font-size: 20px;
                color: gray;
            }
            .sts-7{
                font-size: 20px;
                color: brown;
            }
            .sts-8{
                font-size: 20px;
                color: orange;
            }
            .sts-9{
                font-size: 20px;
                color: black;
            }

            .modal-content{
                width: 50%;
            }

            .splitBtn{
                color: navy;
                cursor: pointer;
            }

            .detailBand{
                text-align: center;
                padding: 5px;
                color: white;
                background-color: navy;
                border-radius: 5px 5px 0px 0px;
            }
        </style>

        <script>
            var tableOptions = {
                "paging": true,
                "lengthMenu": [[-1, 10, 25, 50, 100], ["All", 10, 25, 50, 100]],
                "bSortClasses": false,
                "ordering": true,
                "info": true,
                "fixedColumns": true,
                "order": [[0, "asc"]],
                "columnDefs": [
                    {"targets": [2, 3, 4], "className": "text-left"},
                    {"targets": [5, 6, 7], "className": "text-right"},
                    {"targets": [0, 1, 8, 11], "className": "text-center"}
                ],
                "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
//                    var sino = aData[2];
//                    if (sino !== "") {
//                        if (sino !== "0") {
//                            if (parseInt(sino) % 2 === 0) {
//                                $('td', nRow).css('background-color', '#f5e9e6');
//                            } else {
//                                $('td', nRow).css('background-color', '#f5dfda');
//                            }
//                        } else {
//                            $('td', nRow).css('background-color', '#ffa38a');
//                        }
//                    }
                }
            };

            $(document).ready(function () {

//                getLevel(sessionStorage.getItem('uid'));
                $('#uid').val(sessionStorage.getItem('uid'));

                $('#showTable').DataTable(tableOptions);

                // Setup - add a text input to each footer cell
                $('#showTable tfoot th').not(":eq(0),:eq(12)").each(function () {
                    var title = $(this).text();
                    $(this).html('<input type="text" />');
                });

                // DataTable
                var table = $('#showTable').DataTable();

                // Apply the search
                table.columns().every(function () {
                    var that = this;

                    $('input', this.footer()).on('keyup change', function () {
                        if (that.search() !== this.value) {
                            that.search(this.value).draw();
                        }
                    });
                });

                getAppHead();
                getDetail();

            });

            function getAppHead() {

                $.ajax({
                    url: "/TABLE2/ShowDataTablesISM100?mode=getAppHead"
                            + "&orderNo=${orderNo}&seq=" + ${seq} + ""
                }).done(function (result) {
//                    console.log(result);

                    $('#headTranDate').val(result.ISHTRDT);
                    $('#headReqDate').val(result.ISHRQDT);
                    $('#headStyleFG').val(result.ISHMATCODE + ' ' + result.ISHLOT);
                    $('#headQty').val(result.ISHAMTFG);
                    $('#headRespSty').val(result.ISHRPUSR);
                    $('#headCustomer').val(result.ISHCUNO + ' ' + result.ISHCUNM1);
                    $('#headDest').val(result.ISHDEST);
                    $('#headSaleOrder').val(result.ISHORD);
                    $('#headSeq').val(String.fromCharCode(parseInt(result.ISHSEQ) + 64));
                    $('#headDocNo').val(result.ISHSUBMI);
                    $('#headRemark').val(result.ISHREMARK);
                    $('#headReason').val(result.ISHREASON);

                    $('#footReqUser').html(result.ISHEUSR);
                    $('#footReqDate').html(result.ISHEDT);
                    $('#footAppUser').html(result.ISHAPUSR);
                    $('#footAppDate').html(result.ISHAPDT);

                    $('#KEEPSTS').val(result.LSTS);

                    if (result.LSTS === result.HSTS) {
                        if (result.LSTS === '2' || result.LSTS === '3' || result.LSTS === '4' || result.LSTS === '5' || result.LSTS === '6') {
                            $('#appIcon').attr('src', '../resources/images/ISM/approved.png');
                        } else if (result.LSTS === '8') {
                            $('#appIcon').attr('src', '../resources/images/ISM/cancelled.png');
                        }
                    }

                }).fail(function (jqXHR, textStatus, errorThrown) {
                    // needs to implement if it fails
                });

                $.ajax({
                    url: "/TABLE2/ShowDataTablesISM100",
                    data: {mode: "checkRMsts", orderNo: "${orderNo}", seq: "${seq}"},
                    async: false
                }).done(function (result) {

                    if (result) {
                        $('#sentRMBtn').show();
                        $('#delBtn').show();
                        $('#closeBtn').show();
                        $('#reqAppBtn').hide();
                        $('#respStyBtn').hide();
                        $('#delBtnChild').hide();
                    } else {
                        $('#sentRMBtn').hide();
                        $('#delBtn').hide();
                        $('#closeBtn').hide();
                    }

                }).fail(function (jqXHR, textStatus, errorThrown) {
                    // needs to implement if it fails
                });
            }

            function getDetail() {

                var table = $('#showTable').DataTable();
                table.clear().draw();
                $('#loadIcon').removeClass('hidden');

                $('.checkAllData').prop("checked", false);
//                getAppHead();

                $.ajax({
                    url: "/TABLE2/ShowDataTablesISM100?mode=getDetail"
                            + "&orderNo=" + ${orderNo} + "&seq=" + ${seq} + ""
                }).done(function (result) {
//                    console.log(result);

                    for (var i = 0; i < result.length; i++) {

                        var wh = '';
                        if (result[i].ISDITNO.toString().trim().charAt(0) === '1') {
                            wh = 'WH2';
                        } else {
                            wh = 'WH3';
                        }

//                        var delChild = '';
//                        if (result[i].ISDSINO.toString().trim() === "0" && result[i].FAM.toString().trim() !== "") {
//                            delChild = '<a class="delChildIcon" onclick="deleteDetChild(\'' + result[i].LINO + '-' + result[i].ISDSINO + '\',\'' + result[i].LINO + '\');"><i class="fa fa-2x fa-refresh splitBtn" aria-hidden="true"></i></a>';
//                        } else {
////                            console.log(result[i].STS);
//                            if (result[i].STS === "0") {
//                                var replacedouble = result[i].ISDDESC.toString().replace('"', '$%').trim(); // replace " to $% Before Send Paremeter
//                                delChild = '<a class="splitChildIcon" onclick="splitDet(\'' + result[i].ISDLINO + ' / ' + result[i].ISDSINO + '\',\'' + result[i].ISDITNO.toString().trim() + ' : ' + replacedouble + '\',\'' + result[i].ISDRQQTY + '\',\'' + result[i].LINO + '-' + result[i].ISDSINO + '\',\'' + wh + '\',\'' + result[i].ISDVT.toString().trim() + '\');"><i class="fa fa-2x fa-sitemap splitBtn" aria-hidden="true"></i></a>';
//                            } else {
//                                delChild = '';
////                                delChild = '<a class="splitChildIcon" onclick="alertify.error(\'Can Not Split.\');"><i class="fa fa-2x fa-sitemap splitBtn" style="color: red;" aria-hidden="true"></i></a>';
//                            }
//
//                        }

//                        let checkHTML = "";

//                        console.log(" remain " + result[i].ISDREMAIN + " sts " + result[i].ISDSTS);
//                        console.log("cont " + result[i].ISDSTS.toString().includes("Ben"));

//                        if (result[i].ISDREMAIN !== "C" && result[i].ISDSTS.toString().includes("New")) { // Remain != C and Sts = 0
//                            checkHTML = '<input type="checkbox" class="checkData" wh="' + wh + '" value="' + result[i].LINO + '-' + result[i].ISDSINO + '">';
//                        }
//                        console.log(checkHTML);

                        if (result[i].ISDREMAIN === "C") {
                            $('#appIcon').attr('src', '../resources/images/ISM/cancelled.png');
                        }

                        //hidden checkbox when press new btn
                        table.row.add([
                            result[i].ISDLINO,
                            result[i].FAM,
                            result[i].ISDITNO,
                            result[i].ISDVT,
                            result[i].ISDDESC,
                            result[i].ISDSAPONH,
                            result[i].ISDWMSONH,
                            result[i].ISDRQQTY,
                            result[i].ISDUNIT,
                            result[i].ISDRQDT,
                            result[i].ISDDEST,
                            '<i class="fa fa-circle' + result[i].STSI + ' sts-' + result[i].STS + '" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="' + result[i].ISDSTS + '"></i>'
//                            delChild
                        ]).draw(false);

                    }

                    var level = $('#level').val();
                    if (parseInt(level) >= 7) {
                        $('.delChildIcon, .splitChildIcon').css("display", "none");
                    }
//                    $('.checkAllData, .checkData').prop('disabled', true).prop('checked', true);

                    $('[data-toggle="tooltip"]').tooltip();
                    $('.childSplit').parent().parent().addClass('child-bg');

                    $('#loadIcon').addClass('hidden');

                }).fail(function (jqXHR, textStatus, errorThrown) {
                    // needs to implement if it fails
                });

            }

        </script>

    </head>
    <body>
        <div id="wrapper">
            <!-- /#sidebar-wrapper -->
            <%@ include file="../fragments/sidebar.jsp" %>

            <!-- Page Content -->
            <!-- nav-head-custom -->
            <%@ include file="../fragments/nav_head.jsp" %>

            <div class="container-fluid">
                <br>
                <div class="detailBand">
                    <h3 style="margin: 0;">DETAIL DISPLAY </h3>
                </div>
                <div class="displayContain">
                    <table width="100%">
                        <tr>
                            <th width="5%"></th>
                            <th width="9%">
                                Transaction Date :
                            </th>
                            <th width="14%">
                                <input type="text" id="headTranDate" style="width: 150px;" readonly>
                            </th>
                            <th width="6%">
                                Style FG :
                            </th>
                            <th width="13%">
                                <input type="text" id="headStyleFG" style="width: 200px;" readonly>
                            </th>
                            <th width="9%">
                                Customer :
                            </th>
                            <th width="18%">
                                <input type="text" id="headCustomer" style="width: 300px;" readonly>
                            </th>
                            <th width="7%">
                                Sale Order :
                            </th>
                            <th width="13%">
                                <input type="text" id="headSaleOrder" style="width: 145px;" readonly>

                            </th>
                            <th width="6%"></th>
                            <th width="16%">
                                <input type="text" id="headReason" style="width: 145px;" readonly>
                            </th>
                        </tr>
                        <tr>
                            <th></th>
                            <th>
                                Requested Date :
                            </th>
                            <th>
                                <input type="text" id="headReqDate" style="width: 150px;" readonly>
                            </th>
                            <th>
                                Quantity :
                            </th>
                            <th>
                                <input type="text" id="headQty" style="width: 200px;" readonly>
                            </th>
                            <th>
                                Response Style :
                            </th>
                            <th>
                                <input type="text" id="headRespSty" style="width: 300px;" readonly>
                            </th>
                            <th>
                                Destination :
                            </th>
                            <th>
                                <input type="text" id="headDest" style="width: 150px;" readonly>
                            </th>
                            <th>
                                Doc no. :
                            </th>
                            <th>
                                <input type="text" id="headDocNo" style="width: 200px;" readonly>
                            </th>
                        </tr>
                    </table>
                </div>
                <div class="displayContain">
                    <div class="mngShow">
                        <table width="100%">
                            <tr>
                                <td width="50%"></td>
                                <td width="50%" style="text-align: right;">
                                    <button type="button" id="backBtn" class="btn" style="width: 150px;" onclick="window.location.href = 'display';">Back</button>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <table id="showTable" class="display displayTable">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th></th>
                                <th>Material Code</th>
                                <th>VT</th>
                                <th>Description</th>
                                <th>SAP ONH</th>
                                <th>WMS ONH</th>
                                <th>Requested Qty</th>
                                <th>Unit</th>
                                <th>Requested Date</th>
                                <th>Destination</th>
                                <th>Status</th>
                                <!--<th>Option</th>-->
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <!--<th></th>-->
                            </tr>
                        </tfoot>
                        <tbody>
                        </tbody>
                    </table>

                    <center>
                        <div id="loadIcon" class="hidden">
                            <h3>
                                Loading...
                                <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
                            </h3>
                        </div>
                    </center>
                </div>
            </div>
        </div>
    </body>
</html>
