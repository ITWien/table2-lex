<%@ include file="../fragments/taglibs.jsp" %>
<%@ include file="../fragments/imports.jsp" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.sql.*;" %>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>ISM</title>
        <!-- css :: vendors -->
        <jsp:include page="../fragments/css.jsp" />
        <!-- additional custom :: my-style -->
        <link rel="stylesheet" href="../resources/assets/styles/myStyles.css">
        <link rel="stylesheet" href="../resources/font/THsarabun/thsarabumnew.css">

        <!-- js :: vendors -->
        <jsp:include page="../fragments/script.jsp" />
        <!-- additional custom :: my-script -->
        <script src="../resources/assets/scripts/myScripts.js" async></script>
        <script src="../resources/assets/scripts/toggleLoad.js" async></script>
        <style>
            input[type=text],
            select {
                width: 100%;
                padding: 5px 5px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }

            input[type=date],
            input[type=number] {
                /*width: 100%;*/
                padding: 5px 5px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }

            input[type=submit] {
                width: 100%;
                background-color: #00399b;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=submit]:hover {
                background-color: #3973d6;
            }

            input[type=button] {
                width: 100%;
                background-color: #ef604a;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=button]:hover {
                background-color: #e51e00;
            }
        </style>
        <script>

            jQuery.extend(jQuery.fn.dataTableExt.oSort, {//date sorting
                "date-uk-pre": function (a) {
                    if (a == null || a == "") {
                        return 0;
                    }
                    var ukDatea = a.split('/');
                    return (ukDatea[2] + ukDatea[1] + ukDatea[0]) * 1;
                },

                "date-uk-asc": function (a, b) {
                    return ((a < b) ? -1 : ((a > b) ? 1 : 0));
                },

                "date-uk-desc": function (a, b) {
                    return ((a < b) ? 1 : ((a > b) ? -1 : 0));
                }
            });

            var tableOptions = {
                "paging": true,
                "lengthMenu": [[-1, 10, 25, 50, 100], ["All", 10, 25, 50, 100]],
                "bSortClasses": false,
                "ordering": true,
                "info": true,
                "fixedColumns": true,
                "order": [[1, "desc"], [2, "desc"], [3, "desc"]],
                //                "order": [[2, "desc"], [3, "desc"]],
                "columnDefs": [
                    {"targets": [9, 10], "className": "text-right"},
                    {"targets": [0, 11, 12, 18], "className": "text-center"},
                    {"targets": [0, 18], "orderable": false},
                    {"targets": 1, "type": "date-uk"}
                ]
            };
        </script>
        <style>
            body {
                font-family: "Sarabun", sans-serif !important;
                /*font-family: 'THSarabunNew', sans-serif;*/
            }

            .displayTable {
                width: auto;
                border: 1px solid #ccc;
                border-radius: 4px;
            }

            .displayContain {
                border: 2px solid #ccc;
                border-radius: 5px;
                padding: 20px;
                /*width: fit-content;*/
            }

            /*            td {
    white-space: nowrap;
    cursor: pointer;
}*/

            .checkData ,.checkAllData,.checkDataRM ,.checkAllDataRM{
                width: 20px;
                height: 20px;
            }

            .sts-0{
                font-size: 20px;
                color: navy;
            }
            .sts-1{
                font-size: 20px;
                color: yellow;
            }
            .sts-2{
                font-size: 20px;
                color: green;
            }
            .sts-3{
                font-size: 20px;
                color: blue;
            }
            .sts-4{
                font-size: 20px;
                color: pink;
            }
            .sts-5{
                font-size: 20px;
                color: navy;
            }
            .sts-6{
                font-size: 20px;
                color: gray;
            }
            .sts-7{
                font-size: 20px;
                color: brown;
            }
            .sts-8{
                font-size: 20px;
                color: orange;
            }
            .sts-9{
                font-size: 20px;
                color: black;
            }

            .modal-content{
                width: 50%;
            }

            .editBtn{
                color: navy;
                cursor: pointer;
            }

            .colop{
                display: inline-flex;
            }

            #reMainBtn{
                width: 150px;
                background-color: #F28A50;
                border-color: #F28A50;
                color:white;
                padding: 0px;
            }

            #reMainBtn:hover{
                width: 150px;
                background-color: #ed7328;
                border-color: #F28A50;
                color:white;
                padding: 0px;
            }

        </style>
        <script>
            $(document).ready(function () {

                //                sessionStorage.setItem('uid', '93176');
                //                sessionStorage.setItem('uid', '92655');

                getLevel(sessionStorage.getItem('uid'));

                var levl = $('#level').val();
                console.log(levl);


                $('#uid').val(sessionStorage.getItem('uid'));

                $('#showTable').DataTable(tableOptions);

                // Setup - add a text input to each footer cell
                $('#showTable tfoot th').not(":eq(0),:eq(18)").each(function () {
                    var title = $(this).text();
                    $(this).html('<input type="text" />');
                });

                // DataTable
                var table = $('#showTable').DataTable();
                //                var tableRM = $('#remainTable').DataTable();

                // Apply the search
                table.columns().every(function () {
                    var that = this;
                    $('input', this.footer()).on('keyup change', function () {
                        if (that.search() !== this.value) {
                            that.search(this.value).draw();
                        }
                    });
                });
                //                tableRM.columns().every(function () {
                //                    var that = this;
                //                    $('input', this.footer()).on('keyup change', function () {
                //                        if (that.search() !== this.value) {
                //                            that.search(this.value).draw();
                //                        }
                //                    });
                //                });

                getWH();
                getProductGroup();

                $('.checkAllData').click(function () {
                    var checkAll = $(this);
                    $('.checkData').prop("checked", checkAll.prop("checked"));
                });
                $('.checkAllDataRM').click(function () {
                    var checkAllRM = $(this);
                    $('.checkDataRM').prop("checked", checkAllRM.prop("checked"));
                });

                $('#reqDateBtn').click(function () {

                    var checkData = $('.checkData:checked');

                    if (checkData.length > 0) {
                        $('#reqDateModal').modal('show');
                    } else {
                        alertify.alert("No Data Selected!",
                                function () {
                                    alertify.error('Please select data!');
                                }
                        );
                    }

                });

                $('#updateReqDateBtn').click(function () {

                    var regDate = $('#reqDate').val().toString().replace(/-/g, '');
                    var checkData = $('.checkData:checked');

                    if (checkData.length > 0) {
                        if (regDate === '') {
                            alertify.error('Please select Requested Date!');
                            return false;
                        }

                        var cntEditUser = 0;

                        for (var i = 0; i < checkData.length; i++) {
                            var orderNo = $(checkData[i]).val().split("_")[0];
                            var seq = $(checkData[i]).val().split("_")[1];

                            var editUser = $('#editUser-' + orderNo + "_" + seq).val().toString().trim();

                            if (editUser !== sessionStorage.getItem('uid')) {
                                if (editUser !== 'ok') {
                                    cntEditUser++;
                                }
                            }
                        }

                        if (cntEditUser === 0) {
                            for (var i = 0; i < checkData.length; i++) {

                                var checkhtml = $(checkData[i]).closest("tr").find("td:eq(11)").html();

                                if (checkhtml.toString().includes("0 = New Order")) {
                                    var orderNo = $(checkData[i]).val().split("_")[0];
                                    var seq = $(checkData[i]).val().split("_")[1];
                                    $.ajax({
                                        url: "/TABLE2/ShowDataTablesISM100?mode=updateReqDate"
                                                + "&orderNo=" + orderNo
                                                + "&seq=" + seq
                                                + "&regDate=" + regDate
                                                + "&uid=" + sessionStorage.getItem('uid')

                                    }).done(function (result) {
                                        console.log(result);

                                        $('#reqDate').val("");

                                    }).fail(function (jqXHR, textStatus, errorThrown) {
                                        // needs to implement if it fails
                                    });
                                }
                            }

                            setTimeout(getHead, 1000);
                            alertify.success('Update Success!');

                        } else {
                            alertify.alert("This record is not your owner!",
                                    function () {
                                        alertify.error('กรุณา เลือกเฉพาะรายการของคุณ');
                                    }
                            );
                        }

                    } else {
                        alertify.alert("No Data Selected!",
                                function () {
                                    alertify.error('Please select data!');
                                }
                        );
                    }

                });

                $('#destBtn').click(function () {

                    //                    var wh = $('#wh').val();
                    //                    getDest(wh);

                    var checkData = $('.checkData:checked');

                    if (checkData.length > 0) {
                        var orderNoFirst = $(checkData[0]).val().split("_")[0];
                        var seq = $(checkData[0]).val().split("_")[1];
                        var wh = $('#wh-' + orderNoFirst + "_" + seq).val().toString().trim();
                        $('#wh').val(wh);
                        getDest(wh);

                        $('#destModal').modal('show');
                    } else {
                        alertify.alert("No Data Selected!",
                                function () {
                                    alertify.error('Please select data!');
                                }
                        );
                    }

                });

                $('#updateDestBtn').click(function () {

                    var dest = $('#dest').val();
                    var checkData = $('.checkData:checked');

                    if (checkData.length > 0) {
                        if (dest === '') {
                            alertify.error('Please select Destination!');
                            return false;
                        }

                        var cntEditUser = 0;

                        for (var i = 0; i < checkData.length; i++) {
                            var orderNo = $(checkData[i]).val().split("_")[0];
                            var seq = $(checkData[i]).val().split("_")[1];
                            var editUser = $('#editUser-' + orderNo + "_" + seq).val().toString().trim();

                            if (editUser !== sessionStorage.getItem('uid')) {
                                if (editUser !== 'ok') {
                                    cntEditUser++;
                                }
                            }
                        }

                        if (cntEditUser === 0) {
                            for (var i = 0; i < checkData.length; i++) {

                                var checkhtml = $(checkData[i]).closest("tr").find("td:eq(11)").html();

                                if (checkhtml.toString().includes("0 = New Order")) {
                                    var orderNo = $(checkData[i]).val().split("_")[0];
                                    var seq = $(checkData[i]).val().split("_")[1];
                                    $.ajax({
                                        url: "/TABLE2/ShowDataTablesISM100?mode=updateDest"
                                                + "&orderNo=" + orderNo
                                                + "&dest=" + dest
                                                + "&seq=" + seq
                                                + "&uid=" + sessionStorage.getItem('uid')

                                    }).done(function (result) {
                                        console.log(result);

                                    }).fail(function (jqXHR, textStatus, errorThrown) {
                                        // needs to implement if it fails
                                    });
                                }
                            }

                            setTimeout(getHead, 1000);
                            alertify.success('Update Success!');

                        } else {
                            alertify.alert("This record is not your owner!",
                                    function () {
                                        alertify.error('กรุณา เลือกเฉพาะรายการของคุณ');
                                    }
                            );
                        }

                    } else {
                        alertify.alert("No Data Selected!",
                                function () {
                                    alertify.error('Please select data!');
                                }
                        );
                    }

                });

                $('#reqAppBtn').click(function () {

                    var checkData = $('.checkData:checked');

                    var updateCompl = true;

                    if (checkData.length > 0) {

                        var cntBlankDest = 0;
                        var cntEditUser = 0;

                        for (var i = 0; i < checkData.length; i++) {

                            var orderNo = $(checkData[i]).val().split("_")[0];
                            var seq = $(checkData[i]).val().split("_")[1];
                            var dest = $('#dest-' + orderNo + "_" + seq).val().toString().trim();
                            var editUser = $('#editUser-' + orderNo + "_" + seq).val().toString().trim();

                            if (editUser !== sessionStorage.getItem('uid')) {
                                if (editUser !== 'ok') {
                                    cntEditUser++;
                                }
                            }

                            if (dest === '') {
                                cntBlankDest++;
                            }
                        }

                        if (cntBlankDest === 0) {
                            if (cntEditUser === 0) {
                                for (var i = 0; i < checkData.length; i++) {

                                    var checkhtml = $(checkData[i]).closest("tr").find("td:eq(11)").html();

                                    if (checkhtml.toString().includes("0 = New Order")) {
                                        var orderNo = $(checkData[i]).val().split("_")[0];
                                        var seq = $(checkData[i]).val().split("_")[1];

                                        $.ajax({
                                            url: "/TABLE2/ShowDataTablesISM100?mode=getAppHead"
                                                    + "&orderNo=" + orderNo
                                                    + "&seq=" + seq,
                                            async: false
                                        }).done(function (result) {
                                            if (result.ISHRPUSR === "") {
//                                                alertify.error("ระบุชื่อและเบอร์โทร " + orderNo + " Seq " + seq);
                                                $('#plsinputRSUSR').modal('show');
                                                $('#RSUSR_SOrder').text(orderNo);
                                                $('#RSUSR_Seq').text(seq);
                                                updateCompl = false;

                                            } else {
                                                $.ajax({
                                                    url: "/TABLE2/ShowDataTablesISM100?mode=updateReqApp"
                                                            + "&orderNo=" + orderNo
                                                            + "&seq=" + seq
                                                            + "&uid=" + sessionStorage.getItem('uid')

                                                }).done(function (result) {
                                                    if (result === false) {
                                                        updateCompl = false;
                                                    }
                                                }).fail(function (jqXHR, textStatus, errorThrown) {
                                                    // needs to implement if it fails
                                                });
                                            }
                                        }).fail(function (jqXHR, textStatus, errorThrown) {
                                            // needs to implement if it fails
                                        });

                                    }

                                }

                                if (updateCompl) {
                                    setTimeout(getHead, 1000);
                                    alertify.success('Update Success!');
                                }

                            } else {
                                alertify.alert("This record is not your owner!",
                                        function () {
                                            alertify.error('กรุณา เลือกเฉพาะรายการของคุณ');
                                        }
                                );
                            }

                        } else {
                            alertify.alert("Destination must not be blank!",
                                    function () {
                                        alertify.error('กรุณา กำหนดสถานที่ส่งปลายทาง');
                                    }
                            );
                        }

                    } else {
                        alertify.alert("No Data Selected!",
                                function () {
                                    alertify.error('Please select data!');
                                }
                        );
                    }

                });

                $('#reMainBtn').click(function () {

                    let pdG = $('#productGroup').val();

                    //                    1) เพิ่มปุ่ม Create RemainOrder   เพื่อทำเลือกข้อมูลจาก ISMMASD โดยมีเงื่อนไขดังนี้
                    //                    - ISDSTS >= '6'
                    //                            - ISDRQQTY > ISDPKQTY  (หรือ ISDRQQTY - ISDPKQTY > 0)
                    //                            - ISDREMAIN = ' '
                    //                            - Product group เดียวกัน = จากหน้าจอ
                    //
                    //                    ค่าที่แสดง Req Qty เกิดจาก ISDRQQTY - ISDPKQTY
                    //
                    //                    2) เมื่อ Select จะทำงานดังนี้
                    //                            2.1 Insert new record ใน ISMMASH, ISMMASD ((MAX) Seq + 1)), ISDREMAIN = R: Remain Order, ISDSTS = 0 : New Order, (เก็บค่า Reference XXX เพื่ออ้างอิง
                    //                            2.2 Update old record ใน ISMMASD / ISDREMAIN = S: Select Order, (เก็บค่า Reference XXX เพื่ออ้างอิง

                    //Show Detail
                    if ($.fn.DataTable.isDataTable('#remainTable')) { //before RE-Create New Datatable
                        $('#remainTable').DataTable().destroy();
                    }

                    $('#remainTable tbody').empty();

                    $('#remainTable').dataTable({
                        ajax: {
                            url: "/TABLE2/ShowDataTablesISM100",
                            data: {mode: "getRemainData", pdg: pdG},
                            dataSrc: 'datalist'
                        },
                        "aoColumns": [
                            {'mRender': function (data, type, full) {
                                    return '<input type="checkbox" class="checkDataRM" value="' + full.ISDORD + "_" + full.ISDSEQ + "_" + full.ISDLINO + "_" + full.ISDSINO + "_" + full.FAM + '">';
                                }
                            },
                            {"data": "ISDORD"},
                            {"data": "ISDSEQ"},
                            {"data": "ISDLINO"},
                            {"data": "ISDSINO"},
                            {"data": "ISDITNO"},
                            {"data": "ISDVT"},
                            {"data": "ISDDESC"},
                            {"data": "ISDSAPONH"},
                            {"data": "ISDWMSONH"},
                            {"data": "ISDRQQTY"}
                        ],
                        "initComplete": function (settings, json) {
                            $('#reMainModal').modal('show');
                            $('.checkAllDataRM').prop('checked', false);
                            $('[data-toggle="tooltip"]').tooltip();
                            $('#loadIcon').addClass('hidden');
                        },
                        "createdRow": function (row, data, dataIndex) {
                            if (data.FAM > 0) { //Family
                                if (parseInt(data.ISDSINO) === 0) {// Mom
                                    $(row).css("background-color", "rgb(255,163,138)");
                                } else {// Child
                                    $(row).css("background-color", "rgb(245,223,218)");
                                }
                            }
                        },
                        "ordering": false,
                        "columnDefs": [
                            {"targets": [0, 1, 2, 3, 4, 6], "className": "text-center"},
                            {"targets": [5, 7], "className": "text-left"},
                            {"targets": [8, 9, 10], "className": "text-right"}
                        ]
                    });

                    //                    $.ajax({
                    //                        url: "/TABLE2/ShowDataTablesISM100",
                    //                        data: {mode: "getRemainData", pdg: pdG},
                    //                        async: false
                    //                    }).done(function (result) {
                    //
                    //                        if (result.length > 0) {
                    //                            for (var i = 0; i < result.length; i++) {
                    //                                tableRM.row.add([
                    //                                    '<input type="checkbox" class="checkDataRM" value="' + result[i].ISDORD + "_" + result[i].ISDSEQ + "_" + result[i].ISDLINO + "_" + result[i].ISDSINO + "_" + result[i].FAM + '">',
                    //                                    result[i].ISDORD,
                    //                                    result[i].ISDSEQ,
                    //                                    parseInt(result[i].ISDLINO),
                    //                                    result[i].ISDSINO,
                    //                                    result[i].ISDITNO,
                    //                                    result[i].ISDVT,
                    //                                    result[i].ISDDESC,
                    //                                    result[i].ISDSAPONH,
                    //                                    result[i].ISDWMSONH,
                    //                                    result[i].ISDRQQTY
                    //                                ]).draw(false);
                    //                            }
                    //
                    //                            $('#reMainModal').modal('show');
                    //
                    //                        } else {
                    //                            $('#NotFoundDataRemain').modal('show');
                    //                        }
                    //
                    //                        $('.checkAllDataRM').prop('checked', false);
                    //
                    //                        $('[data-toggle="tooltip"]').tooltip();
                    //
                    //                        $('#loadIcon').addClass('hidden');
                    //
                    //                    }).fail(function (jqXHR, textStatus, errorThrown) {
                    //                        // needs to implement if it fails
                    //                    });

                });

                $('#mngAppBtn').click(function () {

                    var checkData = $('.checkData:checked');
                    var uid = $('#uid').val();

                    if (checkData.length > 0) {

                        for (var i = 0; i < checkData.length; i++) {

                            var checkhtml = $(checkData[i]).closest("tr").find("td:eq(11)").html();

                            if (checkhtml.toString().includes("1 = Requested")) {

                                var orderNo = $(checkData[i]).val().split("_")[0];
                                var seq = $(checkData[i]).val().split("_")[1];

                                $.ajax({
                                    url: "/TABLE2/ShowDataTablesISM100?mode=updateMngApp"
                                            + "&orderNo=" + orderNo
                                            + "&seq=" + seq
                                            + "&uid=" + uid

                                }).done(function (result) {
                                    console.log(result);

                                }).fail(function (jqXHR, textStatus, errorThrown) {
                                    // needs to implement if it fails
                                });
                            }
                        }

                        setTimeout(getHead, 1000);
                        alertify.success('Update Success!');
                    } else {
                        alertify.alert("No Data Selected!",
                                function () {
                                    alertify.error('Please select data!');
                                }
                        );
                    }

                });

                $('#backWardBtn').click(function () {

                    var checkData = $('.checkData:checked');

                    var uid = $('#uid').val();

                    if (checkData.length > 0) {

                        var HasSTSNot1 = false;

                        for (var i = 0; i < checkData.length; i++) {
                            var checkhtml = $(checkData[i]).closest("tr").find("td:eq(11)").html();

                            if (!checkhtml.toString().includes("1 = Requested")) { // STS != 1
                                HasSTSNot1 = true;
                            }
                        }

                        if (HasSTSNot1) {
                            alertify.alert("Please Checked Only Order Has Status = 1");
                        } else {
                            for (var i = 0; i < checkData.length; i++) {
                                var checkhtml = $(checkData[i]).closest("tr").find("td:eq(11)").html();

                                if (checkhtml.toString().includes("1 = Requested")) { // only STS = 1 Can Backward
                                    var orderNo = $(checkData[i]).val().split("_")[0];
                                    var seq = $(checkData[i]).val().split("_")[1];

                                    $.ajax({
                                        url: "/TABLE2/ShowDataTablesISM100?mode=backward"
                                                + "&orderNo=" + orderNo
                                                + "&seq=" + seq
                                                + "&uid=" + uid

                                    }).done(function (result) {
                                        console.log(result);

                                    }).fail(function (jqXHR, textStatus, errorThrown) {
                                        // needs to implement if it fails
                                    });
                                }
                            }

                            setTimeout(getHead, 1000);
                            alertify.success('Backword Success!');
                        }

                    } else {
                        alertify.alert("No Data Selected!",
                                function () {
                                    alertify.error('Please select data!');
                                }
                        );
                    }

                });

                //                $('#updateRemarkCancelBtn').click(function () {
                //
                //                    var checkData = $('.checkData:checked');
                //                    var remark = $('#remark').val();
                //
                //                    if (checkData.length > 0) {
                //
                //                        for (var i = 0; i < checkData.length; i++) {
                //                            var orderNo = $(checkData[i]).val().split("_")[0];
                //                            var seq = $(checkData[i]).val().split("_")[1];
                //                            $.ajax({
                //                                url: "/TABLE2/ShowDataTablesISM100?mode=updateRemarkCancel"
                //                                        + "&orderNo=" + orderNo
                //                                        + "&seq=" + seq
                //                                        + "&remark=" + remark
                //
                //                            }).done(function (result) {
                //                                console.log(result);
                //
                //                            }).fail(function (jqXHR, textStatus, errorThrown) {
                //                                // needs to implement if it fails
                //                            });
                //                        }
                //
                //                        setTimeout(getHead, 1000);
                //                        alertify.success('Update Success!');
                //                    } else {
                //                        alertify.alert("No Data Selected!",
                //                                function () {
                //                                    alertify.error('Please select data!');
                //                                }
                //                        );
                //                    }
                //
                //                });

                $('#mngCancelBtn').click(function () {

                    var checkData = $('.checkData:checked');

                    if (checkData.length > 0) {
                        $('#mngCancelModal').modal('show');
                    } else {
                        alertify.alert("No Data Selected!",
                                function () {
                                    alertify.error('Please select data!');
                                }
                        );
                    }
                });

                $('#updateMngRemarkCancelBtn').click(function () {

                    var checkData = $('.checkData:checked');
                    var remark = $('#mngRemark').val();
                    var uid = $('#uid').val();

                    if (checkData.length > 0) {

                        for (var i = 0; i < checkData.length; i++) {
                            var checkhtml = $(checkData[i]).closest("tr").find("td:eq(11)").html();

                            if (checkhtml.toString().includes("2 = BS Approved") || checkhtml.toString().includes("1 = Requested")) {
                                var orderNo = $(checkData[i]).val().split("_")[0];
                                var seq = $(checkData[i]).val().split("_")[1];
                                $.ajax({
                                    url: "/TABLE2/ShowDataTablesISM100?mode=updateRemarkCancel"
                                            + "&orderNo=" + orderNo
                                            + "&seq=" + seq
                                            + "&remark=" + remark
                                            + "&uid=" + uid

                                }).done(function (result) {
                                    console.log(result);

                                }).fail(function (jqXHR, textStatus, errorThrown) {
                                    // needs to implement if it fails
                                });
                            }
                        }

                        setTimeout(getHead, 1000);
                        alertify.success('Update Success!');
                    } else {
                        alertify.alert("No Data Selected!",
                                function () {
                                    alertify.error('Please select data!');
                                }
                        );
                    }
                });

                $('#wh').on('change', function () {
                    var wh = $('#wh').val();
                    getDest(wh);
                });

                $('#productGroup').on('change', function () {
                    getHead();
                });

                $('#enRemainBtn').click(function () {

                    //checkDataRM
                    var checkDataRM = $('.checkDataRM:checked');
                    var uid = $('#uid').val();

                    if (checkDataRM.length > 0) {

                        var ArrOut = [];

                        for (var i = 0; i < checkDataRM.length; i++) {
                            var ArrIn = {};

                            let orderno = $(checkDataRM[i]).val().split("_")[0];
                            let seq = $(checkDataRM[i]).val().split("_")[1];
                            let line = $(checkDataRM[i]).val().split("_")[2];
                            let sino = $(checkDataRM[i]).val().split("_")[3];
                            let child = $(checkDataRM[i]).val().split("_")[4];

                            ArrIn ["order"] = orderno;
                            ArrIn ["seq"] = seq;
                            ArrIn ["line"] = line;
                            ArrIn ["sino"] = sino;
                            ArrIn ["child"] = child;
                            ArrOut.push(ArrIn);
                        }

                        console.log(ArrOut);

                        //                      insRemainRow
                        $.ajax({
                            url: "/TABLE2/ShowDataTablesISM100",
                            data: {mode: "insRemainLine", data: JSON.stringify(ArrOut), uid: uid}
                        }).done(function (result) {

                            if (result.map.insRMDetRes) {
                                if (result.map.updRMRes) {
                                    console.log("insRMHeadRes " + result.map.insRMHeadRes);
                                    //                                    alertify.success("Remain Order Success.");
                                    location.reload();
                                }
                            }

                        }).fail(function (jqXHR, textStatus, errorThrown) {
                            // needs to implement if it fails
                        });

                    }

                });

            });

            function getLevel(uid) {
                $.ajax({
                    url: "/TABLE2/ShowDataTablesISM100?mode=getLevel"
                            + "&uid=" + uid

                }).done(function (result) {
                    //                    console.log("resl " + result);
                    $('#level').val(result);

                    if (parseInt(result) >= 7) {
                        $('.mngShow').removeClass('hidden');
                    } else {
                        $('.bsnShow').removeClass('hidden');
                    }

                }).fail(function (jqXHR, textStatus, errorThrown) {
                    // needs to implement if it fails
                });
            }

            function getEditUser(orderNo) {
                $.ajax({
                    url: "/TABLE2/ShowDataTablesISM100?mode=getEditUser"
                            + "&orderNo=" + orderNo

                }).done(function (result) {
                    console.log(result);
                    $('#editUser').val(result);

                }).fail(function (jqXHR, textStatus, errorThrown) {
                    // needs to implement if it fails
                });
            }

            function getDest(wh) {

                $.ajax({
                    url: "/TABLE2/ShowDataTablesISM100?mode=getDest"
                            + "&wh=" + wh
                }).done(function (result) {
                    console.log(result);

                    $('#dest').html('');
                    var destText = '';

                    for (var i = 0; i < result.length; i++) {
                        destText += '<option value="' + result[i].uid + '">' + result[i].name + '</option>';
                    }

                    $('#dest').html(destText);

                }).fail(function (jqXHR, textStatus, errorThrown) {
                    // needs to implement if it fails
                });
            }

            function getHead() {
                var table = $('#showTable').DataTable();
                table.clear().draw();
                $('#loadIcon').removeClass('hidden');

                $('.checkAllData').prop("checked", false);

                var uid = $('#uid').val();
                var pdg = $('#productGroup').val();

                $.ajax({
                    url: "/TABLE2/ShowDataTablesISM100?mode=getHead"
                            + "&uid=" + uid
                            + "&pdg=" + pdg
                }).done(function (result) {

                    for (var i = 0; i < result.length; i++) {
                        table.row.add([
                            '<input type="checkbox" class="checkData" value="' + result[i].ISHORD + "_" + result[i].ISHSEQ + '">',
                            result[i].ISHTRDT,
                            '<a style="color: inherit; font-weight: normal !important;" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Tracking" href="/TABLE2/ISM910/display?find=ord&from=ism100&orderNo=' + result[i].ISHORD + '" >' + result[i].ISHORD + '</a>',
                            String.fromCharCode(parseInt(result[i].ISHSEQ) + 64),
                            result[i].ISHCUNO,
                            result[i].ISHCUNM1,
                            result[i].ISHSUBMI,
                            result[i].ISHMATCODE,
                            result[i].ISHLOT,
                            result[i].ISHAMTFG,
                            result[i].ISHNOR,
                            "<b hidden>" + result[i].LSTS + "</b>" + '<i class="fa fa-circle' + result[i].LSTSI + ' sts-' + result[i].LSTS + '" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="' + result[i].ISHLSTS + '"></i>',
                            "<b hidden>" + result[i].HSTS + "</b>" + '<i class="fa fa-circle' + result[i].HSTSI + ' sts-' + result[i].HSTS + '" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="' + result[i].ISHHSTS + '"></i>',
                            result[i].ISHRQDT,
                            result[i].ISHDEST + '<input type="hidden" id="dest-' + result[i].ISHORD + "_" + result[i].ISHSEQ + '" value="' + result[i].ISHDEST + '">',
                            result[i].ISHAPDT + '<input type="hidden" id="wh-' + result[i].ISHORD + "_" + result[i].ISHSEQ + '" value="' + result[i].ISHWHNO + '">',
                            result[i].ISHAPUSR,
                            result[i].ISHUSER + '<input type="hidden" id="editUser-' + result[i].ISHORD + "_" + result[i].ISHSEQ + '" value="' + result[i].ISHEUSR + '">',
                            '<a href="edit?orderNo=' + result[i].ISHORD + "&seq=" + result[i].ISHSEQ + '"><i class="fa fa-2x fa-pencil-square-o editBtn" aria-hidden="true"></i></a>'
                        ]).draw(false);

                    }

                    $('[data-toggle="tooltip"]').tooltip();

                    $('#loadIcon').addClass('hidden');

                }).fail(function (jqXHR, textStatus, errorThrown) {
                    // needs to implement if it fails
                });

            }

            function getWH() {

                $.ajax({
                    url: "/TABLE2/ShowDataTablesISM100?mode=getWH"
                            + "uid=" + sessionStorage.getItem('uid')
                }).done(function (result) {
                    console.log(result);

                    $('#wh').html('');
                    var whText = '';

                    for (var i = 0; i < result.length; i++) {
                        whText += '<option value="' + result[i].uid + '" ' + result[i].warehouse + '>' + result[i].uid + ' : ' + result[i].name + '</option>';
                    }

                    $('#wh').html(whText);

                }).fail(function (jqXHR, textStatus, errorThrown) {
                    // needs to implement if it fails
                });
            }

            function getProductGroup() {
                $.ajax({
                    url: "/TABLE2/ShowDataTablesISM100?mode=getProductGroup"
                            + "&uid=" + sessionStorage.getItem('uid')
                }).done(function (result) {
                    console.log(result);

                    $('#productGroup').html('');
                    var whText = '';

                    for (var i = 0; i < result.length; i++) {
                        whText += '<option value="' + result[i].uid + '">' + result[i].uid + ' : ' + result[i].name + '</option>';
                    }

                    $('#productGroup').html(whText);

                    getHead();

                }).fail(function (jqXHR, textStatus, errorThrown) {
                    // needs to implement if it fails
                });
            }
        </script>

    </head>

    <body>
        <div id="wrapper">

            <!-- /#sidebar-wrapper -->
            <%@ include file="../fragments/sidebar.jsp" %>

            <!-- Page Content -->
            <!-- nav-head-custom -->
            <%@ include file="../fragments/nav_head.jsp" %>

            <div class="container-fluid">
                <!--<div id="wrapper-top">-->
                <%-- PART 2 --%>
                <!--                    <div class="row">
<div class="col-lg-12">
    <div id="set-height" style="height:415px;margin-top:0px;">
        <div id="sidebar-wrapper-top" class="">
            <b class="page-header" style="padding-left:5px;font-size:18px;">
                <%-- <hr style="margin:0px;margin-top:10px;margin-bottom:10px;"> --%>
            </b>
        </div>
    </div>
    </div>
    </div> -->
                <%-- PART 3 --%>
                <!--show Qdest table-->
                <input type="hidden" id="uid">
                <input type="hidden" id="level">
                <input type="hidden" id="editUser">
                <form action="../resources/manual/ISM100.pdf" target="_blank">
                    <table width="100%">
                        <tr>
                            <td width="94%" align="left">
                            </td>
                            <td width="6%" align="right"><input type="submit" value="Manual" /></td>
                        </tr>
                    </table>
                </form>
                <div class="displayContain">
                    <div class="colop">
                        <h4>Product Group : </h4>&nbsp;&nbsp;&nbsp;&nbsp;
                        <select id="productGroup" style="width: 300px;"></select>&nbsp;&nbsp;&nbsp;&nbsp;
                    </div>
                    <div class="colop bsnShow hidden">
                        <button type="button" id="reqDateBtn" class="btn btn-info" title="status 0 [Change Date]" style="width: 150px;">Requested Date</button>&nbsp;&nbsp;&nbsp;&nbsp;
                        <button type="button" id="destBtn" class="btn btn-warning" title="status 0 [Change Destination]" style="width: 150px;">Destination</button>&nbsp;&nbsp;&nbsp;&nbsp;
                        <button type="button" id="reqAppBtn" class="btn btn-success" title="[status 0 => 1]" style="width: 150px;">Request Approval</button>&nbsp;&nbsp;&nbsp;&nbsp;
                        <button type="button" id="reMainBtn" class="form-control" title="Open Popup." >Create RemainOrder</button>&nbsp;&nbsp;&nbsp;&nbsp;
                        <button type="button" id="cancelBtn" class="btn btn-danger hidden" style="width: 150px;" data-toggle="modal" data-target="#cancelModal">Cancel</button>
                    </div>
                    <div class="colop mngShow hidden">
                        <button type="button" id="mngAppBtn" class="btn btn-success" title="[status 1 => 2]"  style="width: 150px;">Approval</button>&nbsp;&nbsp;&nbsp;&nbsp;
                        <button type="button" id="mngCancelBtn" class="btn btn-danger" title="[status 1 or 2 => 9]" style="width: 150px;">Cancel</button>&nbsp;&nbsp;&nbsp;&nbsp;
                        <button type="button" id="backWardBtn" title="[status 1 => 0]"  style="background-color : #F071F4; color: white; width: 150px;" class="btn">Backward</button>
                    </div>

                    <table id="showTable" class="display displayTable">
                        <thead>
                            <tr>
                                <th>
                                    <input type="checkbox" class="checkAllData" >
                                </th>
                                <th>Transaction Date</th>
                                <th>Sale Order</th>
                                <th>Seq</th>
                                <th>Customer Code</th>
                                <th>Customer Name</th>
                                <th>Doc No</th>
                                <th>Style FG</th>
                                <th>Lot</th>
                                <th>Quantity</th>
                                <th>No.</th>
                                <th>LSTS</th>
                                <th>HSTS</th>
                                <th>Requested Date</th>
                                <th>Destination</th>
                                <th>Approved Date</th>
                                <th>Approval</th>
                                <th>User</th>
                                <th>Option</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                            </tr>
                        </tfoot>
                        <tbody>
                        </tbody>
                    </table>

                    <center>
                        <div id="loadIcon" class="hidden">
                            <h3>
                                Loading...
                                <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
                            </h3>
                        </div>
                    </center>

                    <!-- Modal -->
                    <div class="modal fade" id="mngCancelModal" role="dialog">
                        <div class="modal-dialog">
                            <center>
                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">กำหนดหมายเหตุ</h4>
                                    </div>
                                    <div class="modal-body">
                                        <h4>Remark : <input type="text" id="mngRemark"></h4>
                                    </div>
                                    <div class="modal-footer">
                                        <center>
                                            <button type="button" id="updateMngRemarkCancelBtn" class="btn btn-info" data-dismiss="modal">Enter</button>
                                        </center>
                                    </div>
                                </div>
                            </center>
                        </div>
                    </div>

                    <!-- Modal -->
                    <div class="modal fade" id="cancelModal" role="dialog">
                        <div class="modal-dialog">
                            <center>
                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">กำหนดหมายเหตุ</h4>
                                    </div>
                                    <div class="modal-body">
                                        <h4>Remark : <input type="text" id="remark"></h4>
                                    </div>
                                    <div class="modal-footer">
                                        <center>
                                            <button type="button" id="updateRemarkCancelBtn" class="btn btn-info" data-dismiss="modal">Enter</button>
                                        </center>
                                    </div>
                                </div>
                            </center>
                        </div>
                    </div>

                    <!-- Modal -->
                    <div class="modal fade" id="reqDateModal" role="dialog">
                        <div class="modal-dialog">
                            <center>
                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">กำหนดวันที่ต้องการวัตถุดิบ</h4>
                                    </div>
                                    <div class="modal-body">
                                        <h4>Requested Date : <input type="date" id="reqDate"></h4>
                                    </div>
                                    <div class="modal-footer">
                                        <center>
                                            <button type="button" id="updateReqDateBtn" class="btn btn-info" data-dismiss="modal">Enter</button>
                                        </center>
                                    </div>
                                </div>
                            </center>
                        </div>
                    </div>

                    <!-- Modal -->
                    <div class="modal fade" id="destModal" role="dialog">
                        <div class="modal-dialog">
                            <center>
                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">กำหนดสถานที่ส่งปลายทาง</h4>
                                    </div>
                                    <div class="modal-body">
                                        <h4 hidden>
                                            Warehouse :
                                            <select id="wh" style="width: 300px;"></select>
                                        </h4>
                                        <h4>
                                            Destination :
                                            <select id="dest" style="width: 300px;"></select>
                                        </h4>
                                    </div>
                                    <div class="modal-footer">
                                        <center>
                                            <button type="button" id="updateDestBtn" class="btn btn-warning" data-dismiss="modal">Enter</button>
                                        </center>
                                    </div>
                                </div>
                            </center>
                        </div>
                    </div>

                    <!-- Modal -->
                    <div class="modal fade" id="NotFoundDataRemain" role="dialog">
                        <div class="modal-dialog">
                            <center>
                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title"></h4>
                                        <input type="hidden" id="deleteChildLino">
                                    </div>
                                    <div class="modal-body">
                                        <h4 class="modal-title" >Not Found Data For Remain Order.</h4>
                                    </div>
                                    <div class="modal-footer">
                                        <center>
                                            <button type="button" style="width: 100px;" id="" onclick="" class="btn btn-success" data-dismiss="modal">OK</button>
                                        </center>
                                    </div>
                                </div>
                            </center>
                        </div>
                    </div>

                    <!-- Modal -->
                    <div class="modal fade" id="plsinputRSUSR" role="dialog">
                        <div class="modal-dialog">
                            <center>
                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title" style="color:red;">Error !!</h4>
                                    </div>
                                    <div class="modal-body">
                                        <h4 class="modal-title" style="color:black;" >โปรดระบุชื่อและเบอร์โทร ก่อนส่งให้ หัวหน้าอนุมัติ.</h4>
                                        <!--<h4 class="modal-title" style="color:black;" >โปรดระบุชื่อและเบอร์โทร. Order : <label id="RSUSR_SOrder"></label> Seq : <label id="RSUSR_Seq"></label></h4>-->
                                    </div>
                                    <div class="modal-footer">
                                        <center>
                                            <button type="button" style="width: 100px;" id="" onclick="" class="btn btn-success" data-dismiss="modal">OK</button>
                                        </center>
                                    </div>
                                </div>
                            </center>
                        </div>
                    </div>

                    <!-- Modal -->
                    <div class="modal fade-lg" id="reMainModal" role="dialog">
                        <div class="modal-dialog">
                            <center>
                                <!-- Modal content-->
                                <div class="modal-content" style="width:100%;">
                                    <div class="modal-header">
                                        <div class="row">
                                            <div class="col-sm-2" style="text-align:left;">
                                                <h4 class="modal-title">ISM110/S</h4>
                                            </div>
                                            <div class="col-sm-6" style="text-align:center;">
                                                <h4 class="modal-title">Summary Remain S/O. Display</h4>
                                            </div>
                                            <div class="col-sm-4" style="text-align:right;">
                                                <div class="col-sm-11">
                                                    <h4 class="modal-title">
                                                        <label style="font-size:15px;" id="time2"></label>
                                                    </h4>
                                                </div>
                                                <div class="col-sm-1">
                                                    <button type="button" style="font-size: 33px;" class="close" data-dismiss="modal">&times;</button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6" style="text-align:left;">
                                                <button type="button" id="enRemainBtn" style="width:11em; height: 3em;" class="btn btn-success" data-dismiss="modal">
                                                    <label style="font-size: 20px; cursor: pointer;">Select</label>
                                                </button>
                                            </div>
                                            <div class="col-sm-6" style="text-align:right;">
                                                <button type="button" id="backRemainBtn" style="width:11em; height: 3em;" class="btn btn-light" data-dismiss="modal">
                                                    <label style="font-size: 20px; cursor: pointer;">Back</label>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-body">
                                        <table id="remainTable" class="display displayTable" style="width:100%">
                                            <thead>
                                                <tr>
                                                    <th><input type="checkbox" class="checkAllDataRM" ></th>
                                                    <th>Sales Order</th>
                                                    <th>Seq</th>
                                                    <th>No.</th>
                                                    <th></th>
                                                    <th>Material</th>
                                                    <th>VT</th>
                                                    <th>Description</th>
                                                    <th>SAP ONH</th>
                                                    <th>WMS ONH</th>
                                                    <th>Req</th>
                                                </tr>
                                            </thead>

                                            <tbody></tbody>

                                        </table>
                                    </div>
                                    <div class="modal-footer">

                                    </div>
                                </div>
                            </center>
                        </div>
                    </div>
                    <!--**********************-->
                    <!--End Part 3-->
                    <!--</div>  end #wrapper-top-->
                </div>
                <br>
            </div> <!-- end .container-fluid -->
        </div> <!-- end #wrapper -->
    </body>

</html>