<%-- 
    Document   : editISM800in
    Created on : Jun 2, 2021, 8:28:45 AM
    Author     : 93176
--%>

<%@ include file="../fragments/taglibs.jsp" %>
<%@ include file="../fragments/imports.jsp" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.sql.*;" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>ISM</title>
        <!-- css :: vendors -->
        <jsp:include page = "../fragments/css.jsp" />
        <!-- additional custom :: my-style -->
        <link rel="stylesheet" href="../resources/assets/styles/myStyles.css">
        <link rel="stylesheet" href="../resources/font/THsarabun/thsarabumnew.css">

        <!-- js :: vendors -->
        <jsp:include page="../fragments/script.jsp" />
        <!-- additional custom :: my-script -->
        <script src="../resources/assets/scripts/myScripts.js" async></script>
        <script src="../resources/assets/scripts/toggleLoad.js" async></script>

        <script>

            var tableOptions = {
                ajax: {
                    url: "/TABLE2/ISM800GET",
                    data: {mode: "EditDetailDisplayByORD", ord: ${ord}, no:${no}, dt: "${dtshow}"},
                    dataSrc: 'EditDet'
                }, //[ISDDWQNO]
                "columns": [
                    {'mRender': function (data, type, full, row) {

//                            if (full.ISDSAPONH === "0.00" || full.ISDSAPONH === "0") {
//                                return '';
//                            } else {
                            if (full.ISDSTS === "0" || full.ISDSTS === "9") {
                                return '<input type="checkbox" class="checkData" name="CCBOX" value="' + ${no} + ":" + full.ISDORD + ":" + full.ISDLINO + ":" + full.ISDSEQ + '" >';
                            } else {
                                return '';
                            }
//                            }

                        }
                    },
                    {"data": "ISDLINO"},
                    {"data": "ISDITNO"},
                    {"data": "ISDVT"},
                    {"data": "ISDDESC"},
                    {"data": "ISDSAPONH"},
                    {"data": "ISDWMSONH"},
                    {"data": "ISDRQQTY"},
                    {"data": "ISDUNIT"},
//                    {"data": "ISDSTS"},
                    {'mRender': function (data, type, full, row) {
//                            if (full.ISDREMK === "ขายเพิ่ม") {
//                                return full.ISDSTS + " (" + full.ISDREMK + ")";
//                            } else {
                            return full.ISDSTS;
//                            }
                        }
                    },
                    {'mRender': function (data, type, full, row) {
                            return full.STYLE + " " + full.COLOR;
                        }
                    },
                    {'mRender': function (data, type, full, row) {
                            if (full.LOT !== "") {
                                return "#" + full.LOT;
                            } else {
                                return "";
                            }
                        }
                    },
                    {"data": "ISDPURG"},
                    {"data": "ISDSUSER"},
                    {'mRender': function (data, type, full, row) {
//                            return '';
                            return '<i class="fa fa-2x fa-pencil-square-o editBtn" aria-hidden="true" style=" font-size: 30px; color: #001384;"></i>';
//                            return '<a href="" style="cursor: pointer;"><i class="fa fa-2x fa-pencil-square-o editBtn" aria-hidden="true" style=" font-size: 30px; color: #001384;"></i></a>';
                        }
                    }
                ],
                "paging": true,
                "lengthMenu": [[-1, 10, 25, 50, 100], ["All", 10, 25, 50, 100]],
                "bSortClasses": false,
                "ordering": true,
                "info": true,
                "fixedColumns": true,
                "autoWidth": false,
                "order": [[1, "asc"]],
                "columnDefs": [
                    {"targets": [1, 2, 3, 10, 11, 14], "className": "text-left"},
                    {"targets": [0, 9], "className": "text-center"},
                    {"targets": [5, 6, 7], "className": "text-right"},
                    {"targets": [0], "orderable": false}
                ]
            };

            $(document).ready(function () {

                $('#showTable').DataTable(tableOptions);

                // Setup - add a text input to each footer cell
                $('#showTable tfoot th').not(":eq(0),:eq(14)").each(function () {
                    var title = $(this).text();
                    $(this).html('<input type="text" />');
                });

                // DataTable
                var table = $('#showTable').DataTable();

                // Apply the search
                table.columns().every(function () {
                    var that = this;

                    $('input', this.footer()).on('keyup change', function () {
                        if (that.search() !== this.value) {
                            that.search(this.value).draw();
                        }
//                        table.column(i).search(this.value).draw();
                    });
                });

            });
        </script>

        <style>

            .one {
                position: sticky;
                top: 0px;
                background-color:  white;
                z-index:98;
            }
            .two {
                position: sticky;
                top: 50px;
                background-color: white;
                z-index:98;
            }
            .three {
                position: sticky;
                top: 126px;
                background-color: white;
                z-index:98;
            }

            input[type=text], select {
                width: 100%;
                padding: 5px 5px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }

            input[type=date] {
                /*width: 100%;*/
                padding: 5px 5px;
                margin: 8px 0;
                height: 48px;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }

            button[name=ok] {
                width: 25%;
                background-color: #008cff;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            button[name=ok]:hover {
                background-color: #008cff;
            }

            .displayTable {
                /*width: auto;*/
                border: 1px solid #ccc;
                border-radius: 4px;
            }

            .displayContain {
                border: 2px solid #ccc;
                border-radius: 5px;
                padding: 20px;
            }

            .checkData ,.checkAllData{
                width: 20px;
                height: 20px;
            }

            button[name=ok],[name=Cancel] {
                width: 25%;
                background-color: #008cff;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            button[name=ok],[name=Cancel]:hover {
                background-color: #008cff;
            }

            table.dataTable thead th.sorting,
            table.dataTable thead th.sorting_asc,
            table.dataTable thead th.sorting_desc {
                background: none;
                padding: 4px 5px;
            }

        </style>
        <script>
            $(document).ready(function () {

            ${ALRT}

                $('.checkAllData').click(function () {
                    var checkAll = $(this);
                    $('.checkData').prop("checked", checkAll.prop("checked"));
                });

                $('#conBTN').click(function () {

//                    $(".checkData").prop("checked", true);

                    var checkData = $('.checkData:checked');

//                    var dynaSTS = "Nothing";

                    var stsCK = "Pass";

                    if (checkData.length > 0) {
                        for (var i = 0; i < checkData.length; i++) {

                            var checkhtml = $(checkData[i]).closest("tr").find("td:eq(9)").html();

                            if (checkhtml !== "0" && checkhtml !== "0 (ขายเพิ่ม)") {
                                stsCK = "Failed";
                            }

                            console.log(stsCK);

                            if (checkhtml !== "0" && checkhtml !== "0 (ขายเพิ่ม)") {
//                                dynaSTS = "NotAllow";
                                alertify.error('Please Check Order Ststus Not Equal 0!');
                                break;
                            } else if (checkhtml === "0 (ขายเพิ่ม)") {
//                                dynaSTS = "SaleMore";
//                                modal show
                                $('#InsertSaleMore').modal("show");
//                                confirm and insert
                            } else if (checkhtml === "0") {
//                                alertify.success('Success');
//                                dynaSTS = "Nothing";
                                $('#InsertSaleMore').modal("show");
                            }

                        }
                    } else {
                        alertify.error('Please Select Order!');
                    }

//                    if(dynaSTS === "NotAllow"){
//                        
//                    }

//                    if (stsCK == "Failed") {
//                        alertify.error('Please Check Order Ststus Not Equal 0!');
//                        $('#myModal').modal("hide");
//                        $(".checkData").prop("checked", false);
//                    } else {
//                        $.ajax({
//                            url: "/TABLE2/ISM800GET",
//                            data: {mode: "diffDetailByORD", ord: ${ord}, no: ${no}}
//                        }).done(function (result) {
//                            console.log(result.Diff);
//
//                            if (result.Diff != 0) {
//                                $('#myModal').modal("show");
//                            } else {
//                                console.log("Nothing");
//                            }
//
//                        }).fail(function (jqXHR, textStatus, errorThrown) {
//                            // needs to implement if it fails
//                        });
//                    }

                });

                $('#delBTN').click(function () {

                    var checkData = $('.checkData:checked');

                    if (checkData.length > 0) {
                        for (var i = 0; i < checkData.length; i++) {

                            var nb = $(checkData[i]).val().split(":")[0];
                            var order = $(checkData[i]).val().split(":")[1];
                            var line = $(checkData[i]).val().split(":")[2];
                            var seq = $(checkData[i]).val().split(":")[3];
//
                            $.ajax({
                                url: "/TABLE2/ISM800GET",
                                data: {mode: "DelDownloadLineIN", no: nb, order: order, line: line, seq: seq}
                            }).done(function (result) {
                                console.log(result);

                                location.reload();

                            }).fail(function (jqXHR, textStatus, errorThrown) {
                                // needs to implement if it fails
                            });

                        }

                    } else {
                        alertify.alert("No Data Selected!",
                                function () {
                                    alertify.error('Please select data!');
                                }
                        );
                    }

                });

            });

            function salesmore() {
                $(".checkData").prop("checked", true);

                var checkData = $('.checkData:checked');

                var stsCK = "Pass";

                if (checkData.length > 0) {
                    for (var i = 0; i < checkData.length; i++) {
                        var checkhtml = $(checkData[i]).closest("tr").find("td:eq(9)").html();

                        if (checkhtml !== "0" && checkhtml !== "0 (ขายเพิ่ม)") {
                            stsCK = "Failed";
                        }
                    }
                }

                if (stsCK === "Failed") {
                    alertify.error('Please Check Order Ststus Not Equal 0!');
                    $('#myModal').modal("hide");
                    $(".checkData").prop("checked", false);
                } else {
                    $('#ffrm').submit();
                }

            }

            function nofunction() {
                $('#myModal').modal("hide");
                $(".checkData").prop("checked", false);
            }

            function Conf() {

                var checkData = $('.checkData:checked');

                var uid = $('#userid').val();

                if (checkData.length > 0) {

                    var myJsonString;
                    var listpo = [];
                    var pOrder = "";

                    for (var i = 0; i < checkData.length; i++) {

                        var checkhtml = $(checkData[i]).closest("tr").find("td:eq(9)").html();

                        if (checkhtml === "0 (ขายเพิ่ม)" || checkhtml === "0") {

                            var no = $(checkData[i]).val().split(":")[0];
                            var order = $(checkData[i]).val().split(":")[1];
                            var line = $(checkData[i]).val().split(":")[2];
                            var seq = $(checkData[i]).val().split(":")[3];

                            pOrder = order;

                            listpo.push(no + "|" + order + "|" + line + "|" + seq + "|" + checkhtml.replace(" (ขายเพิ่ม)", ""));

//                            $.ajax({
//                                url: "/TABLE2/ISM800GET",
//                                data: {mode: "InsertINByLine", no: no, order: order, line: line, uid: uid},
//                                async: false
//                            }).done(function (result) {
//                                console.log(result.DelVal);
//
////                                location.reload();
//                                $('#InsertSaleMore').modal("hide");
//
//                            }).fail(function (jqXHR, textStatus, errorThrown) {
//                                // needs to implement if it fails
//                            });


                        }

                    }

                    myJsonString = JSON.stringify(listpo);

//                    console.log(pOrder);
//                    console.log(myJsonString);

                    $.ajax({
                        url: "/TABLE2/ISM800GET",
                        data: {mode: "InsertINByLine", pOrder: pOrder, linedata: myJsonString, uid: uid},
                        async: false
                    }).done(function (result) {

                        if (result.DelVal) {
                            location.reload();
                        }

                        alertify.success("To Master Table Success.");

//                        $('#InsertSaleMore').modal("hide");

                    }).fail(function (jqXHR, textStatus, errorThrown) {
                        // needs to implement if it fails
                    });

                }
            }

        </script>
        <style>
            body {
                font-family: Arial, Helvetica, sans-serif;
            }

            /* The Modal (background) */
            .modal {
                display: none; /* Hidden by default */
                position: fixed; /* Stay in place */
                z-index: 1; /* Sit on top */
                padding-top: 100px; /* Location of the box */
                left: 0;
                top: 0;
                width: 100%; /* Full width */
                height: 100%; /* Full height */
                overflow: auto; /* Enable scroll if needed */
                background-color: rgb(0,0,0); /* Fallback color */
                background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
            }

            /* Modal Content */
            .modal-content {
                background-color: #fefefe;
                margin: auto;
                padding: 20px;
                border: 1px solid #888;
                width: fit-content;
                height: 320px;
            }

            /* The Close Button */
            .close {
                color: #aaaaaa;
                float: right;
                font-size: 28px;
                font-weight: bold;
            }

            .close:hover,
            .close:focus {
                color: #000;
                text-decoration: none;
                cursor: pointer;
            }

            button[name=ok],[name=Cancel] {
                width: 25%;
                background-color: #008cff;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            button[name=ok],[name=Cancel]:hover {
                background-color: #008cff;
            }
        </style>

    </head>
    <body onload="document.getElementById('userid').value = sessionStorage.getItem('uid');">

        <div id="wrapper">

            <!-- /#sidebar-wrapper -->
            <%@ include file="../fragments/sidebar.jsp" %>

            <!-- Page Content -->
            <!-- nav-head-custom -->
            <%@ include file="../fragments/nav_head.jsp" %>

            <div class="container-fluid">

                <form action="editin" id="ffrm" method="POST" name="frm" onsubmit="">

                    <input type="hidden" id="userid" name="userid">
                    <input type="hidden" id="no" name="no" value="${no}">
                    <input type="hidden" id="cus" name="cus" value="${cus}">
                    <input type="hidden" id="dt" name="dt" value="${dtpara}">
                    <input type="hidden" id="slord" name="slord" value="${slord}">

                    <br>

                    <div style="width: 100%; height: 50px; background-color: #e0e0e0;" class="one">
                        <!--<div class="sticky" style="width: 100%; height: 50px; background-color: #e0e0e0;">-->
                        <br>
                        <h4><b>EDIT MODE</b></h4>
                    </div>

                    <div class="displayContain">

                        <div class="two">
                            <!--<div class="sticky2">-->
                            <table>
                                <tr>
                                    <th style="width: 400px;" colspan="2">Customer no. : &nbsp; ${cus} &nbsp; : &nbsp; ${cusnam}</th>
                                    <th style="width: 100px;"></th>
                                    <th style="width: 300px;">Sales Order Date : &nbsp;&nbsp; ${dtshow}</th>
                                    <th style="width: 100px;"></th>
                                    <th style="width: 200px;">Business &nbsp;:&nbsp; ${grp}</th>
                                    <th style="width: 400px;" colspan="2">${rson}</th>

                                </tr>
                                <tr style="height:20px;">
                                    <th></th>
                                </tr>
                                <tr>
                                    <th style="width: 400px;" colspan="2">Sales Order no. : &nbsp;&nbsp; ${ord}</th>
                                    <th style="width: 100px;"></th>
                                    <th style="width: 300px;">Delivery Date. : &nbsp;&nbsp; ${dldate}</th>
                                    <!--<th style="width: 100px;"> Sales Order no. : &nbsp;&nbsp; ${ord}</th>-->
                                    <!--<th style="width: 100px;"></th>-->
                                    <th style="width: 200px;"><input style="width: 150px;" id="conBTN" class="btn btn-success" value="Confirm" /></th>
                                    <th style="width: 200px;"><input style="width: 150px;" id="delBTN" type="button" class="btn btn-danger" value="Delete" /></th>
                                    <th style="width: 200px;"><input style="width: 150px;" type="button" class="btn btn-primary" value="Back" onclick="window.location.href = '/TABLE2/ISM800/edit?dt=${dtpara}&cus=${cus}&ord=${ord}&no=${no}'"/></th>
                                    <th style="width: 400px;"></th>
                                </tr>
                            </table>
                        </div>

                        <br>

                        <table id="showTable" class="display displayTable">
                            <thead class="three">
                                <tr>
                                    <th>
                                        <input type="checkbox" class="checkAllData" > ALL
                                    </th>
                                    <th style="width: 5%;">No.</th>
                                    <th>Material Code</th>
                                    <th>VT</th>
                                    <th>Description</th>
                                    <th>SAP ONH</th>
                                    <th>WMS ONH</th>
                                    <th>Requested Qty</th>
                                    <th>Unit</th>
                                    <th>Sts</th>
                                    <th>Style FG</th>
                                    <th>Lot</th>
                                    <th>Purchasing</th>
                                    <th>Create By</th>
                                    <th>Option</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th style="width:40px;"></th>
                                    <th style="width:87px;"></th>
                                    <th style="width:159px;"></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </tfoot>

                            <tbody>
                            </tbody>

                        </table>

                    </div>

                    <center>
                        <div id="myModal" class="modal fade" role="dialog">
                            <div class="modal-dialog modal-sm"> <!-- ???????????? modal ?????????????? ???? xs, sm, md, lg -->
                                <div class="modal-md col-md-6" style="top: 150px; left:300px; background-color: #ddd; border: 1px solid #ccc; border-radius: 4px;">
                                    <div class="modal-header">
                                        <h4 class="modal-title" align="center">Do you need to create S/O to ISMS ?</h4>
                                        <br>
                                        <button name="ok" type="button" onclick="salesmore()">
                                            <font color = "white">YES</font>
                                        </button>
                                        <button name="Cancel" type="button" onclick="nofunction()" data-dismiss="modal">
                                            <font color = "white">NO</font>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- devbanban.com -->
                    </center>

                    <center>
                        <div id="InsertSaleMore" class="modal fade" role="dialog" style="z-index:99;">
                            <div class="modal-dialog modal-sm"> <!-- ???????????? modal ?????????????? ???? xs, sm, md, lg -->
                                <div class="modal-md col-md-6" style="top: 150px; left:300px; background-color: #ddd; border: 1px solid #ccc; border-radius: 4px;">
                                    <div class="modal-header">
                                        <h4 class="modal-title" align="center">Do you need to create S/O to ISMS ?</h4>
                                        <br>
                                        <button name="ok" type="button" onclick="Conf()">
                                            <font color = "white">YES</font>
                                        </button>
                                        <button name="Cancel" type="button" onclick="nofunction()" data-dismiss="modal">
                                            <font color = "white">NO</font>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- devbanban.com -->
                    </center>

                </form>
                <!--End Part 3-->
                <!--</div>  end #wrapper-top -->
            </div> <!-- end .container-fluid -->

        </div> <!-- end #wrapper -->

    </body>
</html>
