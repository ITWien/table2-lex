<%-- 
    Document   : editISM800
    Created on : May 18, 2021, 9:04:48 AM
    Author     : 93176
--%>

<%@ include file="../fragments/taglibs.jsp" %>
<%@ include file="../fragments/imports.jsp" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.sql.*;" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>ISM</title>
        <!-- css :: vendors -->
        <jsp:include page = "../fragments/css.jsp" />
        <!-- additional custom :: my-style -->
        <link rel="stylesheet" href="../resources/assets/styles/myStyles.css">
        <link rel="stylesheet" href="../resources/font/THsarabun/thsarabumnew.css">

        <!-- js :: vendors -->
        <jsp:include page="../fragments/script.jsp" />
        <!-- additional custom :: my-script -->
        <script src="../resources/assets/scripts/myScripts.js" async></script>
        <script src="../resources/assets/scripts/toggleLoad.js" async></script>

        <script>

            var tableOptions = {
                ajax: {
                    url: "/TABLE2/ISM800GET",
                    data: {mode: "EditDetailDisplay", no: ${no}, dt: "${date}"},
                    dataSrc: 'EditDet'
                }, //[ISDDWQNO]
                "columns": [
                    {'mRender': function (data, type, full, row) {
//                            console.log(full.ISDLINO);
//                            return data;
                            return '<input type="checkbox" class="checkData" name="CCBOX" value="' + full.ISDDWQNO + ":" + full.ISDORD + '">';
                        }
                    },
                    {"data": "Date"},
                    {'mRender': function (data, type, full, row) {
                            if (full.ISDBAT != null) {
                                return full.ISDORD + "<label style='color:red; font-size:18px; margin: 0; padding: 0;'>*</label>";
                            } else {
                                return full.ISDORD;
                            }
                        }
                    },
                    {"data": "ISDITNO"},
                    {"data": "ISDLINO"},
                    {'mRender': function (data, type, full, row) {
                            return full.STYLE + " " + full.COLOR;
                        }
                    },
                    {'mRender': function (data, type, full, row) {
                            if (full.LOT !== "") {
                                return "#" + full.LOT;
                            } else {
                                return "";
                            }
                        }
                    },
                    {'mRender': function (data, type, full, row) {
                            return full.ISDREMK;
                        }
                    },
                    {'mRender': function (data, type, full, row) {
//                            var reformateDate = full.Date;
//                            reformateDate = reformateDate.toString().split("/")[2] + reformateDate.toString().split("/")[1] + reformateDate.toString().split("/")[0];
                            return '<a href="editin?slord=' + full.ISDORD + '&no=' + full.ISDDWQNO + '&dt=' + full.Date + ' " style="cursor: pointer;"><i class="fa fa-2x fa-pencil-square-o editBtn" aria-hidden="true" style=" font-size: 30px; color: #001384;"></i></a>';
                        }
                    }
//                    {"data": "ISDREMK"}
                ],
                "paging": true,
                "lengthMenu": [[-1, 10, 25, 50, 100], ["All", 10, 25, 50, 100]],
                "bSortClasses": false,
                "ordering": true,
                "info": true,
                "fixedColumns": true,
                "autoWidth": false,
                "order": [[1, "desc"], [2, "asc"]],
                "columnDefs": [
                    {"targets": [1, 2, 3, 4], "className": "text-left"},
                    {"targets": [0], "className": "text-center"},
                    {"targets": [0], "orderable": false}
                ]
            };

            $(document).ready(function () {

                $('#showTable').DataTable(tableOptions);

                // Setup - add a text input to each footer cell
                $('#showTable tfoot th').not(":eq(0)").each(function () {
                    var title = $(this).text();
                    $(this).html('<input type="text" />');
                });

                // DataTable
                var table = $('#showTable').DataTable();

                // Apply the search
                table.columns().every(function () {
                    var that = this;

                    $('input', this.footer()).on('keyup change', function () {
                        if (that.search() !== this.value) {
                            that.search(this.value).draw();
                        }
//                        table.column(i).search(this.value).draw();
                    });
                });

            });
        </script>
        <style>
            input[type=text], select {
                width: 100%;
                padding: 5px 5px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }

            input[type=date] {
                /*width: 100%;*/
                padding: 5px 5px;
                margin: 8px 0;
                height: 48px;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }

            button[name=ok] {
                width: 25%;
                background-color: #008cff;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            button[name=ok]:hover {
                background-color: #008cff;
            }

            .displayTable {
                /*width: auto;*/
                border: 1px solid #ccc;
                border-radius: 4px;
            }

            .displayContain {
                border: 2px solid #ccc;
                border-radius: 5px;
                padding: 20px;
            }

            .checkData ,.checkAllData{
                width: 20px;
                height: 20px;
            }

            button[name=ok],[name=Cancel] {
                width: 25%;
                background-color: #008cff;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            button[name=ok],[name=Cancel]:hover {
                background-color: #008cff;
            }

            table.dataTable thead th.sorting,
            table.dataTable thead th.sorting_asc,
            table.dataTable thead th.sorting_desc {
                background: none;
                padding: 4px 5px;
            }

        </style>
        <script>

            $(document).ready(function () {

            ${ALRT}

                $('.checkAllData').click(function () {
                    var checkAll = $(this);
                    $('.checkData').prop("checked", checkAll.prop("checked"));
                });

                $('#conBTN').click(function () {

                    var checkData = $('.checkData:checked');
                    var stsCK = "Pass";

                    if (checkData.length > 0) {
                        for (var i = 0; i < checkData.length; i++) {

                            var order = $(checkData[i]).closest("tr").find("td:eq(2)").html();

                            $.ajax({
                                url: "/TABLE2/ISM800GET",
                                data: {mode: "EditDetailDisplayByORDForEdit", ord: order, no:${no}, dt: "${date}"},
                                async: false
                            }).done(function (result) {
                                if (result.sts > 0) {
                                    stsCK = "Failed";
                                }
                            }).fail(function (jqXHR, textStatus, errorThrown) {
                                // needs to implement if it fails
                            });

                        }

                    } else {
                        stsCK = "Failed";
                        alertify.alert("No Data Selected!",
                                function () {
                                    alertify.error('Please select data!');
                                }
                        );
                    }

                    if (stsCK === "Failed") {
                        alertify.error('Please Check Order Ststus Not Equal 0!');
                    } else {

//                        var no = $(checkData[0]).val().split(":")[0];
//                        var order = $(checkData[0]).val().split(":")[1];
//
//                        $.ajax({
//                            url: "/TABLE2/ISM800GET",
//                            data: {mode: "diffDetailByORD", ord: order, no: no}
//                        }).done(function (result) {
//                            console.log(result.Diff);
//
//                            if (result.Diff !== 0) {
                        $('#myModal').modal("show");
//                            } else {
//                                console.log("Nothing");
//                            }
//
//                        }).fail(function (jqXHR, textStatus, errorThrown) {
//                            // needs to implement if it fails
//                        });

                    }

                });

                $('#redwnonhBTN').click(function () {

                    var checkData = $('.checkData:checked');
                    var listorder = [];
                    var uid = $('#userid').val();

                    var stsCK = "Pass";

                    if (checkData.length > 0) {

                        for (var i = 0; i < checkData.length; i++) {
//                            console.log(checkData[i].value);
                            listorder.push(checkData[i].value);
                        }

                    } else {
                        stsCK = "Failed";
                    }

                    var myJsonString = JSON.stringify(listorder);

                    if (stsCK === "Failed") {
                        alertify.alert("No Data Selected!",
                                function () {
                                    alertify.error('Please select data!');
                                }
                        );
                    } else {
                        $.ajax({
                            url: "/TABLE2/ISM800GET",
                            data: {mode: "redownloadSL", orderdata: myJsonString, uid: uid, date: "${date}"},
//                            async: false,
                            beforeSend: function () {
                                $("#loadModal").modal("show");
                            },
                            complete: function () {
                                $("#loadModal").modal("hide");
                            }
                        }).done(function (result) {

                            var passReload = false;
                            var passReloadSeq = false;

                            for (var i = 0; i < result.resJson.myArrayList.length; i++) {

                                if (result.resJson.myArrayList[i].map.resSTS9 === "9") {
                                    if (result.resJson.myArrayList[i].map.resInsDwnDSts9) {
                                        passReloadSeq = true;
                                    }
                                }
                                if (result.resJson.myArrayList[i].map.resSTS0 === "0") {
                                    if (result.resJson.myArrayList[i].map.resUpdDwnSts0) {
                                        passReload = true;
                                    }
                                }

                            }

                            if (passReloadSeq) { // sts = 9 => insert dwn data seq +1
                                alertify.success("Insert Order Seq + 1 Success.");
                            } else {
                                alertify.error("Insert Order Failed.");
                            }
                            if (passReload) { // sts = 0 => update dwn data
                                alertify.success("Update Order Sts = 0 Success.");
                            } else {
                                alertify.error("Update Order Failed.");
                            }

                            var table = $('#showTable').DataTable();
                            table.ajax.reload();

                        }).fail(function (jqXHR, textStatus, errorThrown) {
                            // needs to implement if it fails
                        });
                    }

                });

                $('#delBTN').click(function () {

                    var checkData = $('.checkData:checked');

                    if (checkData.length > 0) {
                        for (var i = 0; i < checkData.length; i++) {

                            var no = $(checkData[i]).val().split(":")[0];
                            var order = $(checkData[i]).val().split(":")[1];

                            $.ajax({
                                url: "/TABLE2/ISM800GET",
                                data: {mode: "DelDownloadIN", no: no, order: order}
                            }).done(function (result) {
                                console.log(result);

                                location.reload();

                            }).fail(function (jqXHR, textStatus, errorThrown) {
                                // needs to implement if it fails
                            });

                        }
                    } else {
                        alertify.alert("No Data Selected!",
                                function () {
                                    alertify.error('Please select data!');
                                }
                        );
                    }

                });

            });

            function salesmore() {
//                $(".checkData").prop("checked", true);

                var checkData = $('.checkData:checked');

                var stsCK = "Pass";

//                if (checkData.length > 0) {
//                    for (var i = 0; i < checkData.length; i++) {
//                        var checkhtml = $(checkData[i]).closest("tr").find("td:eq(4)").html();
//
//                        if (checkhtml !== "S" && checkhtml !== "C") {
//                            stsCK = "Failed";
//                        }
//
//                    }
//                }

                if (stsCK === "Failed") {
                    alertify.error('Please Check Order Ststus Not Equal 0!');
                    $('#myModal').modal("hide");
                    $(".checkData").prop("checked", false);
                } else {
                    $('#ffrm').submit();
                }

            }

            function nofunction() {
                console.log("no function ");
                $(".checkData").prop("checked", false);
            }

        </script>
    </head>
    <body onload="document.getElementById('userid').value = sessionStorage.getItem('uid');">

        <div id="wrapper">

            <!-- /#sidebar-wrapper -->
            <%@ include file="../fragments/sidebar.jsp" %>

            <!-- Page Content -->
            <!-- nav-head-custom -->
            <%@ include file="../fragments/nav_head.jsp" %>

            <div class="container-fluid">

                <form action="edit" id="ffrm" method="POST" name="frm">

                    <c:remove var="ALRT" scope="session" />

                    <input type="hidden" id="userid" name="userid">
                    <input type="hidden" id="no" name="no" value="${no}">
                    <input type="hidden" id="cus" name="cus" value="${cus}">
                    <input type="hidden" id="dt" name="dt" value="${dt}">
                    <input type="hidden" id="date" name="date" value="${date}">

                    <br>

                    <div style="width: 100%; height: 50px; background-color: #e0e0e0;">
                        <br>
                        <h4><b>EDIT MODE</b></h4>
                    </div>

                    <div class="displayContain">

                        <table>
                            <tr>
                                <th style="width: 400px;">Customer no. : &nbsp; ${cus} &nbsp; : &nbsp; ${cusname}</th>
                                <th style="width: 200px;"></th>
                                <th style="width: 200px;"><input type="button" style="width: 150px;" id="conBTN" value="Confirm" class="btn btn-success" ></th>
                                <th style="width: 200px;"><input type="button" style="width: 150px;" id="delBTN" value="Delete" class="btn btn-danger"></th>
                                <th style="width: 200px;"><input style="width: 150px;" value="Back" type="button" class="btn btn-primary" onclick="window.location.href = '/TABLE2/ISM800/display?uid=' + sessionStorage.getItem('uid');"></th>
                                <th style="width: 200px;"><input style="width: 150px;" value="RE-DWN ONH" type="button" id="redwnonhBTN" class="btn btn-info" onclick=""></th>
                                <th style="width: 200px;"></th>
                                <th style="width: 200px;"></th>
                            </tr>
                        </table>

                        <br>

                        <table id="showTable" class="display displayTable">
                            <thead>
                                <tr>
                                    <th>
                                        <input type="checkbox" class="checkAllData" > ALL
                                    </th>
                                    <th>Trans Date</th>
                                    <th>Sales Order No.</th>
                                    <th>Business</th>
                                    <th>Create By</th>
                                    <th>Style FG</th>
                                    <th>Lot</th>
                                    <th></th>
                                    <th>Option</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </tfoot>
                            <tbody>
                            </tbody>

                        </table>

                    </div>

                    <center>
                        <div id="myModal" class="modal fade" role="dialog">
                            <div class="modal-dialog modal-sm"> <!-- ???????????? modal ?????????????? ???? xs, sm, md, lg -->
                                <div class="modal-md col-md-6" style="top: 150px; left:300px; background-color: #ddd; border: 1px solid #ccc; border-radius: 4px;">
                                    <div class="modal-header">
                                        <h4 class="modal-title" align="center">Do you need to create S/O to ISMS ?</h4>
                                        <br>
                                        <button name="ok" type="button" onclick="salesmore()">
                                            <font color = "white">YES</font>
                                        </button>
                                        <button name="Cancel" type="button" onclick="nofunction()" data-dismiss="modal">
                                            <font color = "white">NO</font>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- devbanban.com -->
                    </center>

                    <center>
                        <div id="loadModal" class="modal fade" role="dialog">
                            <div class="modal-dialog modal-sm">
                                <div class="modal-md col-md-6" style="top: 150px; left:300px; background-color: #ddd; border: 1px solid #ccc; border-radius: 4px;">
                                    <div class="modal-header">
                                        <h4 class="modal-title" align="center">Loading ...</h4>

                                        <div id="loadme" class="loader"></div>
                                        <br>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- devbanban.com -->
                    </center>

                </form>
                <!--End Part 3-->
                <!--</div>  end #wrapper-top -->
            </div> <!-- end .container-fluid -->

        </div> <!-- end #wrapper -->


    </body>

</html>
