<%-- 
    Document   : displayISM910
    Created on : Sep 26, 2022, 9:09:02 AM
    Author     : 93176
--%>

<%@ include file="../fragments/taglibs.jsp" %>
<%@ include file="../fragments/imports.jsp" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.sql.*;" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>ISM</title>
        <!-- css :: vendors -->
        <jsp:include page = "../fragments/css.jsp" />
        <!-- additional custom :: my-style -->
        <link rel="stylesheet" href="../resources/assets/styles/myStyles.css">

        <!-- js :: vendors -->
        <jsp:include page = "../fragments/script.jsp" />
        <!-- additional custom :: my-script -->
        <script src="../resources/assets/scripts/myScripts.js" async></script>
        <script src="../resources/assets/scripts/toggleLoad.js" async></script>

        <style>
            input[type=submit] {
                width: 100%;
                background-color: #00399b;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=text]{
                width: 100%;
                padding: 5px 5px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }

            .sts-0{
                font-size: 20px;
                color: navy;
            }
            .sts-1{
                font-size: 20px;
                color: yellow;
            }
            .sts-2{
                font-size: 20px;
                color: green;
            }
            .sts-3{
                font-size: 20px;
                color: blue;
            }
            .sts-4{
                font-size: 20px;
                color: pink;
            }
            .sts-5{
                font-size: 20px;
                color: navy;
            }
            .sts-6{
                font-size: 20px;
                color: gray;
            }
            .sts-7{
                font-size: 20px;
                color: brown;
            }
            .sts-8{
                font-size: 20px;
                color: orange;
            }
            .sts-9{
                font-size: 20px;
                color: black;
            }

            .displayTable {
                /*width: auto;*/
                border: 1px solid #ccc;
                border-radius: 4px;
            }

            .one {
                position: sticky;
                top: 0px;
                background-color: white;
                z-index:99;
            }
            .two {
                position: sticky;
                top: 126px;
                background-color: white;
                z-index:99;
            }
            /*            .three {
                            position: sticky;
                            top: 126px;
                            background-color: white;
                            z-index:99;
                        }*/

        </style>

        <script>

            const format = (num, decimals) => num.toLocaleString('en-US', {
                    minimumFractionDigits: 2,
                    maximumFractionDigits: 2
                });

            function getDatas(param, sorter) {

                var tableOptions = {
                    ajax: {
                        url: "/TABLE2/ShowDataTablesISM910?mode=getDet&param=" + param + "&sorter=" + sorter,
                        dataSrc: ''
                    },
                    "paging": true,
                    "lengthMenu": [[-1, 10, 25, 50, 100], ["All", 10, 25, 50, 100]],
                    "bSortClasses": false,
                    "ordering": false,
                    "info": true,
                    "fixedColumns": true,
//                    "order": [[0, 'desc'], [1, 'asc']],
                    "columns": [
                        {'mRender': function (data, type, full) {
                                return full.ISDORD;
                            }
                        },
                        {'mRender': function (data, type, full) {
                                return full.ISDLINO;
                            }
                        },
                        {'mRender': function (data, type, full) {
                                return full.ISDITNO;
                            }
                        },
                        {'mRender': function (data, type, full) {
                                return full.ISDVT;
                            }
                        },
                        {'mRender': function (data, type, full) {
                                return full.ISDDESC;
                            }
                        },
                        {'mRender': function (data, type, full) { //RQ
                                return format(parseFloat(full.ISDRQQTY));
                            }
                        },
                        {'mRender': function (data, type, full) { //Pick
                                return format(parseFloat(full.ISDPKQTY));
                            }
                        },
                        {'mRender': function (data, type, full) { //Remain
                                let rq = parseFloat(full.ISDRQQTY);
                                let pk = parseFloat(full.ISDPKQTY);
                                return format(rq - pk);
                            }
                        },
                        {'mRender': function (data, type, full) {
                                return full.ISDORD;
                            }
                        },
                        {'mRender': function (data, type, full) {
                                return full.ISDJBNO;
                            }
                        },
                        {'mRender': function (data, type, full) {
                                let yy = full.ISDETM.toString().substring(0, 4);
                                let mm = full.ISDETM.toString().substring(4, 6);
                                let dd = full.ISDETM.toString().substring(6, 8);
                                return dd + "/" + mm + "/" + yy;
                            }
                        },
                        {'mRender': function (data, type, full) {
                                let text = String.fromCharCode(64 + parseInt(full.ISDSEQ)); //65 => A, 66 => B
                                return text;
                            }
                        },
                        {'mRender': function (data, type, full) { //STS New Order
                                if (full.ISDSTS === "0") {
                                    return "<b hidden>" + full.ISDSTS + "</b>" + '<i class="fa fa-circle' + full.STSI + '  sts-' + full.ISDSTS + '" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="' + full.ISDSTS + " = " + full.FAM + '"></i>';
                                } else {
                                    return "";
                                }
                            }
                        },
                        {'mRender': function (data, type, full) { //STS Requestd
                                if (full.ISDSTS === "1") {
                                    return "<b hidden>" + full.ISDSTS + "</b>" + '<i class="fa fa-circle' + full.STSI + ' sts-' + full.ISDSTS + '" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="' + full.ISDSTS + " = " + full.FAM + '"></i>';
                                } else {
                                    return "";
                                }
                            }
                        },
                        {'mRender': function (data, type, full) { //STS Approved
                                if (full.ISDSTS === "2") {
                                    return "<b hidden>" + full.ISDSTS + "</b>" + '<i class="fa fa-circle' + full.STSI + ' sts-' + full.ISDSTS + '" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="' + full.ISDSTS + " = " + full.FAM + '"></i>';
                                } else {
                                    return "";
                                }
                            }
                        },
                        {'mRender': function (data, type, full) { //STS Received
                                if (full.ISDSTS === "4") {
                                    return "<b hidden>" + full.ISDSTS + "</b>" + '<i class="fa fa-circle' + full.STSI + ' sts-' + full.ISDSTS + '" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="' + full.ISDSTS + " = " + full.FAM + '"></i>';
                                } else {
                                    return "";
                                }
                            }
                        },
                        {'mRender': function (data, type, full) { //STS Queued
                                if (full.ISDSTS === "5") {
                                    return "<b hidden>" + full.ISDSTS + "</b>" + '<i class="fa fa-circle' + full.STSI + ' sts-' + full.ISDSTS + '" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="' + full.ISDSTS + " = " + full.FAM + '"></i>';
                                } else {
                                    return "";
                                }
                            }
                        },
                        {'mRender': function (data, type, full) { //STS Issued
                                if (full.ISDSTS === "6") {
                                    return "<b hidden>" + full.ISDSTS + "</b>" + '<i class="fa fa-circle' + full.STSI + ' sts-' + full.ISDSTS + '" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="' + full.ISDSTS + " = " + full.FAM + '"></i>';
                                } else {
                                    return "";
                                }
                            }
                        },
                        {'mRender': function (data, type, full) { //STS Trans
                                if (full.ISDSTS === "7") {
                                    return "<b hidden>" + full.ISDSTS + "</b>" + '<i class="fa fa-circle' + full.STSI + ' sts-' + full.ISDSTS + '" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="' + full.ISDSTS + " = " + full.FAM + '"></i>';
                                } else {
                                    return "";
                                }
                            }
                        },
                        {'mRender': function (data, type, full) { //STS Received
                                if (full.ISDSTS === "8") {
                                    return "<b hidden>" + full.ISDSTS + "</b>" + '<i class="fa fa-circle' + full.STSI + ' sts-' + full.ISDSTS + '" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="' + full.ISDSTS + " = " + full.FAM + '"></i>';
                                } else {
                                    return "";
                                }
                            }
                        },
                        {'mRender': function (data, type, full) { //STS Cancelled
                                if (full.ISDSTS === "9") {
                                    return "<b hidden>" + full.ISDSTS + "</b>" + '<i class="fa fa-circle' + full.STSI + ' sts-' + full.ISDSTS + '" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="' + full.ISDSTS + " = " + full.FAM + ' By ' + full.ISDAPUSER + ' "></i>';
                                } else {
                                    return "";
                                }
                            }
                        }
                    ],
                    "columnDefs": [
                        {"targets": [0], "className": "text-left", "width": "1%"}, //sales order
                        {"targets": [1], "className": "text-right", "width": "1%"}, //line
                        {"targets": [2], "className": "text-left", "width": "2%"}, //matcode
                        {"targets": [3], "className": "text-left", "width": "1%"}, //vt
                        {"targets": [4], "className": "text-left", "width": "5%"}, //desc
                        {"targets": [5], "className": "text-right", "width": "1%"}, //rq_qty
                        {"targets": [6], "className": "text-right", "width": "1%"}, //pick_qty
                        {"targets": [7], "className": "text-right", "width": "1%"}, //remain_qty
                        {"targets": [8], "className": "text-left", "width": "1%"}, //inv_no
                        {"targets": [9], "className": "text-left", "width": "1%"}, //job
                        {"targets": [10], "className": "text-left", "width": "1%"}, //transac_date
                        {"targets": [11], "className": "text-left", "width": "1%"}, //seq
                        {"targets": [12], "className": "text-left", "width": "1%"}, //STS 0
                        {"targets": [13], "className": "text-left", "width": "1%"}, //STS 1
                        {"targets": [14], "className": "text-left", "width": "1%"}, //STS 2
                        {"targets": [15], "className": "text-left", "width": "1%"}, //STS 3
                        {"targets": [16], "className": "text-left", "width": "1%"}, //STS 4
                        {"targets": [17], "className": "text-left", "width": "1%"}, //STS 5
                        {"targets": [18], "className": "text-left", "width": "2%"}, //STS 6
                        {"targets": [19], "className": "text-left", "width": "1%"}, //STS 7
                        {"targets": [20], "className": "text-left", "width": "1%"} //STS 8
                    ]
                };

                if (sorter === "ord") {
                    $('#seloption').val("Sales Order");
                    get("Sales Order");
                    $('#sorter').val(param);

                    //GETHead
                    $.ajax({
                        url: "/TABLE2/ShowDataTablesISM910?mode=getHead",
                        data: {param: param, sorter: sorter},
                        async: false
                    }).done(function (result) {
                        $('#textSalesOrder').text(result[0].ISHORD);
                        $('#textCusCode').text(result[0].ISHCUNO.toString().substring(4, 10));
                        $('#textCusName').text(result[0].ISHCUNM1);
                        $('#textDocNo').text(result[0].ISHSUBMI);
                        $('#textStyleFG').text(result[0].ISHSTYLE + result[0].ISHCOLOR);
                        $('#textLot').text("#" + result[0].ISHLOT.toString().replace("#", ""));
                    }).fail(function (jqXHR, textStatus, errorThrown) {
                        // needs to implement if it fails
                    });

                } else if (sorter === "cus") {
                    $('#seloption').val("Customer Code");
                    get("Customer Code");
                    $('#sorter').val(param);

                    //GETHead
                    $.ajax({
                        url: "/TABLE2/ShowDataTablesISM910?mode=getHead",
                        data: {param: param, sorter: sorter},
                        async: false
                    }).done(function (result) {
//                        $('#textSalesOrder').text(result[0].ISHORD);
                        $('#textCusCode').text(result[0].ISHCUNO.toString().substring(4, 10));
                        $('#textCusName').text(result[0].ISHCUNM1);
//                        $('#textDocNo').text(result[0].ISHSUBMI);
//                        $('#textStyleFG').text(result[0].ISHSTYLE + result[0].ISHCOLOR);
//                        $('#textLot').text("#" + result[0].ISHLOT.toString().replace("#", ""));
                    }).fail(function (jqXHR, textStatus, errorThrown) {
                        // needs to implement if it fails
                    });

                } else if (sorter === "doc") {
                    $('#seloption').val("Doc No");
                    get("Doc No");
                    $('#sorter').val(param);

                    //GETHead
                    $.ajax({
                        url: "/TABLE2/ShowDataTablesISM910?mode=getHead",
                        data: {param: param, sorter: sorter},
                        async: false
                    }).done(function (result) {
                        $('#textSalesOrder').text(result[0].ISHORD);
                        $('#textCusCode').text(result[0].ISHCUNO.toString().substring(4, 10));
                        $('#textCusName').text(result[0].ISHCUNM1);
                        $('#textDocNo').text(result[0].ISHSUBMI);
                        $('#textStyleFG').text(result[0].ISHSTYLE + result[0].ISHCOLOR);
                        $('#textLot').text("#" + result[0].ISHLOT.toString().replace("#", ""));
                    }).fail(function (jqXHR, textStatus, errorThrown) {
                        // needs to implement if it fails
                    });

                } else if (sorter === "sty") {
                    $('#seloption').val("Style FG");
                    get("Style FG");
                    $('#sorter').val(param);

                    //GETHead
                    $.ajax({
                        url: "/TABLE2/ShowDataTablesISM910?mode=getHead",
                        data: {param: param, sorter: sorter},
                        async: false
                    }).done(function (result) {
//                        $('#textSalesOrder').text(result[0].ISHORD);
//                        $('#textCusCode').text(result[0].ISHCUNO.toString().substring(4, 10));
//                        $('#textCusName').text(result[0].ISHCUNM1);
//                        $('#textDocNo').text(result[0].ISHSUBMI);
                        $('#textStyleFG').text(result[0].ISHSTYLE + result[0].ISHCOLOR);
                        $('#textLot').text("#" + result[0].ISHLOT.toString().replace("#", ""));
                    }).fail(function (jqXHR, textStatus, errorThrown) {
                        // needs to implement if it fails
                    });

                } else if (sorter === "lot") {
                    $('#seloption').val("Lot");
                    get("Lot");
                    $('#sorter').val(param);

                    //GETHead
                    $.ajax({
                        url: "/TABLE2/ShowDataTablesISM910?mode=getHead",
                        data: {param: param, sorter: sorter},
                        async: false
                    }).done(function (result) {
//                        $('#textSalesOrder').text(result[0].ISHORD);
//                        $('#textCusCode').text(result[0].ISHCUNO.toString().substring(4, 10));
//                        $('#textCusName').text(result[0].ISHCUNM1);
//                        $('#textDocNo').text(result[0].ISHSUBMI);
//                        $('#textStyleFG').text(result[0].ISHSTYLE + result[0].ISHCOLOR);
                        $('#textLot').text("#" + result[0].ISHLOT.toString().replace("#", ""));
                    }).fail(function (jqXHR, textStatus, errorThrown) {
                        // needs to implement if it fails
                    });
                }

                var table = $('#showTable').DataTable(tableOptions);

            }

            function get(val) {

                let whatSel = val;

                if (whatSel === "Sales Order") {

                    $('#sorter').find('option').remove().end().append('<option value="" selected></option>').val('');

                    //GETSALESORDERNo
                    $.ajax({
                        url: "/TABLE2/ShowDataTablesISM910?mode=getDistinctSalesH",
                        async: false
                    }).done(function (result) {
                        for (var i = 0; i < result.length; i++) {
                            $('#sorter').append("<option>" + result[i].ISHORD + "</option>");
                        }
                    }).fail(function (jqXHR, textStatus, errorThrown) {
                        // needs to implement if it fails
                    });

                } else if (whatSel === "Customer Code") {

                    $('#sorter').find('option').remove().end().append('<option value="" selected></option>').val('');

                    $.ajax({
                        url: "/TABLE2/ShowDataTablesISM910?mode=getDistinctCusNoH",
                        async: false
                    }).done(function (result) {
                        for (var i = 0; i < result.length; i++) {
                            $('#sorter').append("<option>" + result[i].ISHCUNO + "</option>");
                        }
                    }).fail(function (jqXHR, textStatus, errorThrown) {
                        // needs to implement if it fails
                    });

                } else if (whatSel === "Doc No") {

                    $('#sorter').find('option').remove().end().append('<option value="" selected></option>').val('');

                    $.ajax({
                        url: "/TABLE2/ShowDataTablesISM910?mode=getDistinctDocNoH",
                        async: false
                    }).done(function (result) {
                        for (var i = 0; i < result.length; i++) {
                            $('#sorter').append("<option>" + result[i].ISHSUBMI + "</option>");
                        }
                    }).fail(function (jqXHR, textStatus, errorThrown) {
                        // needs to implement if it fails
                    });

                } else if (whatSel === "Style FG") {

                    $('#sorter').find('option').remove().end().append('<option value="" selected></option>').val('');

                    $.ajax({
                        url: "/TABLE2/ShowDataTablesISM910?mode=getDistinctStyFGH",
                        async: false
                    }).done(function (result) {
                        for (var i = 0; i < result.length; i++) {
                            $('#sorter').append("<option>" + result[i].ISHSTYLE + result[i].ISHCOLOR + "</option>");
                        }
                    }).fail(function (jqXHR, textStatus, errorThrown) {
                        // needs to implement if it fails
                    });

                } else if (whatSel === "Lot") {

                    $('#sorter').find('option').remove().end().append('<option value="" selected></option>').val('');

                    $.ajax({
                        url: "/TABLE2/ShowDataTablesISM910?mode=getDistinctLotH",
                        async: false
                    }).done(function (result) {
                        for (var i = 0; i < result.length; i++) {
                            $('#sorter').append("<option>" + result[i].ISHLOT + "</option>");
                        }
                    }).fail(function (jqXHR, textStatus, errorThrown) {
                        // needs to implement if it fails
                    });

                } else {
                    $('#sorter').find('option').remove();
                }
            }

            $(document).ready(function () {

                if ("${find}" === "ord") {
                    if ("${orderNo}" !== "") {
                        getDatas("${orderNo}", "${find}");
                    }
                } else if ("${find}" === "cus") {
                    if ("${cusNo}" !== "") {
                        getDatas("${cusNo}", "${find}");
                    }
                } else if ("${find}" === "doc") {
                    if ("${docNo}" !== "") {
                        getDatas("${docNo}", "${find}");
                    }
                } else if ("${find}" === "sty") {
                    if ("${styFG}" !== "") {
                        getDatas("${styFG}", "${find}");
                    }
                } else if ("${find}" === "lot") {
                    if ("${LotNo}" !== "") {
                        getDatas("${LotNo}", "${find}");
                    }
                }

                // Setup - add a text input to each footer cell
                $('#showTable tfoot th').not(":eq(0)").each(function () {
                    var title = $(this).text();
                    $(this).html('<input type="text" />');
                });

                // DataTable
                var table = $('#showTable').DataTable();

                // Apply the search
                table.columns().every(function () {
                    var that = this;

                    $('input', this.footer()).on('keyup change', function () {
                        if (that.search() !== this.value) {
                            that.search(this.value).draw();
                        }
                    });
                });

                $('#seloption').on('change', function () {

                    let whatSel = this.value;

                    if (whatSel === "Sales Order") {
                        get("Sales Order");
                    } else if (whatSel === "Customer Code") {
                        get("Customer Code");
                    } else if (whatSel === "Doc No") {
                        get("Doc No");
                    } else if (whatSel === "Style FG") {
                        get("Style FG");
                    } else if (whatSel === "Lot") {
                        get("Lot");
                    } else {
                        $('#sorter').find('option').remove();
                    }

                });

                $('#sorter').on('change', function () {

                    let ProgramIDLowwer = "${from}";
//                    let ProgramID = ProgramIDLowwer.toUpperCase();

                    let seloption = $('#seloption').val();

                    if ($.fn.DataTable.isDataTable('#showTable')) { //before RE-Create New Datatable
                        $('#showTable').DataTable().destroy();
                    }

                    $('#showTable tbody').empty();

                    let whatSel = this.value;

                    if (seloption === "Sales Order") {

                        var urlText = window.location.href;

                        var url = new URL(urlText);
                        url.search = '';
                        var new_url = url.toString();

                        document.location = new_url + '?find=ord&from=' + ProgramIDLowwer + '&orderNo=' + whatSel;

                    } else if (seloption === "Customer Code") {

                        var urlText = window.location.href;

                        var url = new URL(urlText);
                        url.search = '';
                        var new_url = url.toString();

                        document.location = new_url + '?find=cus&from=' + ProgramIDLowwer + '&cusNo=' + whatSel;
                    } else if (seloption === "Doc No") {

                        var urlText = window.location.href;

                        var url = new URL(urlText);
                        url.search = '';
                        var new_url = url.toString();

                        document.location = new_url + '?find=doc&from=' + ProgramIDLowwer + '&docNo=' + whatSel;

                    } else if (seloption === "Style FG") {

                        var urlText = window.location.href;

                        var url = new URL(urlText);
                        url.search = '';
                        var new_url = url.toString();

                        document.location = new_url + '?find=sty&from=' + ProgramIDLowwer + '&styFG=' + whatSel;

                    } else if (seloption === "Lot") {

                        var urlText = window.location.href;

                        var url = new URL(urlText);
                        url.search = '';
                        var new_url = url.toString();

                        document.location = new_url + '?find=lot&from=' + ProgramIDLowwer + '&LotNo=' + whatSel;
                    }

                });

                $('#backto100').on('click', function () {

                    let ProgramIDLowwer = "${from}";
                    let ProgramID = ProgramIDLowwer.toUpperCase();

                    window.location.href = "/TABLE2/" + ProgramID + "/display?uid=" + sessionStorage.getItem('uid');
                });

            });

        </script>

    </head>
    <body>
        <div id="wrapper">
            <!-- /#sidebar-wrapper -->
            <%@ include file="../fragments/sidebar.jsp" %>
            <%@ include file="../fragments/nav_head.jsp" %>

            <form action="../resources/manual/ISM910.pdf" target="_blank">
                <table width="100%">
                    <tr id="dont">
                        <td width="94%" align="left"></td>
                        <td width="6%" align="right"><input type="submit" value="Manual" /></td>
                    </tr>
                </table>
            </form>

            <div class="one">

                <div class="col-lg-12" style="height:30px; background-color: white;" ></div>

                <div class="col-lg-12" style="padding-left: 120px; background-color: white;">
                    <div class="col-lg-1">
                        Sales Order
                    </div>
                    <div class="col-lg-1">
                        Customer Code
                    </div>
                    <div class="col-lg-2">
                        Customer Name
                    </div>
                    <div class="col-lg-1">
                        Doc no
                    </div>
                    <div class="col-lg-1">
                        Style FG
                    </div>
                    <div class="col-lg-2">
                        Lot
                    </div>
                    <div class="col-lg-4">
                        <div class="col-lg-4">Sort By</div>
                        <div class="col-lg-4"></div>
                        <div class="col-lg-4"></div>
                    </div>
                </div>

                <div class="col-lg-12" style="height:10px; background-color: white;"></div>

                <div class="col-lg-12" style="padding-left: 120px; background-color: white;">
                    <div class="col-lg-1">
                        <label id="textSalesOrder"></label>
                    </div>
                    <div class="col-lg-1">
                        <label id="textCusCode"></label>
                    </div>
                    <div class="col-lg-2">
                        <label id="textCusName"></label>
                    </div>
                    <div class="col-lg-1">
                        <label id="textDocNo"></label>
                    </div>
                    <div class="col-lg-1">
                        <label id="textStyleFG"></label>
                    </div>
                    <div class="col-lg-2">
                        <label id="textLot"></label>
                    </div>
                    <div class="col-lg-4">
                        <div class="col-lg-4">
                            <select id="seloption" class="form-control">
                                <option selected></option>
                                <option>Sales Order</option>
                                <option>Customer Code</option>
                                <option>Doc No</option>
                                <option>Style FG</option>
                                <option>Lot</option>
                            </select>
                        </div>
                        <div class="col-lg-4"> 
                            <!--<input id="sorter" class="form-control" list="optionsort" >-->
                            <!--<datalist id="optionsort"></datalist>-->
                            <select class="form-control" id="sorter"></select>
                        </div>

                        <div class="col-lg-4" style="text-align:right;">
                            <button style="height:34px; width: 98px;" class="btn btn-danger" id="backto100" >Back</button>
                        </div>
                    </div>
                </div>

                <div class="col-lg-12" style="height:30px; background-color: white;"></div>

            </div>

            <div class="container-fluid">

                <div style="border: 2px solid #ccc; border-radius: 5px; padding: 0px 10px 0px 10px;">

                    <table id="showTable" class="display" style="width:100%; border: 1px solid #ccc; border-radius: 4px;">
                        <thead class="two">
                            <tr>
                                <th>Sales Order</th>
                                <th>Line</th>
                                <th>Material Code</th>
                                <th>VT</th>
                                <th>Desc</th>
                                <th>Req Qty</th>
                                <th>Pick Qty</th>
                                <th>Rem Qty</th>
                                <th>Inv No.</th>
                                <th>Job no</th>
                                <th>Trans date</th>
                                <th>Seq</th>
                                <th colspan="3" style="text-align: center; background-color: #FFF2CC">Business Status</th>
                                <th colspan="4" style="text-align: center; background-color: #DDEBF7">Stock RM Status</th>
                                <th style="text-align: center; background-color: #FFE699">Destination</th>
                                <th></th>
                            </tr>
                            <tr>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th>New</th>
                                <th>Req</th>
                                <th>App</th>
                                <th>Rec</th>
                                <th>Q</th>
                                <th>Isu</th>
                                <th>@ Trans</th>
                                <th>Rec</th>
                                <th>Cancelled</th>
                            </tr>
                        </thead>

                        <tbody></tbody>

                    </table>

                </div>

            </div>
        </div>
    </body>
</html>
