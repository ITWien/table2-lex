<%@ include file="../fragments/taglibs.jsp" %>
<%@ include file="../fragments/imports.jsp" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.sql.*;" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>WMS</title>
        <!-- css :: vendors -->
        <jsp:include page = "../fragments/css.jsp" />
        <!-- additional custom :: my-style -->
        <link rel="stylesheet" href="../resources/assets/styles/myStyles.css">

        <!-- js :: vendors -->
        <jsp:include page = "../fragments/script.jsp" />
        <!-- additional custom :: my-script -->
        <script src="../resources/assets/scripts/myScripts.js" async></script>
        <script src="../resources/assets/scripts/toggleLoad.js" async></script> 
        <style>
            input[type=text], select{
                width: 100%;
                padding: 5px 5px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }

            input[type=date], input[type=number]{
                /*width: 100%;*/
                padding: 5px 5px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }

            input[type=submit] {
                width: 100%;
                background-color: #00399b;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=submit]:hover {
                background-color: #3973d6;
            }

            input[type=button] {
                width: 100%;
                background-color: #ef604a;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=button]:hover {
                background-color: #e51e00;
            }
        </style>
        <script>
            function showWH(dest, close) {
                var modal = document.getElementById(dest);
                modal.style.display = "block";
                var span = document.getElementById(close);
                span.onclick = function () {
                    modal.style.display = "none";
                };
                window.onclick = function (event) {
                    if (event.target === modal) {
                        modal.style.display = "none";
                    }
                };
            }
        </script>
        <style>
            body {font-family: Arial, Helvetica, sans-serif;}

            /* The Modal (background) */
            .modal {
                display: none; /* Hidden by default */
                position: fixed; /* Stay in place */
                z-index: 1; /* Sit on top */
                padding-top: 100px; /* Location of the box */
                left: 0;
                top: 0;
                width: 100%; /* Full width */
                height: 100%; /* Full height */
                overflow: auto; /* Enable scroll if needed */
                background-color: rgb(0,0,0); /* Fallback color */
                background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
            }

            /* Modal Content */
            .modal-content {
                background-color: #fefefe;
                margin: auto;
                padding: 20px;
                border: 1px solid #888;
                width: fit-content;
                height: fit-content;
            }

            /* The Close Button */
            .close {
                color: #aaaaaa;
                float: right;
                font-size: 28px;
                font-weight: bold;
            }

            .close:hover,
            .close:focus {
                color: #000;
                text-decoration: none;
                cursor: pointer;
            }
        </style>
        <script>
            $(document).ready(function () {
                $('#showTable').DataTable({
                    "ordering": false,
                    "lengthMenu": [[-1, 10, 25, 50, 100], ["All", 10, 25, 50, 100]],
                    "bSortClasses": false,
                    "columnDefs": [
                        {orderable: false, targets: [13]},
//                        {orderable: false, targets: [16]},
                        {"width": "7%", "targets": 11},
                        {"width": "15%", "targets": 13}
                    ]
                });

                // Setup - add a text input to each footer cell
                $('#showTable tfoot th').not(":eq(12)").each(function () {
                    var title = $(this).text();
                    $(this).html('<input type="text" />');
                });

                // DataTable
                var table = $('#showTable').DataTable();

                // Apply the search
                table.columns().every(function () {
                    var that = this;
                    $('input', this.footer()).on('keyup change', function () {
                        if (that.search() !== this.value) {
                            that
                                    .search(this.value)
                                    .draw();
                        }
                    });
                });
            });
        </script>
        <script>
            function ValidForm() {
                var wh = document.getElementById('wh').value;
                var shipDate = document.getElementById('shipDate').value;

                if (wh !== '' && shipDate !== '') {
                    document.getElementById('frm').submit();
                }
            }
        </script>
    </head>    
    <body onload="document.getElementById('userid').value = sessionStorage.getItem('uid');">
        <div id="wrapper">

            <!-- /#sidebar-wrapper -->
            <%@ include file="../fragments/sidebar.jsp" %>

            <!-- Page Content -->
            <!-- nav-head-custom -->
            <%@ include file="../fragments/nav_head.jsp" %>

            <div class="container-fluid">
                <!--<div id="wrapper-top">-->
                <%-- PART 2 --%>
                <!--                    <div class="row">
                                        <div class="col-lg-12">
                                            <div id="set-height" style="height:415px;margin-top:0px;">
                                                <div id="sidebar-wrapper-top" class="">
                                                    <b class="page-header" style="padding-left:5px;font-size:18px;">
                <%-- <hr style="margin:0px;margin-top:10px;margin-bottom:10px;"> --%>
            </b>
        </div>
    </div>
</div>
</div> -->
                <%-- PART 3 --%>
                <!--show Qdest table-->
                <form action="../resources/manual/XMS400.pdf" target="_blank">
                    <table width="100%">
                        <tr>
                            <td width="94%"></td>
                            <td width="6%" align="right"><input type="submit" value="Manual" /></td>
                        </tr>
                    </table>
                </form>
                <div style="border: 2px solid #ccc; border-radius: 5px; padding: 20px;">
                    <form id="frm" name="frm" action="display" method="post">
                        <table width="100%">
                            <tr>
                                <td width="20%"><b style="color: #00399b;">Warehouse : </b>
                                    <select name="wh" id="wh" style="width:55%;" onchange="ValidForm();">
                                        <option value="${wh}" selected hidden>${wh} : ${whn}</option>
                                        <c:forEach items="${MCList}" var="p" varStatus="i">
                                            <option value="${p.uid}">${p.uid} : ${p.name}</option>
                                        </c:forEach>
                                    </select>
                                </td>
                                <td width="20%">
                                </td>
                                <td width="25%"></td>
                                <td></td>
                                <td width="5%"></td>
                                <td width="5%"></td>
                                <td width="20%"><b style="color: #00399b;">Shipment Date : </b>
                                    <input type="date" name="shipDate" id="shipDate" style=" height: 30px;" value="${shipDate}" onchange="ValidForm();">
                                </td>
                            </tr>
                        </table>
                    </form>
                    <form id="detail" method="post">
                        <input type="hidden" id="userid" name="userid">
                        <input type="hidden" name="wh" value="${wh}">
                        <input type="hidden" name="shipDate" value="${shipDate}">
                        <table id="showTable" class="display" style="width:100%; border: 1px solid #ccc; border-radius: 4px;">
                            <thead>
                                <tr>
                                    <th colspan="6" style="text-align: center; border-bottom: none;"></th>
                                    <th colspan="4" style="text-align: center; border-bottom-color: #ccc;">Package</th>
                                </tr>
                                <tr>
                                    <th style="text-align: center;">Destination</th>
                                    <th style="text-align: center;">Round</th>
                                    <th style="text-align: center;">Type</th>
                                    <th style="text-align: center;">License no.</th>
                                    <th style="text-align: center;">Driver Name</th>
                                    <th style="text-align: center;">Follower Name</th>
                                    <th style="text-align: center;">Bag</th>
                                    <th style="text-align: center;">Roll</th>
                                    <th style="text-align: center;">Box</th>
                                    <th style="text-align: center;">Pcs</th>
                                    <th style="text-align: center;">Total Docs</th>
                                    <th style="text-align: center;">Doc No.</th>
                                    <th style="text-align: center;">User</th>
                                    <th style="text-align: center;">Option
                                        <a href="create?wh=${wh}&shipDate=${shipDate}"><i class="fa fa-plus-circle" style="font-size:28px; padding-left: 10px"></i></a>
                                        <!--<a href="print?printBy=whdt&wh=${wh}&shipDate=${shipDate}" target="_blank"><i class="fa fa-print" style="font-size:28px; padding-left: 10px; color: #1f7d16;"></i></a>-->
                                    </th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th>dest</th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </tfoot>
                            <tbody>
                                <c:forEach items="${HEADList}" var="x">
                                    <tr>
                                        <td>${x.XMSOTHDEST}</td>
                                        <td>${x.XMSOTHROUND}</td>
                                        <td>${x.XMSOTHTYPE}</td>
                                        <td>${x.XMSOTHLICENNO}</td>
                                        <td>${x.XMSOTHDRIVER}</td>
                                        <td>${x.XMSOTHFOLLOWER}</td>
                                        <td align="right">${x.XMSOTHBAG}&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                        <td align="right">${x.XMSOTHROLL}&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                        <td align="right">${x.XMSOTHBOX}&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                        <td align="right">${x.XMSOTHPCS}&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                        <td align="right">${x.XMSOTHTOTDOC}&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                        <td>${x.XMSOTHDOCNO}</td>
                                        <td>${x.XMSOTHUSER}</td>
                                        <td align="center">
                                            ${x.options}
                                            <!-- The Modal -->
                                            <div id="myModal-del-${x.XMSOTHQNO}" class="modal">
                                                <!-- Modal content -->
                                                <div class="modal-content" style=" height: 350px; width: 500px;">
                                                    <span id="span-del-${x.XMSOTHQNO}" class="close" onclick="document.getElementById('myModal-del-${x.XMSOTHQNO}').style.display = 'none';">&times;</span>
                                                    <p><b><font size="4"> </font></b></p>
                                                    <br>
                                                    <table width="100%">
                                                        <tr style=" background-color: white;">
                                                            <td align="center">
                                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                <b style="color: #00399b;">
                                                                    <font size="5">Do you want to delete ?</font><hr>
                                                                    <font size="4">Destination : ${x.XMSOTHDEST}</font><br>
                                                                    <font size="4">Round : ${x.XMSOTHROUND}</font><br>
                                                                    <font size="4">Type : ${x.XMSOTHTYPE}</font><br>
                                                                    <font size="4">License No. : ${x.XMSOTHLICENNO}</font>
                                                                </b>
                                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                <br><br><br>
                                                                <a class="btn btn btn-outline btn-danger" onclick="document.getElementById('myModal-del-${x.XMSOTHQNO}').style.display = 'none';">
                                                                    <i class="fa fa-times-circle-o" style="font-size:20px;"></i> Cancel</a>  
                                                                &nbsp;
                                                                <a class="btn btn btn-outline btn-success" href="delete?qno=${x.XMSOTHQNO}&wh=${wh}&shipDate=${shipDate}">
                                                                    <i class="fa fa-check-circle-o" style="font-size:20px;"></i> Confirm</a>                      
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                </c:forEach>
                            </tbody>
                        </table>
                    </form>
                    <!--**********************-->
                    <!--End Part 3-->
                    <!--</div>  end #wrapper-top--> 
                </div> 
                <br>
            </div> <!-- end .container-fluid -->
        </div> <!-- end #wrapper -->
    </body>
</html>