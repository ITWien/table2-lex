<%@ include file="../fragments/taglibs.jsp" %>
<%@ include file="../fragments/imports.jsp" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.sql.*;" %>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <title>ISM</title>
        <!-- css :: vendors -->
        <jsp:include page="../fragments/css.jsp" />
        <!-- additional custom :: my-style -->
        <link rel="stylesheet" href="../resources/assets/styles/myStyles.css">
        <link rel="stylesheet" href="../resources/font/THsarabun/thsarabumnew.css">

        <!-- js :: vendors -->
        <jsp:include page="../fragments/script.jsp" />
        <!-- additional custom :: my-script -->
        <script src="../resources/assets/scripts/myScripts.js" async></script>
        <script src="../resources/assets/scripts/toggleLoad.js" async></script>
        <style>
            input[type=text],
            select {
                width: 100%;
                padding: 5px 5px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }

            input[type=date],
            input[type=number] {
                /*width: 100%;*/
                padding: 5px 5px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }

            input[type=submit] {
                width: 100%;
                background-color: #00399b;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=submit]:hover {
                background-color: #3973d6;
            }

            input[type=button] {
                width: 100%;
                background-color: #ef604a;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=button]:hover {
                background-color: #e51e00;
            }
        </style>
        <style>
            body {
                font-family: "Sarabun", sans-serif !important;
                /*font-family: 'THSarabunNew', sans-serif;*/
            }

            .displayTable {
                width: auto;
                border: 1px solid #ccc;
                border-radius: 4px;
            }

            .displayContain {
                border: 2px solid #ccc;
                border-radius: 5px;
                padding: 20px;
                /*width: fit-content;*/
            }

            /*            td {
                            white-space: nowrap;
                            cursor: pointer;
                        }*/

            .checkData ,.checkAllData{
                width: 20px;
                height: 20px;
            }

            .sts-0{
                font-size: 20px;
                color: navy;
            }
            .sts-1{
                font-size: 20px;
                color: yellow;
            }
            .sts-2{
                font-size: 20px;
                color: green;
            }
            .sts-3{
                font-size: 20px;
                color: blue;
            }
            .sts-4{
                font-size: 20px;
                color: pink;
            }
            .sts-5{
                font-size: 20px;
                color: navy;
            }
            .sts-6{
                font-size: 20px;
                color: gray;
            }
            .sts-7{
                font-size: 20px;
                color: brown;
            }
            .sts-8{
                font-size: 20px;
                color: orange;
            }
            .sts-9{
                font-size: 20px;
                color: black;
            }

            .modal-content{
                width: 50%;
            }

            .editBtn{
                color: navy;
                cursor: pointer;
            }

            .destIcon{
                font-size: 50px;
                color: #e3c727;
            }

            .centerTb{
                text-align: center;
                padding: 10px;
            }

            .cardDest{
                width: fit-content;
                text-align: center;
                padding: 20px;
                display: inline-grid;
                cursor: pointer;
                border-radius: 15px;
            }

            .cardDest:hover{
                background-color: #ededed;
            }

            .cardTb-l{
                border: 2px solid #2725a8;
                background-color: #cfdaff;
            }
            .cardTb-r{
                border: 2px solid #2725a8;
            }
        </style>
        <script>
            $(document).ready(function () {

                genCard();

                $('#backBtn').click(function () {
                    window.location.href = 'display';
                });

            });

            function genCard() {

//                var order = '';
//                var seq = '';

            <%--<c:forEach var="x" items="${order}">--%>
//                order += '&order=${x}';
            <%--</c:forEach>--%>

            <%--<c:forEach var="y" items="${seq}">--%>
//                seq += '&seq=${y}';
            <%--</c:forEach>--%>
//                console.log('order: ', order);

                var oANDs = '';
                let zz = '';
            <c:forEach var="z" items="${oANDs}">
                zz = '${z}';
                if (zz !== '' && zz !== null) {
                    oANDs += '&oANDs=' + zz;
                }
            </c:forEach>

                $.ajax({
                    url: "/TABLE2/ShowDataTablesISM200?mode=getCardData"
                            + oANDs
//                            + order + seq + oANDs

                }).done(function (result) {
                    console.log(result);
                    for (var i = 0; i < result.length; i++) {
                        addCard(
                                result[i].DEST,
                                result[i].DESTNAME,
                                result[i].ORDER,
                                result[i].PCS,
                                result[i].SKU,
                                result[i].NEARDATE
                                );
                    }

                    $('.cardDest').click(function () {
                        var dest = $(this).attr('dest');
                        window.location.href = 'prepare?dest=' + dest + oANDs;
//                        window.location.href = 'prepare?dest=' + dest + order + oANDs;
                    });

                }).fail(function (jqXHR, textStatus, errorThrown) {
                    // needs to implement if it fails
                });

            }

            function addCard(dest, destName, order, pcs, sku, nearDate) {

                var cardHtml = $('#cardTmp').html().toString();
                cardHtml = cardHtml.replace('[@DEST]', dest);
                cardHtml = cardHtml.replace('[@DEST]', dest);
                cardHtml = cardHtml.replace('[@DESTNAME]', destName);
                cardHtml = cardHtml.replace('[@ORDER]', order);
                cardHtml = cardHtml.replace('[@PCS]', pcs);
                cardHtml = cardHtml.replace('[@SKU]', sku);
//                cardHtml = cardHtml.replace('[@NEARDATE]', nearDate);
                cardHtml = cardHtml.replace('[@NEARDATE]', '');
                $('#cardList').append(cardHtml);

            }
        </script>
    </head>

    <body>
        <div id="wrapper">

            <!-- /#sidebar-wrapper -->
            <%@ include file="../fragments/sidebar.jsp" %>

            <!-- Page Content -->
            <!-- nav-head-custom -->
            <%@ include file="../fragments/nav_head.jsp" %>

            <div class="container-fluid">
                <!--<div id="wrapper-top">-->
                <%-- PART 2 --%>
                <!--                    <div class="row">
                                                <div class="col-lg-12">
                                                    <div id="set-height" style="height:415px;margin-top:0px;">
                                                        <div id="sidebar-wrapper-top" class="">
                                                            <b class="page-header" style="padding-left:5px;font-size:18px;">
                <%-- <hr style="margin:0px;margin-top:10px;margin-bottom:10px;"> --%>
            </b>
        </div>
    </div>
    </div>
    </div> -->
                <%-- PART 3 --%>
                <!--show Qdest table-->
                <br>
                <div class="displayContain">
                    <div style=" text-align: right;">
                        <button type="button" id="backBtn" class="btn btn-default" style="width: 150px;">Back</button>
                    </div>
                    <div id="cardList">
                    </div>
                    <!--**********************-->
                    <!--End Part 3-->
                    <!--</div>  end #wrapper-top-->
                </div>
                <div id="cardTmp" class="hidden">
                    <div class="cardDest" dest="[@DEST]">
                        <i class="fa fa-folder-open destIcon" aria-hidden="true"></i>
                        <h4><b>[@DEST] : [@DESTNAME]</b></h4>
                        <table>
                            <tr>
                                <th class="centerTb cardTb-l">
                                    T.Order
                                </th>
                                <th class="centerTb cardTb-r">
                                    [@ORDER]
                                </th>
                            </tr>
                            <tr>
                                <th class="centerTb cardTb-l">
                                    T.Pcs
                                </th>
                                <th class="centerTb cardTb-r">
                                    [@PCS]
                                </th>
                            </tr>
                            <tr>
                                <th class="centerTb cardTb-l">
                                    T.SKU
                                </th>
                                <th class="centerTb cardTb-r">
                                    [@SKU]
                                </th>
                            </tr>
                            <tr>
                                <th class="centerTb cardTb-l">
                                    Nearest Date
                                </th>
                                <th class="centerTb cardTb-r">
                                    [@NEARDATE]
                                </th>
                            </tr>
                        </table>
                    </div>
                </div>
                <br>
            </div> <!-- end .container-fluid -->
        </div> <!-- end #wrapper -->
    </body>

</html>