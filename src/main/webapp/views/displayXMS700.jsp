<%@ include file="../fragments/taglibs.jsp" %>
<%@ include file="../fragments/imports.jsp" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.sql.*;" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>WMS</title>
        <!-- css :: vendors -->
        <jsp:include page = "../fragments/css.jsp" />
        <!-- additional custom :: my-style -->
        <link rel="stylesheet" href="../resources/assets/styles/myStyles.css">

        <!-- js :: vendors -->
        <jsp:include page = "../fragments/script.jsp" />
        <!-- additional custom :: my-script -->
        <script src="../resources/assets/scripts/myScripts.js" async></script>
        <script src="../resources/assets/scripts/toggleLoad.js" async></script> 
        <style>
            input[type=text], select{
                width: 90%;
                padding: 5px 5px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }

            input[type=date], input[type=number]{
                /*width: 100%;*/
                padding: 5px 5px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }

            input[type=submit] {
                width: 100%;
                background-color: #00399b;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=submit]:hover {
                background-color: #3973d6;
            }

            input[type=button] {
                width: 100%;
                background-color: #ef604a;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=button]:hover {
                background-color: #e51e00;
            }
        </style>
        <style>
            body {font-family: Arial, Helvetica, sans-serif;}

            /* The Modal (background) */
            .modal {
                display: none; /* Hidden by default */
                position: fixed; /* Stay in place */
                z-index: 1; /* Sit on top */
                padding-top: 100px; /* Location of the box */
                left: 0;
                top: 0;
                width: 100%; /* Full width */
                height: 100%; /* Full height */
                overflow: auto; /* Enable scroll if needed */
                background-color: rgb(0,0,0); /* Fallback color */
                background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
            }

            /* Modal Content */
            .modal-content {
                background-color: #fefefe;
                margin: auto;
                padding: 20px;
                border: 1px solid #888;
                width: fit-content;
                height: fit-content;
            }

            /* The Close Button */
            .close {
                color: #aaaaaa;
                float: right;
                font-size: 28px;
                font-weight: bold;
            }

            .close:hover,
            .close:focus {
                color: #000;
                text-decoration: none;
                cursor: pointer;
            }
        </style>
        <script>
            function ValidForm() {
                var input = document.getElementsByTagName('input');

                for (var i = 0; i < input.length; i++) {
                    if (input[i].name === "inputX" || input[i].name === "inputY") {
                        if (input[i].value.toString().trim() === "") {
                            input[i].name = "EX" + input[i].name;
                        }
                        input[i].value = input[i].value.replace(/,/g, '');
                    }
                }

                document.getElementById('frm').submit();
            }
        </script>
        <script>
            function isNumberKey(evt) {
                var charCode = (evt.which) ? evt.which : event.keyCode;
//                alert(charCode);
                if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                    if (charCode === 46) {
                        return true;
                    } else {
                        return false;
                    }
                } else {
                    return true;
                }
            }

            function addRow() {
                //Create an input type dynamically.
                var element = document.createElement("input");

                //Assign different attributes to the element.
                element.setAttribute("type", "text");
                element.setAttribute("name", "inputX");
                element.setAttribute("style", "text-align: right; width: 45%;");
                element.setAttribute("onkeypress", "return isNumberKey(event)");
                element.setAttribute("onpaste", "getPastedValue(this,event)");

                //Create an input type dynamically.
                var element2 = document.createElement("input");

                //Assign different attributes to the element.
                element2.setAttribute("type", "text");
                element2.setAttribute("name", "inputY");
                element2.setAttribute("style", "text-align: right; width: 45%;");
                element2.setAttribute("onkeypress", "return isNumberKey(event)");
                element2.setAttribute("onpaste", "getPastedValue(this,event)");

                var br = document.createElement("br");

                var foo = document.getElementById("tableInput");

                //Append the element in page (in span).
                foo.appendChild(element);
                foo.appendChild(element2);
                foo.appendChild(br);
            }

            function getPastedValue(obj, event) {
                var val = event.clipboardData.getData('text');
                event.preventDefault();

                var xy = val.toString().split(/\s+/);
                for (var i = 0; i < xy.length; i++) {
                    var input = document.getElementsByTagName('input');
                    var cnt = 0;
                    for (var j = 0; j < input.length; j++) {
                        if (input[j].name === "inputX" || input[j].name === "inputY") {
                            if (input[j].value.toString().trim() === "") {
                                cnt++;
                                input[j].value = xy[i];
                                break;
                            }
                        }
                    }
                    if (cnt === 0 && xy[i].toString().trim() !== "") {
                        addRow();
                        i--;
                    }
                }
            }
        </script>
    </head>    
    <body onload="document.getElementById('userid').value = sessionStorage.getItem('uid');">
        <div id="wrapper">

            <!-- /#sidebar-wrapper -->
            <%@ include file="../fragments/sidebar.jsp" %>

            <!-- Page Content -->
            <!-- nav-head-custom -->
            <%@ include file="../fragments/nav_head.jsp" %>

            <div class="container-fluid">
                <!--<div id="wrapper-top">-->
                <%-- PART 2 --%>
                <!--                    <div class="row">
                                        <div class="col-lg-12">
                                            <div id="set-height" style="height:415px;margin-top:0px;">
                                                <div id="sidebar-wrapper-top" class="">
                                                    <b class="page-header" style="padding-left:5px;font-size:18px;">
                <%-- <hr style="margin:0px;margin-top:10px;margin-bottom:10px;"> --%>
            </b>
        </div>
    </div>
</div>
</div> -->
                <%-- PART 3 --%>
                <!--show Qdest table-->
                <form action="../resources/manual/XMS700.pdf" target="_blank">
                    <table width="100%">
                        <tr>
                            <td width="94%"></td>
                            <td width="6%" align="right"><input type="submit" value="Manual" /></td>
                        </tr>
                    </table>
                </form>
                <div style="border: 2px solid #ccc; border-radius: 5px; padding: 20px;">
                    <form id="frm" name="frm" action="display" method="post">
                        <center>
                            <table>
                                <tr>
                                    <td align="center" style="vertical-align:top;">
                                        <h3>Input Data</h3><hr>
                                        <table style=" width: 100%;">
                                            <tr>
                                                <th style="text-align: center;">X</th>
                                                <th style="text-align: center;">Y</th>
                                            </tr>
                                        </table>
                                        <span id="tableInput">
                                            ${body}<br>
                                            <c:forEach items="${inputXYList}" var="x">
                                                <input type="text" value="${x.code}" onpaste="getPastedValue(this,event);" name="inputX" name="inputX" style="text-align: right; width: 45%;" onkeypress="return isNumberKey(event)">
                                                <input type="text" value="${x.desc}" onpaste="getPastedValue(this,event);" name="inputY" style="text-align: right; width: 45%;" onkeypress="return isNumberKey(event)"><br>
                                            </c:forEach>
                                        </span>
                                        <button type="button" onclick="addRow();" class="btn btn-info" style=" width: 90%;"><i class="fa fa-plus-circle" aria-hidden="true" style=" font-size: 30px;"></i></button>
                                    </td>
                                    <td style=" width: 30px;"></td>
                                    <td align="center" style="vertical-align:top;">
                                        <h3>Equation Result</h3><hr>
                                        <table>
                                            <tr>
                                                <th style="text-align: center;"><h2>${result}</h2></th>
                                            </tr>
                                            <tr>
                                                <th><b style=" opacity: 0;">TMP</b></th>
                                            </tr>
                                        </table>
                                        <button type="button" onclick="ValidForm()" class="btn btn-primary" style=" width: 47%;">Submit</button>
                                    </td>
                                </tr>
                            </table>
                        </center>
                    </form>
                    <!--**********************-->
                    <!--End Part 3-->
                    <!--</div>  end #wrapper-top--> 
                </div> 
                <br>
            </div> <!-- end .container-fluid -->
        </div> <!-- end #wrapper -->
    </body>
</html>