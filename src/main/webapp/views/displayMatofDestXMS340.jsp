<%@ include file="../fragments/taglibs.jsp" %>
<%@ include file="../fragments/imports.jsp" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.sql.*;" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>WMS</title>
        <!-- css :: vendors -->
        <jsp:include page = "../fragments/css.jsp" />
        <!-- additional custom :: my-style -->
        <link rel="stylesheet" href="../resources/assets/styles/myStyles.css">

        <!-- js :: vendors -->
        <jsp:include page = "../fragments/script.jsp" />
        <!-- additional custom :: my-script -->
        <script src="../resources/assets/scripts/myScripts.js" async></script>
        <script src="../resources/assets/scripts/toggleLoad.js" async></script> 
        <style>
            input[type=text], select {
                width: 100%;
                padding: 5px 5px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }

            input[type=date] {
                height: 35px;
                padding: 5px 5px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }

            input[type=submit] {
                width: 100%;
                background-color: #00399b;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=submit]:hover {
                background-color: #3973d6;
            }

            b, th, td {
                color: #134596;
            }
        </style>
        <style>
            button {
                display: inline-block;
                /*padding: 60px 35px;*/
                margin-top: 20px;
                margin-bottom: 20px;
                font-size: 24px;
                cursor: pointer;
                text-align: center;
                text-decoration: none;
                outline: none;
                color: #fff;
                background-color: #1C56A5;
                border: none;
                border-radius: 10px;
                box-shadow: 0 9px #999;
            }

            button:hover {background-color: #00399b}

            button:active {
                background-color: #00399b;
                box-shadow: 0 5px #666;
                transform: translateY(4px);
            }

            input[type=button] {
                width: 100%;
                background-color: #ef604a;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=button]:hover {
                background-color: #e51e00;
            }
        </style>
    </head>    
    <body >
        <div id="wrapper">

            <!-- /#sidebar-wrapper -->
            <%@ include file="../fragments/sidebar.jsp" %>

            <!-- Page Content -->
            <!-- nav-head-custom -->
            <%@ include file="../fragments/nav_head.jsp" %>

            <div class="container-fluid">
                <!--<div id="wrapper-top">-->
                <%-- PART 2 --%>
                <!--                    <div class="row">
                                        <div class="col-lg-12">
                                            <div id="set-height" style="height:415px;margin-top:0px;">
                                                <div id="sidebar-wrapper-top" class="">
                                                    <b class="page-header" style="padding-left:5px;font-size:18px;">
                <%-- <hr style="margin:0px;margin-top:10px;margin-bottom:10px;"> --%>
            </b>
        </div>
    </div>
</div>
</div> -->
                <%-- PART 3 --%>
                <!--show Qdest table-->
                <form action="../resources/manual/XMS340.pdf" target="_blank">
                    <table width="100%">
                        <tr>
                            <td width="94%"></td>
                            <td width="6%" align="right"><input type="submit" value="Manual" /></td>
                        </tr>
                    </table>
                </form>
                <div style="border: 2px solid #ccc; border-radius: 5px; padding: 20px;">
                    <table width="100%">
                        <tr>
                            <th width="100px"><b>Warehouse : </b></th>
                            <td width="500px">
                                <select name="warehouseFrom" style="width:45%;" disabled>
                                    <option value="" selected hidden>${mcF} : ${nameF}</option>
                                </select>
                            </td>
                            <td align="right" width="90px">
                                <b>Select by : </b>
                            </td>
                            <td width="200px">
                                <select name="selectBy" disabled>
                                    <option value="" hidden>Destination</option>
                                </select>
                            </td>
                            <td></td>
                        </tr>
                    </table>
                    <br>
                    <b>${type}</b>
                    <br>
                    <a style="text-decoration: none;">
                        <button class="button" type="button" style="width: 250px; height: 250px; border-radius: 50%; background-color: #0048AB;">
                            <b style="text-shadow: 2px 2px #000; color: #ddd">${dest}</b>
                            <br>
                            &nbsp;<img src="../resources/images/dest/${dest}.jpg" height="170" width="170"  style="border-radius: 50%; border: 5px solid #000;">
                            <br>
                            <b style="text-shadow: 2px 2px #000; color: #ddd">${name}</b>
                        </button>
                    </a>
                    <br>
                    <c:forEach items="${mcList}" var="x">
                        ${x.name}
                        <a href="/TABLE2/XMS340/displayMat?WHS=${x.warehouse}&MTCTRL=${x.uid}"S target="_blank" style="text-decoration: none;">
                            <button class="button" type="button" value="${x.uid}">
                                <br>
                                <img src="../resources/images/matctrl/${x.uid}.jpg" height="150" width="130" style="border-radius: 8px;">
                                <br>
                                ${x.uid}
                            </button>
                        </a>
                    </c:forEach>
                    <!--End Part 3-->
                    <table width="100%">
                        <tr>
                            <td width="100%" align="right">
                                <input style="width: 100px;" type="button" value="Back" onclick="window.history.back()"/>
                            </td>
                        </tr>
                    </table>
                    <!--</div>  end #wrapper-top--> 
                </div> 
                <br>
            </div> <!-- end .container-fluid -->
        </div> <!-- end #wrapper -->
    </body>
</html>