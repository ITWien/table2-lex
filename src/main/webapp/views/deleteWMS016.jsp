<%@ include file="../fragments/taglibs.jsp" %>
<%@ include file="../fragments/imports.jsp" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.sql.*;" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>DELETE MODE</title>
        <!-- css :: vendors -->
        <jsp:include page = "../fragments/css.jsp" />
        <!-- additional custom :: my-style -->
        <link rel="stylesheet" href="../resources/assets/styles/myStyles.css">

        <!-- js :: vendors -->
        <jsp:include page = "../fragments/script.jsp" />
        <!-- additional custom :: my-script -->
        <script src="../resources/assets/scripts/myScripts.js" async></script>
        <script src="../resources/assets/scripts/toggleLoad.js" async></script>
        <script type="text/javascript"> /* add something code to your script */</script>
        <script type="text/javascript">
            var show = function () {
                $('#myModal').modal('show');
            };

            window.setTimeout(show, 0);

        </script>
        <style>
            input[type=text], select {
                width: 100%;
                padding: 5px 5px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }
        </style>
    </head>    
    <body >
        <div id="wrapper">

            <!-- /#sidebar-wrapper -->
            <%@ include file="../fragments/sidebar.jsp" %>

            <!-- Page Content -->
            <!-- nav-head-custom -->
            <%@ include file="../fragments/nav_head.jsp" %>

            <div class="container-fluid">
                <!--                <div id="wrapper-top" align="left">
                <%-- PART 2 --%>
                <div class="row">
                    <div class="col-lg-12">
                        <div id="set-height" style="height:415px;margin-top:0px;">
                            <div id="sidebar-wrapper-top" class="">
                                <b class="page-header" style="padding-left:5px;font-size:18px;">
                <%-- <hr style="margin:0px;margin-top:10px;margin-bottom:10px;"> --%>
            </b>
        </div>
    </div>
</div>
</div> -->

                <%-- PART 3 --%>
                <form action="" method="POST" name="frmD">
                    <table frame="box" width="100%" bordercolor="#e0e0e0">
                        <tr>
                            <th height="50" bgcolor="#f8f8f8">DELETE MODE</th>
                            <th height="50" bgcolor="#f8f8f8"></th>
                            <th height="50" bgcolor="#f8f8f8"></th>
                            <th height="50" bgcolor="#f8f8f8"></th>
                        </tr>
                        <br>
                        <tr>
                            <td width="15%"></td>
                            <td width="15%" align="left"><h4>Status Code :</h4></td>
                            <td width="30%" align="left" style="padding-right: 250px;"><input type="text" name="code" value="${code}" size="20" readonly></td>
                            <td width="40%"></td>
                        </tr>
                        <tr>
                            <td width="15%"></td>
                            <td width="15%" align="left"><h4>Description :</h4></td>
                            <td width="30%" align="left"><input type="text" name="desc" value="${desc}" size="40" readonly></td>
                            <td width="40%"></td>
                        </tr>
                        <br>
                    </table>
                    <center>
                        <div id="myModal" class="modal fade" role="dialog">
                            <div class="modal-dialog modal-sm"> <!-- ???????????? modal ?????????????? ???? xs, sm, md, lg -->
                                <div class="modal-md col-md-6" style="top: 150px; left:300px; background-color: #ddd; border: 1px solid #ccc; border-radius: 4px;">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title" align="center">Do you delete this record ?</h4>
                                        <input type="button" value="No"  style="width: 25%; background-color: #ef604a; color: white; padding: 5px 5px; margin: 8px 0; border: none; border-radius: 4px; cursor: pointer;"
                                               onclick="location.href = '/TABLE2/WMS016/display'">
                                        <a href="/TABLE2/WMS016/display">
                                            <button style="width: 25%; background-color: #2bd14a; color: white; padding: 5px 5px; margin: 8px 0; border: none; border-radius: 4px; cursor: pointer;" >
                                                <font color = "white">Yes</font>
                                            </button>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- devbanban.com -->
                    </center>
                </form>
                <!--End Part 3-->
                <!--</div>  end #wrapper-top -->
            </div> <!-- end .container-fluid -->
        </div> <!-- end #wrapper -->
    </body>
</html>
