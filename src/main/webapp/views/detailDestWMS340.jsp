<%@ include file="../fragments/taglibs.jsp" %>
<%@ include file="../fragments/imports.jsp" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.sql.*;" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>WMS</title>
        <!-- css :: vendors -->
        <jsp:include page = "../fragments/css.jsp" />
        <!-- additional custom :: my-style -->
        <link rel="stylesheet" href="../resources/assets/styles/myStyles.css">

        <!-- js :: vendors -->
        <jsp:include page = "../fragments/script.jsp" />
        <!-- additional custom :: my-script -->
        <script src="../resources/assets/scripts/myScripts.js" async></script>
        <script src="../resources/assets/scripts/toggleLoad.js" async></script> 
        <style>
            input[type=text], select{
                width: 100%;
                padding: 5px 5px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }

            input[type=date], input[type=number]{
                /*width: 100%;*/
                padding: 5px 5px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }

            input[type=submit] {
                width: 100%;
                background-color: #00399b;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=submit]:hover {
                background-color: #3973d6;
            }

            input[type=button] {
                width: 100%;
                background-color: #ef604a;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=button]:hover {
                background-color: #e51e00;
            }
        </style>
        <script>
            function showWH(dest, close) {
                var modal = document.getElementById(dest);
                modal.style.display = "block";
                var span = document.getElementById(close);
                span.onclick = function () {
                    modal.style.display = "none";
                };
                window.onclick = function (event) {
                    if (event.target === modal) {
                        modal.style.display = "none";
                    }
                };
            }
        </script>
        <style>
            body {font-family: Arial, Helvetica, sans-serif;}

            /* The Modal (background) */
            .modal {
                display: none; /* Hidden by default */
                position: fixed; /* Stay in place */
                z-index: 1; /* Sit on top */
                padding-top: 100px; /* Location of the box */
                left: 0;
                top: 0;
                width: 100%; /* Full width */
                height: 100%; /* Full height */
                overflow: auto; /* Enable scroll if needed */
                background-color: rgb(0,0,0); /* Fallback color */
                background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
            }

            /* Modal Content */
            .modal-content {
                background-color: #fefefe;
                margin: auto;
                padding: 20px;
                border: 1px solid #888;
                width: fit-content;
                height: 320px;
            }

            /* The Close Button */
            .close {
                color: #aaaaaa;
                float: right;
                font-size: 28px;
                font-weight: bold;
            }

            .close:hover,
            .close:focus {
                color: #000;
                text-decoration: none;
                cursor: pointer;
            }
        </style>
    </head>    
    <body onload="document.getElementById('userid').value = sessionStorage.getItem('uid');">
        <div id="wrapper">

            <!-- /#sidebar-wrapper -->
            <%@ include file="../fragments/sidebar.jsp" %>

            <!-- Page Content -->
            <!-- nav-head-custom -->
            <%@ include file="../fragments/nav_head.jsp" %>

            <div class="container-fluid">
                <!--<div id="wrapper-top">-->
                <%-- PART 2 --%>
                <!--                    <div class="row">
                                        <div class="col-lg-12">
                                            <div id="set-height" style="height:415px;margin-top:0px;">
                                                <div id="sidebar-wrapper-top" class="">
                                                    <b class="page-header" style="padding-left:5px;font-size:18px;">
                <%-- <hr style="margin:0px;margin-top:10px;margin-bottom:10px;"> --%>
            </b>
        </div>
    </div>
</div>
</div> -->
                <%-- PART 3 --%>
                <!--show Qdest table-->
                <form action="../resources/manual/WMS320.pdf" target="_blank">
                    <table width="100%">
                        <tr>
                            <td width="94%"></td>
                            <td width="6%" align="right"><input type="submit" value="Manual" /></td>
                        </tr>
                    </table>
                </form>
                <div style="border: 2px solid #ccc; border-radius: 5px; padding: 20px;">
                    <!--                    <table width="100%">
                                            <tr>
                                                <td width="100%" align="left"><b style="color: #00399b;">${wh}</b></td>
                                            </tr>
                                        </table>-->
                    <!--****************-->
                    <c:forEach items="${detList}" var="p" varStatus="i">
                        ${p.warehouse}
                        <hr style="height: 3px;">
                        <table width="100%">
                            <tr>
                                <th width="35%">
                                    <b style="color: #00399b;">
                                        Destination :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;${dest}
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        Material Control :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;${p.uid} : ${p.name} 
                                    </b>
                                </th>
                                <td style="text-align:right;">
                                    <b style="color: #00399b;">
                                        QR Code :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                                        <input type="text" name="qr-${p.uid}" id="qr-${p.uid}" style=" width: 500px;" autofocus>
                                        <script>
                                            var input = document.getElementById("qr-${p.uid}");
                                            input.addEventListener("keyup", function (event) {
                                                if (event.keyCode === 13) {
                                                    event.preventDefault();

                                                    var qr = document.getElementById("qr-${p.uid}");
                                                    qr = qr.value.toString().trim();
                                                    qr = qr.split("|");
                                                    var checkboxes = document.getElementsByTagName('input');
                                                    for (var i = 0; i < checkboxes.length; i++) {
                                                        if (checkboxes[i].id === 'selectCk-' + qr[0]) {
                                                            if (checkboxes[i].checked === true) {
                                                                checkboxes[i].checked = false;
                                                            } else if (checkboxes[i].checked === false) {
                                                                checkboxes[i].checked = true;
                                                            }
                                                        }
                                                    }

                                                    document.getElementById("qr-${p.uid}").value = "";
                                                    document.getElementById("qr-${p.uid}").focus();
                                                }
                                            });
                                        </script>
                                    </b>
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    <a style="width: 120px;" class="btn btn-danger" onclick="location.href = '/TABLE2/WMS340/displayDest?sb=dest&wh=${whcod}'">
                                        <i class="fa fa-chevron-circle-left" style="font-size:20px;"></i> Back</a>

                                    <a class="btn btn btn-outline btn-success" id="received" name="received" onclick="submitForms1${p.uid}();">
                                        <i class="fa fa-check-circle-o" style="font-size:20px;"></i> Received</a>

                                    <a class="btn btn btn-outline btn-danger" id="cancelled" name="cancelled" onclick="SC01${p.uid}();">
                                        <i class="fa fa-times-circle-o" style="font-size:20px;"></i> Rejected</a>
                                </td>
                            </tr>
                        </table>
                        <script>
                            function checkAll${p.uid}(ele) {
                                var checkboxes = document.getElementsByTagName('input');
                                if (ele.checked) {
                                    for (var i = 0; i < checkboxes.length; i++) {
                                        if (checkboxes[i].name == 'selectCk-${p.uid}') {
                                            checkboxes[i].checked = true;
                                        }
                                    }
                                } else {
                                    for (var i = 0; i < checkboxes.length; i++) {
                                        console.log(i)
                                        if (checkboxes[i].name == 'selectCk-${p.uid}') {
                                            checkboxes[i].checked = false;
                                        }
                                    }
                                }
                            }
                        </script>
                        <script>
                            SC01${p.uid} = function () {
                                var checkboxes = document.getElementsByTagName('input');
                                var nodataSelect = false;

                                for (var i = 0; i < checkboxes.length; i++) {
                                    if (checkboxes[i].name === 'selectCk-${p.uid}' && checkboxes[i].name !== 'selectAll') {
                                        if (checkboxes[i].checked === true) {
                                            nodataSelect = true;
                                        }
                                    }
                                }

                                if (nodataSelect) {
                                    showWH('myModal-${p.uid}', 'span-${p.uid}');
                                } else {
                                    document.getElementById('myModal-4').style.display = 'block';
                                }
                            };

                            SC02${p.uid} = function () {
                                var checkboxes = document.getElementsByTagName('input');
                                var nodataSelect = false;
                                var isNotStatus9 = false;

                                for (var i = 0; i < checkboxes.length; i++) {
                                    if (checkboxes[i].name === 'selectCk-${p.uid}' && checkboxes[i].name !== 'selectAll') {
                                        if (checkboxes[i].checked === true) {
                                            nodataSelect = true;
                                            if (checkboxes[i].value.toString().split("-")[5] !== "5") {
                                                isNotStatus9 = true;
                                            }
                                        }
                                    }
                                }

                                if (nodataSelect) {
                                    if (isNotStatus9) {
                                        document.getElementById('myModal-5').style.display = 'block';
                                    } else {
                                        showWH('myModal-${p.uid}-1', 'span-${p.uid}-1');
                                    }
                                } else {
                                    document.getElementById('myModal-4').style.display = 'block';
                                }
                            };

                            SC03${p.uid} = function () {
                                var checkboxes = document.getElementsByTagName('input');
                                var nodataSelect = false;

                                for (var i = 0; i < checkboxes.length; i++) {
                                    if (checkboxes[i].name === 'selectCk-${p.uid}' && checkboxes[i].name !== 'selectAll') {
                                        if (checkboxes[i].checked === true) {
                                            nodataSelect = true;
                                        }
                                    }
                                }

                                if (nodataSelect) {
                                    showWH('myModal-${p.uid}-2', 'span-${p.uid}-2');
                                } else {
                                    document.getElementById('myModal-4').style.display = 'block';
                                }
                            };

                            submitForms1${p.uid} = function () {
                                var checkboxes = document.getElementsByTagName('input');
                                var nodataSelect = false;

                                for (var i = 0; i < checkboxes.length; i++) {
                                    if (checkboxes[i].name === 'selectCk-${p.uid}' && checkboxes[i].name !== 'selectAll') {
                                        if (checkboxes[i].checked === true) {
                                            nodataSelect = true;
                                        }
                                    }
                                }

                                if (nodataSelect) {
                                    document.getElementById("detail-${p.uid}").action = "received";
                                    document.getElementById("detail-${p.uid}").submit();
                                } else {
                                    document.getElementById('myModal-4').style.display = 'block';
                                }
                            };

                            submitForms2${p.uid} = function () {
                                var remark = document.getElementById("remarkPre-${p.uid}");
                                var rmcheck = document.getElementById("rmcheck-${p.uid}");
                                if (remark.value === "") {
                                    remark.style.cssText = "background-color: #ffdbdb; resize: none;";
                                    rmcheck.innerHTML = "Please fill Remark for Cancel!";
                                } else {
                                    remark.style.cssText = "background-color: white; resize: none;";
                                    rmcheck.innerHTML = "";
                                    document.getElementById("detail-${p.uid}").action = "canceled";
                                    document.getElementById("detail-${p.uid}").submit();
                                }
                            };

                            submitForms3${p.uid} = function () {
                                var ship = document.getElementById("ship-${p.uid}");
                                var datecheck = document.getElementById("datecheck-${p.uid}");
                                if (ship.value === "") {
                                    ship.style.cssText = "background-color: #ffdbdb;";
                                    datecheck.innerHTML = "Please choose Shipment Date!";
                                } else {
                                    ship.style.cssText = "background-color: white;";
                                    datecheck.innerHTML = "";
                                    document.getElementById("detail-${p.uid}").action = "runno";
                                    document.getElementById("detail-${p.uid}").submit();
                                }
                            };

                            submitForms4${p.uid} = function () {
                                document.getElementById("detail-${p.uid}").action = "print";
                                document.getElementById("detail-${p.uid}").target = "_blank";
                                document.getElementById("detail-${p.uid}").submit();
                            };
                        </script>
                        <SCRIPT language="javascript">
                            function QR${p.uid}() {
                                var qr = document.getElementById("qr-${p.uid}");
                                qr = qr.value.toString().trim();
                                qr = qr.split("|");
                                var checkboxes = document.getElementsByTagName('input');
                                for (var i = 0; i < checkboxes.length; i++) {
                                    if (checkboxes[i].id === 'selectCk-' + qr[0]) {
                                        if (checkboxes[i].checked === true) {
                                            checkboxes[i].checked = false;
                                        } else if (checkboxes[i].checked === false) {
                                            checkboxes[i].checked = true;
                                        }
                                    }
                                }

                                document.getElementById("qr-${p.uid}").value = "";
                            }
                        </SCRIPT>    
                        <!-- The Modal -->
                        <div id="myModal-${p.uid}" class="modal">
                            <!-- Modal content -->
                            <div class="modal-content">
                                <span id="span-${p.uid}" class="close">&times;</span>
                                <p><b><font size="4">Remark for Reject : </font></b></p>
                                <br>
                                <table width="100%">
                                    <tr>
                                        <td align="center">
                                            <p id="rmcheck-${p.uid}" style="color: red;"></p>
                                            <textarea name="remarkPre-${p.uid}" id="remarkPre-${p.uid}" cols="80" rows="5" style="resize: none;" onkeyup="document.getElementById('remark-${p.uid}').value = this.value;"></textarea>
                                            <!--<input type="text" name="remark" style="width: 500px; height: 150px;">-->
                                            <br>
                                            <br>
                                            <a class="btn btn btn-outline btn-danger" onclick="document.getElementById('myModal-${p.uid}').style.display = 'none';">
                                                <i class="fa fa-times-circle-o" style="font-size:20px;"></i> Cancel</a>  
                                            &nbsp;
                                            <a class="btn btn btn-outline btn-success" onclick="submitForms2${p.uid}();">
                                                <i class="fa fa-check-circle-o" style="font-size:20px;"></i> Confirm</a>                      
                                        </td>
                                    </tr>
                                </table>
                            </div>

                        </div>
                        <!-- The Modal -->
                        <div id="myModal-${p.uid}-1" class="modal">
                            <!-- Modal content -->
                            <div class="modal-content">
                                <span id="span-${p.uid}-1" class="close">&times;</span>
                                <p><b><font size="4"></font></b></p>
                                <br>
                                <table width="100%">
                                    <tr>
                                        <td align="center">
                                            <br>
                                            <br>
                                            <p id="datecheck-${p.uid}" style="color: red;"></p>
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <b><font size="4">Shipment Date : </font></b>
                                            <input type="date" name="ship-${p.uid}" id="ship-${p.uid}" onchange="document.getElementById('shdt-${p.uid}').value = this.value;">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <br>
                                            <br>
                                            <a class="btn btn btn-outline btn-danger" onclick="document.getElementById('myModal-${p.uid}-1').style.display = 'none';">
                                                <i class="fa fa-times-circle-o" style="font-size:20px;"></i> Cancel</a>  
                                            &nbsp;
                                            <a class="btn btn btn-outline btn-success" onclick="submitForms3${p.uid}();">
                                                <i class="fa fa-check-circle-o" style="font-size:20px;"></i> Confirm</a>                      
                                        </td>
                                    </tr>
                                </table>
                            </div>

                        </div>
                        <!-- The Modal -->
                        <div id="myModal-${p.uid}-2" class="modal">
                            <!-- Modal content -->
                            <div class="modal-content">
                                <span id="span-${p.uid}-2" class="close">&times;</span>
                                <p><b><font size="4"></font></b></p>
                                <br>
                                <table width="100%">
                                    <tr>
                                        <td align="center">
                                            <br>
                                            <br>
                                            <p id="datecheck-${p.uid}" style="color: red;"></p>
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <b><font size="4">Start From : </font></b>
                                            <input type="number" name="Start-${p.uid}" id="Start-${p.uid}" value="1" min="1" max="114" onchange="document.getElementById('StartFrom-${p.uid}').value = this.value;">
                                            <a onclick="document.getElementById('myModal-${p.uid}-2p').style.display = 'block';" style="cursor: pointer;"><i class="fa fa-th" style="font-size:40px; padding-left: 10px;"></i></a>
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <br>
                                            <br>
                                            <a class="btn btn btn-outline btn-danger" onclick="document.getElementById('myModal-${p.uid}-2').style.display = 'none';">
                                                <i class="fa fa-times-circle-o" style="font-size:20px;"></i> Cancel</a>  
                                            &nbsp;
                                            <a class="btn btn btn-outline btn-success" onclick="submitForms4${p.uid}();">
                                                <i class="fa fa-check-circle-o" style="font-size:20px;"></i> Confirm</a>                      
                                        </td>
                                    </tr>
                                </table>
                            </div>

                        </div>
                        <div id="myModal-${p.uid}-2p" class="modal">
                            <!-- Modal content -->
                            <div class="modal-content" style=" height: 730px; width: 400px;">
                                <span id="span-${p.uid}-2p" class="close" onclick="document.getElementById('myModal-${p.uid}-2p').style.display = 'none';">&times;</span>
                                <p><b><font size="4"></font></b></p>
                                <br>
                                <table width="100%">
                                    <tr>
                                        <td align="center">
                                            <%
                                                int r = 0;
                                                for (int i = 1; i <= 57; i++) {
                                                    if (r == 3) {
                                            %>
                                            <br>
                                            <%
                                                    r = 0;
                                                }
                                            %>
                                            <a style=" width: 100px;" class="btn btn btn-outline btn-success" onclick="document.getElementById('Start-${p.uid}').value = '<%=i%>';
                                                    document.getElementById('StartFrom-${p.uid}').value = '<%=i%>';
                                                    document.getElementById('myModal-${p.uid}-2p').style.display = 'none';">
                                                <%=i%>
                                            </a>  
                                            <%
                                                    r++;
                                                }
                                            %>
                                        </td>
                                    </tr>
                                </table>
                            </div>

                        </div>
                        <div id="myModal-4" class="modal">
                            <!-- Modal content -->
                            <div class="modal-content" style=" height: 200px; width: 500px;">
                                <span id="span-4" class="close" onclick="document.getElementById('myModal-4').style.display = 'none';">&times;</span>
                                <p><b><font size="4"></font></b></p>
                                <table width="100%">
                                    <tr>
                                        <td align="center">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <b style="color: #00399b;"><font size="5">Please select the data !</font></b>
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <br>
                                            <br>
                                            <br>
                                            <a style=" width: 100px;" class="btn btn btn-outline btn-danger" onclick="document.getElementById('myModal-4').style.display = 'none';">
                                                OK
                                            </a>                 
                                        </td>
                                    </tr>
                                </table>
                            </div>

                        </div>
                        <div id="myModal-5" class="modal">
                            <!-- Modal content -->
                            <div class="modal-content" style=" height: 220px; width: 800px;">
                                <span id="span-5" class="close" onclick="document.getElementById('myModal-5').style.display = 'none';">&times;</span>
                                <p><b><font size="4"></font></b></p>
                                <table width="100%">
                                    <tr>
                                        <td align="center">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <b style="color: #00399b;"><font size="5">Only status is 5 : Received that can generate Running No. !</font></b>
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <br>
                                            <br>
                                            <br>
                                            <a style=" width: 100px;" class="btn btn btn-outline btn-danger" onclick="document.getElementById('myModal-5').style.display = 'none';">
                                                OK
                                            </a>                 
                                        </td>
                                    </tr>
                                </table>
                            </div>

                        </div>
                        <script>
                            $(document).ready(function () {
                                $('#showTable-${p.uid}').DataTable({
                                    "lengthMenu": [[-1, 10, 25, 50, 100], ["All", 10, 25, 50, 100]],
                                    "bSortClasses": false,
//                                    "order": [[3]],
                                    "columnDefs": [
//                                        {orderable: false, targets: [0]},
//                                        {orderable: false, targets: [1]},
//                                        {orderable: false, targets: [2]},
//                                        {orderable: false, targets: [3]},
//                                        {orderable: false, targets: [4]},
//                                        {orderable: false, targets: [5]},
//                                        {orderable: false, targets: [6]},
//                                        {orderable: false, targets: [7]},
//                                        {orderable: false, targets: [8]},
//                                        {orderable: false, targets: [9]},
//                                        {orderable: false, targets: [10]},
//                                        {orderable: false, targets: [11]},
//                                        {orderable: false, targets: [12]},
                                        {orderable: false, targets: [14]},
                                        {orderable: false, targets: [15]},
                                        {"width": "7%", "targets": 10},
                                        {"width": "15%", "targets": 15}
                                    ]
                                });

                                // Setup - add a text input to each footer cell
                                $('#showTable-${p.uid} tfoot th').not(":eq(14)").each(function () {
                                    var title = $(this).text();
                                    $(this).html('<input type="text" />');
                                });

                                // DataTable
                                var table = $('#showTable-${p.uid}').DataTable();

                                // Apply the search
                                table.columns().every(function () {
                                    var that = this;
                                    $('input', this.footer()).on('keyup change', function () {
                                        if (that.search() !== this.value) {
                                            that
                                                    .search(this.value)
                                                    .draw();
                                        }
                                    });
                                });
                            });</script>
                        <form id="detail-${p.uid}" method="post">
                            <input type="hidden" id="userid" name="userid">
                            <input type="hidden" name="matCtrl" value="${p.uid}">
                            <input type="hidden" name="mat" value="${mat}">
                            <input type="hidden" name="wh" value="${wh}">
                            <input type="hidden" name="dest" value="${dest}">
                            <input type="hidden" name="remark-${p.uid}" id="remark-${p.uid}">
                            <input type="hidden" name="shdt-${p.uid}" id="shdt-${p.uid}">
                            <input type="hidden" name="StartFrom-${p.uid}" id="StartFrom-${p.uid}" value="1">
                            <table id="showTable-${p.uid}" class="display" style="width:100%; border: 1px solid #ccc; border-radius: 4px;">
                                <thead>
                                    <tr>
                                        <th colspan="6" style="text-align: center; border-bottom: none;"></th>
                                        <th colspan="3" style="text-align: center; border-bottom-color: #ccc;">Package</th>
                                    </tr>
                                    <tr>
                                        <th style="text-align: center;">Group QR ID</th>
                                        <th style="text-align: center;">Material Code</th>
                                        <th style="text-align: center;">Description</th>
                                        <th style="text-align: center;">ID</th>
                                        <th style="text-align: center;">Quantity</th>
                                        <th style="text-align: center;">U/M</th>
                                        <th style="text-align: center;">BAG</th>
                                        <th style="text-align: center;">ROLL</th>
                                        <th style="text-align: center;">BOX</th>
                                        <th style="text-align: center;">M<sup>3</sup></th>
                                        <th style="text-align: center;">Status</th>
                                        <th style="text-align: center;">MVT</th>
                                        <th style="text-align: center;">Queue No.</th>
                                        <th style="text-align: center;">Shipment Date</th>
                                        <td style="text-align: center;">Check All &nbsp;
                                            <input style="width: 30px; height: 30px;" type="checkbox" name="selectAll" onchange="checkAll${p.uid}(this)">
                                        </td>
                                        <th style="text-align: center;">Running no.
                                            <a onclick="SC02${p.uid}();" style="cursor: pointer;"><i class="fa fa-list-ol" style="font-size:25px; padding-left: 10px"></i></a>
                                            <a onclick="SC03${p.uid}();" style="cursor: pointer;"><i class="fa fa-print" style="font-size:25px; padding-left: 10px"></i></a>
                                        </th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th>Material Code</th>
                                        <th>Description</th>
                                        <th>Group QR ID</th>
                                        <th>ID</th>
                                        <th>Quantity</th>
                                        <th>U/M</th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th>M<sup>3</sup></th>
                                        <th>Status</th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <td></td>
                                        <th></th>
                                    </tr>
                                </tfoot>
                                <tbody>
                                    <c:forEach items="${p.detList}" var="x">
                                        <tr>
                                            <td>${x.GID}</td>
                                            <td>${x.matc}</td>
                                            <td>${x.desc}</td>
                                            <td>${x.id}</td>
                                            <td align="right">${x.qty}&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                            <td align="center">${x.um}</td>
                                            <td align="center">${x.bag}</td> 
                                            <td align="center">${x.roll}</td>
                                            <td align="center">${x.box}</td>
                                            <td align="right">${x.m3}</td>
                                            <td>${x.sts}</td>
                                            <td align="center">${x.mvt} : ${x.mvtn}</td>
                                            <td align="center">${x.qno}</td>
                                            <td align="center">${x.shipmentDate}</td>
                                            <td align="center">
                                                ${x.ckBox}
                                            </td>
                                            <td align="center">${x.runno}</td>
                                        </tr>
                                    </c:forEach>
                                </tbody>
                            </table>
                            ${p.desc}
                        </form>
                    </c:forEach>
                    <!--**********************-->
                    <!--End Part 3-->
                    <!--</div>  end #wrapper-top--> 
                </div> 
                <br>
            </div> <!-- end .container-fluid -->
        </div> <!-- end #wrapper -->
    </body>
</html>