<%@ include file="../fragments/taglibs.jsp" %>
<%@ include file="../fragments/imports.jsp" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.sql.*;" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>CREATE MODE</title>
        <!-- css :: vendors -->
        <jsp:include page = "../fragments/css.jsp" />
        <!-- additional custom :: my-style -->
        <link rel="stylesheet" href="../resources/assets/styles/myStyles.css">

        <!-- js :: vendors -->
        <jsp:include page = "../fragments/script.jsp" />
        <!-- additional custom :: my-script -->
        <script src="../resources/assets/scripts/myScripts.js" async></script>
        <script src="../resources/assets/scripts/toggleLoad.js" async></script>
        <script type="text/javascript"> /* add something code to your script */</script>
        ${sendMessage}
        <script>
            function Uppercase() {
                var x = document.getElementById("col");
                x.value = x.value.toUpperCase();
                var y = document.getElementById("zone");
                y.value = y.value.toUpperCase();
            }
        </script>
        <script>
            function isNumberKey(evt) {
                var charCode = (evt.which) ? evt.which : event.keyCode
                if (charCode > 31 && (charCode < 48 || charCode > 57))
                    return false;
                return true;
            }
        </script>
        <script>
            function isAlphabetKey(evt) {
                var charCode = (evt.which) ? evt.which : event.keyCode
                if ((charCode < 65 || charCode > 90) && (charCode < 97 || charCode > 122))
                    return false;
                return true;
            }
        </script>
        <script>
            var show1 = function () {
                $('#myModal1').modal('show');
            };

            function validateForm() {
                var rkno = document.forms["frm"]["rkno"].value;
                var col = document.forms["frm"]["col"].value;
                var row = document.forms["frm"]["row"].value;

                if (rkno.trim() === "") {
                    document.forms["frm"]["rkno"].value = "00";
                }

                if (col.trim() === "") {
                    window.setTimeout(show1, 0);
                    document.getElementById("col").style.cssText = "border: 2px solid #ff9999";
                    return false;
                } else {
                    document.getElementById("col").style.cssText = "border: 1px solid #ccc";
                }

                if (row.trim() === "") {
                    window.setTimeout(show1, 0);
                    document.getElementById("row").style.cssText = "border: 2px solid #ff9999";
                    return false;
                } else {
                    document.getElementById("row").style.cssText = "border: 1px solid #ccc";
                }
            }
        </script>
        <style>
            input[type=text], input[type=password], input[type=number], select {
                width: 100%;
                padding: 5px 5px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }

            input[type=submit] {
                width: 100%;
                background-color: #2bd14a;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=submit]:hover {
                background-color: #45a049;
            }

            input[type=button] {
                width: 100%;
                background-color: #ef604a;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=button]:hover {
                background-color: #e51e00;
            }

            button[name=ok] {
                width: 25%;
                background-color: #008cff;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            button[name=ok]:hover {
                background-color: #008cff;
            }
        </style>
    </head>    
    <body onload="document.getElementById('userid').value = sessionStorage.getItem('uid');">
        <div id="wrapper">

            <!-- /#sidebar-wrapper -->
            <%@ include file="../fragments/sidebar.jsp" %>

            <!-- Page Content -->
            <!-- nav-head-custom -->
            <%@ include file="../fragments/nav_head.jsp" %>

            <div class="container-fluid">
                <!--                <div id="wrapper-top" align="left">
                <%-- PART 2 --%>
                <div class="row">
                    <div class="col-lg-12">
                        <div id="set-height" style="height:415px;margin-top:0px;">
                            <div id="sidebar-wrapper-top" class="">
                                <b class="page-header" style="padding-left:5px;font-size:18px;">
                <%-- <hr style="margin:0px;margin-top:10px;margin-bottom:10px;"> --%>
            </b>
        </div>
    </div>
</div>
</div> -->

                <%-- PART 3 --%>
                <form action="createG" method="POST" name="frm" onsubmit="return validateForm()">
                    <input type="hidden" id="userid" name="userid">
                    <table frame="box" width="100%" bordercolor="#e0e0e0">
                        <tr>
                            <th height="50" bgcolor="#f8f8f8">CREATE MODE</th>
                            <th height="50" bgcolor="#f8f8f8"></th>
                            <th height="50" bgcolor="#f8f8f8"></th>
                            <th height="50" bgcolor="#f8f8f8"></th>
                        </tr>
                        <br>
                        <tr>
                            <td width="15%"></td>
                            <td width="15%" align="left"><h4>Warehouse :</h4></td>
                            <td width="30%" align="left" style="padding-right: 150px;">
                                <select name="wh" style="background-color: #ddd">
                                    <option value="${wh}">${wh} : ${whn}</option>
                                </select></td></td>
                            <td width="30%"></td>
                        </tr>
                        <tr>
                            <td width="15%"></td>
                            <td width="15%" align="left"><h4>Area/Zone :</h4></td>
                            <td width="30%" align="left" style="padding-right: 300px;"><input type="text" id="zone" name="zone" maxlength="2" onkeyup="Uppercase()"></td>
                            <td width="30%"></td>
                        </tr>
                        <tr>
                            <td width="15%"></td>
                            <td width="15%" align="left"><h4>Rack no. :</h4></td>
                            <td width="30%" align="left" style="padding-right: 300px;"><input type="text" id="rkno" name="rkno" maxlength="2" onkeypress="return isNumberKey(event)"></td>
                            <td width="30%"></td>
                        </tr>
                        <tr>
                            <td width="15%"></td>
                            <td width="15%" align="left"><h4>Side :</h4></td>
                            <td width="30%" align="left" style="padding-right: 300px;">
                                <select name="side">
                                    <option value="L">L</option>
                                    <option value="R">R</option>
                                </select></td>
                            <td width="30%"></td>
                        </tr>
                        <tr>
                            <td width="15%"></td>
                            <td width="15%" align="left"><h4>Column :</h4></td>
                            <td width="30%" align="left" style="padding-right: 300px;"><input type="text" id="col" name="col" maxlength="1" onkeypress="return isAlphabetKey(event)" onkeyup="Uppercase()"></td>
                            <td width="30%"></td>
                        </tr>
                        <tr>
                            <td width="15%"></td>
                            <td width="15%" align="left"><h4>Row :</h4></td>
                            <td width="30%" align="left" style="padding-right: 300px;"><input type="text" id="row" name="row" maxlength="1" onkeypress="return isNumberKey(event)"></td>
                            <td width="30%"></td>
                        </tr>
                        <br>
                        <div align="center">
                            <tr>
                                <td></td>
                                <td></td>
                                <td width="30%" align="center"><input style="width: 100px;" type="button" value="Cancel" onclick="window.history.back()"/>
                                    <input style="width: 100px;" type="submit" value="Confirm" /></td>
                                <td></td>
                            </tr>
                        </div>
                    </table>
                    <center>
                        <div id="myModal" class="modal fade" role="dialog">
                            <div class="modal-dialog modal-sm"> <!-- ???????????? modal ?????????????? ???? xs, sm, md, lg -->
                                <div class="modal-md col-md-6" style="top: 150px; left:300px; background-color: #ddd; border: 1px solid #ccc; border-radius: 4px;">
                                    <div class="modal-header">
                                        <h4 class="modal-title" align="center">Duplicate Rack !</h4>
                                        <br>
                                        <button name="ok" type="button" onclick="window.history.back()">
                                            <font color = "white">OK</font>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- devbanban.com -->
                    </center>
                    <center>
                        <div id="myModal1" class="modal fade" role="dialog">
                            <div class="modal-dialog modal-sm"> <!-- ???????????? modal ?????????????? ???? xs, sm, md, lg -->
                                <div class="modal-md col-md-6" style="top: 150px; left:300px; background-color: #ddd; border: 1px solid #ccc; border-radius: 4px;">
                                    <div class="modal-header">
                                        <h4 class="modal-title" align="center">Please fill in the blanks !</h4>
                                        <br>
                                        <button name="ok" type="button" class="close" data-dismiss="modal">
                                            <font color = "white">OK</font>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- devbanban.com -->
                    </center>
                    <center>
                        <div id="myModal2" class="modal fade" role="dialog">
                            <div class="modal-dialog modal-sm"> <!-- ???????????? modal ?????????????? ???? xs, sm, md, lg -->
                                <div class="modal-md col-md-6" style="top: 150px; left:300px; background-color: #ddd; border: 1px solid #ccc; border-radius: 4px;">
                                    <div class="modal-header">
                                        <h4 class="modal-title" align="center">Create Failed !</h4>
                                        <br>
                                        <button name="ok" type="button" onclick="window.location.href = '/TABLE2/WMS009/createG?wh=${wh}'">
                                            <font color = "white">OK</font>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- devbanban.com -->
                    </center>
                </form>
                <!--End Part 3-->
                <!--</div>  end #wrapper-top -->
            </div> <!-- end .container-fluid -->
        </div> <!-- end #wrapper -->


    </body>

</html>