<%@ include file="../fragments/taglibs.jsp" %>
<%@ include file="../fragments/imports.jsp" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.sql.*;" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>DISPLAY MODE</title>
        <!-- css :: vendors -->
        <jsp:include page = "../fragments/css.jsp" />
        <!-- additional custom :: my-style -->
        <link rel="stylesheet" href="../resources/assets/styles/myStyles.css">

        <!-- js :: vendors -->
        <jsp:include page = "../fragments/script.jsp" />
        <!-- additional custom :: my-script -->
        <script src="../resources/assets/scripts/myScripts.js" async></script>
        <script src="../resources/assets/scripts/toggleLoad.js" async></script>
        <script type="text/javascript"> /* add something code to your script */</script>
        <style>
            input[type=text], input[type=password], input[type=number], select {
                width: 100%;
                padding: 5px 5px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }

            input[type=submit] {
                width: 100%;
                background-color: #2bd14a;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=submit]:hover {
                background-color: #45a049;
            }

            input[type=button] {
                width: 100%;
                background-color: #ef604a;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=button]:hover {
                background-color: #e51e00;
            }

            button[name=ok] {
                width: 25%;
                background-color: #008cff;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            button[name=ok]:hover {
                background-color: #008cff;
            }
        </style>
    </head>    
    <body >
        <div id="wrapper">

            <!-- /#sidebar-wrapper -->
            <%@ include file="../fragments/sidebar.jsp" %>

            <!-- Page Content -->
            <!-- nav-head-custom -->
            <%@ include file="../fragments/nav_head.jsp" %>

            <div class="container-fluid">
                <!--                <div id="wrapper-top" align="left">
                <%-- PART 2 --%>
                <div class="row">
                    <div class="col-lg-12">
                        <div id="set-height" style="height:415px;margin-top:0px;">
                            <div id="sidebar-wrapper-top" class="">
                                <b class="page-header" style="padding-left:5px;font-size:18px;">
                <%-- <hr style="margin:0px;margin-top:10px;margin-bottom:10px;"> --%>
            </b>
        </div>
    </div>
</div>
</div> -->

                <%-- PART 3 --%>
                <div style="border: 2px solid #ccc; border-radius: 5px; padding: 20px;">
                    <form>
                        <table frame="box" width="100%" bordercolor="#e0e0e0">
                            <tr>
                                <th height="50" bgcolor="#f8f8f8"></th>
                                <th height="50" bgcolor="#f8f8f8">DISPLAY MODE</th>
                                <th height="50" bgcolor="#f8f8f8"></th>
                                <th height="50" bgcolor="#f8f8f8"></th>
                                <th height="50" bgcolor="#f8f8f8"></th>
                                <th height="50" bgcolor="#f8f8f8"></th>
                            </tr>
                            <br>
                            <tr>
                                <td width="5%" align="left">
                                <td width="15%" align="left"><h4>Material Code :</h4></td>
                                <td width="30%" align="left" style="padding-right: 200px;"><input type="text" id="materialCode" name="materialCode" value="${MatCode}" readonly></td>
                                <td width="5%" align="left">
                                <td width="15%" align="left"><h4>Material Control :</h4></td>
                                <td width="30%" align="left" style="padding-right: 200px;"><input type="text" id="matCtrl" name="matCtrl" value="${MatCtrl}" readonly></td>
                            </tr>
                            <tr>
                                <td width="5%" align="left">
                                <td width="15%" align="left"><h4>Description :</h4></td>
                                <td width="30%" align="left" style="padding-right: 20px;"><input type="text" id="description" name="description" maxlength="40" value="${Desc}" readonly></td>
                                <td width="5%" align="left">
                                <td width="15%" align="left"><h4>Material Ctrl Desc. :</h4></td>
                                <td width="30%" align="left" style="padding-right: 20px;"><input type="text" id="MatCtrlDesc" name="MatCtrlDesc" maxlength="50" value="${MatCtrlDesc}" readonly></td>
                            </tr>
                            <tr>
                                <td width="5%" align="left">
                                <td width="15%" align="left"><h4>Plant :</h4></td>
                                <td width="30%" align="left" style="padding-right: 200px;"><input type="text" id="plant" name="plant" maxlength="4" value="${Plant}" readonly></td>
                                <td width="5%" align="left">
                                <td width="15%" align="left"><h4>P Group :</h4></td>
                                <td width="30%" align="left" style="padding-right: 200px;"><input type="text" id="Pgroup" name="Pgroup" maxlength="3" value="${Pgroup}" readonly></td>
                            </tr>
                            <tr>
                                <td width="5%" align="left">
                                <td width="15%" align="left"><h4>V type :</h4></td>
                                <td width="30%" align="left" style="padding-right: 200px;"><input type="text" id="Vtype" name="Vtype" maxlength="2" value="${Vtype}" readonly></td>
                                <td width="5%" align="left">
                                <td width="15%" align="left"><h4>P Group Name :</h4></td>
                                <td width="30%" align="left" style="padding-right: 20px;"><input type="text" id="PgroupName" name="PgroupName" maxlength="25" value="${PgroupName}" readonly></td>
                            </tr>
                            <tr>
                                <td width="5%" align="left">
                                <td width="15%" align="left"><h4>M type :</h4></td>
                                <td width="30%" align="left" style="padding-right: 200px;"><input type="text" id="Mtype" name="Mtype" maxlength="4" value="${Mtype}" readonly></td>
                                <td width="5%" align="left">
                                <td width="15%" align="left"><h4>Location :</h4></td>
                                <td width="30%" align="left" style="padding-right: 200px;"><input type="text" id="location" name="location" maxlength="4" value="${Location}" readonly></td>
                            </tr>
                            <tr>
                                <td width="5%" align="left">
                                <td width="15%" align="left"><h4>Std U/M :</h4></td>
                                <td width="30%" align="left" style="padding-right: 200px;"><input type="text" id="um" name="um" maxlength="3" value="${UM}" readonly></td>
                                <td width="5%" align="left">
                                <td width="15%" align="left">
                                <td width="30%" align="left">
                            </tr>
                            <tr>
                                <td width="5%" align="left">
                                <td width="15%" align="left"><h4>M Group :</h4></td>
                                <td width="30%" align="left" style="padding-right: 200px;"><input type="text" id="Mgroup" name="Mgroup" maxlength="4" value="${Mgroup}" readonly></td>
                                <td width="5%" align="left">
                                <td width="15%" align="left">
                                <td width="30%" align="left">
                            </tr>
                            <tr>
                                <td width="5%" align="left">
                                <td width="15%" align="left"><h4>M Group Desc. :</h4></td>
                                <td width="30%" align="left" style="padding-right: 20px;"><input type="text" id="MgroupDesc" name="MgroupDesc" maxlength="60" value="${MgroupDesc}" readonly></td>
                                <td width="5%" align="left">
                                <td width="15%" align="left">
                                <td width="30%" align="left"><input style="width: 100px;" type="button" value="Back" onclick="window.history.back()"/></td>
                            </tr>
                        </table>
                    </form>
                </div>
                <!--End Part 3-->
                <!--</div>  end #wrapper-top -->
            </div> <!-- end .container-fluid -->
        </div> <!-- end #wrapper -->


    </body>

</html>