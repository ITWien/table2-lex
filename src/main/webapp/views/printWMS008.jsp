<%@ include file="../fragments/taglibs.jsp" %>
<%@ include file="../fragments/imports.jsp" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.sql.*,com.twc.wms.dao.*" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>PRINT MODE</title>
        <!-- css :: vendors -->
        <jsp:include page = "../fragments/css.jsp" />
        <!-- additional custom :: my-style -->
        <link rel="stylesheet" href="../resources/assets/styles/myStyles.css">

        <!-- js :: vendors -->
        <jsp:include page = "../fragments/script.jsp" />
        <!-- additional custom :: my-script -->
        <script src="../resources/assets/scripts/myScripts.js" async></script>
        <script src="../resources/assets/scripts/toggleLoad.js" async></script>
        <script type="text/javascript"> /* add something code to your script */</script>
        ${sendMessage}
        ${showPDF}
        <script>
            function Uppercase() {
                var x = document.getElementById("Scolumn");
                var y = document.getElementById("Fcolumn");
                x.value = x.value.toUpperCase();
                y.value = y.value.toUpperCase();
            }
        </script>
        <script>
            function isNumberKey(evt) {
                var charCode = (evt.which) ? evt.which : event.keyCode
                if (charCode > 31 && (charCode < 48 || charCode > 57))
                    return false;
                return true;
            }
        </script>
        <script>
            function isAlphabetKey(evt) {
                var charCode = (evt.which) ? evt.which : event.keyCode
                if ((charCode < 65 || charCode > 90) && (charCode < 97 || charCode > 122))
                    return false;
                return true;
            }
        </script>
        <script>
            var show1 = function () {
                $('#myModal1').modal('show');
            };

            function validateForm() {
                var Srkno = document.forms["frm"]["Srkno"].value;
                var Frkno = document.forms["frm"]["Frkno"].value;
                var Scolumn = document.forms["frm"]["Scolumn"].value;
                var Fcolumn = document.forms["frm"]["Fcolumn"].value;
                var Srow = document.forms["frm"]["Srow"].value;
                var Frow = document.forms["frm"]["Frow"].value;

                if (Srkno.trim() === "") {
                    window.setTimeout(show1, 0);
                    document.getElementById("Srkno").style.cssText = "border: 2px solid #ff9999";
                    return false;
                } else {
                    document.getElementById("Srkno").style.cssText = "border: 1px solid #ccc";
                }
                
                if (Frkno.trim() === "") {
                    window.setTimeout(show1, 0);
                    document.getElementById("Frkno").style.cssText = "border: 2px solid #ff9999";
                    return false;
                } else {
                    document.getElementById("Frkno").style.cssText = "border: 1px solid #ccc";
                }
                
                if (Scolumn.trim() === "") {
                    window.setTimeout(show1, 0);
                    document.getElementById("Scolumn").style.cssText = "border: 2px solid #ff9999";
                    return false;
                } else {
                    document.getElementById("Scolumn").style.cssText = "border: 1px solid #ccc";
                }
                
                if (Fcolumn.trim() === "") {
                    window.setTimeout(show1, 0);
                    document.getElementById("Fcolumn").style.cssText = "border: 2px solid #ff9999";
                    return false;
                } else {
                    document.getElementById("Fcolumn").style.cssText = "border: 1px solid #ccc";
                }
                
                if (Srow.trim() === "") {
                    window.setTimeout(show1, 0);
                    document.getElementById("Srow").style.cssText = "border: 2px solid #ff9999";
                    return false;
                } else {
                    document.getElementById("Srow").style.cssText = "border: 1px solid #ccc";
                }
                
                if (Frow.trim() === "") {
                    window.setTimeout(show1, 0);
                    document.getElementById("Frow").style.cssText = "border: 2px solid #ff9999";
                    return false;
                } else {
                    document.getElementById("Frow").style.cssText = "border: 1px solid #ccc";
                }

            }
        </script>
        <style>
            input[type=text], input[type=password], input[type=number], select {
                width: 100%;
                padding: 5px 5px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }

            button[type=submit] {
                width: 100%;
                background-color: #2bd14a;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            button[type=submit]:hover {
                background-color: #45a049;
            }

            input[type=button] {
                width: 100%;
                background-color: #ef604a;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=button]:hover {
                background-color: #e51e00;
            }

            button[name=ok] {
                width: 25%;
                background-color: #008cff;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            button[name=ok]:hover {
                background-color: #008cff;
            }
        </style>
    </head>    
    <body >
        <div id="wrapper">

            <!-- /#sidebar-wrapper -->
            <%@ include file="../fragments/sidebar.jsp" %>

            <!-- Page Content -->
            <!-- nav-head-custom -->
            <%@ include file="../fragments/nav_head.jsp" %>

            <div class="container-fluid">
                <!--                <div id="wrapper-top" align="left">
                <%-- PART 2 --%>
                <div class="row">
                    <div class="col-lg-12">
                        <div id="set-height" style="height:415px;margin-top:0px;">
                            <div id="sidebar-wrapper-top" class="">
                                <b class="page-header" style="padding-left:5px;font-size:18px;">
                <%-- <hr style="margin:0px;margin-top:10px;margin-bottom:10px;"> --%>
            </b>
        </div>
    </div>
</div>
</div> -->

                <%-- PART 3 --%>
                <form action="print" method="POST" name="frm" onsubmit="return validateForm()">
                    <table frame="box" width="100%" bordercolor="#e0e0e0">
                        <tr>
                            <th height="50" bgcolor="#f8f8f8">PRINT MODE</th>
                            <th height="50" bgcolor="#f8f8f8"></th>
                            <th height="50" bgcolor="#f8f8f8">From</th>
                            <th height="50" bgcolor="#f8f8f8">To</th>
                            <th height="50" bgcolor="#f8f8f8"></th>
                        </tr>
                        <br>
                        <tr>
                            <td width="10%"></td>
                            <td width="20%" align="left"><h4>Warehouse :</h4></td>
                            <td width="20%" align="left"><select name="wh">
                                    <option value="${wh}" selected hidden>${wh} : ${whn}</option>
                                    <c:forEach items="${WHList}" var="p" varStatus="i">
                                        <option value="${p.code}">${p.code} : ${p.name}</option>
                                    </c:forEach>
                                </select></td>
                            <td width="20%"></td>
                            <td width="20%"></td>
                        </tr>
                        <tr>
                            <td width="10%"></td>
                            <td width="20%" align="left"><h4>Rack no. :</h4></td>
                            <td width="20%" align="left" style="padding-right: 150px;"><input type="text" id="Srkno" name="Srkno" maxlength="2" value="${Srkno}" onkeypress="return isNumberKey(event)"></td>
                            <td width="20%" align="left" style="padding-right: 150px;"><input type="text" id="Frkno" name="Frkno" maxlength="2" value="${Frkno}" onkeypress="return isNumberKey(event)"></td>
                            <td width="20%"></td>
                        </tr>
                        <tr>
                            <td width="10%"></td>
                            <td width="20%" align="left"><h4>Side(2) :</h4></td>
                            <td width="20%" align="left" style="padding-right: 150px;">
                                <select name="Sside">
                                    <option value="${Sside}" selected hidden>${Sside}</option>
                                    <option value=""></option>
                                    <option value="L">L</option>
                                </select>
                            </td>
                            <td width="20%" align="left" style="padding-right: 150px;">
                                <select name="Fside">
                                    <option value="${Fside}" selected hidden>${Fside}</option>
                                    <option value=""></option>
                                    <option value="R">R</option>
                                </select>
                            </td>
                            <td width="20%"></td>
                        </tr>
                        <tr>
                            <td width="10%"></td>
                            <td width="20%" align="left"><h4>Column :</h4></td>
                            <td width="20%" align="left" style="padding-right: 150px;"><input type="text" id="Scolumn" name="Scolumn" maxlength="1" value="${Scolumn}" onkeypress="return isAlphabetKey(event)" onkeyup="return Uppercase()"></td>
                            <td width="20%" align="left" style="padding-right: 150px;"><input type="text" id="Fcolumn" name="Fcolumn" maxlength="1" value="${Fcolumn}" onkeypress="return isAlphabetKey(event)" onkeyup="return Uppercase()"></td>
                            <td width="20%"></td>
                        </tr>
                        <tr>
                            <td width="10%"></td>
                            <td width="20%" align="left"><h4>Row :</h4></td>
                            <td width="20%" align="left" style="padding-right: 150px;"><input type="text" id="Srow" name="Srow" maxlength="1" value="${Srow}" onkeypress="return isNumberKey(event)"></td>
                            <td width="20%" align="left" style="padding-right: 150px;"><input type="text" id="Frow" name="Frow" maxlength="1" value="${Frow}" onkeypress="return isNumberKey(event)"></td>
                            <td width="20%"></td>
                        </tr>
                        <br>
                        <div align="center">
                            <tr>
                                <td></td>
                                <td></td>
                                <td width="20%" align="center"><input style="width: 100px;" type="button" value="Back" onclick="window.location.href = '/TABLE2/WMS008/display'"/>
                                    <button style="width: 100px;" type="submit"/><i class="fa fa-file-pdf-o"></i> Print</button>
                                </td>
                                <td></td>
                                <td></td>
                            </tr>
                        </div>
                    </table>
                    <center>
                        <div id="myModal1" class="modal fade" role="dialog">
                            <div class="modal-dialog modal-sm"> <!-- ???????????? modal ?????????????? ???? xs, sm, md, lg -->
                                <div class="modal-md col-md-6" style="top: 150px; left:300px; background-color: #ddd; border: 1px solid #ccc; border-radius: 4px;">
                                    <div class="modal-header">
                                        <h4 class="modal-title" align="center">Please fill in the blanks !</h4>
                                        <br>
                                        <button name="ok" type="button" class="close" data-dismiss="modal">
                                            <font color = "white">OK</font>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- devbanban.com -->
                    </center>
                </form>
                <!--End Part 3-->
                <!--</div>  end #wrapper-top -->
            </div> <!-- end .container-fluid -->
        </div> <!-- end #wrapper -->
    </body>

</html>