<%@ include file="../fragments/taglibs.jsp" %>
<%@ include file="../fragments/imports.jsp" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.sql.*;" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>EDIT MODE</title>
        <!-- css :: vendors -->
        <jsp:include page = "../fragments/css.jsp" />
        <!-- additional custom :: my-style -->
        <link rel="stylesheet" href="../resources/assets/styles/myStyles.css">

        <!-- js :: vendors -->
        <jsp:include page = "../fragments/script.jsp" />
        <!-- additional custom :: my-script -->
        <script src="../resources/assets/scripts/myScripts.js" async></script>
        <script src="../resources/assets/scripts/toggleLoad.js" async></script>
        <script type="text/javascript"> /* add something code to your script */</script>
        ${sendMessage}
        <script>
            var show1 = function () {
                $('#myModal1').modal('show');
            };

            function validateForm() {
                var pid = document.forms["frm"]["pid"].value;
                var name = document.forms["frm"]["name"].value;
                var type = document.forms["frm"]["type"].value;

                if (pid.trim() === "") {
                    window.setTimeout(show1, 0);
                    document.getElementById("pid").style.cssText = "border: 2px solid #ff9999";
                    return false;
                } else {
                    document.getElementById("pid").style.cssText = "border: 1px solid #ccc";
                }

                if (name.trim() === "") {
                    window.setTimeout(show1, 0);
                    document.getElementById("name").style.cssText = "border: 2px solid #ff9999";
                    return false;
                } else {
                    document.getElementById("name").style.cssText = "border: 1px solid #ccc";
                }

                if (type.trim() === "") {
                    window.setTimeout(show1, 0);
                    document.getElementById("type").style.cssText = "border: 2px solid #ff9999";
                    return false;
                } else {
                    document.getElementById("type").style.cssText = "border: 1px solid #ccc";
                }
            }
        </script>
        <style>
            input[type=text], input[type=password], input[type=number], select {
                width: 100%;
                padding: 5px 5px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }

            input[type=submit] {
                width: 100%;
                background-color: #2bd14a;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=submit]:hover {
                background-color: #45a049;
            }

            input[type=button] {
                width: 100%;
                background-color: #ef604a;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=button]:hover {
                background-color: #e51e00;
            }

            button[name=ok] {
                width: 25%;
                background-color: #008cff;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            button[name=ok]:hover {
                background-color: #008cff;
            }
        </style>
    </head>    
    <body >
        <div id="wrapper">

            <!-- /#sidebar-wrapper -->
            <%@ include file="../fragments/sidebar.jsp" %>

            <!-- Page Content -->
            <!-- nav-head-custom -->
            <%@ include file="../fragments/nav_head.jsp" %>

            <div class="container-fluid">
                <!--                <div id="wrapper-top" align="left">
                <%-- PART 2 --%>
                <div class="row">
                    <div class="col-lg-12">
                        <div id="set-height" style="height:415px;margin-top:0px;">
                            <div id="sidebar-wrapper-top" class="">
                                <b class="page-header" style="padding-left:5px;font-size:18px;">
                <%-- <hr style="margin:0px;margin-top:10px;margin-bottom:10px;"> --%>
            </b>
        </div>
    </div>
</div>
</div> -->

                <%-- PART 3 --%>
                <form action="edit" method="POST" name="frm" onsubmit="return validateForm()">
                    <table frame="box" width="100%" bordercolor="#e0e0e0">
                        <tr>
                            <th height="50" bgcolor="#f8f8f8">EDIT MODE</th>
                            <th height="50" bgcolor="#f8f8f8"></th>
                            <th height="50" bgcolor="#f8f8f8"></th>
                            <th height="50" bgcolor="#f8f8f8"></th>
                        </tr>
                        <br>
                        <tr>
                            <td width="15%"></td>
                            <td width="15%" align="left"><h4>Program ID :</h4></td>
                            <td width="30%" align="left" style="padding-right: 200px;"><input type="text" id="pid" name="pid" value="${pid}" style="background-color: #ddd" readonly></td>
                            <td width="30%"></td>
                        </tr>
                        <tr>
                            <td width="15%"></td>
                            <td width="15%" align="left"><h4>Name :</h4></td>
                            <td width="30%" align="left"><input type="text" id="name" name="name" maxlength="100" value="${name}"></td>
                            <td width="30%"></td>
                        </tr>
                        <tr>
                            <td width="15%"></td>
                            <td width="15%" align="left"><h4>System :</h4></td>
                            <td width="30%" align="left"><select name="sys">
                                    <option value="${sysid}" selected hidden>${sysid} : ${sysdesc}</option>
                                    <c:forEach items="${QPGSYSList}" var="p" varStatus="i">
                                        <option value="${p.code}">${p.code} : ${p.name}</option> 
                                    </c:forEach>
                                </select></td>
                            <td width="30%"></td>
                        </tr>
                        <tr>
                            <td width="15%"></td>
                            <td width="15%" align="left"><h4>Type :</h4></td>
                            <td width="30%" align="left"><select name="type">
                                    <option value="${typeid}" selected hidden>${typeid} : ${typedesc}</option>
                                    <c:forEach items="${QPGTYPEList}" var="p" varStatus="i">
                                        <option value="${p.code}">${p.code} : ${p.name}</option>
                                    </c:forEach>
                                </select></td>
                            <td width="30%"></td>
                        </tr>
                        <tr>
                            <td width="15%"></td>
                            <td width="15%" align="left"><h4>Responsibility :</h4></td>
                            <td width="30%" align="left"><select name="respon">
                                    <option value="${resuid}" selected hidden>${resuid} : ${resname}</option>
                                    <c:forEach items="${UserList}" var="p" varStatus="i">
                                        <option value="${p.uid}">${p.uid} : ${p.name}</option>
                                    </c:forEach>
                                </select></td>
                            <td width="30%"></td>
                        </tr>
                        <br>
                        <div align="center">
                            <tr>
                                <td></td>
                                <td></td>
                                <td width="30%" align="center"><input style="width: 100px;" type="button" value="Cancel" onclick="window.location.href = '/TABLE2/WMS011/display'"/>
                                    <input style="width: 100px;" type="submit" value="Confirm" /></td>
                                <td></td>
                            </tr>
                        </div>
                    </table>
                    <center>
                        <div id="myModal1" class="modal fade" role="dialog">
                            <div class="modal-dialog modal-sm"> <!-- ???????????? modal ?????????????? ???? xs, sm, md, lg -->
                                <div class="modal-md col-md-6" style="top: 150px; left:300px; background-color: #ddd; border: 1px solid #ccc; border-radius: 4px;">
                                    <div class="modal-header">
                                        <h4 class="modal-title" align="center">Please fill in the blanks !</h4>
                                        <br>
                                        <button name="ok" type="button" class="close" data-dismiss="modal">
                                            <font color = "white">OK</font>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- devbanban.com -->
                    </center>
                    <center>
                        <div id="myModal2" class="modal fade" role="dialog">
                            <div class="modal-dialog modal-sm"> <!-- ???????????? modal ?????????????? ???? xs, sm, md, lg -->
                                <div class="modal-md col-md-6" style="top: 150px; left:300px; background-color: #ddd; border: 1px solid #ccc; border-radius: 4px;">
                                    <div class="modal-header">
                                        <h4 class="modal-title" align="center">Edit Failed !</h4>
                                        <br>
                                        <button name="ok" type="button" onclick="window.location.href = '/TABLE2/WMS011/edit'">
                                            <font color = "white">OK</font>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- devbanban.com -->
                    </center>
                </form>
                <!--End Part 3-->
                <!--</div>  end #wrapper-top -->
            </div> <!-- end .container-fluid -->
        </div> <!-- end #wrapper -->


    </body>

</html>