<%@ include file="../fragments/taglibs.jsp" %>
<%@ include file="../fragments/imports.jsp" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.sql.*;" %>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <title>ISM</title>
        <!-- css :: vendors -->
        <jsp:include page="../fragments/css.jsp" />
        <!-- additional custom :: my-style -->
        <link rel="stylesheet" href="../resources/assets/styles/myStyles.css">
        <link rel="stylesheet" href="../resources/font/THsarabun/thsarabumnew.css">

        <!-- js :: vendors -->
        <jsp:include page="../fragments/script.jsp" />
        <!-- additional custom :: my-script -->
        <script src="../resources/assets/scripts/myScripts.js" async></script>
        <script src="../resources/assets/scripts/toggleLoad.js" async></script>
        <style>
            input[type=text],
            select {
                width: 100%;
                padding: 5px 5px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }

            input[type=date],
            input[type=number] {
                /*width: 100%;*/
                padding: 5px 5px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }

            input[type=submit] {
                width: 100%;
                background-color: #00399b;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=submit]:hover {
                background-color: #3973d6;
            }

            input[type=button] {
                width: 100%;
                background-color: #ef604a;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=button]:hover {
                background-color: #e51e00;
            }
        </style>
        <script>
            var tableOptions = {
                "paging": true,
                "lengthMenu": [[-1, 10, 25, 50, 100], ["All", 10, 25, 50, 100]],
                "bSortClasses": false,
                "ordering": true,
                "info": true,
                "fixedColumns": true,
                "order": [[0, "asc"]],
                "columnDefs": [
//                    {"targets": [10, 11], "className": "text-right"},
//                    {"targets": [0, 12, 13], "className": "text-center"},
                    {"targets": [6, 7], "orderable": false}
                ]
            };
        </script>
        <style>
            body {
                font-family: "Sarabun", sans-serif !important;
                /*font-family: 'THSarabunNew', sans-serif;*/
            }
            .displayTable {
                width: auto;
                border: 1px solid #ccc;
                border-radius: 4px;
            }
            .panel-primary {
                border-color: #7e33b7;
            }
            .panel-primary>.panel-heading {
                color: #fff;
                background-color: #7e33b7;
                border-color: #7e33b7;
            }
        </style>
        <script>
            $(document).ready(function () {
                $('#showTable').DataTable(tableOptions);

                // Setup - add a text input to each footer cell
                $('#showTable tfoot th').not(":eq(6),:eq(7)").each(function () {
                    var title = $(this).text();
                    $(this).html('<input type="text" />');
                });

                // DataTable
                var table = $('#showTable').DataTable();

                // Apply the search
                table.columns().every(function () {
                    var that = this;

                    $('input', this.footer()).on('keyup change', function () {
                        if (that.search() !== this.value) {
                            that.search(this.value).draw();
                        }
                    });
                });

                $('#toggle-srch-btn').click(function () {
                    var txt = $('#toggle-srch-text').html();
                    if (txt === 'Hide') {
                        $('#toggle-srch-text').html('Show');
                    } else if (txt === 'Show') {
                        $('#toggle-srch-text').html('Hide');
                    }
                    $('#search-section').toggle(300);
                });

                $('#customer').keyup(function () {
                    var cus = $(this).val();
                    $.ajax({
                        url: "/TABLE2/ShowDataTablesISM300?mode=getCustomerName"
                                + "&cus=" + cus
                    }).done(function (result) {
                        if (result !== '') {
                            $('#customerName').html(result);
                            $('#jobNo').prop('disabled', false);
                            $('#seqNo').prop('disabled', false);

                            $.ajax({
                                url: "/TABLE2/ShowDataTablesISM300?mode=getJobNoList"
                                        + "&cus=" + cus
                            }).done(function (result) {
                                $('#jobNo').html('');
                                for (var i = 0; i < result.length; i++) {
                                    $('#jobNo').append('<option value="' + result[i] + '">' + result[i] + '</option>');
                                }
                            });

                            $.ajax({
                                url: "/TABLE2/ShowDataTablesISM300?mode=getSeqNoList"
                                        + "&cus=" + cus
                            }).done(function (result) {
                                $('#seqNo').html('');
                                for (var i = 0; i < result.length; i++) {
                                    $('#seqNo').append('<option value="' + result[i] + '">' + String.fromCharCode(parseInt(result[i]) + 64) + '</option>');
                                }
                            });

                        } else {
                            $('#customerName').html('');
                            $('#jobNo').prop('disabled', true);
                            $('#seqNo').prop('disabled', true);
                        }
                    });
                });

                $('#matCtrl').keyup(function () {
                    $.ajax({
                        url: "/TABLE2/ShowDataTablesISM300?mode=getMatName"
                                + "&mat=" + $(this).val()
                    }).done(function (result) {
                        if (result !== '') {
                            $('#matName').html(result);
                        } else {
                            $('#matName').html('');
                        }
                    });
                });

                $('#searchBtn').click(function () {
                    var cus = $('#customer').val();
                    var mat = $('#matCtrl').val();
                    var jobNo = $('#jobNo').val();
                    var seqNo = $('#seqNo').val();

                    if (cus === '' || mat === '' || jobNo === null || seqNo === null) {
                        alertify.error("Please fill out search form.");
                    } else {
                        $.ajax({
                            url: "/TABLE2/ShowDataTablesISM300?mode=getDetailList"
                                    + "&cus=" + cus
                                    + "&mat=" + mat
                                    + "&jobNo=" + jobNo
                                    + "&seqNo=" + seqNo
                        }).done(function (result) {

                        });
                    }

                });
            });
        </script>
    </head>

    <body>
        <div id="wrapper">

            <!-- /#sidebar-wrapper -->
            <%@ include file="../fragments/sidebar.jsp" %>

            <!-- Page Content -->
            <!-- nav-head-custom -->
            <%@ include file="../fragments/nav_head.jsp" %>

            <div class="container-fluid">
                <!--show table-->
                <form action="../resources/manual/ISM300.pdf" target="_blank">
                    <table width="100%">
                        <tr>
                            <td width="94%" align="left">
                            </td>
                            <td width="6%" align="right"><input type="submit" value="Manual" /></td>
                        </tr>
                    </table>
                </form>
                <div class="container-fluid">
                    <div id="search-section">
                        <b>
                            <table>
                                <tr>
                                    <td width="130px;">
                                        Customer
                                    </td>
                                    <td width="20px;" align="center"> : </td>
                                    <td width="200px;">
                                        <input id="customer" class="form-control" type="text" onkeyup="javascript:this.value = this.value.toUpperCase();">
                                    </td>
                                    <td width="400px;">
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <span id="customerName" style="color:#777;font-size:16px;"></span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Mat-Ctrl
                                    </td>
                                    <td width="20px;" align="center"> : </td>
                                    <td>
                                        <input id="matCtrl" class="form-control" type="text" onkeyup="javascript:this.value = this.value.toUpperCase();">
                                    </td>
                                    <td>
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <span id="matName" style="color:#777;font-size:16px;"></span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Job no.
                                    </td>
                                    <td width="20px;" align="center"> : </td>
                                    <td>
                                        <select id="jobNo" class="form-control" type="text" disabled></select>
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>
                                        Sequence no.
                                    </td>
                                    <td width="20px;" align="center"> : </td>
                                    <td>
                                        <select id="seqNo" class="form-control" type="text" disabled></select>
                                    </td>
                                    <td width="200px;" align="center">
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <button id="searchBtn" style="width:60%;" class="btn btn-outline btn-primary">
                                            <b><i class="fa fa-search icon-size-sm"></i> Search</b>
                                        </button>
                                    </td>
                                </tr>
                            </table>
                        </b>
                    </div>

                    <div id="toggle-top" style=" text-align: right;">
                        <button type="button" id="printISM305Btn" style="width:15%; font-weight:bold;" class="btn btn-outline btn-warning"><i class="fa fa-print"></i> Print ISM305</button>
                        <button type="button" id="printISM306Btn" style="width:15%; font-weight:bold;" class="btn btn-outline btn-warning"><i class="fa fa-print"></i> Print ISM306</button>

                        <a id="toggle-srch-btn" style="width:15%;" class="btn btn-outline btn-default  custom-toggle-menu">
                            <span id="toggle-srch-text">Hide</span> Search
                        </a>
                    </div>
                    <hr style="margin:0px;margin-top:10px;margin-bottom:10px;">

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-primary" style="min-height: 485px;">
                                <table class="pull-right" style="position:absolute;top:5.5px;right:25px;font-size:14px;">
                                    <tr>
                                        <td>
                                            <button id="requestApprovalBtn" class="btn btn-success btn-outline btn-sm custom-icon-w-b">
                                                <b style="color:white">Request Approval</b>
                                            </button>
                                        </td>
                                    </tr>
                                </table>

                                <div class="panel-heading">
                                    <h4 style="margin:0px;">Materials Issue</h4>
                                </div>
                                <!-- /.panel-heading -->
                                <div class="panel-body">
                                    <div class="dataTable_wrapper">
                                        <table class="display displayTable" id="showTable">
                                            <thead>
                                                <tr>
                                                    <th style="border-bottom:0px;">Material</th>
                                                    <th style="border-bottom:0px;">Description</th>
                                                    <th style="border-bottom:0px;min-width:70px;">Onhand</th>
                                                    <th style="border-bottom:0px;min-width:70px;">T. Req.</th>
                                                    <th style="border-bottom:0px;min-width:70px;">T. Pick.</th>
                                                    <th style="border-bottom:0px;min-width:70px;">Remain.</th>
                                                    <th style="border-bottom:0px;min-width:70px;">Pick.</th>
                                                    <th style="border-bottom:0px;min-width:80px;">Action</th>
                                                </tr>
                                            </thead>

                                            <tfoot>
                                            <th style="border-left:0px;">Material</th>
                                            <th style="border-left:0px;">Description</th>
                                            <th style="border-left:0px;">Onhand</th>
                                            <th style="border-left:0px;">T. Req.</th>
                                            <th style="border-left:0px;">T. Pick.</th>
                                            <th style="border-left:0px;">Remain.</th>
                                            <th style="border-left:0px;"></th>
                                            <th style="border-left:0px;"></th>
                                            </tfoot>

                                            <tbody></tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br>
                </div> <!-- end .container-fluid -->
            </div> <!-- end #wrapper -->
    </body>

</html>