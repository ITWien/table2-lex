<%@ include file="../fragments/taglibs.jsp" %>
<%@ include file="../fragments/imports.jsp" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.sql.*;" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>WMS</title>
        <!-- css :: vendors -->
        <jsp:include page = "../fragments/css.jsp" />
        <!-- additional custom :: my-style -->
        <link rel="stylesheet" href="../resources/assets/styles/myStyles.css">

        <!-- js :: vendors -->
        <jsp:include page = "../fragments/script.jsp" />
        <!-- additional custom :: my-script -->
        <script src="../resources/assets/scripts/myScripts.js" async></script>
        <script src="../resources/assets/scripts/toggleLoad.js" async></script> 
        <style>
            input[type=text], select{
                width: 90%;
                padding: 5px 5px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }

            input[type=date], input[type=number]{
                /*width: 100%;*/
                padding: 5px 5px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }

            input[type=submit] {
                width: 100%;
                background-color: #00399b;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=submit]:hover {
                background-color: #3973d6;
            }

            input[type=button] {
                width: 100%;
                background-color: #ef604a;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=button]:hover {
                background-color: #e51e00;
            }
        </style>
        <script>
            $(document).ready(function () {
                $('#showTable').DataTable({
//                    fixedHeader: true,
                    "paging": false,
//                    "ordering": false,
                    "lengthMenu": [[-1, 10, 25, 50, 100], ["All", 10, 25, 50, 100]],
                    "bSortClasses": false,
//                    "order": [[0, "desc"]],
                    columnDefs: [
//                        {targets: 0, className: 'dt-body-center'},
//                        {targets: 1, className: 'dt-body-right'},
//                        {targets: 2, className: 'dt-body-center'},
//                        {targets: 3, className: 'dt-body-center'},
//                        {targets: 4, className: 'dt-body-center'},
                        {targets: 3, orderable: false}
                    ],
                    data: dataSet
                });

                $('#idcode').focus();
            }
            );
        </script>
        <style>
            body {font-family: Arial, Helvetica, sans-serif;}

            /* The Modal (background) */
            .modal {
                display: none; /* Hidden by default */
                position: fixed; /* Stay in place */
                z-index: 1; /* Sit on top */
                padding-top: 100px; /* Location of the box */
                left: 0;
                top: 0;
                width: 100%; /* Full width */
                height: 100%; /* Full height */
                overflow: auto; /* Enable scroll if needed */
                background-color: rgb(0,0,0); /* Fallback color */
                background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
            }

            /* Modal Content */
            .modal-content {
                background-color: #fefefe;
                margin: auto;
                padding: 20px;
                border: 1px solid #888;
                width: fit-content;
                height: fit-content;
            }

            /* The Close Button */
            .close {
                color: #aaaaaa;
                float: right;
                font-size: 28px;
                font-weight: bold;
            }

            .close:hover,
            .close:focus {
                color: #000;
                text-decoration: none;
                cursor: pointer;
            }
        </style>
        <script>
            function CatchKey(input, event) {

                var idcode = "";
                var type = "";
                if (event.keyCode === 13) {
                    event.preventDefault();

                    var data = input.value;
                    if (data !== null && data !== undefined) {
                        if (data !== '') {
                            if (data.indexOf('+') > -1) {
                                if (data.split('+').length === 5 && data.split('+')[5] !== "") {
                                    idcode = data;
                                    type = "QRMBARCODE";
                                }
                            } else {
                                var code = input.value.toString().split("|")[0];
                                idcode = code;
                                type = "QRMID";
                            }
                        }
                    }

                } else {
                    input.value = input.value.toUpperCase();
//                    var data = input.value;
//                    if (data !== null && data !== undefined) {
//                        if (data !== '') {
//                            if (data.indexOf('+') > -1) {
//                                if (data.split('+').length === 5 && data.split('+')[5] !== "") {
//                                    idcode = data;
//                                    type = "QRMBARCODE";
//                                }
//                            } else if (data.length === 17) {
//                                idcode = data;
//                                type = "QRMID";
//                            }
//                        }
//                    }
                }

                if (idcode !== "" && type !== "") {
                    idcode = idcode.trim();
                    $.ajax({
                        url: "/TABLE2/ShowDataTablesXMS017?mode=findMaster&idcode=" + idcode + "&type=" + type
                    }).done(function (result) {
                        if (result.QRMID !== undefined) {

                            $('#QRMPLANT').html(result.QRMPLANT);
                            $('#QRMCODE').html(result.QRMCODE);
                            $('#QRMDESC').html(result.QRMDESC);
                            $('#QRMQTY_QRMBUN').html(result.QRMQTY + '  ' + result.QRMBUN);
                            $('#QRMPACKTYPE').html(result.QRMPACKTYPE);
                            $('#remainQty').html(result.QRMQTY);
                            $('#remainBun').html(result.QRMBUN);
                            $('#whDet').html(result.QRMWHSE);
                            $('#plantDet').html(result.QRMPLANT);
                            $('#rmcodeDet').html(result.QRMCODE);
                            $('#rmdescDet').html(result.QRMDESC);
                            $('#storageDet').html(result.QRMSTORAGE);
                            $('#vtypeDet').html(result.QRMVAL);
                            $('#rmidDet').html(result.QRMID);
                            $('#idcode').val(result.QRMID);
                            $('#stsDet').html(result.QRMSTS);
                            $('#ponoDet').html(result.QRMPO);
                            $('#rateFacDet').html(result.QRMCVFQTY + ' ' + result.QRMAUN + ' = ' + result.QRMCVFAC + ' ' + result.QRMBUN);
                            $('#taxInvDet').html(result.QRMTAX);
                            $('#matCtrlDet').html(result.MATCTRL);
                            $('#packTypeDet').html(result.QRMPACKTYPE);
                            $('#rollDet').html(result.QRMROLL);
                            $('#lotNoDet').html(result.QRMLOTNO);
                            $('#qtyDet').html(result.QRMQTY);
                            $('#unitDet').html(result.QRMBUN);
                            $('#dynoDet').html(result.QRMDYNO);
                            $('#alQtyDet').html(result.QRMALQTY);
                            $('#alUnitDet').html(result.QRMAUN);
                            $('#qiStsDet').html(result.QRMQIS);
                            $('#pdDateDet').html(result.QRMPDDATE);
                            $('#wh2Det').html(result.QRMWHSE);
                            $('#rcDateDet').html(result.QRMRCDATE);
                            $('#zoneDet').html(result.QRMZONE);
                            $('#isDateDet').html(result.QRMISDATE);
                            $('#tranDateDet').html(result.QRMTFDATE);
                            $('#rackNoDet').html(result.QRMRACKNO);
                            $('#sideDet').html(result.QRMSIDE);
                            $('#parentDet').html(result.QRMPARENT);
                            $('#colDet').html(result.QRMCOL);
                            $('#rowDet').html(result.QRMROW);
                            $('#palleteDet').html(result.QRMPALLET);
                            $('#locationDet').html(result.QRMLOC);
                            $('#cdtDet').html(result.QRMCDT);
                            $('#edtDet').html(result.QRMEDT);
                            $('#userDet').html(result.QRMUSER);
                            $('#btnClear').show();
                            $('#btnDet').show();
                            $('#idcode').prop('disabled', true);
                            $('#sepDet').show();
                            $('#stdQty').val(result.QRMQTY);
                            $('#stdQty').focus();
                            var pack = result.QRMPACKTYPE;
                            $('#parentPackType').val(pack);
                            $('#sepPackType').val(pack);
                        } else {
                            $('#btnClear').hide();
                            $('#btnDet').hide();
                            $('#masDet').hide();
                            $('#sepDet').hide();
                        }
                    }).fail(function (jqXHR, textStatus, errorThrown) {
                        // needs to implement if it fails
                    });
                }
            }

            function MasDet() {
                $('#masDet').toggle(300);
            }

            function ClearData() {
//                location.reload();
                $('#idcode').val('');
                $('#idcode').prop('disabled', false);
                $('#idcode').focus();
                $('#btnClear').hide();
                $('#btnDet').hide();
                $('#masDet').hide();
                $('#sepDet').hide();
                $('#tableDet').hide();
                $('#stdQty').val('');
                $('#stdQtyMultiply').val('1');
                $('#stdQtyMultiply').prop('disabled', false);
                $('#btnSave').show();
                $('#btnSaveEdit').hide();
                $('#btnGenId').show();
                $('#btnPrePrint').hide();
                $('#btnSave2').prop('disabled', false);
                $('#btnSaveEdit2').prop('disabled', false);
                $('#stdQty').prop('disabled', false);
                $('#sepPackType').prop('disabled', false);
                dataSet = [];
                var table = $('#showTable').DataTable();
                table.clear().draw();
            }

            function calculateRemain() {
                var remainQty = $('#remainQty').html();
                var qty = $('#stdQty').val();
                var qtyMul = $('#stdQtyMultiply').val();
                if (qty !== '' && qtyMul !== '') {
                    //ready
                    if ((parseFloat(qty) * parseFloat(qtyMul)) <= parseFloat(remainQty)) {

                        if ((parseFloat(qty) * parseFloat(qtyMul)) !== 0) {
                            addQtyInline(parseFloat(qty), parseFloat(qtyMul), parseFloat(remainQty));
                            $('#tableDet').show();

                            $('#stdQty').val('');
                            $('#stdQtyMultiply').val('1');

                        } else {
                            alertify.error("ค่าต้องมากกว่าหรือเท่ากับ 0.001");
                        }

                    } else {
                        alertify.error("ไม่สามารถแบ่งเกิน จำนวนค่าคงเหลือหลังจากถูกแบ่ง");
                    }
                } else {
                    if (qty === '') {
                        $('#stdQty').focus();
                    } else if (qtyMul === '') {
                        $('#stdQtyMultiply').focus();
                    }
                }

            }

            function calculateRemainEdit() {
                var idx = sessionStorage.getItem("sessionIdx");
                var parentId = sessionStorage.getItem("sessionParentId");

                var minAf = getMinAfter(parentId);

                var remainQty = $('#remainQty').html();
                var qty = $('#stdQty').val();
                if (qty !== '') {
                    //ready
                    if (parseFloat(qty) <= (parseFloat(minAf) + parseFloat(dataSet[idx][5]))) {

                        if (parseFloat(qty) !== 0) {

                            dataSet[idx][5] = parseFloat(qty).toFixed(3);
                            dataSet[idx][8] = $('#sepPackType').val();

                            var remain = parseFloat(remainQty) - parseFloat(qty);
                            $('#remainQty').html(remain.toFixed(3));

                            reCalQty(parentId, false, 0);

                            var table = $('#showTable').DataTable();
                            table.clear().draw();

                            for (var i = 0; i < dataSet.length; i++) {
                                table.row.add([
                                    i + 1,
                                    dataSet[i][1],
                                    dataSet[i][2],
                                    dataSet[i][3],
                                    dataSet[i][4],
                                    dataSet[i][5],
                                    dataSet[i][6],
                                    dataSet[i][7],
                                    dataSet[i][8],
                                    dataSet[i][9],
                                    dataSet[i][10],
                                    dataSet[i][11]
                                ]).draw(false);
                            }

                            $('#stdQty').val('');
                            $('#stdQtyMultiply').prop('disabled', false);
                            toggleBtnSave();

                            alertify.success("แก้ไข! จำนวนที่ต้องการแบ่งแล้ว");

                        } else {
                            alertify.error("ค่าต้องมากกว่าหรือเท่ากับ 0.001");
                        }

                    } else {
                        alertify.error("ไม่สามารถแบ่งเกิน จำนวนค่าคงเหลือหลังจากถูกแบ่ง");
                    }
                } else {
                    if (qty === '') {
                        $('#stdQty').focus();
                    }
                }
            }

            function reCalQty(parentId, isHead, max) {
                var _array = [];

                for (var i = 0; i < dataSet.length; i++) {
                    if (dataSet[i][3] === parentId) {
                        _array.push(dataSet[i][4]);
                    }
                }

                var maxQty = Math.max.apply(Math, _array);
                if (isHead) {
                    maxQty = max;
                }

                for (var i = 0; i < dataSet.length; i++) {
                    if (dataSet[i][3] === parentId) {

                        dataSet[i][4] = parseFloat(maxQty).toFixed(3);
                        maxQty -= parseFloat(dataSet[i][5]);
                        dataSet[i][6] = parseFloat(maxQty).toFixed(3);
                    }
                }
            }
        </script>
        <script>
            var dataSet = [];

            function addQtyInline(qty, qtyMul, remainQty) {
                var table = $('#showTable').DataTable();

                var rem = remainQty;
                for (var i = 0; i < qtyMul; i++) {
                    dataSet.push([dataSet.length + 1,
                        $('#rmcodeDet').html(),
                        $('#rmdescDet').html(),
                        $('#rmidDet').html(),
                        rem.toFixed(3),
                        qty.toFixed(3),
                        (rem - qty).toFixed(3),
                        $('#parentPackType').val(),
                        $('#sepPackType').val(),
                        '<button type="button" onclick="editQtyInline(this)" class="btn btn-outline btn-warning editBtn"><i class="fa fa-edit"></i></button>\n' +
                                '<button type="button" onclick="removeQtyInline(this)" class="btn btn-outline btn-danger deleteBtn"><i class="fa fa-trash-o"></i></button>',
                        "",
                        ""
                    ]);

                    rem -= qty;
                }

                table.clear().draw();

                for (var i = 0; i < dataSet.length; i++) {
                    table.row.add([
                        i + 1,
                        dataSet[i][1],
                        dataSet[i][2],
                        dataSet[i][3],
                        dataSet[i][4],
                        dataSet[i][5],
                        dataSet[i][6],
                        dataSet[i][7],
                        dataSet[i][8],
                        dataSet[i][9],
                        dataSet[i][10],
                        dataSet[i][11]
                    ]).draw(false);
                }

                alertify.success("เพิ่ม! จำนวนที่ต้องการแบ่งแล้ว");

                var remain = remainQty - (qty * qtyMul);
                $('#remainQty').html(remain.toFixed(3));

                $('#idcode').prop('disabled', false);
                $('#idcode').val('');
                $('#idcode').focus();
            }

            function editQtyInline(ele) {

                var idx = $('#showTable').DataTable().row($(ele).parents('tr')).data()[0];
                var parentId = $('#showTable').DataTable().row($(ele).parents('tr')).data()[3];
                var qtyLine = $('#showTable').DataTable().row($(ele).parents('tr')).data()[5];
                var packLine = $('#showTable').DataTable().row($(ele).parents('tr')).data()[8];

                if (typeof (Storage) !== "undefined") {
                    sessionStorage.setItem("sessionIdx", idx - 1);
                    sessionStorage.setItem("sessionParentId", parentId);
                }

                var remainQty = $('#remainQty').html();
                var remain = parseFloat(remainQty) + parseFloat(qtyLine);
                $('#remainQty').html(remain.toFixed(3));

                $('#sepPackType').val(packLine);
                $('#stdQty').val(qtyLine);
                $('#stdQtyMultiply').prop('disabled', true);
                $('.editBtn').prop('disabled', true);
                $('.deleteBtn').prop('disabled', true);
                toggleBtnSave();
                $('#stdQty').focus();

            }

            function getMinAfter(parentId) {
                var _array = [];

                for (var i = 0; i < dataSet.length; i++) {
                    if (dataSet[i][3] === parentId) {
                        _array.push(dataSet[i][6]);
                    }
                }

                return Math.min.apply(Math, _array);
            }

            function getMaxBefore(parentId) {
                var _array = [];

                for (var i = 0; i < dataSet.length; i++) {
                    if (dataSet[i][3] === parentId) {
                        _array.push(dataSet[i][4]);
                    }
                }

                return Math.max.apply(Math, _array);
            }

            function removeQtyInline(ele) {

                var idx = $('#showTable').DataTable().row($(ele).parents('tr')).data()[0];
                var parentId = $('#showTable').DataTable().row($(ele).parents('tr')).data()[3];
                var qtyBf = $('#showTable').DataTable().row($(ele).parents('tr')).data()[4];
                var qtyLine = $('#showTable').DataTable().row($(ele).parents('tr')).data()[5];

                var isHead = false;
                var maxBf = getMaxBefore(parentId);
                if (parseFloat(qtyBf) === parseFloat(maxBf)) {
                    isHead = true;
                }

                dataSet.splice(idx - 1, 1);

                var remainQty = $('#remainQty').html();
                var remain = parseFloat(remainQty) + parseFloat(qtyLine);
                $('#remainQty').html(remain.toFixed(3));

                reCalQty(parentId, isHead, maxBf);

                var table = $('#showTable').DataTable();
                table.clear().draw();

                for (var i = 0; i < dataSet.length; i++) {
                    table.row.add([
                        i + 1,
                        dataSet[i][1],
                        dataSet[i][2],
                        dataSet[i][3],
                        dataSet[i][4],
                        dataSet[i][5],
                        dataSet[i][6],
                        dataSet[i][7],
                        dataSet[i][8],
                        dataSet[i][9],
                        dataSet[i][10],
                        dataSet[i][11]
                    ]).draw(false);
                }

                alertify.success("ลบ! จำนวนที่ต้องการแบ่งแล้ว");

                if (dataSet.length === 0) {
                    $('#tableDet').hide();
                }
            }

            function toggleBtnSave() {
                $('#btnSave').toggle();
                $('#btnSaveEdit').toggle();
            }

            function toggleBtnGenPrint() {
                $('#btnGenId').toggle();
                $('#btnPrePrint').toggle();
            }

            function generateId() {

                var dt = new Date();
                var time = dt.getHours() + ":" + dt.getMinutes() + ":" + dt.getSeconds();
                console.log('Start Time Gen ID : ' + time);

                var parentIdList = [];

                for (var i = 0; i < dataSet.length; i++) {
                    if (!parentIdList.includes(dataSet[i][3])) {
                        parentIdList.push(dataSet[i][3]);
                    }
                }

                var wh = $('#warehouseId').val();

                var lastGroup = $.ajax({
                    url: "/TABLE2/ShowDataTablesXMS017",
                    type: "GET",
                    data: {mode: 'getLastQNBSER', wh: wh, id: parentIdList[0]},
                    async: false
                }).responseJSON;

                var Obj = $.ajax({
                    url: "/TABLE2/ShowDataTablesXMS017",
                    type: "GET",
                    data: {mode: 'findMaster', idcode: parentIdList[0], type: "QRMID"},
                    async: false
                }).responseJSON;

                var matCtrl = $.ajax({
                    url: "/TABLE2/ShowDataTablesXMS017",
                    type: "GET",
                    data: {mode: 'getMatCtrl', code: Obj.QRMCODE},
                    async: false
                }).responseJSON;

                var uidH = sessionStorage.getItem('uid');
                if (uidH === null) {
                    uidH = '';
                }

                $.ajax({
                    url: "/TABLE2/ShowDataTablesXMS017",
                    type: "GET",
                    data: {mode: 'insertQSHEAD', gid: lastGroup, wh: wh, mat: matCtrl, uid: uidH},
                    async: false
                }).responseJSON;

                $.ajax({
                    url: "/TABLE2/ShowDataTablesXMS017",
                    type: "GET",
                    data: {mode: 'updateQNBSER', wh: wh, id: parentIdList[0]},
                    async: false
                }).responseJSON;

                for (var i = 0; i < parentIdList.length; i++) {

                    var checkLastQr = false;
                    var id = parentIdList[i];
                    var cdt = "";
                    var code = "";
                    var unit = "";
                    var remainQty = getMinAfter(parentIdList[i]);
                    var parentPackType = "";

                    var masterObj = $.ajax({
                        url: "/TABLE2/ShowDataTablesXMS017",
                        type: "GET",
                        data: {mode: 'findMaster', idcode: parentIdList[i], type: "QRMID"},
                        async: false
                    }).responseJSON;

                    code = masterObj.QRMCODE;
                    parentPackType = masterObj.QRMPACKTYPE;
                    cdt = masterObj.QRMCDT;
                    unit = masterObj.QRMBUN;

                    checkLastQr = $.ajax({
                        url: "/TABLE2/ShowDataTablesXMS017",
                        type: "GET",
                        data: {mode: 'checkLastQr', id: id},
                        async: false
                    }).responseJSON;

                    if (!checkLastQr) {
                        $.ajax({
                            url: "/TABLE2/ShowDataTablesXMS017",
                            type: "GET",
                            data: {mode: 'insertGenQr', id: id},
                            async: false
                        }).responseJSON;
                    }

                    var last = $.ajax({
                        url: "/TABLE2/ShowDataTablesXMS017",
                        type: "GET",
                        data: {mode: 'getGenQr', id: id},
                        async: false
                    }).responseJSON;

                    var lastRoll = $.ajax({
                        url: "/TABLE2/ShowDataTablesXMS017",
                        type: "GET",
                        data: {mode: 'getGenRoll', code: code},
                        async: false
                    }).responseJSON;

                    var date = $.ajax({
                        url: "/TABLE2/ShowDataTablesXMS017",
                        type: "GET",
                        data: {mode: 'getDate'},
                        async: false
                    }).responseJSON;

                    var dataSetById = [];
                    for (var j = 0; j < dataSet.length; j++) {
                        if (dataSet[j][3] === id) {
                            dataSetById.push(dataSet[j]);
                        }
                    }

                    $.ajax({
                        url: "/TABLE2/ShowDataTablesXMS017",
                        type: "GET",
                        data: {mode: 'updateGenQr', id: id, len: dataSetById.length},
                        async: false
                    }).responseJSON;

                    $.ajax({
                        url: "/TABLE2/ShowDataTablesXMS017",
                        type: "GET",
                        data: {mode: 'updateGenRoll', code: code, len: dataSetById.length},
                        async: false
                    }).responseJSON;

                    for (var j = 0; j < dataSetById.length; j++) {

                        var runno = (parseInt(last) + j).toString();
                        while (runno.length < 5) {
                            runno = '0' + runno;
                        }

                        var qrmid = id.substring(0, 6) + date + runno;
                        dataSetById[j][10] = qrmid;
                        dataSetById[j][11] = lastGroup;

                        var qrmroll = parseInt(lastRoll) + j;
                        var qrmqty = dataSetById[j][5];
                        var qrmpacktype = dataSetById[j][8];
                        var uid = sessionStorage.getItem('uid');
                        if (uid === null) {
                            uid = '';
                        }

                        $.ajax({
                            url: "/TABLE2/ShowDataTablesXMS017",
                            type: "GET",
                            data: {mode: 'insertQRMMAS', id: id, cdt: cdt, qrmroll: qrmroll, qrmqty: qrmqty, qrmpacktype: qrmpacktype, qrmid: qrmid, uid: uid},
                            async: false
                        }).responseJSON;

                        $.ajax({
                            url: "/TABLE2/ShowDataTablesXMS017",
                            type: "GET",
                            data: {mode: 'insertQRMTRA', id: id, cdt: cdt, qrmroll: qrmroll, qrmqty: qrmqty, qrmpacktype: qrmpacktype, qrmid: qrmid, uid: uid},
                            async: false
                        }).responseJSON;

                        $.ajax({
                            url: "/TABLE2/ShowDataTablesXMS017",
                            type: "GET",
                            data: {mode: 'insertQSDETAIL', wh: wh, lino: dataSetById[j][0], mat: dataSetById[j][1], gid: dataSetById[j][11], id: dataSetById[j][10], bfQty: dataSetById[j][4], afQty: dataSetById[j][6], qty: dataSetById[j][5], unit: unit, bfPack: dataSetById[j][7], afPack: dataSetById[j][8], uid: uid},
                            async: false
                        }).responseJSON;

                    }

                    $.ajax({
                        url: "/TABLE2/ShowDataTablesXMS017",
                        type: "GET",
                        data: {mode: 'insertParentQRMTRA', id: id, cdt: cdt, remainQty: remainQty, parentPackType: parentPackType, uid: uid},
                        async: false
                    }).responseJSON;

                    $.ajax({
                        url: "/TABLE2/ShowDataTablesXMS017",
                        type: "GET",
                        data: {mode: 'updateParentQRMMAS', id: id, cdt: cdt, remainQty: remainQty, parentPackType: parentPackType, uid: uid},
                        async: false
                    }).responseJSON;

                }

                var table = $('#showTable').DataTable();
                table.clear().draw();

                for (var i = 0; i < dataSet.length; i++) {
                    table.row.add([
                        i + 1,
                        dataSet[i][1],
                        dataSet[i][2],
                        dataSet[i][3],
                        dataSet[i][4],
                        dataSet[i][5],
                        dataSet[i][6],
                        dataSet[i][7],
                        dataSet[i][8],
                        dataSet[i][9],
                        dataSet[i][10],
                        dataSet[i][11]
                    ]).draw(false);
                }

                $('.editBtn').prop('disabled', true);
                $('.deleteBtn').prop('disabled', true);
                toggleBtnGenPrint();

                var dt2 = new Date();
                var time2 = dt2.getHours() + ":" + dt2.getMinutes() + ":" + dt2.getSeconds();
                console.log('Finish Time Gen ID : ' + time2);

                var QSHEADobj = $.ajax({
                    url: "/TABLE2/ShowDataTablesXMS017",
                    type: "GET",
                    data: {mode: 'findQSHEAD', gid: lastGroup},
                    async: false
                }).responseJSON;

                if (typeof (Storage) !== "undefined") {
                    sessionStorage.setItem("gidPrint", QSHEADobj.QRMID);
                    sessionStorage.setItem("trdtPrint", QSHEADobj.QRMCODE);
                    sessionStorage.setItem("matCtrlPrint", QSHEADobj.QRMVAL);

                    var pids = "";
                    for (var i = 0; i < parentIdList.length; i++) {
                        if (i === 0) {
                            pids += parentIdList[i];
                        } else {
                            pids += ":" + parentIdList[i];
                        }
                    }

                    sessionStorage.setItem("pidsPrint", pids);
                }
            }

            function prePrint() {
                var gidPrint = sessionStorage.getItem("gidPrint");
                var trdtPrint = sessionStorage.getItem("trdtPrint");
                var matCtrlPrint = sessionStorage.getItem("matCtrlPrint");
                var pidsPrint = sessionStorage.getItem("pidsPrint");

                print(gidPrint, trdtPrint, matCtrlPrint, pidsPrint);

            }
        </script>
    </head>    
    <body onload="document.getElementById('userid').value = sessionStorage.getItem('uid');">
        <div id="wrapper">

            <!-- /#sidebar-wrapper -->
            <%@ include file="../fragments/sidebar.jsp" %>

            <!-- Page Content -->
            <!-- nav-head-custom -->
            <%@ include file="../fragments/nav_head.jsp" %>

            <div class="container-fluid">
                <!--<div id="wrapper-top">-->
                <%-- PART 2 --%>
                <!--                    <div class="row">
                                        <div class="col-lg-12">
                                            <div id="set-height" style="height:415px;margin-top:0px;">
                                                <div id="sidebar-wrapper-top" class="">
                                                    <b class="page-header" style="padding-left:5px;font-size:18px;">
                <%-- <hr style="margin:0px;margin-top:10px;margin-bottom:10px;"> --%>
            </b>
        </div>
    </div>
</div>
</div> -->
                <%-- PART 3 --%>
                <br>
                <div style="border: 2px solid #ccc; border-radius: 5px; padding: 20px;">
                    <table style=" width: 100%;">
                        <tr>
                            <th style="text-align: center; width: 20%;">
                                <b style="color: #00399b; font-size: 20px;">Warehouse : ${wh} ${whn}</b> 
                                <input type="hidden" id="warehouseId" value="${wh}">
                            </th>
                            <th style="text-align: center; width: 15%;">
                                <b style="color: #00399b; font-size: 20px;">ID : </b> 
                            </th>
                            <th style="text-align: center; width: 40%;">
                                <input type="text" id="idcode" name="idcode" style=" width: 80%;" onkeyup="CatchKey(this, event);">
                            </th>
                            <th style="text-align: center; width: 10%;">
                                <a id="btnClear" class="btn btn btn-outline btn-danger" style=" width: fit-content; display: none;" onclick="ClearData();">
                                    <i class="fa fa-times"></i> ล้างข้อมูล
                                </a> 
                            </th>
                            <th style="text-align: center; width: 10%;">
                                <a id="btnDet" class="btn btn btn-outline btn-primary" style=" width: fit-content; display: none;" onclick="MasDet();">
                                    แสดงรายละเอียด 
                                </a>  
                            </th>
                            <th style="text-align: center; width: 25%;">
                                <button type="button" class="btn btn-outline btn-danger" onclick="window.history.back();"><i class="fa fa-arrow-left"></i></button>
                            </th>
                        </tr>
                    </table>
                    <hr>
                    <div class="collapse in" id="masDet" aria-expanded="true" style="display: none;">
                        <div class="container-fluid well">
                            <div style=" padding-top: 15px;" class="">
                                <b class="page-header" style="font-size:18px;">
                                    <div class="row ">
                                        <div class="col-sm-7 col-xs-7 col-md-7">
                                            <div class="row">
                                                <div class="col-sm-3 col-md-3 ">คลังสินค้า</div>
                                                <div class="col-sm-9 col-md-9  ng-binding"><span id="whDet"></span></div>
                                            </div>
                                        </div>
                                        <div class="col-sm-5 col-xs-5 col-md-5 ">
                                            <div class="row">
                                                <div class="col-sm-3 col-md-3">Plant</div>
                                                <div class="col-sm-9 col-md-9 ng-binding"><span id="plantDet"></span></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row ">
                                        <div class="col-sm-7 col-xs-7 col-md-7">
                                            <div class="row">
                                                <div class="col-sm-3 col-md-3">รหัสวัตถุดิบ</div>
                                                <div class="col-sm-3 col-md-3 ng-binding"><span id="rmcodeDet"></span></div>
                                                <div class="col-sm-6 col-md-6 ng-binding"><span id="rmdescDet"></span></div>
                                            </div>
                                        </div>
                                        <div class="col-sm-5 col-xs-5 col-md-5 ">
                                            <div class="row">
                                                <div class="col-sm-3 col-md-3">Storage</div>
                                                <div class="col-sm-9 col-md-9 ng-binding"><span id="storageDet"></span></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row ">
                                        <div class="col-sm-7 col-xs-7 col-md-7">

                                        </div>
                                        <div class="col-sm-5 col-xs-5 col-md-5 ">
                                            <div class="row">
                                                <div class="col-sm-3 col-md-3">Value type</div>
                                                <div class="col-sm-9 col-md-9 ng-binding"><span id="vtypeDet"></span></div>
                                            </div>
                                        </div>
                                    </div>
                                </b>
                            </div>
                            <hr>
                            <div class="body-details-material">
                                <!-- row 1 -->
                                <div class="row">
                                    <div class="col-sm-5 col-md-5">
                                        <div class="row">
                                            <div class="col-sm-4 col-md-4"><b>ID</b></div>
                                            <div class="col-sm-8 col-md-8 text-right ng-binding"><span id="rmidDet"></span></div>
                                        </div>
                                    </div>
                                    <div class="col-sm-2 col-md-2"></div>
                                    <div class="col-sm-5 col-md-5">
                                        <div class="row">
                                            <div class="col-sm-4 col-md-4"><b>Status</b></div>
                                            <div class="col-sm-8 col-md-8 text-right ng-binding"><span id="stsDet"></span></div>
                                        </div>
                                    </div>
                                </div>

                                <!-- row 2 -->
                                <div class="row">
                                    <div class="col-sm-5 col-md-5">
                                        <div class="row">
                                            <div class="col-sm-4 col-md-4"><b>PO no.</b></div>
                                            <div class="col-sm-8 col-md-8 text-right ng-binding"><span id="ponoDet"></span></div>
                                        </div>
                                    </div>
                                    <div class="col-sm-2 col-md-2"></div>
                                    <div class="col-sm-5 col-md-5">
                                        <div class="row">
                                            <div class="col-sm-4 col-md-4"><b>Rate Factor</b></div>
                                            <div class="col-sm-8 col-md-8 text-right ng-binding"><span id="rateFacDet"></span></div>
                                        </div>
                                    </div>
                                </div>

                                <!-- row 3 -->
                                <div class="row">
                                    <div class="col-sm-5 col-md-5">
                                        <div class="row">
                                            <div class="col-sm-4 col-md-4"><b>Tax Inv no.</b></div>
                                            <div class="col-sm-8 col-md-8 text-right ng-binding"><span id="taxInvDet"></span></div>
                                        </div>
                                    </div>
                                    <div class="col-sm-2 col-md-2"></div>
                                    <div class="col-sm-5 col-md-5">
                                        <div class="row">
                                            <div class="col-sm-4 col-md-4"><b>Material Ctrl.</b></div>
                                            <div class="col-sm-8 col-md-8 text-right ng-binding"><span id="matCtrlDet"></span></div>
                                        </div>
                                    </div>
                                </div>
                                <hr>

                                <!-- row 4 -->
                                <div class="row">
                                    <div class="col-sm-5 col-md-5">
                                        <div class="row">
                                            <div class="col-sm-3 col-md-3"><b>No.</b></div>
                                            <div class="col-sm-1 col-md-1 ng-binding"><span id="packTypeDet"></span></div>
                                            <div class="col-sm-8 col-md-8 text-right ng-binding"><span id="rollDet"></span></div>
                                        </div>
                                    </div>
                                    <div class="col-sm-2 col-md-2"></div>
                                    <div class="col-sm-5 col-md-5">
                                        <div class="row">
                                            <div class="col-sm-4 col-md-4"><b>LOT no.</b></div>
                                            <div class="col-sm-8 col-md-8 text-right ng-binding"><span id="lotnoDet"></span></div>
                                        </div>
                                    </div>
                                </div>

                                <!-- row 5 -->
                                <div class="row">
                                    <div class="col-sm-5 col-md-5">
                                        <div class="row">
                                            <div class="col-sm-4 col-md-4"><b>Quantity</b></div>
                                            <div class="col-sm-8 col-md-8 text-right ng-binding"><span id="qtyDet"></span></div>
                                        </div>
                                    </div>
                                    <div class="col-sm-2 col-md-2 ng-binding"><span id="unitDet"></span></div>
                                    <div class="col-sm-5 col-md-5">
                                        <div class="row">
                                            <div class="col-sm-4 col-md-4"><b>Dying no.</b></div>
                                            <div class="col-sm-8 col-md-8 text-right ng-binding"><span id="dynoDet"></span></div>
                                        </div>
                                    </div>
                                </div>

                                <!-- row 6 -->
                                <div class="row">
                                    <div class="col-sm-5 col-md-5">
                                        <div class="row">
                                            <div class="col-sm-4 col-md-4"><b>(Alt) Quantity</b></div>
                                            <div class="col-sm-8 col-md-8 text-right ng-binding"><span id="alQtyDet"></span></div>
                                        </div>
                                    </div>
                                    <div class="col-sm-2 col-md-2 ng-binding"><span id="alUnitDet"></span></div>
                                    <div class="col-sm-5 col-md-5">
                                        <div class="row">
                                            <div class="col-sm-4 col-md-4"><b>QI Status</b></div>
                                            <div class="col-sm-8 col-md-8 text-right ng-binding"><span id="qiStsDet"></span></div>
                                        </div>
                                    </div>
                                </div>
                                <hr>

                                <!-- row 7 -->
                                <div class="row">
                                    <div class="col-sm-5 col-md-5">
                                        <div class="row">
                                            <div class="col-sm-4 col-md-4"><b>Production date</b></div>
                                            <div class="col-sm-8 col-md-8 text-right ng-binding"><span id="pdDateDet"></span></div>
                                        </div>
                                    </div>
                                    <div class="col-sm-2 col-md-2"></div>
                                    <div class="col-sm-5 col-md-5">
                                        <div class="row">
                                            <div class="col-sm-4 col-md-4"><b>Warehouse</b></div>
                                            <div class="col-sm-8 col-md-8 text-right ng-binding"><span id="wh2Det"></span></div>
                                        </div>
                                    </div>
                                </div>

                                <!-- row 8 -->
                                <div class="row">
                                    <div class="col-sm-5 col-md-5">
                                        <div class="row">
                                            <div class="col-sm-4 col-md-4"><b>Receive date</b></div>
                                            <div class="col-sm-8 col-md-8 text-right ng-binding"><span id="rcDateDet"></span></div>
                                        </div>
                                    </div>
                                    <div class="col-sm-2 col-md-2"></div>
                                    <div class="col-sm-5 col-md-5">
                                        <div class="row">
                                            <div class="col-sm-4 col-md-4"><b>Zone</b></div>
                                            <div class="col-sm-8 col-md-8 text-right ng-binding"><span id="zoneDet"></span></div>
                                        </div>
                                    </div>
                                </div>

                                <!-- row 9 -->
                                <div class="row">
                                    <div class="col-sm-5 col-md-5">
                                        <div class="row">
                                            <div class="col-sm-4 col-md-4"><b>Issue date</b></div>
                                            <div class="col-sm-8 col-md-8 text-right ng-binding"><span id="isDateDet"></span></div>
                                        </div>
                                    </div>
                                    <div class="col-sm-2 col-md-2"></div>
                                    <div class="col-sm-5 col-md-5">
                                        <div class="row">
                                            <div class="col-sm-4 col-md-4"><b></b></div>
                                            <div class="col-sm-8 col-md-8 text-right"></div>
                                        </div>
                                    </div>
                                </div>

                                <!-- row 10 -->
                                <div class="row">
                                    <div class="col-sm-5 col-md-5">
                                        <div class="row">
                                            <div class="col-sm-4 col-md-4"><b>Transform date</b></div>
                                            <div class="col-sm-8 col-md-8 text-right ng-binding"><span id="tranDateDet"></span></div>
                                        </div>
                                    </div>
                                    <div class="col-sm-2 col-md-2"></div>
                                    <div class="col-sm-5 col-md-5">
                                        <div class="row">
                                            <div class="col-sm-4 col-md-4"><b>Rack no.</b></div>
                                            <div class="col-sm-8 col-md-8 text-right ng-binding"><span id="rackNoDet"></span></div>
                                        </div>
                                    </div>
                                </div>

                                <!-- row 11 -->
                                <div class="row">
                                    <div class="col-sm-5 col-md-5"></div>
                                    <div class="col-sm-2 col-md-2"></div>
                                    <div class="col-sm-5 col-md-5">
                                        <div class="row">
                                            <div class="col-sm-4 col-md-4"><b>Side</b></div>
                                            <div class="col-sm-8 col-md-8 text-right ng-binding"><span id="sideDet"></span></div>
                                        </div>
                                    </div>
                                </div>

                                <!-- row 12 -->
                                <div class="row">
                                    <div class="col-sm-5 col-md-5">
                                        <div class="row">
                                            <div class="col-sm-4 col-md-4"><b>Parent Code</b></div>
                                            <div class="col-sm-8 col-md-8 text-right ng-binding"><span id="parentDet"></span></div>
                                        </div>
                                    </div>
                                    <div class="col-sm-2 col-md-2"></div>
                                    <div class="col-sm-5 col-md-5">
                                        <div class="row">
                                            <div class="col-sm-4 col-md-4"><b>Column</b></div>
                                            <div class="col-sm-8 col-md-8 text-right ng-binding"><span id="colDet"></span></div>
                                        </div>
                                    </div>
                                </div>

                                <!-- row 13 -->
                                <div class="row">
                                    <div class="col-sm-5 col-md-5"></div>
                                    <div class="col-sm-2 col-md-2"></div>
                                    <div class="col-sm-5 col-md-5">
                                        <div class="row">
                                            <div class="col-sm-4 col-md-4"><b>Row</b></div>
                                            <div class="col-sm-8 col-md-8 text-right ng-binding"><span id="rowDet"></span></div>
                                        </div>
                                    </div>
                                </div>

                                <!-- row 14 -->
                                <div class="row">
                                    <div class="col-sm-5 col-md-5"><br></div>
                                    <div class="col-sm-2 col-md-2"></div>
                                    <div class="col-sm-5 col-md-5">
                                        <div class="row">
                                            <div class="col-sm-4 col-md-4"></div>
                                            <div class="col-sm-8 col-md-8 text-right"></div>
                                        </div>
                                    </div>
                                </div>

                                <!-- row 15 -->
                                <div class="row">
                                    <div class="col-sm-5 col-md-5"></div>
                                    <div class="col-sm-2 col-md-2"></div>
                                    <div class="col-sm-5 col-md-5">
                                        <div class="row">
                                            <div class="col-sm-4 col-md-4"><b>Pallete no.</b></div>
                                            <div class="col-sm-8 col-md-8 text-right ng-binding"><span id="palleteDet"></span></div>
                                        </div>
                                    </div>
                                </div>

                                <!-- row 16 -->
                                <div class="row">
                                    <div class="col-sm-5 col-md-5"></div>
                                    <div class="col-sm-2 col-md-2"></div>
                                    <div class="col-sm-5 col-md-5">
                                        <div class="row">
                                            <div class="col-sm-4 col-md-4"><b>Location</b></div>
                                            <div class="col-sm-8 col-md-8 text-right ng-binding"><span id="locationDet"></span></div>
                                        </div>
                                    </div>
                                </div>
                                <hr>

                                <!-- row 17 -->
                                <div class="row">
                                    <div class="col-sm-5 col-md-5">
                                        <div class="row">
                                            <div class="col-sm-4 col-md-4"><b>Create date &amp; time</b></div>
                                            <div class="col-sm-8 col-md-8 text-right ng-binding"><span id="cdtDet"></span></div>
                                        </div>
                                    </div>
                                    <div class="col-sm-2 col-md-2"></div>
                                    <div class="col-sm-5 col-md-5">
                                        <div class="row">
                                            <div class="col-sm-4 col-md-4"><b>Change date &amp; time</b></div>
                                            <div class="col-sm-8 col-md-8 text-right ng-binding"><span id="edtDet"></span></div>
                                        </div>
                                    </div>
                                </div>

                                <!-- row 18 -->
                                <div class="row">
                                    <div class="col-sm-5 col-md-5"></div>
                                    <div class="col-sm-2 col-md-2"></div>
                                    <div class="col-sm-5 col-md-5">
                                        <div class="row">
                                            <div class="col-sm-4 col-md-4"><b>User ID</b></div>
                                            <div class="col-sm-8 col-md-8 text-right ng-binding"><span id="userDet"></span></div>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                            </div>  <!-- end body-details-material -->
                        </div>   <!-- end container-fluid -->
                    </div>
                    <div id="sepDet" class="row" hidden>
                        <div class="col-xs-1 col-md-1"></div>
                        <div class="col-xs-10 col-md-10">
                            <form accept-charset="utf-8" name="formEdit" class="ng-pristine ng-valid-min ng-valid-step ng-invalid ng-invalid-required">
                                <div class="row">
                                    <div class="col-xs-12 col-md-12">
                                        <h3>
                                            <strong>
                                                <span class="text-info">รหัส : </span>
                                            </strong>
                                            <span id="QRMCODE" class="text ng-binding"></span>
                                        </h3>
                                        <h3>
                                            <strong>
                                                <span class="text-info">รายละเอียด : </span>
                                            </strong>
                                            <span id="QRMDESC" class="text ng-binding"></span>
                                        </h3>
                                        <!--                                        <h3>
                                                                                    <strong>
                                                                                        <span class="text-info">Plant : </span>
                                                                                    </strong>
                                                                                    <span id="QRMPLANT" class="text ng-binding"></span>
                                                                                </h3>-->
                                        <hr>
                                        <h3>
                                            ค่าก่อนถูกแบ่ง =&nbsp; <b id="QRMQTY_QRMBUN" class="ng-binding"></b>
                                            &nbsp;
                                            <span class="text-info"><b><em>และ</em></b></span>
                                            &nbsp;
                                            หน่วยบรรจุ <u>เดิม</u> =&nbsp; <b id="QRMPACKTYPE" class="ng-binding"></b>
                                        </h3>
                                        <hr>
                                        <h1 class="ng-binding hidden">ค่า<u>คงเหลือ</u>หลังจากถูกแบ่ง = &nbsp; <span class="text-warning"><b id="remainQty" class="ng-binding"></b></span> &nbsp; <span id="remainBun"></span></h1>
                                        <!--<hr>-->
                                        <h3>
                                            <div class="row" hidden>
                                                <div class="col-xs-7 col-md-7 line-heigth-followed-select-input">
                                                    ต้องการ <b>เปลี่ยนหน่วยบรรจุของตัวแม่</b> เป็น
                                                </div>
                                                <div class="col-xs-2 col-md-2 text-center line-heigth-followed-select-input">
                                                    <span><i class="fa fa-angle-double-right text-warning"></i></span>
                                                </div>
                                                <div class="col-xs-3 col-md-3">
                                                    <select id="parentPackType" class="form-control input-lg ng-pristine ng-untouched ng-valid ng-not-empty">
                                                        <option label="BAG" value="BAG">BAG</option>
                                                        <option label="BOX" value="BOX">BOX</option>
                                                        <option label="ROLL" value="ROLL">ROLL</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </h3>
                                        <!--<hr>-->
                                        <div class="row">
                                            <div class="col-xs-3 col-md-3">
                                                <h3 class="text-info"><b>ปริมาณ</b></h3>
                                            </div>
                                            <div class="col-xs-1 col-md-1"></div>
                                            <div class="col-xs-3 col-md-3">
                                                <h3>จำนวนบรรจุ</h3>
                                            </div>
                                            <div class="col-xs-3 col-md-3">
                                                <h3>หน่วยบรรจุ</h3>
                                            </div>
                                            <div class="col-xs-1 col-md-1"></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-3 col-md-3">
                                                <input class="form-control input-lg ng-pristine ng-empty ng-valid-min ng-valid-step ng-invalid ng-invalid-required ng-touched" id="stdQty" type="number" step="0.001" min="0.001" required="">
                                            </div>
                                            <div class="col-xs-1 col-md-1 text-center">
                                                <span style="font-size: 3rem;"><i class="fa fa-times text-info"></i></span>
                                            </div>
                                            <div class="col-xs-3 col-md-3">
                                                <input class="form-control input-lg ng-pristine ng-untouched ng-valid ng-not-empty ng-valid-min ng-valid-step" id="stdQtyMultiply" type="number" step="1" min="0" value="1">
                                            </div>
                                            <div class="col-xs-3 col-md-3">
                                                <select id="sepPackType" class="form-control input-lg ng-pristine ng-untouched ng-valid ng-not-empty">
                                                    <option label="BAG" value="BAG">BAG</option>
                                                    <option label="BOX" value="BOX">BOX</option>
                                                    <option label="ROLL" value="ROLL">ROLL</option>
                                                </select>
                                            </div>
                                            <div class="col-xs-1 col-md-1">
                                                <div id="btnSave">
                                                    <button id="btnSave2" type="button" onclick="calculateRemain();" class="btn btn btn-outline btn-success" title="Save / บันทึกข้อมูล" style="display: inline-grid;">
                                                        <i class="fa fa-save"></i>
                                                        บันทึก
                                                    </button>
                                                </div>
                                                <div id="btnSaveEdit" hidden>
                                                    <button id="btnSaveEdit2" type="button" onclick="calculateRemainEdit();" class="btn btn btn-outline btn-warning" title="Save / บันทึกข้อมูล" style="display: inline-grid;">
                                                        <i class="fa fa-save"></i>
                                                        บันทึก
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <!-- ngIf:  var.dataPreview.length > 0 -->
                            <hr>
                            <!-- ngRepeat: data in var.dataPreview|reverse -->
                        </div>
                        <div class="col-xs-1 col-md-1"></div>
                    </div>
                    <script>
                        function confirmBtn() {
                            $('#btnGenAndPrint').show();
                            $('#btnConfirm').hide();

                            $('.editBtn').prop('disabled', true);
                            $('.deleteBtn').prop('disabled', true);
                            $('#btnSave2').prop('disabled', true);
                            $('#btnSaveEdit2').prop('disabled', true);
                            $('#stdQty').prop('disabled', true);
                            $('#stdQtyMultiply').prop('disabled', true);
                            $('#sepPackType').prop('disabled', true);
                            $('#idcode').prop('disabled', true);
                        }

                        function undoBtn() {
                            $('#btnConfirm').show();
                            $('#btnGenAndPrint').hide();

                            $('.editBtn').prop('disabled', false);
                            $('.deleteBtn').prop('disabled', false);
                            $('#btnSave2').prop('disabled', false);
                            $('#btnSaveEdit2').prop('disabled', false);
                            $('#stdQty').prop('disabled', false);
                            $('#stdQtyMultiply').prop('disabled', false);
                            $('#sepPackType').prop('disabled', false);
                            $('#idcode').prop('disabled', false);
                        }
                    </script>
                    <div id="tableDet" class="row" hidden>
                        <div class="col-xs-12 col-md-12">
                            <div class="col-xs-12 col-md-12 text-right">
                                <div id="btnConfirm">
                                    <button type="button" class="btn btn-outline btn-warning" onclick="confirmBtn();" >Confirm</button>
                                </div>
                                <div id="btnGenAndPrint" hidden>
                                    <div id="btnGenId">
                                        <button type="button" class="btn btn-outline btn-danger" onclick="undoBtn();" >Undo</button>
                                        <button type="button" class="btn btn-outline btn-warning" onclick="generateId();" ><i class="fa fa-qrcode"></i> Generate ID</button>
                                    </div>
                                    <div id="btnPrePrint" hidden>
                                        <button type="button" class="btn btn-outline btn-warning" onclick="prePrint();" ><i class="fa fa-qrcode"></i> Preview Print</button>
                                    </div>
                                </div>
                            </div>
                            <table id="showTable" class="display" style="width:100%; border: 1px solid #ccc; border-radius: 4px;">
                                <thead>
                                    <tr>
                                        <th style="width: 100px;">ลำดับ</th>
                                        <th>รหัสวัตถุดิบ</th>
                                        <th>รายละเอียด</th>
                                        <th>Parent ID</th>
                                        <th>ปริมาณก่อนแบ่ง</th>
                                        <th>ปริมาณที่แบ่ง</th>
                                        <th>คงเหลือ</th>
                                        <th>หน่วยก่อนแบ่ง</th>
                                        <th>หน่วยที่แบ่ง</th>
                                        <th>ปรับเปลี่ยน</th>
                                        <th>QR ID</th>
                                        <th>GROUP QR ID</th>
                                    </tr>
                                </thead>
                                <tbody>                         
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!--**********************-->
                    <!--End Part 3-->
                    <!--</div>  end #wrapper-top--> 
                </div> 
                <br>
            </div> <!-- end .container-fluid -->
        </div> <!-- end #wrapper -->
        <script>
            function print(gid, trdt, matCtrl, pids) {
                /* v1  */
                console.log("print..", gid);

                $.ajax({
                    url: '/wms-service/rest/wms017/report/generate?gid=' + gid + '&trdt=' + trdt + '&whse=' + $('#wh').val() + '&uid=' + sessionStorage.getItem('uid') + '&matCtrl=' + matCtrl
                }).done(function (result) {
                    window.open('/wms-service/resources/report/wms017/output/pdf/WMS017-GroupQRID_' + gid + '.pdf');
                }).fail(function (jqXHR, textStatus, errorThrown) {
                    // needs to implement if it fails
                });

                console.log('parentIds = ' + pids);
                // OK
                window.open('/WMS/report/wms017RePrintParentIdReport.jsp?pids=' + pids);
            }
        </script>
    </body>
</html>