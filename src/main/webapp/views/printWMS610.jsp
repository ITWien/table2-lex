<%@ include file="../fragments/taglibs.jsp" %>
<%@ include file="../fragments/imports.jsp" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.sql.*,com.twc.wms.dao.*" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>WMS</title>      
        <!-- css :: vendors -->
        <jsp:include page = "../fragments/css.jsp" />
        <!-- additional custom :: my-style -->
        <link rel="stylesheet" href="../resources/assets/styles/myStyles.css">

        <!-- js :: vendors -->
        <jsp:include page = "../fragments/script.jsp" />
        <!-- additional custom :: my-script -->
        <script src="../resources/assets/scripts/myScripts.js" async></script>
        <script src="../resources/assets/scripts/toggleLoad.js" async></script>
        <script src="../../WMSMenuControl/resources/assets/scripts/isLogin.js"></script>
        <script type="text/javascript"> /* add something code to your script */</script>
        ${sendMessage}
        ${showPDF}
        <script>
            function Uppercase() {
                var x = document.getElementById("Spo");
                var y = document.getElementById("Fpo");
                var z = document.getElementById("Stax");
                var z1 = document.getElementById("Ftax");
                var a = document.getElementById("SmatCode");
                var b = document.getElementById("FmatCode");
                var c = document.getElementById("Suser");
                var d = document.getElementById("Fuser");
                x.value = x.value.toUpperCase();
                y.value = y.value.toUpperCase();
                z.value = z.value.toUpperCase();
                z1.value = z1.value.toUpperCase();
                a.value = a.value.toUpperCase();
                b.value = b.value.toUpperCase();
                c.value = c.value.toUpperCase();
                d.value = d.value.toUpperCase();
            }
        </script>
        <script>
            function isNumberKey(evt) {
                var charCode = (evt.which) ? evt.which : event.keyCode
                if (charCode > 31 && (charCode < 48 || charCode > 57))
                    return false;
                return true;
            }
        </script>
        <script>
            function isAlphabetKey(evt) {
                var charCode = (evt.which) ? evt.which : event.keyCode
                if ((charCode < 65 || charCode > 90) && (charCode < 97 || charCode > 122))
                    return false;
                return true;
            }
        </script>
        <script>
            var show1 = function () {
                $('#myModal1').modal('show');
            };

            var show2 = function () {
                $('#myModal2').modal('show');
            };

            var show3 = function () {
                $('#myModal3').modal('show');
            };

            var show4 = function () {
                $('#myModal4').modal('show');
            };

            var show5 = function () {
                $('#myModal5').modal('show');
            };

            var show6 = function () {
                $('#myModal6').modal('show');
            };

            var show7 = function () {
                $('#myModal7').modal('show');
            };

            var show8 = function () {
                $('#myModal8').modal('show');
            };

            function validateForm() {
                var StranDate = document.forms["frm"]["StranDate"].value;
                var FtranDate = document.forms["frm"]["FtranDate"].value;
                var Splant = document.forms["frm"]["Splant"].value;
                var Fplant = document.forms["frm"]["Fplant"].value;
                var Spo = document.forms["frm"]["Spo"].value;
                var Fpo = document.forms["frm"]["Fpo"].value;
                var Swh = document.forms["frm"]["Swh"].value;
                var Fwh = document.forms["frm"]["Fwh"].value;
                var Stax = document.forms["frm"]["Stax"].value;
                var Ftax = document.forms["frm"]["Ftax"].value;
                var SmatCode = document.forms["frm"]["SmatCode"].value;
                var FmatCode = document.forms["frm"]["FmatCode"].value;
                var Suser = document.forms["frm"]["Suser"].value;
                var Fuser = document.forms["frm"]["Fuser"].value;

                StranDate = StranDate.toString();
                StranDate = StranDate.replace("-", "");
                StranDate = StranDate.replace("-", "");
                var sd = parseInt(StranDate);

                FtranDate = FtranDate.toString();
                FtranDate = FtranDate.replace("-", "");
                FtranDate = FtranDate.replace("-", "");
                var fd = parseInt(FtranDate);

                Splant = Splant.toString();
                Fplant = Fplant.toString();
                var sp = parseInt(Splant);
                var fp = parseInt(Fplant);

                Spo = Spo.toString();
                Fpo = Fpo.toString();
                var spo = parseInt(Spo);
                var fpo = parseInt(Fpo);

                Swh = Swh.toString();
                Fwh = Fwh.toString();

                Stax = Stax.toString();
                Ftax = Ftax.toString();

                SmatCode = SmatCode.toString();
                FmatCode = FmatCode.toString();

                Suser = Suser.toString();
                Fuser = Fuser.toString();

                var WH = [Swh, Fwh];
                WH.sort();

                var TAX = [Stax, Ftax];
                TAX.sort();

                var MC = [SmatCode, FmatCode];
                MC.sort();

                var US = [Suser, Fuser];
                US.sort();

                if (Suser !== US[0]) {
                    if (Fuser !== "") {
                        window.setTimeout(show8, 0);
                        return false;
                    }
                }

                if (SmatCode !== MC[0]) {
                    if (FmatCode !== "") {
                        window.setTimeout(show7, 0);
                        return false;
                    }
                }

                if (Stax !== TAX[0]) {
                    if (Ftax !== "") {
                        window.setTimeout(show6, 0);
                        return false;
                    }
                }

                if (Swh !== WH[0]) {
                    if (Fwh !== "") {
                        window.setTimeout(show5, 0);
                        return false;
                    }
                }

                if (fpo < spo) {
                    window.setTimeout(show4, 0);
                    return false;
                }

                if (fp < sp) {
                    window.setTimeout(show3, 0);
                    return false;
                }

                if (fd < sd) {
                    window.setTimeout(show2, 0);
                    return false;
                }

                if (StranDate.trim() === "") {
                    window.setTimeout(show1, 0);
                    document.getElementById("StranDate").style.cssText = "border: 2px solid #ff9999";
                    return false;
                } else {
                    document.getElementById("StranDate").style.cssText = "border: 1px solid #ccc";
                }
            }
        </script>
        <style>
            input[type=text], input[type=password], input[type=number], select {
                width: 100%;
                padding: 5px 5px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }

            button[type=submit] {
                width: 100%;
                background-color: #2bd14a;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            button[type=submit]:hover {
                background-color: #45a049;
            }

            input[type=reset] {
                width: 100%;
                background-color: #ef604a;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=reset]:hover {
                background-color: #e51e00;
            }

            input[type=date] {
                height: 35px;
                width: 100%;
                padding: 5px 5px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }

            button[name=ok] {
                width: 25%;
                background-color: #008cff;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            button[name=ok]:hover {
                background-color: #008cff;
            }

            input[type=submit] {
                width: 100%;
                background-color: #00399b;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=submit]:hover {
                background-color: #3973d6;
            }
        </style>
    </head>    
    <body >
        <div id="wrapper">

            <!-- /#sidebar-wrapper -->
            <%@ include file="../fragments/sidebar.jsp" %>

            <!-- Page Content -->
            <!-- nav-head-custom -->
            <%@ include file="../fragments/nav_head.jsp" %>

            <div class="container-fluid">
                <!--                <div id="wrapper-top" align="left">
                <%-- PART 2 --%>
                <div class="row">
                    <div class="col-lg-12">
                        <div id="set-height" style="height:415px;margin-top:0px;">
                            <div id="sidebar-wrapper-top" class="">
                                <b class="page-header" style="padding-left:5px;font-size:18px;">
                <%-- <hr style="margin:0px;margin-top:10px;margin-bottom:10px;"> --%>
            </b>
        </div>
    </div>
</div>
</div> -->

                <%-- PART 3 --%>
                <form action="../resources/manual/WMS610.pdf" target="_blank">
                    <table width="100%">
                        <tr>
                            <td width="94%"></td>
                            <td width="6%" align="right"><input type="submit" value="Manual" /></td>
                        </tr>
                    </table>
                </form>
                <form action="print" method="POST" name="frm" onsubmit="return validateForm()">
                    <table frame="box" width="100%" bordercolor="#e0e0e0">
                        <tr>
                            <th height="50" bgcolor="#f8f8f8"></th>
                            <th height="50" bgcolor="#f8f8f8"></th>
                            <th height="50" bgcolor="#f8f8f8">From</th>
                            <th height="50" bgcolor="#f8f8f8">To</th>
                            <th height="50" bgcolor="#f8f8f8"></th>
                        </tr>
                        <br>
                        <tr>
                            <td width="10%"></td>
                            <td width="20%" align="left"><h4>Warehouse :</h4></td>
                            <td width="20%" align="left" style="padding-right: 30px;"><select name="Swh">
                                    <option value="${Swh}" selected hidden>${Swh} : ${Swhn}</option>
                                    <c:forEach items="${WHList}" var="p" varStatus="i">
                                        <option value="${p.code}">${p.code} : ${p.name}</option>
                                    </c:forEach>
                                </select></td>
                            <td width="20%" align="left" style="padding-right: 30px;"><select name="Fwh">
                                    <option value="${Fwh}" selected hidden>${Fwh} : ${Fwhn}</option>
                                    <c:forEach items="${WHList}" var="p" varStatus="i">
                                        <option value="${p.code}">${p.code} : ${p.name}</option>
                                    </c:forEach>
                                </select></td>
                            <td width="20%"></td>
                        </tr>
                        <tr>
                            <td width="10%"></td>
                            <td width="20%" align="left"><h4>Transaction Date :</h4></td>
                            <td width="20%" align="left" style="padding-right: 30px;"><input type="date" id="StranDate" name="StranDate" value="${StranDate}"></td>
                            <td width="20%" align="left" style="padding-right: 30px;"><input type="date" id="FtranDate" name="FtranDate" value="${FtranDate}"></td>
                            <td width="20%"></td>
                        </tr>
                        <tr>
                            <td width="10%"></td>
                            <td width="20%" align="left"><h4>Plant :</h4></td>
                            <td width="20%" align="left" style="padding-right: 30px;"><input type="text" id="Splant" name="Splant" maxlength="4" value="${Splant}" onkeypress="return isNumberKey(event)" list="plantlist">
                                <datalist id="plantlist">
                                    <c:forEach items="${plantList}" var="p" varStatus="i">
                                        <option value="${p.plant}">
                                        </c:forEach>
                                </datalist>
                            </td>
                            <td width="20%" align="left" style="padding-right: 30px;"><input type="text" id="Fplant" name="Fplant" maxlength="4" value="${Fplant}" onkeypress="return isNumberKey(event)" list="plantlist"></td>
                            <td width="20%"></td>
                        </tr>
                        <tr>
                            <td width="10%"></td>
                            <td width="20%" align="left"><h4>PO no. :</h4></td>
                            <td width="20%" align="left" style="padding-right: 30px;">
                                <input type="text" id="Spo" name="Spo" value="${Spo}" maxlength="10" onkeyup="Uppercase()" list="polist">
                                <datalist id="polist">
                                    <c:forEach items="${QMList}" var="p" varStatus="i">
                                        <option value="${p.po}">
                                        </c:forEach>
                                </datalist>
                            </td>
                            <td width="20%" align="left" style="padding-right: 30px;">
                                <input type="text" id="Fpo" name="Fpo" value="${Fpo}" maxlength="10" onkeyup="Uppercase()" list="polist">
                            </td>
                            <td width="20%"></td>
                        </tr>
                        <tr>
                            <td width="10%"></td>
                            <td width="20%" align="left"><h4>Tax Inv no. :</h4></td>
                            <td width="20%" align="left" style="padding-right: 30px;"><input type="text" id="Stax" name="Stax" maxlength="15" value="${Stax}" onkeyup="Uppercase()" list="taxlist">
                                <datalist id="taxlist">
                                    <c:forEach items="${taxList}" var="p" varStatus="i">
                                        <option value="${p.tax}">
                                        </c:forEach>
                                </datalist>
                            </td>
                            <td width="20%" align="left" style="padding-right: 30px;"><input type="text" id="Ftax" name="Ftax" maxlength="15" value="${Ftax}" onkeyup="Uppercase()" list="taxlist"></td>
                            <td width="20%"></td>
                        </tr>
                        <tr>
                            <td width="10%"></td>
                            <td width="20%" align="left"><h4>Material Code :</h4></td>
                            <td width="20%" align="left" style="padding-right: 30px;"><input type="text" id="SmatCode" name="SmatCode" maxlength="11" value="${SmatCode}" onkeyup="Uppercase()" list="matlist">
                                <datalist id="matlist">
                                    <c:forEach items="${matList}" var="p" varStatus="i">
                                        <option value="${p.matCode}">
                                        </c:forEach>
                                </datalist>
                            </td>
                            <td width="20%" align="left" style="padding-right: 30px;"><input type="text" id="FmatCode" name="FmatCode" maxlength="11" value="${FmatCode}" onkeyup="Uppercase()" list="matlist"></td>
                            <td width="20%"></td>
                        </tr>
                        <tr>
                            <td width="10%"></td>
                            <td width="20%" align="left"><h4>User :</h4></td>
                            <td width="20%" align="left" style="padding-right: 30px;"><input type="text" id="Suser" name="Suser" maxlength="10" value="${Suser}" onkeyup="Uppercase()" list="userlist">
                                <datalist id="userlist">
                                    <c:forEach items="${usList}" var="p" varStatus="i">
                                        <option value="${p.user}">
                                        </c:forEach>
                                </datalist>
                            </td>
                            <td width="20%" align="left" style="padding-right: 30px;"><input type="text" id="Fuser" name="Fuser" maxlength="10" value="${Fuser}" onkeyup="Uppercase()" list="userlist"></td>
                            <td width="20%"></td>
                        </tr>
                        <br>
                        <div align="center">
                            <tr>
                                <td></td>
                                <td></td>
                                <td width="20%" align="right" style="padding-right: 30px;"><input style="width: 100px;" type="reset" value="Clear" onclick="window.location.href = '/TABLE2/WMS610/print'"/>
                                </td>
                                <td><button style="width: 100px;" type="submit"/><i class="fa fa-file-pdf-o"></i> Print</button></td>
                                <td></td>
                            </tr>
                        </div>
                    </table>
                    <center>
                        <div id="myModal1" class="modal fade" role="dialog">
                            <div class="modal-dialog modal-sm"> <!-- ???????????? modal ?????????????? ???? xs, sm, md, lg -->
                                <div class="modal-md col-md-6" style="top: 150px; left:300px; background-color: #ddd; border: 1px solid #ccc; border-radius: 4px;">
                                    <div class="modal-header">
                                        <h4 class="modal-title" align="center">Please fill in the blanks !</h4>
                                        <br>
                                        <button name="ok" type="button" class="close" data-dismiss="modal">
                                            <font color = "white">OK</font>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- devbanban.com -->
                    </center>
                    <center>
                        <div id="myModal2" class="modal fade" role="dialog">
                            <div class="modal-dialog modal-sm"> <!-- ???????????? modal ?????????????? ???? xs, sm, md, lg -->
                                <div class="modal-md col-md-6" style="top: 150px; left:300px; background-color: #ddd; border: 1px solid #ccc; border-radius: 4px;">
                                    <div class="modal-header">
                                        <h4 class="modal-title" align="center">Transaction Date(To) must be greater than or equal to Transaction Date(From) !</h4>
                                        <br>
                                        <button name="ok" type="button" class="close" data-dismiss="modal">
                                            <font color = "white">OK</font>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- devbanban.com -->
                    </center>
                    <center>
                        <div id="myModal3" class="modal fade" role="dialog">
                            <div class="modal-dialog modal-sm"> <!-- ???????????? modal ?????????????? ???? xs, sm, md, lg -->
                                <div class="modal-md col-md-6" style="top: 150px; left:300px; background-color: #ddd; border: 1px solid #ccc; border-radius: 4px;">
                                    <div class="modal-header">
                                        <h4 class="modal-title" align="center">Plant(To) must be greater than or equal to Plant(From) !</h4>
                                        <br>
                                        <button name="ok" type="button" class="close" data-dismiss="modal">
                                            <font color = "white">OK</font>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- devbanban.com -->
                    </center>
                    <center>
                        <div id="myModal4" class="modal fade" role="dialog">
                            <div class="modal-dialog modal-sm"> <!-- ???????????? modal ?????????????? ???? xs, sm, md, lg -->
                                <div class="modal-md col-md-6" style="top: 150px; left:300px; background-color: #ddd; border: 1px solid #ccc; border-radius: 4px;">
                                    <div class="modal-header">
                                        <h4 class="modal-title" align="center">PO no.(To) must be greater than or equal to PO no.(From) !</h4>
                                        <br>
                                        <button name="ok" type="button" class="close" data-dismiss="modal">
                                            <font color = "white">OK</font>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- devbanban.com -->
                    </center>
                    <center>
                        <div id="myModal5" class="modal fade" role="dialog">
                            <div class="modal-dialog modal-sm"> <!-- ???????????? modal ?????????????? ???? xs, sm, md, lg -->
                                <div class="modal-md col-md-6" style="top: 150px; left:300px; background-color: #ddd; border: 1px solid #ccc; border-radius: 4px;">
                                    <div class="modal-header">
                                        <h4 class="modal-title" align="center">Warehouse(To) must be greater than or equal to Warehouse(From) !</h4>
                                        <br>
                                        <button name="ok" type="button" class="close" data-dismiss="modal">
                                            <font color = "white">OK</font>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- devbanban.com -->
                    </center>
                    <center>
                        <div id="myModal6" class="modal fade" role="dialog">
                            <div class="modal-dialog modal-sm"> <!-- ???????????? modal ?????????????? ???? xs, sm, md, lg -->
                                <div class="modal-md col-md-6" style="top: 150px; left:300px; background-color: #ddd; border: 1px solid #ccc; border-radius: 4px;">
                                    <div class="modal-header">
                                        <h4 class="modal-title" align="center">Tax Inv no.(To) must be greater than or equal to Tax Inv no.(From) !</h4>
                                        <br>
                                        <button name="ok" type="button" class="close" data-dismiss="modal">
                                            <font color = "white">OK</font>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- devbanban.com -->
                    </center>
                    <center>
                        <div id="myModal7" class="modal fade" role="dialog">
                            <div class="modal-dialog modal-sm"> <!-- ???????????? modal ?????????????? ???? xs, sm, md, lg -->
                                <div class="modal-md col-md-6" style="top: 150px; left:300px; background-color: #ddd; border: 1px solid #ccc; border-radius: 4px;">
                                    <div class="modal-header">
                                        <h4 class="modal-title" align="center">Material Code(To) must be greater than or equal to Material Code(From) !</h4>
                                        <br>
                                        <button name="ok" type="button" class="close" data-dismiss="modal">
                                            <font color = "white">OK</font>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- devbanban.com -->
                    </center>
                    <center>
                        <div id="myModal8" class="modal fade" role="dialog">
                            <div class="modal-dialog modal-sm"> <!-- ???????????? modal ?????????????? ???? xs, sm, md, lg -->
                                <div class="modal-md col-md-6" style="top: 150px; left:300px; background-color: #ddd; border: 1px solid #ccc; border-radius: 4px;">
                                    <div class="modal-header">
                                        <h4 class="modal-title" align="center">User(To) must be greater than or equal to User(From) !</h4>
                                        <br>
                                        <button name="ok" type="button" class="close" data-dismiss="modal">
                                            <font color = "white">OK</font>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- devbanban.com -->
                    </center>
                </form>
                <!--End Part 3-->
                <!--</div>  end #wrapper-top -->
            </div> <!-- end .container-fluid -->
        </div> <!-- end #wrapper -->
    </body>

</html>