<%@ include file="../fragments/taglibs.jsp" %>
<%@ include file="../fragments/imports.jsp" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.sql.*;" %>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <title>ISM</title>
        <!-- css :: vendors -->
        <jsp:include page="../fragments/css.jsp" />
        <!-- additional custom :: my-style -->
        <link rel="stylesheet" href="../resources/assets/styles/myStyles.css">
        <link rel="stylesheet" href="../resources/font/THsarabun/thsarabumnew.css">

        <!-- js :: vendors -->
        <jsp:include page="../fragments/script.jsp" />
        <!-- additional custom :: my-script -->
        <script src="../resources/assets/scripts/myScripts.js" async></script>
        <script src="../resources/assets/scripts/toggleLoad.js" async></script>
        <style>
            input[type=text],
            select {
                width: 100%;
                padding: 5px 5px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }

            input[type=date],
            input[type=number] {
                /*width: 100%;*/
                padding: 5px 5px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }

            input[type=submit] {
                width: 100%;
                background-color: #00399b;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=submit]:hover {
                background-color: #3973d6;
            }

            input[type=button] {
                width: 100%;
                background-color: #ef604a;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=button]:hover {
                background-color: #e51e00;
            }
        </style>
        <script>
            var tableOptions = {
                "paging": false,
                "lengthMenu": [[-1, 10, 25, 50, 100], ["All", 10, 25, 50, 100]],
                "bSortClasses": false,
                "ordering": false,
                "scrollY": '50vh',
//                "scrollX": true,
                "scrollCollapse": true,
                "info": false,
                "fixedColumns": true,
                "order": [[0, "asc"]],
                "columnDefs": [
                    {"targets": [3, 4, 5, 6, 7], "className": "text-right"},
                    {"targets": [0, 8], "className": "text-center"}
//                    {"targets": [10], "orderable": false}
                ]
            };
        </script>
        <style>
            body {
                font-family: "Sarabun", sans-serif !important;
                /*font-family: 'THSarabunNew', sans-serif;*/
            }

            .displayTable {
                width: auto;
                border: 1px solid #ccc;
                border-radius: 4px;
            }

            .displayContain {
                border: 2px solid #ccc;
                border-radius: 5px;
                padding: 20px;
                padding-top: 0px;
                /*width: fit-content;*/
            }

            /*            td {
                            white-space: nowrap;
                            cursor: pointer;
                        }*/

            .checkData ,.checkAllData{
                width: 20px;
                height: 20px;
            }

            .sts-0{
                font-size: 20px;
                color: navy;
            }
            .sts-1{
                font-size: 20px;
                color: yellow;
            }
            .sts-2{
                font-size: 20px;
                color: green;
            }
            .sts-3{
                font-size: 20px;
                color: blue;
            }
            .sts-4{
                font-size: 20px;
                color: pink;
            }
            .sts-5{
                font-size: 20px;
                color: navy;
            }
            .sts-6{
                font-size: 20px;
                color: gray;
            }
            .sts-7{
                font-size: 20px;
                color: brown;
            }
            .sts-8{
                font-size: 20px;
                color: orange;
            }
            .sts-9{
                font-size: 20px;
                color: black;
            }

            .modal-content{
                width: 50%;
            }

            #fileFrom{
                height: 700px;
                border-radius: 10px;
                border: 5px solid #88d9f2;
                padding: 10px;
                /*text-align: center;*/
                overflow: auto;
            }

            #fileTo{
                height: 700px;
                border-radius: 10px;
                border: 5px dashed #88d9f2;
                padding: 10px;
                /*text-align: center;*/
                overflow: auto;
            }

            .styleLotIcon{
                font-size: 50px;
                color: #59b7d4;
            }

            .cardStyle{
                width: fit-content;
                text-align: center;
                padding: 10px;
                display: inline-grid;
                cursor: pointer;
                border-radius: 15px;
            }

            #fileFrom .cardStyle:hover{
                background-color: #d0ecf5;
            }
            #fileTo .cardStyle:hover{
                background-color: #f5d6d0;
            }

            .centerTb{
                text-align: center;
                padding: 7px;
            }

            .rightTb{
                text-align: right;
                padding: 7px;
            }

            .cardTb-l{
                border: 2px solid #2725a8;
                background-color: #cfdaff;
            }
            .cardTb-r{
                border: 2px solid #2725a8;
            }

            #showTable tbody tr {
                cursor: pointer;
            }

            .alarmStyle{
                background-color: #f5d6d0;
                border: 3px dashed red;
            }
        </style>
        <script>
            var detailsList = null;
            var updateQnoList = [];

            $(document).ready(function () {

                console.log("this");

                $('#showTable').DataTable(tableOptions);

                $('#backBtn').click(function () {
                    window.location.href = '${goBack}';
                });

                getSummary();

                $('#showTable tbody').on('click', 'tr', function () {
                    var table = $('#showTable').DataTable();
                    var row = table.row(this);
                    var thisTr = this;

                    if (row.child.isShown()) {
                        row.child.hide();
                        $(this).css('background-color', '');

                    } else {

                        var matCode = row.data()[1].toString().trim();
                        var jobNo = "${jobNo}";

                        $.ajax({
                            url: "/TABLE2/ShowDataTablesISM200?mode=getHeadByMatcodeJobNo"
                                    + "&matCode=" + matCode
                                    + "&jobNo=" + jobNo

                        }).done(function (result) {
                            console.log(result);
                            row.child(format(row.data(), result)).show();
                            $(thisTr).css('background-color', '#a0e8b1');
                            $('#childTB-' + row.data()[1].toString().trim()).parent().css('background-color', '#a0e8b1');

                        }).fail(function (jqXHR, textStatus, errorThrown) {
                            // needs to implement if it fails
                        });

                    }

                });

                $('#createBtn').click(function () {

                    var uid = sessionStorage.getItem('uid');
                    var jobCreateDate = $('#jobCreateDate').val();
                    var jobDebitDate = $('#jobDebitDate').val();
                    var jobTransferDate = $('#jobTransferDate').val();

                    if (jobCreateDate !== "" && jobDebitDate !== "" && jobTransferDate !== "") {

                        var table = $('#showTable').DataTable();
                        table.clear().draw();
                        $('#loadIcon').removeClass('hidden');

                        var cnt = 1;
                        var mat = 'st';

                        for (var i = 0; i < detailsList.length; i++) {
                            if (mat !== 'st') {
                                if (mat !== detailsList[i].ISDMTCTRL) {
                                    cnt++;
                                }
                            }
                            detailsList[i].QNO = cnt;
                            mat = detailsList[i].ISDMTCTRL;
                        }

                        //UPDATE STATUS 4 -> 5 send JobNo To Get Head And Detail
                        var jbno = $('#jobNo').val();
                        var sts = '5';

                        $.ajax({
                            async: false,
                            url: "/TABLE2/ShowDataTablesISM200?mode=updateStatus4To5&jbno=" + jbno + "&sts=" + sts

                        }).done(function (result) {
                            console.log(result);

                        }).fail(function (jqXHR, textStatus, errorThrown) {
                            // needs to implement if it fails
                        });

                        $.ajax({
                            async: false,
                            url: "/TABLE2/ShowDataTablesISM200?mode=getQnoList"
                                    + "&cnt=" + cnt
                                    + "&wh=${wh}"

                        }).done(function (result) {
                            console.log(result);

                            cnt = 1;
                            mat = 'st';

                            for (var i = 0; i < detailsList.length; i++) {
                                if (mat !== 'st') {
                                    if (mat !== detailsList[i].ISDMTCTRL) {
                                        cnt++;
                                    }
                                }
                                detailsList[i].ISDQNO = result[cnt - 1];
                                mat = detailsList[i].ISDMTCTRL;
                            }

                        }).fail(function (jqXHR, textStatus, errorThrown) {
                            // needs to implement if it fails
                        });

//                    end gen queue

                        var result = detailsList;

                        for (var i = 0; i < result.length; i++) {

                            var diff = parseFloat(result[i].ISDSAPONH.toString().replace(/,/g, '')) - parseFloat(result[i].ISDRQQTY.toString().replace(/,/g, ''));
                            var per = (parseFloat(result[i].ISDSAPONH.toString().replace(/,/g, '')) / parseFloat(result[i].ISDRQQTY.toString().replace(/,/g, ''))) * 100;
                            if (per > 100.00) {
                                per = 100.00;
                            }

                            var oe = '';
                            if (result[i].ISDQNO === '') {
                                oe = 'none';
                            } else if (parseInt(result[i].ISDQNO) % 2 === 0) {
                                oe = 'even';
                            } else {
                                oe = 'odd';
                            }

                            var upQ = {
                                ORD: result[i].ISDORD,
                                LINO: result[i].ISDLINO,
                                SINO: result[i].ISDSINO,
                                QNO: result[i].ISDQNO
                            };

                            updateQnoList.push(upQ);

                            table.row.add([
                                (i + 1),
                                result[i].ISDITNO,
                                result[i].ISDDESC,
                                result[i].ISDSAPONH,
                                result[i].ISDWMSONH,
                                result[i].ISDRQQTY,
                                currencyFormat(diff, 2),
                                currencyFormat(per, 2),
                                result[i].ISDUNIT,
                                result[i].ISDMTCTRL + ' : ' + result[i].MAT_NAME,
                                result[i].ISDUSER + ' : ' + result[i].USR_NAME,
                                '<b class="showQno-' + oe + '">' + result[i].ISDQNO + '</b>'
                            ]).draw(false);

                        }
                        $('#loadIcon').addClass('hidden');
                        $('#resLen').html(result.length);

                        $('.showQno-odd').parent().css('background-color', '#afc9ed');
                        $('.showQno-even').parent().css('background-color', '#b5edaf');

                        console.log('updateQnoList: ', updateQnoList);
                        $.ajax({
                            url: "/TABLE2/ShowDataTablesISM200?mode=updateQnoList"
                                    + "&jobCreateDate=" + jobCreateDate
                                    + "&jobDebitDate=" + jobDebitDate
                                    + "&jobTransferDate=" + jobTransferDate
                                    + "&jobNo=${jobNo}",
                            type: 'POST',
                            contentType: 'application/json',
                            data: JSON.stringify(updateQnoList)

                        }).done(function (result) {
                            console.log('updateQnoList result: ', result);
                            alertify.success('Update Success!');

                            $('#createBtn').addClass('disabled');

                        }).fail(function (jqXHR, textStatus, errorThrown) {
                            // needs to implement if it fails
                        });

                    } else {
                        $('#myModal').modal("show");
                    }

                });

            });

            function getSummary() {
                var table = $('#showTable').DataTable();
                table.clear().draw();
                $('#loadIcon').removeClass('hidden');

                var date = new Date();
                var curDate = date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate();

                $.ajax({
                    url: "/TABLE2/ShowDataTablesISM200?mode=getHeadSummary"
                            + "&jobNo=${jobNo}"

                }).done(function (result) {
                    console.log(result);

                    $('#jobCreateDate').val(result.ISHP1DT);
                    $('#jobDebitDate').val(result.ISHA1DT);
                    $('#jobTransferDate').val(result.ISHT1DT);

                }).fail(function (jqXHR, textStatus, errorThrown) {
                    // needs to implement if it fails
                });

                $.ajax({
                    url: "/TABLE2/ShowDataTablesISM200?mode=getDetailSummary"
                            + "&jobNo=${jobNo}"
                            + "&wh=${wh}"

                }).done(function (result) {
                    console.log(result);
                    detailsList = result;
                    var disBtn = false;

                    for (var i = 0; i < result.length; i++) {

                        var diff = parseFloat(result[i].ISDSAPONH.toString().replace(/,/g, '')) - parseFloat(result[i].ISDRQQTY.toString().replace(/,/g, ''));
                        var per = (parseFloat(result[i].ISDSAPONH.toString().replace(/,/g, '')) / parseFloat(result[i].ISDRQQTY.toString().replace(/,/g, ''))) * 100;
                        if (per > 100.00) {
                            per = 100.00;
                        }

                        var oe = '';
                        if (result[i].ISDQNO === '') {
                            oe = 'none';
                        } else if (parseInt(result[i].ISDQNO) % 2 === 0) {
                            oe = 'even';
                        } else {
                            oe = 'odd';
                        }

                        if (oe !== 'none') {
                            disBtn = true;
                        }

                        if (result[i].ISDMTCTRL === "") {
                            disBtn = true;
                        }

                        if (result[i].ISDMTCTRL === null) {
                            disBtn = true;
                        }

                        table.row.add([
                            (i + 1),
                            result[i].ISDITNO,
                            result[i].ISDDESC,
                            result[i].ISDSAPONH,
                            result[i].ISDWMSONH,
                            result[i].ISDRQQTY,
                            currencyFormat(diff, 2),
                            currencyFormat(per, 2),
                            result[i].ISDUNIT,
                            result[i].ISDMTCTRL + ' : ' + result[i].MAT_NAME,
                            result[i].ISDUSER + ' : ' + result[i].USR_NAME,
                            '<b class="showQno-' + oe + '">' + result[i].ISDQNO + '</b>'
                        ]).draw(false);

                    }
                    $('#loadIcon').addClass('hidden');
                    $('#resLen').html(result.length);

                    $('.showQno-odd').parent().css('background-color', '#afc9ed');
                    $('.showQno-even').parent().css('background-color', '#b5edaf');

                    if (disBtn) {
                        $('#createBtn').addClass('disabled');
                    }

                }).fail(function (jqXHR, textStatus, errorThrown) {
                    // needs to implement if it fails
                });

            }

            function currencyFormat(num, fp) {
                var pp = Math.pow(10, fp);
                num = Math.round(num * pp) / pp;
                return num.toFixed(fp).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
            }

            function clearTable() {
                var table = $('#showTable').DataTable();
                table.clear().draw();
            }

            function format(d, res) {

                var tableText = '';

                tableText += '<table id="childTB-' + d[1].toString().trim() + '" cellpadding="5" cellspacing="0" border="0" style="padding-left:50px; width: 100%;">' +
                        '<tr style="color: purple;">' +
                        '<th>MVT</th>' +
                        '<th>Trans Dt</th>' +
                        '<th>Sale Order</th>' +
                        '<th>Customer</th>' +
                        '<th>Name</th>' +
                        '<th>Doc no.</th>' +
                        '<th>FG Style</th>' +
                        '<th>Style</th>' +
                        '<th>Color</th>' +
                        '<th>Lot</th>' +
                        '<th>Qty</th>' +
                        '</tr>';

                for (var i = 0; i < res.length; i++) {

                    tableText += '<tr>' +
                            '<td>' + res[i].ISHMVT + '</td>' +
                            '<td>' + res[i].ISHTRDT + '</td>' +
                            '<td>' + res[i].ISHORD + '</td>' +
                            '<td>' + res[i].ISHCUNO + '</td>' +
                            '<td>' + res[i].ISHCUNM1 + '</td>' +
                            '<td>' + res[i].ISHSUBMI + '</td>' +
                            '<td>' + res[i].ISHMATCODE + '</td>' +
                            '<td>' + res[i].ISHSTYLE + '</td>' +
                            '<td>' + res[i].ISHCOLOR + '</td>' +
                            '<td>' + res[i].ISHLOT + '</td>' +
                            '<td>' + res[i].ISHAMTFG + '</td>' +
                            '</tr>';

                }

                tableText += '</table>';

                return tableText;

            }
        </script>
    </head>

    <body onload="clearTable();">
        <div id="wrapper">

            <!-- /#sidebar-wrapper -->
            <%@ include file="../fragments/sidebar.jsp" %>

            <!-- Page Content -->
            <!-- nav-head-custom -->
            <%@ include file="../fragments/nav_head.jsp" %>

            <div class="container-fluid">
                <!--<div id="wrapper-top">-->
                <%-- PART 2 --%>
                <!--                    <div class="row">
                                                <div class="col-lg-12">
                                                    <div id="set-height" style="height:415px;margin-top:0px;">
                                                        <div id="sidebar-wrapper-top" class="">
                                                            <b class="page-header" style="padding-left:5px;font-size:18px;">
                <%-- <hr style="margin:0px;margin-top:10px;margin-bottom:10px;"> --%>
            </b>
        </div>
    </div>
    </div>
    </div> -->
                <%-- PART 3 --%>
                <!--show Qdest table-->
                <br>
                <div class="displayContain">
                    <table id="showTable" class="display displayTable">
                        <thead>
                            <tr style="background-color: #cbeaf2;">
                                <th style="text-align: right;">
                                    Job No. :
                                </th>
                                <th style="text-align: left;">
                                    <input type="text" id="jobNo" style="width: 150px;" value="${jobNo}" readonly>
                                </th>
                                <th style="text-align: left;">
                                    Warehouse : ${whn}
                                </th>
                                <th colspan="4" style="text-align: center;">
                                    <h3>SUMMARY</h3>
                                </th>
                                <th colspan="5" style="text-align: right;">
                                    <button type="button" id="backBtn" class="btn btn-default" style="width: 150px;">Back</button>
                                </th>
                            </tr>
                            <tr>
                                <th colspan="12">
                                    <div class="row">
                                        <div class="col-md-4" style="text-align: center;">
                                            วันที่จัดงาน :
                                            <input type="date" id="jobCreateDate" style=" width: 300px;">
                                        </div>
                                        <div class="col-md-4" style="text-align: center;">
                                            วันที่ตัดบัญชี :
                                            <input type="date" id="jobDebitDate" style=" width: 300px;">
                                        </div>
                                        <div class="col-md-4" style="text-align: center;">
                                            วันที่ขึ้นรถ :
                                            <input type="date" id="jobTransferDate" style=" width: 300px;">
                                        </div>
                                    </div>
                                </th>
                            </tr>
                            <tr>
                                <th style=" width: 100px;">ลำดับที่</th>
                                <th>รหัสวัตถุดิบ</th>
                                <th>ชื่อวัตถุดิบ</th>
                                <th>SAP ONHAND</th>
                                <th>WMS ONHAND</th>
                                <th>ปริมาณที่ต้องการ</th>
                                <th>DIFF</th>
                                <th>%</th>
                                <th>U/M</th>
                                <th>MTCTRL</th>
                                <th>USER</th>
                                <th>QUEUE NO.</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                            </tr>
                        </tfoot>
                        <tbody>
                        </tbody>
                    </table>
                    <span id="resLen">0</span> Rows
                    <center>
                        <div id="loadIcon" class="hidden">
                            <h3>
                                Loading...
                                <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
                            </h3>
                        </div>
                        <br>
                        <button type="button" id="createBtn" class="btn btn-warning" style="width: 150px;">Create SET Q <i id="loadIconBtn" class="fa fa-spinner fa-pulse fa-2x fa-fw hidden"></i></button>
                        <!--<button type="button" id="transferBtn" class="btn btn-success disabled" style="width: 150px;">Transfer SET Q</button>-->
                    </center>
                    <!--**********************-->
                    <!--End Part 3-->
                    <!--</div>  end #wrapper-top-->

                    <!-- Modal -->
                    <center>
                        <div id="myModal" class="modal fade" role="dialog">
                            <div class="modal-dialog">

                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">Warning</h4>
                                    </div>
                                    <div class="modal-body">
                                        <p>Plaese Select Date.</p>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </center>
                </div>
                <br>
            </div> <!-- end .container-fluid -->
        </div> <!-- end #wrapper -->
    </body>

</html>