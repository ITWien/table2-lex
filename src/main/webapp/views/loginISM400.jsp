<%@ include file="../includes/taglibs.jsp" %>
<%@ include file="../includes/imports.jsp" %>
<!DOCTYPE html>
<html lang="en">
    <%@ include file="../includes/head.jsp" %>
    <style>
        input[type=submit] {
            clear:none;
            width: 100%;
            background-color: #00399b;
            color: white;
            padding: 5px 5px;
            margin: 8px 0;
            border: none;
            border-radius: 4px;
            cursor: pointer;
        }

        input[type=submit]:hover {
            background-color: #3973d6;
        }
    </style>
    <body >

        <div id="wrapper">

            <!-- /#sidebar-wrapper -->
            <%@ include file="../fragments/sidebar.jsp" %>

            <!-- Page Content -->
            <!-- nav-head-custom -->
            <%@ include file="../includes/MSS_head.jsp" %>
            <div class="container-fluid">

                <form action="../resources/manual/ISM400.pdf" target="_blank">
                    <table width="100%">
                        <tr>
                            <td width="94%" align="left">
                            </td>
                            <td width="6%" align="right"><input type="submit" value="Manual" style="font-weight: normal;" /></td>
                        </tr>
                    </table>
                </form>

                <div class="row">
                    <div class="col-md-4 col-md-offset-4">
                        <div class="login-panel panel panel-primary panel-p">
                            <div class="panel-heading">
                                <h4 style="margin:0px;">Please Sign In</h4>
                            </div>
                            <div class="panel-body" style="background-color:#FBFBFB;">
                                <h1 style="text-align:center;font-size:100px;margin-bottom:35px;color:#226097;">
                                    <i class="fa fa-user"></i>
                                </h1>
                                <fieldset>

                                    <div  class="input-group">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                        <input id="user" class="form-control" placeholder="User" name="user" type="text" value="" title="User Login">
                                    </div>

                                    <div  class="input-group">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                                        <input id="password" class="form-control" placeholder="Password" name="password" type="password" value="" title="Password" >
                                    </div>
                                    <br>
                                    <a onclick="login()" class="btn btn-lg btn-primary btn-block btn-outline"><i class="fa fa-sign-in"></i> Login</a>
                                    <script type="text/javascript">

                                        function login() {
                                            var user = $("#user").val();
                                            var password = $("#password").val();

                                            if (password != "" && user != "") {
                                                $.get('../ISM400AJAX', {a: 'login', user: $("#user").val(), password: $("#password").val()}, function (responseText) {

                                                    if (responseText == 0) {
                                                        alertify.error("Incorrect password !");
                                                    } else {
                                                        location.reload();
                                                    }

                                                });
                                            } else {
                                                alertify.error("Please fill out password form.");
                                            }

                                        }

                                    </script>
                                </fieldset>
                            </div>
                        </div>
                    </div>
                </div>

                <script>

                    var sidebarVisible = localStorage.getItem('wrapper');
                    var checkIMenu = $("#i-menu").html();

                    $(document).ready(function () {

                        toggleClick();

                        toggleLoad();

                        getTime();

                        setInterval(function () {
                            getTime();
                        }, 1000);

                    });

                </script>

                </body>

                </html>