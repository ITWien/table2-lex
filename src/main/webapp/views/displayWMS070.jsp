<%@ include file="../fragments/taglibs.jsp" %>
<%@ include file="../fragments/imports.jsp" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.sql.*;" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>WMS</title>
        <!-- css :: vendors -->
        <jsp:include page = "../fragments/css.jsp" />
        <!-- additional custom :: my-style -->
        <link rel="stylesheet" href="../resources/assets/styles/myStyles.css">

        <!-- js :: vendors -->
        <jsp:include page = "../fragments/script.jsp" />
        <!-- additional custom :: my-script -->
        <script src="../resources/assets/scripts/myScripts.js" async></script>
        <script src="../resources/assets/scripts/toggleLoad.js" async></script>  
        <script src="../../WMSMenuControl/resources/assets/scripts/isLogin.js"></script>
        <script>
            $(document).ready(function () {
                $('#showTable').DataTable({
                    "lengthMenu": [[10, 25, 50, 100], [10, 25, 50, 100]],
                    "bSortClasses": false,
                    "autoWidth": false,
                    "columnDefs": [
                        {"width": "8%", "targets": 11},
                        {"width": "8%", "targets": 0},
                        {targets: 9, className: 'dt-body-right'}
                    ]
                });
                // Setup - add a text input to each footer cell
                $('#showTable tfoot th').each(function () {
                    var title = $(this).text();
                    $(this).html('<input type="text" />');
                });
                // DataTable
                var table = $('#showTable').DataTable();
                // Apply the search
                table.columns().every(function () {
                    var that = this;
                    $('input', this.footer()).on('keyup change', function () {
                        if (that.search() !== this.value) {
                            that
                                    .search(this.value)
                                    .draw();
                        }
                    });
                });
            });
        </script>
        <style>
            input[type=text], select {
                width: 100%;
                padding: 5px 5px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }

            input[type=date] {
                height: 35px;
                padding: 5px 5px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }

            input[type=submit] {
                width: 100%;
                background-color: #00399b;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=submit]:hover {
                background-color: #3973d6;
            }

            b, th, td {
                color: #134596;
            }
        </style>
    </head>    
    <body >
        <div id="wrapper">

            <!-- /#sidebar-wrapper -->
            <%@ include file="../fragments/sidebar.jsp" %>

            <!-- Page Content -->
            <!-- nav-head-custom -->
            <%@ include file="../fragments/nav_head.jsp" %>

            <div class="container-fluid">
                <!--<div id="wrapper-top">-->
                <%-- PART 2 --%>
                <!--                    <div class="row">
                                        <div class="col-lg-12">
                                            <div id="set-height" style="height:415px;margin-top:0px;">
                                                <div id="sidebar-wrapper-top" class="">
                                                    <b class="page-header" style="padding-left:5px;font-size:18px;">
                <%-- <hr style="margin:0px;margin-top:10px;margin-bottom:10px;"> --%>
            </b>
        </div>
    </div>
</div>
</div> -->
                <%-- PART 3 --%>
                <!--show Qdest table-->
                <form action="../resources/manual/WMS070.pdf" target="_blank">
                    <table width="100%">
                        <tr>
                            <td width="94%"></td>
                            <td width="6%" align="right"><input style="width: 100px;" type="submit" value="Manual" /></td>
                        </tr>
                    </table>
                </form>
                <div style="border: 2px solid #ccc; border-radius: 5px; padding: 20px;">
                    <form action="display" method="POST">
                        <table width="100%">
                            <tr>
                                <th width="90px"><b>คลังสินค้า : </b></th>
                                <td width="180px">
                                    <select name="warehouse">
                                        <option value="${mc}" selected hidden>${mc} : ${name}</option>
                                        <c:forEach items="${MCList}" var="p" varStatus="i">
                                            <option value="${p.uid}">${p.uid} : ${p.name}</option>
                                        </c:forEach>
                                    </select>
                                </td>
                                <td></td>
                                <td align="right" width="250px">
                                    <b>Select by Transaction Date : </b>
                                </td>
                                <td align="right" width="370px">
                                    <input type="date" id="startdate" name="startdate" value="${sd}"/> -
                                    <input type="date" id="enddate" name="enddate" value="${ed}"/>&nbsp;&nbsp;&nbsp;
                                </td>
                                <td align="right" width="6%">
                                    <input style="width: 100px;" type="submit" value="Search"/>
                                </td>
                            </tr>
                        </table>
                    </form>
                    <!--                <form action="display" method="POST">
                                        <div class="container" width="100%">
                                            <div class="row">
                                                <div class="col-2">
                                                    <b>คลังสินค้า : </b>
                                                </div>
                                                <div class="col-2">
                                                    <select name="warehouse" style="width: 200px;">
                                                        <option value="${mc}" selected hidden>${mc} : ${name}</option>
                    <c:forEach items="${MCList}" var="p" varStatus="i">
                        <option value="${p.uid}">${p.uid} : ${p.name}</option>
                    </c:forEach>
                </select>
            </div>
            <div class="col-2"></div>
            <div class="col-2">
                <b>Select by Transaction Date : </b>
            </div>
            <div class="col-2">
                <input type="date" id="startdate" name="startdate" value="${sd}"/> -
                <input type="date" id="enddate" name="enddate" value="${ed}"/>
            </div>
            <div class="col-2">
                <input style="width: 100px;" type="submit" value="Search"/>
            </div>
        </div>
    </div>
    </form>-->
                    <table id="showTable" class="display" style="width:100%; border: 1px solid #ccc; border-radius: 4px;">
                        <thead>
                            <tr>
                                <th>Trans Date & Time</th>
                                <th>MVT</th>
                                <th>TTYP</th>
                                <th>Material Code</th>
                                <th>Description</th>
                                <th>ID</th>
                                <th>Doc no.</th>
                                <th>SAP Doc no.</th>
                                <th>M Ctrl</th>
                                <th>Quantity</th>
                                <th>Unit</th>
                                <th>Create Date & Time</th>
                                <th>User ID</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>Trans Date & Time</th>
                                <th>MVT</th>
                                <th>TTYP</th>
                                <th>Material Code</th>
                                <th>Description</th>
                                <th>ID</th>
                                <th>Doc no.</th>
                                <th>SAP Doc no.</th>
                                <th>M Ctrl</th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                            </tr>
                        </tfoot>
                        <tbody>
                            <c:forEach items="${MCNList}" var="x" varStatus="i">
                                <tr onclick="window.location.href = '/TABLE2/WMS070/detail?rn=${x.matcode4}&wh=${mc}'">
                                    <td>${x.matcode}</td>
                                    <td>${x.desc}</td>
                                    <td>${x.plant}</td>
                                    <td>${x.vtype}</td>
                                    <td>${x.mtype}</td>
                                    <td>${x.um}</td>
                                    <td>${x.mgroup}</td>
                                    <td>${x.mgdesc}</td>
                                    <td>${x.matcode1}</td>
                                    <td>${x.matcode2}</td>
                                    <td>${x.pgroup}</td>
                                    <td>${x.matcode3}</td>
                                    <td>${x.location}</td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                    <!--End Part 3-->
                    <br>
                    <!--</div>  end #wrapper-top--> 
                </div> 
            </div> <!-- end .container-fluid -->
        </div> <!-- end #wrapper -->
    </body>
</html>