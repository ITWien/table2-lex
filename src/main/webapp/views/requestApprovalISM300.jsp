<%@ include file="../includes/taglibs.jsp" %>
<%@ include file="../includes/imports.jsp" %>
<!DOCTYPE html>
<html lang="en">
    <%@ include file="../includes/head.jsp" %>
    <body>

        <div id="wrapper">

            <!-- /#sidebar-wrapper -->
            <%@ include file="../includes/slidebar.jsp" %>

            <!-- Page Content -->
            <!-- nav-head-custom -->
            <%@ include file="../includes/MSS_head.jsp" %>
            <div class="container-fluid">
                <div id="wrapper-top">
                    <div class="row">
                        <div class="col-lg-12">
                            <div id="set-height" style="height:265px;margin-top:40px;">
                                <div id="sidebar-wrapper-top">
                                    <b class="page-header" style="padding-left:5px;font-size:18px;">
                                        <table>
                                            <tr>
                                                <td width="130px;">
                                                    Customer
                                                </td>
                                                <td width="20px;" align="center"> : </td>
                                                <td>
                                                    <input disabled id="sPOFG" value="${POFG}" class="form-control" type="text" onkeyup="javascript:this.value = this.value.toUpperCase();getCustomerName();getDropDownGroup();getDropDownSQ();">
                                                </td>
                                                <td>
                                                    <div id="customerName" style="margin-left:20px;color:#777;font-size:16px;"></div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Mat-Ctrl
                                                </td>
                                                <td width="20px;" align="center"> : </td>
                                                <td>
                                                    <input disabled id="sLGPBE" value="${LGPBE}" class="form-control" style="margin-top:10px;" type="text" onkeyup="javascript:this.value = this.value.toUpperCase();getMatName();">
                                                </td>
                                                <td>
                                                    <div id="MatName" style="margin-left:20px;color:#777;font-size:16px;"></div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Job no.
                                                </td>
                                                <td width="20px;" align="center"> : </td>
                                                <td>
                                                    <div id="dpGRPNO" style="margin-top:10px;">
                                                        <input class="form-control" type="text" disabled>
                                                    </div>
                                                </td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Sequence no.
                                                </td>
                                                <td width="20px;" align="center"> : </td>
                                                <td>
                                                    <div id="dpSEQNO" style="margin-top:10px;">
                                                        <input class="form-control" type="text" disabled>
                                                    </div>
                                                </td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td></td>
                                                <td height="34px;">
                                                    <div id="checkB" style="font-size:17px;margin-top:10px;padding-left:5px;color:#777">
                                                        <!-- <input id="sCHECK" type="checkbox" > Not enough -->
                                                    </div>
                                                </td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td></td>
                                                <td>
                                                    <!-- <button onclick="search()" style="width:100%;margin-top:10px;" class="btn btn-outline btn-primary">
                                                        <b><i class="fa fa-search icon-size-sm"></i> Search</b>
                                                    </button> -->
                                                </td>
                                                <td></td>
                                            </tr>
                                        </table>
                                    </b>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="toggle-top" style="position:relative;">
                        <button onclick="closeWin()" type="button" style="position:absolute;left:0 ;top:-40px;width:15%;z-index: 2000;font-weight:bold;" class="btn btn-outline btn-danger"><i class="fa fa-reply"></i> Exit</button>

                        <script type="text/javascript">

                            function closeWin() {
//                                close();
                                window.location = "MC?a=list&POFG=${POFG}&LGPBE=${LGPBE}&GRPNO=${GRPNO}&ISSUENO=${ISSUENO}";
                            }

                        </script>

                        <button type="button" onclick="send()" style="position:absolute;right:15.5% ;top:-40px;width:15%;z-index: 2000;font-weight:bold;" class="btn btn-outline btn-warning">Send to approve</button>

                        <!-- <button type="button" onclick="send()" style="position:absolute;right:16% ;top:-40px;width:15%;z-index: 2000;font-weight:bold;" class="btn btn-outline btn-warning">Send to logistics</button> -->

                        <a style="position:absolute;right:0 ;top:-40px;width:15%;z-index: 2000;" href="#menu-toggle2" class="btn btn-outline btn-default  custom-toggle-menu" id="menu-toggle2">
                            <div id="i-toggle-top"></div>
                        </a>
                        <hr style="margin:0px;margin-top:10px;margin-bottom:10px;">
                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-primary" style="min-height: 485px;">
                                <div class="panel-heading">
                                    <h4 style="margin:0px;">Request Approval</h4>
                                </div>
                                <!-- /.panel-heading -->
                                <div id="panel-body" class="panel-body">
                                    <div class="dataTable_wrapper">
                                        <form id="checkForm" action="../ISM300AJAX" method="post">
                                            <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                                <thead>
                                                    <tr>
                                                        <th style="border-bottom:0px;">Material</th>
                                                        <th style="border-bottom:0px;">Description</th>
                                                        <th style="border-bottom:0px;">Unit</th>
                                                        <th style="border-bottom:0px;">T. Req.</th>
                                                        <th style="border-bottom:0px;">T. Pick.</th>
                                                        <th style="border-bottom:0px;">Percent</th>
                                                        <th style="border-bottom:0px;">Status</th>
                                                        <th style="border-bottom:0px;text-align:center"><input type="checkbox" id="select_all"/> A. <b hidden><input type="checkbox" id="select_all2"/> L.</b></th>
                                                    </tr>
                                                </thead>

                                                <tfoot>
                                                <th style="border-left:0px;">Material</th>
                                                <th style="border-left:0px;">Description</th>
                                                <th style="border-left:0px;">Unit</th>
                                                <th style="border-left:0px;">T. Req.</th>
                                                <th style="border-left:0px;">T. Pick.</th>
                                                <th style="border-left:0px;">Percent</th>
                                                <th style="border-left:0px;">Status</th>
                                                <th style="border-left:0px;"></th>
                                                </tfoot>
                                                <tbody>
                                                    <c:forEach items="${listIssue}" var="MPDATAD">
                                                        <tr>
                                                            <td><c:out value="${MPDATAD.MATNR}" escapeXml="false" /></td>
                                                            <td ><c:out value="${MPDATAD.ARKTX}" escapeXml="false" /></td>
                                                            <td align="center" style=""><c:out value="${MPDATAD.VRKME}" escapeXml="false" /></td>
                                                            <td align="right" style="min-width:90px;"><fmt:formatNumber type="number" pattern="###,##0.000" value="${MPDATAD.KWMENG}" /></td>
                                                            <td align="right" style="min-width:80px;"><fmt:formatNumber type="number" pattern="###,##0.000" value="${MPDATAD.ISSUETOPICK}" /></td>
                                                            <c:choose>
                                                                <c:when test="${MPDATAD.PERCENT*100 >= 105}">
                                                                    <td align="right" style="position: relative;color:red;font-weight:bold">
                                                                        <i style="position: absolute;left:10px;" class="fa fa-check"></i> +
                                                                        <fmt:formatNumber type="percent" maxIntegerDigits="3" value="${MPDATAD.PERCENT}" />
                                                                    </td>
                                                                </c:when>
                                                                <c:when test="${MPDATAD.PERCENT*100 >= 0 && MPDATAD.PERCENT*100 < 105}">
                                                                    <td align="right" style="position: relative;color:green;font-weight:bold">
                                                                        <i style="position: absolute;left:10px;" class="fa fa-check"></i> +
                                                                        <fmt:formatNumber type="percent" maxIntegerDigits="3" value="${MPDATAD.PERCENT}" />
                                                                    </td>
                                                                </c:when>
                                                                <c:when test="${MPDATAD.PERCENT*100 == -1}">
                                                                    <td align="right" style="position: relative;color:orange;font-weight:bold">
                                                                        <i style="position: absolute;left:10px;" class="fa fa-times"></i>
                                                                        <fmt:formatNumber type="percent" maxIntegerDigits="3" value="${MPDATAD.PERCENT}" />
                                                                    </td>
                                                                </c:when>
                                                                <c:otherwise>
                                                                    <td align="right" style="position: relative;color:#226097;font-weight:bold">
                                                                        <i style="position: absolute;left:10px;" class="fa fa-exclamation"></i>
                                                                        <fmt:formatNumber type="percent" maxIntegerDigits="3" value="${MPDATAD.PERCENT}" />
                                                                    </td>
                                                                </c:otherwise>
                                                            </c:choose>
                                                            <c:choose>
                                                                <c:when test="${MPDATAD.STATUS == 0}">
                                                                    <td align="center" style="color:#226097;font-weight:bold">
                                                                    </c:when>
                                                                    <c:when test="${MPDATAD.STATUS == 1}">
                                                                    <td align="center" style="color:orange;font-weight:bold">
                                                                    </c:when>
                                                                    <c:when test="${MPDATAD.STATUS == 2}">
                                                                    <td align="center" style="color:green;font-weight:bold">
                                                                    </c:when>
                                                                    <c:when test="${MPDATAD.STATUS == 3}">
                                                                    <td align="center" style="color:#B512A4;font-weight:bold">
                                                                    </c:when>
                                                                    <c:when test="${MPDATAD.STATUS == 4}">
                                                                    <td align="center" style="color:#226097;font-weight:bold;">
                                                                    </c:when>
                                                                    <c:when test="${MPDATAD.STATUS == 5}">
                                                                    <td align="center" style="color:orange;font-weight:bold;">
                                                                    </c:when>
                                                                    <c:when test="${MPDATAD.STATUS == 6}">
                                                                    <td align="center" style="color:green;font-weight:bold;">
                                                                    </c:when>
                                                                    <c:when test="${MPDATAD.STATUS == 9}">
                                                                    <td align="center" style="color:red;font-weight:bold">
                                                                    </c:when>
                                                                    <c:otherwise>
                                                                    <td align="center" style="font-weight:bold">
                                                                    </c:otherwise>
                                                                </c:choose>
                                                                <c:out value="${MPDATAD.STATUSDESC}" escapeXml="false" />
                                                            </td>
                                                            <td align="center">
                                                                <c:choose>
                                                                    <c:when test="${(MPDATAD.STATUS == 0 || MPDATAD.STATUS == 9) && MPDATAD.PERCENT >= 0}">
                                                                        <input type="checkbox" class="checkbox"  name="nCheck" value="1|${GRPNO}|${ISSUENO}|${fn:trim(MPDATAD.MATNR)}">
                                                                    </c:when>
                                                                    <c:when test="${(MPDATAD.STATUS == 2)}">
                                                                        <input type="checkbox" class="checkbox"  name="nCheck" value="2|${GRPNO}|${ISSUENO}|${fn:trim(MPDATAD.MATNR)}">
                                                                    </c:when>
                                                                    <c:otherwise>
                                                                        <input type="checkbox" class="checkbox" name="nCheck" value="1|${GRPNO}|${ISSUENO}|${fn:trim(MPDATAD.MATNR)}">
                                                                    </c:otherwise>
                                                                </c:choose>
                                                            </td>
                                                        </tr>
                                                    </c:forEach>
                                                </tbody>
                                            </table>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <script>

                var sidebarVisible = localStorage.getItem('wrapper');
                var sidebarVisible2 = localStorage.getItem('wrapper-top');
                var checkIMenu = $("#i-menu").html();
                var checkToggleTop = $("#i-toggle-top").html();

                $(document).ready(function () {

                    $("#wrapper").css({"-webkit-transition": "all 0.0s ease"}, {"-moz-transition": "all 0.0s ease"}, {"-o-transition": "all 0.0s ease"}, {"transition": "all 0.0s ease"});

                    $("#sidebar-wrapper").css({"-webkit-transition": "all 0.0s ease"}, {"-moz-transition": "all 0.0s ease"}, {"-o-transition": "all 0.0s ease"}, {"transition": "all 0.0s ease"});

                    $("#menu-toggle").css({"display": "none"});

                    $("#mHead").css({"margin-top": "11px"});

                    $("#wrapper").toggleClass("toggled");

                    toggleClick2();

                    toggleLoad2();

                    getTime();

                    getCustomerName();

                    getMatName();

                    getDropDownGroup();

                    getDropDownSQ();

                    if ("${CHECK}" == "1") {
                        $('#sCHECK').prop('checked', true);
                    }

                    setInterval(function () {
                        getTime();
                    }, 1000);

                    $('#dataTables-example').DataTable({
                        "aLengthMenu": [[6, 20, 50, -1], [6, 20, 50, "All"]],
                        "iDisplayLength": -1,
                        "bStateSave": true,
                        "aoColumnDefs": [
                            {'bSortable': false, 'aTargets': [1]},
                            {'bSortable': false, 'aTargets': [7]}
                        ],

                        responsive: true
                    });

                    $('#dataTables-example tfoot th').not(":eq(7)").each(function () {
                        var title = $(this).text();
                        $(this).html('<input type="text" class="form-control input-sm" style="width:100%" placeholder="' + title + '" />');
                    });

                    // DataTable
                    var table = $('#dataTables-example').DataTable();

                    // Apply the search
                    table.columns().every(function () {
                        var that = this;

                        $('input', this.footer()).on('keyup change', function () {
                            if (that.search() !== this.value) {
                                that.search(this.value).draw();
                            }
                        });
                    });

                    var state = table.state.loaded();
                    if (state) {
                        table.columns().eq(0).each(function (colIdx) {
                            var colSearch = state.columns[colIdx].search;

                            if (colSearch.search) {
                                $('input', table.column(colIdx).footer()).val(colSearch.search);
                            }
                        });

                        table.draw();
                    }

                    //select all checkboxes
                    $("#select_all").change(function () {  //"select all" change
                        if ($("#select_all2")[0].checked == true) {
                            $("#select_all2").click();
                            $("#select_all").click();
                        }
                        var status = this.checked; // "select all" checked status
                        $('.checkbox').each(function () { //iterate all listed checkbox items
                            if ($(this).val().substring(0, 1) == "1") {
                                this.checked = status; //change ".checkbox" checked status
                            }
                        });
                    });

                    //uncheck "select all", if one of the listed checkbox item is unchecked
                    $('.checkbox').change(function () { //".checkbox" change
                        if (this.checked == false) { //if this item is unchecked
                            $("#select_all")[0].checked = false; //change "select all" checked status to false
                        }
                    });

                    //select all checkboxes
                    $("#select_all2").change(function () {  //"select all" change
                        if ($("#select_all")[0].checked == true) {
                            $("#select_all").click();
                            $("#select_all2").click();
                        }
                        var status = this.checked; // "select all" checked status
                        $('.checkbox').each(function () { //iterate all listed checkbox items
                            if ($(this).val().substring(0, 1) == "2") {
                                this.checked = status; //change ".checkbox" checked status
                            }
                        });
                    });

                    //uncheck "select all", if one of the listed checkbox item is unchecked
                    $('.checkbox').change(function () { //".checkbox" change
                        if (this.checked == false) { //if this item is unchecked
                            $("#select_all2")[0].checked = false; //change "select all" checked status to false
                        }
                    });
                });

                (function ($) {
                    $.fn.autosubmit = function () {
                        this.submit(function (event) {
                            event.preventDefault();
                            var form = $(this);
                            $.ajax({
                                type: form.attr('method'),
                                url: form.attr('action'),
                                data: form.serialize()
                            }).done(function (data) {
                                // Optionally alert the user of success here...
                            }).fail(function (data) {
                                // Optionally alert the user of an error here...
                            });
                        });
                        return this;
                    }
                })(jQuery)

                function toggleLoad2() {
                    if (sidebarVisible2 != null && sidebarVisible2 == 1) {
                        $("#wrapper-top").css({"-webkit-transition": "all 0.0s ease"}, {"-moz-transition": "all 0.0s ease"}, {"-o-transition": "all 0.0s ease"}, {"transition": "all 0.0s ease"});
                        $("#sidebar-wrapper-top").css({"-webkit-transition": "all 0.0s ease"}, {"-moz-transition": "all 0.0s ease"}, {"-o-transition": "all 0.0s ease"}, {"transition": "all 0.0s ease"});
                        $("#wrapper-top").toggleClass("toggled");
                        $("#i-toggle-top").html("<b class=\"custom-text\">Show search</b>");
                        // $("#set-height").css({"margin-top":"50px"});
                    } else {
                        $("#i-toggle-top").html("<b class=\"custom-text\">Hide search</b>");
                    }
                }

                function toggleClick2() {
                    $("#menu-toggle2").click(function (e) {

                        e.preventDefault();
                        $("#wrapper-top").toggleClass("toggled");

                        if ($("#i-toggle-top").html() == "<b class=\"custom-text\">Show search</b>") {
                            $("#wrapper-top").css({"-webkit-transition": "all 0.5s ease"}, {"-moz-transition": "all 0.5s ease"}, {"-o-transition": "all 0.5s ease"}, {"transition": "all 0.5s ease"});
                            $("#sidebar-wrapper-top").css({"-webkit-transition": "all 0.5s ease"}, {"-moz-transition": "all 0.5s ease"}, {"-o-transition": "all 0.5s ease"}, {"transition": "all 0.5s ease"});
                            $("#i-toggle-top").html("<b class=\"custom-text\">Hide search</b>");
                            // $("#toggle-top").css({"margin-top":"0px"});
                            localStorage.setItem('wrapper-top', 0);
                        } else {
                            $("#i-toggle-top").html("<b class=\"custom-text\">Show search</b>");
                            // $("#toggle-top").css({"margin-top":"50px"});
                            localStorage.setItem('wrapper-top', 1);
                        }

                    });
                }


                function getCustomerName() {
                    var POFG = $("#sPOFG").val();
                    $.get('../ISM300AJAX', {a: 'getCustomerName', POFG: POFG}, function (responseText) {
                        $('#customerName').html(responseText);
                    });
                }

                function getMatName() {
                    var LGPBE = $("#sLGPBE").val();
                    $.get('../ISM300AJAX', {a: 'getMatName', LGPBE: LGPBE}, function (responseText) {
                        $('#MatName').html(responseText);
                    });
                }

                function getDropDownGroup() {
                    var POFG = $("#sPOFG").val();
                    $.get('../ISM300AJAX', {a: 'getDropDownGroup', POFG: POFG}, function (responseText) {
                        if (responseText != "null") {
                            $('#dpGRPNO').html(responseText);
                            $('#sGRPNO').val("${GRPNO}");
                            $('#sGRPNO').attr('disabled', 'disabled');
                        } else {
                            $('#dpGRPNO').html("<input class=\"form-control\" type=\"text\" disabled>");
                        }
                    });
                }

                function getDropDownSQ() {
                    var POFG = $("#sPOFG").val();
                    $.get('../ISM300AJAX', {a: 'getDropDownSQ', POFG: POFG}, function (responseText) {
                        if (responseText != "null") {
                            $('#dpSEQNO').html(responseText);
                            $('#sISSUENO').val("${ISSUENO}");
                            $('#sISSUENO').attr('disabled', 'disabled');
                        } else {
                            $('#dpSEQNO').html("<input class=\"form-control\" type=\"text\" disabled>");
                        }
                    });
                }

                function search() {

                    var POFG = $('#sPOFG').val();
                    var LGPBE = $("#sLGPBE").val();
                    var GRPNO = $("#sGRPNO").val();
                    var ISSUENO = $("#sISSUENO").val();

                    if (POFG == "" || LGPBE == "" || GRPNO == "undefined" || ISSUENO == "undefined") {
                        alertify.error("Please fill out search form.");
                    } else {
                        window.location = "MC?a=list&POFG=" + POFG + "&LGPBE=" + LGPBE + "&GRPNO=" + GRPNO + "&ISSUENO=" + ISSUENO;
                    }

                }

                function send() {
                    status = 0;

                    $('.checkbox').each(function () { //iterate all listed checkbox items
                        if (this.checked == true) {
                            status = 1;
                        }
                    });

                    if (status == 1) {
                        $('#panel-body').loader('show', '<i style="font-size:100px;" class="fa fa-refresh fa-spin"></i>');
                        var frm = $('#checkForm');
                        $.ajax({
                            type: frm.attr('method'),
                            url: frm.attr('action'),
                            data: frm.serialize(),
                            success: function (data) {
                                location.reload();
                            }
                        });
                    } else {
                        alertify.error("Please select one or more data.");
                    }

                }

            </script>

    </body>

</html>