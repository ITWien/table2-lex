<%@ include file="../fragments/taglibs.jsp" %>
<%@ include file="../fragments/imports.jsp" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.sql.*;" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>WMS</title>
        <!-- css :: vendors -->
        <jsp:include page = "../fragments/css.jsp" />
        <!-- additional custom :: my-style -->
        <link rel="stylesheet" href="../resources/assets/styles/myStyles.css">

        <!-- js :: vendors -->
        <jsp:include page = "../fragments/script.jsp" />
        <!-- additional custom :: my-script -->
        <script src="../resources/assets/scripts/myScripts.js" async></script>
        <script src="../resources/assets/scripts/toggleLoad.js" async></script> 
        ${sendMessage}
        <style>
            input[type=text], select{
                width: 100%;
                padding: 5px 5px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }

            input[type=date], input[type=number]{
                /*width: 100%;*/
                padding: 5px 5px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }

            input[type=submit] {
                width: 100%;
                background-color: #00399b;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=submit]:hover {
                background-color: #3973d6;
            }

            input[type=button] {
                width: 100%;
                background-color: #ef604a;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=button]:hover {
                background-color: #e51e00;
            }
        </style>
        <style>
            body {font-family: Arial, Helvetica, sans-serif;}

            /* The Modal (background) */
            .modal {
                display: none; /* Hidden by default */
                position: fixed; /* Stay in place */
                z-index: 1; /* Sit on top */
                padding-top: 100px; /* Location of the box */
                left: 0;
                top: 0;
                width: 100%; /* Full width */
                height: 100%; /* Full height */
                overflow: auto; /* Enable scroll if needed */
                background-color: rgb(0,0,0); /* Fallback color */
                background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
            }

            /* Modal Content */
            .modal-content {
                background-color: #fefefe;
                margin: auto;
                padding: 20px;
                border: 1px solid #888;
                width: fit-content;
                height: 320px;
            }

            /* The Close Button */
            .close {
                color: #aaaaaa;
                float: right;
                font-size: 28px;
                font-weight: bold;
            }

            .close:hover,
            .close:focus {
                color: #000;
                text-decoration: none;
                cursor: pointer;
            }

            button[name=ok] {
                width: 25%;
                background-color: #008cff;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            button[name=ok]:hover {
                background-color: #008cff;
            }
        </style>
        <script>
            $(document).ready(function () {
                $('#showTable').DataTable({
                    "lengthMenu": [[10, 25, 50, 100], [10, 25, 50, 100]],
                    "bSortClasses": false,
                    "columnDefs": [
                        {orderable: false, targets: [3]},
                        {orderable: false, targets: [4]},
                        {orderable: false, targets: [5]},
                        {"width": "35%", "targets": 1}
                    ]
                });

                // Setup - add a text input to each footer cell
                $('#showTable tfoot th').not(":eq(3)").not(":eq(3)").not(":eq(3)").each(function () {
                    var title = $(this).text();
                    $(this).html('<input type="text" />');
                });

                // DataTable
                var table = $('#showTable').DataTable();

                // Apply the search
                table.columns().every(function () {
                    var that = this;
                    $('input', this.footer()).on('keyup change', function () {
                        if (that.search() !== this.value) {
                            that
                                    .search(this.value)
                                    .draw();
                        }
                    });
                });
            });
        </script>
    </head>    
    <body onload="document.getElementById('userid').value = sessionStorage.getItem('uid');">
        <div id="wrapper">

            <!-- /#sidebar-wrapper -->
            <%@ include file="../fragments/sidebar.jsp" %>

            <!-- Page Content -->
            <!-- nav-head-custom -->
            <%@ include file="../fragments/nav_head.jsp" %>

            <div class="container-fluid">
                <!--<div id="wrapper-top">-->
                <%-- PART 2 --%>
                <!--                    <div class="row">
                                        <div class="col-lg-12">
                                            <div id="set-height" style="height:415px;margin-top:0px;">
                                                <div id="sidebar-wrapper-top" class="">
                                                    <b class="page-header" style="padding-left:5px;font-size:18px;">
                <%-- <hr style="margin:0px;margin-top:10px;margin-bottom:10px;"> --%>
            </b>
        </div>
    </div>
</div>
</div> -->
                <%-- PART 3 --%>
                <!--show Qdest table-->
                <form action="../resources/manual/WMS120.pdf" target="_blank">
                    <table width="100%">
                        <tr>
                            <td width="94%"></td>
                            <td width="6%" align="right"><input type="submit" value="Manual" /></td>
                        </tr>
                    </table>
                </form>
                <div style="border: 2px solid #fff; border-radius: 5px; padding: 20px;">
                    <form id="frm" name="frm" action="display" method="post">
                        <input type="hidden" id="userid" name="userid">
                        <table width="100%">
                            <tr>
                                <td width="70%" align="right"><b style="color: #00399b; font-size: 20px;">Pallet & QR Code : </b>
                                    <input type="text" name="from" id="from" style="width:60%;" autofocus>
                                    <script>
                                        var input = document.getElementById("from");
                                        input.addEventListener("keyup", function (event) {
                                            if (event.keyCode === 13) {
                                                event.preventDefault();
                                                document.getElementById("from").value = document.getElementById("from").value.toString().trim().split("|")[0];
                                                document.getElementById("to").value = document.getElementById("to").value.toString().trim().split("|")[0];
                                                document.getElementById('frm').submit();
                                            } else {
                                                document.getElementById("from").value = document.getElementById("from").value.toUpperCase();
                                            }
                                        });
                                    </script>
                                    &nbsp;&nbsp;
                                    <i class="fa fa-qrcode" aria-hidden="true" style="font-size:30px;"></i>
                                </td>
                                <td width="30%" align="right"><b style="color: #00399b; font-size: 20px;">To Pallet : </b>
                                    <input type="text" name="to" id="to" style="width:60%;">
                                    <script>
                                        var input = document.getElementById("to");
                                        input.addEventListener("keyup", function (event) {
                                            if (event.keyCode === 13) {
                                                event.preventDefault();
                                                document.getElementById("from").value = document.getElementById("from").value.toString().trim().split("|")[0];
                                                document.getElementById("to").value = document.getElementById("to").value.toString().trim().split("|")[0];
                                                document.getElementById('frm').submit();
                                            } else {
                                                document.getElementById("to").value = document.getElementById("to").value.toUpperCase();
                                            }
                                        });
                                    </script>
                                    &nbsp;&nbsp;
                                    <i class="fa fa-university" aria-hidden="true" style="font-size:25px;"></i>
                                </td>
                            </tr>
                        </table>                    
                        <table id="showTable" class="display" style="width:100%; border: 1px solid #ccc; border-radius: 4px;">
                            <thead>
                                <tr>
                                    <th style="text-align: center;">Material Code</th>
                                    <th style="text-align: center;">Description</th>
                                    <th style="text-align: center;">ID</th>
                                    <th style="text-align: center;">From Pallet</th>
                                    <th style="text-align: center;">To Pallet</th>
                                    <th style="text-align: center;">Delete</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </tfoot>
                            <tbody>
                                <c:forEach items="${PALList}" var="x">
                                <div style="display: none;"><input type="text" id="QID" name="QID" value="${x.id}"></div>
                                <div style="display: none;"><input type="text" id="ID" name="ID" value="${x.no}"></div>
                                <div style="display: none;"><input type="text" id="TO" name="TO" value="${x.to}"></div>
                                <tr>                                        
                                    <td>${x.code}</td>
                                    <td>${x.desc}</td>
                                    <td align="center">${x.id}</td>
                                    <td align="center"><input type="text" value="${x.from}" style=" width: 150px; text-align: center;" disabled></td>
                                    <td align="center"><input type="text" value="${x.to}" style=" width: 150px; text-align: center;" disabled></td>
                                    <td align="center">
                                        <a onclick="document.getElementById('myModal-4-${x.no}').style.display = 'block';" style=" cursor: pointer;"><i class="fa fa-trash" aria-hidden="true" style="font-size:25px;"></i></a>
                                        <div id="myModal-4-${x.no}" class="modal">
                                            <!-- Modal content -->
                                            <div class="modal-content" style=" height: 300px; width: 500px;">
                                                <span id="span-4-${x.no}" class="close" onclick="document.getElementById('myModal-4-${x.no}').style.display = 'none';">&times;</span>
                                                <p><b style="color: #00399b;"><font size="5">Do you want to delete ?</font></b></p>
                                                <table width="100%">
                                                    <tr>
                                                        <td align="right" width="50%">
                                                            <b><font size="4">Material Code :</font></b><br>
                                                            <b><font size="4">Description :</font></b><br>
                                                            <b><font size="4">ID :</font></b><br>
                                                            <b><font size="4">From Pallet :</font></b><br>
                                                            <b><font size="4">To Pallet :</font></b><br><br><br>
                                                            <a class="btn btn btn-outline btn-danger" onclick="document.getElementById('myModal-4-${x.no}').style.display = 'none';">
                                                                <i class="fa fa-times-circle-o" style="font-size:20px;"></i> Cancel</a> 
                                                        </td>
                                                        <td align="left" width="50%">
                                                            <b><font size="4">${x.code}</font></b><br>
                                                            <b><font size="4">${x.desc}</font></b><br>
                                                            <b><font size="4">${x.id}</font></b><br>
                                                            <b><font size="4">${x.from}</font></b><br>
                                                            <b><font size="4">${x.to}</font></b><br><br><br>
                                                            <a class="btn btn btn-outline btn-success" onclick="window.location.href = '/TABLE2/WMS120/delete?no=${x.no}'">
                                                                <i class="fa fa-check-circle-o" style="font-size:20px;"></i> Confirm</a>  
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>

                                        </div>
                                    </td>
                                </tr>
                            </c:forEach>
                            </tbody>
                        </table>
                        <center>
                            <button type="button" class="btn btn-primary" onclick="document.getElementById('myModal-3').style.display = 'block';"><i class="fa fa-floppy-o" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Confirm</button>
                            <button type="button" class="btn btn-danger" onclick="document.getElementById('myModal-4').style.display = 'block';"><i style="font-size: 18px;" class="fa fa-times-circle-o" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Clear All</button>
                        </center>                        
                        <br><br><br>
                        <center>
                            <div id="myModal2" class="modal fade" role="dialog">
                                <div class="modal-dialog modal-sm"> <!-- ???????????? modal ?????????????? ???? xs, sm, md, lg -->
                                    <div class="modal-md col-md-6" style="top: 150px; left:300px; background-color: #ddd; border: 1px solid #ccc; border-radius: 4px;">
                                        <div class="modal-header">
                                            <h4 class="modal-title" align="center">From Pallet Result</h4>
                                            <br>
                                            ${Success}
                                            ${Fail}
                                            ${Duplicate}
                                            ${nodata}
                                            <br>
                                            <button name="ok" type="button" onclick="window.location.href = '/TABLE2/WMS120/display'">
                                                <font color = "white">OK</font>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- devbanban.com -->
                        </center>
                        <center>
                            <div id="myModal3" class="modal fade" role="dialog">
                                <div class="modal-dialog modal-sm"> <!-- ???????????? modal ?????????????? ???? xs, sm, md, lg -->
                                    <div class="modal-md col-md-6" style="top: 150px; left:300px; background-color: #ddd; border: 1px solid #ccc; border-radius: 4px;">
                                        <div class="modal-header">
                                            <h4 class="modal-title" align="center">To Pallet Result</h4>
                                            <br>
                                            ${Success}
                                            ${Fail}
                                            <br>
                                            <button name="ok" type="button" onclick="window.location.href = '/TABLE2/WMS120/display'">
                                                <font color = "white">OK</font>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- devbanban.com -->
                        </center>
                        <div id="myModal-3" class="modal">
                            <!-- Modal content -->
                            <div class="modal-content" style=" height: 200px; width: 500px;">
                                <span id="span-3" class="close" onclick="document.getElementById('myModal-3').style.display = 'none';">&times;</span>
                                <p><b><font size="4"></font></b></p>
                                <table width="100%">
                                    <tr>
                                        <td align="center">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <b style="color: #00399b;"><font size="5">Confirm Pallet Movement ?</font></b>
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <br><br><br>
                                            <a class="btn btn btn-outline btn-danger" onclick="document.getElementById('myModal-3').style.display = 'none';">
                                                <i class="fa fa-times-circle-o" style="font-size:20px;"></i> Cancel</a>  
                                            &nbsp;
                                            <a class="btn btn btn-outline btn-success" onclick="document.getElementById('frm').action = 'edit'; document.getElementById('frm').submit();">
                                                <i class="fa fa-check-circle-o" style="font-size:20px;"></i> Confirm</a>                    
                                        </td>
                                    </tr>
                                </table>
                            </div>

                        </div>
                        <div id="myModal-4" class="modal">
                            <!-- Modal content -->
                            <div class="modal-content" style=" height: 200px; width: 500px;">
                                <span id="span-4" class="close" onclick="document.getElementById('myModal-4').style.display = 'none';">&times;</span>
                                <p><b><font size="4"></font></b></p>
                                <table width="100%">
                                    <tr>
                                        <td align="center">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <b style="color: #00399b;"><font size="5">Clear All Pallet Movement ?</font></b>
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <br><br><br>
                                            <a class="btn btn btn-outline btn-danger" onclick="document.getElementById('myModal-4').style.display = 'none';">
                                                <i class="fa fa-times-circle-o" style="font-size:20px;"></i> Cancel</a>  
                                            &nbsp;
                                            <a class="btn btn btn-outline btn-success" onclick="document.getElementById('frm').action = 'deleteAll'; document.getElementById('frm').submit();">
                                                <i class="fa fa-check-circle-o" style="font-size:20px;"></i> Confirm</a>                    
                                        </td>
                                    </tr>
                                </table>
                            </div>

                        </div>

                    </form>
                    <!--**********************-->
                    <!--End Part 3-->
                    <!--</div>  end #wrapper-top--> 
                </div> 
                <br>
            </div> <!-- end .container-fluid -->
        </div> <!-- end #wrapper -->
    </body>
</html>