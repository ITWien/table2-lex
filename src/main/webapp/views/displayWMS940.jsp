<%@ include file="../fragments/taglibs.jsp" %>
<%@ include file="../fragments/imports.jsp" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.sql.*;" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>WMS</title>
        <!-- css :: vendors -->
        <jsp:include page = "../fragments/css.jsp" />
        <!-- additional custom :: my-style -->
        <link rel="stylesheet" href="../resources/assets/styles/myStyles.css">

        <!-- js :: vendors -->
        <jsp:include page = "../fragments/script.jsp" />
        <!-- additional custom :: my-script -->
        <script src="../resources/assets/scripts/myScripts.js" async></script>
        <script src="../resources/assets/scripts/toggleLoad.js" async></script> 
        <style>
            input[type=text], select{
                width: 100%;
                padding: 5px 5px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }

            input[type=date], input[type=number]{
                /*width: 100%;*/
                padding: 5px 5px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }

            input[type=submit] {
                width: 100%;
                background-color: #00399b;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=submit]:hover {
                background-color: #3973d6;
            }

            input[type=button] {
                width: 100%;
                background-color: #ef604a;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=button]:hover {
                background-color: #e51e00;
            }

            #sts0 {
                width: 40px;
                background-color: #0e50ba;
                color: white;
                padding: 5px 5px;
                /*margin: 8px 0;*/
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }
            #sts1 {
                width: 40px;
                background-color: #002c72;
                color: white;
                padding: 5px 5px;
                /*margin: 8px 0;*/
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }
            #sts2 {
                width: 40px;
                background-color: #28d65f;
                color: white;
                padding: 5px 5px;
                /*margin: 8px 0;*/
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }
            #sts5 {
                width: 40px;
                background-color: #5e00b7;
                color: white;
                padding: 5px 5px;
                /*margin: 8px 0;*/
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }
            #sts6 {
                width: 40px;
                background-color: #00af89;
                color: white;
                padding: 5px 5px;
                /*margin: 8px 0;*/
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }
            #sts7 {
                width: 40px;
                background-color: #002aa0;
                color: white;
                padding: 5px 5px;
                /*margin: 8px 0;*/
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }
            #sts8 {
                width: 40px;
                background-color: #d61717;
                color: white;
                padding: 5px 5px;
                /*margin: 8px 0;*/
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }
            #sts9 {
                width: 40px;
                background-color: #d61717;
                color: white;
                padding: 5px 5px;
                /*margin: 8px 0;*/
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }
            #stsC {
                width: 40px;
                background-color: #000000;
                color: white;
                padding: 5px 5px;
                /*margin: 8px 0;*/
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }
            #stsD {
                width: 40px;
                background-color: #ff9400;
                color: white;
                padding: 5px 5px;
                /*margin: 8px 0;*/
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }
            #stsH {
                width: 40px;
                background-color: #12a9b7;
                color: white;
                padding: 5px 5px;
                /*margin: 8px 0;*/
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }
            #stsR {
                width: 40px;
                background-color: #ff9400;
                color: white;
                padding: 5px 5px;
                /*margin: 8px 0;*/
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }
        </style>
        <script>
            $(document).ready(function () {
                $('#showTable').DataTable({
                    "paging": false,
                    "lengthMenu": [[-1, 10, 25, 50, 100], ["All", 10, 25, 50, 100]],
                    "bSortClasses": false,
                    fixedColumns: true,
                    "order": [[0, "desc"]],
                    "columnDefs": [
                        {orderable: false, targets: [3]},
                        {orderable: false, targets: [4]},
                        {orderable: false, targets: [5]},
                        {orderable: false, targets: [6]},
                        {orderable: false, targets: [7]},
                        {orderable: false, targets: [8]},
                        {orderable: false, targets: [9]},
                        {orderable: false, targets: [10]},
                        {orderable: false, targets: [11]},
                        {"width": "12%", "targets": 0},
                        {"width": "12%", "targets": 1},
                        {"width": "30%", "targets": 2},
                        {"width": "9%", "targets": 3},
                        {"width": "9%", "targets": 4},
                        {"width": "9%", "targets": 5},
                        {"width": "9%", "targets": 6},
                        {"width": "9%", "targets": 7},
                        {"width": "9%", "targets": 8},
                        {"width": "9%", "targets": 9},
                        {"width": "9%", "targets": 10},
                        {"width": "9%", "targets": 11}
                    ]
                });

                // Setup - add a text input to each footer cell
                $('#showTable tfoot th').not(":eq(3)").not(":eq(3)").not(":eq(3)").not(":eq(3)").not(":eq(3)")
                        .not(":eq(3)").not(":eq(3)").not(":eq(3)").not(":eq(3)").not(":eq(3)").not(":eq(3)").not(":eq(3)").each(function () {
                    var title = $(this).text();
                    $(this).html('<input type="text" />');
                });

                // DataTable
                var table = $('#showTable').DataTable();

                // Apply the search
                table.columns().every(function () {
                    var that = this;
                    $('input', this.footer()).on('keyup change', function () {
                        if (that.search() !== this.value) {
                            that
                                    .search(this.value)
                                    .draw();
                        }
                    });
                });
            });
        </script>
        <script>
            function checkSortBy() {
                var sortBy = document.getElementById("sortBy");
                var select = document.getElementById("data");
                select.options.length = 0;

                var option = document.createElement("option");
                option.text = "";
                option.value = "";
                option.hidden = true;
                select.add(option);

                if (sortBy.value === "user") {
            <c:forEach items="${userList}" var="x">
                    var option = document.createElement("option");
                    option.text = "${x.id} : ${x.desc}";
                                option.value = "${x.id} : ${x.desc}";
                                            select.add(option);
            </c:forEach>
                                        } else if (sortBy.value === "mat") {
            <c:forEach items="${matList}" var="x">
                                            var option = document.createElement("option");
                                            option.text = "${x.id} : ${x.desc}";
                                                        option.value = "${x.id} : ${x.desc}";
                                                                    select.add(option);
            </c:forEach>
                                                                } else if (sortBy.value === "dest") {
            <c:forEach items="${destList}" var="x">
                                                                    var option = document.createElement("option");
                                                                    option.text = "${x.id} : ${x.desc}";
                                                                                option.value = "${x.id} : ${x.desc}";
                                                                                            select.add(option);
            </c:forEach>
                                                                                        } else if (sortBy.value === "app") {
            <c:forEach items="${appList}" var="x">
                                                                                            var option = document.createElement("option");
                                                                                            option.text = "${x.id} : ${x.desc}";
                                                                                                        option.value = "${x.id} : ${x.desc}";
                                                                                                                    select.add(option);
            </c:forEach>
                                                                                                                } else if (sortBy.value === "doc") {
            <c:forEach items="${docList}" var="x">
                                                                                                                    var option = document.createElement("option");
                                                                                                                    option.text = "${x.id} : ${x.desc}";
                                                                                                                                option.value = "${x.id} : ${x.desc}";
                                                                                                                                            select.add(option);
            </c:forEach>
                                                                                                                                        } else if (sortBy.value === "mvt") {
            <c:forEach items="${mvtList}" var="x">
                                                                                                                                            var option = document.createElement("option");
                                                                                                                                            option.text = "${x.id} : ${x.desc}";
                                                                                                                                                        option.value = "${x.id} : ${x.desc}";
                                                                                                                                                                    select.add(option);
            </c:forEach>
                                                                                                                                                                }


                                                                                                                                                            }
        </script>
        <script>
            function CD() {
                var cd = document.getElementById("ym").value;
                window.location.href = "/TABLE2/WMS940/display?CD=" + cd;
            }

            function subRow(qno, head) {
                var tr = document.getElementsByTagName('tr');

                for (var i = 0; i < tr.length; i++) {
                    if (tr[i].style.display === "") {
                        if (tr[i].id === qno) {
                            tr[i].style.display = "none";
                            document.getElementById(head).style.cssText = "cursor: pointer; background-color: #e5e5e5; color: black;";
                        }
                    } else {
                        if (tr[i].id === qno) {
                            tr[i].style.display = "";
                            document.getElementById(head).style.cssText = "cursor: pointer; background-color: #003682; color: white;";
                        }
                    }
                }
            }

            function expand() {
                document.getElementById("exp").style.display = "none";
                document.getElementById("com").style.display = "";
                var tr = document.getElementsByTagName('tr');

                for (var i = 0; i < tr.length; i++) {
                    if (tr[i].id.toString().split("-")[0] === "H") {
                        tr[i].style.cssText = "cursor: pointer; background-color: #003682; color: white;";
                    } else if (tr[i].id.toString().split("-")[0] === "D") {
                        tr[i].style.display = "";
                    }
                }
            }

            function compress() {
                document.getElementById("exp").style.display = "";
                document.getElementById("com").style.display = "none";
                var tr = document.getElementsByTagName('tr');

                for (var i = 0; i < tr.length; i++) {
                    if (tr[i].id.toString().split("-")[0] === "H") {
                        tr[i].style.cssText = "cursor: pointer; background-color: #e5e5e5; color: black;";
                    } else if (tr[i].id.toString().split("-")[0] === "D") {
                        tr[i].style.display = "none";
                    }
                }
            }

            function hide() {
                document.getElementById("hide").style.display = "none";
                document.getElementById("show").style.display = "";
                document.getElementById("frm").style.display = "none";
            }

            function show() {
                document.getElementById("hide").style.display = "";
                document.getElementById("show").style.display = "none";
                document.getElementById("frm").style.display = "";
            }
        </script>
        <style>
            .header {
                padding: 10px 16px;
                background: white;
                color: black;
                display: none;
            }

            .sticky {
                position: fixed;
                top: 0;
                width: 100%;
                display: block;

            }
        </style>

    </head>    
    <body>
        <div id="wrapper">

            <!-- /#sidebar-wrapper -->
            <%@ include file="../fragments/sidebar.jsp" %>

            <!-- Page Content -->
            <!-- nav-head-custom -->
            <%@ include file="../fragments/nav_head.jsp" %>

            <div class="container-fluid">
                <!--<div id="wrapper-top">-->
                <%-- PART 2 --%>
                <!--                    <div class="row">
                                        <div class="col-lg-12">
                                            <div id="set-height" style="height:415px;margin-top:0px;">
                                                <div id="sidebar-wrapper-top" class="">
                                                    <b class="page-header" style="padding-left:5px;font-size:18px;">
                <%-- <hr style="margin:0px;margin-top:10px;margin-bottom:10px;"> --%>
            </b>
        </div>
    </div>
    </div>
    </div> -->
                <%-- PART 3 --%>
                <!--show Qdest table-->
                <form action="../resources/manual/WMS940.pdf" target="_blank">
                    <table width="100%">
                        <tr id="dont">
                            <td width="94%" align="left">
                                <a style=" width: 100px; height: 35px;" class="btn btn-danger" id="hide" name="hide" onclick="hide();">
                                    <i class="fa fa-window-close-o" style="font-size:15px;"></i>&nbsp;&nbsp; Hide</a>
                                <a style=" width: 100px; height: 35px; display: none;" class="btn btn-success" id="show" name="show" onclick="show();">
                                    <i class="fa fa-window-maximize" style="font-size:15px;"></i>&nbsp;&nbsp; Show</a>
                            </td>
                            <td width="6%" align="right"><input type="submit" value="Manual" /></td>
                        </tr>
                    </table>
                </form>
                <div style="border: 2px solid #ccc; border-radius: 5px; padding: 20px;">
                    <form id="frm" name="frm" action="display" method="post">
                        <input id="sts" name="sts" value="" hidden>
                        <table width="100%">
                            <tr id="dont">                              
                                <td align="left"><b style="color: #00399b;">Year / Month : </b>
                                    <input style="width:100px;" type="text" placeholder="เช่น 201903" name="ym" id="ym" value="${ym}" onchange="CD();" maxlength="6">
                                    <script>
                                        var input = document.getElementById("ym");
                                        input.addEventListener("keyup", function (event) {
                                            if (event.keyCode === 13) {
                                                event.preventDefault();

                                                var cd = document.getElementById("ym").value;
                                                window.location.href = "/TABLE2/WMS940/display?CD=" + cd;
                                            }
                                        });
                                    </script>
                                    <input type="text" style="width: 0px; border-color: white;">
                                    <!--                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                        <a style=" width: 150px;" class="btn btn-primary" id="exp" name="exp" onclick="expand();">
                                                                            <i class="fa fa-expand" style="font-size:20px;"></i>&nbsp;&nbsp; Expand All</a>
                                                                        <a style=" width: 150px; display: none;" class="btn btn-warning" id="com" name="com" onclick="compress();">
                                                                            <i class="fa fa-compress" style="font-size:20px;"></i>&nbsp;&nbsp; Collapse All</a>-->
                                    <b style="color: #00399b;">Warehouse : </b>
                                    <select name="wh" style="width:200px;" onchange="window.location.href = '/TABLE2/WMS940/display?wh=' + this.value;">
                                        <option value="${mcF}" selected hidden>${mcF} : ${nameF}</option>
                                        <c:forEach items="${MCList}" var="p" varStatus="i">
                                            <option value="${p.uid}">${p.uid} : ${p.name}</option> 
                                        </c:forEach>
                                    </select>
                                </td>
                                <td align="right"><b style="color: #00399b;">Selected By : </b>
                                    <select name="sortBy" id="sortBy" style="width:250px; height: 34px;" onchange="checkSortBy();">
                                        ${sortBy}
                                        <option value="user">1 : User</option>
                                        <option value="mat">2 : Material Control</option>
                                        <option value="dest">3 : Destination</option>
                                        <option value="app">4 : Approve User</option>
                                        <option value="doc">5 : Document No.</option>
                                        <option value="mvt">6 : Movement Type</option>
                                    </select>
                                </td>
                                <td align="right" width="360px"> 
                                    <select name="data" id="data" style="width:350px; height: 34px;" onchange="this.form.submit();">
                                        <option value="${data}" hidden>${data}</option>
                                        <c:forEach items="${mainList}" var="x">
                                            <option value="${x.id} : ${x.desc}">${x.id} : ${x.desc}</option>
                                        </c:forEach>
                                    </select>
                                </td>
                            </tr>
                        </table>
                    </form>
                    <table id="showTable" class="display" style="width:100%; border: 1px solid #ccc; border-radius: 4px;">
                        <thead>
                            <tr>
                                <th colspan="3" style="text-align: center; border-bottom: none;">
                                    <a style=" width: 150px; height: 30px;" class="btn btn-primary" id="exp" name="exp" onclick="expand();">
                                        <!--<i class="fa fa-expand" style="font-size:20px;"></i>&nbsp;&nbsp;--> 
                                        Expand All</a>
                                    <a style=" width: 150px; height: 30px; display: none;" class="btn btn-warning" id="com" name="com" onclick="compress();">
                                        <!--<i class="fa fa-compress" style="font-size:20px;"></i>&nbsp;&nbsp;--> 
                                        Collapse All</a>
                                </th>
                                <th colspan="5" style="text-align: center; border-bottom-color: #ccc; background-color: #f7f7f7;">STOCK RM</th>
                                <th colspan="2" style="text-align: center; border-bottom-color: #ccc;">LOGISTICS</th>
                                <th colspan="1" style="text-align: center; border-bottom-color: #ccc; background-color: #f7f7f7;">DESTINATION</th>
                            </tr>
                            <tr>
                                <th colspan="3" style="text-align: center; border-bottom: none;"></th>
                                <th colspan="1" style="text-align: center; border-bottom: none; background-color: #f9f9f9; cursor: pointer;" onclick="document.getElementById('sts').value = '0';
                                        document.getElementById('frm').submit();">New Queue</th>
                                <th colspan="1" style="text-align: center; border-bottom: none; background-color: #f9f9f9; cursor: pointer;" onclick="document.getElementById('sts').value = '1'; document.getElementById('frm').submit();">Requested</th>
                                <th colspan="3" style="text-align: center; border-bottom: none; background-color: #f9f9f9; cursor: pointer;" onclick="document.getElementById('sts').value = '2'; document.getElementById('frm').submit();">Approved</th>
                                <th colspan="1" style="text-align: center; border-bottom: none; cursor: pointer;" onclick="document.getElementById('sts').value = '5';
                                        document.getElementById('frm').submit();">Received</th>
                                <th colspan="1" style="text-align: center; border-bottom: none; cursor: pointer;" onclick="document.getElementById('sts').value = '6';
                                        document.getElementById('frm').submit();">Transported</th>
                                <th colspan="1" style="text-align: center; border-bottom: none; background-color: #f9f9f9; cursor: pointer;" onclick="document.getElementById('sts').value = '7';
                                        document.getElementById('frm').submit();">Received</th>
                                <th colspan="1" style="text-align: center; border-bottom: none; cursor: pointer;" onclick="document.getElementById('sts').value = 'C';
                                        document.getElementById('frm').submit();">COMPLETED</th>
                            </tr>
                            <tr id="dont">
                                <th style="text-align: center;">Queue No.</th>
                                <th style="text-align: center;">Material Code</th>
                                <th style="text-align: center;">ID</th>
                                <th style="text-align: center; color: #c4001d; background-color: #fcfcfc; cursor: pointer;" onclick="document.getElementById('sts').value = 'R';
                                        document.getElementById('frm').submit();">Returned</th>
                                <th style="text-align: center; background-color: #fcfcfc;"></th>
                                <th style="text-align: center; color: #c4001d; background-color: #fcfcfc; cursor: pointer;" onclick="document.getElementById('sts').value = '8';
                                        document.getElementById('frm').submit();">Cancelled</th>
                                <th style="text-align: center; background-color: #fcfcfc;"></th>
                                <th style="text-align: center; color: #c4001d; background-color: #fcfcfc; cursor: pointer;" onclick="document.getElementById('sts').value = '9';
                                        document.getElementById('frm').submit();">Rejected</th>
                                <th style="text-align: center;"></th>
                                <th style="text-align: center; color: #c4001d; cursor: pointer;" onclick="document.getElementById('sts').value = 'H';
                                        document.getElementById('frm').submit();">Hold</th>
                                <th style="text-align: center; color: #c4001d; background-color: #fcfcfc; cursor: pointer;" onclick="document.getElementById('sts').value = 'D';
                                        document.getElementById('frm').submit();">Returned</th>
                                <th style="text-align: center;"></th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr id="dont">
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                            </tr>
                        </tfoot>
                        <tbody>
                            <c:forEach items="${detList}" var="x">
                                <tr id="H-${x.qno}" onclick="subRow('D-${x.qno}', 'H-${x.qno}');" style="cursor: pointer; background-color: white;">
                                    <td><b style="font-size: 18px;">${x.qno}</b></td>
                                    <td><div style="font-size: 17px;">${x.shipmentDate}</div></td>
                                    <td><div style="font-size: 17px;">${x.desc}</div></td>
                                    <td><div style="font-size: 17px;">${x.mvt}</div></td>
                                    <td><div style="font-size: 17px;">${x.matc}</div></td>
                                    <td><div style="font-size: 17px;">${x.status}</div></td>
                                    <td><div style="font-size: 17px;">${x.GID}</div></td>
                                    <td><div style="font-size: 17px;">${x.um}</div></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <c:forEach items="${x.subQNO}" var="p">
                                    <tr id="D-${x.qno}" style="display: none;">
                                        <td>${p.qno}</td>
                                        <td>${p.matc}</td>
                                        <td>${p.id}</td>
                                        ${p.status}
                                    </tr>
                                </c:forEach>
                            </c:forEach>
                        </tbody>
                    </table>
                    <!--**********************-->
                    <!--End Part 3-->
                    <!--</div>  end #wrapper-top--> 
                </div> 
                <br>
            </div> <!-- end .container-fluid -->
            <div class="header" id="myHeader">
                <table style="width:100%; border-bottom: 1px solid black;">
                    <tr style="height: 40px;">
                        <th colspan="3" style="text-align: center; border-bottom: none;"></th>
                        <th colspan="5" style="text-align: center; border-bottom-color: #ccc; background-color: #f7f7f7;">STOCK RM</th>
                        <th colspan="2" style="text-align: center; border-bottom-color: #ccc;">LOGISTICS</th>
                        <th colspan="1" style="text-align: center; border-bottom-color: #ccc; background-color: #f7f7f7;">DESTINATION</th>
                    </tr>
                    <tr style="height: 40px;">
                        <th colspan="1" style="text-align: center; border-bottom: none;">Queue No.</th>
                        <th colspan="1" style="text-align: center; border-bottom: none;">Material Code</th>
                        <th colspan="1" style="text-align: center; border-bottom: none;">ID</th>
                        <th colspan="1" style="text-align: center; border-bottom: none; background-color: #f9f9f9; cursor: pointer;" onclick="document.getElementById('sts').value = '0';
                                document.getElementById('frm').submit();">New Queue</th>
                        <th colspan="1" style="text-align: center; border-bottom: none; background-color: #f9f9f9; cursor: pointer;" onclick="document.getElementById('sts').value = '1'; document.getElementById('frm').submit();">Requested</th>
                        <th colspan="3" style="text-align: center; border-bottom: none; background-color: #f9f9f9; cursor: pointer;" onclick="document.getElementById('sts').value = '2'; document.getElementById('frm').submit();">Approved</th>
                        <th colspan="1" style="text-align: center; border-bottom: none; cursor: pointer;" onclick="document.getElementById('sts').value = '5';
                                document.getElementById('frm').submit();">Received</th>
                        <th colspan="1" style="text-align: center; border-bottom: none; cursor: pointer;" onclick="document.getElementById('sts').value = '6';
                                document.getElementById('frm').submit();">Transported</th>
                        <th colspan="1" style="text-align: center; border-bottom: none; background-color: #f9f9f9; cursor: pointer;" onclick="document.getElementById('sts').value = '7';
                                document.getElementById('frm').submit();">Received</th>
                        <th colspan="1" style="text-align: center; border-bottom: none; cursor: pointer;" onclick="document.getElementById('sts').value = 'C';
                                document.getElementById('frm').submit();">COMPLETED</th>
                    </tr>
                    <tr id="dont" style="height: 40px;">
                        <th style="text-align: center; width: 150px;"></th>
                        <th style="text-align: center; width: 150px;"></th>
                        <th style="text-align: center; width: 275px;"></th>
                        <th style="text-align: center; width: 130px; color: #c4001d; background-color: #fcfcfc; cursor: pointer;" onclick="document.getElementById('sts').value = 'R';
                                document.getElementById('frm').submit();">Returned</th>
                        <th style="text-align: center; width: 140px; background-color: #fcfcfc;"></th>
                        <th style="text-align: center; width: 140px; color: #c4001d; background-color: #fcfcfc; cursor: pointer;" onclick="document.getElementById('sts').value = '8';
                                document.getElementById('frm').submit();">Cancelled</th>
                        <th style="text-align: center; width: 50px; background-color: #fcfcfc;"></th>
                        <th style="text-align: center; width: 140px; color: #c4001d; background-color: #fcfcfc; cursor: pointer;" onclick="document.getElementById('sts').value = '9';
                                document.getElementById('frm').submit();">Rejected</th>
                        <th style="text-align: center; width: 140px;"></th>
                        <th style="text-align: center; width: 140px; color: #c4001d; cursor: pointer;" onclick="document.getElementById('sts').value = 'H';
                                document.getElementById('frm').submit();">Hold</th>
                        <th style="text-align: center; width: 140px; color: #c4001d; background-color: #fcfcfc; cursor: pointer;" onclick="document.getElementById('sts').value = 'D';
                                document.getElementById('frm').submit();">Returned</th>
                        <th style="text-align: center; width: 180px;"></th>
                    </tr>
                </table>
            </div>
        </div> <!-- end #wrapper -->
        <script>
            window.onscroll = function () {
                myFunction();
            };

            var header = document.getElementById("myHeader");
            var sticky = header.offsetTop;

            function myFunction() {
                if (window.pageYOffset > sticky) {
                    header.classList.add("sticky");
                } else {
                    header.classList.remove("sticky");
                }
            }
        </script>
    </body>
</html>