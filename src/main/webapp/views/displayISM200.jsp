<%@ include file="../fragments/taglibs.jsp" %>
<%@ include file="../fragments/imports.jsp" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.sql.*;" %>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <title>ISM</title>
        <!-- css :: vendors -->
        <jsp:include page="../fragments/css.jsp" />
        <!-- additional custom :: my-style -->
        <link rel="stylesheet" href="../resources/assets/styles/myStyles.css">
        <link rel="stylesheet" href="../resources/font/THsarabun/thsarabumnew.css">

        <!-- js :: vendors -->
        <jsp:include page="../fragments/script.jsp" />
        <!-- additional custom :: my-script -->
        <script src="../resources/assets/scripts/myScripts.js" async></script>
        <script src="../resources/assets/scripts/toggleLoad.js" async></script>
        <style>
            input[type=text],
            select {
                width: 100%;
                padding: 5px 5px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }

            input[type=date],
            input[type=number] {
                /*width: 100%;*/
                padding: 5px 5px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }

            input[type=submit] {
                width: 100%;
                background-color: #00399b;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=submit]:hover {
                background-color: #3973d6;
            }

            input[type=button] {
                width: 100%;
                background-color: #ef604a;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=button]:hover {
                background-color: #e51e00;
            }
        </style>
        <script>

            jQuery.extend(jQuery.fn.dataTableExt.oSort, {//date sorting
                "date-uk-pre": function (a) {
                    if (a == null || a == "") {
                        return 0;
                    }
                    var ukDatea = a.split('/');
                    return (ukDatea[2] + ukDatea[1] + ukDatea[0]) * 1;
                },

                "date-uk-asc": function (a, b) {
                    return ((a < b) ? -1 : ((a > b) ? 1 : 0));
                },

                "date-uk-desc": function (a, b) {
                    return ((a < b) ? 1 : ((a > b) ? -1 : 0));
                }
            });

            var tableOptions = {
                "paging": true,
                "lengthMenu": [[-1, 10, 25, 50, 100], ["All", 10, 25, 50, 100]],
                "bSortClasses": false,
                "ordering": true,
                "info": true,
                "fixedColumns": true,
                "order": [[1, "desc"], [2, "desc"], [3, "desc"], [4, "desc"]],
                "columnDefs": [
                    {"targets": [10, 11], "className": "text-right"},
                    {"targets": [0, 12, 13], "className": "text-center"},
                    {"targets": [0], "orderable": false},
                    {"targets": [1, 2], "type": "date-uk"}
                ]
            };
        </script>
        <style>
            body {
                font-family: "Sarabun", sans-serif !important;
                /*font-family: 'THSarabunNew', sans-serif;*/
            }

            .checkData ,.checkAllData,.checkDataRM ,.checkAllDataRM{
                width: 20px;
                height: 20px;
            }

            .displayTable {
                width: auto;
                border: 1px solid #ccc;
                border-radius: 4px;
            }

            .displayContain {
                border: 2px solid #ccc;
                border-radius: 5px;
                padding: 20px;
                /*width: fit-content;*/
            }

            /*            td {
                            white-space: nowrap;
                            cursor: pointer;
                        }*/

            .checkData ,.checkAllData{
                width: 20px;
                height: 20px;
            }

            .sts-0{
                font-size: 20px;
                color: navy;
            }
            .sts-1{
                font-size: 20px;
                color: yellow;
            }
            .sts-2{
                font-size: 20px;
                color: green;
            }
            .sts-3{
                font-size: 20px;
                color: blue;
            }
            .sts-4{
                font-size: 20px;
                color: pink;
            }
            .sts-5{
                font-size: 20px;
                color: navy;
            }
            .sts-6{
                font-size: 20px;
                color: gray;
            }
            .sts-7{
                font-size: 20px;
                color: brown;
            }
            .sts-8{
                font-size: 20px;
                color: orange;
            }
            .sts-9{
                font-size: 20px;
                color: black;
            }

            .modal-content{
                width: 50%;
            }

            .editBtn{
                color: navy;
                cursor: pointer;
            }
        </style>
        <script>
            $(document).ready(function () {

                $('.checkAllDataRM').click(function () {
                    var checkAllRM = $(this);
                    $('.checkDataRM').prop("checked", checkAllRM.prop("checked"));
                });

//                sessionStorage.setItem('uid', '90309');

                $('#showTable').DataTable(tableOptions);

                // Setup - add a text input to each footer cell
                $('#showTable tfoot th').not(":eq(0)").each(function () {
                    var title = $(this).text();
                    $(this).html('<input type="text" />');
                });

                // DataTable
                var table = $('#showTable').DataTable();

                // Apply the search
                table.columns().every(function () {
                    var that = this;

                    $('input', this.footer()).on('keyup change', function () {
                        if (that.search() !== this.value) {
                            that.search(this.value).draw();
                        }
                    });
                });


                getWH(); //Comment call getHead() function
                getProductGroup();

                $('.checkAllData').click(function () {
                    var checkAll = $(this);
                    $('.checkData').prop("checked", checkAll.prop("checked"));
                });

                $('#receivedBtn').click(function () {

                    var checkData = $('.checkData:checked');

                    if (checkData.length > 0) {

                        for (var i = 0; i < checkData.length; i++) {

                            var checkhtml = $(checkData[i]).closest("tr").find("td:eq(12)").html();

                            if (checkhtml.toString().includes("2 = BS Approved")) {
                                var orderNo = $(checkData[i]).val().split("_")[0];
                                var seq = $(checkData[i]).val().split("_")[1];
                                $.ajax({
                                    url: "/TABLE2/ShowDataTablesISM200?mode=updateReceived"
                                            + "&orderNo=" + orderNo
                                            + "&seq=" + seq
                                            + "&uid=" + sessionStorage.getItem('uid')

                                }).done(function (result) {
                                    console.log(result);

                                }).fail(function (jqXHR, textStatus, errorThrown) {
                                    // needs to implement if it fails
                                });
                            }

                        }

                        setTimeout(getHead, 1000);
                        alertify.success('Update Success!');

//                        $.ajax({
//                            url: "/TABLE2/ShowDataTablesISM200?mode=updateJob"
//
//                        }).done(function (result) {
//
//                        }).fail(function (jqXHR, textStatus, errorThrown) {
//                            // needs to implement if it fails
//                        });
                    } else {
                        alertify.alert("No Data Selected!",
                                function () {
                                    alertify.error('Please select data!');
                                }
                        );
                    }

                });

                $('#cancelBtn').click(function () {

                    var checkData = $('.checkData:checked');

                    if (checkData.length > 0) {

                        for (var i = 0; i < checkData.length; i++) {
                            var orderNo = $(checkData[i]).val();
                            $.ajax({
                                url: "/TABLE2/ShowDataTablesISM200?mode=updateCancel"
                                        + "&orderNo=" + orderNo

                            }).done(function (result) {
                                console.log(result);

                            }).fail(function (jqXHR, textStatus, errorThrown) {
                                // needs to implement if it fails
                            });
                        }

                        setTimeout(getHead, 1000);
                        alertify.success('Update Success!');
                    } else {
                        alertify.alert("No Data Selected!",
                                function () {
                                    alertify.error('Please select data!');
                                }
                        );
                    }

                });

                $('#sumByDestBtn').click(function () {

                    var checkData = $('.checkData:checked');

                    if (checkData.length > 0) {
                        $('#orderForm').html('');
                        for (var i = 0; i < checkData.length; i++) {
                            var orderNo = $(checkData[i]).val().split("_")[0];
                            var seq = $(checkData[i]).val().split("_")[1];
//                            $('#orderForm').append('<input type="hidden" name="order" value="' + orderNo + '">');
//                            $('#orderForm').append('<input type="hidden" name="seq" value="' + seq + '">');
                            $('#orderForm').append('<input type="hidden" name="oANDs" value="' + orderNo + "_" + seq + '">');
                        }
                        $('#orderForm').submit();

                    } else {
                        alertify.alert("No Data Selected!",
                                function () {
                                    alertify.error('Please select data!');
                                }
                        );
                    }
                });

                $('#backWardBtn').click(function () {

                    var checkData = $('.checkData:checked');

                    var uid = sessionStorage.getItem('uid');

                    if (checkData.length > 0) {

                        var HasSTSNot1 = false;

                        for (var i = 0; i < checkData.length; i++) {
                            var checkhtml = $(checkData[i]).closest("tr").find("td:eq(12)").html();

                            if (!checkhtml.toString().includes("4") && !checkhtml.toString().includes("2")) { // STS != 2 4
                                HasSTSNot1 = true;
                            }
                        }

                        if (HasSTSNot1) {
                            alertify.alert("Please Checked Only Order Has Status = 4,2");
                        } else {
                            for (var i = 0; i < checkData.length; i++) {
                                var checkhtml = $(checkData[i]).closest("tr").find("td:eq(12)").html();

                                if (checkhtml.toString().includes("4") || checkhtml.toString().includes("2")) { // only STS = 2 4 Can Backward
//                                    var orderNo = $(checkData[i]).val();
                                    var orderNo = $(checkData[i]).val().split("_")[0];
                                    var seq = $(checkData[i]).val().split("_")[1];
                                    $.ajax({
                                        url: "/TABLE2/ShowDataTablesISM200?mode=backward"
                                                + "&orderNo=" + orderNo
                                                + "&seq=" + seq
                                                + "&uid=" + uid

                                    }).done(function (result) {
                                        console.log(result);

                                    }).fail(function (jqXHR, textStatus, errorThrown) {
                                        // needs to implement if it fails
                                    });
                                }
                            }

                            setTimeout(getHead, 1000);
                            alertify.success('Backword Success!');
                        }

                    } else {
                        alertify.alert("No Data Selected!",
                                function () {
                                    alertify.error('Please select data!');
                                }
                        );
                    }

//                    $('#orderForm').html('');
//                    if (checkData.length > 0) {
//                        for (var i = 0; i < checkData.length; i++) {
//                            $('#orderForm').append('<input type="hidden" name="order" value="' + checkData[i].value + '">');
//                        }
//                        $('#orderForm').submit();
//
//                    } else {
//                        window.location.href = 'sumByDest';
//                    }
                });

                $('#closeOrderBtn').click(function () {

                    $('#modalCloseOrder').modal('show');

                });

                $('#confClose').on('click', function () {

                    var appDate = $('#approveDateforClose').val();

                    if (appDate !== "") {

                        //Show Detail
                        if ($.fn.DataTable.isDataTable('#closeTable')) { //before RE-Create New Datatable
                            $('#closeTable').DataTable().destroy();
                        }

                        $('#closeTable tbody').empty();

                        $('#closeTable').dataTable({
                            ajax: {
                                url: "/TABLE2/ShowDataTablesISM200",
                                data: {mode: "closeOrderData", appDate: appDate},
                                dataSrc: 'datalist'
                            },
                            "aoColumns": [
                                {'mRender': function (data, type, full) {
                                        return '<input type="checkbox" class="checkDataRM" value="' + full.ISDORD + "_" + full.ISDSEQ + "_" + full.ISDLINO + "_" + full.ISDSINO + '">';
                                    }
                                },
                                {"data": "ISDORD"},
                                {"data": "ISDSEQ"},
                                {"data": "ISDLINO"},
                                {"data": "ISDSTS"},
                                {"data": "ISDITNO"},
                                {"data": "ISDVT"},
                                {"data": "ISDDESC"}
//                                {"data": "ISDSAPONH"},
//                                {"data": "ISDWMSONH"},
//                                {"data": "ISDRQQTY"}
                            ],
                            "initComplete": function (settings, json) {
                                $('#reMainModal').modal('show');
                                $('.checkAllDataRM').prop('checked', false);
                                $('[data-toggle="tooltip"]').tooltip();
                                $('#loadIcon').addClass('hidden');
                            },
                            "createdRow": function (row, data, dataIndex) {
//                                if (data.FAM > 0) { //Family
//                                    if (parseInt(data.ISDSINO) === 0) {// Mom
//                                        $(row).css("background-color", "rgb(255,163,138)");
//                                    } else {// Child
//                                        $(row).css("background-color", "rgb(245,223,218)");
//                                    }
//                                }
                            },
                            "ordering": false,
                            "columnDefs": [
                                {"targets": [0, 1, 2, 3, 4, 6], "className": "text-center"},
                                {"targets": [5], "className": "text-left"}
                            ]
                        });

                        $('#closeOrderModal').modal('show');
                        $('#modalCloseOrder').modal('hide');

                    } else {
                        alertify.error("Please Select Approve Date.");
                    }

                });

                $('#enCloseBtn').on('click', function () { //confirm close Order flag to C
                    var checkDataRM = $('.checkDataRM:checked');
                    var uid = $('#uid').val();

                    if (checkDataRM.length > 0) {
                        var ArrOut = [];
                        for (var i = 0; i < checkDataRM.length; i++) {
                            var ArrIn = {};
                            let orderno = $(checkDataRM[i]).val().split("_")[0];
                            let seq = $(checkDataRM[i]).val().split("_")[1];
                            let line = $(checkDataRM[i]).val().split("_")[2];
                            let sino = $(checkDataRM[i]).val().split("_")[3];

                            ArrIn ["order"] = orderno;
                            ArrIn ["seq"] = seq;
                            ArrIn ["line"] = line;
                            ArrIn ["sino"] = sino;
                            ArrOut.push(ArrIn);
                        }

                        //Update Close Order Sts
                        $.ajax({
                            url: "/TABLE2/ShowDataTablesISM200",
                            data: {mode: "updateClose", data: JSON.stringify(ArrOut), uid: uid}
                        }).done(function (result) {

                            console.log(result);

                        }).fail(function (jqXHR, textStatus, errorThrown) {
                            // needs to implement if it fails
                        });

                    }
                });

                $('#wh').change(function () {
                    getHead();
                });

                $('#whPop').change(function () {
                    $('#wh').val($(this).val());
                    getHead();
                    $('#whModal').modal('hide');
                });

                $('#productGroup').on('change', function () {
                    getHead();
                });

            });

            function getHead() {
                console.log("Head");
                var table = $('#showTable').DataTable();
                table.clear().draw();
                $('#loadIcon').removeClass('hidden');

                $('.checkAllData').prop("checked", false);

                var wh = $('#wh').val();
                var pdg = $('#productGroup').val();

                $.ajax({
                    url: "/TABLE2/ShowDataTablesISM200?mode=getHead"
                            + "&wh=" + wh
                            + "&pdg=" + pdg
                }).done(function (result) {
                    console.log(result);

                    for (var i = 0; i < result.length; i++) {

                        table.row.add([
                            '<input type="checkbox" class="checkData" value="' + result[i].ISHORD + "_" + result[i].ISHSEQ + '">',
                            result[i].ISHAPDT,
                            result[i].ISHTRDT,
                            '<a style="color: inherit; font-weight: normal !important;" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Tracking" href="/TABLE2/ISM910/display?find=ord&from=ism200&orderNo=' + result[i].ISHORD + '" >' + result[i].ISHORD + '</a>',
                            String.fromCharCode(parseInt(result[i].ISHSEQ) + 64),
                            result[i].ISHCUNO,
                            result[i].ISHCUNM1,
                            result[i].ISHSUBMI,
                            '<label style="margin: 0; font-weight: normal; cursor:pointer; " onclick="openModalRPStyle(' + result[i].ISHORD + ',' + result[i].ISHSEQ + ')" >' + result[i].ISHMATCODE + '</label>',
//                            result[i].ISHMATCODE,
                            result[i].ISHLOT,
                            result[i].ISHAMTFG,
                            result[i].ISHNOR,
                            "<b hidden>" + result[i].LSTS + "</b>" + '<i class="fa fa-circle' + result[i].LSTSI + ' sts-' + result[i].LSTS + '" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="' + result[i].ISHLSTS + '"></i>',
                            "<b hidden>" + result[i].HSTS + "</b>" + '<i class="fa fa-circle' + result[i].HSTSI + ' sts-' + result[i].HSTS + '" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="' + result[i].ISHHSTS + '"></i>',
                            result[i].ISHRQDT,
                            result[i].ISHDEST,
                            result[i].ISHAPUSR,
                            result[i].ISHUSER,
                            result[i].ISHRCDT,
                            '<a href="summary?dest=' + result[i].ISHPOFG + '&jobNo=' + result[i].ISHJBNO + '&wh=' + result[i].ISHWHNO + '&goBack=display">' + result[i].ISHJBNO + '</a>',
                            '<center><a href="show?orderNo=' + result[i].ISHORD + "&seq=" + result[i].ISHSEQ + '"><i class="fa fa-2x fa-desktop editBtn"></i></a></center>'
                        ]).draw(false);

                    }

                    if (i === 0) {
                        console.log("Auto call getData again");
//                        getHead();
                    }


                    $('[data-toggle="tooltip"]').tooltip();

                    $('#loadIcon').addClass('hidden');

                }).fail(function (jqXHR, textStatus, errorThrown) {
                    // needs to implement if it fails
                });

            }

            function openModalRPStyle(order, seq) {

                $.ajax({
                    url: "/TABLE2/ShowDataTablesISM200",
                    data: {mode: "getRPStyle", orderNo: order, seq: seq},
                    async: false

                }).done(function (result) {

                    if (result[0].ISHRPUSR === "") {
                        $('#NamemodalRPStyle').text("ไม่พบข้อมูล");
                    } else {
                        $('#NamemodalRPStyle').text(result[0].ISHRPUSR);
                    }

                }).fail(function (jqXHR, textStatus, errorThrown) {
                    // needs to implement if it fails
                });

                //open modal
                $('#modalShowRPStyle').modal('show');

            }

            function getWH() {

                $.ajax({
                    url: "/TABLE2/ShowDataTablesISM200?mode=getWH"
                            + "&uid=" + sessionStorage.getItem('uid'),
                    async: false
                }).done(function (result) {
                    console.log(result);

                    $('#wh').html('');
                    var whText = '';

                    for (var i = 0; i < result.length; i++) {
                        whText += '<option value="' + result[i].uid + '" ' + result[i].warehouse + '>' + result[i].uid + ' : ' + result[i].name + '</option>';
                    }

                    $('#wh').html(whText);
                    $('#whPop').html(whText);

                    if ('${wh}' !== '') {
                        $('#wh').val('${wh}');
                        $('#whPop').val('${wh}');
                        getHead();
                        $('#whModal').modal('hide');
                    } else {
//                        $('#whModal').modal('show');
                    }

//                    getHead(); //Comment By Aip

                }).fail(function (jqXHR, textStatus, errorThrown) {
                    // needs to implement if it fails
                });
            }

            function getProductGroup() {
                $.ajax({
                    url: "/TABLE2/ShowDataTablesISM100?mode=getProductGroup"
                            + "&uid=" + sessionStorage.getItem('uid')
                }).done(function (result) {
                    console.log(result);

                    $('#productGroup').html('');
                    var whText = '';

                    for (var i = 0; i < result.length; i++) {
                        whText += '<option value="' + result[i].uid + '">' + result[i].uid + ' : ' + result[i].name + '</option>';
                    }

                    $('#productGroup').html(whText);
                    getHead();

                }).fail(function (jqXHR, textStatus, errorThrown) {
                    // needs to implement if it fails
                });
            }
        </script>
    </head>

    <body>
        <div id="wrapper">

            <!-- /#sidebar-wrapper -->
            <%@ include file="../fragments/sidebar.jsp" %>

            <!-- Page Content -->
            <!-- nav-head-custom -->
            <%@ include file="../fragments/nav_head.jsp" %>

            <div class="container-fluid">
                <!--<div id="wrapper-top">-->
                <%-- PART 2 --%>
                <!--                    <div class="row">
                                                <div class="col-lg-12">
                                                    <div id="set-height" style="height:415px;margin-top:0px;">
                                                        <div id="sidebar-wrapper-top" class="">
                                                            <b class="page-header" style="padding-left:5px;font-size:18px;">
                <%-- <hr style="margin:0px;margin-top:10px;margin-bottom:10px;"> --%>
            </b>
        </div>
    </div>
    </div>
    </div> -->
                <%-- PART 3 --%>
                <!--show Qdest table-->
                <input type="hidden" id="jobNo">
                <form action="../resources/manual/ISM200.pdf" target="_blank">
                    <table width="100%">
                        <tr>
                            <td width="94%" align="left">
                            </td>
                            <td width="6%" align="right"><input type="submit" value="Manual" /></td>
                        </tr>
                    </table>
                </form>
                <div class="displayContain">
                    <div class="row">
                        <div class="col-sm-12">
                            Warehouse : <select id="wh" style="width: 300px;"></select>&nbsp;&nbsp;
                            Product Group : <select id="productGroup" style="width: 300px;"></select>
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            <button type="button" id="receivedBtn" title="[status 2 => 4]" class="btn btn-success" style="width: 150px;">Received</button>
                            <button type="button" id="cancelBtn" class="hidden" style="width: 150px;">Cancel</button>
                            <button type="button" id="sumByDestBtn" class="btn btn-warning" style="width: 150px;">Simulation</button>
                            <button type="button" id="backWardBtn" title="[status 2 or 4 => 0]" style="background-color : #F071F4; color: white; width: 150px;" class="btn">Backward</button>
                            <button type="button" id="closeOrderBtn" title="dummy btn" style="width: 150px;" class="btn btn-info">Close Order</button>
                        </div>
                    </div>
                    <form id="orderForm" action="sumByDest" method="post">

                    </form>

                    <table id="showTable" class="display displayTable">
                        <thead>
                            <tr>
                                <th>
                                    <input type="checkbox" class="checkAllData" >
                                </th>
                                <th>Approved Date</th>
                                <th>Transaction Date</th>
                                <th>Sale Order</th>
                                <th>Seq</th>
                                <th>Customer Code</th>
                                <th>Customer Name</th>
                                <th>Doc No</th>
                                <th>Style FG</th>
                                <th>Lot</th>
                                <th>Quantity</th>
                                <th>No.</th>
                                <th>LSTS</th>
                                <th>HSTS</th>
                                <th>Requested Date</th>
                                <th>Destination</th>
                                <th>Approval</th>
                                <th>User</th>
                                <th>Received Date</th>
                                <th>Job no.</th>
                                <th>Options</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                            </tr>
                        </tfoot>
                        <tbody>
                        </tbody>
                    </table>

                    <center>
                        <div id="loadIcon" class="hidden">
                            <h3>
                                Loading...
                                <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
                            </h3>
                        </div>
                    </center>

                    <!-- Modal -->
                    <div class="modal fade" id="whModal" role="dialog">
                        <div class="modal-dialog">
                            <center>
                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">เลือกคลังวัตถุดิบ</h4>
                                    </div>
                                    <div class="modal-body">
                                        <h4>
                                            Warehouse :
                                            <select id="whPop" style="width: 300px;"></select>
                                        </h4>
                                    </div>
                                </div>
                            </center>
                        </div>
                    </div>

                    <!-- Modal -->
                    <center style="font-size: 23px;">
                        <div id="modalShowRPStyle" class="modal fade" role="dialog">
                            <div class="modal-dialog">
                                <div class="modal-lg">
                                    <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title" style="font-size: 30px;">Response Style Name</h4>
                                        </div>

                                        <div class="modal-body">
                                            <label id="NamemodalRPStyle"></label>
                                        </div>

                                        <div class="modal-footer" style="text-align : center">
                                            <input type="button" style="font-size: 18px;" data-dismiss="modal" class="btn btn-success"  value="OK">
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </center>

                    <!-- Modal -->
                    <center style="font-size: 23px;">
                        <div id="modalCloseOrder" class="modal fade" role="dialog">
                            <div class="modal-dialog">
                                <div class="modal-lg">
                                    <!-- Modal content-->
                                    <div class="modal-content"  style="width:60%;">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title" style="font-size: 30px;">Close Order By Approve Date</h4>
                                        </div>

                                        <div class="modal-body">
                                            <div class="row">
                                                <div class="col-sm-2"></div>
                                                <div class="col-sm-8"><input type="date" id="approveDateforClose" class="form-control"></div>
                                                <div class="col-sm-2"></div>
                                            </div>
                                        </div>

                                        <div class="modal-footer" style="text-align : center">
                                            <div class="row">
                                                <div class="col-sm-4"></div>
                                                <div class="col-sm-4"><input type="button" style="font-size: 18px;" class="btn btn-danger" id="confClose" value="OK"></div>
                                                <div class="col-sm-4"></div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Modal -->
                        <div class="modal fade-lg" id="closeOrderModal" role="dialog">
                            <div class="modal-dialog">
                                <center>
                                    <!-- Modal content-->
                                    <div class="modal-content" style="width:100%;">
                                        <div class="modal-header">
                                            <div class="row">
                                                <div class="col-sm-4" style="text-align:left;">
                                                    <h4 class="modal-title">ISM110/S</h4>
                                                </div>
                                                <div class="col-sm-4" style="text-align:center;">
                                                    <h4 class="modal-title">Close Order List. Display</h4>
                                                </div>
                                                <div class="col-sm-4" style="text-align:right;">
                                                    <div class="col-sm-11">
                                                        <h4 class="modal-title">
                                                            <label style="font-size:15px;" id="time2"></label>
                                                        </h4>
                                                    </div>
                                                    <div class="col-sm-1">
                                                        <button type="button" style="font-size: 33px;" class="close" data-dismiss="modal">&times;</button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-6" style="text-align:left;">
                                                    <button type="button" id="enCloseBtn" style="width:11em; height: 3em;" class="btn btn-danger" data-dismiss="modal">
                                                        <label style="font-size: 20px; cursor: pointer;">Delete</label>
                                                    </button>
                                                </div>
                                                <div class="col-sm-6" style="text-align:right;">
                                                    <button type="button" id="backCloseBtn" style="width:11em; height: 3em;" class="btn btn-light" data-dismiss="modal">
                                                        <label style="font-size: 20px; cursor: pointer;">Back</label>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-body">
                                            <table id="closeTable" class="display displayTable" style="width:100%">
                                                <thead>
                                                    <tr>
                                                        <th><input type="checkbox" class="checkAllDataRM" ></th>
                                                        <th>Sales Order</th>
                                                        <th>Seq</th>
                                                        <th>No.</th>
                                                        <th>Status</th>
                                                        <th>Material</th>
                                                        <th>VT</th>
                                                        <th>Description</th>
                                                    </tr>
                                                </thead>

                                                <tbody></tbody>

                                            </table>
                                        </div>
                                        <div class="modal-footer">

                                        </div>
                                    </div>
                                </center>
                            </div>
                        </div>

                    </center>

                    <!--**********************-->
                    <!--End Part 3-->
                    <!--</div>  end #wrapper-top-->
                </div>
                <br>
            </div> <!-- end .container-fluid -->
        </div> <!-- end #wrapper -->
    </body>

</html>