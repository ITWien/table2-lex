<%@ include file="../fragments/taglibs.jsp" %>
<%@ include file="../fragments/imports.jsp" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.sql.*;" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>WMS</title>
        <!-- css :: vendors -->
        <jsp:include page = "../fragments/css.jsp" />
        <!-- additional custom :: my-style -->
        <link rel="stylesheet" href="../resources/assets/styles/myStyles.css">

        <!-- js :: vendors -->
        <jsp:include page = "../fragments/script.jsp" />
        <!-- additional custom :: my-script -->
        <script src="../resources/assets/scripts/myScripts.js" async></script>
        <script src="../resources/assets/scripts/toggleLoad.js" async></script> 
        <style>
            input[type=text], select{
                width: 100%;
                padding: 5px 5px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }

            input[type=date], input[type=number]{
                /*width: 100%;*/
                padding: 5px 5px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }

            input[type=submit] {
                width: 100%;
                background-color: #00399b;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=submit]:hover {
                background-color: #3973d6;
            }

            input[type=button] {
                width: 100%;
                background-color: #ef604a;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=button]:hover {
                background-color: #e51e00;
            }
        </style>
        <script>
            function showModal(code, id, qty, packtype, location, unit, descs, grpdescs) {

                document.getElementById('dcode').innerHTML = code;
                document.getElementById('did').innerHTML = id;
                document.getElementById('dqty').innerHTML = qty + ' ' + unit;
                document.getElementById('dpacktype').innerHTML = packtype;
                document.getElementById('dlocation').innerHTML = location;
                document.getElementById('ddescs').innerHTML = descs;
                document.getElementById('dgrpdescs').innerHTML = grpdescs;

                document.getElementById('id').value = id;
                document.getElementById('matMaster').action = '/WMS/WMS001/DP';

                var modal = document.getElementById('myModal');
                modal.style.display = "block";
                var span = document.getElementById('span');
                span.onclick = function () {
                    modal.style.display = "none";
                };
                window.onclick = function (event) {
                    if (event.target === modal) {
                        modal.style.display = "none";
                    }
                };
            }
        </script>
        <style>
            body {font-family: Arial, Helvetica, sans-serif;}

            /* The Modal (background) */
            .modal {
                display: none; /* Hidden by default */
                position: fixed; /* Stay in place */
                z-index: 1; /* Sit on top */
                padding-top: 100px; /* Location of the box */
                left: 0;
                top: 0;
                width: 100%; /* Full width */
                height: 100%; /* Full height */
                overflow: auto; /* Enable scroll if needed */
                background-color: rgb(0,0,0); /* Fallback color */
                background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
            }

            /* Modal Content */
            .modal-content {
                background-color: #fefefe;
                margin: auto;
                padding: 20px;
                border: 1px solid #888;
                width: fit-content;
                height: fit-content;
            }

            /* The Close Button */
            .close {
                color: #aaaaaa;
                float: right;
                font-size: 28px;
                font-weight: bold;
            }

            .close:hover,
            .close:focus {
                color: #000;
                text-decoration: none;
                cursor: pointer;
            }
        </style>
        <script>
            function ValidForm() {
                var rm = document.getElementById('rm').value;
                var plant = document.getElementById('plant').value;

                if (rm !== '' && plant !== '') {
                    if (rm.length >= 8) {
                        document.getElementById('frm').submit();
                        document.getElementById('loader').style.display = 'block';
                    }
                }
            }
        </script>
        <style>
            #loader {
                border: 16px solid #f3f3f3;
                border-radius: 100%;
                border-top: 16px solid #3498db;
                width: 240px;
                height: 240px;
                -webkit-animation: spin 2s linear infinite;
                animation: spin 2s linear infinite;
                margin: auto;
                /*margin-left:1200px;*/
                margin-top: 10px;
                margin-bottom: 10px;
            }   

            .panel-default {
                border-color: transparent;
                background-color: transparent;
            }

            @-webkit-keyframes spin {
                0% { -webkit-transform: rotate(0deg); }
                100% { -webkit-transform: rotate(360deg); }
            }

            @keyframes spin {
                0% { transform: rotate(0deg); }
                100% { transform: rotate(360deg); }
            }

        </style>
        <script>
            $(document).ready(function () {
                $('#dimensionTable').DataTable({
                    "bPaginate": false,
                    "bLengthChange": false,
                    "bFilter": true,
                    "bInfo": false,
                    "bAutoWidth": false,
                    "ordering": false
                });

                removeTableSpace();
            });

            function removeTableSpace() {
                var wrapT = $('#dimensionTable_wrapper').children('.row');
                wrapT[0].remove();
                wrapT[2].remove();
            }
        </script>
    </head>    
    <body onload="document.getElementById('userid').value = sessionStorage.getItem('uid');
            $('.carousel').carousel('pause');">
        <div id="wrapper">

            <!-- /#sidebar-wrapper -->
            <%@ include file="../fragments/sidebar.jsp" %>
            <!--test-->

            <!-- Page Content -->
            <!-- nav-head-custom -->
            <%@ include file="../fragments/nav_head.jsp" %>

            <div class="container-fluid">
                <!--<div id="wrapper-top">-->
                <%-- PART 2 --%>
                <!--                    <div class="row">
                                        <div class="col-lg-12">
                                            <div id="set-height" style="height:415px;margin-top:0px;">
                                                <div id="sidebar-wrapper-top" class="">
                                                    <b class="page-header" style="padding-left:5px;font-size:18px;">
                <%-- <hr style="margin:0px;margin-top:10px;margin-bottom:10px;"> --%>
            </b>
        </div>
    </div>
</div>
</div> -->
                <%-- PART 3 --%>
                <!--show Qdest table-->
                <form action="../resources/manual/WMS970.pdf" target="_blank">
                    <table width="100%">
                        <tr>
                            <td width="94%"></td>
                            <td width="6%" align="right"><input type="submit" value="Manual" /></td>
                        </tr>
                    </table>
                </form>
                <form id="matMaster" action="" target="_blank">
                    <input type="hidden" id="userid" name="uid">
                    <input type="hidden" id="action" name="action" value="findQRMasterByQRId">
                    <input type="hidden" id="id" name="id">
                </form>
                <div style="border: 2px solid #ccc; border-radius: 5px; padding: 20px;">
                    <form id="frm" name="frm" action="display" method="post">
                        <table width="100%">
                            <tr>
                                <td width="20%"><b style="color: #00399b;">RM Style : </b>
                                    <input type="text" name="rm" id="rm" style=" height: 30px; width: 60%;" value="${rm}" autofocus>
                                    <script>
                                        var input = document.getElementById("rm");
                                        input.addEventListener("keyup", function (event) {
                                            input.value = input.value.toUpperCase();
                                            if (event.keyCode === 13) {
                                                event.preventDefault();
                                                ValidForm();
                                            }
                                        });
                                    </script> 
                                </td>
                                <td width="20%">
                                    <b style="color: #00399b;">Plant : </b>
                                    <select id="plant" name="plant" style=" width: 60%;" onchange="ValidForm();">
                                        <option value="${plant}" selected hidden>${plant}</option>
                                        <option value="1000">1000</option>
                                        <option value="1050">1050</option>
                                        <option value="9000">9000</option>
                                    </select>
                                </td>
                                <td width="25%">
                                    <div ${dimensionListShow}>
                                        <table id="dimensionTable" class="display" style="width:100%; border: 1px solid #ccc; border-radius: 4px;">
                                            <thead>
                                                <tr>
                                                    <th colspan="5" style="text-align: center;">Dimensions (cm)</th>
                                                </tr>
                                                <tr>
                                                    <th></th>
                                                    <th style="text-align: center;">กว้าง</th>
                                                    <th style="text-align: center;">ยาว</th>
                                                    <th style="text-align: center;">สูง</th>
                                                    <th style="text-align: center;">เส้นผ่าศูนย์กลาง</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <c:forEach varStatus="vs" items="${dimensionList}" var="x">
                                                    <tr>
                                                        <th>${x.pack}</th>
                                                        <td style="text-align: right;">${x.width}</td>
                                                        <td style="text-align: right;">${x.length}</td>
                                                        <td style="text-align: right;">${x.height}</td>
                                                        <td style="text-align: right;">${x.diameter}</td>
                                                    </tr>  
                                                </c:forEach>
                                            </tbody>
                                        </table>
                                    </div>
                                </td>
                                <td></td>
                                <td width="5%"></td>
                                <td width="5%"></td>
                                <td width="20%"><b style="color: #00399b;">Sorting Order : </b>
                                    <select id="so" name="so" style=" width: 60%;" onchange="ValidForm();">
                                        <option value="${so}" selected hidden>${son}</option>
                                        <option value="matCode">1 : รหัสวัตถุดิบ</option>
                                    </select>
                                </td>
                            </tr>
                        </table>
                    </form>
                    <br>
                    <div id="loader" style="display: none;"></div>
                    <table style=" width: 100%;">
                        <c:forEach varStatus="vs" items="${headList}" var="x">
                            <c:if test="${vs.index!=0}">
                                <tr>
                                    <th style=" text-align: center;">
                                        <h3>${x.COL}&nbsp;&nbsp;&nbsp;
                                            <a style=" cursor: pointer;" onclick="document.getElementById('myModal-img-${rm}${x.COL}').style.display = '${x.noi > 0 ? 'block':'none'}';">
                                                <i class="fa fa-picture-o" style=" font-size: 30px; color: ${x.noi > 0 ? '#00d431':'#dbdbdb'};"></i>
                                            </a>
                                        </h3>
                                        <c:if test="${x.noi > 0}">
                                            <div id="myModal-img-${rm}${x.COL}" class="modal" style="padding-top: 50px;">  
                                                <!-- Modal content -->
                                                <div class="modal-content" style=" height: fit-content; width: fit-content;">
                                                    <span id="span-img-${rm}${x.COL}" class="close" onclick="document.getElementById('myModal-img-${rm}${x.COL}').style.display = 'none';">&times;</span>
                                                    <div class="container">
                                                        <h2>Material Code : ${rm}${x.COL}</h2>  
                                                        <div id="myCarousel-${rm}${x.COL}" class="carousel slide" data-ride="carousel">
                                                            <!-- Indicators -->
                                                            <ol class="carousel-indicators" id="nav-img-${rm}${x.COL}">
                                                                <c:forEach varStatus="ip" items="${x.imageList}" var="p">
                                                                    <c:if test="${ip.index==0}">
                                                                        <li data-target="#myCarousel-${rm}${x.COL}" data-slide-to="0" class="active"></li>
                                                                        </c:if>
                                                                        <c:if test="${ip.index!=0}">
                                                                        <li data-target="#myCarousel-${rm}${x.COL}" data-slide-to="${ip.index}"></li>
                                                                        </c:if>
                                                                    </c:forEach>
                                                            </ol>

                                                            <!-- Wrapper for slides -->
                                                            <div class="carousel-inner" id="slid-img-${rm}${x.COL}"> 
                                                                <c:forEach varStatus="ip" items="${x.imageList}" var="p">
                                                                    <c:if test="${ip.index==0}">
                                                                        <div class="item active">
                                                                            <img src="http://10.11.9.142:8080/WMS/images/raw-materials/${p}" style="width: 100%;">
                                                                        </div>
                                                                    </c:if>
                                                                    <c:if test="${ip.index!=0}">     
                                                                        <div class="item">
                                                                            <img src="http://10.11.9.142:8080/WMS/images/raw-materials/${p}" style="width: 100%;"> 
                                                                        </div>  
                                                                    </c:if>
                                                                </c:forEach>   
                                                            </div>

                                                            <!-- Left and right controls -->
                                                            <a class="left carousel-control" href="#myCarousel-${rm}${x.COL}" data-slide="prev">
                                                                <span class="glyphicon glyphicon-chevron-left"></span>
                                                                <span class="sr-only">Previous</span>
                                                            </a>
                                                            <a class="right carousel-control" href="#myCarousel-${rm}${x.COL}" data-slide="next">
                                                                <span class="glyphicon glyphicon-chevron-right"></span>
                                                                <span class="sr-only">Next</span>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </c:if>
                                    </th>
                                    <%
                                        int it = 0;
                                    %>
                                    <c:forEach items="${x.detList}" var="y"> 
                                        <th style=" text-align: center;"><input type="text" style=" border: 1px solid #e8e8e8; width: 100px; text-align: right; cursor: pointer;" value="${y.QTY}" onclick="showModal('${y.CODE}', '${y.ID}', '${y.QTY}', '${y.PACKTYPE}', '${y.LOCATION}', '${y.UNIT}', '${y.DESCS}', '${y.GRPDESCS}');" readonly></th>
                                            <%
                                                it++;
                                                if (it == 10) {
                                                    it = 0;
                                            %>
                                    </tr>
                                    <tr>
                                        <th></th>
                                            <%
                                                }
                                            %>
                                        </c:forEach>
                                </tr>
                            </c:if>
                            <c:if test="${vs.index==0}">
                                <tr style="border-bottom:1pt solid #ccc;">
                                    <!--<th style=" text-align: center;"></th>-->
                                    <th style=" text-align: center;"></th>
                                    <th style=" text-align: right;"><h4><b>BAG :</b></h4></th>
                                    <th style=" text-align: center;"><input class="bg-primary" type="text" style=" width: 100px; text-align: right;" value="${x.BAG}" readonly></th>
                                    <!--<th style=" text-align: center;"></th>-->
                                    <th style=" text-align: right;"><h4><b>ROLL :</b></h4></th>
                                    <th style=" text-align: center;"><input class="bg-primary" type="text" style=" width: 100px; text-align: right;" value="${x.ROLL}" readonly></th>
                                    <!--<th style=" text-align: center;"></th>-->
                                    <th style=" text-align: right;"><h4><b>BOX :</b></h4></th>
                                    <th style=" text-align: center;"><input class="bg-primary" type="text" style=" width: 100px; text-align: right;" value="${x.BOX}" readonly></th>
                                    <th style=" text-align: center;"></th>
                                    <th style=" text-align: center;"><h4><b>TOTAL : ${x.COL}</b></h4></th>
                                    <th style=" text-align: center;"><input class="bg-primary" type="text" style=" width: 100px; text-align: right;" value="${x.TOTAL}" readonly></th>
                                    <th style=" text-align: center;"></th>
                                    <th style=" text-align: center;"><h4><b>Quantity</b></h4></th>
                                    <th style=" text-align: center;"><input class="bg-primary" type="text" style=" width: 100px; text-align: right;" value="${x.QTY}" readonly></th>
                                    <th style=" text-align: center;"><h4><b>${x.UNIT}</b></h4></th>
                                </tr>
                            </c:if>
                            <c:if test="${vs.index!=0}">
                                <tr style="border-bottom:1pt solid #ccc;">
                                    <!--<th style=" text-align: center;"></th>-->
                                    <th style=" text-align: center;"></th>
                                    <th style=" text-align: right;"><h4><b>BAG :</b></h4></th>
                                    <th style=" text-align: center;"><input class="bg-info" type="text" style=" width: 100px; text-align: right;" value="${x.BAG}" readonly></th>
                                    <!--<th style=" text-align: center;"></th>-->
                                    <th style=" text-align: right;"><h4><b>ROLL :</b></h4></th>
                                    <th style=" text-align: center;"><input class="bg-info" type="text" style=" width: 100px; text-align: right;" value="${x.ROLL}" readonly></th>
                                    <!--<th style=" text-align: center;"></th>-->
                                    <th style=" text-align: right;"><h4><b>BOX :</b></h4></th>
                                    <th style=" text-align: center;"><input class="bg-info" type="text" style=" width: 100px; text-align: right;" value="${x.BOX}" readonly></th>
                                    <th style=" text-align: center;"></th>
                                    <th style=" text-align: center;"><h4><b>TOTAL : ${x.COL}</b></h4></th>
                                    <th style=" text-align: center;"><input class="bg-info" type="text" style=" width: 100px; text-align: right;" value="${x.TOTAL}" readonly></th>
                                    <th style=" text-align: center;"></th>
                                    <th style=" text-align: center;"><h4><b>Quantity</b></h4></th>
                                    <th style=" text-align: center;"><input class="bg-info" type="text" style=" width: 100px; text-align: right;" value="${x.QTY}" readonly></th>
                                    <th style=" text-align: center;"><h4><b>${x.UNIT}</b></h4></th>
                                </tr>
                            </c:if>
                        </c:forEach>
                    </table>
                    <div id="myModal" class="modal">
                        <!-- Modal content -->
                        <div class="modal-content" style=" height: 380px; width: 500px;">
                            <span id="span" class="close" onclick="document.getElementById('myModal').style.display = 'none';">&times;</span>
                            <p><b><font size="4"> </font></b></p>
                            <table width="100%">
                                <tr style=" background-color: white;">
                                    <td align="center">
                                        <b style="color: #00399b;">
                                            <font size="5">Material Detail</font><hr>
                                            <table>
                                                <tr>
                                                    <td>
                                                        <font size="4">Code</font>
                                                    </td>
                                                    <td>
                                                        <font size="4">: &nbsp;&nbsp;&nbsp;<span id="dcode"></span></font>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <font size="4">Description &nbsp;&nbsp;&nbsp;</font>
                                                    </td>
                                                    <td>
                                                        <font size="4">: &nbsp;&nbsp;&nbsp;<span id="ddescs"></span></font>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <font size="4">Group Desc &nbsp;&nbsp;&nbsp;</font>
                                                    </td>
                                                    <td>
                                                        <font size="4">: &nbsp;&nbsp;&nbsp;<span id="dgrpdescs"></span></font>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <font size="4">ID</font>
                                                    </td>
                                                    <td>
                                                        <font size="4">: &nbsp;&nbsp;&nbsp;<span id="did"></span></font>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <font size="4">Quantity &nbsp;&nbsp;&nbsp;</font>
                                                    </td>
                                                    <td>
                                                        <font size="4">: &nbsp;&nbsp;&nbsp;<span id="dqty"></span></font>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <font size="4">Pack Type &nbsp;&nbsp;&nbsp;</font>
                                                    </td>
                                                    <td>
                                                        <font size="4">: &nbsp;&nbsp;&nbsp;<span id="dpacktype"></span></font>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <font size="4">Location &nbsp;&nbsp;&nbsp;</font>
                                                    </td>
                                                    <td>
                                                        <font size="4">: &nbsp;&nbsp;&nbsp;<span id="dlocation"></span></font>
                                                    </td>
                                                </tr>
                                            </table>
                                        </b>
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <br>
                                        <a class="btn btn btn-outline btn-info" onclick="document.getElementById('matMaster').submit();">
                                            Material Master
                                        </a>   
                                        &nbsp;&nbsp;
                                        <a class="btn btn btn-outline btn-danger" onclick="document.getElementById('myModal').style.display = 'none';">
                                            Close
                                        </a> 
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <!--**********************-->
                    <!--End Part 3-->
                    <!--</div>  end #wrapper-top--> 
                </div> 
                <br><br>
            </div> <!-- end .container-fluid -->
        </div> <!-- end #wrapper -->
    </body>
</html>