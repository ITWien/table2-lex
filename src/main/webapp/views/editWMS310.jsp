<%@ include file="../fragments/taglibs.jsp" %>
<%@ include file="../fragments/imports.jsp" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.sql.*;" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>WMS</title>
        <!-- css :: vendors -->
        <jsp:include page = "../fragments/css.jsp" />
        <!-- additional custom :: my-style -->
        <link rel="stylesheet" href="../resources/assets/styles/myStyles.css">

        <!-- js :: vendors -->
        <jsp:include page = "../fragments/script.jsp" />
        <!-- additional custom :: my-script -->
        <script src="../resources/assets/scripts/myScripts.js" async></script>
        <script src="../resources/assets/scripts/toggleLoad.js" async></script> 
        ${sendM}  
        <script>
            $(document).ready(function () {
                $('#showTable').DataTable({
                    "lengthMenu": [[-1, 10, 25, 50, 100], ["All", 10, 25, 50, 100]],
                    "bSortClasses": false,
                    "order": [[0, "desc"]],
                    "columnDefs": [
                        {orderable: false, targets: [10]}
//                        {"width": "20%", "targets": 4}
                    ]
                });

                // Setup - add a text input to each footer cell
                $('#showTable tfoot th').not(":eq(10)").each(function () {
                    var title = $(this).text();
                    $(this).html('<input type="text" id="search" />');
                });

                // DataTable
                var table = $('#showTable').DataTable();

                // Apply the search
                table.columns().every(function () {
                    var that = this;

                    $('input', this.footer()).on('keyup change', function () {
                        if (that.search() !== this.value) {
                            that
                                    .search(this.value)
                                    .draw();
                        }
                    });
                });
            });
        </script>
        <script>
            function checkAll(ele) {
                var checkboxes = document.getElementsByTagName('input');
                if (ele.checked) {
                    for (var i = 0; i < checkboxes.length; i++) {
                        if (checkboxes[i].type == 'checkbox') {
                            checkboxes[i].checked = true;
                        }
                    }
                } else {
                    for (var i = 0; i < checkboxes.length; i++) {
                        console.log(i)
                        if (checkboxes[i].type == 'checkbox') {
                            checkboxes[i].checked = false;
                        }
                    }
                }
            }
        </script>
        <script>
            submitForms1 = function (size) {
                if (size === 0) {
                    document.getElementById("detail").action = "rejected";
                    document.getElementById("detail").submit();
                } else {
                    var checkboxes = document.getElementsByTagName('input');
                    var nodataSelect = false;

                    for (var i = 0; i < checkboxes.length; i++) {
                        if (checkboxes[i].type === 'checkbox' && checkboxes[i].name !== 'selectAll') {
                            if (checkboxes[i].checked === true) {
                                nodataSelect = true;
                            }
                        }
                    }

                    if (nodataSelect) {
                        document.getElementById("detail").action = "rejected";
                        document.getElementById("detail").submit();
                    } else {
                        document.getElementById('myModal-4').style.display = 'block';
                    }
                }
            };

            submitForms2 = function () {
                var checkboxes = document.getElementsByTagName('input');
                var mvt = document.getElementById("mvtH");
                var nodataSelect = false;

                for (var i = 0; i < checkboxes.length; i++) {
                    if (checkboxes[i].type === 'checkbox' && checkboxes[i].name !== 'selectAll') {
                        if (checkboxes[i].checked === true) {
                            nodataSelect = true;
                        }
                    }
                }

                if (nodataSelect) {
                    if (mvt.value === "301") {
                        document.getElementById('myModal-2').style.display = 'block';
                    } else if (mvt.value === "311") {
                        document.getElementById('myModal-3').style.display = 'block';
                    } else {
                        document.getElementById("detail").action = "approved";
                        document.getElementById("detail").submit();
                    }
                } else {
                    document.getElementById('myModal-4').style.display = 'block';
                }
            };

            submitFormsAPP = function () {
                document.getElementById("detail").action = "approved";
                document.getElementById("detail").submit();
            };

            submitForms3 = function () {
                var checkboxes = document.getElementsByTagName('input');
                var nodataSelect = false;
                var isNotStatus9 = false;

                for (var i = 0; i < checkboxes.length; i++) {
                    if (checkboxes[i].type === 'checkbox' && checkboxes[i].name !== 'selectAll') {
                        if (checkboxes[i].checked === true) {
                            nodataSelect = true;
                            if (checkboxes[i].value.toString().split("-")[1] !== "9") {
                                isNotStatus9 = true;
                            }
                        }
                    }
                }

                if (nodataSelect) {
                    if (isNotStatus9) {
                        document.getElementById('myModal-5').style.display = 'block';
                    } else {
                        document.getElementById("detail").action = "released";
                        document.getElementById("detail").submit();
                    }
                } else {
                    document.getElementById('myModal-4').style.display = 'block';
                }
            };
        </script>
        <style type="text/css" media="screen">
            .form-valid.ng-invalid{
                background-color: #ffa17d;
            }
            table thead tr th {
                text-align: center;
            }
            .panel-right {
                border: 0.05em solid lightgray;
                border-radius: 0.25em 0.25em;
                margin-top: -1.7em;
                padding: 0.3em;
                padding-left: 0.6em;
                padding-right: 0.6em;
                font-size: 0.9em;
                float: right;
            }
            .panel-label-right{
                margin-top: -1.6em;
                padding: 0.3em;
                padding-left: 0.6em;
                padding-right: 0.6em;
                float: right;
            }
            .col-xs-1, .col-sm-1, .col-md-1, .col-lg-1, .col-xs-2, .col-sm-2, .col-md-2, .col-lg-2, .col-xs-3, .col-sm-3, .col-md-3, .col-lg-3, .col-xs-4, .col-sm-4, .col-md-4, .col-lg-4, .col-xs-5, .col-sm-5, .col-md-5, .col-lg-5, .col-xs-6, .col-sm-6, .col-md-6, .col-lg-6, .col-xs-7, .col-sm-7, .col-md-7, .col-lg-7, .col-xs-8, .col-sm-8, .col-md-8, .col-lg-8, .col-xs-9, .col-sm-9, .col-md-9, .col-lg-9, .col-xs-10, .col-sm-10, .col-md-10, .col-lg-10, .col-xs-11, .col-sm-11, .col-md-11, .col-lg-11, .col-xs-12, .col-sm-12, .col-md-12, .col-lg-12 {
                padding-right: 10px;
                padding-left: 10px;
            }

            input[id=search], input[id=plantFrom], input[id=plantTo], input[id=valueFrom], input[id=valueTo] {
                width: 100%;
                padding: 5px 5px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }

            input[id=totalQty] {
                width: 150px;
                padding: 5px 5px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
                text-align: right; 
                font-weight: bold;
            }

            input[type=button] {
                width: 100%;
                background-color: #ef604a;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=button]:hover {
                background-color: #e51e00;
            }

            button[name=ok] {
                width: 25%;
                background-color: #008cff;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            button[name=ok]:hover {
                background-color: #008cff;
            }
        </style>
        <style>
            body {font-family: Arial, Helvetica, sans-serif;}

            /* The Modal (background) */
            .modal {
                display: none; /* Hidden by default */
                position: fixed; /* Stay in place */
                z-index: 1; /* Sit on top */
                padding-top: 100px; /* Location of the box */
                left: 0;
                top: 0;
                width: 100%; /* Full width */
                height: 100%; /* Full height */
                overflow: auto; /* Enable scroll if needed */
                background-color: rgb(0,0,0); /* Fallback color */
                background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
            }

            /* Modal Content */
            .modal-content {
                background-color: #fefefe;
                margin: auto;
                padding: 20px;
                border: 1px solid #888;
                width: fit-content;
                height: 320px;
            }

            /* The Close Button */
            .close {
                color: #aaaaaa;
                float: right;
                font-size: 28px;
                font-weight: bold;
            }

            .close:hover,
            .close:focus {
                color: #000;
                text-decoration: none;
                cursor: pointer;
            }
        </style>
        <script>
            function EDIT() {
                document.getElementById('warehouseAdd').disabled = false;
                document.getElementById('queueAdd').disabled = false;
                document.getElementById('mvtAdd').disabled = false;
                document.getElementById('destAdd').disabled = false;
                document.getElementById('save').disabled = false;
            }
        </script>

    </head>    
    <body onload="document.getElementById('userid').value = sessionStorage.getItem('uid');">
        <div id="wrapper">

            <!-- /#sidebar-wrapper -->
            <%@ include file="../fragments/sidebar.jsp" %>

            <!-- Page Content -->
            <!-- nav-head-custom -->
            <%@ include file="../fragments/nav_head.jsp" %>

            <div class="container-fluid">
                <!--<div id="wrapper-top">-->
                <%-- PART 2 --%>
                <!--                    <div class="row">
                                        <div class="col-lg-12">
                                            <div id="set-height" style="height:415px;margin-top:0px;">
                                                <div id="sidebar-wrapper-top" class="">
                                                    <b class="page-header" style="padding-left:5px;font-size:18px;">
                <%-- <hr style="margin:0px;margin-top:10px;margin-bottom:10px;"> --%>
            </b>
        </div>
    </div>
</div>
</div> -->
                <%-- PART 3 --%>
                <form name="form" id="head" action="edit" method="post">
                    <input type="hidden" class="form-control" id="action" name="action" value="scanAdd">
                    <input type="hidden" class="form-control" id="type" name="type" >
                </form>
                <hr>
                <form id="qih" action="editH" method="post">
                    <table width="100%">
                        <tr>
                            <td width="10%" align="left">
                                <h4 style="color: #0060a5; font-weight: bold;">Warehouse : </h4>
                            </td>
                            <td width="15%" align="left" style="padding-right: 1%;">
                                <select class="form-control" name="warehouseAdd" id="warehouseAdd" disabled>  
                                    <option value="${wh}" >
                                        ${wh} : ${whn}
                                    </option>
                                </select>
                            </td>
                            <td width="10%" align="left">
                                <h4 style="color: #0060a5; font-weight: bold;">MVT : </h4>
                            </td>
                            <td width="15%" align="left" style="padding-right: 1%;">
                                <select class="form-control" name="mvtAdd" id="mvtAdd" disabled>                    
                                    <option value="${mvt}" hidden>
                                        ${mvt} : ${mvtn}
                                    </option>
                                    <c:forEach items="${mvtList}" var="p" varStatus="i">
                                        <option value="${p.code}">${p.code} : ${p.desc}</option>
                                    </c:forEach>
                                </select>
                            </td>
                            <td width="10%" align="left">
                                <h4 style="color: #0060a5; font-weight: bold;">Trans Date : </h4>
                            </td>
                            <td width="15%" align="left" style="padding-right: 1%;">
                                <input type="text" class="form-control" id="transDateAdd" name="transDateAdd" value="${transDate}" disabled style="cursor:context-menu;">
                            </td>
                            <td width="10%" align="left">
                                <h4 style="color: #0060a5; font-weight: bold;">User : </h4>
                            </td>
                            <td width="15%" align="left" style="padding-right: 1%;">
                                <h4 style="color: #0060a5; font-weight: bold;">${user}</h4>
                                <input type="hidden" class="form-control" id="uid" name="uid" value="${user}" disabled>
                            </td>
                        </tr>
                        <tr>
                            <td width="10%" align="left">
                                <h4 style="color: #0060a5; font-weight: bold;">Destination : </h4>
                            </td>
                            <td width="15%" align="left" style="padding-right: 1%;">
                                <select class="form-control" name="destAdd" id="destAdd" disabled>                    
                                    <option value="${dest}" hidden>
                                        ${dest}
                                    </option>
                                    <c:forEach items="${destList}" var="p" varStatus="i">
                                        <option value="${p.code}">${p.code} : ${p.desc}</option>
                                    </c:forEach>
                                </select>
                            </td>
                            <td width="10%" align="left">
                                <h4 style="color: #0060a5; font-weight: bold;">Total Docs : </h4>
                            </td>
                            <td width="15%" align="left" style="padding-right: 1%;">
                                <input type="text" class="form-control" id="docsAdd" name="docsAdd" value="" pattern="\d*" maxlength="3" disabled>
                            </td>
                            <td width="10%" align="left">
                                <h4 style="color: #0060a5; font-weight: bold;">SET : </h4>
                            </td>
                            <td width="15%" align="left" style="padding-right: 1%;">
                                <table width="100%">
                                    <tr>
                                        <td width="60%" align="left" style="padding-right: 2%;">
                                            <select class="form-control" name="setAdd" id="setAdd" disabled> 
                                                <option value="${set}" >
                                                    ${set}
                                                </option>
                                            </select>
                                        </td>
                                        <td width="40%" align="left">
                                            <input type="text" class="form-control" id="setDesc" value="${setTotal}" name="setDesc" disabled>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td width="10%" align="left">
                                <h4 style="color: #0060a5; font-weight: bold;">QUEUE NO. : </h4>
                            </td>
                            <td width="15%" align="left" style="padding-right: 1%;">
                                <input type="text" class="form-control" id="queueAdd" name="queueAdd" value="${qno}" onfocus="this.blur();" disabled>
                            </td>
                        </tr>
                    </table>
                    <table width="100%">
                        <tr>
                            <td width="10%" align="left">
                                <h4 style="color: #0060a5; font-weight: bold;">Product Group : </h4>
                            </td>
                            <td width="15%" align="left" style="padding-right: 1%;">
                                <select class="form-control" name="productGroupAdd" id="productGroupAdd" disabled>                    
                                    <option value="${pgroup}" >
                                        ${pgroup}
                                    </option>
                                </select>
                            </td>
                            <td width="10%" align="left">
                                <h4 style="color: #0060a5; font-weight: bold;">Material Control : </h4>
                            </td>
                            <td width="15%" align="left" style="padding-right: 1%;">
                                <select class="form-control" name="matControlAdd" id="matControlAdd" disabled>                    
                                    <option value="${mc}">
                                        ${mc}
                                    </option>
                                </select>
                            </td>
                            <td width="10%" align="left">
                                <button type="button" class="btn btn-primary" onclick="EDIT();"><i class="fa fa-edit" style="font-size:20px;"></i>&nbsp;&nbsp;Edit</button>
                                <button id="save" type="button" class="btn btn-success" onclick="this.form.submit();" disabled><i class="fa fa-save" style="font-size:20px;"></i>&nbsp;&nbsp;Save</button>
                            </td>
                            <td width="15%" align="left"></td>
                            <td width="10%" align="left"></td>
                            <td width="15%" align="left"></td>
                        </tr>
                    </table>
                </form>
                <hr>
                <!--END--> 
                <table width="100%">
                    <tr>
                        <td style="text-align:right;">
                            <a style="width: 120px;" class="btn btn-danger" onclick="location.href = '/TABLE2/WMS310/Fdisplay?WHS=${wh}&MTCTRL=${mc1}'">
                                <i class="fa fa-chevron-circle-left" style="font-size:20px;"></i> Back</a>

                            <a class="btn btn btn-outline btn-primary" id="released" name="released" onclick="submitForms3();">
                                <i class="fa fa-at" style="font-size:20px;"></i> Released</a>

                            <a class="btn btn btn-outline btn-success" id="approved" name="approved" onclick="submitForms2();">
                                <i class="fa fa-check-circle-o" style="font-size:20px;"></i> Approved</a>

                            <a class="btn btn btn-outline btn-danger" id="rejected" name="rejected" onclick="submitForms1(${size});">
                                <i class="fa fa-times-circle-o" style="font-size:20px;"></i> Canceled</a>
                        </td>
                    </tr>
                </table>
                <br>
                <div class="row">
                    <div class="col-sm-12 col-lg-12">
                        <div class="panel panel-primary" style="min-height: 485px;">
                            <div class="panel-heading">
                                <h4 style="margin:0px;">Approved</h4>
                            </div>
                            <!-- /.panel-heading -->
                            <div class="panel-body">
                                <div style=" position: absolute; top: 65px; right: 40%;">
                                    <b>TOTAL : </b>
                                    <input type="text" id="totalQty" name="totalQty" value="${totalQty}">
                                </div> 
                                <form id="detail" method="post">
                                    <input type="hidden" id="userid" name="userid">
                                    <input type="hidden" id="whH" name="whH" value="${wh}" >
                                    <input type="hidden" id="qnoH" name="qnoH" value="${qno}" >
                                    <input type="hidden" id="tqty" name="tqty" value="${totalQty}">
                                    <input type="hidden" id="mvtH" name="mvtH" value="${mvt}">
                                    <input type="hidden" id="plantH" name="plantH" value="${plantT}">
                                    <input type="hidden" id="size" name="size" value="${size}">
                                    <table id="showTable" class="display" style="width:100%; border: 1px solid #ccc; border-radius: 4px;">
                                        <thead>
                                            <tr>
                                                <th style="text-align: center;">No.</th>
                                                <th style="text-align: center;">Material Code</th>
                                                <th style="text-align: center;">Description</th>
                                                <th style="text-align: center;">Group QR ID</th>
                                                <th style="text-align: center;">ID</th>
                                                <th style="text-align: center;">Quantity</th>
                                                <th style="text-align: center;">U/M</th>
                                                <th style="text-align: center;">Package</th>
                                                <th style="text-align: center;">Remark</th>
                                                <th style="text-align: center;">Status</th>
                                                <td style="text-align: center;">Check All &nbsp; 
                                                    <input style="width: 30px; height: 30px;" type="checkbox" name="selectAll" onchange="checkAll(this)">
                                                </td>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th>No.</th>
                                                <th>Material Code</th>
                                                <th>Description</th>
                                                <th>Group QR ID</th>
                                                <th>ID</th>
                                                <th>Quantity</th>
                                                <th>U/M</th>
                                                <th>Package</th>
                                                <th>Remark</th>
                                                <th>Status</th>
                                                <th></th>
                                            </tr>
                                        </tfoot>
                                        <tbody>
                                            <c:forEach items="${deList}" var="x" varStatus="i">
                                                <tr>
                                                    <td align="center">${x.no}</td>
                                                    <td>${x.matc}</td>
                                                    <td>${x.desc}</td>
                                                    <td>${x.GID}</td>
                                                    <td>${x.id}</td>
                                                    <td align="right">${x.qty}</td>
                                                    <td align="center">${x.um}</td>
                                                    <td align="center">${x.pack}</td>
                                                    <td>${x.remark}</td>
                                                    <td>${x.status}</td>
                                                    <td align="center">
                                                        ${x.ckBox}
                                                    </td>
                                                </tr>
                                            </c:forEach>
                                        </tbody>
                                    </table>
                                </form>
                            </div>
                        </div>
                        <center>
                            <div id="myModal" class="modal fade" role="dialog">
                                <div class="modal-dialog modal-sm"> <!-- ???????????? modal ?????????????? ???? xs, sm, md, lg -->
                                    <div class="modal-md col-md-6" style="top: 150px; left:300px; background-color: #ddd; border: 1px solid #ccc; border-radius: 4px;">
                                        <div class="modal-header">
                                            <h4 class="modal-title" align="center">Only status is 9(Rejected) that can be released.</h4>
                                            <br>
                                            <button name="ok" type="button" class="close" data-dismiss="modal">
                                                <font color = "white">OK</font>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- devbanban.com -->
                        </center>
                        <div id="myModal-2" class="modal">
                            <!-- Modal content -->
                            <div class="modal-content" style=" height: 280px; width: 600px;">
                                <span id="span-2" class="close" onclick="document.getElementById('myModal-2').style.display = 'none';">&times;</span>
                                <p><b><font size="4"></font></b></p>
                                <table width="100%">
                                    <tr>
                                        <td align="center">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <b style="color: #00399b;"><font size="5">Transfer between PLANT</font></b>
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <br>
                                            <br>
                                            <table width="100%">
                                                <tr>
                                                    <th width="25%"></th>
                                                    <th width="25%" style="text-align: center;">From</th>
                                                    <th width="25%" style="text-align: center;">To</th>
                                                    <th width="25%"></th>
                                                </tr>
                                                <tr>
                                                    <th width="25%" style="text-align: center;">PLANT</th>
                                                    <th width="25%" style="text-align: center;"><input type="text" id="plantFrom" style="width: 95%; text-align: center;" value="${plantF}" disabled></th>
                                                    <th width="25%" style="text-align: center;"><input type="text" id="plantTo" style="width: 95%; text-align: center;" value="${plantT}" disabled></th>
                                                    <th width="25%"></th>
                                                </tr>
                                            </table>
                                            <br>
                                            <a class="btn btn btn-outline btn-danger" onclick="document.getElementById('myModal-2').style.display = 'none';">
                                                <i class="fa fa-times-circle-o" style="font-size:20px;"></i> Cancel</a>  
                                            &nbsp;
                                            <a class="btn btn btn-outline btn-success" onclick="submitFormsAPP();">
                                                <i class="fa fa-check-circle-o" style="font-size:20px;"></i> Confirm</a>                      
                                        </td>
                                    </tr>
                                </table>
                            </div>

                        </div>
                        <div id="myModal-3" class="modal">
                            <!-- Modal content -->
                            <div class="modal-content" style=" height: 280px; width: 600px;">
                                <span id="span-3" class="close" onclick="document.getElementById('myModal-3').style.display = 'none';">&times;</span>
                                <p><b><font size="4"></font></b></p>
                                <table width="100%">
                                    <tr>
                                        <td align="center">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <b style="color: #00399b;"><font size="5">DEVALUE</font></b>
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <br>
                                            <br>
                                            <table width="100%">
                                                <tr>
                                                    <th width="25%"></th>
                                                    <th width="25%" style="text-align: center;">From</th>
                                                    <th width="25%" style="text-align: center;">To</th>
                                                    <th width="25%"></th>
                                                </tr>
                                                <tr>
                                                    <th width="25%" style="text-align: center;">Devalue</th>
                                                    <th width="25%" style="text-align: center;"><input type="text" id="valueFrom" style="width: 95%; text-align: center;" value="01" disabled></th>
                                                    <th width="25%" style="text-align: center;"><input type="text" id="valueTo" style="width: 95%; text-align: center;" value="03" disabled></th>
                                                    <th width="25%"></th>
                                                </tr>
                                            </table>
                                            <br>
                                            <a class="btn btn btn-outline btn-danger" onclick="document.getElementById('myModal-3').style.display = 'none';">
                                                <i class="fa fa-times-circle-o" style="font-size:20px;"></i> Cancel</a>  
                                            &nbsp;
                                            <a class="btn btn btn-outline btn-success" onclick="submitFormsAPP();">
                                                <i class="fa fa-check-circle-o" style="font-size:20px;"></i> Confirm</a>                    
                                        </td>
                                    </tr>
                                </table>
                            </div>

                        </div>
                        <div id="myModal-4" class="modal">
                            <!-- Modal content -->
                            <div class="modal-content" style=" height: 200px; width: 500px;">
                                <span id="span-4" class="close" onclick="document.getElementById('myModal-4').style.display = 'none';">&times;</span>
                                <p><b><font size="4"></font></b></p>
                                <table width="100%">
                                    <tr>
                                        <td align="center">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <b style="color: #00399b;"><font size="5">Please select the data !</font></b>
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <br>
                                            <br>
                                            <br>
                                            <a style=" width: 100px;" class="btn btn btn-outline btn-danger" onclick="document.getElementById('myModal-4').style.display = 'none';">
                                                OK
                                            </a>                 
                                        </td>
                                    </tr>
                                </table>
                            </div>

                        </div>
                        <div id="myModal-5" class="modal">
                            <!-- Modal content -->
                            <div class="modal-content" style=" height: 220px; width: 700px;">
                                <span id="span-5" class="close" onclick="document.getElementById('myModal-5').style.display = 'none';">&times;</span>
                                <p><b><font size="4"></font></b></p>
                                <table width="100%">
                                    <tr>
                                        <td align="center">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <b style="color: #00399b;"><font size="5">Only status is 9 : Canceled that can be released !</font></b>
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <br>
                                            <br>
                                            <br>
                                            <a style=" width: 100px;" class="btn btn btn-outline btn-danger" onclick="document.getElementById('myModal-5').style.display = 'none';">
                                                OK
                                            </a>                 
                                        </td>
                                    </tr>
                                </table>
                            </div>

                        </div>
                    </div>
                </div>
                <br>
            </div> <!-- end .container-fluid -->
        </div> <!-- end #wrapper -->
    </body>
</html>