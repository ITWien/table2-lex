<%@ include file="../fragments/taglibs.jsp" %>
<%@ include file="../fragments/imports.jsp" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.sql.*;" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>WMS</title>
        <!-- css :: vendors -->
        <jsp:include page = "../fragments/css.jsp" />
        <!-- additional custom :: my-style -->
        <link rel="stylesheet" href="../resources/assets/styles/myStyles.css">

        <!-- js :: vendors -->
        <jsp:include page = "../fragments/script.jsp" />
        <!-- additional custom :: my-script -->
        <script src="../resources/assets/scripts/myScripts.js" async></script>
        <script src="../resources/assets/scripts/toggleLoad.js" async></script>  
        <!--<script src="../../WMSMenuControl/resources/assets/scripts/isLogin.js"></script>-->  
        <script>
            $(document).ready(function () {
                $('#showTable').DataTable({
                    "paging": false,
                    "ordering": false,
                    "lengthMenu": [[10, 25, 50, 100], [10, 25, 50, 100]],
                    "bSortClasses": false,
                    columnDefs: [
//                        {orderable: false, targets: [10]},
//                        {"width": "10%", "targets": 2},
//                        {"width": "10%", "targets": 3},
//                        {"width": "10%", "targets": 10},
//                        {"width": "10%", "targets": 11},
//                        {"width": "10%", "targets": 12},
//                        {"width": "10%", "targets": 13},
//                        {"width": "7%", "targets": 14},
                    ]
                });

                // Setup - add a text input to each footer cell
//                $('#showTable tfoot th').each(function () {
//                    var title = $(this).text();
//                    $(this).html('<input type="text" />');
//                });
                // DataTable
//                var table = $('#showTable').DataTable();
                // Apply the search
//                table.columns().every(function () {
//                    var that = this;
//                    $('input', this.footer()).on('keyup change', function () {
//                        if (that.search() !== this.value) {
//                            that
//                                    .search(this.value)
//                                    .draw();
//                        }
//                    });
//                });
            });
        </script>
        <style>
            input[type=text], select {
                width: 100%;
                padding: 5px 5px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }

            input[type=date], input[type=number]{
                /*width: 100%;*/
                padding: 5px 5px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }

            input[type=submit] {
                width: 100%;
                background-color: #00399b;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=submit]:hover {
                background-color: #3973d6;
            }

            button[type=submit] {
                background: transparent;
                border: none !important;
            }

            i[id=ic]:hover {
                background-color: #042987;
                border-radius: 15px;
            }

            i[id=ic2]:hover {
                background-color: #a12828;
                border-radius: 15px;
            }

            i[id=ic3]:hover {
                background-color: #0b8c00;
                border-radius: 15px;
            }
        </style>
        <style>
            body {font-family: Arial, Helvetica, sans-serif;}

            /* The Modal (background) */
            .modal {
                display: none; /* Hidden by default */
                position: fixed; /* Stay in place */
                z-index: 1; /* Sit on top */
                padding-top: 100px; /* Location of the box */
                left: 0;
                top: 0;
                width: 100%; /* Full width */
                height: 100%; /* Full height */
                overflow: auto; /* Enable scroll if needed */
                background-color: rgb(0,0,0); /* Fallback color */
                background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
            }

            /* Modal Content */
            .modal-content {
                background-color: #fefefe;
                margin: auto;
                padding: 20px;
                border: 1px solid #888;
                width: fit-content;
                height: 320px;
            }

            /* The Close Button */
            .close {
                color: #aaaaaa;
                float: right;
                font-size: 28px;
                font-weight: bold;
            }

            .close:hover,
            .close:focus {
                color: #000;
                text-decoration: none;
                cursor: pointer;
            }
        </style>
        <style>
            body {font-family: Arial, Helvetica, sans-serif;}

            /* The Modal (background) */
            .modal {
                display: none; /* Hidden by default */
                position: fixed; /* Stay in place */
                z-index: 1; /* Sit on top */
                padding-top: 100px; /* Location of the box */
                left: 0;
                top: 0;
                width: 100%; /* Full width */
                height: 100%; /* Full height */
                overflow: auto; /* Enable scroll if needed */
                background-color: rgb(0,0,0); /* Fallback color */
                background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
            }

            /* Modal Content */
            .modal-content {
                background-color: #fefefe;
                margin: auto;
                padding: 20px;
                border: 1px solid #888;
                width: fit-content;
                height: 320px;
            }

            /* The Close Button */
            .close {
                color: #aaaaaa;
                float: right;
                font-size: 28px;
                font-weight: bold;
            }

            .close:hover,
            .close:focus {
                color: #000;
                text-decoration: none;
                cursor: pointer;
            }
        </style>
    </head>    
    <body onload="document.getElementById('userid').value = sessionStorage.getItem('uid');">
        <div id="wrapper">

            <!-- /#sidebar-wrapper -->
            <%@ include file="../fragments/sidebar.jsp" %>

            <!-- Page Content -->
            <!-- nav-head-custom -->
            <%@ include file="../fragments/nav_head.jsp" %>

            <div class="container-fluid">
                <!--<div id="wrapper-top">-->
                <%-- PART 2 --%>
                <!--                    <div class="row">
                                        <div class="col-lg-12">
                                            <div id="set-height" style="height:415px;margin-top:0px;">
                                                <div id="sidebar-wrapper-top" class="">
                                                    <b class="page-header" style="padding-left:5px;font-size:18px;">
                <%-- <hr style="margin:0px;margin-top:10px;margin-bottom:10px;"> --%>
            </b>
        </div>
    </div>
</div>
</div> -->
                <%-- PART 3 --%>
                <!--show Qdest table-->
                <br>
                <div style="border: 2px solid #ccc; border-radius: 5px; padding: 20px;">
                    <form id="formData" name="formData" action="create" method="post">
                        <input type="hidden" id="preDel" name="preDel">
                        <table width="100%">
                            <tr>
                                <td width="10%">
                                    <b style="color: #00399b;">Warehouse : </b>
                                </td>
                                <td width="15%">
                                    <select name="wh" id="wh" >
                                        <option value="${wh}" selected hidden>${wh} : ${whn}</option>
                                        <%--<c:forEach items="${MCList}" var="p" varStatus="i">--%>
                                            <!--<option value="${p.uid}">${p.uid} : ${p.name}</option>-->
                                        <%--</c:forEach>--%>
                                    </select>
                                </td>
                                <td width="10%"></td>
                                <td width="15%"></td>
                                <td width="10%"></td>
                                <td width="15%"></td>
                                <td width="2%"></td>
                                <td width="8%">
                                    <b style="color: #00399b;">Shipment Date : </b>
                                </td>
                                <td width="15%">
                                    <select name="shipDate" id="shipDate">
                                        <option value="${shipDate}" selected hidden>${shipDate2}</option>
                                        <%--<c:forEach items="${shipDateList}" var="x">--%>
                                            <!--<option value="${x.code}">${x.desc}</option>-->
                                        <%--</c:forEach>--%>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td width="10%">
                                    <b style="color: #00399b;">License No. : </b>
                                </td>
                                <td width="15%">
                                    <select name="lino" id="lino">
                                        <option value="${lino}" selected hidden>${lino}</option>
                                        <c:forEach items="${linoList}" var="x">
                                            <option value="${x.code}">${x.code}</option>
                                        </c:forEach>
                                    </select>
                                </td>
                                <td width="10%" style=" text-align: center;">
                                    <b style="color: #00399b;">Driver Name : </b>
                                </td>
                                <td width="15%">
                                    <input type="text" name="driver" style=" border-color: transparent;" id="driver" value="${driver}" readonly>
                                </td>
                                <td width="10%" style=" text-align: center;">
                                    <b style="color: #00399b;">Follower Name : </b>
                                </td>
                                <td width="15%">
                                    <input type="text" name="follower" style=" border-color: transparent;" id="follower" value="${follower}" readonly>
                                </td>
                                <td width="2%"></td>
                                <td width="8%">
                                    <b style="color: #00399b;">Round : </b>
                                </td>
                                <td width="15%">
                                    <select name="round" id="round">
                                        <option value="${round}" selected hidden>${round}</option>
                                        <c:forEach items="${roundList}" var="x">
                                            <option value="${x.code}">${x.code}</option>
                                        </c:forEach>
                                    </select>
                                </td>
                            </tr>
                        </table>
                        <br>
                        <center>
                            <span class="fa-stack fa-lg fa-2x" onclick="window.location.href = '/TABLE2/XMS800/display?shipDate=${shipDate}&wh=${wh}';" style=" cursor: pointer;">
                                <i id="ic2" class="fa fa-circle fa-stack-2x" style="color: #d43737;"></i>
                                <i id="ic2" class="fa fa-arrow-left fa-stack-1x fa-inverse"></i>
                            </span>
                            <span class="fa-stack fa-lg fa-2x" onclick="document.getElementById('formData').submit();" style=" cursor: pointer;">
                                <i id="ic" class="fa fa-circle fa-stack-2x" style="color: #154baf;"></i>
                                <i id="ic" class="fa fa-download fa-stack-1x fa-inverse"></i>
                            </span>
                            <a class="fa-stack fa-lg fa-2x" href="/TABLE2/XMS800/print?qno=${qno}&wh=${wh}" target="_blank" style=" cursor: pointer; ${added}">
                                <i id="ic3" class="fa fa-circle fa-stack-2x" style="color: #0b8c00;"></i>
                                <i id="ic3" class="fa fa-print fa-stack-1x fa-inverse"></i>
                            </a>
                        </center>
                        <input type="hidden" id="userid" name="userid">
                        <table id="showTable" class="display" style="width:50%; border: 1px solid #ccc; border-radius: 4px;">
                            <thead> 
                                <tr>
                                    <th colspan="1" style="text-align: center; border-bottom: none;"></th>
                                    <th colspan="4" style="text-align: center; border-bottom-color: #ccc;">Package</th>
                                    <th colspan="1" style="text-align: center; border-bottom: none; background-color: #b0d7ff;"></th>
                                </tr>
                                <tr>
                                    <th style="text-align: right;"></th>
                                    <th style="text-align: center;">Bag</th>
                                    <th style="text-align: center;">Roll</th>
                                    <th style="text-align: center;">Box</th>
                                    <th style="text-align: center;">Pcs</th>
                                    <th style="text-align: center; background-color: #b0d7ff;">Total Package</th>
                                </tr>
                            </thead>
                            <tbody id="det-body">
                                <c:forEach items="${detList}" var="x">
                                    <tr>
                                        <td style="text-align: left; ${x.borderTop}">${x.XMSWIDDEST} : ${x.XMSWIDDESTN}</td>
                                        <td style="text-align: center; ${x.borderTop}">${x.XMSWIDBAG}</td>
                                        <td style="text-align: center; ${x.borderTop}">${x.XMSWIDROLL}</td>
                                        <td style="text-align: center; ${x.borderTop}">${x.XMSWIDBOX}</td>
                                        <td style="text-align: center; ${x.borderTop}">${x.XMSWIDPCS}</td>
                                        <td style="text-align: center; background-color: #b0d7ff; ${x.borderTop}"><fmt:formatNumber pattern="###,###,###,##0" value="${x.XMSWIDTOT}" /></td>
                                    </tr>
                                </c:forEach>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th style="text-align: left;">Total</th>
                                    <th style="text-align: center;"><fmt:formatNumber pattern="###,###,###,##0" value="${XMSWIDBAGsum}" /></th>
                                    <th style="text-align: center;"><fmt:formatNumber pattern="###,###,###,##0" value="${XMSWIDROLLsum}" /></th>
                                    <th style="text-align: center;"><fmt:formatNumber pattern="###,###,###,##0" value="${XMSWIDBOXsum}" /></th>
                                    <th style="text-align: center;"><fmt:formatNumber pattern="###,###,###,##0" value="${XMSWIDPCSsum}" /></th>
                                    <th style="text-align: center; background-color: #b0d7ff;"><fmt:formatNumber pattern="###,###,###,##0" value="${XMSWIDTOTsum}" /></th>
                                </tr>
                            </tfoot>
                        </table>
                    </form>
                </div>
                <!--End Part 3-->
                <br>
                <!--</div>  end #wrapper-top--> 
            </div> <!-- end .container-fluid -->
        </div> <!-- end #wrapper -->
    </body>
</html>