<%@ include file="../fragments/taglibs.jsp" %>
<%@ include file="../fragments/imports.jsp" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.sql.*;" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>WMS</title>
        <!-- css :: vendors -->
        <jsp:include page = "../fragments/css.jsp" />
        <!-- additional custom :: my-style -->
        <link rel="stylesheet" href="../resources/assets/styles/myStyles.css">

        <!-- js :: vendors -->
        <jsp:include page = "../fragments/script.jsp" />
        <!-- additional custom :: my-script -->
        <script src="../resources/assets/scripts/myScripts.js" async></script>
        <script src="../resources/assets/scripts/toggleLoad.js" async></script>   
        <script>
            $(document).ready(function () {
                $('#showTable').DataTable({
                    "lengthMenu": [[10, 25, 50, 100], [10, 25, 50, 100]],
                    "bSortClasses": false
                });
                // Setup - add a text input to each footer cell
                $('#showTable tfoot th').not(":eq(3)").each(function () {
                    var title = $(this).text();
                    $(this).html('<input type="text" />');
                });
                // DataTable
                var table = $('#showTable').DataTable();
                // Apply the search
                table.columns().every(function () {
                    var that = this;
                    $('input', this.footer()).on('keyup change', function () {
                        if (that.search() !== this.value) {
                            that
                                    .search(this.value)
                                    .draw();
                        }
                    });
                });
            });
        </script>
        <style>
            input[type=text], select {
                width: 100%;
                padding: 5px 5px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }

            input[type=submit] {
                width: 100%;
                background-color: #00399b;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=submit]:hover {
                background-color: #3973d6;
            }
        </style>
    </head>    
    <body >
        <div id="wrapper">

            <!-- /#sidebar-wrapper -->
            <%@ include file="../fragments/sidebar.jsp" %>

            <!-- Page Content -->
            <!-- nav-head-custom -->
            <%@ include file="../fragments/nav_head.jsp" %>

            <div class="container-fluid">
                <!--<div id="wrapper-top">-->
                <%-- PART 2 --%>
                <!--                    <div class="row">
                                        <div class="col-lg-12">
                                            <div id="set-height" style="height:415px;margin-top:0px;">
                                                <div id="sidebar-wrapper-top" class="">
                                                    <b class="page-header" style="padding-left:5px;font-size:18px;">
                <%-- <hr style="margin:0px;margin-top:10px;margin-bottom:10px;"> --%>
            </b>
        </div>
    </div>
</div>
</div> -->
                <%-- PART 3 --%>
                <!--show Qdest table-->
                <form action="../resources/manual/WMS004.pdf" target="_blank">
                    <table width="100%">
                        <tr>
                            <td width="94%"></td>
                            <td width="6%" align="right"><input type="submit" value="Manual" /></td>
                        </tr>
                    </table>
                </form>
                <div style="border: 2px solid #ccc; border-radius: 5px; padding: 20px;">
                    <table id="showTable" class="display" style="width:100%; border: 1px solid #ccc; border-radius: 4px;">
                        <thead>
                            <tr>
                                <th>Material Control</th>
                                <th>Description</th>
                                <th>Warehouse</th>
                                <th>Option<a href="create"><i class="glyphicon glyphicon-plus-sign" style="font-size:15px; padding-left: 10px"></i></a></th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>Material Control</th>
                                <th>Description</th>
                                <th>Warehouse</th>
                                <th></th>
                            </tr>
                        </tfoot>
                        <tbody>
                            <c:forEach items="${MCList}" var="p" varStatus="i">
                                <tr>
                                    <td>${p.uid}</td>
                                    <td>${p.name}</td>
                                    <td>${p.warehouse}</td>
                                    <td>
                                        <a href="edit?uid=${p.uid}" ><i class="glyphicon glyphicon-edit" style="font-size:15px;"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <a href="delete?uid=${p.uid}" ><i class="glyphicon glyphicon-trash" style="font-size:15px;"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <!--<a href="ndisplay?uid=${p.uid}" ><i class="fa fa-sitemap" style="font-size:30px;"></i></a>-->
    <!--                                    <a href="edit?uid=${p.uid}"><img src="../resources/images/icons/edit.png" width="25" height="25" ></a>
                                        <a href="delete?uid=${p.uid}"><img src="../resources/images/icons/trash.png" width="25" height="25" ></a>-->
                                        <a href="ndisplay?uid=${p.uid}"><img src="../resources/images/icons/connection.png" width="20" height="20" ></a>
                                    </td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>
                <!--End Part 3-->
                <br>
                <!--</div>  end #wrapper-top--> 
            </div> <!-- end .container-fluid -->
        </div> <!-- end #wrapper -->
    </body>
</html>