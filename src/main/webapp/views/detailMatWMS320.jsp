<%@ include file="../fragments/taglibs.jsp" %>
<%@ include file="../fragments/imports.jsp" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.sql.*;" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>WMS</title>
        <!-- css :: vendors -->
        <jsp:include page = "../fragments/css.jsp" />
        <!-- additional custom :: my-style -->
        <link rel="stylesheet" href="../resources/assets/styles/myStyles.css">

        <!-- js :: vendors -->
        <jsp:include page = "../fragments/script.jsp" />
        <!-- additional custom :: my-script -->
        <script src="../resources/assets/scripts/myScripts.js" async></script>
        <script src="../resources/assets/scripts/toggleLoad.js" async></script> 
        <style>
            input[type=text], select {
                width: 100%;
                padding: 5px 5px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }

            input[type=submit] {
                width: 100%;
                background-color: #00399b;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=submit]:hover {
                background-color: #3973d6;
            }

            input[type=button] {
                width: 100%;
                background-color: #ef604a;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=button]:hover {
                background-color: #e51e00;
            }
        </style>
    </head>    
    <body >
        <div id="wrapper">

            <!-- /#sidebar-wrapper -->
            <%@ include file="../fragments/sidebar.jsp" %>

            <!-- Page Content -->
            <!-- nav-head-custom -->
            <%@ include file="../fragments/nav_head.jsp" %>

            <div class="container-fluid">
                <!--<div id="wrapper-top">-->
                <%-- PART 2 --%>
                <!--                    <div class="row">
                                        <div class="col-lg-12">
                                            <div id="set-height" style="height:415px;margin-top:0px;">
                                                <div id="sidebar-wrapper-top" class="">
                                                    <b class="page-header" style="padding-left:5px;font-size:18px;">
                <%-- <hr style="margin:0px;margin-top:10px;margin-bottom:10px;"> --%>
            </b>
        </div>
    </div>
</div>
</div> -->
                <%-- PART 3 --%>
                <!--show Qdest table-->
                <form action="../resources/manual/WMS320.pdf" target="_blank">
                    <table width="100%">
                        <tr>
                            <td width="94%"></td>
                            <td width="6%" align="right"><input type="submit" value="Manual" /></td>
                        </tr>
                    </table>
                </form>
                <div style="border: 2px solid #ccc; border-radius: 5px; padding: 20px;">
                    <table width="100%">
                        <tr>
                            <th width="50%"><b style="color: #00399b;">Warehouse :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;${wh} : ${whn}</b></th>
                        </tr>
                    </table>
                    <!--****************-->
                    <c:forEach items="${detList}" var="p">
                        <hr style="height: 3px;">
                        <table width="100%">
                            <tr>
                                <th width="50%">
                                    <b style="color: #00399b;">
                                        Material Control :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;${mat} : ${matn}
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        Destination :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;${p.uid} : ${p.name} 
                                    </b>
                                </th>
                                <td style="text-align:right;">
                                    <a style="width: 120px;" class="btn btn-danger" onclick="window.history.back()">
                                        <i class="fa fa-chevron-circle-left" style="font-size:20px;"></i> Back</a>
                                </td>
                            </tr>
                        </table>
                        <script>
                            $(document).ready(function () {
                                $('#showTable-${p.uid}').DataTable({
                                    "lengthMenu": [[10, 25, 50, 100], [10, 25, 50, 100]],
                                    "bSortClasses": false
                                });

                                // Setup - add a text input to each footer cell
                                $('#showTable-${p.uid} tfoot th').each(function () {
                                    var title = $(this).text();
                                    $(this).html('<input type="text" />');
                                });

                                // DataTable
                                var table = $('#showTable-${p.uid}').DataTable();

                                // Apply the search
                                table.columns().every(function () {
                                    var that = this;

                                    $('input', this.footer()).on('keyup change', function () {
                                        if (that.search() !== this.value) {
                                            that
                                                    .search(this.value)
                                                    .draw();
                                        }
                                    });
                                });
                            });
                        </script>
                        <table id="showTable-${p.uid}" class="display" style="width:100%; border: 1px solid #ccc; border-radius: 4px;">
                            <thead>
                                <tr>
                                    <th style="text-align: center;">Material Code</th>
                                    <th style="text-align: center;">Description</th>
                                    <th style="text-align: center;">Quantity</th>
                                    <th style="text-align: center;">U/M</th>
                                    <th style="text-align: center;">Q'Pack</th>
                                    <th style="text-align: center;">Package</th>
                                    <th style="text-align: center;">M<sup>3</sup></th>
                                    <th style="text-align: center;">Running no.</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th>Material Code</th>
                                    <th>Description</th>
                                    <th>Quantity</th>
                                    <th>U/M</th>
                                    <th>Q'Pack</th>
                                    <th>Package</th>
                                    <th>M<sup>3</sup></th>
                                    <th>Running no.</th>
                                </tr>
                            </tfoot>
                            <tbody>
                                <c:forEach items="${p.detList}" var="x">
                                    <tr>
                                        <td>${x.matc}</td>
                                        <td>${x.desc}</td>
                                        <td align="right">${x.qty}&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                        <td align="center">${x.um}</td>
                                        <td align="center">${x.no}</td>
                                        <td align="center">${x.pack}</td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                </c:forEach>
                            </tbody>
                        </table>
                    </c:forEach>
                    <!--**********************-->
                    <!--End Part 3-->
                    <!--</div>  end #wrapper-top--> 
                </div> 
                <br>
            </div> <!-- end .container-fluid -->
        </div> <!-- end #wrapper -->
    </body>
</html>