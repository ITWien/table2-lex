<%@ include file="../fragments/taglibs.jsp" %>
<%@ include file="../fragments/imports.jsp" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.sql.*;" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>WMS</title>
        <!-- css :: vendors -->
        <jsp:include page = "../fragments/css.jsp" />
        <!-- additional custom :: my-style -->
        <!--<link rel="stylesheet" href="../resources/assets/styles/myStyles.css">-->

        <!-- js :: vendors -->
        <jsp:include page = "../fragments/script.jsp" />
        <!-- additional custom :: my-script -->
        <script src="../resources/assets/scripts/myScripts.js" async></script>
        <script src="../resources/assets/scripts/toggleLoad.js" async></script> 

        <style>
            #sidebar-wrapper { 
                background-color: #f8f8f8;
                border-right:1px solid #e5e5e5;
            }
            body{
                background-color: #FFFFFF;
                font-size: 15px;
                min-width: 1024px;
            }
            .head-menu {
                margin-bottom:11px; 
                margin-top:11px;
                color:#777;
                font-weight: bold;
            }
            .head-custom {
                margin: 0px;
                color:#2E3E92;
                font-weight: bold;
            }
            .modal-title {
                font-size: 20px;
                color:#2E3E92;
                font-weight: bold;
            }
            .modal .modal-dialog { 
                width: 65%; 
                min-width: 700px;
            }
            .modal .modal-dialog.smaller {
                width: 30%;
                min-width:250px;
            }
            .custom-toggle-menu {
                margin:0px;
                margin-right:5px;
            }
            .nav-head-custom {
                height: 42px;
                width: 100%;
                background-color: #f8f8f8;
                border-bottom:1px solid #e5e5e5;
            }
            .page-header {
                color:#226097;
                font-weight: bold;
            }
            .icon-size-sm {
                font-size: 16px;
            }
            .icon-size-lg {
                font-size: 20px;
            }
            .btn-size-sm {
                padding-top:7px;
                padding-bottom:3px;
            }
            .custom-body {
                border-radius: 10px;
                background-color: #F7F7F7;
                width: 320px;
                padding: 10px;
                height: 40px;
                border-width:1.8px; 
                border-style:dashed;
                border-color: #C1C1C1;
            }

            .custom-icon-w-b i {
                color: white;
            } 
            .custom-text {
                color:#777;
            }
            tfoot input {
                width: 100%;
                padding: 3px;
                box-sizing: border-box;
            }
            .dataTables_filter{ 
                display: none; 
            }
            .checkbox.form-check-data {
                margin-top: 5px;
                margin-bottom: 0px;
            }

            .form-value-type {
                padding-left: 5px;
                padding-top: 10px;
            }

            .text-center {
                text-align: center;
            }

            .has-line-bottom {
                border-bottom-style: solid;
                border-bottom-width: thin;
            }
        </style>
        <style>
            input[type=text], select{
                width: 100%;
                padding: 5px 5px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }

            input[type=date], input[type=number]{
                /*width: 100%;*/
                padding: 5px 5px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }

            input[type=submit], button {
                width: 100%;
                background-color: #00399b;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=submit]:hover, button:hover {
                background-color: #3973d6;
            }

            input[type=button] {
                width: 100%;
                background-color: #ef604a;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=button]:hover {
                background-color: #e51e00;
            }
        </style>
        <script>
            $(document).ready(function () {
                $('#showTable').DataTable({
                    fixedHeader: true,
                    "paging": false,
                    "lengthMenu": [[-1, 10, 25, 50, 100], ["All", 10, 25, 50, 100]],
                    "bSortClasses": false,
                    'rowCallback': function (row, data, index) {
                        $(row).find('td:eq(7)').css('background-color', '#91c2ff');
                    },
                    fixedColumns: true,
//                    "order": [[0, "desc"]],
                    "columnDefs": [
//                        {visible: false, targets: [2]},
//                        {visible: false, targets: [3]},
//                        {visible: false, targets: [4]},
//                        {visible: false, targets: [5]},

//                        {orderable: false, targets: [6]},
                        {orderable: false, targets: [7]},
                        {orderable: false, targets: [8]},
                        {orderable: false, targets: [9]},
                        {orderable: false, targets: [10]},
                        {orderable: false, targets: [11]},
//                        {orderable: false, targets: [12]},
//                        {orderable: false, targets: [13]},
//                        {orderable: false, targets: [14]},
//                        {orderable: false, targets: [15]},
//                        {orderable: false, targets: [16]},
//                        {targets: 6, className: 'dt-body-right'},
                        {targets: 7, className: 'dt-body-right'},
                        {targets: 8, className: 'dt-body-right'},
                        {targets: 9, className: 'dt-body-right'},
                        {targets: 10, className: 'dt-body-right'},
                        {targets: 11, className: 'dt-body-right'},
                        {targets: 12, className: 'dt-body-right'},
//                        {targets: 12, className: 'dt-body-right'},
//                        {targets: 13, className: 'dt-body-right'},
//                        {targets: 14, className: 'dt-body-right'},
//                        {targets: 15, className: 'dt-body-right'},
//                        {targets: 16, className: 'dt-body-right'}
                    ],
                    "ajax": "/TABLE2/ShowDataTables960?uid=" + sessionStorage.getItem('uid'),
                    "sAjaxDataProp": "",
                    "aoColumns": [
                        {"mDataProp": "user"},
                        {"mDataProp": "wh"},
                        {"mDataProp": "matCt"},
                        {"mDataProp": "plant"},
                        {"mDataProp": "rm"},
                        {"mDataProp": "matCo"},
                        {"mDataProp": "desc"},
                        {"mDataProp": "sts123"},
                        {"mDataProp": "sts1"},
                        {"mDataProp": "sts2"},
                        {"mDataProp": "sts3"},
                        {"mDataProp": "sts4"},
                        {"mDataProp": "stsP"}
                    ],
                    "processing": true,
                    oLanguage: {sProcessing: "<div id='loader'></div>"}
                });
                var arr = [];
//                document.getElementById("demo").innerHTML = arr.join(" * ");

                // Setup - add a text input to each footer cell
                $('#showTable tfoot th').each(function () {
                    var title = $(this).text();
                    $(this).html('<input type="text" />');
                });
                // DataTable
                var table = $('#showTable').DataTable();
                new $.fn.dataTable.FixedHeader(table);
                $('#bbb3').click(function () {

                    var tr = document.getElementsByTagName('tr');
                    for (var i = 0; i < tr.length; i++) {
                        if (tr[i].id.toString().substring(0, 3) === "DET") {
                            tr[i].style.display = "none";
                        }
                    }

                    arr = [];
//                    document.getElementById("demo").innerHTML = arr.join(" * ");
                });
                $('#showTable tbody').on('click', 'td', function () {
                    var row_clicked = $(this).closest('tr');
                    var data = table.row(row_clicked).data();
                    var idx = table.cell(this).index().column;
                    var title = table.column(idx).header();
                    if ($(title).html() === 'Plant') {

//                        row_clicked.addClass('selected');

                        var index = arr.indexOf(data[Object.keys(data)[1]]
                                + "-" + data[Object.keys(data)[3]]
                                + "-" + data[Object.keys(data)[5]]
                                + "-" + data[Object.keys(data)[7]]
                                + "-" + data[Object.keys(data)[9]]);
                        if (index > -1) {
                            arr.splice(index, 1);
//                            document.getElementById("demo").innerHTML = arr.join(" * ");
                            var tr = document.getElementsByTagName('tr');
                            for (var i = 0; i < tr.length; i++) {
                                if ($(title).html() === 'Plant') {
                                    if (tr[i].id.toString().split("-")[1] === data[Object.keys(data)[1]]
                                            && tr[i].id.toString().split("-")[2] === data[Object.keys(data)[3]]
                                            && tr[i].id.toString().split("-")[3] === data[Object.keys(data)[5]]
                                            && tr[i].id.toString().split("-")[4] === data[Object.keys(data)[7]]) {
                                        tr[i].style.display = "none";
                                    }
                                } else if ($(title).html() === 'RM Style') {
                                    if (tr[i].id.toString().split("-")[1] === data[Object.keys(data)[1]]
                                            && tr[i].id.toString().split("-")[2] === data[Object.keys(data)[3]]
                                            && tr[i].id.toString().split("-")[3] === data[Object.keys(data)[5]]
                                            && tr[i].id.toString().split("-")[4] === data[Object.keys(data)[7]]
                                            && tr[i].id.toString().split("-")[5] === data[Object.keys(data)[9]]) {
                                        tr[i].style.display = "none";
                                    }
                                }
                            }

                        } else {
                            if (data[Object.keys(data)[7]] !== 'TOTAL') {

//                                alert('*' + $(title).html() + '*-----*' + table.cell(this).data() + '*-----*' + data[Object.keys(data)[1]] + '*');

                                arr.push(data[Object.keys(data)[1]]
                                        + "-" + data[Object.keys(data)[3]]
                                        + "-" + data[Object.keys(data)[5]]
                                        + "-" + data[Object.keys(data)[7]]
                                        + "-" + data[Object.keys(data)[9]]);
//                                document.getElementById("demo").innerHTML = arr.join(" * ");

                                $.ajax({
                                    url: "/TABLE2/ShowDataTables960?mode=" + $(title).html() + "&user=" + data[Object.keys(data)[1]]
                                            + "&wh=" + data[Object.keys(data)[3]]
                                            + "&matct=" + data[Object.keys(data)[5]]
                                            + "&plant=" + data[Object.keys(data)[7]]
                                            + "&rm=" + data[Object.keys(data)[9]]
                                            + "&id=DET-" + data[Object.keys(data)[1]]
                                            + "-" + data[Object.keys(data)[3]]
                                            + "-" + data[Object.keys(data)[5]]
                                            + "-" + data[Object.keys(data)[7]]
                                            + "-" + data[Object.keys(data)[9]]
                                }).done(function (result) {
//                                    table.clear().draw();
                                    table.rows.add(result).draw();
                                    table.order([0, 'asc'], [1, 'asc'], [2, 'asc'], [3, 'asc'], [4, 'asc'], [5, 'asc']).draw();
                                }).fail(function (jqXHR, textStatus, errorThrown) {
                                    // needs to implement if it fails
                                });
                            }
                        }

                    }

                });
                // Apply the search
                table.columns().every(function () {
                    var that = this;
                    $('input', this.footer()).on('keyup change', function () {
                        if (that.search() !== this.value) {
                            that.search(this.value).draw();
                        }
                    });
                });
            }
            );
        </script>
        <script>
            function CD() {
                var cd = document.getElementById("ym").value;
                window.location.href = "/TABLE2/WMS960/display?CD=" + cd;
            }

            function subRow(qno, head) {
                var tr = document.getElementsByTagName('tr');
                for (var i = 0; i < tr.length; i++) {
                    if (tr[i].style.display === "") {
                        if (tr[i].id === qno) {
                            tr[i].style.display = "none";
                            document.getElementById(head).style.cssText = "cursor: pointer; background-color: #e5e5e5; color: black;";
                        }
                    } else {
                        if (tr[i].id === qno) {
                            tr[i].style.display = "";
                            document.getElementById(head).style.cssText = "cursor: pointer; background-color: #003682; color: white;";
                        }
                    }
                }
            }

            function expand() {
                document.getElementById("exp").style.display = "none";
                document.getElementById("com").style.display = "";
                var tr = document.getElementsByTagName('tr');
                for (var i = 0; i < tr.length; i++) {
                    if (tr[i].id.toString().split("-")[0] === "H") {
                        tr[i].style.cssText = "cursor: pointer; background-color: #003682; color: white;";
                    } else if (tr[i].id.toString().split("-")[0] === "D") {
                        tr[i].style.display = "";
                    }
                }
            }

            function compress() {
                document.getElementById("exp").style.display = "";
                document.getElementById("com").style.display = "none";
                var tr = document.getElementsByTagName('tr');
                for (var i = 0; i < tr.length; i++) {
                    if (tr[i].id.toString().split("-")[0] === "H") {
                        tr[i].style.cssText = "cursor: pointer; background-color: #e5e5e5; color: black;";
                    } else if (tr[i].id.toString().split("-")[0] === "D") {
                        tr[i].style.display = "none";
                    }
                }
            }

            function hide() {
                document.getElementById("hide").style.display = "none";
                document.getElementById("show").style.display = "";
                document.getElementById("frm").style.display = "none";
            }

            function show() {
                document.getElementById("hide").style.display = "";
                document.getElementById("show").style.display = "none";
                document.getElementById("frm").style.display = "";
            }
        </script>
        <style>
            .header {
                padding: 10px 16px;
                background: white;
                color: black;
                display: none;
            }

            .sticky {
                position: fixed;
                top: 0;
                width: 100%;
                display: block;

            }
        </style>
        <script>
            function colHid(id, col) {
                var table = $('#showTable').DataTable();
                if (document.getElementById(id).value === "0") {
                    table.column(col).visible(false);
                    document.getElementById(id).value = "1";
                    if (col === 2) {
                        table.column(3).visible(false);
                        document.getElementById("RM").value = "1";
                        table.column(4).visible(false);
                        document.getElementById("matCo").value = "1";
                        table.column(5).visible(false);
                        document.getElementById("desc").value = "1";
                    } else if (col === 3) {
                        table.column(4).visible(false);
                        document.getElementById("matCo").value = "1";
                        table.column(5).visible(false);
                        document.getElementById("desc").value = "1";
                    } else if (col === 4) {
                        table.column(5).visible(false);
                        document.getElementById("desc").value = "1";
                    }
                } else if (document.getElementById(id).value === "1") {
                    table.column(col).visible(true);
                    document.getElementById(id).value = "0";
                }

            }
        </script>
        <script>
            /*!
             FixedHeader 3.1.5
             ©2009-2018 SpryMedia Ltd - datatables.net/license
             */
            (function (d) {
                "function" === typeof define && define.amd ? define(["jquery", "datatables.net"], function (g) {
                    return d(g, window, document)
                }) : "object" === typeof exports ? module.exports = function (g, i) {
                    g || (g = window);
                    if (!i || !i.fn.dataTable)
                        i = require("datatables.net")(g, i).$;
                    return d(i, g, g.document)
                } : d(jQuery, window, document)
            })(function (d, g, i, k) {
                var j = d.fn.dataTable, l = 0, h = function (a, b) {
                    if (!(this instanceof h))
                        throw"FixedHeader must be initialised with the 'new' keyword.";
                    !0 === b && (b = {});
                    a = new j.Api(a);
                    this.c = d.extend(!0,
                            {}, h.defaults, b);
                    this.s = {dt: a, position: {theadTop: 0, tbodyTop: 0, tfootTop: 0, tfootBottom: 0, width: 0, left: 0, tfootHeight: 0, theadHeight: 0, windowHeight: d(g).height(), visible: !0}, headerMode: null, footerMode: null, autoWidth: a.settings()[0].oFeatures.bAutoWidth, namespace: ".dtfc" + l++, scrollLeft: {header: -1, footer: -1}, enable: !0};
                    this.dom = {floatingHeader: null, thead: d(a.table().header()), tbody: d(a.table().body()), tfoot: d(a.table().footer()), header: {host: null, floating: null, placeholder: null}, footer: {host: null, floating: null,
                            placeholder: null}};
                    this.dom.header.host = this.dom.thead.parent();
                    this.dom.footer.host = this.dom.tfoot.parent();
                    var e = a.settings()[0];
                    if (e._fixedHeader)
                        throw"FixedHeader already initialised on table " + e.nTable.id;
                    e._fixedHeader = this;
                    this._constructor()
                };
                d.extend(h.prototype, {enable: function (a) {
                        this.s.enable = a;
                        this.c.header && this._modeChange("in-place", "header", !0);
                        this.c.footer && this.dom.tfoot.length && this._modeChange("in-place", "footer", !0);
                        this.update()
                    }, headerOffset: function (a) {
                        a !== k && (this.c.headerOffset =
                                a, this.update());
                        return this.c.headerOffset
                    }, footerOffset: function (a) {
                        a !== k && (this.c.footerOffset = a, this.update());
                        return this.c.footerOffset
                    }, update: function () {
                        this._positions();
                        this._scroll(!0)
                    }, _constructor: function () {
                        var a = this, b = this.s.dt;
                        d(g).on("scroll" + this.s.namespace, function () {
                            a._scroll()
                        }).on("resize" + this.s.namespace, j.util.throttle(function () {
                            a.s.position.windowHeight = d(g).height();
                            a.update()
                        }, 50));
                        var e = d(".fh-fixedHeader");
                        !this.c.headerOffset && e.length && (this.c.headerOffset = e.outerHeight());
                        e = d(".fh-fixedFooter");
                        !this.c.footerOffset && e.length && (this.c.footerOffset = e.outerHeight());
                        b.on("column-reorder.dt.dtfc column-visibility.dt.dtfc draw.dt.dtfc column-sizing.dt.dtfc responsive-display.dt.dtfc", function () {
                            a.update()
                        });
                        b.on("destroy.dtfc", function () {
                            a.c.header && a._modeChange("in-place", "header", true);
                            a.c.footer && a.dom.tfoot.length && a._modeChange("in-place", "footer", true);
                            b.off(".dtfc");
                            d(g).off(a.s.namespace)
                        });
                        this._positions();
                        this._scroll()
                    }, _clone: function (a, b) {
                        var e = this.s.dt,
                                c = this.dom[a], f = "header" === a ? this.dom.thead : this.dom.tfoot;
                        !b && c.floating ? c.floating.removeClass("fixedHeader-floating fixedHeader-locked") : (c.floating && (c.placeholder.remove(), this._unsize(a), c.floating.children().detach(), c.floating.remove()), c.floating = d(e.table().node().cloneNode(!1)).css("table-layout", "fixed").attr("aria-hidden", "true").removeAttr("id").append(f).appendTo("body"), c.placeholder = f.clone(!1), c.placeholder.find("*[id]").removeAttr("id"), c.host.prepend(c.placeholder), this._matchWidths(c.placeholder,
                                c.floating))
                    }, _matchWidths: function (a, b) {
                        var e = function (b) {
                            return d(b, a).map(function () {
                                return d(this).width()
                            }).toArray()
                        }, c = function (a, c) {
                            d(a, b).each(function (a) {
                                d(this).css({width: c[a], minWidth: c[a]})
                            })
                        }, f = e("th"), e = e("td");
                        c("th", f);
                        c("td", e)
                    }, _unsize: function (a) {
                        var b = this.dom[a].floating;
                        b && ("footer" === a || "header" === a && !this.s.autoWidth) ? d("th, td", b).css({width: "", minWidth: ""}) : b && "header" === a && d("th, td", b).css("min-width", "")
                    }, _horizontal: function (a, b) {
                        var e = this.dom[a], c = this.s.position,
                                d = this.s.scrollLeft;
                        e.floating && d[a] !== b && (e.floating.css("left", c.left - b), d[a] = b)
                    }, _modeChange: function (a, b, e) {
                        var c = this.dom[b], f = this.s.position, g = this.dom["footer" === b ? "tfoot" : "thead"], h = d.contains(g[0], i.activeElement) ? i.activeElement : null;
                        h && h.blur();
                        if ("in-place" === a) {
                            if (c.placeholder && (c.placeholder.remove(), c.placeholder = null), this._unsize(b), "header" === b ? c.host.prepend(g) : c.host.append(g), c.floating)
                                c.floating.remove(), c.floating = null
                        } else
                            "in" === a ? (this._clone(b, e), c.floating.addClass("fixedHeader-floating").css("header" ===
                                    b ? "top" : "bottom", this.c[b + "Offset"]).css("left", f.left + "px").css("width", f.width + "px"), "footer" === b && c.floating.css("top", "")) : "below" === a ? (this._clone(b, e), c.floating.addClass("fixedHeader-locked").css("top", f.tfootTop - f.theadHeight).css("left", f.left + "px").css("width", f.width + "px")) : "above" === a && (this._clone(b, e), c.floating.addClass("fixedHeader-locked").css("top", f.tbodyTop).css("left", f.left + "px").css("width", f.width + "px"));
                        h && h !== i.activeElement && setTimeout(function () {
                            h.focus()
                        }, 10);
                        this.s.scrollLeft.header =
                                -1;
                        this.s.scrollLeft.footer = -1;
                        this.s[b + "Mode"] = a
                    }, _positions: function () {
                        var a = this.s.dt.table(), b = this.s.position, e = this.dom, a = d(a.node()), c = a.children("thead"), f = a.children("tfoot"), e = e.tbody;
                        b.visible = a.is(":visible");
                        b.width = a.outerWidth();
                        b.left = a.offset().left;
                        b.theadTop = c.offset().top;
                        b.tbodyTop = e.offset().top;
                        b.theadHeight = b.tbodyTop - b.theadTop;
                        f.length ? (b.tfootTop = f.offset().top, b.tfootBottom = b.tfootTop + f.outerHeight(), b.tfootHeight = b.tfootBottom - b.tfootTop) : (b.tfootTop = b.tbodyTop + e.outerHeight(),
                                b.tfootBottom = b.tfootTop, b.tfootHeight = b.tfootTop)
                    }, _scroll: function (a) {
                        var b = d(i).scrollTop(), e = d(i).scrollLeft(), c = this.s.position, f;
                        if (this.s.enable && (this.c.header && (f = !c.visible || b <= c.theadTop - this.c.headerOffset ? "in-place" : b <= c.tfootTop - c.theadHeight - this.c.headerOffset ? "in" : "below", (a || f !== this.s.headerMode) && this._modeChange(f, "header", a), this._horizontal("header", e)), this.c.footer && this.dom.tfoot.length))
                            b = !c.visible || b + c.windowHeight >= c.tfootBottom + this.c.footerOffset ? "in-place" : c.windowHeight +
                                    b > c.tbodyTop + c.tfootHeight + this.c.footerOffset ? "in" : "above", (a || b !== this.s.footerMode) && this._modeChange(b, "footer", a), this._horizontal("footer", e)
                    }});
                h.version = "3.1.5";
                h.defaults = {header: !0, footer: !1, headerOffset: 0, footerOffset: 0};
                d.fn.dataTable.FixedHeader = h;
                d.fn.DataTable.FixedHeader = h;
                d(i).on("init.dt.dtfh", function (a, b) {
                    if ("dt" === a.namespace) {
                        var e = b.oInit.fixedHeader, c = j.defaults.fixedHeader;
                        if ((e || c) && !b._fixedHeader)
                            c = d.extend({}, c, e), !1 !== e && new h(b, c)
                    }
                });
                j.Api.register("fixedHeader()", function () {});
                j.Api.register("fixedHeader.adjust()", function () {
                    return this.iterator("table", function (a) {
                        (a = a._fixedHeader) && a.update()
                    })
                });
                j.Api.register("fixedHeader.enable()", function (a) {
                    return this.iterator("table", function (b) {
                        b = b._fixedHeader;
                        a = a !== k ? a : !0;
                        b && a !== b.s.enable && b.enable(a)
                    })
                });
                j.Api.register("fixedHeader.disable()", function () {
                    return this.iterator("table", function (a) {
                        (a = a._fixedHeader) && a.s.enable && a.enable(!1)
                    })
                });
                d.each(["header", "footer"], function (a, b) {
                    j.Api.register("fixedHeader." + b + "Offset()",
                            function (a) {
                                var c = this.context;
                                return a === k ? c.length && c[0]._fixedHeader ? c[0]._fixedHeader[b + "Offset"]() : k : this.iterator("table", function (c) {
                                    if (c = c._fixedHeader)
                                        c[b + "Offset"](a)
                                })
                            })
                });
                return h
            });
        </script>
        <style>
            table.fixedHeader-floating{position:fixed !important;background-color:white}table.fixedHeader-floating.no-footer{border-bottom-width:0}table.fixedHeader-locked{position:absolute !important;background-color:white}@media print{table.fixedHeader-floating{display:none}}
        </style>
        <style>
            #loader {
                border: 16px solid #f3f3f3;
                border-radius: 100%;
                border-top: 16px solid #3498db;
                width: 240px;
                height: 240px;
                -webkit-animation: spin 2s linear infinite;
                animation: spin 2s linear infinite;
                margin: auto;
                /*margin-left:1200px;*/
                margin-top:250px;
            }   

            .panel-default {
                border-color: transparent;
                background-color: transparent;
            }

            @-webkit-keyframes spin {
                0% { -webkit-transform: rotate(0deg); }
                100% { -webkit-transform: rotate(360deg); }
            }

            @keyframes spin {
                0% { transform: rotate(0deg); }
                100% { transform: rotate(360deg); }
            }

        </style>
        <style>
            body {font-family: Arial, Helvetica, sans-serif;}

            /* The Modal (background) */
            .modal {
                display: none; /* Hidden by default */
                position: fixed; /* Stay in place */
                z-index: 1; /* Sit on top */
                padding-top: 100px; /* Location of the box */
                left: 0;
                top: 0;
                width: 100%; /* Full width */
                height: 100%; /* Full height */
                overflow: auto; /* Enable scroll if needed */
                background-color: rgb(0,0,0); /* Fallback color */
                background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
            }

            /* Modal Content */
            .modal-content {
                background-color: #fefefe;
                margin: auto;
                padding: 20px;
                border: 1px solid #888;
                width: fit-content;
                height: 320px;
            }

            /* The Close Button */
            .close {
                color: #aaaaaa;
                float: right;
                font-size: 28px;
                font-weight: bold;
            }

            .close:hover,
            .close:focus {
                color: #000;
                text-decoration: none;
                cursor: pointer;
            }
        </style>

    </head>    
    <body onload="document.getElementById('userid').value = sessionStorage.getItem('uid');">
        <div id="wrapper">

            <!-- /#sidebar-wrapper -->
            <%@ include file="../fragments/sidebar.jsp" %>

            <!-- Page Content -->
            <!-- nav-head-custom -->
            <%@ include file="../fragments/nav_head.jsp" %>

            <div class="container-fluid">
                <!--<div id="wrapper-top">-->
                <%-- PART 2 --%>
                <!--                    <div class="row">
                                        <div class="col-lg-12">
                                            <div id="set-height" style="height:415px;margin-top:0px;">
                                                <div id="sidebar-wrapper-top" class="">
                                                    <b class="page-header" style="padding-left:5px;font-size:18px;">
                <%-- <hr style="margin:0px;margin-top:10px;margin-bottom:10px;"> --%>
            </b>
        </div>
    </div>
    </div>
    </div> -->
                <%-- PART 3 --%>
                <!--show Qdest table-->
                <form action="../resources/manual/WMS960.pdf" target="_blank">
                    <table width="100%">
                        <tr id="dont">
                            <td width="94%" align="left"></td>
                            <td width="6%" align="right"><input type="submit" value="Manual" /></td>
                        </tr>
                    </table>
                </form>
                <input id="matCT" type="hidden" value="1">
                <input id="RM" type="hidden" value="1">
                <input id="matCo" type="hidden" value="1">
                <input id="desc" type="hidden" value="1">
                <input id="userid" type="hidden">
                <div style="border: 2px solid #ccc; border-radius: 5px; padding: 20px;">
                    <!--<p id="demo"></p>-->
                    <table style="width:100%;">
                        <tr style="width:100%;">
                            <td style="width:50%;">
                                <button id="bbb3" style=" width: 200px;">Collapse All</button>
                            </td>
                            <td style="width:50%; text-align: right;">
                                <a title="Print" onclick="document.getElementById('myModal-3').style.display = 'block';" style="cursor: pointer;"><i class="fa fa-print" aria-hidden="true" style=" font-size: 40px; color: #001384;"></i></a>
                                <div id="myModal-3" class="modal">
                                    <!-- Modal content -->
                                    <div class="modal-content" style=" height: 360px; width: 600px;">
                                        <span id="span-3" class="close" onclick="document.getElementById('myModal-3').style.display = 'none';">&times;</span>
                                        <p><b><font size="4"></font></b></p>
                                        <table width="100%">
                                            <tr style="background-color: white;">
                                                <td align="center">
                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                    <b style="color: #00399b;">
                                                        <font size="5">Summary Onhand Report</font><hr>

                                                        <table width="100%">
                                                            <tr>
                                                                <td width="10%"></td>
                                                                <td width="30%"></td>
                                                                <td width="30%">From</td>
                                                                <td width="30%">To</td>
                                                            </tr>
                                                            <tr>
                                                                <td></td>
                                                                <td><font size="4">Mat Ctrl : </font></td>
                                                                <td>
                                                                    <select id="mc" style="height: 35px; width: 200px;">
                                                                        <c:forEach items="${UAList}" var="x">
                                                                            <option value="${x.uid}">${x.uid} : ${x.name}</option>
                                                                        </c:forEach>
                                                                    </select>
                                                                </td>
                                                                <td></td>
                                                            </tr><tr>
                                                                <td></td>
                                                                <td><font size="4">Plant : </font></td>
                                                                <td>
                                                                    <select id="plantF" style="height: 35px; width: 100px;">
                                                                        <option value="1000">1000</option>
                                                                        <option value="1050">1050</option>
                                                                    </select>
                                                                </td>
                                                                <td>
                                                                    <select id="plantT" style="height: 35px; width: 100px;">
                                                                        <option value="1000">1000</option>
                                                                        <option value="1050">1050</option>
                                                                    </select>
                                                                </td>
                                                                <td></td>
                                                            </tr>
                                                        </table>
                                                    </b>
                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                    <br><br>
                                                    <a class="btn btn btn-outline btn-success" onclick="window.location.href = 'print?matct=' + document.getElementById('mc').value + '&plantF=' + document.getElementById('plantF').value + '&plantT=' + document.getElementById('plantT').value + '&uid=' + document.getElementById('userid').value;">
                                                        <i class="fa fa-print" style="font-size:20px;"></i> Print</a>                    
                                                </td>
                                            </tr>
                                        </table>
                                    </div>

                                </div>
                            </td>
                        </tr>
                    </table>
                    <table id="showTable" class="display" style="width:100%; border: 1px solid #ccc; border-radius: 4px;">
                        <thead>
                            <tr id="dont">
                                <th style="width: 200px;">User</th>
                                <th style="width: 200px;">Warehouse</th>
                                <th style="width: 180px;">Mat Ctrl</th>
                                <th style="width: 180px;">Plant</th>
                                <th style="width: 180px;">RM Style</th>
                                <th style="width: 180px;">Mat Code</th>
                                <th style="width: 180px;">Description</th>
                                <th style="width: 150px; background-color: #91c2ff;">MC (1+2+3+4)<br>Responsibility</th>
                                <th style="width: 150px;">1:<br>Received</th>
                                <th style="width: 150px;">2: Complete</th>
                                <th style="width: 150px;">3:<br>Issue</th>
                                <th style="width: 150px;">4:<br>Prepare to send</th>
                                <th style="width: 150px;">P:<br>Pending</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr id="dont">
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th style="background-color: #91c2ff;"></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                            </tr>
                        </tfoot>
                        <tbody>
                            <%--<c:forEach items="${WHList}" var="x">--%>
                            <!--                                <tr>
                                                                <td>${x.wh}</td>
                                                                <td>${x.plant}</td>
                                                                <td>${x.matCt}</td>
                                                                <td>${x.rm}</td>
                                                                <td>${x.matCo}</td>
                                                                <td>${x.desc}</td>
                                                                <td style="text-align: right;">${x.sts123}</td>
                                                                <td style="text-align: right;">${x.sts456}</td>
                                                                <td style="text-align: right;">${x.sts1}</td>
                                                                <td style="text-align: right;">${x.sts2}</td>
                                                                <td style="text-align: right;">${x.sts3}</td>
                                                                <td style="text-align: right;">${x.sts4}</td>
                                                                <td style="text-align: right;">${x.sts5}</td>
                                                                <td style="text-align: right;">${x.sts6}</td>
                                                                <td style="text-align: right;">${x.sts7}</td>
                                                                <td style="text-align: right;">${x.stsO}</td>
                                                                <td style="text-align: right;">${x.stsP}</td>
                                                            </tr>                          -->
                            <%--</c:forEach>--%>
                        </tbody>
                    </table>
                    <!--**********************-->
                    <!--End Part 3-->
                    <!--</div>  end #wrapper-top--> 
                </div> 
                <br>
            </div> <!-- end .container-fluid -->
            <!--            <div class="header" id="myHeader">
                            <table style="width:100%; border-bottom: 1px solid black;">
                                <tr id="dont">
                                    <th style="width: 200px;">Warehouse</th>
                                    <th style="width: 180px;">Plant</th>
                                    <th style="width: 180px;">Mat Ctrl</th>
                                    <th style="width: 180px;">RM Style</th>
                                    <th style="width: 180px;">Mat Code</th>
                                    <th style="width: 180px;">Description</th>
                                    <th style="width: 150px;">MC<br>Responsibility</th>
                                    <th style="width: 150px;">LG<br>Responsibility</th>
                                    <th style="width: 150px;">1:<br>Received</th>
                                    <th style="width: 150px;">2: Complete</th>
                                    <th style="width: 150px;">3:<br>Issue</th>
                                    <th style="width: 150px;">4:<br>Prepare<br>to send</th>
                                    <th style="width: 150px;">5: LG<br>Receipt</th>
                                    <th style="width: 150px;">6:<br>Transported</th>
                                    <th style="width: 150px;">7:<br>Delivery</th>
                                    <th style="width: 150px;">O: Out<br>of stock</th>
                                    <th style="width: 150px;">P:<br>Pending</th>
                                </tr>
                            </table>
                        </div>-->
        </div> <!-- end #wrapper -->
        <script>
            window.onscroll = function () {
                myFunction();
            };
            var header = document.getElementById("myHeader");
            var sticky = header.offsetTop;
            function myFunction() {
                if (window.pageYOffset > sticky) {
                    header.classList.add("sticky");
                } else {
                    header.classList.remove("sticky");
                }
            }
        </script>
    </body>
</html>