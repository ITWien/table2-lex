<%@ include file="../fragments/taglibs.jsp" %>
<%@ include file="../fragments/imports.jsp" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.sql.*;" %>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>WMS</title>
    <!-- css :: vendors -->
    <jsp:include page="../fragments/css.jsp" />
    <!-- additional custom :: my-style -->
    <link rel="stylesheet" href="../resources/assets/styles/myStyles.css">

    <!-- js :: vendors -->
    <jsp:include page="../fragments/script.jsp" />
    <!-- additional custom :: my-script -->
    <script src="../resources/assets/scripts/myScripts.js" async></script>
    <script src="../resources/assets/scripts/toggleLoad.js" async></script>
    <script src="https://www.chartjs.org/dist/2.9.3/Chart.min.js"></script>
    <script src="https://www.chartjs.org/samples/latest/utils.js"></script>
    ${sendMessage}
    <style>
        input[type=text],
        select {
            width: 100%;
            padding: 5px 5px;
            margin: 8px 0;
            display: inline-block;
            border: 1px solid #ccc;
            border-radius: 4px;
            box-sizing: border-box;
        }

        input[type=date],
        input[type=number] {
            /*width: 100%;*/
            padding: 5px 5px;
            margin: 8px 0;
            display: inline-block;
            border: 1px solid #ccc;
            border-radius: 4px;
            box-sizing: border-box;
        }

        input[type=submit] {
            width: 100%;
            background-color: #00399b;
            color: white;
            padding: 5px 5px;
            margin: 8px 0;
            border: none;
            border-radius: 4px;
            cursor: pointer;
        }

        input[type=submit]:hover {
            background-color: #3973d6;
        }

        input[type=button] {
            width: 100%;
            background-color: #ef604a;
            color: white;
            padding: 5px 5px;
            margin: 8px 0;
            border: none;
            border-radius: 4px;
            cursor: pointer;
        }

        input[type=button]:hover {
            background-color: #e51e00;
        }
    </style>
    <style>
        button[name=ok] {
            width: 25%;
            background-color: #008cff;
            color: white;
            padding: 5px 5px;
            margin: 8px 0;
            border: none;
            border-radius: 4px;
            cursor: pointer;
        }

        button[name=ok]:hover {
            background-color: #008cff;
        }
    </style>
    <script>
        $(document).ready(function () {
            $('#showTableWMS160').DataTable({
                "paging": false,
                "lengthMenu": [[10, 25, 50, 100], [10, 25, 50, 100]],
                "bSortClasses": false,
                "order": [[1, "asc"]],
                "columnDefs": [
                    { orderable: false, targets: [9] },
                    //                        {"width": "15%", "targets": 7},
                    //                        {"width": "15%", "targets": 8},
                    { targets: 0, className: 'dt-body-right' },
                    { targets: 1, className: 'dt-body-center' },
                    { targets: 3, className: 'dt-body-right' },
                    { targets: 4, className: 'dt-body-right' },
                    { targets: 5, className: 'dt-body-right' },
                    { targets: 6, className: 'dt-body-right' }
                ],
                "ajax": "/TABLE2/ShowDataTablesWMS160?mode=display",
                "sAjaxDataProp": "",
                "aoColumns": [
                    { "mDataProp": "checkBax" },
                    { "mDataProp": "QDMSIZE" },
                    { "mDataProp": "QDMDESC" },
                    { "mDataProp": "QDMCMWIDTH" },
                    { "mDataProp": "QDMCMLENGTH" },
                    { "mDataProp": "QDMINWIDTH" },
                    { "mDataProp": "QDMINLENGTH" },
                    { "mDataProp": "QDMUSER" },
                    { "mDataProp": "QDMCDT" },
                    { "mDataProp": "option" }
                ],
                "processing": true
            });
            $('#showTableWMS165').DataTable({
                "paging": false,
                "lengthMenu": [[10, 25, 50, 100], [10, 25, 50, 100]],
                "bSortClasses": false,
                "order": [[0, "dsc"]],
                "columnDefs": [
                    { orderable: false, targets: [3] },
                    //                        {"width": "15%", "targets": 7},
                    //                        {"width": "15%", "targets": 8},
                    { targets: 0, className: 'dt-body-center' },
                    { targets: 1, className: 'dt-body-right' },
                    { targets: 2, className: 'dt-body-center' },
                    //                        {targets: 4, className: 'dt-body-right'},
                    //                        {targets: 5, className: 'dt-body-right'},
                    //                        {targets: 6, className: 'dt-body-right'}
                ],
                "ajax": "/TABLE2/ShowDataTablesWMS165?mode=getDet&style=",
                "sAjaxDataProp": "",
                "aoColumns": [
                    { "mDataProp": "QDMBLINO" },
                    { "mDataProp": "QDMBQTY" },
                    { "mDataProp": "QDMBHEIGHT" },
                    { "mDataProp": "option" }
                ],
                "processing": true
            });
            $('#showTableWMS170').DataTable({
                "paging": false,
                "lengthMenu": [[10, 25, 50, 100], [10, 25, 50, 100]],
                "bSortClasses": false,
                "order": [[0, "dsc"]],
                "columnDefs": [
                    { orderable: false, targets: [4] },
                    //                        {"width": "15%", "targets": 7},
                    //                        {"width": "15%", "targets": 8},
                    { targets: 0, className: 'dt-body-center' },
                    { targets: 1, className: 'dt-body-right' },
                    { targets: 2, className: 'dt-body-right' },
                    { targets: 3, className: 'dt-body-center' },
                    //                        {targets: 4, className: 'dt-body-right'},
                    //                        {targets: 5, className: 'dt-body-right'},
                    //                        {targets: 6, className: 'dt-body-right'}
                ],
                "ajax": "/TABLE2/ShowDataTablesWMS170?mode=getDet&style=",
                "sAjaxDataProp": "",
                "aoColumns": [
                    { "mDataProp": "QDMRLINO" },
                    { "mDataProp": "QDMRLENGTH1" },
                    { "mDataProp": "QDMRLENGTH2" },
                    { "mDataProp": "QDMRHEIGHT" },
                    { "mDataProp": "option" }
                ],
                "processing": true
            });
        });
        var xTimeBAGMAS = setInterval(function () {
            document.getElementById('timeBAGMAS').innerHTML = document.getElementById('time').innerHTML;
            document.getElementById('timeBAGCAL').innerHTML = document.getElementById('time').innerHTML;
            document.getElementById('timeROLLCAL').innerHTML = document.getElementById('time').innerHTML;
        }, 500);
        function addBag() {
            var size = document.getElementById('add-size').value;
            var desc = document.getElementById('add-desc').value;
            var inWidth = document.getElementById('add-inWidth').value;
            var inLength = document.getElementById('add-inLength').value;
            var uid = sessionStorage.getItem('uid');
            if (size !== "" && desc !== "" && inWidth !== "" && inLength !== "") {
                $.ajax({
                    url: "/TABLE2/ShowDataTablesWMS160?mode=add"
                        + "&size=" + size
                        + "&desc=" + desc
                        + "&inWidth=" + inWidth
                        + "&inLength=" + inLength
                        + "&uid=" + uid
                });
                document.getElementById('add-size').value = "";
                document.getElementById('add-desc').value = "";
                document.getElementById('add-inWidth').value = "";
                document.getElementById('add-inLength').value = "";
                $.ajax({
                    url: "/TABLE2/ShowDataTablesWMS160?mode=display"
                }).done(function (result) {
                    ReTable(result);
                    ReTable(result);
                }).fail(function (jqXHR, textStatus, errorThrown) {
                    // needs to implement if it fails
                });
            }
        }

        function editBag(size) {
            var desc = document.getElementById('edit-desc-' + size).value;
            var inWidth = document.getElementById('edit-inWidth-' + size).value;
            var inLength = document.getElementById('edit-inLength-' + size).value;
            var uid = sessionStorage.getItem('uid');
            if (size !== "" && desc !== "" && inWidth !== "" && inLength !== "") {
                $.ajax({
                    url: "/TABLE2/ShowDataTablesWMS160?mode=edit"
                        + "&size=" + size
                        + "&desc=" + desc
                        + "&inWidth=" + inWidth
                        + "&inLength=" + inLength
                        + "&uid=" + uid
                });
                $.ajax({
                    url: "/TABLE2/ShowDataTablesWMS160?mode=display"
                }).done(function (result) {
                    ReTable(result);
                    ReTable(result);
                }).fail(function (jqXHR, textStatus, errorThrown) {
                    // needs to implement if it fails
                });
            }

        }

        function deleteBag(size) {
            $.ajax({
                url: "/TABLE2/ShowDataTablesWMS160?mode=delete&size=" + size
            });
            $.ajax({
                url: "/TABLE2/ShowDataTablesWMS160?mode=display"
            }).done(function (result) {
                ReTable(result);
                ReTable(result);
            }).fail(function (jqXHR, textStatus, errorThrown) {
                // needs to implement if it fails
            });
        }

        function ReTable(result) {
            var table = $('#showTableWMS160').DataTable();
            table.clear().draw();
            table.rows.add(result).draw();
            table.order([1, 'asc']).draw();
        }

        function RefreshTable() {
            var element = document.getElementById("refreshIcon");
            element.classList.add("fa-spin");
            $.ajax({
                url: "/TABLE2/ShowDataTablesWMS160?mode=display"
            }).done(function (result) {
                ReTable(result);
                ReTable(result);
                setTimeout(function () {
                    element.classList.remove("fa-spin");
                }, 1000);
            }).fail(function (jqXHR, textStatus, errorThrown) {
                // needs to implement if it fails
            });
        }

        function confirmBag() {
            var sizeList = document.getElementsByName('size');
            for (var i = 0; i < sizeList.length; i++) {
                if (sizeList[i].checked === true) {
                    document.getElementById('D_WIDTH').value = document.getElementById('WIDTH-' + sizeList[i].value).value;
                    document.getElementById('D_LENGTH').value = document.getElementById('LENGTH-' + sizeList[i].value).value;
                }
            }
        }
        //----------------------------------------------------------------------------------------
        function generateData() {
            var data = [];
            var input = document.getElementsByTagName('input');
            var x = [];
            var y = [];
            for (var i = 0; i < input.length; i++) {
                if (input[i].name === "detQty") {
                    x.push(parseFloat(input[i].value));
                } else if (input[i].name === "detHeight") {
                    y.push(parseFloat(input[i].value));
                }
            }

            for (var i = 0; i < x.length; i++) {
                data.push({
                    x: x[i],
                    y: y[i]
                });
            }

            return data;
        }

        function generateData170() {
            var data = [];
            var input = document.getElementsByTagName('input');
            var x = [];
            var y = [];
            for (var i = 0; i < input.length; i++) {
                if (input[i].name === "detLength2") {
                    x.push(parseFloat(input[i].value));
                } else if (input[i].name === "detHeightRoll") {
                    y.push(parseFloat(input[i].value));
                }
            }

            for (var i = 0; i < x.length; i++) {
                data.push({
                    x: x[i],
                    y: y[i]
                });
            }

            return data;
        }

        function GenChart() {
            var scatterChartData = {
                datasets: [{
                    label: 'ปริมาณ , ความสูง(CM)',
                    borderColor: '#fc7777',
                    backgroundColor: '#fc7777',
                    pointRadius: 5,
                    pointHoverRadius: 10,
                    data: generateData(),
                    showLine: true,
                    fill: false
                }]
            };
            document.getElementById('canvas').innerHTML = '';
            var ctx = document.getElementById('canvas').getContext('2d');
            var myScatter = new Chart.Scatter(ctx, {
                data: scatterChartData,
                options: {
                    title: {
                        display: false,
                        text: 'Chart'
                    },
                    scales: {
                        xAxes: [{
                            type: 'linear',
                            scaleLabel: {
                                display: true,
                                labelString: 'ปริมาณ'
                            },
                            ticks: {
                                beginAtZero: true
                            }
                        }],
                        yAxes: [{
                            type: 'linear',
                            scaleLabel: {
                                display: true,
                                labelString: 'ความสูง (CM)'
                            },
                            ticks: {
                                beginAtZero: true
                            }
                        }]
                    }
                }
            });
        }

        function GenChart170() {
            var scatterChartData = {
                datasets: [{
                    label: 'ความยาว(CM) , ความหนา(CM)',
                    borderColor: '#fc7777',
                    backgroundColor: '#fc7777',
                    pointRadius: 5,
                    pointHoverRadius: 10,
                    data: generateData170(),
                    showLine: true,
                    fill: false
                }]
            };
            document.getElementById('canvas170').innerHTML = '';
            var ctx = document.getElementById('canvas170').getContext('2d');
            var myScatter170 = new Chart.Scatter(ctx, {
                data: scatterChartData,
                options: {
                    title: {
                        display: false,
                        text: 'Chart'
                    },
                    scales: {
                        xAxes: [{
                            type: 'linear',
                            scaleLabel: {
                                display: true,
                                labelString: 'ความยาว (CM)'
                            },
                            ticks: {
                                beginAtZero: true
                            }
                        }],
                        yAxes: [{
                            type: 'linear',
                            scaleLabel: {
                                display: true,
                                labelString: 'ความหนา (CM)'
                            },
                            ticks: {
                                beginAtZero: true
                            }
                        }]
                    }
                }
            });
        }

        function GetDataWMS165() {
            var styl = document.getElementById("styl").value;
            $.ajax({
                url: "/TABLE2/ShowDataTablesWMS165?mode=getHed&style=" + styl
            }).done(function (result) {

                document.getElementById("qrmcode").value = result.QDMBSTYLE === undefined ? '' : result.QDMBSTYLE;
                document.getElementById("qrmdesc").value = result.QDMBDESC === undefined ? '' : result.QDMBDESC;
                document.getElementById("qrmbun").value = result.QDMBUNIT === undefined ? '' : result.QDMBUNIT;
                document.getElementById("equation").value = result.QDMBEQUA === undefined ? '' : result.QDMBEQUA;
                //                    document.getElementById("valueX").value = document.getElementById("qty").value;
                document.getElementById("valueY").value = result.QDMBY === undefined ? '' : result.QDMBY;
            }).fail(function (jqXHR, textStatus, errorThrown) {
                // needs to implement if it fails
            });
            $.ajax({
                url: "/TABLE2/ShowDataTablesWMS165?mode=getDet&style=" + styl
            }).done(function (result) {
                var table = $('#showTableWMS165').DataTable();
                table.clear().draw();
                table.rows.add(result).draw();
                table.order([0, 'dsc']).draw();
            }).fail(function (jqXHR, textStatus, errorThrown) {
                // needs to implement if it fails
            });
            setTimeout(function () {
                GenChart();
            }, 500);
        }

        function GetDataWMS170() {
            var styl = document.getElementById("styl").value;
            $.ajax({
                url: "/TABLE2/ShowDataTablesWMS170?mode=getHed&style=" + styl
            }).done(function (result) {

                document.getElementById("qrmcodeRoll").value = result.QDMRSTYLE === undefined ? '' : result.QDMRSTYLE;
                document.getElementById("qrmdescRoll").value = result.QDMRDESC === undefined ? '' : result.QDMRDESC;
                document.getElementById("qrmbunRoll").value = result.QDMRUNIT === undefined ? '' : result.QDMRUNIT;
                document.getElementById("equationRoll").value = result.QDMREQUA === undefined ? '' : result.QDMREQUA;
                //                    document.getElementById("valueXRoll").value = document.getElementById("qty").value;
                document.getElementById("valueYRoll").value = result.QDMRY === undefined ? '' : result.QDMRY;
            }).fail(function (jqXHR, textStatus, errorThrown) {
                // needs to implement if it fails
            });
            $.ajax({
                url: "/TABLE2/ShowDataTablesWMS170?mode=getDet&style=" + styl
            }).done(function (result) {
                var table = $('#showTableWMS170').DataTable();
                table.clear().draw();
                table.rows.add(result).draw();
                table.order([0, 'dsc']).draw();
            }).fail(function (jqXHR, textStatus, errorThrown) {
                // needs to implement if it fails
            });
            setTimeout(function () {
                GenChart170();
            }, 500);
        }


        function confirmBagDet() {
            document.getElementById("D_HEIGHT").value = document.getElementById("valueY").value;
        }

        function confirmRollDet() {
            document.getElementById("D_DIAMETER").value = document.getElementById("valueYRoll").value;
        }

        function EquaRes() {
            var qrmcode = document.getElementById("qrmcode").value;
            var qrmdesc = document.getElementById("qrmdesc").value;
            var qrmbun = document.getElementById("qrmbun").value;
            var errCount = 0;
            $.ajax({
                url: "/TABLE2/ShowDataTablesWMS165?mode=getDet&style=" + qrmcode
            }).done(function (result) {
                if (result.length > 1) {
                    $.ajax({
                        url: "/TABLE2/ShowDataTablesWMS165?mode=checkHed&style=" + qrmcode
                    }).done(function (result) {
                        if (result === 'f') {

                            var input = document.getElementsByTagName('input');
                            var x = [];
                            var y = [];
                            for (var i = 0; i < input.length; i++) {
                                if (input[i].name === "detQty") {
                                    x.push(parseFloat(input[i].value));
                                } else if (input[i].name === "detHeight") {
                                    y.push(parseFloat(input[i].value));
                                }
                            }

                            var m = slope(x, y);
                            var b = intercept(x, y);
                            var x = parseFloat(document.getElementById("valueX").value);
                            var y = (m * x) + b;
                            var equa = 'y = ' + m.toFixed(4) + 'x [plus] ' + b.toFixed(4);
                            if (!isNaN(y)) {

                                var uid = sessionStorage.getItem('uid');
                                if (uid === null) {
                                    uid = '';
                                }

                                var equaResIcon = document.getElementById("equaResIcon");
                                equaResIcon.classList.remove("hidden");
                                equaResIcon.classList.add("fa-spin");
                                $.ajax({
                                    url: "/TABLE2/ShowDataTablesWMS165?mode=updateBagHed"
                                        + "&code=" + qrmcode
                                        + "&desc=" + qrmdesc
                                        + "&unit=" + qrmbun
                                        + "&equa=" + equa
                                        + "&x=" + x
                                        + "&y=" + y.toFixed(4)
                                        + "&uid=" + uid
                                }).done(function (result) {
                                    if (result) {

                                        for (var i = 0; i < input.length; i++) {
                                            if (input[i].name === "detHeight") {
                                                $.ajax({
                                                    url: "/TABLE2/ShowDataTablesWMS165?mode=updateBagDet"
                                                        + "&code=" + qrmcode
                                                        + "&line=" + input[i].id.toString().split('-')[0]
                                                        + "&qty=" + $('#' + input[i].id.toString().split('-')[0] + '-detQty').val()
                                                        + "&height=" + parseFloat(input[i].value)
                                                        + "&uid=" + uid
                                                }).done(function (result) {
                                                    //                                                alert(result);
                                                }).fail(function (jqXHR, textStatus, errorThrown) {
                                                    // needs to implement if it fails
                                                });
                                            }
                                        }

                                        GetDataWMS165();
                                        setTimeout(function () {
                                            equaResIcon.classList.remove("fa-spin");
                                            equaResIcon.classList.add("hidden");
                                        }, 1000);
                                    } else {
                                        document.getElementById("detailNE").innerHTML = "Update Failed !\n";
                                        errCount++;
                                    }

                                }).fail(function (jqXHR, textStatus, errorThrown) {
                                    // needs to implement if it fails
                                });
                            } else {
                                document.getElementById("detailNE").innerHTML = "Invalid format (not a number) !\n";
                                errCount++;
                            }

                        } else if (result === 't') {
                            document.getElementById("detailNE").innerHTML = "Style not Exist !\n";
                            errCount++;
                        }

                    }).fail(function (jqXHR, textStatus, errorThrown) {
                        // needs to implement if it fails
                    });
                } else {
                    document.getElementById("detailNE").innerHTML = "Bag Detail must more than 1 detail !\n";
                    errCount++;
                }
            }).fail(function (jqXHR, textStatus, errorThrown) {
                // needs to implement if it fails
            });
            if (errCount === 0) {
                document.getElementById("styleNE").innerHTML = "";
                document.getElementById("detailNE").innerHTML = "";
            }

        }

        function EquaResRoll() {
            var qrmcode = document.getElementById("qrmcodeRoll").value;
            var qrmdesc = document.getElementById("qrmdescRoll").value;
            var qrmbun = document.getElementById("qrmbunRoll").value;
            var errCount = 0;
            $.ajax({
                url: "/TABLE2/ShowDataTablesWMS170?mode=getDet&style=" + qrmcode
            }).done(function (result) {
                if (result.length > 1) {
                    $.ajax({
                        url: "/TABLE2/ShowDataTablesWMS170?mode=checkHed&style=" + qrmcode
                    }).done(function (result) {
                        if (result === 'f') {

                            var input = document.getElementsByTagName('input');
                            var x = [];
                            var y = [];
                            for (var i = 0; i < input.length; i++) {
                                if (input[i].name === "detLength2") {
                                    x.push(parseFloat(input[i].value));
                                } else if (input[i].name === "detHeightRoll") {
                                    y.push(parseFloat(input[i].value));
                                }
                            }

                            var m = slope(x, y);
                            var b = intercept(x, y);
                            var x = parseFloat(document.getElementById("valueXRoll").value);
                            var y = (m * (x * 100)) + b;
                            var equa = 'y = ' + m.toFixed(4) + 'x [plus] ' + b.toFixed(4);
                            if (!isNaN(y)) {

                                var uid = sessionStorage.getItem('uid');
                                if (uid === null) {
                                    uid = '';
                                }

                                var equaResIcon = document.getElementById("equaResIconRoll");
                                equaResIcon.classList.remove("hidden");
                                equaResIcon.classList.add("fa-spin");
                                $.ajax({
                                    url: "/TABLE2/ShowDataTablesWMS170?mode=updateRollHed"
                                        + "&code=" + qrmcode
                                        + "&desc=" + qrmdesc
                                        + "&unit=" + qrmbun
                                        + "&equa=" + equa
                                        + "&x=" + x
                                        + "&y=" + y.toFixed(4)
                                        + "&uid=" + uid
                                }).done(function (result) {
                                    if (result) {

                                        for (var i = 0; i < input.length; i++) {
                                            if (input[i].name === "detHeightRoll") {

                                                $.ajax({
                                                    url: "/TABLE2/ShowDataTablesWMS170?mode=updateRollDet"
                                                        + "&code=" + qrmcode
                                                        + "&line=" + input[i].id.toString().split('-')[0]
                                                        + "&length=" + $('#' + input[i].id.toString().split('-')[0] + '-detLength1').val()
                                                        + "&height=" + parseFloat(input[i].value)
                                                        + "&uid=" + uid
                                                }).done(function (result) {
                                                    //                                                alert(result);
                                                }).fail(function (jqXHR, textStatus, errorThrown) {
                                                    // needs to implement if it fails
                                                });
                                            }
                                        }

                                        GetDataWMS170();
                                        setTimeout(function () {
                                            equaResIcon.classList.remove("fa-spin");
                                            equaResIcon.classList.add("hidden");
                                        }, 1000);
                                    } else {
                                        document.getElementById("detailNERoll").innerHTML = "Update Failed !\n";
                                        errCount++;
                                    }

                                }).fail(function (jqXHR, textStatus, errorThrown) {
                                    // needs to implement if it fails
                                });
                            } else {
                                document.getElementById("detailNERoll").innerHTML = "Invalid format (not a number) !\n";
                                errCount++;
                            }

                        } else if (result === 't') {
                            document.getElementById("detailNERoll").innerHTML = "Style not Exist !\n";
                            errCount++;
                        }

                    }).fail(function (jqXHR, textStatus, errorThrown) {
                        // needs to implement if it fails
                    });
                } else {
                    document.getElementById("detailNERoll").innerHTML = "Roll Detail must more than 1 detail !\n";
                    errCount++;
                }
            }).fail(function (jqXHR, textStatus, errorThrown) {
                // needs to implement if it fails
            });
            if (errCount === 0) {
                document.getElementById("styleNERoll").innerHTML = "";
                document.getElementById("detailNERoll").innerHTML = "";
            }

        }

        function slope(x, y) {
            var xsum = 0;
            var ysum = 0;
            for (var i = 0; i < x.length; i++) {
                xsum += x[i];
                ysum += y[i];
            }

            var xbar = xsum / x.length;
            var ybar = ysum / y.length;
            var xbsum = 0;
            var ybsum = 0;
            for (var i = 0; i < x.length; i++) {
                xbsum += (x[i] - xbar) * (y[i] - ybar);
                ybsum += (x[i] - xbar) * (x[i] - xbar);
            }

            var m = xbsum / ybsum;
            return m;
        }

        function intercept(x, y) {
            var xsum = 0;
            var ysum = 0;
            for (var i = 0; i < x.length; i++) {
                xsum += x[i];
                ysum += y[i];
            }

            var xbar = xsum / x.length;
            var ybar = ysum / y.length;
            var xbsum = 0;
            var ybsum = 0;
            for (var i = 0; i < x.length; i++) {
                xbsum += (x[i] - xbar) * (y[i] - ybar);
                ybsum += (x[i] - xbar) * (x[i] - xbar);
            }

            var m = xbsum / ybsum;
            var b = ybar - (m * xbar);
            return b;
        }

        function DeleteDet(code, line) {

            var input = document.getElementsByTagName('input');
            var uid = sessionStorage.getItem('uid');
            if (uid === null) {
                uid = '';
            }
            for (var i = 0; i < input.length; i++) {
                if (input[i].name === "detHeight") {

                    $.ajax({
                        url: "/TABLE2/ShowDataTablesWMS165?mode=updateBagDet"
                            + "&code=" + code
                            + "&line=" + input[i].id.toString().split('-')[0]
                            + "&qty=" + $('#' + input[i].id.toString().split('-')[0] + '-detQty').val()
                            + "&height=" + parseFloat(input[i].value)
                            + "&uid=" + uid
                    }).done(function (result) {
                        //                                                alert(result);
                    }).fail(function (jqXHR, textStatus, errorThrown) {
                        // needs to implement if it fails
                    });
                }
            }

            $.ajax({
                url: "/TABLE2/ShowDataTablesWMS165?mode=deleteDet"
                    + "&code=" + code
                    + "&line=" + line
            }).done(function (result) {
                //                  alert(result);
            }).fail(function (jqXHR, textStatus, errorThrown) {
                // needs to implement if it fails
            });
            GetDataWMS165();
            GetDataWMS165();
        }

        function DeleteDetRoll(code, line) {

            var input = document.getElementsByTagName('input');
            var uid = sessionStorage.getItem('uid');
            if (uid === null) {
                uid = '';
            }
            for (var i = 0; i < input.length; i++) {
                if (input[i].name === "detHeightRoll") {

                    $.ajax({
                        url: "/TABLE2/ShowDataTablesWMS170?mode=updateRollDet"
                            + "&code=" + code
                            + "&line=" + input[i].id.toString().split('-')[0]
                            + "&length=" + $('#' + input[i].id.toString().split('-')[0] + '-detLength1').val()
                            + "&height=" + parseFloat(input[i].value)
                            + "&uid=" + uid
                    }).done(function (result) {
                        //                                                alert(result);
                    }).fail(function (jqXHR, textStatus, errorThrown) {
                        // needs to implement if it fails
                    });
                }
            }

            $.ajax({
                url: "/TABLE2/ShowDataTablesWMS170?mode=deleteDet"
                    + "&code=" + code
                    + "&line=" + line
            }).done(function (result) {
                //                  alert(result);
            }).fail(function (jqXHR, textStatus, errorThrown) {
                // needs to implement if it fails
            });
            GetDataWMS170();
            GetDataWMS170();
        }
    </script>
    <style>
        .bg-readonly {
            background-color: #ccc;
        }

        canvas {
            -moz-user-select: none;
            -webkit-user-select: none;
            -ms-user-select: none;
        }
    </style>
    <style>
        * {
            box-sizing: border-box
        }

        /* Set height of body and the document to 100% */
        body,
        html {
            height: 100%;
            margin: 0;
            font-family: Arial;
        }

        /* Style tab links */
        .tablink {
            background-color: #ccc;
            color: white;
            float: left;
            border: none;
            outline: none;
            cursor: pointer;
            padding: 14px 16px;
            font-size: 17px;
            width: 10%;
        }

        .tablink:hover {
            background-color: #777;
        }

        /* Style the tab content (and add height:100% for full page content) */
        .tabcontent {
            display: none;
            padding: 50px 20px;
            height: 100%;
        }

        .hhBand {
            cursor: default;
        }

        .headBand {
            cursor: default;
            transition: 1.0s;
            display: inline-block;
            color: #FFD700;
        }

        /* .hhBand:hover { */
        /*transform: scale(2);*/
        /* } */

        .headBandHover {
            /*transform: scale(1.5);*/
            /*transform: rotateZ(360deg);*/
            /*transform: rotateX(360deg);*/
            transform: rotateY(180deg);
            /*opacity: 0;*/
            /*color: white;*/
        }
    </style>
</head>

<body>
    <center>
        <br>
        <div id="waveBand" class="hhBand">
            <div class="headBand">
                <h4><b>S</b></h4>
            </div>
            <div class="headBand">
                <h4><b>A</b></h4>
            </div>
            <div class="headBand">
                <h4><b>N</b></h4>
            </div>
            <div class="headBand">
                <h4><b>D</b></h4>
            </div>
            <div class="headBand">
                <h4><b>B</b></h4>
            </div>
            <div class="headBand">
                <h4><b>O</b></h4>
            </div>
            <div class="headBand">
                <h4><b>X</b></h4>
            </div>
        </div>
        <input type="text" id="waveBandText" placeholder="type the header" onkeyup="waveBandTextCg();">
        <label>
            <input type="checkbox" id="valIsSplit" checked="" onchange="waveBandTextCg();">
            Split
        </label>
        <script>
            function waveBandTextCg() {
                var val = $('#waveBandText').val();
                if (val.toString().trim() === "") {
                    val = "SANDBOX";
                }
                $('#waveBand').html('');
                var valSplit = val;
                if (!$('#valIsSplit').prop('checked')) {
                    valSplit = val.split("--");
                }
                for (var i = 0; i < valSplit.length; i++) {
                    $('#waveBand').append('<div class="headBand"><h4><b>' + valSplit[i] + '</b></h4></div>\n');
                }
            }

            var headBandCnt = (parseInt($('.headBand').length - 1) * 100);
            setInterval(function aniHead() {
                var eles = $('.headBand');
                var i = 0;
                function myLoop() {
                    setTimeout(function () {
                        $(eles[i]).toggleClass('headBandHover');
                        i++;
                        if (i < eles.length) {
                            myLoop();
                        }
                    }, 100);
                }
                myLoop();
            }, headBandCnt);
        </script>
        <hr>
    </center>
    <button class="tablink" onclick="openPage('Typing', this)" id="typingTab">Typing</button>
    <button class="tablink" onclick="openPage('Tetris', this)" id="tetrisTab">Tetris</button>
    <button class="tablink" onclick="openPage('Snake', this)" id="snakeTab">Snake</button>
    <button class="tablink" onclick="openPage('Keyboard', this)" id="keyboardTab">Keyboard</button>
    <button class="tablink" onclick="openPage('Audition', this)" id="auditionTab">Audition</button>
    <button class="tablink" onclick="openPage('API', this)" id="apiTab">API</button>
    <button class="tablink" onclick="openPage('Canvas', this)" id="canvasTab">Canvas</button>
    <button class="tablink" onclick="openPage('Matching', this)" id="matchingTab">Matching</button>
    <button class="tablink" onclick="openPage('Calculator', this)" id="calculatorTab">Calculator</button>
    <button class="tablink" onclick="openPage('WMSPROGRAM', this)" id="wmsprogramTab">WMS</button>

    <div id="Typing" class="tabcontent">
        <style>
            .typingBoard {
                border: 2px solid #e5e5e5;
                border-radius: 15px;
                color: #008cff;
            }
        </style>
        <script>
            var initText = 'hello, it\'s Me';

            $(document).ready(function () {

                displayParagraph();
            });

            function displayParagraph() {
                var text = initText.toString();
                for (let i = 0; i < text.length; i++) {
                    $('#testText').append(text[i]);
                }
            }

            $(document).bind('keydown', function (e) {
                var keyCont = $('#Typing.tabcontent');
                if (!keyCont.is(':hidden')) {
                    if (e.key !== 'CapsLock' && e.key !== 'Shift') {
                        if (e.key === 'Backspace') {
                            arrText.pop();
                        } else {
                            arrText.push(e.key);
                            checkType();
                        }

                        $('#testText').html('');
                        displayParagraph();
                        $('#resType').html(arrText);
                    }
                }
            });

            var arrText = [];

            function checkType() {
                var text = initText.toString();

                if (arrText.length > 0) {
                    if (arrText[arrText.length - 1] === text[arrText.length - 1]) {
                        alertify.success('true');
                    } else {
                        alertify.error('false');
                    }
                }
            }
        </script>
        <center>
            <br>
            <h3>Typing Test</h3>
            <hr>
            <div class="typingBoard">
                <h2 id="testText"></h2>
            </div>
            <hr>
            <div id="resType"></div>
        </center>
    </div>

    <div id="Tetris" class="tabcontent">
        <style>
            .ttStagePx {
                border: 1px solid #ebebeb;
            }

            .divInTrTt:hover {
                background-color: #ccc;
                border-color: #ccc;
            }

            .ttStagePxActive {
                border-color: #000;
            }

            .ttStagePxActivePermanent {
                border-color: #000;
            }

            .divInTrTt {
                width: 10px;
                height: 10px;
            }

            .noselectTt {
                -webkit-touch-callout: none;
                /* iOS Safari */
                -webkit-user-select: none;
                /* Safari */
                -khtml-user-select: none;
                /* Konqueror HTML */
                -moz-user-select: none;
                /* Old versions of Firefox */
                -ms-user-select: none;
                /* Internet Explorer/Edge */
                user-select: none;
                /* Non-prefixed version, currently
                                      supported by Chrome, Edge, Opera and Firefox */
            }
        </style>
        <center>
            <br>
            <h3>Tetris</h3>
            <hr>
            <script>
                $('#ttStage').empty();
                var brick = 100;
                var brickRow = Math.sqrt(brick);
                var iter = 0;
                for (let i = 0; i < brick; i++) {
                    iter++;
                    $('#ttStage').append('<div class="ttStagePx"></div>');
                    if (iter === brickRow) {
                        $('#ttStage').append('<br>');
                        iter = 0;
                    }
                }
            </script>
            <div class="row">
                <div class="col-sm-3" align="right">
                    <div id="arrowDisplay">
                        <i class="fa fa-arrow-left fa-5x"></i>
                    </div>
                </div>
                <div class="col-sm-6">
                    <table id="ttTable">
                        <%
                            int iterTt = 0;
                            int rowNoTt = 1;
                            for (int i = 0; i < 2000; i++) {
                                iterTt++;

                                if (i == 0) {
                        %>
                        <tr>
                            <td id="<%=rowNoTt%>-<%=iterTt%>" class="ttStagePx noselectTt">
                                <div class="divInTrTt"></div>
                            </td>
                            <%
                            } else if (iterTt == 50) {
                            %>
                            <td id="<%=rowNoTt%>-<%=iterTt%>" class="ttStagePx noselectTt">
                                <div class="divInTrTt"></div>
                            </td>
                        </tr>
                        <%            iterTt = 0;
                            rowNoTt++;
                        } else {
                        %>
                        <td id="<%=rowNoTt%>-<%=iterTt%>" class="ttStagePx noselectTt">
                            <div class="divInTrTt"></div>
                        </td>
                        <%
                                }
                            }
                        %>
                    </table>
                </div>
                <div class="col-sm-3" align="left">
                    <h1>Tetris Count : <span id="ttCnt">0</span></h1>
                    <hr>
                    <button class="btn btn-primary" onclick="clearTtStage()">Clear Stage</button>
                    <hr>
                    <button class="btn btn-danger" onclick="stopTtStage()">Stop Stage</button>
                </div>
            </div>
            <!-- Modal -->
            <div class="modal fade" id="ttGameOverModal" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Game Over!</h4>
                        </div>
                        <div class="modal-body">
                            <p>
                            <h3>Score : <span id="ttScore"></span>
                            </h3>
                            </p>
                            <p>
                            <h3>Estimate Time : <span id="ttTime"></span>
                            </h3>
                            </p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>

                </div>
            </div>
        </center>
        <script>
            var cntSt = 0;
            var ttHeadId = [];
            var ttColor = [
                "red",
                "green",
                "blue",
                "yellow",
                "pink",
                "purple",
                "orange"
            ];
            var curTtBgColor = null;
            var startTt = new Date();

            function genNewTt() {
                curTtBgColor = ttColor[Math.floor(Math.random() * ttColor.length)];
                if (curTtBgColor === "orange") {
                    ttHeadId = ['2-24', '3-24', '3-25', '3-26'];

                } else if (curTtBgColor === "red") {
                    ttHeadId = ['3-23', '3-24', '3-25', '3-26'];

                } else if (curTtBgColor === "green") {
                    ttHeadId = ['2-25', '2-26', '3-24', '3-25'];

                } else if (curTtBgColor === "blue") {
                    ttHeadId = ['2-25', '3-24', '3-25', '3-26'];

                } else if (curTtBgColor === "yellow") {
                    ttHeadId = ['2-26', '3-24', '3-25', '3-26'];

                } else if (curTtBgColor === "pink") {
                    ttHeadId = ['2-25', '2-26', '3-25', '3-26'];

                } else if (curTtBgColor === "purple") {
                    ttHeadId = ['2-24', '2-25', '3-25', '3-26'];

                }

                for (let i = 0; i < ttHeadId.length; i++) {
                    $('#' + ttHeadId[i] + '.ttStagePx').addClass('ttStagePxActive');
                    $('#' + ttHeadId[i] + '.ttStagePx').css('background-color', curTtBgColor);
                }
            }

            genNewTt();

            var repeatTet = null;
            var ttHeadIdTmp = [];
            var isHaveSomeExist = false;
            var isBottom = false;
            var instTtSpeed = 100;
            var ttSpeed = instTtSpeed;
            var arrowArrTt = ['ArrowDown', 'ArrowLeft', 'ArrowRight'];
            $(document).bind('keydown', function (e) {
                var keyCont = $('#Tetris.tabcontent');
                if (!keyCont.is(':hidden')) {
                    if (arrowArrTt.includes(e.key)) {
                        var va = e.key.toString().replace('Arrow', '').toLowerCase();
                        $('#arrowDisplay').html('<i class="fa fa-arrow-' + va + ' fa-5x"></i>');
                        if (cntSt === 0) {
                            startTt = new Date();
                        }
                        cntSt++;
                        stopTtStage();
                        moveTet(e.key);
                        repeatTet = setInterval(function () {
                            moveTet('ArrowDown');
                            $('#arrowDisplay').html('<i class="fa fa-arrow-down fa-5x"></i>');
                        }, ttSpeed);
                    }
                }
            });

            function moveTet(ekey) {
                ttHeadIdTmp = [];
                isHaveSomeExist = false;
                isBottom = false;
                $('.ttStagePx.ttStagePxActive').removeAttr('style');
                $('.ttStagePx').removeClass('ttStagePxActive');
                if (ekey === 'ArrowLeft') {
                    for (let i = 0; i < ttHeadId.length; i++) {
                        var ttHeadSplit = ttHeadId[i].split('-');
                        var cgRow = 0;
                        var cgCol = 0;
                        var maxRow = 40;
                        var maxCol = 50;

                        cgCol = -1;

                        var newTtHead = (parseInt(ttHeadSplit[0]) + cgRow) + '-' + (parseInt(ttHeadSplit[1]) + cgCol);

                        if ($('#' + newTtHead + '.ttStagePx').length <= 0) {
                            stopTtStage();
                            drawLastTt();
                            break;

                        } else {
                            var isEx = $('#' + newTtHead + '.ttStagePx').hasClass('ttStagePxActivePermanent');
                            if (isEx) {
                                isHaveSomeExist = true;
                                stopTtStage();
                                $('.ttStagePx').removeClass('ttStagePxActive');
                                drawLastTt();
                                break;
                            } else {
                                ttHeadIdTmp.push(ttHeadId[i]);
                                ttHeadId[i] = newTtHead;
                            }
                        }

                        $('#' + ttHeadId[i] + '.ttStagePx').addClass('ttStagePxActive');
                        $('#' + ttHeadId[i] + '.ttStagePx').css('background-color', curTtBgColor);
                    }

                } else {
                    for (let i = ttHeadId.length - 1; i >= 0; i--) {
                        var ttHeadSplit = ttHeadId[i].split('-');
                        var cgRow = 0;
                        var cgCol = 0;
                        var maxRow = 40;
                        var maxCol = 50;

                        if (ekey === 'ArrowDown') {
                            cgRow = 1;

                        } else if (ekey === 'ArrowRight') {
                            cgCol = 1;

                        }

                        var newTtHead = (parseInt(ttHeadSplit[0]) + cgRow) + '-' + (parseInt(ttHeadSplit[1]) + cgCol);

                        if ($('#' + newTtHead + '.ttStagePx').length <= 0) {
                            stopTtStage();
                            if (ekey === 'ArrowDown') {
                                isBottom = true;
                                drawLastTt();
                                break;

                            } else if (ekey === 'ArrowRight') {
                                drawLastTt();
                                break;

                            }
                        } else {
                            var isEx = $('#' + newTtHead + '.ttStagePx').hasClass('ttStagePxActivePermanent');
                            if (ekey === 'ArrowDown' && isEx) {
                                isHaveSomeExist = true;
                                stopTtStage();
                                $('.ttStagePx').removeClass('ttStagePxActive');
                                drawLastTt();
                                break;
                            } else if (ekey === 'ArrowRight' && isEx) {
                                stopTtStage();
                                drawLastTt();
                                break;
                            } else {
                                ttHeadIdTmp.push(ttHeadId[i]);
                                ttHeadId[i] = newTtHead;
                            }
                        }

                        $('#' + ttHeadId[i] + '.ttStagePx').addClass('ttStagePxActive');
                        $('#' + ttHeadId[i] + '.ttStagePx').css('background-color', curTtBgColor);
                    }
                }

                if (isHaveSomeExist) {
                    for (let i = ttHeadId.length - 1; i >= 0; i--) {
                        if (ttHeadIdTmp.length === 0) {
                            break;
                        }
                        ttHeadId[i] = ttHeadIdTmp.shift();
                    }
                    drawLastTt();

                    if (ekey === 'ArrowDown') {
                        endCurTt();
                    }

                }

                if (isBottom) {
                    endCurTt();
                }
            }

            function clearTtStage() {
                $('.ttStagePx').removeClass('ttStagePxActive');
                $('.ttStagePx').removeClass('ttStagePxActivePermanent');
                $('.ttStagePx').removeAttr('style');

                genNewTt();

                stopTtStage();
                ttSpeed = instTtSpeed;
                cntSt = 0;
                $('#ttCnt').html(0);
                $('#ttScore').html(0);
                $('#ttTime').html('');
            }

            function stopTtStage() {
                clearInterval(repeatTet);
            }

            function drawLastTt() {
                $('.ttStagePx').removeClass('ttStagePxActive');
                for (let i = 0; i < ttHeadId.length; i++) {
                    $('#' + ttHeadId[i] + '.ttStagePx').addClass('ttStagePxActive');
                    $('#' + ttHeadId[i] + '.ttStagePx').css('background-color', curTtBgColor);
                }
            }

            function endCurTt() {
                for (let i = 0; i < ttHeadId.length; i++) {
                    var bgColor = $('#' + ttHeadId[i] + '.ttStagePx').css('background-color');
                    $('#' + ttHeadId[i] + '.ttStagePx').removeClass('ttStagePxActive');
                    $('#' + ttHeadId[i] + '.ttStagePx').addClass('ttStagePxActivePermanent');
                    $('#' + ttHeadId[i] + '.ttStagePx').css('background-color', bgColor);
                }
                $('.ttStagePx').not('.ttStagePxActivePermanent').removeAttr('style');

                var ttCnt = parseInt($('#ttCnt').html()) + 1;
                $('#ttCnt').html(ttCnt);

                var topRow = parseInt(ttHeadId[0].toString().split('-')[0]);

                if (topRow > 4) {
                    genNewTt();

                    stopTtStage();
                    moveTet('ArrowDown');
                    repeatTet = setInterval(function () {
                        moveTet('ArrowDown');
                        $('#arrowDisplay').html('<i class="fa fa-arrow-down fa-5x"></i>');
                    }, ttSpeed);
                } else {
                    $('#ttScore').html(ttCnt);

                    var endTt = new Date();
                    var estMillisec = parseInt(endTt.getTime()) - parseInt(startTt.getTime());
                    var estSecond = parseInt(estMillisec / 1000);
                    var estTime = new Date(0, 0, 0, 0, 0, estSecond, 0);

                    var ttTimeRange = fillZeroLeft(startTt.getHours()) + ':' + fillZeroLeft(startTt.getMinutes()) + ':' + fillZeroLeft(startTt.getSeconds()) + ' - ' + fillZeroLeft(endTt.getHours()) + ':' + fillZeroLeft(endTt.getMinutes()) + ':' + fillZeroLeft(endTt.getSeconds());

                    var estHrs = estTime.getHours() + ' Hours ';
                    var estMins = estTime.getMinutes() + ' Minutes ';
                    var estSecs = estTime.getSeconds() + ' Seconds ';

                    if (parseInt(estTime.getHours()) === 0) {
                        estHrs = '';
                    }
                    if (parseInt(estTime.getMinutes()) === 0) {
                        estMins = '';
                    }
                    if (parseInt(estTime.getSeconds()) === 0) {
                        estSecs = '';
                    }

                    $('#ttTime').html(estHrs + estMins + estSecs + ' [ ' + ttTimeRange + ' ] ');

                    $('#ttGameOverModal').modal('show');
                }
            }

            function fillZeroLeft(val) {
                var len = val.toString().trim().length;
                var reVal = val.toString().trim();
                if (len < 2) {
                    reVal = '0' + reVal;
                }
                return reVal;
            }
        </script>
    </div>

    <div id="Snake" class="tabcontent">
        <style>
            .snStagePx {
                border: 1px solid #ebebeb;
            }

            .divInTrSn:hover {
                background-color: #ccc;
                border-color: #ccc;
            }

            .snStagePxActive {
                background-color: #000;
                /* border-color: #000; */
            }

            .snStagePxApple {
                background-color: #F00;
                border-color: #F00;
            }

            .divInTrSn {
                width: 10px;
                height: 10px;
            }

            .noselectSn {
                -webkit-touch-callout: none;
                /* iOS Safari */
                -webkit-user-select: none;
                /* Safari */
                -khtml-user-select: none;
                /* Konqueror HTML */
                -moz-user-select: none;
                /* Old versions of Firefox */
                -ms-user-select: none;
                /* Internet Explorer/Edge */
                user-select: none;
                /* Non-prefixed version, currently
                                      supported by Chrome, Edge, Opera and Firefox */
            }
        </style>
        <center>
            <br>
            <h3>Snake</h3>
            <hr>
            <script>
                $('#snStage').empty();
                var brick = 100;
                var brickRow = Math.sqrt(brick);
                var iter = 0;
                for (let i = 0; i < brick; i++) {
                    iter++;
                    $('#snStage').append('<div class="snStagePx"></div>');
                    if (iter === brickRow) {
                        $('#snStage').append('<br>');
                        iter = 0;
                    }
                }
            </script>
            <div class="row">
                <div class="col-sm-3" align="right">
                    <div id="arrowDisplay">
                        <i class="fa fa-arrow-left fa-5x"></i>
                    </div>
                </div>
                <div class="col-sm-6">
                    <table id="snTable">
                        <%
                            int iterSn = 0;
                            int rowNoSn = 1;
                            for (int i = 0; i < 2000; i++) {
                                iterSn++;

                                if (i == 0) {
                        %>
                        <tr>
                            <td id="<%=rowNoSn%>-<%=iterSn%>" class="snStagePx noselectSn">
                                <div class="divInTrSn"></div>
                            </td>
                            <%
                            } else if (iterSn == 50) {
                            %>
                            <td id="<%=rowNoSn%>-<%=iterSn%>" class="snStagePx noselectSn">
                                <div class="divInTrSn"></div>
                            </td>
                        </tr>
                        <%            iterSn = 0;
                            rowNoSn++;
                        } else {
                        %>
                        <td id="<%=rowNoSn%>-<%=iterSn%>" class="snStagePx noselectSn">
                            <div class="divInTrSn"></div>
                        </td>
                        <%
                                }
                            }
                        %>
                    </table>
                </div>
                <div class="col-sm-3" align="left">
                    <h1>Apple : <span id="snLen">0</span></h1>
                    <br>
                    <h1>Length : <span id="snLen2">1</span></h1>
                    <h1>Max Length : <span id="snLen2max">1</span></h1>
                    <hr>
                    <button class="btn btn-primary" onclick="clearSnStage()">Clear Stage</button>
                    <hr>
                    <button class="btn btn-danger" onclick="stopSnStage()">Stop Stage</button>
                </div>
            </div>
        </center>
        <script>
            var snHeadId = '21-25';
            $('#' + snHeadId + '.snStagePx').addClass('snStagePxActive');

            function randomApple() {
                $('.snStagePxApple').removeClass('snStagePxApple');
                var ran = Math.floor(Math.random() * ($('.snStagePx').not('.snStagePxActive').length - 1));
                $('.snStagePx').not('.snStagePxActive').eq(ran).addClass('snStagePxApple');
            }

            randomApple();

            var snBody = [snHeadId];
            var repeatSlither = null;
            var snSpeed = 200;

            var arrowArr = ['ArrowUp', 'ArrowDown', 'ArrowLeft', 'ArrowRight'];
            $(document).bind('keydown', function (e) {
                var keyCont = $('#Snake.tabcontent');
                if (!keyCont.is(':hidden')) {
                    if (arrowArr.includes(e.key)) {
                        var va = e.key.toString().replace('Arrow', '').toLowerCase();
                        $('#arrowDisplay').html('<i class="fa fa-arrow-' + va + ' fa-5x"></i>');

                        clearInterval(repeatSlither);
                        slither(e.key);
                        repeatSlither = setInterval(function () {
                            slither(e.key);
                        }, snSpeed);


                    }
                }
            });

            function slither(ekey) {
                var snHeadSplit = snHeadId.split('-');
                var cgRow = 0;
                var cgCol = 0;
                var maxRow = 40;
                var maxCol = 50;

                if (ekey === 'ArrowUp') {
                    cgRow = -1;

                } else if (ekey === 'ArrowDown') {
                    cgRow = 1;

                } else if (ekey === 'ArrowLeft') {
                    cgCol = -1;

                } else if (ekey === 'ArrowRight') {
                    cgCol = 1;

                }

                var newSnHead = (parseInt(snHeadSplit[0]) + cgRow) + '-' + (parseInt(snHeadSplit[1]) + cgCol);

                if ($('#' + newSnHead + '.snStagePx').length <= 0) {
                    if (ekey === 'ArrowUp') {
                        newSnHead = maxRow + '-' + (parseInt(snHeadSplit[1]) + cgCol);

                    } else if (ekey === 'ArrowDown') {
                        newSnHead = 1 + '-' + (parseInt(snHeadSplit[1]) + cgCol);

                    } else if (ekey === 'ArrowLeft') {
                        newSnHead = (parseInt(snHeadSplit[0]) + cgRow) + '-' + maxCol;

                    } else if (ekey === 'ArrowRight') {
                        newSnHead = (parseInt(snHeadSplit[0]) + cgRow) + '-' + 1;

                    }

                }

                var isSnake = $('#' + newSnHead + '.snStagePx').hasClass('snStagePxActive');
                if (isSnake) {
                    var idxSn = parseInt(snBody.indexOf(newSnHead));
                    var len2 = parseInt($('#snLen2').html());
                    for (let i = 0; i < (len2 - idxSn); i++) {
                        $('#' + snBody.pop() + '.snStagePx').removeClass('snStagePxActive');
                        var len2f = parseInt($('#snLen2').html()) - 1;
                        $('#snLen2').html(len2f);
                        var len2max = parseInt($('#snLen2max').html());
                        if (len2f > len2max) {
                            $('#snLen2max').html(len2f);
                        }
                        snSpeed += 10;
                    }

                }

                snHeadId = newSnHead;
                snBody.unshift(newSnHead);
                var isApple = $('#' + snHeadId + '.snStagePx').hasClass('snStagePxApple');
                if (isApple) {
                    snBody.unshift(newSnHead);
                    var len = parseInt($('#snLen').html()) + 1;
                    $('#snLen').html(len);
                    var len2 = parseInt($('#snLen2').html()) + 1;
                    $('#snLen2').html(len2);
                    var len2max = parseInt($('#snLen2max').html());
                    if (len2 > len2max) {
                        $('#snLen2max').html(len2);
                    }
                    randomApple();
                    snSpeed -= 10;
                }

                $('#' + snHeadId + '.snStagePx').addClass('snStagePxActive');
                $('#' + snBody.pop() + '.snStagePx').removeClass('snStagePxActive');

            }

            function clearSnStage() {
                snHeadId = '21-25';
                $('.snStagePx').removeClass('snStagePxActive');
                $('#' + snHeadId + '.snStagePx').addClass('snStagePxActive');

                snBody = [snHeadId];
                stopSnStage();
                randomApple();
                $('#snLen').html(0);
                $('#snLen2').html(1);
                $('#snLen2max').html(1);
                snSpeed = 200;
            }

            function stopSnStage() {
                clearInterval(repeatSlither);
            }
        </script>
    </div>

    <div id="Keyboard" class="tabcontent">
        <style>
            .keyContainer {
                height: 200px;
                width: 50px;
                position: relative;
                /*margin: 20px;*/
                display: inline-block;
            }

            .keyBase {
                transition: 0.4s;
                background-color: white;
                border: 3px solid black;
                border-radius: 0px 0px 10px 10px;
                box-shadow: 10px 10px 5px 0px rgba(0, 0, 0, 0.75);
                height: 200px;
                width: 50px;
                cursor: pointer;
                position: absolute;
            }

            .keyBaseBlack {
                color: white;
                background-color: black;
                height: 120px;
                width: 40px;
                z-index: 9;
                margin-left: 30px;
            }

            .keyBase:hover {
                transform: scale(1.01);
            }

            .keyBaseActive {
                transition: 0.1s;
                transform: scale(0.95);
                box-shadow: 5px 5px 5px 0px rgba(0, 0, 0, 0.75);
            }

            .keyNote {
                position: absolute;
                bottom: 0;
                text-align: center;
                width: 100%;
            }

            .keyBaseBind {
                transition: 0.4s;
                background-color: white;
                border: 3px solid black;
                border-radius: 10px;
                height: fit-content;
                width: 50px;
                cursor: pointer;
                display: inline-block;
            }

            .keyBaseBindBlack {
                color: white;
                background-color: black;
                height: fit-content;
                width: 40px;
            }

            .keyNoteBind {
                text-align: center;
                width: 100%;
            }
        </style>
        <center>
            <br>
            <h3>Digital Keyboard</h3>
            <hr>
            <div id="digitKey">
                <div class="keyContainer">
                    <div id="1-piano_middle_C" class="keyBase">
                        <div class="keyNote">
                            <h3>C</h3>
                        </div>
                    </div>
                    <div id="1-piano_C_sharp" class="keyBase keyBaseBlack">
                        <div class="keyNote">D<sup>b</sup><br>C<sup>#</sup></div>
                    </div>
                </div>
                <div class="keyContainer">
                    <div id="1-piano_D" class="keyBase">
                        <div class="keyNote">
                            <h3>D</h3>
                        </div>
                    </div>
                    <div id="1-piano_D_sharp" class="keyBase keyBaseBlack">
                        <div class="keyNote">E<sup>b</sup><br>D<sup>#</sup></div>
                    </div>
                </div>
                <div class="keyContainer">
                    <div id="1-piano_E" class="keyBase">
                        <div class="keyNote">
                            <h3>E</h3>
                        </div>
                    </div>
                </div>
                <div class="keyContainer">
                    <div id="1-piano_F" class="keyBase">
                        <div class="keyNote">
                            <h3>F</h3>
                        </div>
                    </div>
                    <div id="1-piano_F_sharp" class="keyBase keyBaseBlack">
                        <div class="keyNote">G<sup>b</sup><br>F<sup>#</sup></div>
                    </div>
                </div>
                <div class="keyContainer">
                    <div id="1-piano_G" class="keyBase">
                        <div class="keyNote">
                            <h3>G</h3>
                        </div>
                    </div>
                    <div id="1-piano_G_sharp" class="keyBase keyBaseBlack">
                        <div class="keyNote">A<sup>b</sup><br>G<sup>#</sup></div>
                    </div>
                </div>
                <div class="keyContainer">
                    <div id="1-piano_A" class="keyBase">
                        <div class="keyNote">
                            <h3>A</h3>
                        </div>
                    </div>
                    <div id="1-piano_A_sharp" class="keyBase keyBaseBlack">
                        <div class="keyNote">B<sup>b</sup><br>A<sup>#</sup></div>
                    </div>
                </div>
                <div class="keyContainer">
                    <div id="1-piano_B" class="keyBase">
                        <div class="keyNote">
                            <h3>B</h3>
                        </div>
                    </div>
                </div>
                <div class="keyContainer">
                    <div id="2-piano_middle_C" class="keyBase">
                        <div class="keyNote">
                            <h3>C</h3>
                        </div>
                    </div>
                    <div id="2-piano_C_sharp" class="keyBase keyBaseBlack">
                        <div class="keyNote">D<sup>b</sup><br>C<sup>#</sup></div>
                    </div>
                </div>
                <div class="keyContainer">
                    <div id="2-piano_D" class="keyBase">
                        <div class="keyNote">
                            <h3>D</h3>
                        </div>
                    </div>
                    <div id="2-piano_D_sharp" class="keyBase keyBaseBlack">
                        <div class="keyNote">E<sup>b</sup><br>D<sup>#</sup></div>
                    </div>
                </div>
                <div class="keyContainer">
                    <div id="2-piano_E" class="keyBase">
                        <div class="keyNote">
                            <h3>E</h3>
                        </div>
                    </div>
                </div>
                <div class="keyContainer">
                    <div id="2-piano_F" class="keyBase">
                        <div class="keyNote">
                            <h3>F</h3>
                        </div>
                    </div>
                    <div id="2-piano_F_sharp" class="keyBase keyBaseBlack">
                        <div class="keyNote">G<sup>b</sup><br>F<sup>#</sup></div>
                    </div>
                </div>
                <div class="keyContainer">
                    <div id="2-piano_G" class="keyBase">
                        <div class="keyNote">
                            <h3>G</h3>
                        </div>
                    </div>
                </div>
            </div>
            <hr>
            <style>
                #settingIcon {
                    cursor: pointer;
                    font-size: 30px;
                    transition: 0.6s;
                }

                #settingIcon:hover {
                    transform: rotateZ(45deg);
                }

                #settingBox {
                    display: none;
                    border: 2px solid black;
                    border-radius: 15px;
                    margin: 20px;
                    padding: 20px;
                }
            </style>
            <i id="settingIcon" class="fa fa-cog" aria-hidden="true"></i>
            <br>
            Setting
            <script>
                $('.keyBase').on('mousedown', function () {
                    var note = $(this).attr('id').toString().split('-')[1];
                    var noteAudio = new Audio('http://www.vibrationdata.com/' + note + '.mp3');
                    noteAudio.volume = 1.0;
                    noteAudio.play();

                    $('.keyBase').removeClass('keyBaseActive');
                    $(this).addClass('keyBaseActive');

                }).on('mouseup mouseleave', function () {
                    $('.keyBase').removeClass('keyBaseActive');

                });

                var keyBindJson = {
                    c: "a",
                    cSharp: "w",
                    d: "s",
                    dSharp: "e",
                    e: "d",
                    f: "f",
                    fSharp: "t",
                    g: "g",
                    gSharp: "y",
                    a: "h",
                    aSharp: "u",
                    b: "j",
                    c2: "k",
                    cSharp2: "o",
                    d2: "l",
                    dSharp2: "p",
                    e2: ";",
                    f2: "'",
                    fSharp2: "]",
                    g2: "Enter"
                };

                $(document).bind('keydown', function (e) {
                    var keyCont = $('#Keyboard.tabcontent');
                    var settingBox = $('#settingBox');
                    if (!keyCont.is(':hidden') && settingBox.is(':hidden')) {
                        var key = Object.keys(keyBindJson).find(key => keyBindJson[key] === e.key);
                        if (key !== undefined) {
                            var id = '';
                            if (key === 'c') {
                                id = '1-piano_middle_C';

                            } else if (key === 'cSharp') {
                                id = '1-piano_C_sharp';

                            } else if (key === 'd') {
                                id = '1-piano_D';

                            } else if (key === 'dSharp') {
                                id = '1-piano_D_sharp';

                            } else if (key === 'e') {
                                id = '1-piano_E';

                            } else if (key === 'f') {
                                id = '1-piano_F';

                            } else if (key === 'fSharp') {
                                id = '1-piano_F_sharp';

                            } else if (key === 'g') {
                                id = '1-piano_G';

                            } else if (key === 'gSharp') {
                                id = '1-piano_G_sharp';

                            } else if (key === 'a') {
                                id = '1-piano_A';

                            } else if (key === 'aSharp') {
                                id = '1-piano_A_sharp';

                            } else if (key === 'b') {
                                id = '1-piano_B';

                            } else if (key === 'c2') {
                                id = '2-piano_middle_C';

                            } else if (key === 'cSharp2') {
                                id = '2-piano_C_sharp';

                            } else if (key === 'd2') {
                                id = '2-piano_D';

                            } else if (key === 'dSharp2') {
                                id = '2-piano_D_sharp';

                            } else if (key === 'e2') {
                                id = '2-piano_E';

                            } else if (key === 'f2') {
                                id = '2-piano_F';

                            } else if (key === 'fSharp2') {
                                id = '2-piano_F_sharp';

                            } else if (key === 'g2') {
                                id = '2-piano_G';

                            }
                            $('#' + id).mousedown();
                        }

                    }
                }).bind('keyup', function (e) {
                    var keyCont = $('#Keyboard.tabcontent');
                    var settingBox = $('#settingBox');
                    if (!keyCont.is(':hidden') && settingBox.is(':hidden')) {
                        $('.keyBase').mouseup();
                    }
                });
            </script>
            <hr>
            <style>
                .keySet {
                    display: inline-block;
                }

                .expIcon {
                    text-align: right;
                }
            </style>
            <div id="settingBox">
                <div class="row">
                    <div class="col-sm-4"></div>
                    <div class="col-sm-4">
                        <b>Key Binding</b>
                    </div>
                    <div class="col-sm-4 expIcon">
                        <i id="toggleSetInputIcon" class="fa fa-expand fa-2x" aria-hidden="true"
                            style="cursor: pointer;"></i>
                    </div>
                </div>
                <hr>
                <div class="keySet">
                    <div id="set-cSharp" class="keyBaseBind keyBaseBindBlack">
                        <div class="keyNoteBind">D<sup>b</sup><br>C<sup>#</sup></div>
                    </div>
                    <br>
                    <input id="set-cSharp-input" type="text" maxlength="1" class="setInput"
                        style="width: 80px; display: none; text-align: center;">
                </div>
                <div class="keySet">
                    <div id="set-dSharp" class="keyBaseBind keyBaseBindBlack">
                        <div class="keyNoteBind">E<sup>b</sup><br>D<sup>#</sup></div>
                    </div>
                    <br>
                    <input id="set-dSharp-input" type="text" maxlength="1" class="setInput"
                        style="width: 80px; display: none; text-align: center;">
                </div>
                |
                <div class="keySet">
                    <div id="set-fSharp" class="keyBaseBind keyBaseBindBlack">
                        <div class="keyNoteBind">G<sup>b</sup><br>F<sup>#</sup></div>
                    </div>
                    <br>
                    <input id="set-fSharp-input" type="text" maxlength="1" class="setInput"
                        style="width: 80px; display: none; text-align: center;">
                </div>
                <div class="keySet">
                    <div id="set-gSharp" class="keyBaseBind keyBaseBindBlack">
                        <div class="keyNoteBind">A<sup>b</sup><br>G<sup>#</sup></div>
                    </div>
                    <br>
                    <input id="set-gSharp-input" type="text" maxlength="1" class="setInput"
                        style="width: 80px; display: none; text-align: center;">
                </div>
                <div class="keySet">
                    <div id="set-aSharp" class="keyBaseBind keyBaseBindBlack">
                        <div class="keyNoteBind">B<sup>b</sup><br>A<sup>#</sup></div>
                    </div>
                    <br>
                    <input id="set-aSharp-input" type="text" maxlength="1" class="setInput"
                        style="width: 80px; display: none; text-align: center;">
                </div>
                |
                <div class="keySet">
                    <div id="set-cSharp2" class="keyBaseBind keyBaseBindBlack">
                        <div class="keyNoteBind">D<sup>b</sup><br>C<sup>#</sup></div>
                    </div>
                    <br>
                    <input id="set-cSharp2-input" type="text" maxlength="1" class="setInput"
                        style="width: 80px; display: none; text-align: center;">
                </div>
                <div class="keySet">
                    <div id="set-dSharp2" class="keyBaseBind keyBaseBindBlack">
                        <div class="keyNoteBind">E<sup>b</sup><br>D<sup>#</sup></div>
                    </div>
                    <br>
                    <input id="set-dSharp2-input" type="text" maxlength="1" class="setInput"
                        style="width: 80px; display: none; text-align: center;">
                </div>
                |
                <div class="keySet">
                    <div id="set-fSharp2" class="keyBaseBind keyBaseBindBlack">
                        <div class="keyNoteBind">G<sup>b</sup><br>F<sup>#</sup></div>
                    </div>
                    <br>
                    <input id="set-fSharp2-input" type="text" maxlength="1" class="setInput"
                        style="width: 80px; display: none; text-align: center;">
                </div>
                <hr>
                <div class="keySet">
                    <div id="set-c" class="keyBaseBind">
                        <div class="keyNoteBind">
                            <h3>C</h3>
                        </div>
                    </div>
                    <br>
                    <input id="set-c-input" type="text" maxlength="1" class="setInput"
                        style="width: 80px; display: none; text-align: center;">
                </div>
                <div class="keySet">
                    <div id="set-d" class="keyBaseBind">
                        <div class="keyNoteBind">
                            <h3>D</h3>
                        </div>
                    </div>
                    <br>
                    <input id="set-d-input" type="text" maxlength="1" class="setInput"
                        style="width: 80px; display: none; text-align: center;">
                </div>
                <div class="keySet">
                    <div id="set-e" class="keyBaseBind">
                        <div class="keyNoteBind">
                            <h3>E</h3>
                        </div>
                    </div>
                    <br>
                    <input id="set-e-input" type="text" maxlength="1" class="setInput"
                        style="width: 80px; display: none; text-align: center;">
                </div>
                <div class="keySet">
                    <div id="set-f" class="keyBaseBind">
                        <div class="keyNoteBind">
                            <h3>F</h3>
                        </div>
                    </div>
                    <br>
                    <input id="set-f-input" type="text" maxlength="1" class="setInput"
                        style="width: 80px; display: none; text-align: center;">
                </div>
                <div class="keySet">
                    <div id="set-g" class="keyBaseBind">
                        <div class="keyNoteBind">
                            <h3>G</h3>
                        </div>
                    </div>
                    <br>
                    <input id="set-g-input" type="text" maxlength="1" class="setInput"
                        style="width: 80px; display: none; text-align: center;">
                </div>
                <div class="keySet">
                    <div id="set-a" class="keyBaseBind">
                        <div class="keyNoteBind">
                            <h3>A</h3>
                        </div>
                    </div>
                    <br>
                    <input id="set-a-input" type="text" maxlength="1" class="setInput"
                        style="width: 80px; display: none; text-align: center;">
                </div>
                <div class="keySet">
                    <div id="set-b" class="keyBaseBind">
                        <div class="keyNoteBind">
                            <h3>B</h3>
                        </div>
                    </div>
                    <br>
                    <input id="set-b-input" type="text" maxlength="1" class="setInput"
                        style="width: 80px; display: none; text-align: center;">
                </div>
                <div class="keySet">
                    <div id="set-c2" class="keyBaseBind">
                        <div class="keyNoteBind">
                            <h3>C</h3>
                        </div>
                    </div>
                    <br>
                    <input id="set-c2-input" type="text" maxlength="1" class="setInput"
                        style="width: 80px; display: none; text-align: center;">
                </div>
                <div class="keySet">
                    <div id="set-d2" class="keyBaseBind">
                        <div class="keyNoteBind">
                            <h3>D</h3>
                        </div>
                    </div>
                    <br>
                    <input id="set-d2-input" type="text" maxlength="1" class="setInput"
                        style="width: 80px; display: none; text-align: center;">
                </div>
                <div class="keySet">
                    <div id="set-e2" class="keyBaseBind">
                        <div class="keyNoteBind">
                            <h3>E</h3>
                        </div>
                    </div>
                    <br>
                    <input id="set-e2-input" type="text" maxlength="1" class="setInput"
                        style="width: 80px; display: none; text-align: center;">
                </div>
                <div class="keySet">
                    <div id="set-f2" class="keyBaseBind">
                        <div class="keyNoteBind">
                            <h3>F</h3>
                        </div>
                    </div>
                    <br>
                    <input id="set-f2-input" type="text" maxlength="1" class="setInput"
                        style="width: 80px; display: none; text-align: center;">
                </div>
                <div class="keySet">
                    <div id="set-g2" class="keyBaseBind">
                        <div class="keyNoteBind">
                            <h3>G</h3>
                        </div>
                    </div>
                    <br>
                    <input id="set-g2-input" type="text" maxlength="1" class="setInput"
                        style="width: 80px; display: none; text-align: center;">
                </div>
                <hr>
                <button id="saveSetBtn" class="btn btn-success">Save</button>
            </div>
            <script>
                $('.keySet .keyBaseBind').click(function () {
                    $('#' + $(this).attr('id') + '-input').toggle(200);
                    $('#' + $(this).attr('id') + '-input').focus();
                });

                $('#saveSetBtn').click(function () {
                    var setInput = $('.setInput');
                    for (var i = 0; i < setInput.length; i++) {
                        var val = setInput[i].value;
                        var note = setInput[i].id.toString().split('-')[1];
                        keyBindJson[note] = val;
                    }

                    //                    $('.setInput').hide(200);
                    $('#settingBox').hide(500);
                    alertify.success('Saved!');
                });

                $('#toggleSetInputIcon').click(function () {
                    $('.setInput').toggle(200);
                });

                $('#settingIcon').click(function () {
                    var settingBox = $('#settingBox');
                    if (settingBox.is(':hidden')) {
                        Object.keys(keyBindJson).forEach(function (k) {
                            $('#set-' + k + '-input').val(keyBindJson[k]);
                        });

                        $('.setInput').css('display', 'block');
                    }

                    $('#settingBox').toggle(500);
                    $('html, body').animate({ scrollTop: $('#settingBox').offset().top }, 'slow');
                });

                $('.setInput').css('min-width', '80px').css('background-color', '#abece2');

                $('.setInput').on('keydown', function (e) {
                    var setInput = $('.setInput');
                    var isDup = false;
                    for (var i = 0; i < setInput.length; i++) {
                        if (setInput[i].value === e.key) {
                            isDup = true;
                            break;
                        }
                    }

                    if (isDup) {
                        alertify.error('Duplicate key binding!');
                    } else {
                        $(this).val(e.key);
                    }

                }).on('keyup', function () {
                    $(this).css('width', (($(this).val().length + 1) * 15) + 'px');
                });
            </script>
        </center>
        <br>
    </div>

    <div id="Audition" class="tabcontent">
        <style>
            #audiStage {
                border: 2px solid #6a00b5;
                width: 100px;
                height: 100px;
            }

            .audiArrow {
                transition: 0.2s;
            }

            .audiArrow-hit {
                transform: scale(1.5);
                color: #008cff;
                opacity: 0;
            }

            .audiArrow-miss {
                transform: scale(1.5);
                color: red;
                opacity: 0;
            }
        </style>
        <script>
            var ar = new Array(33, 34, 35, 36, 37, 38, 39, 40);

            $(document).keydown(function (e) {
                var key = e.which;
                //console.log(key);
                //if(key==35 || key == 36 || key == 37 || key == 39)
                if ($.inArray(key, ar) > -1) {
                    e.preventDefault();
                    return false;
                }
                return true;
            });

            function currencyFormat(num) {
                return num.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
            }

            var arrowMaster = ['ArrowUp', 'ArrowDown', 'ArrowLeft', 'ArrowRight'];
            var arrow = [];

            $(document).bind('keydown', function (e) {
                var audiCont = $('#Audition.tabcontent');
                if (!audiCont.is(':hidden')) {
                    if (true) {
                        var lastEle = $('.audiArrow').last();
                        if (arrow[arrow.length - 1] === e.key) {
                            $(lastEle).toggleClass('audiArrow-hit');
                            var point = parseInt($('#audiPoint').html().toString().replace(/,/g, '')) + 1000;
                            $('#audiPoint').html(currencyFormat(point));
                        } else if (arrowMaster.includes(e.key)) {
                            $(lastEle).toggleClass('audiArrow-miss');
                            var point = parseInt($('#audiPoint').html().toString().replace(/,/g, '')) - 250;
                            $('#audiPoint').html(currencyFormat(point));
                        }
                        arrow.pop();
                        //                      drawArrow();
                    }
                }
            });

            var addArrow = null;
            function startAudi() {
                addArrow = setInterval(function () {

                    var arrowRandom = arrowMaster[Math.floor(Math.random() * arrowMaster.length)];
                    arrow = [];
                    arrow.push(arrowRandom);

                    drawArrow();
                }, 800);
            }

            function drawArrow() {
                $('#audiStage').html('');

                for (var i = 0; i < arrow.length; i++) {
                    var vector = arrow[i].toString().replace('Arrow', '').toLowerCase();
                    $('#audiStage').append('<i class="fa fa-arrow-' + vector + ' fa-5x audiArrow"></i><br>');
                }

            }

            function togSSBtn() {
                $('.startBtn').toggle();
                $('.stopBtn').toggle();
            }</script>
        <center>
            <br>
            <div id="audiStage"></div>
            <div>
                <h3>
                    <b style="color: #6a00b5;">
                        POINT :
                        <span id="audiPoint" style="color: #FFD700;">0</span>
                    </b>
                </h3>
            </div>
            <br>
            <div onclick="togSSBtn()
                            ;">
                <button class="btn btn-success startBtn" onclick="startAudi()
                                ;">Start</button>
                <button class="btn btn-danger stopBtn" onclick="clearInterval(addArrow)
                                ;" style="display: none;">Stop</button>
            </div>
            <hr>
            <style>
                .towerBase {
                    transition: 0.8s;
                    background-color: #000;
                    height: 200px;
                    width: 5px;
                    display: inline-block;
                }

                .tower {
                    width: 10px;
                }
            </style>
            <div id="tableTower" class="row">
            </div>
            <br>
            <input type="range" id="towerCtrl" oninput="drawWave(this.value);" value="0" min="0" max="100"
                style="width: 1000px;">
            <script>
                var maxRange = parseInt($('#towerCtrl').attr('max'));

                $('#tableTower').append('<div class="towerBase"></div>');
                for (var i = 0; i <= maxRange; i++) {
                    $('#tableTower').append('<div class="towerBase tower"></div>');
                }

                var cnt = 0;
                var towerHeight = 200;

                function drawWave(val) {
                    //                    $('.alertify-log-show').hide();
                    //                    alertify.success(val);
                    cnt = parseInt(val);
                    $('#towerCtrl').val(val);
                    var idx = parseInt(val);

                    var color = '#777';

                    $('.tower').css('height', towerHeight + 'px');
                    $('.tower').css('background-color', color);

                    var cpx = Math.floor(Math.random() * towerHeight);
                    $('.tower:eq(' + (idx) + ')').css('height', cpx + 'px');
                    $('.tower:eq(' + (idx) + ')').css('background-color', '#FFD700');

                    var epoc = maxRange;
                    var inpx = towerHeight / (epoc + 1);
                    var px = inpx;
                    for (var i = 1; i <= epoc; i++) {
                        px = Math.floor(Math.random() * towerHeight);
                        $('.tower:eq(' + (idx - i) + ')').css('height', px + 'px');
                        $('.tower:eq(' + (idx + i) + ')').css('height', px + 'px');

                        var percent = parseInt((px / towerHeight) * 100);
                        if (percent <= 25) {
                            color = '#03a142';
                        } else if (percent >= 75) {
                            color = '#b01100';
                        } else {
                            color = '#777';
                        }
                        $('.tower:eq(' + (idx - i) + ')').css('background-color', color);
                        $('.tower:eq(' + (idx + i) + ')').css('background-color', color);

                        //                        px += inpx;
                    }
                }

                var runWave = null;
                function startWave() {
                    runWave = setInterval(function () {
                        drawWave(cnt);
                        if (cnt >= maxRange) {
                            cnt = 0;
                        } else {
                            cnt += 1;
                        }
                    }, 2000);
                }

                function togSSBtnWave() {
                    $('.startBtnWave').toggle();
                    $('.stopBtnWave').toggle();
                }

                $('.tower').on('click mouseover', function () {
                    drawWave($('.tower').index($(this)));
                });</script>
            <div onclick="togSSBtnWave()

                            ;">
                <button class="btn btn-success startBtnWave" onclick="startWave()

                                ;">Start</button>
                <button class="btn btn-danger stopBtnWave" onclick="clearInterval(runWave)
                                ;" style="display: none;">Stop</button>
            </div>
        </center>
    </div>

    <div id="API" class="tabcontent">
        <style>
            .iconIdk.fa-chevron-down {
                color: #004075;
            }

            .iconIdk.fa-chevron-right {
                color: #008cff;
            }

            .ulIdk {
                transition: 0.6s;
            }

            .iconIdk {
                transition: 0.6s;
            }
        </style>
        <script>
            function testAPI(event, url, type) {
                if (event.keyCode === 13) {

                    var username = "test";
                    var password = "1234";

                    var dataJson = {
                        "channel": "",
                        "email": "prodigy53@viriyah.co.th",
                        "firstname": "ABCD01241136",
                        "lastname": "ทดสอบ",
                        "phone": "0864493739",
                        "begindate": "2020-02-24",
                        "carinsuretype": "plan",
                        "carmake": "TOYOTA",
                        "carmodel": "VIOS",
                        "carregisyear": "2020",
                        "cartrim": "รุ่นย่อย",
                        "countryto": "",
                        "deduct": "000006",
                        "enddate": "2020-06-16",
                        "birthdate": "1984-04-30",
                        "note": "note test",
                        "opd": "",
                        "plan": "แผนประกัน",
                        "product": "1",
                        "timeperiod": "11.00-12.00",
                        "type": "แบบประกัน"
                    };

                    var dataToSend = dataJson;
                    if (type === "GET") {
                        dataToSend = JSON.stringify(dataJson);
                    }

                    $.ajax({
                        type: type,
                        url: url,
                        dataType: 'json',
                        data: dataToSend,
                        headers: {
                            'Authorization': 'Basic ' + btoa(username + ':' + password)
                        }
                    }).done(function (data) {
                        $('#resAPI').html('<button type="button" class="btn-success btn-sm" style="width: 100px;">Complete</button>');
                        $('#papi').html('');
                        var jsonText = JSON.stringify(data);

                        jsonText = jsonText.replace(/\[/g, '<i class="iconIdk fa fa-chevron-down"></i><ul class="ulIdk"><li>');
                        jsonText = jsonText.replace(/\]/g, '</li></ul>');

                        jsonText = jsonText.replace(/\{/g, '<i class="iconIdk fa fa-chevron-down"></i><ul class="ulIdk"><li>');
                        jsonText = jsonText.replace(/\}/g, '</li></ul>');

                        jsonText = jsonText.replace(/\,/g, '</li><li>');

                        $('#papi').html(jsonText);

                        idi();

                        $('.iconIdk.fa-chevron-down').on('click', function () {
                            $(this).toggleClass('fa-chevron-down');
                            $(this).toggleClass('fa-chevron-right');
                            $('#ulIdk-' + $(this).attr('id')).toggle();
                        });
                        $('.iconIdk.fa-chevron-right').on('click', function () {
                            $(this).toggleClass('fa-chevron-right');
                            $(this).toggleClass('fa-chevron-down');
                            $('#ulIdk-' + $(this).attr('id')).toggle();
                        });

                    }).fail(function (jqXHR, textStatus, errorThrown) {
                        $('#resAPI').html('<button type="button" class="btn-danger btn-sm" style="width: 100px;">Error</button>');
                        $('#papi').html(JSON.stringify(jqXHR));
                    });

                    //                    var xhr = new XMLHttpRequest();
                    //                    xhr.open(type, url, true);
                    //                    xhr.withCredentials = true;
                    //                    xhr.setRequestHeader("Authorization", 'Basic ' + btoa(username + ':' + password));
                    //                    xhr.send(JSON.stringify(dataJson));
                    //                    xhr.onload = function () {
                    //                        $('#papi').html(xhr.responseText);
                    //                    };


                }
            }

            function idi() {
                var eles = $('.iconIdk');
                for (var i = 0; i < eles.length; i++) {
                    $(eles[i]).attr('id', 'iconIdk-' + i);
                }

                var eles2 = $('.ulIdk');
                for (var i = 0; i < eles2.length; i++) {
                    $(eles2[i]).attr('id', 'ulIdk-iconIdk-' + i);
                }
            }

            function collapseJson() {
                $('.iconIdk.fa-chevron-down').click();
            }

            function expandJson() {
                $('.iconIdk.fa-chevron-right').click();
            }

            function expCol(ele) {
                if ($(ele).html().toString().trim() === "Collapse All") {
                    $(ele).html("Expand All");
                    collapseJson();
                } else {
                    $(ele).html("Collapse All");
                    expandJson();
                }
            }</script>
        <div style="margin: 50px;">
            URL : <input type="text" autofocus="" id="urlAPI" value="https://jsonplaceholder.typicode.com/users"
                onkeyup="testAPI(event, this.value, $('#typeAPI').val())

                                         ;">
            <select id="typeAPI" onchange="$('#urlAPI').focus()

                            ;">
                <option value="GET">GET</option>
                <option value="POST">POST</option>
            </select>
            Result : <span id="resAPI"></span>
        </div>
        <button onclick="expCol(this)">Collapse All</button>
        <div id="papi" style="margin: 50px;" contenteditable="false"></div>
    </div>

    <div id="Canvas" class="tabcontent">
        <center>
            <script>
                var bodyClick = false;

                function paint(ele) {

                    $('body').mousedown(function () {
                        bodyClick = true;
                    }).mouseup(function () {
                        bodyClick = false;
                    });

                    var tools = $('input[name=tools]:checked').val();

                    if (bodyClick) {
                        if (tools === "pencil") {
                            $(ele).css('background-color', $('#colorSelect').val());
                            $(ele).css('border-color', $('#colorSelect').val());

                        } else if (tools === "eraser") {
                            $(ele).css('background-color', 'white');
                            $(ele).css('border-color', 'white');

                        }
                    }

                }

                function paint2(ele) {

                    var tools = $('input[name=tools]:checked').val();
                    if (tools === "eraser") {
                        $(ele).css('background-color', 'white');
                        $(ele).css('border-color', 'white');

                    } else if (tools === "plusSign") {
                        var row_ct = parseInt(ele.id.toString().split("-")[0]);
                        var col_ct = parseInt(ele.id.toString().split("-")[1]);

                        var rtId = (row_ct) + '-' + (col_ct + 1);
                        var ltId = (row_ct) + '-' + (col_ct - 1);
                        var dwId = (row_ct + 1) + '-' + (col_ct);
                        var upId = (row_ct - 1) + '-' + (col_ct);

                        coloring(ele);
                        coloring($('#' + rtId));
                        coloring($('#' + ltId));
                        coloring($('#' + dwId));
                        coloring($('#' + upId));

                    } else if (tools === "fill") {
                        var ct_color = $(ele).css("background-color");

                        var eles = $(".pixel").filter(function () {
                            return ($(this).css("background-color") === ct_color);
                        });

                        $(eles).css('background-color', $('#colorSelect').val());
                        $(eles).css('border-color', $('#colorSelect').val());

                    } else if (tools === "pencil") {
                        coloring(ele);
                    }
                }

                function coloring(ele) {
                    $(ele).css('background-color', $('#colorSelect').val());
                    $(ele).css('border-color', $('#colorSelect').val());
                }

                function clearCanvas() {
                    $('.pixel').css('background-color', 'white');
                    $('.pixel').css('border-color', 'white');
                }
                function bgCanvas() {
                    var eles = $(".pixel").filter(function () {
                        return ($(this).css("background-color") === "rgba(0, 0, 0, 0)" || $(this).css("background-color") === "rgb(255, 255, 255)" || $(this).css("background-color") === "white");
                    });

                    $(eles).css('background-color', $('#colorSelect').val());
                    $(eles).css('border-color', $('#colorSelect').val());
                }</script>
            <style>
                .pixel {
                    border: 1px solid white;
                }

                .divInTr:hover {
                    background-color: #ccc;
                    border-color: #ccc;
                }

                .divInTr {
                    width: 10px;
                    height: 10px;
                }

                .canvas:hover {
                    cursor: pointer;
                }

                .sizeInput {
                    width: 80px;
                }

                .flip:hover {
                    transform: rotateX(360deg);
                    transition: transform 0.6s;
                }

                .hoverArrow:hover {
                    color: #e51e00;
                }

                .noselect {
                    -webkit-touch-callout: none;
                    /* iOS Safari */
                    -webkit-user-select: none;
                    /* Safari */
                    -khtml-user-select: none;
                    /* Konqueror HTML */
                    -moz-user-select: none;
                    /* Old versions of Firefox */
                    -ms-user-select: none;
                    /* Internet Explorer/Edge */
                    user-select: none;
                    /* Non-prefixed version, currently
                                          supported by Chrome, Edge, Opera and Firefox */
                }
            </style>
            <label style="cursor: pointer;">
                Adjust Size Canvas
                <br>
                <label>
                    Width : <input id="widVal" class="sizeInput" type="number" value="100" min="0" max="100" onchange="changeSizeInput()



                                    ;">
                    <input id="tableWidth" type="range" value="100" style="width: 200px;" oninput="changeSizeText();"
                        onchange="changeTableSize()


                                           ;">
                </label>
                <label>
                    Height : <input id="heiVal" class="sizeInput" type="number" value="30" min="0" max="100" onchange="changeSizeInput()


                                    ;">
                    <input id="tableHeight" type="range" value="30" style="width: 200px;" oninput="changeSizeText();"
                        onchange="changeTableSize()

                                           ;">
                </label>
            </label>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <script>
                setInterval(function getTime() {

                    var oldHr = sessionStorage.getItem("oldHr");
                    var oldMn = sessionStorage.getItem("oldMn");
                    var oldSc = sessionStorage.getItem("oldSc");

                    var d = new Date();
                    var hr = d.getHours();
                    var mn = d.getMinutes();
                    var sc = d.getSeconds();

                    if (parseInt(oldHr) !== parseInt(hr)) {
                        $('#hrNo').toggle(250);
                        $('#hrNo').toggle(250);
                    }
                    if (parseInt(oldMn) !== parseInt(mn)) {
                        $('#mnNo').toggle(250);
                        $('#mnNo').toggle(250);
                    }
                    $('#scNo').toggle(250);

                    $('#hrNo').html(hr);
                    $('#mnNo').html(mn);
                    $('#scNo').html(sc);

                    if (typeof (Storage) !== "undefined") {
                        sessionStorage.setItem("oldHr", hr);
                        sessionStorage.setItem("oldMn", mn);
                        sessionStorage.setItem("oldSc", sc);
                    }
                }, 500);</script>
            <span id="hrBall" class="fa-stack fa-lg flip">
                <i class="fa fa-circle fa-stack-2x"></i>
                <i id="hrNo" class="fa-stack-1x fa-inverse"></i>
            </span>
            <span id="mnBall" class="fa-stack fa-lg flip">
                <i class="fa fa-circle fa-stack-2x"></i>
                <i id="mnNo" class="fa-stack-1x fa-inverse"></i>
            </span>
            <span id="scBall" class="fa-stack fa-lg flip">
                <i class="fa fa-circle fa-stack-2x"></i>
                <i id="scNo" class="fa-stack-1x fa-inverse"></i>
            </span>
            <div style="height: fit-content; background-color: #ccc;">
                <table id="canvasTable">
                    <%
                        int iter = 0;
                        int rowNo = 1;
                        for (int i = 0; i < 3000; i++) {
                            iter++;

                            if (i == 0) {
                    %>
                    <tr>
                        <td id="<%=rowNo%>-<%=iter%>" class="pixel noselect" onmouseover="paint(this)


                                        ;" onclick="paint2(this)

                                                        ;">
                            <div class="divInTr"></div>
                        </td>
                        <%
                        } else if (iter == 100) {
                        %>
                        <td id="<%=rowNo%>-<%=iter%>" class="pixel noselect" onmouseover="paint(this)


                                        ;" onclick="paint2(this);">
                            <div class="divInTr"></div>
                        </td>
                    <tr>
                        <%            iter = 0;
                            rowNo++;
                        } else {
                        %>
                        <td id="<%=rowNo%>-<%=iter%>" class="pixel noselect" onmouseover="paint(this)


                                        ;" onclick="paint2(this)


                                                        ;">
                            <div class="divInTr"></div>
                        </td>
                        <%
                                }
                            }
                        %>
                </table>
            </div>
            <script>
                clearCanvas();

                function changeSizeInput() {
                    $('#tableWidth').val($('#widVal').val());
                    $('#tableHeight').val($('#heiVal').val());
                    changeTableSize();
                }

                function changeSizeText() {
                    var width = $('#tableWidth').val();
                    var height = $('#tableHeight').val();
                    $('#widVal').val(width);
                    $('#heiVal').val(height);
                }

                function changeTableSize() {
                    $('#canvasTable').html('');
                    var width = $('#tableWidth').val();
                    var height = $('#tableHeight').val();
                    var inTable = "";

                    var iter = 0;
                    var rowNo = 1;
                    for (var i = 0; i < (width * height); i++) {
                        iter++;
                        if (i === 0) {
                            inTable += '<tr><td id="' + rowNo + '-' + iter + '" class="pixel noselect" onmouseover="paint(this);" onclick="paint2(this);"><div class="divInTr"></div></td>';
                        } else if (parseInt(iter) === parseInt(width)) {
                            inTable += '<td id="' + rowNo + '-' + iter + '" class="pixel noselect" onmouseover="paint(this);" onclick="paint2(this);"><div class="divInTr"></div></td></tr><tr>';
                            iter = 0;
                            rowNo++;
                        } else {
                            inTable += '<td id="' + rowNo + '-' + iter + '" class="pixel noselect" onmouseover="paint(this);" onclick="paint2(this);"><div class="divInTr"></div></td>';
                        }
                    }

                    $('#canvasTable').html(inTable);

                    clearCanvas();
                }</script>
            <br>
            <label style="cursor: pointer;">
                <input type="radio" name="tools" value="pencil" checked>
                <i class="fa fa-pencil fa-3x" aria-hidden="true"></i>
            </label>
            <label style="cursor: pointer;">
                <input type="radio" name="tools" value="fill">
                <i class="fa fa-tint fa-3x" aria-hidden="true"></i>
            </label>
            <label style="cursor: pointer;">
                <input type="radio" name="tools" value="plusSign">
                <i class="fa fa-plus fa-3x" aria-hidden="true"></i>
            </label>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <input type="color" id="colorSelect" name="favcolor" value="#000">
            <button onclick="bgCanvas()


                            ;">Background Color</button>
            <button onclick="clearCanvas()



                            ;">Clear</button>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <label style="cursor: pointer;">
                <input type="radio" name="tools" value="eraser">
                <i class="fa fa-eraser fa-3x" aria-hidden="true"></i>
            </label>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <label style="cursor: pointer;">
                <i id="moveUp" class="fa fa-arrow-up fa-2x hoverArrow" aria-hidden="true"></i>
                <br>
                <i id="moveLeft" class="fa fa-arrow-left fa-2x hoverArrow" aria-hidden="true"></i>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <i id="moveRight" class="fa fa-arrow-right fa-2x hoverArrow" aria-hidden="true"></i>
                <br>
                <i id="moveDown" class="fa fa-arrow-down fa-2x hoverArrow" aria-hidden="true"></i>
            </label>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <input type="number" value="1" id="moveDistance" style="width: 50px;">
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <i id="savePic" class="fa fa-download fa-2x" aria-hidden="true" style="cursor: pointer;"></i>
            <span id="savePercent"></span>
            %

            <script>
                $('#savePic').click(function () {
                    var pixels = $('.pixel');
                    for (let i = 0; i < pixels.length; i++) {
                        $(pixels[i]).css('background-color', 'black').css('border-color', 'black');
                        $('#savePercent').html((((i + 1) / pixels.length) * 100).toFixed(2));
                    }
                });

                $('.hoverArrow').on('click', function () {
                    $id = $(this).attr('id');
                    var eles = $('.pixel');
                    var distance = parseInt($('#moveDistance').val());

                    if ($id === 'moveUp') {
                        for (var i = 0; i < eles.length; i++) {
                            var row = eles[i].id.toString().split("-")[0];
                            var col = eles[i].id.toString().split("-")[1];
                            var copyId = (parseInt(row) + distance).toString() + "-" + col;

                            eles[i].style.backgroundColor = $('#' + copyId).css('background-color');
                            eles[i].style.borderColor = $('#' + copyId).css('border-color');
                        }

                    } else if ($id === 'moveDown') {
                        for (var i = eles.length - 1; i >= 0; i--) {
                            var row = eles[i].id.toString().split("-")[0];
                            var col = eles[i].id.toString().split("-")[1];
                            var copyId = (parseInt(row) - distance).toString() + "-" + col;

                            eles[i].style.backgroundColor = $('#' + copyId).css('background-color');
                            eles[i].style.borderColor = $('#' + copyId).css('border-color');
                        }

                    } else if ($id === 'moveLeft') {
                        for (var i = 0; i < eles.length; i++) {
                            var row = eles[i].id.toString().split("-")[0];
                            var col = eles[i].id.toString().split("-")[1];
                            var copyId = row + "-" + (parseInt(col) + distance).toString();

                            eles[i].style.backgroundColor = $('#' + copyId).css('background-color');
                            eles[i].style.borderColor = $('#' + copyId).css('border-color');
                        }

                    } else if ($id === 'moveRight') {
                        for (var i = eles.length - 1; i >= 0; i--) {
                            var row = eles[i].id.toString().split("-")[0];
                            var col = eles[i].id.toString().split("-")[1];
                            var copyId = row + "-" + (parseInt(col) - distance).toString();

                            eles[i].style.backgroundColor = $('#' + copyId).css('background-color');
                            eles[i].style.borderColor = $('#' + copyId).css('border-color');
                        }

                    }

                });</script>

            <hr>
        </center>
    </div>

    <div id="Matching" class="tabcontent">
        <style>
            .scene {
                background-color: transparent;
                width: 150px;
                height: 150px;
                perspective: 1000px;
                cursor: pointer;
                transition: transform 0.4s;
            }

            .scene:hover {
                -ms-transform: scale(1.1);
                /* IE 9 */
                -webkit-transform: scale(1.1);
                /* Safari 3-8 */
                transform: scale(1.1);
            }

            .card {
                position: relative;
                width: 100%;
                height: 100%;
                text-align: center;
                transition: transform 0.4s;
                transform-style: preserve-3d;
                box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
                border-radius: 15px;
            }

            .card.is-flipped {
                transform: rotateY(180deg);
            }

            .card_face {
                position: absolute;
                width: 100%;
                height: 100%;
                -webkit-backface-visibility: hidden;
                backface-visibility: hidden;
                border-radius: 15px;
            }

            .card_face-front {
                background-color: #bbb;
            }

            .card_face-back {
                background-color: #2980b9;
                transform: rotateY(180deg);
            }

            .frontCardIcon {
                font-size: 150px;
                color: #777;
                transform: rotateZ(0deg);
                transition: transform 0.5s;
            }

            .frontCardIcon:hover {
                transform: rotateZ(360deg);
                color: #51a351;
            }

            .backCardIcon {
                font-size: 100px;
                color: white;
            }

            td.borderCard {
                padding: 5px;
            }
        </style>
        <script>
            $(document).ready(function () {

                var score = 0;

                $('.card').on('click', function () {

                    if (parseInt($('#cntOpen').val()) >= 2) {
                        $('#cntOpen').val(0);
                        $('.is-flipped').toggleClass('is-flipped');
                    }

                    $(this).toggleClass('is-flipped');
                    var isMatch = false;
                    if (($(this).attr('id').toString().split('_')[0] === $('#currentWord').html().toString().split('_')[0])) {
                        if (($(this).attr('id').toString().split('_')[1] !== $('#currentWord').html().toString().split('_')[1])) {
                            var stId = $(this).attr('id');
                            var ndId = $('#currentWord').html();
                            if ($('#' + stId).hasClass('is-flipped') && $('#' + ndId).hasClass('is-flipped')) {
                                alertify.success('Score +1');
                                score++;
                                $('#' + stId).delay(1000).toggle(1000);
                                $('#' + ndId).delay(1000).toggle(1000);
                                isMatch = true;
                            }
                        }
                    }

                    $('#currentWord').html($(this).attr('id'));

                    if ($(this).hasClass('is-flipped')) {
                        $('#cntOpen').val(parseInt($('#cntOpen').val()) + 1);
                    } else {
                        $('#cntOpen').val(parseInt($('#cntOpen').val()) - 1);
                    }
                    if (isMatch) {
                        $('#cntOpen').val(0);
                    }

                    if (score === 15) {
                        $('#winModal').modal('toggle');
                    }

                    if (parseInt($('#cntOpen').val()) < 0) {
                        $('#cntOpen').val(0);
                    }
                });

                $('#openAllBtn').on('click', function () {
                    $('.card').toggleClass('is-flipped');
                });

                $('#clearBtn').on('click', function () {
                    $('#randomBtn').click();
                    $('#cntOpen').val(0);
                    score = 0;
                    $('.is-flipped').toggleClass('is-flipped');

                    var eles = $(".card").filter(function () {
                        return ($(this).css("display") === "none");
                    });
                    $(eles).css("display", "");
                });

                var iconList = ['anchor', 'anchor', 'bomb', 'bomb', 'car', 'car', 'bicycle', 'bicycle', 'gamepad', 'gamepad'
                    , 'home', 'home', 'heart', 'heart', 'motorcycle', 'motorcycle', 'futbol-o', 'futbol-o', 'fighter-jet', 'fighter-jet'
                    , 'rocket', 'rocket', 'ship', 'ship', 'trophy', 'trophy', 'tree', 'tree', 'gift', 'gift'];

                $('#randomBtn').on('click', function () {
                    shuffle(iconList);
                    var cards = $(".card");
                    for (var i = 0; i < cards.length; i++) {
                        cards[i].id = iconList[i] + '_' + i;
                        $('#' + i + '-icon').attr('class', 'fa fa-' + iconList[i] + ' backCardIcon');
                    }
                });

                function shuffle(a) {
                    var j, x, i;
                    for (i = a.length - 1; i > 0; i--) {
                        j = Math.floor(Math.random() * (i + 1));
                        x = a[i];
                        a[i] = a[j];
                        a[j] = x;
                    }
                    return a;
                }

                $('#randomBtn').click();

            });</script>
        <center>
            <h3>Matching</h3>
            <p>Match the right!</p>
            <p>Current : <span id="currentWord"></span></p>
            <p>Count Open : <input id="cntOpen" type="number" value="0" style="width: 80px;"></p>
            <button id="openAllBtn">Open/Close All</button>
            <button id="randomBtn">Random</button>
            <button id="clearBtn">Clear</button>
            <br><br>
            <!-- Modal -->
            <div class="modal fade" id="winModal" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">You Won !</h4>
                        </div>
                        <div class="modal-body">
                            <h1>Congratulations <i class="fa fa-sign-language" aria-hidden="true"></i></h1>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>

                </div>
            </div>
            <table id="matchingTable">
                <tr>
                    <td class="borderCard">
                        <div class="scene scene-card">
                            <div class="card" id="times">
                                <div class="card_face card_face-front">
                                    <i class="fa fa-question-circle-o frontCardIcon" aria-hidden="true"></i>
                                </div>
                                <div class="card_face card_face-back">
                                    <br>
                                    <i id="0-icon" class="fa fa-times backCardIcon" aria-hidden="true"></i>
                                </div>
                            </div>
                        </div>
                    </td>
                    <td class="borderCard">
                        <div class="scene scene-card">
                            <div class="card" id="times">
                                <div class="card_face card_face-front">
                                    <i class="fa fa-question-circle-o frontCardIcon" aria-hidden="true"></i>
                                </div>
                                <div class="card_face card_face-back">
                                    <br>
                                    <i id="1-icon" class="fa fa-times backCardIcon" aria-hidden="true"></i>
                                </div>
                            </div>
                        </div>
                    </td>
                    <td class="borderCard">
                        <div class="scene scene-card">
                            <div class="card" id="times">
                                <div class="card_face card_face-front">
                                    <i class="fa fa-question-circle-o frontCardIcon" aria-hidden="true"></i>
                                </div>
                                <div class="card_face card_face-back">
                                    <br>
                                    <i id="2-icon" class="fa fa-times backCardIcon" aria-hidden="true"></i>
                                </div>
                            </div>
                        </div>
                    </td>
                    <td class="borderCard">
                        <div class="scene scene-card">
                            <div class="card" id="times">
                                <div class="card_face card_face-front">
                                    <i class="fa fa-question-circle-o frontCardIcon" aria-hidden="true"></i>
                                </div>
                                <div class="card_face card_face-back">
                                    <br>
                                    <i id="3-icon" class="fa fa-times backCardIcon" aria-hidden="true"></i>
                                </div>
                            </div>
                        </div>
                    </td>
                    <td class="borderCard">
                        <div class="scene scene-card">
                            <div class="card" id="times">
                                <div class="card_face card_face-front">
                                    <i class="fa fa-question-circle-o frontCardIcon" aria-hidden="true"></i>
                                </div>
                                <div class="card_face card_face-back">
                                    <br>
                                    <i id="4-icon" class="fa fa-times backCardIcon" aria-hidden="true"></i>
                                </div>
                            </div>
                        </div>
                    </td>
                    <td class="borderCard">
                        <div class="scene scene-card">
                            <div class="card" id="times">
                                <div class="card_face card_face-front">
                                    <i class="fa fa-question-circle-o frontCardIcon" aria-hidden="true"></i>
                                </div>
                                <div class="card_face card_face-back">
                                    <br>
                                    <i id="5-icon" class="fa fa-times backCardIcon" aria-hidden="true"></i>
                                </div>
                            </div>
                        </div>
                    </td>
                    <td class="borderCard">
                        <div class="scene scene-card">
                            <div class="card" id="times">
                                <div class="card_face card_face-front">
                                    <i class="fa fa-question-circle-o frontCardIcon" aria-hidden="true"></i>
                                </div>
                                <div class="card_face card_face-back">
                                    <br>
                                    <i id="6-icon" class="fa fa-times backCardIcon" aria-hidden="true"></i>
                                </div>
                            </div>
                        </div>
                    </td>
                    <td class="borderCard">
                        <div class="scene scene-card">
                            <div class="card" id="times">
                                <div class="card_face card_face-front">
                                    <i class="fa fa-question-circle-o frontCardIcon" aria-hidden="true"></i>
                                </div>
                                <div class="card_face card_face-back">
                                    <br>
                                    <i id="7-icon" class="fa fa-times backCardIcon" aria-hidden="true"></i>
                                </div>
                            </div>
                        </div>
                    </td>
                    <td class="borderCard">
                        <div class="scene scene-card">
                            <div class="card" id="times">
                                <div class="card_face card_face-front">
                                    <i class="fa fa-question-circle-o frontCardIcon" aria-hidden="true"></i>
                                </div>
                                <div class="card_face card_face-back">
                                    <br>
                                    <i id="8-icon" class="fa fa-times backCardIcon" aria-hidden="true"></i>
                                </div>
                            </div>
                        </div>
                    </td>
                    <td class="borderCard">
                        <div class="scene scene-card">
                            <div class="card" id="times">
                                <div class="card_face card_face-front">
                                    <i class="fa fa-question-circle-o frontCardIcon" aria-hidden="true"></i>
                                </div>
                                <div class="card_face card_face-back">
                                    <br>
                                    <i id="9-icon" class="fa fa-times backCardIcon" aria-hidden="true"></i>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="borderCard">
                        <div class="scene scene-card">
                            <div class="card" id="times">
                                <div class="card_face card_face-front">
                                    <i class="fa fa-question-circle-o frontCardIcon" aria-hidden="true"></i>
                                </div>
                                <div class="card_face card_face-back">
                                    <br>
                                    <i id="10-icon" class="fa fa-times backCardIcon" aria-hidden="true"></i>
                                </div>
                            </div>
                        </div>
                    </td>
                    <td class="borderCard">
                        <div class="scene scene-card">
                            <div class="card" id="times">
                                <div class="card_face card_face-front">
                                    <i class="fa fa-question-circle-o frontCardIcon" aria-hidden="true"></i>
                                </div>
                                <div class="card_face card_face-back">
                                    <br>
                                    <i id="11-icon" class="fa fa-times backCardIcon" aria-hidden="true"></i>
                                </div>
                            </div>
                        </div>
                    </td>
                    <td class="borderCard">
                        <div class="scene scene-card">
                            <div class="card" id="times">
                                <div class="card_face card_face-front">
                                    <i class="fa fa-question-circle-o frontCardIcon" aria-hidden="true"></i>
                                </div>
                                <div class="card_face card_face-back">
                                    <br>
                                    <i id="12-icon" class="fa fa-times backCardIcon" aria-hidden="true"></i>
                                </div>
                            </div>
                        </div>
                    </td>
                    <td class="borderCard">
                        <div class="scene scene-card">
                            <div class="card" id="times">
                                <div class="card_face card_face-front">
                                    <i class="fa fa-question-circle-o frontCardIcon" aria-hidden="true"></i>
                                </div>
                                <div class="card_face card_face-back">
                                    <br>
                                    <i id="13-icon" class="fa fa-times backCardIcon" aria-hidden="true"></i>
                                </div>
                            </div>
                        </div>
                    </td>
                    <td class="borderCard">
                        <div class="scene scene-card">
                            <div class="card" id="times">
                                <div class="card_face card_face-front">
                                    <i class="fa fa-question-circle-o frontCardIcon" aria-hidden="true"></i>
                                </div>
                                <div class="card_face card_face-back">
                                    <br>
                                    <i id="14-icon" class="fa fa-times backCardIcon" aria-hidden="true"></i>
                                </div>
                            </div>
                        </div>
                    </td>
                    <td class="borderCard">
                        <div class="scene scene-card">
                            <div class="card" id="times">
                                <div class="card_face card_face-front">
                                    <i class="fa fa-question-circle-o frontCardIcon" aria-hidden="true"></i>
                                </div>
                                <div class="card_face card_face-back">
                                    <br>
                                    <i id="15-icon" class="fa fa-times backCardIcon" aria-hidden="true"></i>
                                </div>
                            </div>
                        </div>
                    </td>
                    <td class="borderCard">
                        <div class="scene scene-card">
                            <div class="card" id="times">
                                <div class="card_face card_face-front">
                                    <i class="fa fa-question-circle-o frontCardIcon" aria-hidden="true"></i>
                                </div>
                                <div class="card_face card_face-back">
                                    <br>
                                    <i id="16-icon" class="fa fa-times backCardIcon" aria-hidden="true"></i>
                                </div>
                            </div>
                        </div>
                    </td>
                    <td class="borderCard">
                        <div class="scene scene-card">
                            <div class="card" id="times">
                                <div class="card_face card_face-front">
                                    <i class="fa fa-question-circle-o frontCardIcon" aria-hidden="true"></i>
                                </div>
                                <div class="card_face card_face-back">
                                    <br>
                                    <i id="17-icon" class="fa fa-times backCardIcon" aria-hidden="true"></i>
                                </div>
                            </div>
                        </div>
                    </td>
                    <td class="borderCard">
                        <div class="scene scene-card">
                            <div class="card" id="times">
                                <div class="card_face card_face-front">
                                    <i class="fa fa-question-circle-o frontCardIcon" aria-hidden="true"></i>
                                </div>
                                <div class="card_face card_face-back">
                                    <br>
                                    <i id="18-icon" class="fa fa-times backCardIcon" aria-hidden="true"></i>
                                </div>
                            </div>
                        </div>
                    </td>
                    <td class="borderCard">
                        <div class="scene scene-card">
                            <div class="card" id="times">
                                <div class="card_face card_face-front">
                                    <i class="fa fa-question-circle-o frontCardIcon" aria-hidden="true"></i>
                                </div>
                                <div class="card_face card_face-back">
                                    <br>
                                    <i id="19-icon" class="fa fa-times backCardIcon" aria-hidden="true"></i>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="borderCard">
                        <div class="scene scene-card">
                            <div class="card" id="times">
                                <div class="card_face card_face-front">
                                    <i class="fa fa-question-circle-o frontCardIcon" aria-hidden="true"></i>
                                </div>
                                <div class="card_face card_face-back">
                                    <br>
                                    <i id="20-icon" class="fa fa-times backCardIcon" aria-hidden="true"></i>
                                </div>
                            </div>
                        </div>
                    </td>
                    <td class="borderCard">
                        <div class="scene scene-card">
                            <div class="card" id="times">
                                <div class="card_face card_face-front">
                                    <i class="fa fa-question-circle-o frontCardIcon" aria-hidden="true"></i>
                                </div>
                                <div class="card_face card_face-back">
                                    <br>
                                    <i id="21-icon" class="fa fa-times backCardIcon" aria-hidden="true"></i>
                                </div>
                            </div>
                        </div>
                    </td>
                    <td class="borderCard">
                        <div class="scene scene-card">
                            <div class="card" id="times">
                                <div class="card_face card_face-front">
                                    <i class="fa fa-question-circle-o frontCardIcon" aria-hidden="true"></i>
                                </div>
                                <div class="card_face card_face-back">
                                    <br>
                                    <i id="22-icon" class="fa fa-times backCardIcon" aria-hidden="true"></i>
                                </div>
                            </div>
                        </div>
                    </td>
                    <td class="borderCard">
                        <div class="scene scene-card">
                            <div class="card" id="times">
                                <div class="card_face card_face-front">
                                    <i class="fa fa-question-circle-o frontCardIcon" aria-hidden="true"></i>
                                </div>
                                <div class="card_face card_face-back">
                                    <br>
                                    <i id="23-icon" class="fa fa-times backCardIcon" aria-hidden="true"></i>
                                </div>
                            </div>
                        </div>
                    </td>
                    <td class="borderCard">
                        <div class="scene scene-card">
                            <div class="card" id="times">
                                <div class="card_face card_face-front">
                                    <i class="fa fa-question-circle-o frontCardIcon" aria-hidden="true"></i>
                                </div>
                                <div class="card_face card_face-back">
                                    <br>
                                    <i id="24-icon" class="fa fa-times backCardIcon" aria-hidden="true"></i>
                                </div>
                            </div>
                        </div>
                    </td>
                    <td class="borderCard">
                        <div class="scene scene-card">
                            <div class="card" id="times">
                                <div class="card_face card_face-front">
                                    <i class="fa fa-question-circle-o frontCardIcon" aria-hidden="true"></i>
                                </div>
                                <div class="card_face card_face-back">
                                    <br>
                                    <i id="25-icon" class="fa fa-times backCardIcon" aria-hidden="true"></i>
                                </div>
                            </div>
                        </div>
                    </td>
                    <td class="borderCard">
                        <div class="scene scene-card">
                            <div class="card" id="times">
                                <div class="card_face card_face-front">
                                    <i class="fa fa-question-circle-o frontCardIcon" aria-hidden="true"></i>
                                </div>
                                <div class="card_face card_face-back">
                                    <br>
                                    <i id="26-icon" class="fa fa-times backCardIcon" aria-hidden="true"></i>
                                </div>
                            </div>
                        </div>
                    </td>
                    <td class="borderCard">
                        <div class="scene scene-card">
                            <div class="card" id="times">
                                <div class="card_face card_face-front">
                                    <i class="fa fa-question-circle-o frontCardIcon" aria-hidden="true"></i>
                                </div>
                                <div class="card_face card_face-back">
                                    <br>
                                    <i id="27-icon" class="fa fa-times backCardIcon" aria-hidden="true"></i>
                                </div>
                            </div>
                        </div>
                    </td>
                    <td class="borderCard">
                        <div class="scene scene-card">
                            <div class="card" id="times">
                                <div class="card_face card_face-front">
                                    <i class="fa fa-question-circle-o frontCardIcon" aria-hidden="true"></i>
                                </div>
                                <div class="card_face card_face-back">
                                    <br>
                                    <i id="28-icon" class="fa fa-times backCardIcon" aria-hidden="true"></i>
                                </div>
                            </div>
                        </div>
                    </td>
                    <td class="borderCard">
                        <div class="scene scene-card">
                            <div class="card" id="times">
                                <div class="card_face card_face-front">
                                    <i class="fa fa-question-circle-o frontCardIcon" aria-hidden="true"></i>
                                </div>
                                <div class="card_face card_face-back">
                                    <br>
                                    <i id="29-icon" class="fa fa-times backCardIcon" aria-hidden="true"></i>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>

        </center>
    </div>

    <div id="Calculator" class="tabcontent">
        <center>
            <style>
                .calBody {
                    width: 400px;
                    height: fit-content;
                    background-color: #d2d2d2;
                    border-radius: 15px;
                }

                .calInput {
                    width: 350px;
                    height: fit-content;
                    min-height: 50px;
                    background-color: white;
                    border-radius: 15px 15px 0px 0px;
                    text-align: right;
                    padding: 10px;
                    font-size: 25px;
                }

                .calAnswer {
                    width: 350px;
                    height: fit-content;
                    min-height: 50px;
                    background-color: #f7f7f7;
                    border-radius: 0px 0px 15px 15px;
                    text-align: right;
                    padding: 10px;
                    font-size: 25px;
                    cursor: pointer;
                }

                .calBtn {
                    width: 350px;
                    height: fit-content;
                    font-size: 25px;
                    text-align: center;
                }

                .col-sm-3 {
                    padding: 6px;
                }

                .btnCal {
                    width: 100%;
                    font-size: 20px;
                    transition: 0.6s;
                }

                .btnCal:hover {
                    transform: scale(1.1);
                }
            </style>
            <script>
                $(document).ready(function () {

                    $('#calAnswerId').on('dblclick', function () {
                        var copyText = $(this);
                        copyText.select();
                        document.execCommand("copy");
                        alertify.success('Copied !');
                    });

                    $('.calBtnChild').on('click', function () {
                        if ($(this).html().toString().trim() === "CL") {
                            $('#calInputId').html('');
                            $('#calAnswerId').html('0');

                        } else if ($(this).html().toString().trim() === "=") {
                            var inputText = $('#calInputId').html().toString().trim();
                            $('#calAnswerId').html(eval(inputText));

                        } else {
                            $('#calInputId').html($('#calInputId').html() + $(this).val());
                        }
                    });

                });</script>
            <h3>Calculator</h3>
            <p>Cal the right answer!</p>
            <div class="calBody">
                <br>
                <div id="calInputId" class="calInput" contenteditable="true">

                </div>
                <br>
                <div id="calAnswerId" class="calAnswer">
                    0
                </div>
                <br>
                <div class="calBtn">
                    <div class="row">
                        <div class="col-sm-3">
                            <button type="button" class="calBtnChild btnCal" value="(">(</button>
                        </div>
                        <div class="col-sm-3">
                            <button type="button" class="calBtnChild btnCal" value=")">)</button>
                        </div>
                        <div class="col-sm-3">
                            <button type="button" class="calBtnChild btnCal" value="%">%</button>
                        </div>
                        <div class="col-sm-3">
                            <button type="button" class="calBtnChild btnCal" value="CL">CL</button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3">
                            <button type="button" class="calBtnChild btnCal btn-default" value="7">7</button>
                        </div>
                        <div class="col-sm-3">
                            <button type="button" class="calBtnChild btnCal btn-default" value="8">8</button>
                        </div>
                        <div class="col-sm-3">
                            <button type="button" class="calBtnChild btnCal btn-default" value="9">9</button>
                        </div>
                        <div class="col-sm-3">
                            <button type="button" class="calBtnChild btnCal" value="/">÷</button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3">
                            <button type="button" class="calBtnChild btnCal btn-default" value="4">4</button>
                        </div>
                        <div class="col-sm-3">
                            <button type="button" class="calBtnChild btnCal btn-default" value="5">5</button>
                        </div>
                        <div class="col-sm-3">
                            <button type="button" class="calBtnChild btnCal btn-default" value="6">6</button>
                        </div>
                        <div class="col-sm-3">
                            <button type="button" class="calBtnChild btnCal" value="*">×</button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3">
                            <button type="button" class="calBtnChild btnCal btn-default" value="1">1</button>
                        </div>
                        <div class="col-sm-3">
                            <button type="button" class="calBtnChild btnCal btn-default" value="2">2</button>
                        </div>
                        <div class="col-sm-3">
                            <button type="button" class="calBtnChild btnCal btn-default" value="3">3</button>
                        </div>
                        <div class="col-sm-3">
                            <button type="button" class="calBtnChild btnCal" value="-">-</button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3">
                            <button type="button" class="calBtnChild btnCal btn-default" value="0">0</button>
                        </div>
                        <div class="col-sm-3">
                            <button type="button" class="calBtnChild btnCal btn-default" value=".">.</button>
                        </div>
                        <div class="col-sm-3">
                            <button type="button" class="calBtnChild btnCal btn-primary" value="=">=</button>
                        </div>
                        <div class="col-sm-3">
                            <button type="button" class="calBtnChild btnCal" value="+">+</button>
                        </div>
                    </div>
                </div>
                <br>
            </div>
        </center>
    </div>
    <div id="WMSPROGRAM" class="tabcontent">
        <center>
            <!-- Modal -->
            <div id="BAGMAS" class="modal fade" role="dialog">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content" style="width: auto; height: auto;">
                        <div class="modal-header">
                            <h4 class="modal-title">
                                <div class="row">
                                    <div class="col-sm-4" style="text-align: left;">WMS160</div>
                                    <div class="col-sm-4">BAG MASTER. DISPLAY</div>
                                    <div class="col-sm-4" id="timeBAGMAS" style="text-align: right;">TIME</div>
                                </div>
                            </h4>
                        </div>
                        <div class="modal-body">
                            <table id="showTableWMS160" class="display"
                                style="width:100%; border: 1px solid #ccc; border-radius: 4px;">
                                <thead>
                                    <tr>
                                        <th colspan="3" style="text-align: center; border-bottom-color: transparent;">
                                        </th>
                                        <th colspan="2" style="text-align: center; border-bottom-color: #ef604a;">
                                            เซนติเมตร</th>
                                        <th colspan="2" style="text-align: center; border-bottom-color: #009926;">นิ้ว
                                        </th>
                                        <th colspan="2" style="text-align: center; border-bottom-color: transparent;">
                                        </th>
                                        <th style="text-align: center; border-bottom-color: transparent;">
                                            <a title="Refresh" style=" cursor: pointer;" onclick="RefreshTable()

                                                            ;"><i id="refreshIcon" class="fa fa-refresh"
                                                    aria-hidden="true" style="font-size:25px;"></i></a>
                                        </th>
                                    </tr>
                                    <tr>
                                        <th style="text-align: center;"></th>
                                        <th style="text-align: center;">Size</th>
                                        <th style="text-align: center;">Desc</th>
                                        <th style="text-align: center;">กว้าง</th>
                                        <th style="text-align: center;">ยาว</th>
                                        <th style="text-align: center;">กว้าง</th>
                                        <th style="text-align: center;">ยาว</th>
                                        <th style="text-align: center;">User</th>
                                        <th style="text-align: center;">Create Date</th>
                                        <th style="text-align: center;">Option</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th></th>
                                        <th>
                                            <input type="text" id="add-size" style="width: 80px;" onkeyup="this.value = this.value.toUpperCase()

                                                            ;" maxlength="3">
                                        </th>
                                        <th>
                                            <input type="text" id="add-desc" style="width: 100px;">
                                        </th>
                                        <th>
                                            <input type="text" id="add-inWidth" style="text-align: right; width: 80px;">
                                        </th>
                                        <th>
                                            <input type="text" id="add-inLength"
                                                style="text-align: right; width: 80px;">
                                        </th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th>
                                            <a title="Add" style=" cursor: pointer;" onclick="addBag()

                                                            ;"><i class="fa fa-plus" aria-hidden="true"
                                                    style="font-size:25px;"></i></a>
                                        </th>
                                    </tr>
                                </tfoot>
                                <tbody>
                                </tbody>
                            </table>
                            <center>
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#BAGMAS"
                                    onclick="confirmBag()


                                                        ;">Confirm</button>
                            </center>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-toggle="modal"
                                data-target="#BAGMAS">Close</button>
                        </div>
                    </div>

                </div>
            </div>
        </center>
        <center>
            <!-- Modal -->
            <div id="BAGCAL" class="modal fade" role="dialog">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content" style="width: auto; height: auto;">
                        <div class="modal-header">
                            <h4 class="modal-title">
                                <div class="row">
                                    <div class="col-sm-4" style="text-align: left;">WMS165</div>
                                    <div class="col-sm-4">Standard Bag Dimension. Calculation</div>
                                    <div class="col-sm-4" id="timeBAGCAL" style="text-align: right;">TIME</div>
                                </div>
                            </h4>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-sm-2" style=" text-align: right;">
                                    <b style="color: #00399b; font-size: 20px;">QR Code : </b>
                                </div>
                                <div class="col-sm-7">
                                    <input type="text" name="qr" id="qr">
                                    <script>
                                        var input = document.getElementById("qr");
                                        input.addEventListener("keyup", function (event) {
                                            if (event.keyCode === 13) {
                                                event.preventDefault();
                                                var tagInput = document.getElementsByTagName('input');
                                                var uid = sessionStorage.getItem('uid');
                                                if (uid === null) {
                                                    uid = '';
                                                }
                                                for (var i = 0; i < tagInput.length; i++) {
                                                    if (tagInput[i].name === "detHeight") {

                                                        $.ajax({
                                                            url: "/TABLE2/ShowDataTablesWMS165?mode=updateBagDet"
                                                                + "&code=" + $('#qrmcode').val()
                                                                + "&line=" + tagInput[i].id.toString().split('-')[0]
                                                                + "&qty=" + $('#' + tagInput[i].id.toString().split('-')[0] + '-detQty').val()
                                                                + "&height=" + parseFloat(tagInput[i].value)
                                                                + "&uid=" + uid
                                                        }).done(function (result) {
                                                            //                                                alert(result);
                                                        }).fail(function (jqXHR, textStatus, errorThrown) {
                                                            // needs to implement if it fails
                                                        });
                                                    }
                                                }

                                                var styl = document.getElementById("styl").value;
                                                var qrmid = document.getElementById("qr").value.toString().trim().split("|")[0];
                                                $.ajax({
                                                    url: "/TABLE2/ShowDataTablesWMS165?mode=getQRMMAS&qrmid=" + qrmid
                                                }).done(function (result) {
                                                    input.value = '';
                                                    if (styl.toString().trim() === result.QDMBSTYLE.toString().trim()) {
                                                        document.getElementById("qrmcode").value = result.QDMBSTYLE;
                                                        document.getElementById("qrmdesc").value = result.QDMBDESC;
                                                        document.getElementById("qrmbun").value = result.QDMBUNIT;
                                                        document.getElementById("styleNE").innerHTML = "";
                                                        var uid = sessionStorage.getItem('uid');
                                                        if (uid === null) {
                                                            uid = '';
                                                        }

                                                        var qrmcode = document.getElementById("qrmcode").value;
                                                        var qrmdesc = document.getElementById("qrmdesc").value;
                                                        var qrmbun = document.getElementById("qrmbun").value;
                                                        $.ajax({
                                                            url: "/TABLE2/ShowDataTablesWMS165?mode=checkHed"
                                                                + "&style=" + result.QDMBSTYLE
                                                        }).done(function (result) {
                                                            if (result === 't') {
                                                                $.ajax({
                                                                    url: "/TABLE2/ShowDataTablesWMS165?mode=addHed"
                                                                        + "&code=" + qrmcode
                                                                        + "&desc=" + qrmdesc
                                                                        + "&unit=" + qrmbun
                                                                        + "&uid=" + uid
                                                                }).done(function (result) {
                                                                    //                                                                                alert(result);
                                                                }).fail(function (jqXHR, textStatus, errorThrown) {
                                                                    // needs to implement if it fails
                                                                });
                                                            }
                                                        }).fail(function (jqXHR, textStatus, errorThrown) {
                                                            // needs to implement if it fails
                                                        });

                                                        $.ajax({
                                                            url: "/TABLE2/ShowDataTablesWMS165?mode=checkDet"
                                                                + "&qrmid=" + qrmid
                                                        }).done(function (res) {
                                                            if (res === "f") {
                                                                alertify.error("ไม่สามารถยิงวัตถุดิบซ้ำได้ !");
                                                                GetDataWMS165();
                                                                GetDataWMS165();
                                                                document.getElementById("qr").value = '';
                                                            } else {
                                                                $.ajax({
                                                                    url: "/TABLE2/ShowDataTablesWMS165?mode=addDet"
                                                                        + "&code=" + result.QDMBSTYLE + "&qty=" + result.QTY
                                                                        + "&uid=" + uid
                                                                        + "&qrmid=" + qrmid
                                                                }).done(function (result) {
                                                                    if (result) {
                                                                        GetDataWMS165();
                                                                        GetDataWMS165();
                                                                        document.getElementById("qr").value = '';
                                                                    }
                                                                }).fail(function (jqXHR, textStatus, errorThrown) {
                                                                    // needs to implement if it fails
                                                                });
                                                            }
                                                        }).fail(function (jqXHR, textStatus, errorThrown) {
                                                            // needs to implement if it fails
                                                        });
                                                    } else {
                                                        //                                                                    document.getElementById("qrmcode").value = "";
                                                        //                                                                    document.getElementById("qrmdesc").value = "";
                                                        //                                                                    document.getElementById("qrmbun").value = "";

                                                        document.getElementById("styleNE").innerHTML = "Style ไม่ตรงกัน";
                                                    }

                                                }).fail(function (jqXHR, textStatus, errorThrown) {
                                                    // needs to implement if it fails
                                                });
                                            } else {
                                                document.getElementById("qr").value = document.getElementById("qr").value.toUpperCase();
                                            }
                                        });</script>
                                </div>
                                <div class="col-sm-3" style=" text-align: left;">
                                    <i class="fa fa-qrcode" aria-hidden="true" style="font-size:30px;"></i>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-2" style=" text-align: right;">
                                    <b style="color: #00399b; font-size: 20px;">Style : </b>
                                </div>
                                <div class="col-sm-3">
                                    <input type="text" name="qrmcode" id="qrmcode" readonly>
                                    <p id="styleNE" style="color: #e51e00;"></p>
                                </div>
                                <div class="col-sm-3">
                                    <input type="text" name="qrmdesc" id="qrmdesc" readonly>
                                </div>
                                <div class="col-sm-2" style=" text-align: right;">
                                    <b style="color: #00399b; font-size: 20px;">U/M : </b>
                                </div>
                                <div class="col-sm-2">
                                    <input type="text" name="qrmbun" id="qrmbun" readonly>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-sm-6">
                                    <p id="detailNE" style="color: #e51e00;"></p>
                                    <table id="showTableWMS165" class="display"
                                        style="width:100%; border: 1px solid #ccc; border-radius: 4px;">
                                        <thead>
                                            <tr>
                                                <th style="text-align: center;">No.</th>
                                                <th style="text-align: center;">ปริมาณ</th>
                                                <th style="text-align: center;">ความสูง (CM)</th>
                                                <th style="text-align: center;"></th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th></th>
                                                <th style=" text-align: right;">
                                                    <input type="text" id="manAddQty"
                                                        style="width:100px; text-align: right;">
                                                    <script>
                                                        var input = document.getElementById("manAddQty");
                                                        input.addEventListener("keyup", function (event) {
                                                            if (event.keyCode === 13) {
                                                                event.preventDefault();
                                                                addBagDet();
                                                            }
                                                        });</script>
                                                </th>
                                                <th style=" text-align: left;">
                                                    <a title="Add" style=" cursor: pointer;" onclick="addBagDet
                                                                    ();"><i class="fa fa-plus" aria-hidden="true"
                                                            style="font-size:25px;"></i></a>
                                                    <script>
                                                        function addBagDet() {
                                                            var qrmcode = document.getElementById("qrmcode").value;
                                                            var manAddQty = document.getElementById("manAddQty").value;
                                                            $.ajax({
                                                                url: "/TABLE2/ShowDataTablesWMS165?mode=addDet"
                                                                    + "&code=" + qrmcode
                                                                    + "&qty=" + manAddQty
                                                                    + "&uid="
                                                            }).done(function (result) {
                                                                if (result) {
                                                                    GetDataWMS165();
                                                                    GetDataWMS165();
                                                                }
                                                            }).fail(function (jqXHR, textStatus, errorThrown) {
                                                                // needs to implement if it fails
                                                            });
                                                        }</script>
                                                    <!--<button type="button" class="btn btn-success">Equation Result</button>-->
                                                </th>
                                                <th></th>
                                            </tr>
                                        </tfoot>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-sm-6" style=" text-align: center;">
                                    <div class="row">
                                        <div class="col-sm-2">
                                        </div>
                                        <div class="col-sm-6">
                                            <b>
                                                <h4>Equation Result</h4>
                                            </b>
                                        </div>
                                        <div class="col-sm-2">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-2">
                                        </div>
                                        <div class="col-sm-6">
                                            <input type="text" id="equation" class="bg-readonly" readonly>
                                        </div>
                                        <div class="col-sm-2">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-2">
                                            <h4>x = </h4>
                                        </div>
                                        <div class="col-sm-6">
                                            <input type="text" id="valueX" style=" text-align: right;">
                                        </div>
                                        <div class="col-sm-2">
                                            <h4> M</h4>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-2">
                                            <h4>y = </h4>
                                        </div>
                                        <div class="col-sm-6">
                                            <input type="text" id="valueY"
                                                style=" text-align: right; background-color: lightgreen;" readonly>
                                        </div>
                                        <div class="col-sm-2">
                                            <h4> CM</h4>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-2">
                                        </div>
                                        <div class="col-sm-6">
                                            <i id="equaResIcon" class="fa fa-refresh hidden" aria-hidden="true"
                                                style="font-size:25px;"></i>
                                            &nbsp;&nbsp;&nbsp;
                                            <button type="button" class="btn btn-success" onclick="EquaRes()

                                                            ;">Equation Result</button>
                                        </div>
                                        <div class="col-sm-2">
                                            <button type="button" class="btn btn-primary" data-toggle="modal"
                                                data-target="#BAGCAL" onclick="confirmBagDet()

                                                                    ;">Confirm</button>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div style="width:90%; padding-top:15px;">
                                            <canvas id="canvas"></canvas>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-toggle="modal"
                                data-target="#BAGCAL">Close</button>
                        </div>
                    </div>

                </div>
            </div>
        </center>
        <center>
            <!-- Modal -->
            <div id="ROLLCAL" class="modal fade" role="dialog">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content" style="width: auto; height: auto;">
                        <div class="modal-header">
                            <h4 class="modal-title">
                                <div class="row">
                                    <div class="col-sm-4" style="text-align: left;">WMS170</div>
                                    <div class="col-sm-4">Standard Roll Dimension. Calculation</div>
                                    <div class="col-sm-4" id="timeROLLCAL" style="text-align: right;">TIME</div>
                                </div>
                            </h4>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-sm-2" style=" text-align: right;">
                                    <b style="color: #00399b; font-size: 20px;">QR Code : </b>
                                </div>
                                <div class="col-sm-7">
                                    <input type="text" name="qrRoll" id="qrRoll">
                                    <script>
                                        var input = document.getElementById("qrRoll");
                                        input.addEventListener("keyup", function (event) {
                                            if (event.keyCode === 13) {
                                                event.preventDefault();
                                                var tagInput = document.getElementsByTagName('input');
                                                var uid = sessionStorage.getItem('uid');
                                                if (uid === null) {
                                                    uid = '';
                                                }
                                                for (var i = 0; i < tagInput.length; i++) {
                                                    if (tagInput[i].name === "detHeightRoll") {

                                                        $.ajax({
                                                            url: "/TABLE2/ShowDataTablesWMS170?mode=updateRollDet"
                                                                + "&code=" + $('#qrmcodeRoll').val()
                                                                + "&line=" + tagInput[i].id.toString().split('-')[0]
                                                                + "&length=" + $('#' + tagInput[i].id.toString().split('-')[0] + '-detLength1').val()
                                                                + "&height=" + parseFloat(tagInput[i].value)
                                                                + "&uid=" + uid
                                                        }).done(function (result) {
                                                            //                                                alert(result);
                                                        }).fail(function (jqXHR, textStatus, errorThrown) {
                                                            // needs to implement if it fails
                                                        });
                                                    }
                                                }

                                                var styl = document.getElementById("styl").value;
                                                var qrmid = document.getElementById("qrRoll").value.toString().trim().split("|")[0];
                                                $.ajax({
                                                    url: "/TABLE2/ShowDataTablesWMS170?mode=getQRMMAS&qrmid=" + qrmid
                                                }).done(function (result) {
                                                    input.value = '';
                                                    if (styl.toString().trim() === result.QDMRSTYLE.toString().trim()) {
                                                        document.getElementById("qrmcodeRoll").value = result.QDMRSTYLE;
                                                        document.getElementById("qrmdescRoll").value = result.QDMRDESC;
                                                        document.getElementById("qrmbunRoll").value = result.QDMRUNIT;
                                                        document.getElementById("styleNERoll").innerHTML = "";
                                                        var uid = sessionStorage.getItem('uid');
                                                        if (uid === null) {
                                                            uid = '';
                                                        }

                                                        var qrmcode = document.getElementById("qrmcodeRoll").value;
                                                        var qrmdesc = document.getElementById("qrmdescRoll").value;
                                                        var qrmbun = document.getElementById("qrmbunRoll").value;
                                                        $.ajax({
                                                            url: "/TABLE2/ShowDataTablesWMS170?mode=checkHed"
                                                                + "&style=" + result.QDMRSTYLE
                                                        }).done(function (result) {
                                                            if (result === 't') {
                                                                $.ajax({
                                                                    url: "/TABLE2/ShowDataTablesWMS170?mode=addHed"
                                                                        + "&code=" + qrmcode
                                                                        + "&desc=" + qrmdesc
                                                                        + "&unit=" + qrmbun
                                                                        + "&uid=" + uid
                                                                }).done(function (result) {
                                                                    //                                                                                alert(result);
                                                                }).fail(function (jqXHR, textStatus, errorThrown) {
                                                                    // needs to implement if it fails
                                                                });
                                                            }
                                                        }).fail(function (jqXHR, textStatus, errorThrown) {
                                                            // needs to implement if it fails
                                                        });

                                                        $.ajax({
                                                            url: "/TABLE2/ShowDataTablesWMS170?mode=checkDet"
                                                                + "&qrmid=" + qrmid
                                                        }).done(function (res) {
                                                            if (res === "f") {
                                                                alertify.error("ไม่สามารถยิงวัตถุดิบซ้ำได้ !");
                                                                GetDataWMS170();
                                                                GetDataWMS170();
                                                                document.getElementById("qrRoll").value = '';
                                                            } else {
                                                                $.ajax({
                                                                    url: "/TABLE2/ShowDataTablesWMS170?mode=addDet"
                                                                        + "&code=" + result.QDMRSTYLE + "&length=" + result.QTY
                                                                        + "&uid=" + uid
                                                                        + "&qrmid=" + qrmid
                                                                }).done(function (result) {
                                                                    if (result) {
                                                                        GetDataWMS170();
                                                                        GetDataWMS170();
                                                                        document.getElementById("qrRoll").value = '';
                                                                    }
                                                                }).fail(function (jqXHR, textStatus, errorThrown) {
                                                                    // needs to implement if it fails
                                                                });
                                                            }
                                                        }).fail(function (jqXHR, textStatus, errorThrown) {
                                                            // needs to implement if it fails
                                                        });
                                                    } else {
                                                        //                                                                    document.getElementById("qrmcode").value = "";
                                                        //                                                                    document.getElementById("qrmdesc").value = "";
                                                        //                                                                    document.getElementById("qrmbun").value = "";

                                                        document.getElementById("styleNERoll").innerHTML = "Style ไม่ตรงกัน";
                                                    }

                                                }).fail(function (jqXHR, textStatus, errorThrown) {
                                                    // needs to implement if it fails
                                                });
                                            } else {
                                                document.getElementById("qrRoll").value = document.getElementById("qrRoll").value.toUpperCase();
                                            }
                                        });</script>
                                </div>
                                <div class="col-sm-3" style=" text-align: left;">
                                    <i class="fa fa-qrcode" aria-hidden="true" style="font-size:30px;"></i>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-2" style=" text-align: right;">
                                    <b style="color: #00399b; font-size: 20px;">Style : </b>
                                </div>
                                <div class="col-sm-3">
                                    <input type="text" name="qrmcodeRoll" id="qrmcodeRoll" readonly>
                                    <p id="styleNERoll" style="color: #e51e00;"></p>
                                </div>
                                <div class="col-sm-3">
                                    <input type="text" name="qrmdescRoll" id="qrmdescRoll" readonly>
                                </div>
                                <div class="col-sm-2" style=" text-align: right;">
                                    <b style="color: #00399b; font-size: 20px;">U/M : </b>
                                </div>
                                <div class="col-sm-2">
                                    <input type="text" name="qrmbunRoll" id="qrmbunRoll" readonly>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-sm-6">
                                    <p id="detailNERoll" style="color: #e51e00;"></p>
                                    <table id="showTableWMS170" class="display"
                                        style="width:100%; border: 1px solid #ccc; border-radius: 4px;">
                                        <thead>
                                            <tr>
                                                <th style="text-align: center;">No.</th>
                                                <th style="text-align: center;">ความยาว (M)</th>
                                                <th style="text-align: center;">ความยาว (CM)</th>
                                                <th style="text-align: center;">ความหนา (CM)</th>
                                                <th style="text-align: center;"></th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th></th>
                                                <th style=" text-align: right;">
                                                    <input type="text" id="manAddLength"
                                                        style="width:100px; text-align: right;">
                                                    <script>
                                                        var input = document.getElementById("manAddLength");
                                                        input.addEventListener("keyup", function (event) {
                                                            if (event.keyCode === 13) {
                                                                event.preventDefault();
                                                                addRollDet();
                                                            }
                                                        });</script>
                                                </th>
                                                <th style=" text-align: left;">
                                                    <a title="Add" style=" cursor: pointer;" onclick="addRollDet
                                                                    ()

                                                                    ;"><i class="fa fa-plus" aria-hidden="true"
                                                            style="font-size:25px;"></i></a>
                                                    <script>
                                                        function addRollDet() {
                                                            var qrmcode = document.getElementById("qrmcodeRoll").value;
                                                            var manAddLength = document.getElementById("manAddLength").value;
                                                            $.ajax({
                                                                url: "/TABLE2/ShowDataTablesWMS170?mode=addDet"
                                                                    + "&code=" + qrmcode
                                                                    + "&length=" + manAddLength
                                                                    + "&uid="
                                                            }).done(function (result) {
                                                                if (result) {
                                                                    GetDataWMS170();
                                                                    GetDataWMS170();
                                                                }
                                                            }).fail(function (jqXHR, textStatus, errorThrown) {
                                                                // needs to implement if it fails
                                                            });
                                                        }</script>
                                                    <!--<button type="button" class="btn btn-success">Equation Result</button>-->
                                                </th>
                                                <th></th>
                                            </tr>
                                        </tfoot>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-sm-6" style=" text-align: center;">
                                    <div class="row">
                                        <div class="col-sm-2">
                                        </div>
                                        <div class="col-sm-6">
                                            <b>
                                                <h4>Equation Result</h4>
                                            </b>
                                        </div>
                                        <div class="col-sm-2">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-2">
                                        </div>
                                        <div class="col-sm-6">
                                            <input type="text" id="equationRoll" class="bg-readonly" readonly>
                                        </div>
                                        <div class="col-sm-2">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-2">
                                            <h4>x = </h4>
                                        </div>
                                        <div class="col-sm-6">
                                            <input type="text" id="valueXRoll" style=" text-align: right;">
                                        </div>
                                        <div class="col-sm-2">
                                            <h4> M</h4>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-2">
                                            <h4>y = </h4>
                                        </div>
                                        <div class="col-sm-6">
                                            <input type="text" id="valueYRoll"
                                                style=" text-align: right; background-color: lightgreen;" readonly>
                                        </div>
                                        <div class="col-sm-2">
                                            <h4> CM</h4>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-2">
                                        </div>
                                        <div class="col-sm-6">
                                            <i id="equaResIconRoll" class="fa fa-refresh hidden" aria-hidden="true"
                                                style="font-size:25px;"></i>
                                            &nbsp;&nbsp;&nbsp;
                                            <button type="button" class="btn btn-success" onclick="EquaResRoll()


                                                            ;">Equation Result</button>
                                        </div>
                                        <div class="col-sm-2">
                                            <button type="button" class="btn btn-primary" data-toggle="modal"
                                                data-target="#ROLLCAL" onclick="confirmRollDet()



                                                                    ;">Confirm</button>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div style="width:90%; padding-top:15px;">
                                            <canvas id="canvas170"></canvas>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-toggle="modal"
                                data-target="#ROLLCAL">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </center>
        <center>
            <!-- Modal -->
            <div id="EQUCAL" class="modal fade" role="dialog">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content" style="width: auto; height: auto;">
                        <div class="modal-header">
                            <h4 class="modal-title">
                                <div class="row">
                                    <div class="col-sm-4" style="text-align: left;"></div>
                                    <div class="col-sm-4">Equation Calculation</div>
                                    <div class="col-sm-4" style="text-align: right;"></div>
                                </div>
                            </h4>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-sm-1"></div>
                                <div class="col-sm-8" style="border-right: 2px solid;">
                                    <h4>
                                        เส้นผ่าศูนย์กลาง(cm)&nbsp;&nbsp; = &nbsp;&nbsp;ความชัน &nbsp;x&nbsp;
                                        ปริมาณความยาวผ้า(cm) &nbsp;+&nbsp; ขนาดแกนผ้า(cm)
                                        <br><br>
                                        <input type="text" id="ecDiameter" style="width: 170px; text-align: right;">
                                        &nbsp;&nbsp;=&nbsp;&nbsp; ความชัน &nbsp;x&nbsp;
                                        <input type="text" id="ecQty" style="width: 170px; text-align: right;" disabled>
                                        &nbsp;+&nbsp;
                                        <input type="text" id="ecB" style="width: 170px; text-align: right;">
                                    </h4>
                                    <br>
                                    <center>
                                        <button type="button" style="width: 7em;" class="btn btn-danger"
                                            data-toggle="modal" data-target="#EQUCAL"> Back </button>
                                        <button type="button" style="width: 7em;" class="btn btn-primary"
                                            onclick="clearEcData();"> Clear </button>
                                        <button type="button" style="width: 7em;" class="btn btn-success" onclick="confirmEcData()
                                                        ;" data-toggle="modal" data-target="#EQUCAL"> Confirm </button>
                                    </center>
                                    <script>
                                        function initEcData() {
                                            var dia = $('#D_DIAMETER').val();
                                            if (parseFloat(dia) <= 0) {
                                                dia = '';
                                            }
                                            $('#ecDiameter').val(dia);
                                            $('#ecQty').val(parseFloat(parseFloat($('#qty').val()).toFixed(3) * 100).toFixed(3));

                                            var b = $('#equationN').val().split("+")[1];
                                            if (parseFloat(b) <= 0) {
                                                b = '';
                                            }
                                            $('#ecB').val(b);

                                            var widt = $('#widt').val();
                                            if (parseFloat(widt) <= 0) {
                                                widt = '';
                                            }
                                            $('#ecWidth').val(widt);
                                        }

                                        function clearEcData() {
                                            $('#ecDiameter').val('');
                                            $('#ecQty').val('');
                                            $('#ecB').val('');
                                            $('#ecWidth').val('');
                                        }

                                        function confirmEcData() {

                                            var ecDiameter = parseFloat($('#ecDiameter').val());
                                            var ecQty = parseFloat($('#ecQty').val());
                                            var ecB = parseFloat($('#ecB').val());
                                            var slope = (ecDiameter - ecB) / ecQty;
                                            var m = parseFloat(slope).toFixed(4);
                                            var b = parseFloat($('#ecB').val()).toFixed(4);

                                            $('#equationN').val('y = ' + m + 'x + ' + b);
                                            $('#D_DIAMETER').val(parseFloat($('#ecDiameter').val()).toFixed(2));
                                            $('#widt').val(parseFloat($('#ecWidth').val()).toFixed(2));
                                        }</script>
                                </div>
                                <div class="col-sm-2">
                                    <h4>
                                        หน้ากว้าง
                                        <br><br>
                                        <input type="text" id="ecWidth" style="width: 150px; text-align: right;">
                                    </h4>
                                </div>
                                <div class="col-sm-1"></div>
                            </div>
                            <br>
                        </div>
                    </div>
                </div>
            </div>
        </center>
        <center>
            <!-- Modal -->
            <div id="EQUCALBAG" class="modal fade" role="dialog">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content" style="width: auto; height: auto;">
                        <div class="modal-header">
                            <h4 class="modal-title">
                                <div class="row">
                                    <div class="col-sm-4" style="text-align: left;"></div>
                                    <div class="col-sm-4">Equation Calculation</div>
                                    <div class="col-sm-4" style="text-align: right;"></div>
                                </div>
                            </h4>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-sm-1"></div>
                                <div class="col-sm-10">
                                    <h4>
                                        ความสูง(cm)&nbsp;&nbsp; = &nbsp;&nbsp;ความชัน &nbsp;x&nbsp; ปริมาณวัตถุดิบ
                                        &nbsp;+&nbsp; ค่าคงที่(cm)
                                        <br><br>
                                        <input type="text" id="ecHeight" style="width: 170px; text-align: right;">
                                        &nbsp;&nbsp;=&nbsp;&nbsp; ความชัน &nbsp;x&nbsp;
                                        <input type="text" id="ecQtyBag" style="width: 170px; text-align: right;"
                                            disabled>
                                        &nbsp;+&nbsp;
                                        <input type="text" id="ecBBag" style="width: 170px; text-align: right;"
                                            disabled>
                                    </h4>
                                    <br>
                                    <center>
                                        <button type="button" style="width: 7em;" class="btn btn-danger"
                                            data-toggle="modal" data-target="#EQUCALBAG"> Back </button>
                                        <button type="button" style="width: 7em;" class="btn btn-primary" onclick="clearEcDataBag()

                                                        ;"> Clear </button>
                                        <button type="button" style="width: 7em;" class="btn btn-success" onclick="confirmEcDataBag()

                                                        ;" data-toggle="modal" data-target="#EQUCALBAG"> Confirm
                                        </button>
                                    </center>
                                    <script>
                                        function initEcDataBag() {
                                            var height = $('#D_HEIGHT').val();
                                            if (parseFloat(height) <= 0) {
                                                height = '';
                                            }
                                            $('#ecHeight').val(height);
                                            $('#ecQtyBag').val(parseFloat($('#qty').val()).toFixed(3));

                                            $('#ecBBag').val('0');
                                        }

                                        function clearEcDataBag() {
                                            $('#ecHeight').val('');
                                            //                                            $('#ecQtyBag').val('');
                                            //                                            $('#ecBBag').val('');
                                        }

                                        function confirmEcDataBag() {

                                            var ecHeight = parseFloat($('#ecHeight').val());
                                            var ecQtyBag = parseFloat($('#ecQtyBag').val());
                                            var ecBBag = parseFloat($('#ecBBag').val());
                                            var slope = (ecHeight - ecBBag) / ecQtyBag;
                                            var m = parseFloat(slope).toFixed(4);
                                            var b = parseFloat($('#ecBBag').val()).toFixed(4);

                                            $('#equationN').val('y = ' + m + 'x + ' + b);
                                            $('#D_HEIGHT').val(parseFloat($('#ecHeight').val()).toFixed(2));
                                        }</script>
                                </div>
                                <div class="col-sm-1"></div>
                            </div>
                            <br>
                        </div>
                    </div>
                </div>
            </div>
        </center>
        <br>
        <!--1FSS005 , 1FPA7861-->
        <input name="qty" id="qty" value="71.043">
        <input type="button" data-toggle="modal" data-target="#EQUCAL" onclick="initEcData()

                        ;" value="1" style="width: 40px; border-radius: 50%;">
        <input type="button" data-toggle="modal" data-target="#EQUCALBAG" onclick="initEcDataBag()
                        ;" value="1" style="width: 40px; border-radius: 50%;">
        <br>
        style : <input type="text" id="styl" value="XXTESTXX">
        width : <input type="text" id="D_WIDTH" data-toggle="modal" data-target="#BAGMAS">
        length : <input type="text" id="D_LENGTH" data-toggle="modal" data-target="#BAGMAS">
        height : <input type="text" id="D_HEIGHT" data-toggle="modal" data-target="#BAGCAL" onclick="GetDataWMS165()


                        ;">
        equation : <input type="text" id="equationN" value="+0.0000">
        diameter : <input type="text" id="D_DIAMETER" value="0.00" data-toggle="modal" data-target="#ROLLCAL" onclick="GetDataWMS170()

                        ;">
        widt : <input type="text" id="widt" value="0.00">
        num of Images : <input type="text" id="NOIMG" onclick="GetDataWMS001($('#styl').val())


                        ;">
        <span class="qrmcode-fim">1FCO0892BL</span>
        <i class="fa fa-qrcode imgOf1FCO0892BL"></i>
        <span class="qrmcode-fim">1FCO0892HH</span>
        <i class="fa fa-qrcode imgOf1FCO0892HH"></i>
        <span class="qrmcode-fim">1FCO0892ER</span>
        <i class="fa fa-qrcode imgOf1FCO0892ER"></i>

        <table id="example" class="display tableWithImg" style="width:100%">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Position</th>
                    <th>Office</th>
                    <th>Age</th>
                    <th>Start date</th>
                    <th>Salary</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>Tiger Nixon</td>
                    <td>System Architect</td>
                    <td>Edinburgh</td>
                    <td>61</td>
                    <td>2011/04/25</td>
                    <td>$320,800</td>
                </tr>
                <tr>
                    <td>Garrett Winters</td>
                    <td>Accountant</td>
                    <td>Tokyo</td>
                    <td>63</td>
                    <td>2011/07/25</td>
                    <td>$170,750</td>
                </tr>
                <tr>
                    <td>Ashton Cox</td>
                    <td>Junior Technical Author</td>
                    <td>San Francisco</td>
                    <td>66</td>
                    <td>2009/01/12</td>
                    <td>$86,000</td>
                </tr>
                <tr>
                    <td>Cedric Kelly</td>
                    <td>Senior Javascript Developer</td>
                    <td>Edinburgh</td>
                    <td>22</td>
                    <td>2012/03/29</td>
                    <td>$433,060</td>
                </tr>
                <tr>
                    <td>Airi Satou</td>
                    <td>Accountant</td>
                    <td>Tokyo</td>
                    <td>33</td>
                    <td>2008/11/28</td>
                    <td>$162,700</td>
                </tr>
                <tr>
                    <td>Brielle Williamson</td>
                    <td>Integration Specialist</td>
                    <td>New York</td>
                    <td>61</td>
                    <td>2012/12/02</td>
                    <td>$372,000</td>
                </tr>
                <tr>
                    <td>Herrod Chandler</td>
                    <td>Sales Assistant</td>
                    <td>San Francisco</td>
                    <td>59</td>
                    <td>2012/08/06</td>
                    <td>$137,500</td>
                </tr>
                <tr>
                    <td>Rhona Davidson</td>
                    <td>Integration Specialist</td>
                    <td>Tokyo</td>
                    <td>55</td>
                    <td>2010/10/14</td>
                    <td>$327,900</td>
                </tr>
                <tr>
                    <td>Colleen Hurst</td>
                    <td>Javascript Developer</td>
                    <td>San Francisco</td>
                    <td>39</td>
                    <td>2009/09/15</td>
                    <td>$205,500</td>
                </tr>
                <tr>
                    <td>Sonya Frost</td>
                    <td>Software Engineer</td>
                    <td>Edinburgh</td>
                    <td>23</td>
                    <td>2008/12/13</td>
                    <td>$103,600</td>
                </tr>
                <tr>
                    <td>Jena Gaines</td>
                    <td>Office Manager</td>
                    <td>London</td>
                    <td>30</td>
                    <td>2008/12/19</td>
                    <td>$90,560</td>
                </tr>
                <tr>
                    <td>Quinn Flynn</td>
                    <td>Support Lead</td>
                    <td>Edinburgh</td>
                    <td>22</td>
                    <td>2013/03/03</td>
                    <td>$342,000</td>
                </tr>
                <tr>
                    <td>Charde Marshall</td>
                    <td>Regional Director</td>
                    <td>San Francisco</td>
                    <td>36</td>
                    <td>2008/10/16</td>
                    <td>$470,600</td>
                </tr>
                <tr>
                    <td>Haley Kennedy</td>
                    <td>Senior Marketing Designer</td>
                    <td>London</td>
                    <td>43</td>
                    <td>2012/12/18</td>
                    <td>$313,500</td>
                </tr>
                <tr>
                    <td>Tatyana Fitzpatrick</td>
                    <td>Regional Director</td>
                    <td>London</td>
                    <td>19</td>
                    <td>2010/03/17</td>
                    <td>$385,750</td>
                </tr>
                <tr>
                    <td>Michael Silva</td>
                    <td>Marketing Designer</td>
                    <td>London</td>
                    <td>66</td>
                    <td>2012/11/27</td>
                    <td>$198,500</td>
                </tr>
                <tr>
                    <td>Paul Byrd</td>
                    <td>Chief Financial Officer (CFO)</td>
                    <td>New York</td>
                    <td>64</td>
                    <td>2010/06/09</td>
                    <td>$725,000</td>
                </tr>
                <tr>
                    <td>Gloria Little</td>
                    <td>Systems Administrator</td>
                    <td>New York</td>
                    <td>59</td>
                    <td>2009/04/10</td>
                    <td>$237,500</td>
                </tr>
                <tr>
                    <td>Bradley Greer</td>
                    <td>Software Engineer</td>
                    <td>London</td>
                    <td>41</td>
                    <td>2012/10/13</td>
                    <td>$132,000</td>
                </tr>
                <tr>
                    <td>Dai Rios</td>
                    <td>Personnel Lead</td>
                    <td>Edinburgh</td>
                    <td>35</td>
                    <td>2012/09/26</td>
                    <td>$217,500</td>
                </tr>
                <tr>
                    <td>Jenette Caldwell</td>
                    <td>Development Lead</td>
                    <td>New York</td>
                    <td>30</td>
                    <td>2011/09/03</td>
                    <td>$345,000</td>
                </tr>
                <tr>
                    <td>Yuri Berry</td>
                    <td>Chief Marketing Officer (CMO)</td>
                    <td>New York</td>
                    <td>40</td>
                    <td>2009/06/25</td>
                    <td>$675,000</td>
                </tr>
                <tr>
                    <td>Caesar Vance</td>
                    <td>Pre-Sales Support</td>
                    <td>New York</td>
                    <td>21</td>
                    <td>2011/12/12</td>
                    <td>$106,450</td>
                </tr>
                <tr>
                    <td>Doris Wilder</td>
                    <td>Sales Assistant</td>
                    <td>Sydney</td>
                    <td>23</td>
                    <td>2010/09/20</td>
                    <td>$85,600</td>
                </tr>
                <tr>
                    <td>Angelica Ramos</td>
                    <td>Chief Executive Officer (CEO)</td>
                    <td>London</td>
                    <td>47</td>
                    <td>2009/10/09</td>
                    <td>$1,200,000</td>
                </tr>
                <tr>
                    <td>Gavin Joyce</td>
                    <td>Developer</td>
                    <td>Edinburgh</td>
                    <td>42</td>
                    <td>2010/12/22</td>
                    <td>$92,575</td>
                </tr>
                <tr>
                    <td>Jennifer Chang</td>
                    <td>Regional Director</td>
                    <td>Singapore</td>
                    <td>28</td>
                    <td>2010/11/14</td>
                    <td>$357,650</td>
                </tr>
                <tr>
                    <td>Brenden Wagner</td>
                    <td>Software Engineer</td>
                    <td>San Francisco</td>
                    <td>28</td>
                    <td>2011/06/07</td>
                    <td>$206,850</td>
                </tr>
                <tr>
                    <td>Fiona Green</td>
                    <td>Chief Operating Officer (COO)</td>
                    <td>San Francisco</td>
                    <td>48</td>
                    <td>2010/03/11</td>
                    <td>$850,000</td>
                </tr>
                <tr>
                    <td>Shou Itou</td>
                    <td>Regional Marketing</td>
                    <td>Tokyo</td>
                    <td>20</td>
                    <td>2011/08/14</td>
                    <td>$163,000</td>
                </tr>
                <tr>
                    <td>Michelle House</td>
                    <td>Integration Specialist</td>
                    <td>Sydney</td>
                    <td>37</td>
                    <td>2011/06/02</td>
                    <td>$95,400</td>
                </tr>
                <tr>
                    <td>Suki Burks</td>
                    <td>Developer</td>
                    <td>London</td>
                    <td>53</td>
                    <td>2009/10/22</td>
                    <td>$114,500</td>
                </tr>
                <tr>
                    <td>Prescott Bartlett</td>
                    <td>Technical Author</td>
                    <td>London</td>
                    <td>27</td>
                    <td>2011/05/07</td>
                    <td>$145,000</td>
                </tr>
                <tr>
                    <td>Gavin Cortez</td>
                    <td>Team Leader</td>
                    <td>San Francisco</td>
                    <td>22</td>
                    <td>2008/10/26</td>
                    <td>$235,500</td>
                </tr>
                <tr>
                    <td>Martena Mccray</td>
                    <td>Post-Sales support</td>
                    <td>Edinburgh</td>
                    <td>46</td>
                    <td>2011/03/09</td>
                    <td>$324,050</td>
                </tr>
                <tr>
                    <td>Unity Butler</td>
                    <td>Marketing Designer</td>
                    <td>San Francisco</td>
                    <td>47</td>
                    <td>2009/12/09</td>
                    <td>$85,675</td>
                </tr>
                <tr>
                    <td>Howard Hatfield</td>
                    <td>Office Manager</td>
                    <td>San Francisco</td>
                    <td>51</td>
                    <td>2008/12/16</td>
                    <td>$164,500</td>
                </tr>
                <tr>
                    <td>Hope Fuentes</td>
                    <td>Secretary</td>
                    <td>San Francisco</td>
                    <td>41</td>
                    <td>2010/02/12</td>
                    <td>$109,850</td>
                </tr>
                <tr>
                    <td>Vivian Harrell</td>
                    <td>Financial Controller</td>
                    <td>San Francisco</td>
                    <td>62</td>
                    <td>2009/02/14</td>
                    <td>$452,500</td>
                </tr>
                <tr>
                    <td>Timothy Mooney</td>
                    <td>Office Manager</td>
                    <td>London</td>
                    <td>37</td>
                    <td>2008/12/11</td>
                    <td>$136,200</td>
                </tr>
                <tr>
                    <td>Jackson Bradshaw</td>
                    <td>Director</td>
                    <td>New York</td>
                    <td>65</td>
                    <td>2008/09/26</td>
                    <td>$645,750</td>
                </tr>
                <tr>
                    <td>Olivia Liang</td>
                    <td>Support Engineer</td>
                    <td>Singapore</td>
                    <td>64</td>
                    <td>2011/02/03</td>
                    <td>$234,500</td>
                </tr>
                <tr>
                    <td>Bruno Nash</td>
                    <td>Software Engineer</td>
                    <td>London</td>
                    <td>38</td>
                    <td>2011/05/03</td>
                    <td>$163,500</td>
                </tr>
                <tr>
                    <td>Sakura Yamamoto</td>
                    <td>Support Engineer</td>
                    <td>Tokyo</td>
                    <td>37</td>
                    <td>2009/08/19</td>
                    <td>$139,575</td>
                </tr>
                <tr>
                    <td>Thor Walton</td>
                    <td>Developer</td>
                    <td>New York</td>
                    <td>61</td>
                    <td>2013/08/11</td>
                    <td>$98,540</td>
                </tr>
                <tr>
                    <td>Finn Camacho</td>
                    <td>Support Engineer</td>
                    <td>San Francisco</td>
                    <td>47</td>
                    <td>2009/07/07</td>
                    <td>$87,500</td>
                </tr>
                <tr>
                    <td>Serge Baldwin</td>
                    <td>Data Coordinator</td>
                    <td>Singapore</td>
                    <td>64</td>
                    <td>2012/04/09</td>
                    <td>$138,575</td>
                </tr>
                <tr>
                    <td>Zenaida Frank</td>
                    <td>Software Engineer</td>
                    <td>New York</td>
                    <td>63</td>
                    <td>2010/01/04</td>
                    <td>$125,250</td>
                </tr>
                <tr>
                    <td>Zorita Serrano</td>
                    <td>Software Engineer</td>
                    <td>San Francisco</td>
                    <td>56</td>
                    <td>2012/06/01</td>
                    <td>$115,000</td>
                </tr>
                <tr>
                    <td>Jennifer Acosta</td>
                    <td>Junior Javascript Developer</td>
                    <td>Edinburgh</td>
                    <td>43</td>
                    <td>2013/02/01</td>
                    <td>$75,650</td>
                </tr>
                <tr>
                    <td>Cara Stevens</td>
                    <td>Sales Assistant</td>
                    <td>New York</td>
                    <td>46</td>
                    <td>2011/12/06</td>
                    <td>$145,600</td>
                </tr>
                <tr>
                    <td>Hermione Butler</td>
                    <td>Regional Director</td>
                    <td>London</td>
                    <td>47</td>
                    <td>2011/03/21</td>
                    <td>$356,250</td>
                </tr>
                <tr>
                    <td>Lael Greer</td>
                    <td>Systems Administrator</td>
                    <td>London</td>
                    <td>21</td>
                    <td>2009/02/27</td>
                    <td>$103,500</td>
                </tr>
                <tr>
                    <td>Jonas Alexander</td>
                    <td>Developer</td>
                    <td>San Francisco</td>
                    <td>30</td>
                    <td>2010/07/14</td>
                    <td>$86,500</td>
                </tr>
                <tr>
                    <td>Shad Decker</td>
                    <td>Regional Director</td>
                    <td>Edinburgh</td>
                    <td>51</td>
                    <td>2008/11/13</td>
                    <td>$183,000</td>
                </tr>
                <tr>
                    <td>Michael Bruce</td>
                    <td>Javascript Developer</td>
                    <td>Singapore</td>
                    <td>29</td>
                    <td>2011/06/27</td>
                    <td>$183,000</td>
                </tr>
                <tr>
                    <td>Donna Snider</td>
                    <td>Customer Support</td>
                    <td>New York</td>
                    <td>27</td>
                    <td>2011/01/25</td>
                    <td>$112,000</td>
                </tr>
            </tbody>
            <tfoot>
                <tr>
                    <th>Name</th>
                    <th>Position</th>
                    <th>Office</th>
                    <th>Age</th>
                    <th>Start date</th>
                    <th>Salary</th>
                </tr>
            </tfoot>
        </table>

        <table id="example2" class="display tableWithImg" style="width:100%">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Position</th>
                    <th>Office</th>
                    <th>Age</th>
                    <th>Start date</th>
                    <th>Salary</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>Tiger Nixon</td>
                    <td>System Architect</td>
                    <td>Edinburgh</td>
                    <td>61</td>
                    <td>2011/04/25</td>
                    <td>$320,800</td>
                </tr>
                <tr>
                    <td>Garrett Winters</td>
                    <td>Accountant</td>
                    <td>Tokyo</td>
                    <td>63</td>
                    <td>2011/07/25</td>
                    <td>$170,750</td>
                </tr>
                <tr>
                    <td>Ashton Cox</td>
                    <td>Junior Technical Author</td>
                    <td>San Francisco</td>
                    <td>66</td>
                    <td>2009/01/12</td>
                    <td>$86,000</td>
                </tr>
                <tr>
                    <td>Cedric Kelly</td>
                    <td>Senior Javascript Developer</td>
                    <td>Edinburgh</td>
                    <td>22</td>
                    <td>2012/03/29</td>
                    <td>$433,060</td>
                </tr>
                <tr>
                    <td>Airi Satou</td>
                    <td>Accountant</td>
                    <td>Tokyo</td>
                    <td>33</td>
                    <td>2008/11/28</td>
                    <td>$162,700</td>
                </tr>
                <tr>
                    <td>Brielle Williamson</td>
                    <td>Integration Specialist</td>
                    <td>New York</td>
                    <td>61</td>
                    <td>2012/12/02</td>
                    <td>$372,000</td>
                </tr>
                <tr>
                    <td>Herrod Chandler</td>
                    <td>Sales Assistant</td>
                    <td>San Francisco</td>
                    <td>59</td>
                    <td>2012/08/06</td>
                    <td>$137,500</td>
                </tr>
                <tr>
                    <td>Rhona Davidson</td>
                    <td>Integration Specialist</td>
                    <td>Tokyo</td>
                    <td>55</td>
                    <td>2010/10/14</td>
                    <td>$327,900</td>
                </tr>
                <tr>
                    <td>Colleen Hurst</td>
                    <td>Javascript Developer</td>
                    <td>San Francisco</td>
                    <td>39</td>
                    <td>2009/09/15</td>
                    <td>$205,500</td>
                </tr>
                <tr>
                    <td>Sonya Frost</td>
                    <td>Software Engineer</td>
                    <td>Edinburgh</td>
                    <td>23</td>
                    <td>2008/12/13</td>
                    <td>$103,600</td>
                </tr>
                <tr>
                    <td>Jena Gaines</td>
                    <td>Office Manager</td>
                    <td>London</td>
                    <td>30</td>
                    <td>2008/12/19</td>
                    <td>$90,560</td>
                </tr>
                <tr>
                    <td>Quinn Flynn</td>
                    <td>Support Lead</td>
                    <td>Edinburgh</td>
                    <td>22</td>
                    <td>2013/03/03</td>
                    <td>$342,000</td>
                </tr>
                <tr>
                    <td>Charde Marshall</td>
                    <td>Regional Director</td>
                    <td>San Francisco</td>
                    <td>36</td>
                    <td>2008/10/16</td>
                    <td>$470,600</td>
                </tr>
                <tr>
                    <td>Haley Kennedy</td>
                    <td>Senior Marketing Designer</td>
                    <td>London</td>
                    <td>43</td>
                    <td>2012/12/18</td>
                    <td>$313,500</td>
                </tr>
                <tr>
                    <td>Tatyana Fitzpatrick</td>
                    <td>Regional Director</td>
                    <td>London</td>
                    <td>19</td>
                    <td>2010/03/17</td>
                    <td>$385,750</td>
                </tr>
                <tr>
                    <td>Michael Silva</td>
                    <td>Marketing Designer</td>
                    <td>London</td>
                    <td>66</td>
                    <td>2012/11/27</td>
                    <td>$198,500</td>
                </tr>
                <tr>
                    <td>Paul Byrd</td>
                    <td>Chief Financial Officer (CFO)</td>
                    <td>New York</td>
                    <td>64</td>
                    <td>2010/06/09</td>
                    <td>$725,000</td>
                </tr>
                <tr>
                    <td>Gloria Little</td>
                    <td>Systems Administrator</td>
                    <td>New York</td>
                    <td>59</td>
                    <td>2009/04/10</td>
                    <td>$237,500</td>
                </tr>
                <tr>
                    <td>Bradley Greer</td>
                    <td>Software Engineer</td>
                    <td>London</td>
                    <td>41</td>
                    <td>2012/10/13</td>
                    <td>$132,000</td>
                </tr>
                <tr>
                    <td>Dai Rios</td>
                    <td>Personnel Lead</td>
                    <td>Edinburgh</td>
                    <td>35</td>
                    <td>2012/09/26</td>
                    <td>$217,500</td>
                </tr>
                <tr>
                    <td>Jenette Caldwell</td>
                    <td>Development Lead</td>
                    <td>New York</td>
                    <td>30</td>
                    <td>2011/09/03</td>
                    <td>$345,000</td>
                </tr>
                <tr>
                    <td>Yuri Berry</td>
                    <td>Chief Marketing Officer (CMO)</td>
                    <td>New York</td>
                    <td>40</td>
                    <td>2009/06/25</td>
                    <td>$675,000</td>
                </tr>
                <tr>
                    <td>Caesar Vance</td>
                    <td>Pre-Sales Support</td>
                    <td>New York</td>
                    <td>21</td>
                    <td>2011/12/12</td>
                    <td>$106,450</td>
                </tr>
                <tr>
                    <td>Doris Wilder</td>
                    <td>Sales Assistant</td>
                    <td>Sydney</td>
                    <td>23</td>
                    <td>2010/09/20</td>
                    <td>$85,600</td>
                </tr>
                <tr>
                    <td>Angelica Ramos</td>
                    <td>Chief Executive Officer (CEO)</td>
                    <td>London</td>
                    <td>47</td>
                    <td>2009/10/09</td>
                    <td>$1,200,000</td>
                </tr>
                <tr>
                    <td>Gavin Joyce</td>
                    <td>Developer</td>
                    <td>Edinburgh</td>
                    <td>42</td>
                    <td>2010/12/22</td>
                    <td>$92,575</td>
                </tr>
                <tr>
                    <td>Jennifer Chang</td>
                    <td>Regional Director</td>
                    <td>Singapore</td>
                    <td>28</td>
                    <td>2010/11/14</td>
                    <td>$357,650</td>
                </tr>
                <tr>
                    <td>Brenden Wagner</td>
                    <td>Software Engineer</td>
                    <td>San Francisco</td>
                    <td>28</td>
                    <td>2011/06/07</td>
                    <td>$206,850</td>
                </tr>
                <tr>
                    <td>Fiona Green</td>
                    <td>Chief Operating Officer (COO)</td>
                    <td>San Francisco</td>
                    <td>48</td>
                    <td>2010/03/11</td>
                    <td>$850,000</td>
                </tr>
                <tr>
                    <td>Shou Itou</td>
                    <td>Regional Marketing</td>
                    <td>Tokyo</td>
                    <td>20</td>
                    <td>2011/08/14</td>
                    <td>$163,000</td>
                </tr>
                <tr>
                    <td>Michelle House</td>
                    <td>Integration Specialist</td>
                    <td>Sydney</td>
                    <td>37</td>
                    <td>2011/06/02</td>
                    <td>$95,400</td>
                </tr>
                <tr>
                    <td>Suki Burks</td>
                    <td>Developer</td>
                    <td>London</td>
                    <td>53</td>
                    <td>2009/10/22</td>
                    <td>$114,500</td>
                </tr>
                <tr>
                    <td>Prescott Bartlett</td>
                    <td>Technical Author</td>
                    <td>London</td>
                    <td>27</td>
                    <td>2011/05/07</td>
                    <td>$145,000</td>
                </tr>
                <tr>
                    <td>Gavin Cortez</td>
                    <td>Team Leader</td>
                    <td>San Francisco</td>
                    <td>22</td>
                    <td>2008/10/26</td>
                    <td>$235,500</td>
                </tr>
                <tr>
                    <td>Martena Mccray</td>
                    <td>Post-Sales support</td>
                    <td>Edinburgh</td>
                    <td>46</td>
                    <td>2011/03/09</td>
                    <td>$324,050</td>
                </tr>
                <tr>
                    <td>Unity Butler</td>
                    <td>Marketing Designer</td>
                    <td>San Francisco</td>
                    <td>47</td>
                    <td>2009/12/09</td>
                    <td>$85,675</td>
                </tr>
                <tr>
                    <td>Howard Hatfield</td>
                    <td>Office Manager</td>
                    <td>San Francisco</td>
                    <td>51</td>
                    <td>2008/12/16</td>
                    <td>$164,500</td>
                </tr>
                <tr>
                    <td>Hope Fuentes</td>
                    <td>Secretary</td>
                    <td>San Francisco</td>
                    <td>41</td>
                    <td>2010/02/12</td>
                    <td>$109,850</td>
                </tr>
                <tr>
                    <td>Vivian Harrell</td>
                    <td>Financial Controller</td>
                    <td>San Francisco</td>
                    <td>62</td>
                    <td>2009/02/14</td>
                    <td>$452,500</td>
                </tr>
                <tr>
                    <td>Timothy Mooney</td>
                    <td>Office Manager</td>
                    <td>London</td>
                    <td>37</td>
                    <td>2008/12/11</td>
                    <td>$136,200</td>
                </tr>
                <tr>
                    <td>Jackson Bradshaw</td>
                    <td>Director</td>
                    <td>New York</td>
                    <td>65</td>
                    <td>2008/09/26</td>
                    <td>$645,750</td>
                </tr>
                <tr>
                    <td>Olivia Liang</td>
                    <td>Support Engineer</td>
                    <td>Singapore</td>
                    <td>64</td>
                    <td>2011/02/03</td>
                    <td>$234,500</td>
                </tr>
                <tr>
                    <td>Bruno Nash</td>
                    <td>Software Engineer</td>
                    <td>London</td>
                    <td>38</td>
                    <td>2011/05/03</td>
                    <td>$163,500</td>
                </tr>
                <tr>
                    <td>Sakura Yamamoto</td>
                    <td>Support Engineer</td>
                    <td>Tokyo</td>
                    <td>37</td>
                    <td>2009/08/19</td>
                    <td>$139,575</td>
                </tr>
                <tr>
                    <td>Thor Walton</td>
                    <td>Developer</td>
                    <td>New York</td>
                    <td>61</td>
                    <td>2013/08/11</td>
                    <td>$98,540</td>
                </tr>
                <tr>
                    <td>Finn Camacho</td>
                    <td>Support Engineer</td>
                    <td>San Francisco</td>
                    <td>47</td>
                    <td>2009/07/07</td>
                    <td>$87,500</td>
                </tr>
                <tr>
                    <td>Serge Baldwin</td>
                    <td>Data Coordinator</td>
                    <td>Singapore</td>
                    <td>64</td>
                    <td>2012/04/09</td>
                    <td>$138,575</td>
                </tr>
                <tr>
                    <td>Zenaida Frank</td>
                    <td>Software Engineer</td>
                    <td>New York</td>
                    <td>63</td>
                    <td>2010/01/04</td>
                    <td>$125,250</td>
                </tr>
                <tr>
                    <td>Zorita Serrano</td>
                    <td>Software Engineer</td>
                    <td>San Francisco</td>
                    <td>56</td>
                    <td>2012/06/01</td>
                    <td>$115,000</td>
                </tr>
                <tr>
                    <td>Jennifer Acosta</td>
                    <td>Junior Javascript Developer</td>
                    <td>Edinburgh</td>
                    <td>43</td>
                    <td>2013/02/01</td>
                    <td>$75,650</td>
                </tr>
                <tr>
                    <td>Cara Stevens</td>
                    <td>Sales Assistant</td>
                    <td>New York</td>
                    <td>46</td>
                    <td>2011/12/06</td>
                    <td>$145,600</td>
                </tr>
                <tr>
                    <td>Hermione Butler</td>
                    <td>Regional Director</td>
                    <td>London</td>
                    <td>47</td>
                    <td>2011/03/21</td>
                    <td>$356,250</td>
                </tr>
                <tr>
                    <td>Lael Greer</td>
                    <td>Systems Administrator</td>
                    <td>London</td>
                    <td>21</td>
                    <td>2009/02/27</td>
                    <td>$103,500</td>
                </tr>
                <tr>
                    <td>Jonas Alexander</td>
                    <td>Developer</td>
                    <td>San Francisco</td>
                    <td>30</td>
                    <td>2010/07/14</td>
                    <td>$86,500</td>
                </tr>
                <tr>
                    <td>Shad Decker</td>
                    <td>Regional Director</td>
                    <td>Edinburgh</td>
                    <td>51</td>
                    <td>2008/11/13</td>
                    <td>$183,000</td>
                </tr>
                <tr>
                    <td>Michael Bruce</td>
                    <td>Javascript Developer</td>
                    <td>Singapore</td>
                    <td>29</td>
                    <td>2011/06/27</td>
                    <td>$183,000</td>
                </tr>
                <tr>
                    <td>Donna Snider</td>
                    <td>Customer Support</td>
                    <td>New York</td>
                    <td>27</td>
                    <td>2011/01/25</td>
                    <td>$112,000</td>
                </tr>
            </tbody>
            <tfoot>
                <tr>
                    <th>Name</th>
                    <th>Position</th>
                    <th>Office</th>
                    <th>Age</th>
                    <th>Start date</th>
                    <th>Salary</th>
                </tr>
            </tfoot>
        </table>

        <script>
            $(document).ready(function () {
                $('#example').DataTable({
                    "pagingType": "full_numbers"
                });
                $('#example2').DataTable({
                    "pagingType": "full_numbers"
                });
                function PP() {
                    var ele = $('.qrmcode-fim');
                    for (var i = 0; i < ele.length; i++) {
                        var noi = "";
                        noi = $.ajax({
                            url: "/TABLE2/ShowDataTablesWMS001",
                            type: "GET",
                            data: { qrmcode: ele[i].innerHTML },
                            async: false
                        }).responseJSON;
                        console.log('noi ' + ele[i].innerHTML + ' : ' + noi);
                        if (noi > 0) {
                            $('.imgOf' + ele[i].innerHTML).css('color', '#00d431');
                        } else {
                            $('.imgOf' + ele[i].innerHTML).css('color', '#dbdbdb');
                        }
                    }
                }

                $('#DataTables_Table_0_paginate').click(function () {
                    PP();
                });
            });</script>
    </div>

    <script>
        function openPage(pageName, elmnt) {
            var i, tabcontent, tablinks;
            tabcontent = document.getElementsByClassName("tabcontent");
            for (i = 0; i < tabcontent.length; i++) {
                tabcontent[i].style.display = "none";
            }
            tablinks = document.getElementsByClassName("tablink");
            for (i = 0; i < tablinks.length; i++) {
                tablinks[i].style.backgroundColor = "";
            }
            document.getElementById(pageName).style.display = "block";
            elmnt.style.backgroundColor = '#555';
        }

        document.getElementById("typingTab").click();
    </script>

</body>

</html>