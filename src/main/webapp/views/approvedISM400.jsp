<%@ include file="../includes/taglibs.jsp" %>
<%@ include file="../includes/imports.jsp" %>
<!DOCTYPE html>
<html lang="en">
    <%@ include file="../includes/head.jsp" %>
    <style>
        .danger-red {
            background: red;
            color: white;
        }
        .table-hover tr:hover{
            color:black;
        }

        input[type=submit] {
            clear:none;
            width: 100%;
            background-color: #00399b;
            color: white;
            padding: 5px 5px;
            margin: 8px 0;
            border: none;
            border-radius: 4px;
            cursor: pointer;
        }

        input[type=submit]:hover {
            background-color: #3973d6;
        }
    </style>
    <body>

        <div id="wrapper">

            <!-- /#sidebar-wrapper -->
            <%@ include file="../fragments/sidebar.jsp" %>

            <!-- Page Content -->
            <!-- nav-head-custom -->
            <%@ include file="../includes/MSS_head.jsp" %>
            <div class="container-fluid">
                
                <form action="../resources/manual/ISM400.pdf" target="_blank">
                    <table width="100%">
                        <tr>
                            <td width="94%" align="left">
                            </td>
                            <td width="6%" align="right"><input type="submit" value="Manual" style="font-weight: normal;" /></td>
                        </tr>
                    </table>
                </form>
                
                <div id="wrapper-top">
                    <div class="row">
                        <div class="col-lg-12">
                            <div id="set-height" style="height:265px;margin-top:40px;">
                                <div id="sidebar-wrapper-top">
                                    <b class="page-header" style="padding-left:5px;font-size:18px;">
                                        <table>
                                            <tr>
                                                <td width="130px;">
                                                    Customer
                                                </td>
                                                <td width="20px;" align="center"> : </td>
                                                <td>
                                                    <input id="sPOFG" value="${POFG}" class="form-control" type="text" onkeyup="javascript:this.value = this.value.toUpperCase();getCustomerName();getDropDownGroup();getDropDownSQ();">
                                                </td>
                                                <td>
                                                    <div id="customerName" style="margin-left:20px;color:#777;font-size:16px;"></div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Job no.
                                                </td>
                                                <td width="20px;" align="center"> : </td>
                                                <td>
                                                    <div id="dpGRPNO" style="margin-top:10px;">
                                                        <input class="form-control" type="text" disabled>
                                                    </div>
                                                </td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Sequence no.
                                                </td>
                                                <td width="20px;" align="center"> : </td>
                                                <td>
                                                    <div id="dpSEQNO" style="margin-top:10px;">
                                                        <input class="form-control" type="text" disabled>
                                                    </div>
                                                </td>
                                                <td></td>
                                            </tr>
                                            </tr>
                                            <tr>
                                                <td>
                                                    View
                                                </td>
                                                <td width="20px;" align="center"> : </td>
                                                <td>
                                                    <select id="sVIEW" class="form-control" style="margin-top:10px;">
                                                        <option value="0">All</option>
                                                        <option value="1">more than 5%</option>
                                                        <option value="2">less than or equal to 5%</option>
                                                    </select>
                                                </td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td></td>
                                                <td height="34px;">
                                                    <div id="checkB" style="font-size:17px;margin-top:10px;padding-left:5px;color:#777">
                                                        <!-- <input id="sCHECK" type="checkbox" > Not enough -->
                                                    </div>
                                                </td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td></td>
                                                <td>
                                                    <button onclick="search()" style="width:100%;margin-top:10px;" class="btn btn-outline btn-primary">
                                                        <b><i class="fa fa-search icon-size-sm"></i> Search</b>
                                                    </button>
                                                </td>
                                                <td></td>
                                            </tr>
                                        </table>
                                    </b>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="toggle-top" style="position:relative;">
                        <button type="button" onclick="Approved()" style="position:absolute;right:31.5% ;top:-40px;width:15%;z-index: 2000;font-weight:bold;" class="btn btn-outline btn-success"><i class="fa fa-check-circle-o"></i> Approved</button>

                        <script type="text/javascript">
                            function Approved() {

                                status = 0;

                                $('.checkbox').each(function () { //iterate all listed checkbox items
                                    if (this.checked == true) {
                                        status = 1;
                                    }
                                });

                                if (status == 1) {
                                    $("#fType").val("1");
                                    $('#panel-body').loader('show', '<i style="font-size:100px;" class="fa fa-refresh fa-spin"></i>');
                                    var frm = $('#checkForm');
                                    $.ajax({
                                        type: frm.attr('method'),
                                        url: frm.attr('action'),
                                        data: frm.serialize(),
                                        success: function (data) {
                                            location.reload();
                                        }
                                    });
                                } else {
                                    alertify.error("Please select one or more data.");
                                }

                            }
                        </script>

                        <button type="button" onclick="Rejected()" style="position:absolute;right:16% ;top:-40px;width:15%;z-index: 2000;font-weight:bold;" class="btn btn-outline btn-danger"><i class="fa fa-times-circle-o"></i> Rejected</button>

                        <script type="text/javascript">
                            function Rejected() {

                                status = 0;

                                $('.checkbox').each(function () { //iterate all listed checkbox items
                                    if (this.checked == true) {
                                        status = 1;
                                    }
                                });

                                if (status == 1) {
                                    $("#fType").val("2");
                                    alertify.confirm("<b class='head-custom' style='font-size:18px;'> Are you sure you want to reject this list ? </b>", function (e) {
                                        if (e) {
                                            //after clicking OK
                                            $('#panel-body').loader('show', '<i style="font-size:100px;" class="fa fa-refresh fa-spin"></i>');
                                            var frm = $('#checkForm');
                                            $.ajax({
                                                type: frm.attr('method'),
                                                url: frm.attr('action'),
                                                data: frm.serialize(),
                                                success: function (data) {
                                                    location.reload();
                                                }
                                            });

                                        } else {
                                            //after clicking Cancel
                                        }
                                    });
                                } else {
                                    alertify.error("Please select one or more data.");
                                }
                            }
                        </script>

                        <a style="position:absolute;right:0 ;top:-40px;width:15%;z-index: 2000;" href="#menu-toggle2" class="btn btn-outline btn-default  custom-toggle-menu" id="menu-toggle2">
                            <div id="i-toggle-top"></div>
                        </a>
                        <hr style="margin:0px;margin-top:10px;margin-bottom:10px;">
                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-primary" style="min-height: 485px;">
                                <table class="pull-right" style="position:absolute;top:5.5px;right:25px;font-size:14px;">
                                    <tr>
                                        <td>
                                            <b style="color:white;margin-right:10px;"><c:out value="${USER}" escapeXml="false" /> (<c:out value="${MSWHSE}" escapeXml="false" />)</b>
                                        </td>
                                        <td>
                                            <button onclick="logout()" class="btn btn-default btn-outline btn-sm custom-icon-w-b">
                                                <b style="color:white"><i class="fa fa-sign-out"></i> Logout</b>
                                            </button>
                                            <script type="text/javascript">
                                                function logout() {
                                                    alertify.confirm("<b class='head-custom' style='font-size:18px;'> Are you sure you want to logout ? </b>", function (e) {
                                                        if (e) {
                                                            //after clicking OK
                                                            $.get('../ISM400AJAX', {a: 'logout'}, function (responseText) {
                                                                window.location = "/TABLE2/ISM400/AP";
                                                            });

                                                        } else {
                                                            //after clicking Cancel
                                                        }
                                                    });
                                                }
                                            </script>
                                        </td>
                                    </tr>
                                </table>

                                <div class="panel-heading">
                                    <h4 style="margin:0px;">Approved</h4>
                                </div>

                                <!-- /.panel-heading -->
                                <div id="panel-body" class="panel-body">
                                    <div class="dataTable_wrapper">
                                        <form id="checkForm" action="../ISM400AJAX" method="post">
                                            <input value="${userid}" name="uidbyAip" id="uidbyAip" hidden>
                                            <input type="hidden" id="fType" name="fType">
                                            <input type="hidden" name="fName" value="${USER}">
                                            <table class="table table-bordered table-hover" id="dataTables-example">
                                                <thead>
                                                    <tr>
                                                        <th style="border-bottom:0px;min-width:35px;">MC</th>
                                                        <th style="border-bottom:0px;">Material</th>
                                                        <th style="border-bottom:0px;">Description</th>
                                                        <th style="border-bottom:0px;min-width:30px;">U.</th>
                                                        <th style="border-bottom:0px;min-width:55px;">Req.</th>
                                                        <th style="border-bottom:0px;min-width:55px;">Pick.</th>
                                                        <th style="border-bottom:0px;min-width:75px;">Percent</th>
                                                        <th style="border-bottom:0px;min-width:80px;">Status</th>
                                                        <th style="border-bottom:0px;text-align:center;"><input type="checkbox" id="select_all"/></th>
                                                    </tr>
                                                </thead>

                                                <tfoot>
                                                <th style="border-left:0px;">MC</th>
                                                <th style="border-left:0px;">Material</th>
                                                <th style="border-left:0px;">Description</th>
                                                <th style="border-left:0px;">U.</th>
                                                <th style="border-left:0px;">Req.</th>
                                                <th style="border-left:0px;">Pick.</th>
                                                <th style="border-left:0px;">Percent</th>
                                                <th style="border-left:0px;">Status</th>
                                                <th style="border-left:0px;"></th>
                                                </tfoot>

                                                <tbody>
                                                    <c:forEach items="${listIssue}" var="MPDATAD">

                                                        <c:choose>
                                                            <c:when test="${MPDATAD.PERCENT < 0}">
                                                                <tr  class="danger-red">
                                                                </c:when>
                                                                <c:otherwise>
                                                                <tr>
                                                                </c:otherwise>
                                                            </c:choose>


                                                            <td><c:out value="${MPDATAD.LGPBE   }" escapeXml="false" /></td>
                                                            <td><c:out value="${MPDATAD.MATNR}" escapeXml="false" /></td>
                                                            <td ><c:out value="${MPDATAD.ARKTX}" escapeXml="false" /></td>
                                                            <td align="center"><c:out value="${MPDATAD.VRKME}" escapeXml="false" /></td>
                                                            <td align="right"><fmt:formatNumber type="number" pattern="###,##0.000" value="${MPDATAD.KWMENG}" /></td>
                                                            <td align="right"><fmt:formatNumber type="number" pattern="###,##0.000" value="${MPDATAD.ISSUETOPICK}" /></td>
                                                            <c:choose>
                                                                <c:when test="${MPDATAD.PERCENT*100 >= 105}">
                                                                    <td align="right" style="position: relative;color:green;font-weight:bold">
                                                                        <i style="position: absolute;left:10px;" class="fa fa-check"></i> +
                                                                        <fmt:formatNumber type="percent" maxIntegerDigits="3" value="${MPDATAD.PERCENT}" />
                                                                    </td>
                                                                </c:when>
                                                                <c:when test="${MPDATAD.PERCENT*100 >= 0 && MPDATAD.PERCENT*100 < 105}">
                                                                    <td align="right" style="position: relative;color:green;font-weight:bold">
                                                                        <i style="position: absolute;left:10px;" class="fa fa-check"></i> +
                                                                        <fmt:formatNumber type="percent" maxIntegerDigits="3" value="${MPDATAD.PERCENT}" />
                                                                    </td>
                                                                </c:when>
                                                                <c:when test="${MPDATAD.PERCENT*100 == -1}">
                                                                    <td align="right" style="position: relative;color:orange;font-weight:bold">
                                                                        <i style="position: absolute;left:10px;" class="fa fa-times"></i>
                                                                        <fmt:formatNumber type="percent" maxIntegerDigits="3" value="${MPDATAD.PERCENT}" />
                                                                    </td>
                                                                </c:when>
                                                                <c:otherwise>
                                                                    <td align="right" style="position: relative;color:#226097;font-weight:bold">
                                                                        <i style="position: absolute;left:10px;" class="fa fa-exclamation"></i>
                                                                        <fmt:formatNumber type="percent" maxIntegerDigits="3" value="${MPDATAD.PERCENT}" />
                                                                    </td>
                                                                </c:otherwise>
                                                            </c:choose>
                                                            <c:choose>
                                                                <c:when test="${MPDATAD.STATUS == 0}">
                                                                    <td align="center" style="color:#226097;font-weight:bold">
                                                                    </c:when>
                                                                    <c:when test="${MPDATAD.STATUS == 1}">
                                                                    <td align="center" style="color:orange;font-weight:bold">
                                                                    </c:when>
                                                                    <c:when test="${MPDATAD.STATUS == 2}">
                                                                    <td align="center" style="color:green;font-weight:bold">
                                                                    </c:when>
                                                                    <c:when test="${MPDATAD.STATUS == 3}">
                                                                    <td align="center" style="color:#B512A4;font-weight:bold;min-width:120px;">
                                                                    </c:when>
                                                                    <c:when test="${MPDATAD.STATUS == 4}">
                                                                    <td align="center" style="color:#226097;font-weight:bold;min-width:120px;">
                                                                    </c:when>
                                                                    <c:when test="${MPDATAD.STATUS == 5}">
                                                                    <td align="center" style="color:orange;font-weight:bold;min-width:120px;">
                                                                    </c:when>
                                                                    <c:when test="${MPDATAD.STATUS == 6}">
                                                                    <td align="center" style="color:green;font-weight:bold;min-width:120px;">
                                                                    </c:when>
                                                                    <c:when test="${MPDATAD.STATUS == 9}">
                                                                    <td align="center" style="color:red;font-weight:bold">
                                                                    </c:when>
                                                                    <c:otherwise>
                                                                    <td align="center" style="font-weight:bold">
                                                                    </c:otherwise>
                                                                </c:choose>
                                                                <c:out value="${MPDATAD.STATUSDESC}" escapeXml="false" />
                                                            </td>
                                                            <td align="center">
                                                                <c:choose>
                                                                    <c:when test="${(MPDATAD.STATUS > 0) && (MPDATAD.ISSUETOPICK > 0)}">
                                                                        <input type="checkbox" class="checkbox"  name="nCheck" value="1|${GRPNO}|${ISSUENO}|${fn:trim(MPDATAD.MATNR)}">
                                                                        <input type="text" name="stsforCK" value="${MPDATAD.STATUS}" hidden>
                                                                        <input type="text" name="mtforCK" value="${MPDATAD.LGPBE}" hidden>
                                                                        <input type="text" name="pickforCK" value="${MPDATAD.ISSUETOPICK}" hidden>
                                                                    </c:when>
                                                                    <c:otherwise>
<!--                                                                        <input type="checkbox" class="checkbox" name="nCheck" value="1|${GRPNO}|${ISSUENO}|${fn:trim(MPDATAD.MATNR)}">
                                                                        <input type="text" name="stsforCK" value="${MPDATAD.STATUS}" hidden>
                                                                        <input type="text" name="mtforCK" value="${MPDATAD.LGPBE}" hidden>-->
                                                                    </c:otherwise>
                                                                </c:choose>
                                                            </td>
                                                        </tr>
                                                    </c:forEach>
                                                </tbody>
                                            </table>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <script>

                var sidebarVisible = localStorage.getItem('wrapper');
                var sidebarVisible2 = localStorage.getItem('wrapper-top');
                var checkIMenu = $("#i-menu").html();
                var checkToggleTop = $("#i-toggle-top").html();

                $(document).ready(function () {

                    toggleClick();

                    toggleLoad();

                    toggleClick2();

                    toggleLoad2();

                    getTime();

                    getCustomerName();

                    getDropDownGroup();

                    getDropDownSQ();

                    if ("${VIEW}" != "" && "${VIEW}" != "null") {
                        $('#sVIEW').val("${VIEW}");
                    }

                    if ("${CHECK}" == "1") {
                        $('#sCHECK').prop('checked', true);
                    }

                    setInterval(function () {
                        getTime();
                    }, 1000);


                    $('#dataTables-example').DataTable({
                        "aLengthMenu": [[6, 20, 50, -1], [6, 20, 50, "All"]],
                        "iDisplayLength": -1,
                        "bStateSave": true,
                        "aoColumnDefs": [
                            {'bSortable': false, 'aTargets': [2]},
                            {'bSortable': false, 'aTargets': [8]}
                        ],

                        responsive: true
                    });

                    $('#dataTables-example tfoot th').not(":eq(8)").each(function () {
                        var title = $(this).text();
                        $(this).html('<input type="text" class="form-control input-sm" style="width:100%" placeholder="' + title + '" />');
                    });

                    // DataTable
                    var table = $('#dataTables-example').DataTable();

                    // Apply the search
                    table.columns().every(function () {
                        var that = this;

                        $('input', this.footer()).on('keyup change', function () {
                            if (that.search() !== this.value) {
                                that.search(this.value).draw();
                            }
                        });
                    });

                    var state = table.state.loaded();
                    if (state) {
                        table.columns().eq(0).each(function (colIdx) {
                            var colSearch = state.columns[colIdx].search;

                            if (colSearch.search) {
                                $('input', table.column(colIdx).footer()).val(colSearch.search);
                            }

                        });

                        table.draw();
                    }

                    //select all checkboxes
                    $("#select_all").change(function () {  //"select all" change Mod by Aip
                        var status = this.checked; // "select all" checked status

                        let Mered = false; // Check Order Line Color Red Eq Not Pick And Status = 1 : requested
                        let Meck = false; // Check Order get Checked
                        let MeSomeThing = false;

                        $('.checkbox').each(function () {

//                            let classtext = $(this).closest("tr").attr('class');
                            let closTR = $(this).closest("tr")[0];

                            let pickall = closTR.cells[5].innerHTML;
                            let stsall = closTR.cells[7].innerHTML.trim().toString().split(" - ")[0];

                            if (pickall > 0 && stsall > 0) {
                                if ($(this).val().substring(0, 1) == "1") {
                                    this.checked = status; //change ".checkbox" checked status
                                }
                            } else {
                                this.checked = false;
                                Mered = true;
                            }

//                            if (classtext.toString().includes("danger-red")) { // Order Line Red 
//                                this.checked = false;
//                                Mered = true;
//                            } else {
//                                if ($(this).val().substring(0, 1) == "1") {
//                                    this.checked = status; //change ".checkbox" checked status
//                                }
//                            }

//                            let checkedbox = $(this).closest('tr').find('[type=checkbox]').prop('checked');
                            if (status === true) {
                                Meck = true;
                            }

                            MeSomeThing = true;

                        });

                        if (MeSomeThing == false) {
                            alertify.alert('Warning Can Not Checked').set('autoReset', false);
                            this.checked = false;
                        }

                        if (Mered && Meck) { // RedLine && Meck = true
                            alertify.alert('Warning Order must be Pick > 0 And Status == 1').set('autoReset', false);
                            this.checked = false;
                        }
//                        if (Mered && Meck) { // RedLine && Meck = true
//                            alertify.alert('Warning Some Order Pick = 0 And Status = requested').set('autoReset', false);
//                        }

//                        $('.checkbox').each(function () { //iterate all listed checkbox items
//                            if ($(this).val().substring(0, 1) == "1") {
//                                this.checked = status; //change ".checkbox" checked status
//                            }
//                        });
                    });

                    //uncheck "select all", if one of the listed checkbox item is unchecked
                    $('.checkbox').change(function () { //".checkbox" change

                        if (this.checked == true) { //checkbox check
                            let message = "";

                            //Loop through all checked CheckBoxes in GridView.
                            $("#dataTables-example input[type=checkbox]:checked").each(function () {
                                var row = $(this).closest("tr")[0];
                                message += row.cells[5].innerHTML;
                                message += "|" + row.cells[7].innerHTML.trim();
                                message += "+";
                            });

                            if (message !== "") {

                                var textsplit = message.split("|");

                                //sts = 1 && pick > 0
                                var pickNum = parseFloat(textsplit[0]);
//                                var stsOne = textsplit[1];
                                var stsOneSp = message.split(" - ")[0].split("|")[1];

                                if (pickNum > 0 && stsOneSp > 0) {

                                } else {
                                    alertify.alert('Warning This Order must be Pick > 0 And Status == 1').set('autoReset', false);
                                    this.checked = false;
                                }

                            }
                        }

                        if (this.checked == false) { //if this item is unchecked
                            $("#select_all")[0].checked = false; //change "select all" checked status to false
                        }
                    });

                });

                function toggleLoad2() {
                    if (sidebarVisible2 != null && sidebarVisible2 == 1) {
                        $("#wrapper-top").css({"-webkit-transition": "all 0.0s ease"}, {"-moz-transition": "all 0.0s ease"}, {"-o-transition": "all 0.0s ease"}, {"transition": "all 0.0s ease"});
                        $("#sidebar-wrapper-top").css({"-webkit-transition": "all 0.0s ease"}, {"-moz-transition": "all 0.0s ease"}, {"-o-transition": "all 0.0s ease"}, {"transition": "all 0.0s ease"});
                        $("#wrapper-top").toggleClass("toggled");
                        $("#i-toggle-top").html("<b class=\"custom-text\">Show search</b>");
                        // $("#set-height").css({"margin-top":"50px"});
                    } else {
                        $("#i-toggle-top").html("<b class=\"custom-text\">Hide search</b>");
                    }
                }

                function toggleClick2() {
                    $("#menu-toggle2").click(function (e) {

                        e.preventDefault();
                        $("#wrapper-top").toggleClass("toggled");

                        if ($("#i-toggle-top").html() == "<b class=\"custom-text\">Show search</b>") {
                            $("#wrapper-top").css({"-webkit-transition": "all 0.5s ease"}, {"-moz-transition": "all 0.5s ease"}, {"-o-transition": "all 0.5s ease"}, {"transition": "all 0.5s ease"});
                            $("#sidebar-wrapper-top").css({"-webkit-transition": "all 0.5s ease"}, {"-moz-transition": "all 0.5s ease"}, {"-o-transition": "all 0.5s ease"}, {"transition": "all 0.5s ease"});
                            $("#i-toggle-top").html("<b class=\"custom-text\">Hide search</b>");
                            // $("#toggle-top").css({"margin-top":"0px"});
                            localStorage.setItem('wrapper-top', 0);
                        } else {
                            $("#i-toggle-top").html("<b class=\"custom-text\">Show search</b>");
                            // $("#toggle-top").css({"margin-top":"50px"});
                            localStorage.setItem('wrapper-top', 1);
                        }

                    });
                }


                function getCustomerName() {
                    var POFG = $("#sPOFG").val();
                    $.get('../ISM300AJAX', {a: 'getCustomerName', POFG: POFG}, function (responseText) {
                        $('#customerName').html(responseText);
                    });
                }
//                g
                function getDropDownGroup() {
                    var POFG = $("#sPOFG").val();
                    $.get('../ISM300AJAX', {a: 'getDropDownGroup', POFG: POFG}, function (responseText) {
                        if (responseText != "null") {
                            $('#dpGRPNO').html(responseText);
                            $('#sGRPNO').val("${GRPNO}");
                        } else {
                            $('#dpGRPNO').html("<input class=\"form-control\" type=\"text\" disabled>");
                        }
                    });
                }

                function getDropDownSQ() {
                    var POFG = $("#sPOFG").val();
                    $.get('../ISM300AJAX', {a: 'getDropDownSQ', POFG: POFG}, function (responseText) {
                        if (responseText != "null") {
                            $('#dpSEQNO').html(responseText);
                            $('#sISSUENO').val("${ISSUENO}");
                        } else {
                            $('#dpSEQNO').html("<input class=\"form-control\" type=\"text\" disabled>");
                        }
                    });
                }

                function search() {

                    var POFG = $('#sPOFG').val();
                    var GRPNO = $("#sGRPNO").val();
                    var ISSUENO = $("#sISSUENO").val();
                    var VIEW = $("#sVIEW").val();

                    if (POFG == "" || GRPNO == "undefined" || ISSUENO == "undefined") {
                        alertify.error("Please fill out search form.");
                    } else {
                        window.location = "AP?a=list&POFG=" + POFG + "&VIEW=" + VIEW + "&GRPNO=" + GRPNO + "&ISSUENO=" + ISSUENO;
                    }

                }

            </script>

    </body>

</html>