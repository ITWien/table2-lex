<%@ include file="../fragments/taglibs.jsp" %>
<%@ include file="../fragments/imports.jsp" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.sql.*;" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>WMS</title>
        <!-- css :: vendors -->
        <jsp:include page = "../fragments/css.jsp" />
        <!-- additional custom :: my-style -->
        <link rel="stylesheet" href="../resources/assets/styles/myStyles.css">

        <!-- js :: vendors -->
        <jsp:include page = "../fragments/script.jsp" />
        <!-- additional custom :: my-script -->
        <script src="../resources/assets/scripts/myScripts.js" async></script>
        <script src="../resources/assets/scripts/toggleLoad.js" async></script> 
        <script>
            $(document).ready(function () {
                $('#showTable').DataTable({
                    "lengthMenu": [[-1, 10, 25, 50, 100], ["All", 10, 25, 50, 100]],
                    "bSortClasses": false,
                    "order": [[0, "desc"]],
                    "columnDefs": [
                        {orderable: false, targets: [10]},
                        {orderable: false, targets: [11]},
                        {orderable: false, targets: [12]}
//                        {"width": "20%", "targets": 4}
                    ]
                });

                // Setup - add a text input to each footer cell
                $('#showTable tfoot th').not(":eq(10)").not(":eq(10)").not(":eq(10)").each(function () {
                    var title = $(this).text();
                    $(this).html('<input type="text" />');
                });

                // DataTable
                var table = $('#showTable').DataTable();

                // Apply the search
                table.columns().every(function () {
                    var that = this;

                    $('input', this.footer()).on('keyup change', function () {
                        if (that.search() !== this.value) {
                            that
                                    .search(this.value)
                                    .draw();
                        }
                    });
                });
            });
        </script>
        <style>
            input[type=text], select {
                width: 100%;
                padding: 5px 5px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }

            input[type=submit] {
                width: 100%;
                background-color: #00399b;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=submit]:hover {
                background-color: #3973d6;
            }
        </style>
        <script>
            function checkAll(ele) {
                var checkboxes = document.getElementsByTagName('input');
                if (ele.checked) {
                    for (var i = 0; i < checkboxes.length; i++) {
                        if (checkboxes[i].type == 'checkbox') {
                            checkboxes[i].checked = true;
                        }
                    }
                } else {
                    for (var i = 0; i < checkboxes.length; i++) {
                        console.log(i)
                        if (checkboxes[i].type == 'checkbox') {
                            checkboxes[i].checked = false;
                        }
                    }
                }
            }

            submitForms1 = function () {
                var checkboxes = document.getElementsByTagName('input');
                var nodataSelect = false;

                for (var i = 0; i < checkboxes.length; i++) {
                    if (checkboxes[i].type === 'checkbox' && checkboxes[i].name !== 'selectAll') {
                        if (checkboxes[i].checked === true) {
                            nodataSelect = true;
                        }
                    }
                }

                if (nodataSelect) {
                    document.getElementById('myModal-5').style.display = 'block';
                    document.getElementById('appall').submit();
                } else {
                    document.getElementById('myModal-4').style.display = 'block';
                }
            };
        </script>
        <style>
            body {font-family: Arial, Helvetica, sans-serif;}

            /* The Modal (background) */
            .modal {
                display: none; /* Hidden by default */
                position: fixed; /* Stay in place */
                z-index: 1; /* Sit on top */
                padding-top: 100px; /* Location of the box */
                left: 0;
                top: 0;
                width: 100%; /* Full width */
                height: 100%; /* Full height */
                overflow: auto; /* Enable scroll if needed */
                background-color: rgb(0,0,0); /* Fallback color */
                background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
            }

            /* Modal Content */
            .modal-content {
                background-color: #fefefe;
                margin: auto;
                padding: 20px;
                border: 1px solid #888;
                width: fit-content;
                height: 320px;
            }

            /* The Close Button */
            .close {
                color: #aaaaaa;
                float: right;
                font-size: 28px;
                font-weight: bold;
            }

            .close:hover,
            .close:focus {
                color: #000;
                text-decoration: none;
                cursor: pointer;
            }
        </style>
    </head>    
    <body >
        <div id="wrapper">

            <!-- /#sidebar-wrapper -->
            <%@ include file="../fragments/sidebar.jsp" %>

            <!-- Page Content -->
            <!-- nav-head-custom -->
            <%@ include file="../fragments/nav_head.jsp" %>

            <div class="container-fluid">
                <!--<div id="wrapper-top">-->
                <%-- PART 2 --%>
                <!--                    <div class="row">
                                        <div class="col-lg-12">
                                            <div id="set-height" style="height:415px;margin-top:0px;">
                                                <div id="sidebar-wrapper-top" class="">
                                                    <b class="page-header" style="padding-left:5px;font-size:18px;">
                <%-- <hr style="margin:0px;margin-top:10px;margin-bottom:10px;"> --%>
            </b>
        </div>
    </div>
</div>
</div> -->
                <%-- PART 3 --%>
                <!--show Qdest table-->
                <form action="../resources/manual/WMS311.pdf" target="_blank">
                    <table width="100%">
                        <tr>
                            <td width="94%"></td>
                            <td width="6%" align="right"><input type="submit" value="Manual" /></td>
                        </tr>
                    </table>
                </form>
                <div style="border: 2px solid #ccc; border-radius: 5px; padding: 20px;">
                    <form action="Fdisplay" method="post">
                        <table>
                            <tr>
                                <th width="120px"><b>Warehouse : </b></th>
                                <td width="200px">
                                    <select name="warehouse" onchange="this.form.submit()">
                                        <option value="${wh}" selected hidden>${wh} : ${whn}</option>
                                        <c:forEach items="${MCList}" var="p" varStatus="i">
                                            <option value="${p.uid}">${p.uid} : ${p.name}</option>
                                        </c:forEach>
                                    </select>
                                    <input type="hidden" name="matCtrl" value="${mc}">
                                </td>
                            </tr>
                        </table>
                    </form>
                    <form id="appall" action="approvedAll" method="post">
                        <input type="hidden" name="WHS" value="${wh}">
                        <input type="hidden" name="MTCTRL" value="${mc}">
                        <table width="100%">
                            <tr>
                                <td style="text-align:right;">
                                    <a style="width: 120px;" class="btn btn-danger" onclick="location.href = '/TABLE2/WMS311/display?whF=${wh}&whT=${wh}'">
                                        <i class="fa fa-chevron-circle-left" style="font-size:20px;"></i> Back</a>

                                    <!--                                    <a class="btn btn btn-outline btn-success" id="approvedALL" name="approvedALL" onclick="submitForms1();">
                                                                            <i class="fa fa-check-circle-o" style="font-size:20px;"></i> Approved</a>-->
                                </td>
                            </tr>
                        </table>
                        <table id="showTable" class="display" style="width:100%; border: 1px solid #ccc; border-radius: 4px;">
                            <thead>
                                <tr>
                                    <th style="text-align: center;">Trans Date</th>
                                    <th style="text-align: center;">Queue No.</th>
                                    <th style="text-align: center;">MVT</th>
                                    <th style="text-align: center;">Destination</th>
                                    <th style="text-align: center;">User</th>
                                    <th style="text-align: center;">SET</th>
                                    <th style="text-align: center;">Product Group</th>
                                    <th style="text-align: center;">Mat Control</th>
                                    <th style="text-align: center;">Low Status</th>
                                    <th style="text-align: center;">High Status</th>
                                    <th style="text-align: center;">Total QTY</th>
                                    <th style="text-align: center;">Option</th>
                                    <td style="text-align: center;">Check All &nbsp; 
                                        <input type="checkbox" name="selectAll" onchange="checkAll(this)">
                                    </td>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th>Trans Date</th>
                                    <th>Queue No.</th>
                                    <th>MVT</th>
                                    <th>Destination</th>
                                    <th>User</th>
                                    <th>SET</th>
                                    <th>Product Group</th>
                                    <th>Mat Control</th>
                                    <th>Low Status</th>
                                    <th>High Status</th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </tfoot>
                            <tbody>
                                <c:forEach items="${MTCList}" var="p" varStatus="i">
                                    <tr>
                                        <td>${p.transDate}</td>
                                        <td>${p.queueNo}</td>
                                        <td>${p.mvt}</td>
                                        <td>${p.dest}</td>
                                        <td>${p.user}</td>
                                        <td>${p.set}</td>
                                        <td>${p.productGroup}</td>
                                        <td>${p.matCtrl}</td>
                                        <td>${p.status}</td>
                                        <td>${p.histatus}</td>
                                        <td align="right">${p.totalQty}</td>
                                        <td align="right">
                                            ${p.editOption}
                                            <a href="detail?qno=${p.queueNo}&wh=${p.wh}" class="btn btn-default" style="padding: 3px 6px;" >
                                                <i class="fa fa-desktop fa-lg" style="color:darkblue;"></i>
                                            </a>
                                        </td>
                                        <td align="center">
                                            ${p.ckBox}
                                        </td>
                                    </tr>
                                </c:forEach>
                            </tbody>
                        </table>
                        <div id="myModal-4" class="modal">
                            <!-- Modal content -->
                            <div class="modal-content" style=" height: 200px; width: 500px;">
                                <span id="span-4" class="close" onclick="document.getElementById('myModal-4').style.display = 'none';">&times;</span>
                                <p><b><font size="4"></font></b></p>
                                <table width="100%">
                                    <tr>
                                        <td align="center">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <b style="color: #00399b;"><font size="5">Please select data !</font></b>
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <br>
                                            <br>
                                            <br>
                                            <a style=" width: 100px;" class="btn btn btn-outline btn-danger" onclick="document.getElementById('myModal-4').style.display = 'none';">
                                                OK
                                            </a>                 
                                        </td>
                                    </tr>
                                </table>            
                            </div>

                        </div>
                        <div id="myModal-5" class="modal"> 
                            <!-- Modal content -->
                            <div class="modal-content" style=" height: 180px; width: 500px;">
                                <!--<span id="span-5" class="close" onclick="document.getElementById('myModal-5').style.display = 'none';">&times;</span>-->
                                <br>
                                <p><b><font size="4"></font></b></p>
                                <table width="100%">
                                    <tr>
                                        <td align="center">
                                            <b style="color: #00399b;"><font size="5">Please wait</font></b>
                                            <br>
                                            <b style="color: #00399b;"><font size="5">In Process &nbsp;&nbsp;<i class='fa fa-spinner fa-spin ' style="font-size:30px;"></i></font></b>
                                            <br>
                                            <br>
                                            <!--                                            <a style=" width: 100px;" class="btn btn btn-outline btn-danger" onclick="document.getElementById('myModal-5').style.display = 'none';">
                                                                                            OK
                                                                                        </a>                 -->
                                        </td>
                                    </tr>
                                </table>            
                            </div>

                        </div>
                    </form>
                    <!--End Part 3-->
                    <!--</div>  end #wrapper-top--> 
                </div> 
                <br>
            </div> <!-- end .container-fluid -->
        </div> <!-- end #wrapper -->
    </body>
</html>