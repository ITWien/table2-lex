<%@ include file="../fragments/taglibs.jsp" %>
<%@ include file="../fragments/imports.jsp" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.sql.*;" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>WMS</title>
        <!-- css :: vendors -->
        <jsp:include page = "../fragments/css.jsp" />
        <!-- additional custom :: my-style -->
        <link rel="stylesheet" href="../resources/assets/styles/myStyles.css">

        <!-- js :: vendors -->
        <jsp:include page = "../fragments/script.jsp" />
        <!-- additional custom :: my-script -->
        <script src="../resources/assets/scripts/myScripts.js" async></script>
        <script src="../resources/assets/scripts/toggleLoad.js" async></script> 
        <script>
            var show1 = function () {
                $('#myModal1').modal('show');
            };

            var show2 = function () {
                $('#myModal2').modal('show');
            };

            function validateForm() {
                var StranDate = document.forms["frm"]["startdate"].value;
                var FtranDate = document.forms["frm"]["enddate"].value;
                var Swh = document.forms["frm"]["warehouseFrom"].value;
                var Fwh = document.forms["frm"]["warehouseTo"].value;

                StranDate = StranDate.toString();
                StranDate = StranDate.replace("-", "");
                StranDate = StranDate.replace("-", "");
                var sd = parseInt(StranDate);

                FtranDate = FtranDate.toString();
                FtranDate = FtranDate.replace("-", "");
                FtranDate = FtranDate.replace("-", "");
                var fd = parseInt(FtranDate);

                Swh = Swh.toString();
                Fwh = Fwh.toString();

                var WH = [Swh, Fwh];
                WH.sort();

                if (Swh !== WH[0]) {
                    if (Fwh !== "") {
                        window.setTimeout(show1, 0);
                        return false;
                    }
                }

                if (fd < sd) {
                    window.setTimeout(show2, 0);
                    return false;
                }
            }
        </script>
        <style>
            input[type=text], select {
                width: 100%;
                padding: 5px 5px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }

            input[type=date] {
                height: 35px;
                padding: 5px 5px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }

            input[type=submit] {
                width: 100%;
                background-color: #00399b;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=submit]:hover {
                background-color: #3973d6;
            }

            b, th, td {
                color: #134596;
            }
        </style>
        <style>
            button {
                display: inline-block;
                /*padding: 60px 35px;*/
                margin-top: 20px;
                margin-bottom: 20px;
                font-size: 24px;
                cursor: pointer;
                text-align: center;
                text-decoration: none;
                outline: none;
                color: #fff;
                background-color: #1C56A5;
                border: none;
                border-radius: 10px;
                box-shadow: 0 9px #999;
            }

            button:hover {background-color: #00399b}

            button:active {
                background-color: #00399b;
                box-shadow: 0 5px #666;
                transform: translateY(4px);
            }
        </style>
    </head>    
    <body >
        <div id="wrapper">

            <!-- /#sidebar-wrapper -->
            <%@ include file="../fragments/sidebar.jsp" %>

            <!-- Page Content -->
            <!-- nav-head-custom -->
            <%@ include file="../fragments/nav_head.jsp" %>

            <div class="container-fluid">
                <!--<div id="wrapper-top">-->
                <%-- PART 2 --%>
                <!--                    <div class="row">
                                        <div class="col-lg-12">
                                            <div id="set-height" style="height:415px;margin-top:0px;">
                                                <div id="sidebar-wrapper-top" class="">
                                                    <b class="page-header" style="padding-left:5px;font-size:18px;">
                <%-- <hr style="margin:0px;margin-top:10px;margin-bottom:10px;"> --%>
            </b>
        </div>
    </div>
</div>
</div> -->
                <%-- PART 3 --%>
                <!--show Qdest table-->
                <form action="../resources/manual/WMS311.pdf" target="_blank">
                    <table width="100%">
                        <tr>
                            <td width="94%"></td>
                            <td width="6%" align="right"><input type="submit" value="Manual" /></td>
                        </tr>
                    </table>
                </form>
                <div style="border: 2px solid #ccc; border-radius: 5px; padding: 20px;">
                    <form action="display" method="POST" name="frm" onsubmit="return validateForm()">
                        <table width="100%">
                            <tr>
                                <th width="100px"><b>Warehouse : </b></th>
                                <td width="500px">
                                    <select name="warehouseFrom" style="width:45%;" onchange="this.form.submit()" onsubmit="return validateForm()">
                                        <option value="${mcF}" selected hidden>${mcF} : ${nameF}</option>
                                        <c:forEach items="${MCList}" var="p" varStatus="i">
                                            <option value="${p.uid}">${p.uid} : ${p.name}</option>
                                        </c:forEach>
                                    </select>
                                    -
                                    <select name="warehouseTo" style="width:45%;" onchange="this.form.submit()" onsubmit="return validateForm()">
                                        <option value="${mcT}" selected hidden>${mcT} : ${nameT}</option>
                                        <c:forEach items="${MCList}" var="p" varStatus="i">
                                            <option value="${p.uid}">${p.uid} : ${p.name}</option>
                                        </c:forEach>
                                    </select>
                                </td>
                                <td></td>
                                <!--                                <td align="right" width="250px">
                                                                    <b>Select by Transaction Date : </b>
                                                                </td>
                                                                <td align="right" width="340px">
                                                                    <input type="date" id="startdate" name="startdate" value="${sd}" onchange="this.form.submit()" onsubmit="return validateForm()"/> -
                                                                    <input type="date" id="enddate" name="enddate" value="${ed}" onchange="this.form.submit()" onsubmit="return validateForm()"/>
                                                                </td>-->
                            </tr>
                        </table>
                        <br>
                        <c:forEach items="${MTCList}" var="p" varStatus="i">
                            ${p.name}
                            <a href="/TABLE2/WMS311/Fdisplay?WHS=${p.warehouse}&MTCTRL=${p.uid}&whF=${mcF}&whT=${mcT}">
                                <button class="button" type="button" value="${p.uid}">
                                    ${p.desc}
                                    <br>
                                    <img src="../resources/images/matctrl/${p.uid}.jpg" height="150" width="130" style="border-radius: 8px;">
                                    <br>
                                    ${p.uid}<br>
                                    <font size="3" color="#d6d6d6">Total Queue : </font><font size="4" style="text-shadow: 2px 2px #000000;">${p.tolQno}</font><br>
                                    <font size="3" color="#d6d6d6">T.QTY : </font><font size="4" style="text-shadow: 2px 2px #000000;">${p.tolQty}</font>
                                </button>
                            </a>
                        </c:forEach>
                        <center>
                            <div id="myModal1" class="modal fade" role="dialog">
                                <div class="modal-dialog modal-sm"> <!-- ???????????? modal ?????????????? ???? xs, sm, md, lg -->
                                    <div class="modal-md col-md-6" style="top: 150px; left:300px; background-color: #ddd; border: 1px solid #ccc; border-radius: 4px;">
                                        <div class="modal-header">
                                            <h4 class="modal-title" align="center">Warehouse(To) must be greater than or equal to Warehouse(From) !</h4>
                                            <br>
                                            <button name="ok" type="button" class="close" data-dismiss="modal">
                                                <font color = "white">OK</font>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- devbanban.com -->
                        </center>
                        <center>
                            <div id="myModal2" class="modal fade" role="dialog">
                                <div class="modal-dialog modal-sm"> <!-- ???????????? modal ?????????????? ???? xs, sm, md, lg -->
                                    <div class="modal-md col-md-6" style="top: 150px; left:300px; background-color: #ddd; border: 1px solid #ccc; border-radius: 4px;">
                                        <div class="modal-header">
                                            <h4 class="modal-title" align="center">Transaction Date(To) must be greater than or equal to Transaction Date(From) !</h4>
                                            <br>
                                            <button name="ok" type="button" class="close" data-dismiss="modal">
                                                <font color = "white">OK</font>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- devbanban.com -->
                        </center>
                    </form>
                    <!--End Part 3-->
                    <br>
                    <!--</div>  end #wrapper-top--> 
                    <b>All Material Control : ${numMat}</b>
                </div> 
                <br>
            </div> <!-- end .container-fluid -->
        </div> <!-- end #wrapper -->
    </body>
</html>