<%@ include file="../fragments/taglibs.jsp" %>
<%@ include file="../fragments/imports.jsp" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.sql.*;" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>GENERATE MODE</title>
        <!-- css :: vendors -->
        <jsp:include page = "../fragments/css.jsp" />
        <!-- additional custom :: my-style -->
        <link rel="stylesheet" href="../resources/assets/styles/myStyles.css">

        <!-- js :: vendors -->
        <jsp:include page = "../fragments/script.jsp" />
        <!-- additional custom :: my-script -->
        <script src="../resources/assets/scripts/myScripts.js" async></script>
        <script src="../resources/assets/scripts/toggleLoad.js" async></script>  
        ${showPDF}
        <script>
            $(document).ready(function () {
                $('#showTable').DataTable({
                    "lengthMenu": [[10, 25, 50, 100], [10, 25, 50, 100]],
                    "bSortClasses": false
                });

                // Setup - add a text input to each footer cell
                $('#showTable tfoot th').not(":eq(6)").each(function () {
                    var title = $(this).text();
                    $(this).html('<input type="text" />');
                });

                // DataTable
                var table = $('#showTable').DataTable();

                // Apply the search
                table.columns().every(function () {
                    var that = this;

                    $('input', this.footer()).on('keyup change', function () {
                        if (that.search() !== this.value) {
                            that
                                    .search(this.value)
                                    .draw();
                        }
                    });
                });
            });
        </script>
        <style>
            input[type=text], select {
                width: 100%;
                padding: 5px 5px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }

            input[type=submit] {
                width: 100%;
                background-color: #00399b;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=submit]:hover {
                background-color: #3973d6;
            }
            
            input[type=button] {
                width: 100%;
                background-color: #ef604a;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=button]:hover {
                background-color: #e51e00;
            }
        </style>
    </head>    
    <body >
        <div id="wrapper">

            <!-- /#sidebar-wrapper -->
            <%@ include file="../fragments/sidebar.jsp" %>

            <!-- Page Content -->
            <!-- nav-head-custom -->
            <%@ include file="../fragments/nav_head.jsp" %>

            <div class="container-fluid">
                <!--<div id="wrapper-top">-->
                <%-- PART 2 --%>
                <!--                    <div class="row">
                                        <div class="col-lg-12">
                                            <div id="set-height" style="height:415px;margin-top:0px;">
                                                <div id="sidebar-wrapper-top" class="">
                                                    <b class="page-header" style="padding-left:5px;font-size:18px;">
                <%-- <hr style="margin:0px;margin-top:10px;margin-bottom:10px;"> --%>
            </b>
        </div>
    </div>
</div>
</div> -->
                <%-- PART 3 --%>
                <!--show Qdest table-->
                <div align="right">
                    <input style="width: 100px;" type="button" value="Back" onclick="window.location.href = '/TABLE2/WMS008/connect?wh=${wh}'"/>
                </div>
                <table id="showTable" class="display" style="width:100%; border: 1px solid #ccc; border-radius: 4px;">
                    <thead>
                        <tr>
                            <th>Warehouse</th>
                            <th>Area/Zone</th>
                            <th>Rack no.</th>
                            <th>Side</th>
                            <th>Column</th>
                            <th>Row</th>
                            <th>Option
                                <a href="createG?wh=${wh}"><i class="glyphicon glyphicon-plus-sign" style="font-size:15px; padding-left: 10px"></i></a>
                            </th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>Warehouse</th>
                            <th>Area/Zone</th>
                            <th>Rack no.</th>
                            <th>Side</th>
                            <th>Column</th>
                            <th>Row</th>
                            <th></th>
                        </tr>
                    </tfoot>
                    <tbody>
                        <c:forEach items="${QdestList}" var="x" varStatus="i">
                            <tr>
                                <td>${x.zone1}</td>
                                <td>${x.zone}</td>
                                <td>${x.rkno}</td>
                                <td>${x.side}</td>
                                <td>${x.column}</td>
                                <td>${x.row}</td>
                                <td>
                                    <a href="deleteG?wh=${x.zone1}&zone=${x.zone}&loc=${x.rkno}${x.side}${x.column}${x.row}" ><i class="glyphicon glyphicon-trash" style="font-size:15px;"></i></a>
                                    &nbsp;&nbsp;&nbsp;&nbsp;
                                    <a href="printG?wh=${x.zone1}&loc=${x.rkno}${x.side}${x.column}${x.row}" target="_blank"><i class="fa fa-print" style="font-size:20px; padding-left: 10px"></i></a>
                                </td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
                <!--End Part 3-->
                <br>
                <!--</div>  end #wrapper-top--> 
            </div> <!-- end .container-fluid -->
        </div> <!-- end #wrapper -->
    </body>
</html>