<%@ include file="../fragments/taglibs.jsp" %>
<%@ include file="../fragments/imports.jsp" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.sql.*;" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>WMS</title>
        <!-- css :: vendors -->
        <jsp:include page = "../fragments/css.jsp" />
        <!-- additional custom :: my-style -->
        <link rel="stylesheet" href="../resources/assets/styles/myStyles.css">

        <!-- js :: vendors -->
        <jsp:include page = "../fragments/script.jsp" />
        <!-- additional custom :: my-script -->
        <script src="../resources/assets/scripts/myScripts.js" async></script>  
        <script src="../resources/assets/scripts/toggleLoad.js" async></script> 
        <script>
            var show1 = function () {
                $('#myModal1').modal('show');
            };

            var show2 = function () {
                $('#myModal2').modal('show');
            };

            function validateForm() {
                var StranDate = document.forms["frm"]["startdate"].value;
                var FtranDate = document.forms["frm"]["enddate"].value;
                var Swh = document.forms["frm"]["warehouseFrom"].value;
                var Fwh = document.forms["frm"]["warehouseTo"].value;

                StranDate = StranDate.toString();
                StranDate = StranDate.replace("-", "");
                StranDate = StranDate.replace("-", "");
                var sd = parseInt(StranDate);

                FtranDate = FtranDate.toString();
                FtranDate = FtranDate.replace("-", "");
                FtranDate = FtranDate.replace("-", "");
                var fd = parseInt(FtranDate);

                Swh = Swh.toString();
                Fwh = Fwh.toString();

                var WH = [Swh, Fwh];
                WH.sort();

                if (Swh !== WH[0]) {
                    if (Fwh !== "") {
                        window.setTimeout(show1, 0);
                        return false;
                    }
                }

                if (fd < sd) {
                    window.setTimeout(show2, 0);
                    return false;
                }
            }

            function showWH(dest, close) {
                var modal = document.getElementById(dest);
                modal.style.display = "block";

                var span = document.getElementById(close);

                span.onclick = function () {
                    modal.style.display = "none";
                };

                window.onclick = function (event) {
                    if (event.target === modal) {
                        modal.style.display = "none";
                    }
                };
            }
        </script>
        <style>
            input[type=text], select {
                width: 100%;
                padding: 5px 5px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }

            input[type=date] {
                height: 35px;
                padding: 5px 5px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }

            input[type=submit] {
                width: 100%;
                background-color: #00399b;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=submit]:hover {
                background-color: #3973d6;
            }

            b, th, td {
                color: #134596;
            }
        </style>
        <style>
            button {
                display: inline-block;
                /*padding: 60px;*/
                margin-top: 20px;
                margin-bottom: 20px;
                font-size: 24px;
                cursor: pointer;
                text-align: center;
                text-decoration: none;
                outline: none;
                color: #fff;
                background-color: #ffffff;
                border: none;
                border-radius: 50%;
                /*box-shadow: 0 9px #999;*/
            }

            button:hover {background-color: #eaeaea}

            button:active {
                background-color: #eaeaea;
                /*box-shadow: 0 5px #666;*/
                transform: translateY(4px);
            }
        </style>
        <style>
            body {font-family: Arial, Helvetica, sans-serif;}

            /* The Modal (background) */
            .modal {
                display: none; /* Hidden by default */
                position: fixed; /* Stay in place */
                z-index: 1; /* Sit on top */
                padding-top: 100px; /* Location of the box */
                left: 0;
                top: 0;
                width: 100%; /* Full width */
                height: 100%; /* Full height */
                overflow: auto; /* Enable scroll if needed */
                background-color: rgb(0,0,0); /* Fallback color */
                background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
            }

            /* Modal Content */
            .modal-content {
                background-color: #fefefe;
                margin: auto;
                padding: 20px;
                border: 1px solid #888;
                width: fit-content;
                height: 320px;
            }

            /* The Close Button */
            .close {
                color: #aaaaaa;
                float: right;
                font-size: 28px;
                font-weight: bold;
            }

            .close:hover,
            .close:focus {
                color: #000;
                text-decoration: none;
                cursor: pointer;
            }
        </style>
    </head>    
    <body >
        <div id="wrapper">

            <!-- /#sidebar-wrapper -->
            <%@ include file="../fragments/sidebar.jsp" %>

            <!-- Page Content -->
            <!-- nav-head-custom -->
            <%@ include file="../fragments/nav_head.jsp" %>

            <div class="container-fluid">
                <!--<div id="wrapper-top">-->
                <%-- PART 2 --%>
                <!--                    <div class="row">
                                        <div class="col-lg-12">
                                            <div id="set-height" style="height:415px;margin-top:0px;">
                                                <div id="sidebar-wrapper-top" class="">
                                                    <b class="page-header" style="padding-left:5px;font-size:18px;">
                <%-- <hr style="margin:0px;margin-top:10px;margin-bottom:10px;"> --%>
            </b>
        </div>
    </div>
</div>
</div> -->
                <%-- PART 3 --%>
                <!--show Qdest table-->
                <form action="../resources/manual/WMS341.pdf" target="_blank">
                    <table width="100%">
                        <tr>
                            <td width="94%"></td>
                            <td width="6%" align="right"><input type="submit" value="Manual" /></td>
                        </tr>
                    </table>
                </form>
                <div style="border: 2px solid #ccc; border-radius: 5px; padding: 20px;">
                    <form action="displayDest" method="POST" name="frm" onsubmit="return validateForm()">
                        <table width="100%">
                            <tr>
                                <th width="100px"><b>Warehouse : </b></th>
                                <td width="500px">
                                    <select name="warehouseFrom" style="width:45%;" onchange="this.form.submit()" onsubmit="return validateForm()">
                                        <option value="${mcF}" selected hidden>${mcF} : ${nameF}</option>
                                        <c:forEach items="${MCList}" var="p" varStatus="i">
                                            <option value="${p.uid}">${p.uid} : ${p.name}</option>
                                        </c:forEach>
                                    </select>
                                </td>
                                <!--                                <td align="right" width="250px">
                                                                    <b>Select by Transaction Date : </b>
                                                                </td>
                                                                <td align="right" width="340px">
                                                                    <input type="date" id="startdate" name="startdate" value="${sd}" onchange="this.form.submit()" onsubmit="return validateForm()"/> -
                                                                    <input type="date" id="enddate" name="enddate" value="${ed}" onchange="this.form.submit()" onsubmit="return validateForm()"/>
                                                                </td>-->
                                <td align="right" width="90px">
                                    <b>Select by : </b>
                                </td>
                                <td width="200px">
                                    <select name="selectBy" onchange="this.form.submit()">
                                        <option value="" hidden>Destination</option>
                                        <option value="dest">Destination</option>
                                        <option value="mat">Material Control</option>
                                    </select>
                                </td>
                                <td></td>
                            </tr>
                        </table>
                        <br>
                        <c:forEach items="${MTCList}" var="p" varStatus="i">
                            ${p.name}
                            <a id="${p.uid}" style="text-decoration: none;" onclick="return showWH('myModal-${p.uid}', 'span-${p.uid}');"> 
                                <button class="button" type="button" style="width: 200px; height: 200px;">
                                    <b style="color: #0048AB">${p.uid}</b>
                                    <br>
                                    &nbsp;<img src="../resources/images/dest/DEST.jpg" height="120" width="120"  style="border-radius: 50%;">
                                    <br>
                                    <b style="color: #0048AB; font-size: 15px;">${p.desc}</b>
                                </button>
                            </a>
                            <!-- The Modal -->
                            <div id="myModal-${p.uid}" class="modal">
                                <!-- Modal content -->
                                <div class="modal-content">
                                    <span id="span-${p.uid}" class="close">&times;</span>
                                    <p><b><font size="4">${p.uid} : ${p.desc}</font></b></p>

                                    <table width="100%">
                                        <tr>
                                            <td align="center">
                                                <c:forEach items="${p.whList}" var="x">
                                                    <a href="/TABLE2/WMS341/detailDest?mat=${x.uid}&wh=${mcF}&dest=${p.uid}" style="text-decoration: none;">
                                                        <button class="button" type="button" style="border-radius: 10px; background-color: #fff; width: 240px; height: 220px; color: #000; border: 2px solid #000;">
                                                            <b><font size="4">${x.uid}</font></b>
                                                            <hr style="border-top: 3px solid #ccc;">
                                                            <p align="left">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SKU = ${x.name}</p>
                                                            <p align="left">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Q'PACK = ${x.warehouse}</p>
                                                            <p align="left">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;M<sup>3</sup> = ${x.desc}</p>
                                                        </button>
                                                    </a>
                                                </c:forEach>
                                            </td>
                                        </tr>
                                    </table>
                                </div>

                            </div>
                        </c:forEach>
                        <center>
                            <div id="myModal1" class="modal fade" role="dialog">
                                <div class="modal-dialog modal-sm"> <!-- ???????????? modal ?????????????? ???? xs, sm, md, lg -->
                                    <div class="modal-md col-md-6" style="top: 150px; left:300px; background-color: #ddd; border: 1px solid #ccc; border-radius: 4px;">
                                        <div class="modal-header">
                                            <h4 class="modal-title" align="center">Warehouse(To) must be greater than or equal to Warehouse(From) !</h4>
                                            <br>
                                            <button name="ok" type="button" class="close" data-dismiss="modal">
                                                <font color = "white">OK</font>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- devbanban.com -->
                        </center>
                        <center>
                            <div id="myModal2" class="modal fade" role="dialog">
                                <div class="modal-dialog modal-sm"> <!-- ???????????? modal ?????????????? ???? xs, sm, md, lg -->
                                    <div class="modal-md col-md-6" style="top: 150px; left:300px; background-color: #ddd; border: 1px solid #ccc; border-radius: 4px;">
                                        <div class="modal-header">
                                            <h4 class="modal-title" align="center">Transaction Date(To) must be greater than or equal to Transaction Date(From) !</h4>
                                            <br>
                                            <button name="ok" type="button" class="close" data-dismiss="modal">
                                                <font color = "white">OK</font>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- devbanban.com -->
                        </center>
                    </form>
                    <!--End Part 3-->
                    <br>
                    <!--</div>  end #wrapper-top--> 
                    <b>All Destination : ${numDest}</b>
                </div> 
                <br>
            </div> <!-- end .container-fluid -->
        </div> <!-- end #wrapper -->
    </body>
</html>