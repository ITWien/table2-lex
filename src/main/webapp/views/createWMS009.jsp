<%@ include file="../fragments/taglibs.jsp" %>
<%@ include file="../fragments/imports.jsp" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.sql.*;" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>CREATE MODE</title>
        <!-- css :: vendors -->
        <jsp:include page = "../fragments/css.jsp" />
        <!-- additional custom :: my-style -->
        <link rel="stylesheet" href="../resources/assets/styles/myStyles.css">

        <!-- js :: vendors -->
        <jsp:include page = "../fragments/script.jsp" />
        <!-- additional custom :: my-script -->
        <script src="../resources/assets/scripts/myScripts.js" async></script>
        <script src="../resources/assets/scripts/toggleLoad.js" async></script>
        <script type="text/javascript"> /* add something code to your script */</script>
        ${sendMessage}
        <script>
            function ShowPass() {
                var x = document.getElementById("password");
                if (x.type === "password") {
                    x.type = "text";
                } else {
                    x.type = "password";
                }
            }

            function Preview(ele) {
                $('#im-up').attr('src', ele.value);
                if (ele.files && ele.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $('#im-up').attr('src', e.target.result);
                    }
                    reader.readAsDataURL(ele.files[0]);
                }
            }
        </script>
        <script>
            var show1 = function () {
                $('#myModal1').modal('show');
            };

            function validateForm() {
                var id = document.forms["frm"]["userid"].value;
                var pass = document.forms["frm"]["password"].value;
                var name = document.forms["frm"]["name"].value;
                var level = document.forms["frm"]["level"].value;
                var wh = document.forms["frm"]["warehouse"].value;

                if (id.trim() === "") {
                    window.setTimeout(show1, 0);
                    document.getElementById("userid").style.cssText = "border: 2px solid #ff9999";
                    return false;
                } else {
                    document.getElementById("userid").style.cssText = "border: 1px solid #ccc";
                }

                if (pass.trim() === "") {
                    window.setTimeout(show1, 0);
                    document.getElementById("password").style.cssText = "border: 2px solid #ff9999";
                    return false;
                } else {
                    document.getElementById("password").style.cssText = "border: 1px solid #ccc";
                }

                if (name.trim() === "") {
                    window.setTimeout(show1, 0);
                    document.getElementById("name").style.cssText = "border: 2px solid #ff9999";
                    return false;
                } else {
                    document.getElementById("name").style.cssText = "border: 1px solid #ccc";
                }

                if (level.trim() === "") {
                    window.setTimeout(show1, 0);
                    document.getElementById("level").style.cssText = "border: 2px solid #ff9999";
                    return false;
                } else {
                    document.getElementById("level").style.cssText = "border: 1px solid #ccc";
                }

                if (wh.trim() === "") {
                    window.setTimeout(show1, 0);
                    return false;
                }
            }
        </script>
        <style>
            input[type=text], input[type=password], input[type=number], select {
                width: 100%;
                padding: 5px 5px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }

            input[type=submit] {
                width: 100%;
                background-color: #2bd14a;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=submit]:hover {
                background-color: #45a049;
            }

            input[type=button] {
                width: 100%;
                background-color: #ef604a;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=button]:hover {
                background-color: #e51e00;
            }

            button[name=ok] {
                width: 25%;
                background-color: #008cff;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            button[name=ok]:hover {
                background-color: #008cff;
            }
        </style>
    </head>    
    <body onload="document.getElementById('userid22').value = sessionStorage.getItem('uid');">
        <div id="wrapper">

            <!-- /#sidebar-wrapper -->
            <%@ include file="../fragments/sidebar.jsp" %>

            <!-- Page Content -->
            <!-- nav-head-custom -->
            <%@ include file="../fragments/nav_head.jsp" %>

            <div class="container-fluid">
                <!--                <div id="wrapper-top" align="left">
                <%-- PART 2 --%>
                <div class="row">
                    <div class="col-lg-12">
                        <div id="set-height" style="height:415px;margin-top:0px;">
                            <div id="sidebar-wrapper-top" class="">
                                <b class="page-header" style="padding-left:5px;font-size:18px;">
                <%-- <hr style="margin:0px;margin-top:10px;margin-bottom:10px;"> --%>
            </b>
        </div>
    </div>
</div>
</div> -->

                <%-- PART 3 --%>
                <form action="create" method="POST" name="frm" onsubmit="return validateForm()" enctype="multipart/form-data">
                    <input type="hidden" id="userid22" name="userid22">
                    <table frame="box" width="100%" bordercolor="#e0e0e0">
                        <tr>
                            <th height="50" bgcolor="#f8f8f8">CREATE MODE</th>
                            <th height="50" bgcolor="#f8f8f8"></th>
                            <th height="50" bgcolor="#f8f8f8"></th>
                            <th height="50" bgcolor="#f8f8f8"></th>
                        </tr>
                        <br>
                        <tr>
                            <td width="15%"></td>
                            <td width="15%" align="left"><h4>User ID :</h4></td>
                            <td width="30%" align="left" style="padding-right: 200px;"><input type="text" id="userid" name="userid" maxlength="10"></td>
                            <td width="30%"></td>
                        </tr>
                        <tr>
                            <td width="15%"></td>
                            <td width="15%" align="left"><h4>Password :</h4></td>
                            <td width="30%" align="left"><input type="password" id="password" name="password" maxlength="50"></td>
                            <td width="30%"><a onclick="ShowPass()"><i class="fa fa-eye" style="font-size:20px; padding-left: 10px"></i></a></td>
                        </tr>
                        <tr>
                            <td width="15%"></td>
                            <td width="15%" align="left"><h4>Name :</h4></td>
                            <td width="30%" align="left"><input type="text" id="name" name="name" maxlength="50"></td>
                            <td width="30%"></td>
                        </tr>

                        <div class="row" style="position:absolute;top:150px;right:150px;">
                            <img border="5" width="250" height="300" name="im-up" id="im-up" hspace="30" align="right" src="../resources/images/male.jpg"> 
                        </div>

                        <tr>
                            <td width="15%"></td>
                            <td width="15%" align="left"><h4>Level :</h4></td>
                            <td width="30%" align="left" style="padding-right: 300px;"><input type="number" id="level" name="level" min="1" max="9" maxlength="2"></td>
                            <td width="30%"></td>
                        </tr>
                        <tr>
                            <td width="15%"></td>
                            <td width="15%" align="left"><h4>Warehouse :</h4></td>
                            <td width="30%" align="left"><select name="warehouse">
                                    <c:forEach items="${WHList}" var="p" varStatus="i">
                                        <option value="${p.code}">${p.code} : ${p.name}</option>
                                    </c:forEach>
                                </select></td>
                            <td width="30%"></td>
                        </tr>
                        <tr>
                            <td width="15%"></td>
                            <td width="15%" align="left"><h4>Department :</h4></td>
                            <td width="30%" align="left"><input type="text" name="department" maxlength="50"></td>
                            <td width="30%"></td>
                        </tr>
                        <tr>
                            <td width="15%"></td>
                            <td width="15%" align="left"><h4>Menuset :</h4></td>
                            <td width="30%" align="left"><input type="text" name="menuset" maxlength="20"></td>
                            <td width="30%"></td>
                        </tr>
                        <br>
                        <div align="center">
                            <tr>
                                <td></td>
                                <td></td>
                                <td width="30%" align="center"><input style="width: 100px;" type="button" value="Cancel" onclick="window.location.href = '/TABLE2/WMS009/display'"/>
                                    <input style="width: 100px;" type="submit" value="Confirm" /></td>
                                <td align="center">
                                    <input style=" padding-left: 300px;" type="file" name="filUpload" size="30" onchange="return Preview(this)" id="filUpload">
                                </td>
                            </tr>
                        </div>
                    </table>
                    <center>
                        <div id="myModal" class="modal fade" role="dialog">
                            <div class="modal-dialog modal-sm"> <!-- ???????????? modal ?????????????? ???? xs, sm, md, lg -->
                                <div class="modal-md col-md-6" style="top: 150px; left:300px; background-color: #ddd; border: 1px solid #ccc; border-radius: 4px;">
                                    <div class="modal-header">
                                        <h4 class="modal-title" align="center">Duplicate User ID !</h4>
                                        <br>
                                        <button name="ok" type="button" onclick="window.location.href = '/TABLE2/WMS009/create'">
                                            <font color = "white">OK</font>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- devbanban.com -->
                    </center>
                    <center>
                        <div id="myModal1" class="modal fade" role="dialog">
                            <div class="modal-dialog modal-sm"> <!-- ???????????? modal ?????????????? ???? xs, sm, md, lg -->
                                <div class="modal-md col-md-6" style="top: 150px; left:300px; background-color: #ddd; border: 1px solid #ccc; border-radius: 4px;">
                                    <div class="modal-header">
                                        <h4 class="modal-title" align="center">Please fill in the blanks !</h4>
                                        <br>
                                        <button name="ok" type="button" class="close" data-dismiss="modal">
                                            <font color = "white">OK</font>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- devbanban.com -->
                    </center>
                    <center>
                        <div id="myModal2" class="modal fade" role="dialog">
                            <div class="modal-dialog modal-sm"> <!-- ???????????? modal ?????????????? ???? xs, sm, md, lg -->
                                <div class="modal-md col-md-6" style="top: 150px; left:300px; background-color: #ddd; border: 1px solid #ccc; border-radius: 4px;">
                                    <div class="modal-header">
                                        <h4 class="modal-title" align="center">Create Failed !</h4>
                                        <br>
                                        <button name="ok" type="button" onclick="window.location.href = '/TABLE2/WMS009/create'">
                                            <font color = "white">OK</font>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- devbanban.com -->
                    </center>
                </form>
                <!--End Part 3-->
                <!--</div>  end #wrapper-top -->
            </div> <!-- end .container-fluid -->
        </div> <!-- end #wrapper -->


    </body>

</html>