<%@ include file="../fragments/taglibs.jsp" %>
<%@ include file="../fragments/imports.jsp" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.sql.*;" %>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <title>ISM</title>
        <!-- css :: vendors -->
        <jsp:include page="../fragments/css.jsp" />
        <!-- additional custom :: my-style -->
        <link rel="stylesheet" href="../resources/assets/styles/myStyles.css">
        <link rel="stylesheet" href="../resources/font/THsarabun/thsarabumnew.css">

        <!-- js :: vendors -->
        <jsp:include page="../fragments/script.jsp" />
        <!-- additional custom :: my-script -->
        <script src="../resources/assets/scripts/myScripts.js" async></script>
        <script src="../resources/assets/scripts/toggleLoad.js" async></script>
        <style>
            input[type=text],
            select {
                width: 100%;
                padding: 5px 5px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }

            input[type=date],
            input[type=number] {
                /*width: 100%;*/
                padding: 5px 5px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }

            input[type=submit] {
                width: 100%;
                background-color: #00399b;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=submit]:hover {
                background-color: #3973d6;
            }

            input[type=button] {
                width: 100%;
                background-color: #ef604a;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=button]:hover {
                background-color: #e51e00;
            }

            #closeBtn{
                /*width: 150px;*/
                background-color: #575656;
                border-color: #575656;
                color:white;
                /*padding: 0px;*/
            }

            #closeBtn:hover{
                /*width: 150px;*/
                background-color: #000000;
                border-color: #000000;
                color:white;
                /*padding: 0px;*/
            }

        </style>
        <script>
            var tableOptions = {
                "paging": true,
                "lengthMenu": [[-1, 10, 25, 50, 100], ["All", 10, 25, 50, 100]],
                "bSortClasses": false,
                "ordering": true,
                "info": true,
                "fixedColumns": true,
                "order": [[1, "asc"]],
                "columnDefs": [
                    {"targets": 0, "className": "text-center", "orderable": false}, //
                    {"targets": 1, "className": "text-right"}, //
                    {"targets": 2, "className": "text-right"}, //
                    {"targets": 3},
                    {"targets": 4},
                    {"targets": 5},
                    {"targets": 6, "className": "text-right"}, //
                    {"targets": 7, "className": "text-right"}, //
                    {"targets": 8, className: "text-center"}, //
                    {"targets": 9, "className": "text-center"}, //
                    {"targets": 10},
                    {"targets": 11},
                    {"targets": 12, "className": "text-center"}, //
                    {"targets": 13, "className": "text-center", "orderable": false}//

//                    {"targets": [1, 2, 6, 7, 8], "className": "text-right"},
//                    {"targets": [0, 9, 12, 13], "className": "text-center"},
//                    {"targets": [0, 13], "orderable": false}
                ],
                "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {

                    var sino = aData[2];
                    if (sino !== "") {
                        if (sino !== "0") {
                            if (parseInt(sino) % 2 === 0) {
                                $('td', nRow).css('background-color', '#f5e9e6');
                            } else {
                                $('td', nRow).css('background-color', '#f5dfda');
                            }
                        } else {
                            $('td', nRow).css('background-color', '#ffa38a');
                        }
                    }
                }
            };
        </script>
        <style>
            body {
                font-family: "Sarabun", sans-serif !important;
                /*font-family: 'THSarabunNew', sans-serif;*/
            }

            .displayTable {
                width: auto;
                border: 1px solid #ccc;
                border-radius: 4px;
            }

            .displayContain {
                border: 2px solid #ccc;
                border-radius: 0px 0px 5px 5px;
                padding: 20px;
                /*width: fit-content;*/
            }

            /*            td {
                            white-space: nowrap;
                            cursor: pointer;
                        }*/

            .checkData ,.checkAllData{
                width: 20px;
                height: 20px;
            }

            /*            .sts-0{
                            font-size: 20px;
                            color: navy;
                        }
                        .sts-1{
                            font-size: 20px;
                            color: yellow;
                        }
                        .sts-2{
                            font-size: 20px;
                            color: green;
                        }
                        .sts-3{
                            font-size: 20px;
                            color: blue;
                        }
                        .sts-4{
                            font-size: 20px;
                            color: navy;
                        }
                        .sts-8{
                            font-size: 20px;
                            color: black;
                        }*/

            .sts-0{
                font-size: 20px;
                color: navy;
            }
            .sts-1{
                font-size: 20px;
                color: yellow;
            }
            .sts-2{
                font-size: 20px;
                color: green;
            }
            .sts-3{
                font-size: 20px;
                color: blue;
            }
            .sts-4{
                font-size: 20px;
                color: pink;
            }
            .sts-5{
                font-size: 20px;
                color: navy;
            }
            .sts-6{
                font-size: 20px;
                color: gray;
            }
            .sts-7{
                font-size: 20px;
                color: brown;
            }
            .sts-8{
                font-size: 20px;
                color: orange;
            }
            .sts-9{
                font-size: 20px;
                color: black;
            }

            .modal-content{
                width: 50%;
            }

            .splitBtn{
                color: navy;
                cursor: pointer;
            }

            .detailBand{
                text-align: center;
                padding: 5px;
                color: white;
                background-color: navy;
                border-radius: 5px 5px 0px 0px;
            }

            body {
                margin: 0;
            }

            /*            section {
                            height: 2000px;
                            padding-top: 100px;
                        }*/

            .one {
                position: sticky;
                top: 0px;
                background-color: white;
                z-index:99;
            }
            .two {
                position: sticky;
                top: 136px;
                background-color: white;
                z-index:99;
            }
            .three {
                position: sticky;
                top: 184px;
                background-color: white;
                z-index:99;
            }

        </style>
        <script>
            $(document).ready(function () {

//                sessionStorage.setItem('uid', '93147');
//                sessionStorage.setItem('uid', '92655');
                getLevel(sessionStorage.getItem('uid'));
                $('#uid').val(sessionStorage.getItem('uid'));

                $('#showTable').DataTable(tableOptions);

                // Setup - add a text input to each footer cell
                $('#showTable tfoot th').not(":eq(0),:eq(13)").each(function () {
                    var title = $(this).text();
                    $(this).html('<input type="text" />');
                });

                // DataTable
                var table = $('#showTable').DataTable();

                // Apply the search
                table.columns().every(function () {
                    var that = this;

                    $('input', this.footer()).on('keyup change', function () {
                        if (that.search() !== this.value) {
                            that.search(this.value).draw();
                        }
                    });
                });

                getAppHead();
                getDetail();
                getWH();

                $('.checkAllData').click(function () {
                    var checkAll = $(this);
                    $('.checkData').prop("checked", checkAll.prop("checked"));
                });

                $('#updateReqDateBtn').click(function () {

                    var regDate = $('#reqDate').val().toString().replace(/-/g, '');
                    var checkData = $('.checkData:checked');

                    if (checkData.length > 0) {
                        if (regDate === '') {
                            alertify.error('Please select Requested Date!');
                            return false;
                        }

                        for (var i = 0; i < checkData.length; i++) {
                            var lino = $(checkData[i]).val();
                            $.ajax({
                                url: "/TABLE2/ShowDataTablesISM100?mode=updateReqDateDetail"
                                        + "&orderNo=${orderNo}"
                                        + "&seq=${seq}"
                                        + "&lino=" + lino
                                        + "&regDate=" + regDate
                                        + "&uid=" + sessionStorage.getItem('uid')

                            }).done(function (result) {
                                console.log(result);

                            }).fail(function (jqXHR, textStatus, errorThrown) {
                                // needs to implement if it fails
                            });
                        }

                        setTimeout(getDetail, 1000);
                        alertify.success('Update Success!');
                    } else {
                        alertify.alert("No Data Selected!",
                                function () {
                                    alertify.error('Please select data!');
                                }
                        );
                    }

                });

                $('#updateDestBtn').click(function () {

                    var dest = $('#dest').val();
                    var checkData = $('.checkData:checked');

                    if (checkData.length > 0) {
                        if (dest === '') {
                            alertify.error('Please select Destination!');
                            return false;
                        }

                        for (var i = 0; i < checkData.length; i++) {
                            var lino = $(checkData[i]).val();
                            $.ajax({
                                url: "/TABLE2/ShowDataTablesISM100?mode=updateDestDetail"
                                        + "&orderNo=${orderNo}"
                                        + "&seq=${seq}"
                                        + "&lino=" + lino
                                        + "&dest=" + dest
                                        + "&uid=" + sessionStorage.getItem('uid')

                            }).done(function (result) {
                                console.log(result);

                            }).fail(function (jqXHR, textStatus, errorThrown) {
                                // needs to implement if it fails
                            });
                        }

                        setTimeout(getDetail, 1000);
                        getAppHead();
                        alertify.success('Update Success!');
                    } else {
                        alertify.alert("No Data Selected!",
                                function () {
                                    alertify.error('Please select data!');
                                }
                        );
                    }

                });

                $('#reqAppBtn').click(function () {

                    var resText = $('#headRespSty').val();

                    if (resText === "") {

                        $('#headRespSty').css("border", "5px solid red");
                        $('#headRespSty').effect("shake", {times: 3}, 700);
                        var interval = setInterval(function () {
                            $('#headRespSty').css("border", "");
                            clearInterval(interval);
                        }, 700);

                    } else {
                        $('.checkAllData, .checkData').prop('checked', true);
                        var checkData = $('.checkData:checked');

                        if (checkData.length > 0) {

                            for (var i = 0; i < checkData.length; i++) {
                                var lino = $(checkData[i]).val();
                                $.ajax({
                                    url: "/TABLE2/ShowDataTablesISM100?mode=updateReqAppDetail"
                                            + "&orderNo=${orderNo}"
                                            + "&lino=" + lino
                                            + "&seq=" + ${seq}
                                    + "&uid=" + sessionStorage.getItem('uid')

                                }).done(function (result) {
                                    console.log(result);

                                }).fail(function (jqXHR, textStatus, errorThrown) {
                                    // needs to implement if it fails
                                });
                            }

                            setTimeout(getDetail, 1000);
                            alertify.success('Update Success!');
                        } else {
                            alertify.alert("No Data Selected!",
                                    function () {
                                        alertify.error('Please select data!');
                                    }
                            );
                        }
                    }

                });

                $('#respStyBtn').click(function () {
                    //open modal response style
                    $('#respStyModal').modal('show');
                });

                $('#updateRespStyBtn').click(function () {

                    let orderN = $('#headSaleOrder').val();
                    let seq = $('#headSeq').val();
                    let resinput = $('#respStyInput').val();

                    if (resinput === "") {
                        alertify.error("Please Fill Response Style.");
                    } else {
                        $.ajax({
                            url: "/TABLE2/ShowDataTablesISM100",
                            data: {mode: "updateRespStyHead", orderNo: orderN, seq: seq.charCodeAt(0) - 64, resinput: resinput},
//                        contentType: 'application/json; charset=UTF-8',
                            async: false
                        }).done(function (result) {

                            if (result) {
                                location.reload();
                            } else {
                                $('#modalAlrtFailed').modal('show');
                                $('#respStyInput').val("");
                            }

                        }).fail(function (jqXHR, textStatus, errorThrown) {
                            // needs to implement if it fails
                        });
                    }

                });

                $('#updateRemarkCancelBtn').click(function () {

                    var checkData = $('.checkData:checked');
                    var remark = $('#remark').val();

                    if (checkData.length > 0) {

                        for (var i = 0; i < checkData.length; i++) {
                            var lino = $(checkData[i]).val();
                            $.ajax({
                                url: "/TABLE2/ShowDataTablesISM100?mode=updateRemarkCancelDetail"
                                        + "&orderNo=${orderNo}"
                                        + "&lino=" + lino
                                        + "&remark=" + remark

                            }).done(function (result) {
                                console.log(result);

                            }).fail(function (jqXHR, textStatus, errorThrown) {
                                // needs to implement if it fails
                            });
                        }

                        setTimeout(getDetail, 1000);
                        alertify.success('Update Success!');
                    } else {
                        alertify.alert("No Data Selected!",
                                function () {
                                    alertify.error('Please select data!');
                                }
                        );
                    }

                });

                $('#mngAppBtn').click(function () { //Approval By OrderNo , Seq

                    var uid = $('#uid').val();
                    var orderNo = '${orderNo}';

                    $.ajax({
                        url: "/TABLE2/ShowDataTablesISM100?mode=updateMngApp"
                                + "&orderNo=" + orderNo
                                + "&uid=" + uid
                                + "&seq=" + ${seq}

                    }).done(function (result) {
                        console.log(result);

                        setTimeout(getDetail, 1000);
                        getAppHead();
                        alertify.success('Update Success!');

                    }).fail(function (jqXHR, textStatus, errorThrown) {
                        // needs to implement if it fails
                    });

                });

                $('#backWardBtn').click(function () {

                    var uid = $('#uid').val();
                    var orderNo = '${orderNo}';

                    var kpSTS = $('#KEEPSTS').val();

                    if (kpSTS !== "1") { // STS != 1
                        alertify.alert("This Order Status Not Equal 1");
                    } else {

                        $.ajax({
                            url: "/TABLE2/ShowDataTablesISM100?mode=backward"
                                    + "&orderNo=" + orderNo
                                    + "&seq=" + ${seq}
                            + "&uid=" + uid

                        }).done(function (result) {
                            console.log(result);

                            setTimeout(getDetail, 1000);
                            alertify.success('Update Success!');

                        }).fail(function (jqXHR, textStatus, errorThrown) {
                            // needs to implement if it fails
                        });

                    }

                });

                $('#updateMngRemarkCancelBtn').click(function () {

                    var remark = $('#mngRemark').val();
                    var orderNo = '${orderNo}';
                    var uid = $('#uid').val();

                    $.ajax({
                        url: "/TABLE2/ShowDataTablesISM100?mode=updateRemarkCancel"
                                + "&orderNo=" + orderNo
                                + "&remark=" + remark
                                + "&seq=" + ${seq}
                        + "&uid=" + uid

                    }).done(function (result) {
                        console.log(result);

                        setTimeout(getDetail, 1000);
                        alertify.success('Update Success!');

                    }).fail(function (jqXHR, textStatus, errorThrown) {
                        // needs to implement if it fails
                    });

                });

                $('#wh').on('change', function () {
                    var wh = $('#wh').val();
                    getDest(wh);
                });

                $('#whSplit').on('change', function () {
                    var wh = $('#whSplit').val();
                    getDestSplit(wh);
                });

                $('#destBtn').on('click', function () {

                    var checkData = $('.checkData:checked');
                    if (checkData.length > 0) {
                        var wh = $(checkData[0]).attr('wh');
                        $('#wh').val(wh);
                        getDest(wh);

                        $('#destModal').modal('show');
                    } else {
                        alertify.alert("No Data Selected!",
                                function () {
                                    alertify.error('Please select data!');
                                }
                        );
                    }
                });

                $('#reqDateBtn').on('click', function () {
                    $('.checkAllData, .checkData').prop('checked', true);
                    $('#reqDateModal').modal('show');
                });

                $('#qtyNew').on('change keyup', function () {
                    var qtyOld = $('#qtySplit').val();
                    var qtyNew = $('#qtyNew').val();
                    qtyOld = qtyOld.toString().replace(/,/g, '');
                    qtyNew = qtyNew.toString().replace(/,/g, '');

                    if (isNaN(qtyNew)) {
                        alertify.error('Quantity must be a number !');
                        $('#qtyNew').val('');
                    } else {
                        if (parseFloat(qtyNew) > parseFloat(qtyOld)) {
                            alertify.error('Quantity not enough !');
                            $('#qtyNew').val(qtyOld);
                        }
                    }
                });

                $('#updateSplitBtn').click(function () {
                    var orderNo = '${orderNo}';
                    var lino = $('#linoSplit').val();
                    var qtyOld = $('#qtySplit').val();
                    var qtyNew = $('#qtyNew').val();
                    var dest = $('#destSplit').val();
                    var seq = $('#headSeq').val();
                    qtyOld = qtyOld.toString().replace(/,/g, '');
                    qtyNew = qtyNew.toString().replace(/,/g, '');
                    var qtyRem = parseFloat(qtyOld) - parseFloat(qtyNew);
                    var uid = sessionStorage.getItem('uid');

                    if (qtyNew !== '' && dest !== '' && dest !== null) {

                        $.ajax({
                            url: "/TABLE2/ShowDataTablesISM100?mode=splitDetail"
                                    + "&orderNo=" + orderNo
                                    + "&lino=" + lino
                                    + "&qtyOld=" + qtyOld
                                    + "&qtyNew=" + qtyNew
                                    + "&qtyRem=" + qtyRem
                                    + "&dest=" + dest
                                    + "&uid=" + uid
                                    + "&seq=" + (seq.charCodeAt(0) - 64)

                        }).done(function (result) {
                            console.log(result);

                            alertify.success('Split Success!');

                        }).fail(function (jqXHR, textStatus, errorThrown) {
                            // needs to implement if it fails

                            alertify.error('Split failed!');
                        });

                        setTimeout(getDetail, 1000);
                        $('#splitModal').modal('hide');
                    } else {
                        alertify.error('Please fill information !');
                    }
                });

                $('#sentRMBtn').click(function () {
                    console.log("S_RM_BTN");

                    var uid = $('#uid').val();
                    var orderNo = '${orderNo}';

                    var checkData = $('.checkData:checked');

                    if (checkData.length > 0) {

                        var ArrOut = [];

                        for (var i = 0; i < checkData.length; i++) {
                            var ArrIn = {};
                            let line = $(checkData[i]).val();
                            ArrIn ["line"] = line;
                            ArrOut.push(ArrIn);
                        }

                        //showTable
                        $.ajax({
                            url: "/TABLE2/ShowDataTablesISM100",
                            data: {mode: "updateMngAppPerLine", orderNo: orderNo, uid: uid, data: JSON.stringify(ArrOut), seq:${seq}}

                        }).done(function (result) {

                            let complete = true;

                            //index 0 = Head
                            for (var i = 0; i < result.myArrayList.length; i++) {
                                if (i === 0) {
//                                    console.log("update Head : " + result.myArrayList[i].map.upHead);
                                    if (result.myArrayList[i].map.upHead === false) {
                                        complete = result.myArrayList[i].map.upHead;
                                    }
                                } else {
//                                    console.log("line : " + result.myArrayList[i].map.line);
//                                    console.log("update old : " + result.myArrayList[i].map.upRM);
//                                    console.log("update new : " + result.myArrayList[i].map.upSts);
                                    if (result.myArrayList[i].map.upRM === false) {
                                        complete = result.myArrayList[i].map.upRM;
                                    }
                                    if (result.myArrayList[i].map.upSts === false) {
                                        complete = result.myArrayList[i].map.upSts;
                                    }
                                }
                            }

                            if (complete) {
//                                alertify.success('Update Success!');
                                setTimeout(location.reload(), 1000);
                            }

                        }).fail(function (jqXHR, textStatus, errorThrown) {
                            // needs to implement if it fails
                        });

                    } else {
                        alertify.error("Please Select line.");
                    }

                });

                $('#delBtnChild').click(function () {
                    var hSO = $('#headSaleOrder').val();
                    var hSEQ = $('#headSeq').val();
                    $('#DeleteAllModal').modal("show");
                    $('#soText').text(hSO);
                    $('#seqText').text(hSEQ);
                });

                $('#approveDeleteAll').click(function () {

                    var hSO = $('#soText').text();
                    var hSEQ = $('#seqText').text();

                    $.ajax({
                        url: "/TABLE2/ShowDataTablesISM100",
                        data: {mode: "deleteSO", orderNo: hSO, seq: hSEQ.charCodeAt(0) - 64}
                    }).done(function (result) {

                        if (result.map.delH) {
                            alertify.success("Delete Success.");
                            setTimeout(window.location.href = '/TABLE2/ISM100/display', 1000);
                        } else {
                            if (result.map.getSTS === "0") {
                                alertify.error("Delete Failed.");
                            } else {
                                alertify.error("Delete Failed. Status = " + result.map.getSTS);
                            }
                        }


                        console.log(result);
                        console.log(result.map.delH);

                    }).fail(function (jqXHR, textStatus, errorThrown) {
                        // needs to implement if it fails
                    });

                });

                $('#delBtn').click(function () {
                    console.log("DEL_BTN");
                    var uid = $('#uid').val();
                    var orderNo = '${orderNo}';

                    var checkData = $('.checkData:checked');

                    if (checkData.length > 0) {
                        var ArrOut = [];

                        for (var i = 0; i < checkData.length; i++) {
                            var ArrIn = {};
                            let line = $(checkData[i]).val();
                            ArrIn ["line"] = line;
                            ArrOut.push(ArrIn);
                        }

                        //showTable
                        $.ajax({
                            url: "/TABLE2/ShowDataTablesISM100",
                            data: {mode: "deletePerLine", orderNo: orderNo, uid: uid, data: JSON.stringify(ArrOut), seq:${seq}}

                        }).done(function (result) {

                            let complete = true;

                            //index 0 = Head
                            for (var i = 0; i < result.myArrayList.length; i++) {
                                if (i === 0) {
                                    if (result.myArrayList[i].map.delHead === false) {
                                        complete = result.myArrayList[i].map.delHead;
                                    }
                                } else {
                                    if (result.myArrayList[i].map.delOld === false) {
                                        complete = result.myArrayList[i].map.delOld;
                                    }
                                    if (result.myArrayList[i].map.upClear === false) {
                                        complete = result.myArrayList[i].map.upClear;
                                    }
                                }
                            }

                            if (complete) {
//                                alertify.success('Update Success!');
                                setTimeout(window.location.href = '/TABLE2/ISM100/display', 1000);
                            }

                        }).fail(function (jqXHR, textStatus, errorThrown) {
                            // needs to implement if it fails
                        });

                    } else {
                        alertify.error("Please Select line.");
                    }

                });

                $('#closeBtn').click(function () {
                    console.log("CLO_BTN");

                    var uid = $('#uid').val();
                    var orderNo = '${orderNo}';

                    var checkData = $('.checkData:checked');

                    if (checkData.length > 0) {
                        var ArrOut = [];

                        for (var i = 0; i < checkData.length; i++) {
                            var ArrIn = {};
                            let line = $(checkData[i]).val();
                            ArrIn ["line"] = line;
                            ArrOut.push(ArrIn);
                        }

                        //showTable
                        $.ajax({
                            url: "/TABLE2/ShowDataTablesISM100",
                            data: {mode: "closePerLine", orderNo: orderNo, uid: uid, data: JSON.stringify(ArrOut), seq:${seq}}

                        }).done(function (result) {

                            let complete = true;

                            //index 0 = Head
                            for (var i = 0; i < result.myArrayList.length; i++) {
//                                if (i === 0) {
//                                    if (result.myArrayList[i].map.delHead === false) {
//                                        complete = result.myArrayList[i].map.delHead;
//                                    }
//                                } else {
                                if (result.myArrayList[i].map.closeOld === false) {
                                    complete = result.myArrayList[i].map.closeOld;
                                }
                                if (result.myArrayList[i].map.closeNew === false) {
                                    complete = result.myArrayList[i].map.closeNew;
                                }
//                                }
                            }

                            if (complete) {
//                                alertify.success('Update Success!');
                                setTimeout(location.reload(), 1000);
                            }

                        }).fail(function (jqXHR, textStatus, errorThrown) {
                            // needs to implement if it fails
                        });

                    } else {
                        alertify.error("Please Select line.");
                    }

                });

            });

            function getLevel(uid) {
                $.ajax({
                    url: "/TABLE2/ShowDataTablesISM100?mode=getLevel"
                            + "&uid=" + uid

                }).done(function (result) {
                    console.log(result);
                    $('#level').val(result);

                    if (parseInt(result) >= 7) {
                        $('.mngShow').removeClass('hidden');
                    } else {
                        $('.bsnShow').removeClass('hidden');
                    }

                }).fail(function (jqXHR, textStatus, errorThrown) {
                    // needs to implement if it fails
                });
            }

            function getDest(wh) {

                $.ajax({
                    url: "/TABLE2/ShowDataTablesISM100?mode=getDest"
                            + "&wh=" + wh
                }).done(function (result) {
                    console.log(result);

                    $('#dest').html('');
                    var destText = '';

                    for (var i = 0; i < result.length; i++) {
                        destText += '<option value="' + result[i].uid + '">' + result[i].name + '</option>';
                    }

                    $('#dest').html(destText);

                }).fail(function (jqXHR, textStatus, errorThrown) {
                    // needs to implement if it fails
                });
            }

            function getDestSplit(wh) {

                $.ajax({
                    url: "/TABLE2/ShowDataTablesISM100?mode=getDest"
                            + "&wh=" + wh
                }).done(function (result) {
                    console.log(result);

                    $('#destSplit').html('');
                    var destText = '';

                    for (var i = 0; i < result.length; i++) {
                        destText += '<option value="' + result[i].uid + '">' + result[i].name + '</option>';
                    }

                    $('#destSplit').html(destText);

                }).fail(function (jqXHR, textStatus, errorThrown) {
                    // needs to implement if it fails
                });
            }

            function getDetail() {

                var table = $('#showTable').DataTable();
                table.clear().draw();
                $('#loadIcon').removeClass('hidden');

                $('.checkAllData').prop("checked", false);
//                getAppHead();

                $.ajax({
                    url: "/TABLE2/ShowDataTablesISM100?mode=getDetail"
                            + "&orderNo=" + ${orderNo} + "&seq=" + ${seq} + ""
                }).done(function (result) {
                    console.log(result);

                    for (var i = 0; i < result.length; i++) {

                        var wh = '';
                        if (result[i].ISDITNO.toString().trim().charAt(0) === '1') {
                            wh = 'WH2';
                        } else {
                            wh = 'WH3';
                        }

                        var delChild = '';
                        if (result[i].ISDSINO.toString().trim() === "0" && result[i].FAM.toString().trim() !== "") {
                            delChild = '<a class="delChildIcon" onclick="deleteDetChild(\'' + result[i].LINO + '-' + result[i].ISDSINO + '\',\'' + result[i].LINO + '\');"><i class="fa fa-2x fa-refresh splitBtn" aria-hidden="true"></i></a>';
                        } else {
//                            console.log(result[i].STS);
                            if (result[i].STS === "0") {
                                var replacedouble = result[i].ISDDESC.toString().replace('"', '$%').trim(); // replace " to $% Before Send Paremeter
                                delChild = '<a class="splitChildIcon" onclick="splitDet(\'' + result[i].ISDLINO + ' / ' + result[i].ISDSINO + '\',\'' + result[i].ISDITNO.toString().trim() + ' : ' + replacedouble + '\',\'' + result[i].ISDRQQTY + '\',\'' + result[i].LINO + '-' + result[i].ISDSINO + '\',\'' + wh + '\',\'' + result[i].ISDVT.toString().trim() + '\');"><i class="fa fa-2x fa-sitemap splitBtn" aria-hidden="true"></i></a>';
                            } else {
                                delChild = '';
//                                delChild = '<a class="splitChildIcon" onclick="alertify.error(\'Can Not Split.\');"><i class="fa fa-2x fa-sitemap splitBtn" style="color: red;" aria-hidden="true"></i></a>';
                            }

                        }

                        let checkHTML = "";

//                        console.log(" remain " + result[i].ISDREMAIN + " sts " + result[i].ISDSTS);
//                        console.log("cont " + result[i].ISDSTS.toString().includes("Ben"));

                        if (result[i].ISDREMAIN !== "C" && (result[i].ISDSTS.toString().includes("Req") || result[i].ISDSTS.toString().includes("New"))) { // Remain != C and Sts = 0
                            checkHTML = '<input type="checkbox" class="checkData" wh="' + wh + '" value="' + result[i].LINO + '-' + result[i].ISDSINO + '">';
                        }
//                        console.log(checkHTML);

                        if (result[i].ISDREMAIN === "C") {
                            $('#appIcon').attr('src', '../resources/images/ISM/cancelled.png');
                        }

                        //hidden checkbox when press new btn
                        table.row.add([
                            checkHTML,
                            result[i].ISDLINO,
                            result[i].FAM,
                            result[i].ISDITNO,
                            result[i].ISDVT,
                            result[i].ISDDESC,
                            result[i].ISDSAPONH,
                            result[i].ISDWMSONH,
                            result[i].ISDRQQTY,
                            result[i].ISDUNIT,
                            result[i].ISDRQDT,
                            result[i].ISDDEST,
                            '<i class="fa fa-circle' + result[i].STSI + ' sts-' + result[i].STS + '" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="' + result[i].ISDSTS + '"></i>',
                            delChild
                        ]).draw(false);

                    }

                    var level = $('#level').val();
                    if (parseInt(level) >= 7) {
                        $('.delChildIcon, .splitChildIcon').css("display", "none");
                    }
//                    $('.checkAllData, .checkData').prop('disabled', true).prop('checked', true);

                    $('[data-toggle="tooltip"]').tooltip();
                    $('.childSplit').parent().parent().addClass('child-bg');

                    $('#loadIcon').addClass('hidden');

                }).fail(function (jqXHR, textStatus, errorThrown) {
                    // needs to implement if it fails
                });

            }

            function getWH() {

                $.ajax({
                    url: "/TABLE2/ShowDataTablesISM100?mode=getWH"
                }).done(function (result) {
                    console.log(result);

                    $('#wh').html('');
                    var whText = '';

                    for (var i = 0; i < result.length; i++) {
                        whText += '<option value="' + result[i].uid + '">' + result[i].uid + ' : ' + result[i].name + '</option>';
                    }

                    $('#wh').html(whText);
                    $('#whSplit').html(whText);

                }).fail(function (jqXHR, textStatus, errorThrown) {
                    // needs to implement if it fails
                });
            }

            function getAppHead() {

                $.ajax({
                    url: "/TABLE2/ShowDataTablesISM100?mode=getAppHead"
                            + "&orderNo=${orderNo}&seq=" + ${seq} + ""
                }).done(function (result) {
                    console.log(result);

                    $('#headTranDate').val(result.ISHTRDT);
                    $('#headReqDate').val(result.ISHRQDT);
                    $('#headStyleFG').val(result.ISHMATCODE + ' ' + result.ISHLOT);
                    $('#headQty').val(result.ISHAMTFG);
                    $('#headRespSty').val(result.ISHRPUSR);
                    $('#headCustomer').val(result.ISHCUNO + ' ' + result.ISHCUNM1);
                    $('#headDest').val(result.ISHDEST);
                    $('#headSaleOrder').val(result.ISHORD);
                    $('#headSeq').val(String.fromCharCode(parseInt(result.ISHSEQ) + 64));
                    $('#headDocNo').val(result.ISHSUBMI);
                    $('#headRemark').val(result.ISHREMARK);
                    $('#headReason').val(result.ISHREASON);
                    $('#headPOFull').val(result.ISHBSTKD);

                    $('#footReqUser').html(result.ISHEUSR);
                    $('#footReqDate').html(result.ISHEDT);
                    $('#footAppUser').html(result.ISHAPUSR);
                    $('#footAppDate').html(result.ISHAPDT);

                    $('#KEEPSTS').val(result.LSTS);

                    if (result.LSTS === result.HSTS) {
                        if (result.LSTS === '2' || result.LSTS === '3' || result.LSTS === '4' || result.LSTS === '5' || result.LSTS === '6') {
                            $('#appIcon').attr('src', '../resources/images/ISM/approved.png');
                        } else if (result.LSTS === '8') {
                            $('#appIcon').attr('src', '../resources/images/ISM/cancelled.png');
                        }
                    }

                }).fail(function (jqXHR, textStatus, errorThrown) {
                    // needs to implement if it fails
                });

                $.ajax({
                    url: "/TABLE2/ShowDataTablesISM100",
                    data: {mode: "checkRMsts", orderNo: "${orderNo}", seq: "${seq}"},
                    async: false
                }).done(function (result) {

                    if (result) {
                        $('#sentRMBtn').show();
                        $('#delBtn').show();
                        $('#closeBtn').show();
                        $('#reqAppBtn').hide();
                        $('#respStyBtn').hide();
                        $('#delBtnChild').hide();
                    } else {
                        $('#sentRMBtn').hide();
                        $('#delBtn').hide();
                        $('#closeBtn').hide();
                    }

                }).fail(function (jqXHR, textStatus, errorThrown) {
                    // needs to implement if it fails
                });
            }

            function splitDet(lineNo, matCode, qty, lino, wh, vt) {
                $('#whSplit').val(wh);
                getDestSplit(wh);

                $('#linoText').html(lineNo);
                $('#matCodeText').html(matCode.toString().replace('$%', '"')); // replace back
                $('#valueType').html(vt);
                $('#qtyText').html(qty);
                $('#qtySplit').val(qty);
                $('#qtyNew').val('');
                $('#linoSplit').val(lino);

                $('#splitModal').modal('show');
            }

            function deleteDetChild(lino, li) {

                $('#deleteChildLino').val(lino);
                $('#noDel').html(parseInt(li));
                $('#deleteChildModal').modal('show');
            }

            function deleteChild() {
                var orderNo = '${orderNo}';
                var lino = $('#deleteChildLino').val();

                $.ajax({
                    url: "/TABLE2/ShowDataTablesISM100?mode=deleteDetailChild"
                            + "&orderNo=" + orderNo
                            + "&lino=" + lino

                }).done(function (result) {
                    console.log(result);

                    alertify.success('Delete splited lines Success!');

                }).fail(function (jqXHR, textStatus, errorThrown) {
                    // needs to implement if it fails

                    alertify.error('Delete splited lines failed!');
                });

                setTimeout(getDetail, 1000);
            }

        </script>
    </head>

    <body>
        <div id="wrapper">

            <!-- /#sidebar-wrapper -->
            <%@ include file="../fragments/sidebar.jsp" %>

            <!-- Page Content -->
            <!-- nav-head-custom -->
            <%@ include file="../fragments/nav_head.jsp" %>

            <div class="container-fluid">
                <!--<div id="wrapper-top">-->
                <%-- PART 2 --%>
                <!--                    <div class="row">
                                                <div class="col-lg-12">
                                                    <div id="set-height" style="height:415px;margin-top:0px;">
                                                        <div id="sidebar-wrapper-top" class="">
                                                            <b class="page-header" style="padding-left:5px;font-size:18px;">
                <%-- <hr style="margin:0px;margin-top:10px;margin-bottom:10px;"> --%>
            </b>
        </div>
    </div>
    </div>
    </div> -->
                <%-- PART 3 --%>

                <br>
                <input type="hidden" id="uid">
                <input type="hidden" id="level">
                <input type="hidden" id="KEEPSTS">

                <!--<div class="displayContain">-->

                <div class="one">
                    <div class="detailBand">
                        <h3 style="margin: 0;">DETAIL DISPLAY </h3>
                    </div>
                    <!--<div class="displayContain">-->
                    <table width="100%">
                        <tr>
                            <th width="5%"></th>
                            <th width="9%">
                                Transaction Date :
                            </th>
                            <th width="14%">
                                <input type="text" id="headTranDate" style="width: 150px;" readonly>
                            </th>
                            <th width="6%">
                                Style FG :
                            </th>
                            <th width="13%">
                                <input type="text" id="headStyleFG" style="width: 200px;" readonly>
                            </th>
                            <th width="9%">
                                Customer :
                            </th>
                            <th width="18%">
                                <input type="text" id="headCustomer" style="width: 300px;" readonly>
                            </th>
                            <th width="7%">
                                Sale Order :
                            </th>
                            <th width="13%">
                                <input type="text" id="headSaleOrder" style="width: 145px;" readonly>
                                <input type="text" id="headSeq" style="width: 50px;" readonly>
                            </th>
                            <th width="6%"></th>
                            <th width="16%">
                                <input type="text" id="headReason" style="width: 145px;" readonly>
                            </th>
                        </tr>
                        <tr>
                            <th></th>
                            <th>
                                Requested Date :
                            </th>
                            <th>
                                <input type="text" id="headReqDate" style="width: 150px;" readonly>
                            </th>
                            <th>
                                Quantity :
                            </th>
                            <th>
                                <input type="text" id="headQty" style="width: 200px;" readonly>
                            </th>
                            <th>
                                Response Style :
                            </th>
                            <th>
                                <!--<input type="text" id="headRespSty" style="width: 300px;" readonly>-->
                                <input type="text" id="headRespSty" style="width: 300px;" placeholder="ระบุชื่อและเบอร์โทร" readonly>
                            </th>
                            <th>
                                Destination :
                            </th>
                            <th>
                                <input type="text" id="headDest" style="width: 150px;" readonly>
                            </th>
                            <th>
                                Doc no. :
                            </th>
                            <th>
                                <input type="text" id="headDocNo" style="width: 200px;" readonly>
                            </th>
                        </tr>
                    </table>
                    <!--</div>-->
                </div>

                <div class="two">
                    <div class="bsnShow hidden">
                        <table width="100%">
                            <tr>
                                <td width="50%">
                                    <button type="button" id="reqDateBtn" class="btn btn-info" style="width: 150px;">Requested Date</button>
                                    <button type="button" id="destBtn" class="btn btn-warning" style="width: 150px;">Destination</button>
                                    <button type="button" id="reqAppBtn" class="btn btn-success" style="width: 150px;">Request Approval</button>
                                    <button type="button" id="respStyBtn" class="btn btn-primary" style="width: 150px;">Response Style</button>
                                    <button type="button" id="delBtnChild" class="btn btn-danger" style="width: 150px;">Delete</button>
                                    <button type="button" id="cancelBtn" class="btn btn-danger hidden" style="width: 150px;" data-toggle="modal" data-target="#cancelModal">Cancel</button>
                                    <input type="text" id="" style="width: 10px; border: none;" readonly>
                                </td>
                                <td width="50%" style="text-align: right;">
                                    <button type="button" id="sentRMBtn" class="btn btn-warning" style="width: 150px;">Send ST.RM</button>
                                    <button type="button" id="delBtn" class="btn btn-danger" style="width: 150px;">Delete</button>
                                    <button type="button" id="closeBtn" class="btn btn-info" style="width: 150px;">Close</button>
                                    <button type="button" id="backBtn" class="btn" style="width: 150px;" onclick="window.location.href = 'display';">Back</button>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="mngShow hidden">
                        <table width="100%">
                            <tr>
                            <tr>
                                <th width="28%">
                                    <button type="button" id="mngAppBtn" class="btn btn-success" style="width: 150px;">Approval</button>
                                    <button type="button" id="mngCancelBtn" class="btn btn-danger" style="width: 150px;" data-toggle="modal" data-target="#mngCancelModal">Cancel</button>
                                    <button type="button" id="backWardBtn" style="background-color : #F071F4; color: white; width: 150px;" class="btn">Backward</button>
                                </th>
                                <th width="18%">
                                    <b>Remark : </b> 
                                    <input type="text" id="headRemark" style="width: 255px;" readonly>
                                </th>
                                <th width="28%">
                                    <b>Text : </b>
                                    <input type="text" id="headPOFull" style="width: 350px;" readonly>
                                </th>
                                <th width="7%"></th>
                                <th width="13%"></th>
                                <th width="6%"></th>
                                <th width="16%" style="text-align: right;">
                                    <button type="button" id="backBtn" class="btn" style="width: 150px;" onclick="window.location.href = 'display';">Back</button>
                                </th>
                            </tr>
                        </table>
                    </div>
                </div>
                <!--</div>-->

                <table id="showTable" class="display displayTable" style="width: 100%;"> 
                    <thead class="three">
                        <tr>
                            <th style="width:39px;">
                                <input type="checkbox" class="checkAllData" >
                            </th>
                            <th style="width:51px;">No</th>
                            <th style="width:24px;"></th>
                            <th style="width:157px;">Material Code</th>
                            <th style="width:50px;">VT</th>
                            <th style="width:135px;">Description</th>
                            <th style="width:116px;">SAP ONH</th>
                            <th style="width:124px;">WMS ONH</th>
                            <th style="width:167px;">Requested Qty</th>
                            <th style="width:63px;">Unit</th>
                            <th style="width:177px;">Requested Date</th>
                            <th style="width:134px;">Destination</th>
                            <th style="width:86px;">Status</th>
                            <th style="width:78px;">Option</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th style="width:39px;"></th>
                            <th style="width:51px;"></th>
                            <th style="width:24px;"></th>
                            <th style="width:157px;"></th>
                            <th style="width:50px;"></th>
                            <th style="width:135px;"></th>
                            <th style="width:116px;"></th>
                            <th style="width:124px;"></th>
                            <th style="width:167px;"></th>
                            <th style="width:63px;"></th>
                            <th style="width:177px;"></th>
                            <th style="width:134px;"></th>
                            <th style="width:86px;"></th>
                            <th style="width:78px;"></th>
                        </tr>
                    </tfoot>
                    <tbody class="sticky3tbody">
                    </tbody>
                </table>

                <center>
                    <div id="loadIcon" class="hidden">
                        <h3>
                            Loading...
                            <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
                        </h3>
                    </div>
                </center>

                <!-- Modal -->
                <div class="modal fade" id="mngCancelModal" role="dialog">
                    <div class="modal-dialog">
                        <center>
                            <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">กำหนดหมายเหตุ</h4>
                                </div>
                                <div class="modal-body">
                                    <h4>Remark : <input type="text" id="mngRemark"></h4>
                                </div>
                                <div class="modal-footer">
                                    <center>
                                        <button type="button" id="updateMngRemarkCancelBtn" class="btn btn-info" data-dismiss="modal">Enter</button>
                                    </center>
                                </div>
                            </div>
                        </center>
                    </div>
                </div>

                <!-- Modal -->
                <div class="modal fade" id="deleteChildModal" role="dialog">
                    <div class="modal-dialog">
                        <center>
                            <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">Delete splited lines ?</h4>
                                    <input type="hidden" id="deleteChildLino">
                                </div>
                                <div class="modal-body">
                                    <h4>ลบรายการที่ถูกแบ่งออก No. <span id="noDel"></span></h4>
                                </div>
                                <div class="modal-footer">
                                    <center>
                                        <button type="button" style="width: 100px;" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                                        <button type="button" style="width: 100px;" id="deleteChildBtn" onclick="deleteChild()" class="btn btn-success" data-dismiss="modal">OK</button>
                                    </center>
                                </div>
                            </div>
                        </center>
                    </div>
                </div>

                <!-- Modal -->
                <div class="modal fade" id="cancelModal" role="dialog">
                    <div class="modal-dialog">
                        <center>
                            <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">กำหนดหมายเหตุ</h4>
                                </div>
                                <div class="modal-body">
                                    <h4>Remark : <input type="text" id="remark"></h4>
                                </div>
                                <div class="modal-footer">
                                    <center>
                                        <button type="button" id="updateRemarkCancelBtn" class="btn btn-info" data-dismiss="modal">Enter</button>
                                    </center>
                                </div>
                            </div>
                        </center>
                    </div>
                </div>

                <!-- Modal -->
                <div class="modal fade" id="reqDateModal" role="dialog">
                    <div class="modal-dialog">
                        <center>
                            <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">กำหนดวันที่ต้องการวัตถุดิบ</h4>
                                </div>
                                <div class="modal-body">
                                    <h4>Requested Date : <input type="date" id="reqDate"></h4>
                                </div>
                                <div class="modal-footer">
                                    <center>
                                        <button type="button" id="updateReqDateBtn" class="btn btn-info" data-dismiss="modal">Enter</button>
                                    </center>
                                </div>
                            </div>
                        </center>
                    </div>
                </div>

                <!-- Modal -->
                <div class="modal fade" id="respStyModal" role="dialog">
                    <div class="modal-dialog">
                        <center>
                            <!-- Modal content-->
                            <div class="modal-content" style="position: absolute; left: 25em; top: 12em;">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                </div>
                                <div class="modal-body">
                                    <h4>Response Style : <input placeholder="ระบุชื่อและเบอร์โทร" type="text" style="width:50%;" id="respStyInput" maxlength="20" class="form-control"></h4>
                                </div>
                                <div class="modal-footer">
                                    <center>
                                        <button type="button" id="updateRespStyBtn" class="btn btn-info" data-dismiss="modal">Enter</button>
                                    </center>
                                </div>
                            </div>
                        </center>
                    </div>
                </div>

                <!-- Modal -->
                <center style="font-size: 23px;">
                    <div id="modalAlrtFailed" class="modal fade" role="dialog">
                        <div class="modal-dialog">
                            <div class="modal-lg">
                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title" style="font-size: 30px;">Error !!</h4>
                                    </div>

                                    <div class="modal-body">
                                        <label>แก้ไข Response Style ไม่สำเร็จ.</label>
                                    </div>

                                    <div class="modal-footer" style="text-align : center">
                                        <input type="button" style="font-size: 18px;" data-dismiss="modal" class="btn btn-success"  value="OK">
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </center>

                <!-- Modal -->
                <div class="modal fade" id="destModal" role="dialog">
                    <div class="modal-dialog">
                        <center>
                            <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">กำหนดสถานที่ส่งปลายทาง</h4>
                                </div>
                                <div class="modal-body">
                                    <h4 hidden>
                                        Warehouse :
                                        <select id="wh" style="width: 300px;"></select>
                                    </h4>
                                    <h4>
                                        Destination :
                                        <select id="dest" style="width: 300px;"></select>
                                    </h4>
                                </div>
                                <div class="modal-footer">
                                    <center>
                                        <button type="button" id="updateDestBtn" class="btn btn-warning" data-dismiss="modal">Enter</button>
                                    </center>
                                </div>
                            </div>
                        </center>
                    </div>
                </div>

                <!-- Modal -->
                <div class="modal fade" id="DeleteAllModal" role="dialog">
                    <div class="modal-dialog">
                        <center>
                            <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title" style="font-size: 26px;" >Delete Data ?</h4>
                                </div>
                                <div class="modal-body" style="font-size: 26px;">
                                    Sales Order : <label id="soText"></label> &nbsp; Seq : <label id="seqText"></label>
                                </div>
                                <div class="modal-footer">
                                    <center>
                                        <button type="button" id="approveDeleteAll" class="btn btn-success" data-dismiss="modal" style="font-size: 17px;">Confirm</button>
                                        <button type="button" id="" class="btn btn-danger" data-dismiss="modal" style="font-size: 17px;">Cancel</button>
                                    </center>
                                </div>
                            </div>
                        </center>
                    </div>
                </div>

                <!-- Modal -->
                <div class="modal fade" id="splitModal" role="dialog">
                    <div class="modal-dialog">
                        <center>
                            <!-- Modal content-->
                            <div class="modal-content" style="width: fit-content;">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">Split Line</h4>
                                </div>
                                <div class="modal-body">
                                    <table width="100%">
                                        <tr>
                                            <td width="30%">
                                                <h4>ลำดับที่ : </h4>
                                            </td>
                                            <td colspan="2" width="60%">
                                                <span id="linoText">4 / 0</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="30%">
                                                <h4>รหัสวัตถุดิบ : </h4>
                                            </td>
                                            <td colspan="2" width="60%">
                                                <span id="matCodeText">1FMT0936 NO.212 WIRE-5 (UTT)</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="30%">
                                                <h4>VT : </h4>
                                            </td>
                                            <td colspan="2" width="60%">
                                                <span id="valueType"></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="30%">
                                                <h4>ปริมาณที่ต้องการ : </h4>
                                            </td>
                                            <td width="30%" style="text-align: center;">
                                                <h4>เดิม</h4>
                                            </td>
                                            <td width="30%" style="text-align: center;">
                                                <h4>ใหม่</h4>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="30%"></td>
                                            <td width="30%" style="text-align: center;">
                                                <span id="qtyText">4,300.00</span>
                                            </td>
                                            <td width="30%" style="text-align: center;">
                                                <input type="text" id="qtyNew" style="text-align: center;">
                                            </td>
                                        </tr>
                                    </table>
                                    <hr>
                                    <h4 hidden>
                                        Warehouse :
                                        <select id="whSplit" style="width: 300px;"></select>
                                    </h4>
                                    <h4>
                                        Destination :
                                        <select id="destSplit" style="width: 300px;"></select>
                                    </h4>
                                    <input type="hidden" id="qtySplit">
                                    <input type="hidden" id="linoSplit">
                                </div>
                                <div class="modal-footer">
                                    <center>
                                        <button type="button" id="updateSplitBtn" class="btn btn-warning" >Enter</button>
                                    </center>
                                </div>
                            </div>
                        </center>
                    </div>
                </div>

                <hr>
                <center>
                    <table width="100%">
                        <tr>
                            <th width="15%" style="text-align: center; border-bottom: 1px dashed black;">
                                <span id="footReqUser"></span>
                            </th>
                            <th width="1%" style="text-align: center; border-bottom: 1px dashed black;">
                                /
                            </th>
                            <th width="15%" style="text-align: center; border-bottom: 1px dashed black;">
                                <span id="footReqDate"></span>
                            </th>
                            <th width="5%" style="text-align: center;">
                                <b style="opacity: 0;">SPACE</b>
                            </th>
                            <th width="15%" style="text-align: center; border-bottom: 1px dashed black;">
                                <span id="footAppUser"></span>
                            </th>
                            <th width="1%" style="text-align: center; border-bottom: 1px dashed black;">
                                /
                            </th>
                            <th width="15%" style="text-align: center; border-bottom: 1px dashed black;">
                                <span id="footAppDate"></span>
                            </th>
                            <th width="10%" rowspan="2">
                                <img id="appIcon" style="width: 270px; height: 200px;">
                            </th>
                        </tr>
                        <tr>
                            <th style="text-align: center; vertical-align: text-top;">
                                ผู้บันทึก
                            </th>
                            <th style="text-align: center; vertical-align: text-top;">

                            </th>
                            <th style="text-align: center; vertical-align: text-top;">
                                วันที่ขออนุมัติ
                            </th>
                            <th style="text-align: center; vertical-align: text-top;">

                            </th>
                            <th style="text-align: center; vertical-align: text-top;">
                                ผู้อนุมัติ
                            </th>
                            <th style="text-align: center; vertical-align: text-top;">

                            </th>
                            <th style="text-align: center; vertical-align: text-top;">
                                วันที่อนุมัติ
                            </th>
                        </tr>
                    </table>
                </center>
                <!--**********************-->
                <!--End Part 3-->
                <!--</div>  end #wrapper-top-->
                <!--</div>-->

                <br>
            </div> <!-- end .container-fluid -->
        </div> <!-- end #wrapper -->
    </body>

</html>