<%@ include file="../fragments/taglibs.jsp" %>
<%@ include file="../fragments/imports.jsp" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.sql.*;" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>WMS</title>
        <!-- css :: vendors -->
        <jsp:include page = "../fragments/css.jsp" />
        <!-- additional custom :: my-style -->
        <link rel="stylesheet" href="../resources/assets/styles/myStyles.css">

        <!-- js :: vendors -->
        <jsp:include page = "../fragments/script.jsp" />
        <!-- additional custom :: my-script -->
        <script src="../resources/assets/scripts/myScripts.js" async></script>
        <script src="../resources/assets/scripts/toggleLoad.js" async></script>  
        <!--<script src="../../WMSMenuControl/resources/assets/scripts/isLogin.js"></script>-->  
        <script>
            $(document).ready(function () {
                $('#showTable').DataTable({
                    "paging": false,
                    "ordering": false,
                    "lengthMenu": [[10, 25, 50, 100], [10, 25, 50, 100]],
                    "bSortClasses": false,
                    columnDefs: [
//                        {orderable: false, targets: [10]},
                        {"width": "10%", "targets": 3},
                        {"width": "30%", "targets": 10},
                        {"width": "7%", "targets": 11},
//                        {"width": "10%", "targets": 11},
//                        {"width": "10%", "targets": 12},
//                        {"width": "10%", "targets": 13},
                    ]
                });

                // Setup - add a text input to each footer cell
//                $('#showTable tfoot th').each(function () {
//                    var title = $(this).text();
//                    $(this).html('<input type="text" />');
//                });
                // DataTable
//                var table = $('#showTable').DataTable();
                // Apply the search
//                table.columns().every(function () {
//                    var that = this;
//                    $('input', this.footer()).on('keyup change', function () {
//                        if (that.search() !== this.value) {
//                            that
//                                    .search(this.value)
//                                    .draw();
//                        }
//                    });
//                });
            });
        </script>
        <style>
            input[type=text], select {
                width: 100%;
                padding: 5px 5px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }

            input[type=date], input[type=number]{
                /*width: 100%;*/
                padding: 5px 5px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }

            input[type=submit] {
                width: 100%;
                background-color: #00399b;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=submit]:hover {
                background-color: #3973d6;
            }

            button[type=submit] {
                background: transparent;
                border: none !important;
            }

            i[id=ic]:hover {
                background-color: #042987;
                border-radius: 15px;
            }

            i[id=ic2]:hover {
                background-color: #a12828;
                border-radius: 15px;
            }
        </style>
        <style>
            body {font-family: Arial, Helvetica, sans-serif;}

            /* The Modal (background) */
            .modal {
                display: none; /* Hidden by default */
                position: fixed; /* Stay in place */
                z-index: 1; /* Sit on top */
                padding-top: 100px; /* Location of the box */
                left: 0;
                top: 0;
                width: 100%; /* Full width */
                height: 100%; /* Full height */
                overflow: auto; /* Enable scroll if needed */
                background-color: rgb(0,0,0); /* Fallback color */
                background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
            }

            /* Modal Content */
            .modal-content {
                background-color: #fefefe;
                margin: auto;
                padding: 20px;
                border: 1px solid #888;
                width: fit-content;
                height: 320px;
            }

            /* The Close Button */
            .close {
                color: #aaaaaa;
                float: right;
                font-size: 28px;
                font-weight: bold;
            }

            .close:hover,
            .close:focus {
                color: #000;
                text-decoration: none;
                cursor: pointer;
            }
        </style>
        <style>
            body {font-family: Arial, Helvetica, sans-serif;}

            /* The Modal (background) */
            .modal {
                display: none; /* Hidden by default */
                position: fixed; /* Stay in place */
                z-index: 1; /* Sit on top */
                padding-top: 100px; /* Location of the box */
                left: 0;
                top: 0;
                width: 100%; /* Full width */
                height: 100%; /* Full height */
                overflow: auto; /* Enable scroll if needed */
                background-color: rgb(0,0,0); /* Fallback color */
                background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
            }

            /* Modal Content */
            .modal-content {
                background-color: #fefefe;
                margin: auto;
                padding: 20px;
                border: 1px solid #888;
                width: fit-content;
                height: 320px;
            }

            /* The Close Button */
            .close {
                color: #aaaaaa;
                float: right;
                font-size: 28px;
                font-weight: bold;
            }

            .close:hover,
            .close:focus {
                color: #000;
                text-decoration: none;
                cursor: pointer;
            }
        </style>
        <script>
            var rowIDX = ${rowIDX};

            function EditDet(idx) {
                var type = 'hidden';
                var dis = '';

                if (document.getElementById('sender-' + idx).type === 'hidden') {
                    type = 'text';
                    dis = 'none';
                } else if (document.getElementById('sender-' + idx).type === 'text') {
                    type = 'hidden';
                    dis = '';
                }

                document.getElementById('sender-' + idx).type = type;
                document.getElementById('rec-' + idx).type = type;
                document.getElementById('dept-' + idx).type = type;
                document.getElementById('desc-' + idx).type = type;
                document.getElementById('bag-' + idx).type = type;
                document.getElementById('roll-' + idx).type = type;
                document.getElementById('box-' + idx).type = type;
                document.getElementById('pcs-' + idx).type = type;
                document.getElementById('tot-' + idx).type = type;
                document.getElementById('totdoc-' + idx).type = type;
                document.getElementById('remark-' + idx).type = type;

                document.getElementById('Dsender-' + idx).style.display = dis;
                document.getElementById('Drec-' + idx).style.display = dis;
                document.getElementById('Ddept-' + idx).style.display = dis;
                document.getElementById('Ddesc-' + idx).style.display = dis;
                document.getElementById('Dbag-' + idx).style.display = dis;
                document.getElementById('Droll-' + idx).style.display = dis;
                document.getElementById('Dbox-' + idx).style.display = dis;
                document.getElementById('Dpcs-' + idx).style.display = dis;
                document.getElementById('Dtot-' + idx).style.display = dis;
                document.getElementById('Dtotdoc-' + idx).style.display = dis;
                document.getElementById('Dremark-' + idx).style.display = dis;
            }

            function DeleteDet(x) {
                document.getElementById('preDel').value = x.id;
                document.getElementById('myModal-del').style.display = 'block';
            }

            function DeleteDet2() {
                var xid = document.getElementById('preDel').value;
                var x = document.getElementById(xid);
                DeleteDet3(x);
            }

            function DeleteDet3(x) {
                var table = $('#showTable').DataTable();
                table.row($(x).parents('tr')).remove().draw();
                SumOnAdd();
                document.getElementById('myModal-del').style.display = 'none';
            }

            function UpdateDIV(x) {
                document.getElementById('D' + x.id).innerHTML = '&nbsp;' + x.value;
            }

            function UpdateDIVright(x) {
                document.getElementById('D' + x.id).innerHTML = x.value + '&nbsp;';
            }

            function AddDet() {
                var sender = document.getElementById('add-sender').value;
                var rec = document.getElementById('add-rec').value;
                var dept = document.getElementById('add-dept').value;
                var desc = document.getElementById('add-desc').value;
                var bag = document.getElementById('add-bag').value;
                var roll = document.getElementById('add-roll').value;
                var box = document.getElementById('add-box').value;
                var pcs = document.getElementById('add-pcs').value;
                var tot = document.getElementById('add-tot').value;
                var totdoc = document.getElementById('add-totdoc').value;
                var remark = document.getElementById('add-remark').value;

                if (sender.toString().trim() === "") {
                    document.getElementById('myModal-3').style.display = 'block';
                } else {
                    var table = $('#showTable').DataTable();

                    table.row.add([
                        '<div style="text-align: left;" id="Dsender-' + rowIDX + '">&nbsp;' + sender + '</div><input type="hidden" id="sender-' + rowIDX + '" name="sender" value="' + sender + '" onkeyup="this.value = this.value.toUpperCase(); NextField(event, \'rec-' + rowIDX + '\');" onchange="UpdateDIV(this);">'
                                , '<div style="text-align: left;" id="Drec-' + rowIDX + '">&nbsp;' + rec + '</div><input type="hidden" id="rec-' + rowIDX + '" name="rec" value="' + rec + '" onkeyup="NextField(event, \'dept-' + rowIDX + '\');" onchange="UpdateDIV(this); SelectDeptEdit(this.value,' + rowIDX + ');">'
                                , '<div style="text-align: left;" id="Ddept-' + rowIDX + '">&nbsp;' + dept + '</div><input type="hidden" id="dept-' + rowIDX + '" name="dept" value="' + dept + '" list="sellerList" onkeyup="NextField(event, \'desc-' + rowIDX + '\');" onchange="UpdateDIV(this); SelectDeptEdit(this.value,' + rowIDX + ');">'
                                , '<div style="text-align: left;" id="Ddesc-' + rowIDX + '">&nbsp;' + desc + '</div><input type="hidden" id="desc-' + rowIDX + '" name="desc" value="' + desc + '" list="descList-' + rowIDX + '" onkeyup="NextField(event, \'bag-' + rowIDX + '\');" onchange="UpdateDIV(this);"><dataList name="descList-' + rowIDX + '" id="descList-' + rowIDX + '" ></dataList>'
                                , '<div style="text-align: right;" id="Dbag-' + rowIDX + '">' + bag + '&nbsp;</div><input type="hidden" id="bag-' + rowIDX + '" name="bag" value="' + bag + '" style="text-align: right;" onkeyup="NextField(event, \'roll-' + rowIDX + '\');" onchange="FormatAdd(this.id, 0); SumOnEdit(' + rowIDX + '); SumOnAdd(); UpdateDIVright(this);">'
                                , '<div style="text-align: right;" id="Droll-' + rowIDX + '">' + roll + '&nbsp;</div><input type="hidden" id="roll-' + rowIDX + '" name="roll" value="' + roll + '" style="text-align: right;" onkeyup="NextField(event, \'box-' + rowIDX + '\');" onchange="FormatAdd(this.id, 0); SumOnEdit(' + rowIDX + '); SumOnAdd(); UpdateDIVright(this);">'
                                , '<div style="text-align: right;" id="Dbox-' + rowIDX + '">' + box + '&nbsp;</div><input type="hidden" id="box-' + rowIDX + '" name="box" value="' + box + '" style="text-align: right;" onkeyup="NextField(event, \'pcs-' + rowIDX + '\');" onchange="FormatAdd(this.id, 0); SumOnEdit(' + rowIDX + '); SumOnAdd(); UpdateDIVright(this);">'
                                , '<div style="text-align: right;" id="Dpcs-' + rowIDX + '">' + pcs + '&nbsp;</div><input type="hidden" id="pcs-' + rowIDX + '" name="pcs" value="' + pcs + '" style="text-align: right;" onkeyup="NextField(event, \'totdoc-' + rowIDX + '\');" onchange="FormatAdd(this.id, 0); SumOnEdit(' + rowIDX + '); SumOnAdd(); UpdateDIVright(this);">'
                                , '<div style="text-align: right;" id="Dtot-' + rowIDX + '">' + tot + '&nbsp;</div><input type="hidden" id="tot-' + rowIDX + '" name="tot" value="' + tot + '" style="text-align: right; background-color: #F0F0F0;" readonly onchange="FormatAdd(this.id, 0); SumOnAdd(); UpdateDIVright(this);">'
                                , '<div style="text-align: right;" id="Dtotdoc-' + rowIDX + '">' + totdoc + '&nbsp;</div><input type="hidden" id="totdoc-' + rowIDX + '" name="totdoc" value="' + totdoc + '" style="text-align: right;" onkeyup="NextField(event, \'remark-' + rowIDX + '\');" onchange="FormatAdd(this.id, 0); SumOnAdd(); UpdateDIVright(this);">'
                                , '<div style="text-align: left;" id="Dremark-' + rowIDX + '">&nbsp;' + remark + '</div><input type="hidden" id="remark-' + rowIDX + '" name="remark" value="' + remark + '" onkeyup="NextField(event, \'invno-' + rowIDX + '\');" onchange="UpdateDIV(this);">'
                                , '<a onclick="EditDet(' + rowIDX + ');" style="cursor: pointer;"><i class="fa fa-edit" style="font-size:25px; padding-left: 10px"></i></a>'
                                + '<a id="del-' + rowIDX + '" onclick="DeleteDet(this);" style="cursor: pointer;"><i class="fa fa-trash" style="font-size:25px; padding-left: 10px"></i></a>'
                    ]).draw(false);

                    rowIDX++;

                    document.getElementById('add-sender').value = '';
                    document.getElementById('add-rec').value = '';
                    document.getElementById('add-dept').value = '';
                    document.getElementById('add-desc').value = '';
                    document.getElementById('add-bag').value = '';
                    document.getElementById('add-roll').value = '';
                    document.getElementById('add-box').value = '';
                    document.getElementById('add-pcs').value = '';
                    document.getElementById('add-tot').value = '';
                    document.getElementById('add-totdoc').value = '';
                    document.getElementById('add-remark').value = '';

                    document.getElementById('add-sender').focus();

                    document.getElementById('formData').submit();
                }
            }

            function currencyFormat(num, fp) {
                var pp = Math.pow(10, fp);
                num = Math.round(num * pp) / pp;
                return num.toFixed(fp).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
            }

            function FormatAdd(id, fp) {
                var x = parseFloat(document.getElementById(id).value.replace(/,/g, ''));
                if (isNaN(x)) {
                    x = 0;
                }
                document.getElementById(id).value = currencyFormat(x, fp);

            }

            function SumOnEdit(idx) {
                var bag = parseFloat(document.getElementById('bag-' + idx).value.replace(/,/g, ''));
                var roll = parseFloat(document.getElementById('roll-' + idx).value.replace(/,/g, ''));
                var box = parseFloat(document.getElementById('box-' + idx).value.replace(/,/g, ''));
                var pcs = parseFloat(document.getElementById('pcs-' + idx).value.replace(/,/g, ''));

                if (isNaN(bag)) {
                    bag = 0;
                }
                if (isNaN(roll)) {
                    roll = 0;
                }
                if (isNaN(box)) {
                    box = 0;
                }
                if (isNaN(pcs)) {
                    pcs = 0;
                }

                document.getElementById('tot-' + idx).value = currencyFormat((bag + roll + box + pcs), 0);
                document.getElementById('Dtot-' + idx).innerHTML = currencyFormat((bag + roll + box + pcs), 0) + '&nbsp;';
            }

            function SumOnAdd() {
                var bag = parseFloat(document.getElementById('add-bag').value.replace(/,/g, ''));
                var roll = parseFloat(document.getElementById('add-roll').value.replace(/,/g, ''));
                var box = parseFloat(document.getElementById('add-box').value.replace(/,/g, ''));
                var pcs = parseFloat(document.getElementById('add-pcs').value.replace(/,/g, ''));
                var tot = parseFloat(document.getElementById('add-tot').value.replace(/,/g, ''));
                var totdoc = parseFloat(document.getElementById('add-totdoc').value.replace(/,/g, ''));

                if (isNaN(bag)) {
                    bag = 0;
                }
                if (isNaN(roll)) {
                    roll = 0;
                }
                if (isNaN(box)) {
                    box = 0;
                }
                if (isNaN(pcs)) {
                    pcs = 0;
                }
                if (isNaN(tot)) {
                    tot = 0;
                }
                if (isNaN(totdoc)) {
                    totdoc = 0;
                }

                document.getElementById('add-tot').value = currencyFormat((bag + roll + box + pcs), 0);
                tot = parseFloat(document.getElementById('add-tot').value.replace(/,/g, ''));

                var input = document.getElementsByTagName('input');
                var sumBag = 0;
                var sumRoll = 0;
                var sumBox = 0;
                var sumPcs = 0;
                var sumTot = 0;
                var sumTotdoc = 0;

                for (var i = 0; i < input.length; i++) {
                    if (input[i].name === 'bag') {
                        if (input[i].value.toString().trim() === '') {
                            sumBag += 0;
                        } else {
                            sumBag += parseFloat(input[i].value.replace(/,/g, ''));
                        }
                    } else if (input[i].name === 'roll') {
                        if (input[i].value.toString().trim() === '') {
                            sumRoll += 0;
                        } else {
                            sumRoll += parseFloat(input[i].value.replace(/,/g, ''));
                        }
                    } else if (input[i].name === 'box') {
                        if (input[i].value.toString().trim() === '') {
                            sumBox += 0;
                        } else {
                            sumBox += parseFloat(input[i].value.replace(/,/g, ''));
                        }
                    } else if (input[i].name === 'pcs') {
                        if (input[i].value.toString().trim() === '') {
                            sumPcs += 0;
                        } else {
                            sumPcs += parseFloat(input[i].value.replace(/,/g, ''));
                        }
                    } else if (input[i].name === 'tot') {
                        if (input[i].value.toString().trim() === '') {
                            sumTot += 0;
                        } else {
                            sumTot += parseFloat(input[i].value.replace(/,/g, ''));
                        }
                    } else if (input[i].name === 'totdoc') {
                        if (input[i].value.toString().trim() === '') {
                            sumTotdoc += 0;
                        } else {
                            sumTotdoc += parseFloat(input[i].value.replace(/,/g, ''));
                        }
                    }
                }

                document.getElementById('tot-bag').innerHTML = currencyFormat(bag + sumBag, 0);
                document.getElementById('tot-roll').innerHTML = currencyFormat(roll + sumRoll, 0);
                document.getElementById('tot-box').innerHTML = currencyFormat(box + sumBox, 0);
                document.getElementById('tot-pcs').innerHTML = currencyFormat(pcs + sumPcs, 0);
                document.getElementById('tot-tot').innerHTML = currencyFormat(tot + sumTot, 0);
                document.getElementById('tot-totdoc').innerHTML = currencyFormat(totdoc + sumTotdoc, 0);

                document.getElementById('in-bag').value = (bag + sumBag);
                document.getElementById('in-roll').value = (roll + sumRoll);
                document.getElementById('in-box').value = (box + sumBox);
                document.getElementById('in-pcs').value = (pcs + sumPcs);
                document.getElementById('in-totdoc').value = (totdoc + sumTotdoc);

            }

            function CheckType(type) {
                var wh = document.getElementById('wh').value.toString().trim();
                var shipDate = document.getElementById('shipDate').value.toString().trim();
                var dest = document.getElementById('dest').value.toString().trim();
                var round = document.getElementById('round').value.toString().trim();
                var lino = document.getElementById('lino').value.toString().trim();
                var driver = document.getElementById('driver').value.toString().trim();
                var follower = document.getElementById('follower').value.toString().trim();

                if (type === 'บิลผ่าน') {
                    window.location.href = '/TABLE2/XMS400/create?wh=' + wh + '&shipDate=' + shipDate
                            + '&dest=' + dest
                            + '&round=' + round
                            + '&lino=' + lino
                            + '&driver=' + driver
                            + '&follower=' + follower;
                } else if (type === 'วัตถุดิบ') {
                    window.location.href = '/TABLE2/XMS400/createMAT?wh=' + wh + '&shipDate=' + shipDate
                            + '&dest=' + dest
                            + '&round=' + round
                            + '&lino=' + lino
                            + '&driver=' + driver
                            + '&follower=' + follower;
                }
            }

            function NextField(evt, id) {
                if (evt.keyCode === 13) {
                    evt.preventDefault();

                    document.getElementById(id).focus();
                }
            }

            function NextFieldAdd(evt) {
                if (evt.keyCode === 13) {
                    evt.preventDefault();

                    AddDet();
                }
            }

            function NextFieldChange(id) {
                document.getElementById(id).focus();
            }
        </script>
    </head>    
    <body onload="document.getElementById('userid').value = sessionStorage.getItem('uid');
            SumOnAdd();">
        <div id="wrapper">

            <!-- /#sidebar-wrapper -->
            <%@ include file="../fragments/sidebar.jsp" %>

            <!-- Page Content -->
            <!-- nav-head-custom -->
            <%@ include file="../fragments/nav_head.jsp" %>

            <div class="container-fluid">
                <!--<div id="wrapper-top">-->
                <%-- PART 2 --%>
                <!--                    <div class="row">
                                        <div class="col-lg-12">
                                            <div id="set-height" style="height:415px;margin-top:0px;">
                                                <div id="sidebar-wrapper-top" class="">
                                                    <b class="page-header" style="padding-left:5px;font-size:18px;">
                <%-- <hr style="margin:0px;margin-top:10px;margin-bottom:10px;"> --%>
            </b>
        </div>
    </div>
</div>
</div> -->
                <%-- PART 3 --%>
                <!--show Qdest table-->
                <br>
                <div style="border: 2px solid #ccc; border-radius: 5px; padding: 20px;">
                    <form id="formData" name="formData" action="createOT" method="post">
                        <input type="hidden" id="preDel" name="preDel">
                        <table width="100%">
                            <tr>
                                <td width="10%">
                                    <b style="color: #00399b;">Warehouse : </b>
                                </td>
                                <td width="15%">
                                    <select name="wh" id="wh">
                                        <option value="${wh}" selected hidden>${wh} : ${whn}</option>
                                        <c:forEach items="${MCList}" var="p" varStatus="i">
                                            <option value="${p.uid}">${p.uid} : ${p.name}</option>
                                        </c:forEach>
                                    </select>
                                </td>
                                <td width="10%"></td>
                                <td width="15%"></td>
                                <td width="10%"></td>
                                <td width="15%"></td>
                                <td width="2%"></td>
                                <td width="8%">
                                    <b style="color: #00399b;">Shipment Date : </b>
                                </td>
                                <td width="15%">
                                    <input type="date" name="shipDate" id="shipDate" style=" height: 30px;" value="${shipDate}" >
                                </td>
                            </tr>
                            <tr>
                                <td width="10%">
                                    <b style="color: #00399b;">Destination : </b>
                                </td>
                                <td width="15%">
                                    <select name="dest" id="dest" >
                                        <option value="${dest}" hidden>${dest} : ${destn}</option>
                                        <c:forEach items="${destList}" var="p" varStatus="i">
                                            <option value="${p.code}">${p.code} : ${p.desc}</option>
                                        </c:forEach>
                                    </select>
                                </td>
                                <td width="10%"></td>
                                <td width="15%"></td>
                                <td width="10%"></td>
                                <td width="15%" style=" text-align: right;"></td>
                                <td width="2%"></td>
                                <td width="8%">
                                    <b style="color: #00399b;">Round : </b>
                                </td>
                                <td width="15%">
                                    <input type="number" name="round" id="round" value="${round}" style=" height: 30px; width:55%;">
                                </td>
                            </tr>
                            <tr>
                                <td width="10%">
                                    <b style="color: #00399b;">License No. : </b>
                                </td>
                                <td width="15%">
                                    <input type="text" name="lino" id="lino" value="${lino}">
                                </td>
                                <td width="10%" style=" text-align: center;">
                                    <b style="color: #00399b;">Driver Name : </b>
                                </td>
                                <td width="15%">
                                    <input type="text" name="driver" id="driver" value="${driver}" >
                                </td>
                                <td width="10%" style=" text-align: center;">
                                    <b style="color: #00399b;">Follower Name : </b>
                                </td>
                                <td width="15%">
                                    <input type="text" name="follower" id="follower" value="${follower}" >
                                </td>
                                <td width="2%"></td>
                                <td width="8%">
                                    <b style="color: #00399b;">Type : </b>
                                </td>
                                <td width="15%">
                                    <select name="type" id="type" style="width:55%;" onchange="CheckType(this.value);">
                                        <option value="บิลผ่าน">บิลผ่าน</option>
                                        <option value="วัตถุดิบ">วัตถุดิบ</option>
                                        <option value="อื่นๆ" selected>อื่นๆ</option>
                                    </select>
                                </td>
                            </tr>
                        </table>
                        <input type="hidden" id="userid" name="userid">
                        <input type="hidden" id="in-bag" name="in-bag" value="${bagH}">
                        <input type="hidden" id="in-roll" name="in-roll" value="${rollH}">
                        <input type="hidden" id="in-box" name="in-box" value="${boxH}">
                        <input type="hidden" id="in-pcs" name="in-pcs" value="${pcsH}">
                        <input type="hidden" id="in-totdoc" name="in-totdoc" value="${totdocH}">
                        <table id="showTable" class="display" style="width:100%; border: 1px solid #ccc; border-radius: 4px;">
                            <thead> 
                                <tr>
                                    <th colspan="4" style="text-align: center; border-bottom: none;"></th>
                                    <th colspan="5" style="text-align: center; border-bottom-color: #ccc;">Package</th>
                                </tr>
                                <tr>
                                    <th>Sender</th>
                                    <th>Receiver</th>
                                    <th>Department</th>
                                    <th>Description</th>
                                    <th style="text-align: right;">Bag</th>
                                    <th style="text-align: right;">Roll</th>
                                    <th style="text-align: right;">Box</th>
                                    <th style="text-align: right;">Pcs</th>
                                    <th style="text-align: right;">Total</th>
                                    <th style="text-align: right;">Total Docs</th>
                                    <th>Remark</th>
                                    <th>Option</th>
                                </tr>
                                <tr>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th style="text-align: right;">TOTAL</th>
                                    <th style="text-align: right;">
                                        <span id="tot-bag"></span>&nbsp;
                                    </th>
                                    <th style="text-align: right;">
                                        <span id="tot-roll"></span>&nbsp;
                                    </th>
                                    <th style="text-align: right;">
                                        <span id="tot-box"></span>&nbsp;
                                    </th>
                                    <th style="text-align: right;">
                                        <span id="tot-pcs"></span>&nbsp;
                                    </th>
                                    <th style="text-align: right;">
                                        <span id="tot-tot"></span>&nbsp;
                                    </th>
                                    <th style="text-align: right;">
                                        <span id="tot-totdoc"></span>&nbsp;
                                    </th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody id="det-body">

                            </tbody>
                            <tfoot>
                                <tr style=" background-color: #ebf3ff;">
                                    <td style="padding: 8px 10px;">
                                        <input type="text" id="add-sender" onkeyup="this.value = this.value.toUpperCase(); NextField(event, 'add-rec');">
                                    </td>
                                    <td style="padding: 8px 10px;">
                                        <input type="text" name="add-rec" id="add-rec" onkeyup="NextField(event, 'add-dept');">
                                    </td>
                                    <td style="padding: 8px 10px;">
                                        <input type="text" name="add-dept" id="add-dept" onkeyup="NextField(event, 'add-desc');" onchange="SelectDept(this.value);">
                                    </td>
                                    <td style="padding: 8px 10px;">
                                        <input type="text" id="add-desc" list="descList" onkeyup="NextField(event, 'add-bag');">
                                        <dataList name="descList" id="descList" ></dataList>
                                    </td>
                                    <td style="padding: 8px 10px;">
                                        <input type="text" id="add-bag" onkeyup="NextField(event, 'add-roll');" style="text-align: right;" onchange="FormatAdd(this.id, 0);
                                                SumOnAdd();">
                                    </td>
                                    <td style="padding: 8px 10px;">
                                        <input type="text" id="add-roll" onkeyup="NextField(event, 'add-box');" style="text-align: right;" onchange="FormatAdd(this.id, 0);
                                                SumOnAdd();">
                                    </td>
                                    <td style="padding: 8px 10px;">
                                        <input type="text" id="add-box" onkeyup="NextField(event, 'add-pcs');" style="text-align: right;" onchange="FormatAdd(this.id, 0);
                                                SumOnAdd();">
                                    </td>
                                    <td style="padding: 8px 10px;">
                                        <input type="text" id="add-pcs" onkeyup="NextField(event, 'add-totdoc');" style="text-align: right;" onchange="FormatAdd(this.id, 0);
                                                SumOnAdd();">
                                    </td>
                                    <td style="padding: 8px 10px;">
                                        <input type="text" id="add-tot" style="text-align: right; background-color: #F0F0F0;" onchange="FormatAdd(this.id, 0);
                                                SumOnAdd();" readonly>
                                    </td>
                                    <td style="padding: 8px 10px;">
                                        <input type="text" id="add-totdoc" onkeyup="NextField(event, 'add-remark');" style="text-align: right;" onchange="FormatAdd(this.id, 0);
                                                SumOnAdd();">
                                    </td>
                                    <td style="padding: 8px 10px;">
                                        <input type="text" id="add-remark" onkeyup="NextFieldAdd(event);">
                                    </td>
                                    <td>
                                        <a onclick="AddDet();" style="cursor: pointer;"><i class="glyphicon glyphicon-plus-sign" style="font-size:25px; padding-left: 10px"></i></a>
                                        <div id="myModal-3" class="modal">
                                            <!-- Modal content -->
                                            <div class="modal-content" style=" height: 200px; width: 500px;">
                                                <span id="span-3" class="close" onclick="document.getElementById('myModal-3').style.display = 'none';
                                                        document.getElementById('add-sender').focus();">&times;</span>
                                                <p><b><font size="4"></font></b></p>
                                                <table width="100%">
                                                    <tr style=" background-color: white;">
                                                        <td align="center">
                                                            <b style="color: #00399b;">
                                                                <font size="5">Please fill Product Sender !</font>
                                                            </b>
                                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                            <br><br><br>
                                                            <a style="width: 100px;" class="btn btn btn-outline btn-info" onclick="document.getElementById('myModal-3').style.display = 'none';
                                                                    document.getElementById('add-sender').focus();">
                                                                OK 
                                                            </a>                     
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </tfoot>
                        </table>
                        <center>
                            <span class="fa-stack fa-lg fa-2x" onclick="window.location.href = '/TABLE2/XMS400/display?wh=${wh}&shipDate=${shipDate}';" style=" cursor: pointer;">
                                <i id="ic2" class="fa fa-circle fa-stack-2x" style="color: #d43737;"></i>
                                <i id="ic2" class="fa fa-arrow-left fa-stack-1x fa-inverse"></i>
                            </span>
                            <span class="fa-stack fa-lg fa-2x" onclick="document.getElementById('formData').submit();" style=" cursor: pointer;">
                                <i id="ic" class="fa fa-circle fa-stack-2x" style="color: #154baf;"></i>
                                <i id="ic" class="fa fa-save fa-stack-1x fa-inverse"></i>
                            </span>
                        </center>
                    </form>
                    <div id="myModal-del" class="modal">
                        <!-- Modal content -->
                        <div class="modal-content" style=" height: 200px; width: 450px;">
                            <span id="span-del" class="close" onclick="document.getElementById('myModal-del').style.display = 'none';">&times;</span>
                            <p><b><font size="4"></font></b></p>
                            <table width="100%">
                                <tr style=" background-color: white;">
                                    <td align="center">
                                        <b style="color: #00399b;">
                                            <font size="5">Delete this row ?</font>
                                        </b>
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <br><br><br>
                                        <a style="width: 100px;" class="btn btn btn-outline btn-danger" onclick="document.getElementById('myModal-del').style.display = 'none';">
                                            Cancel 
                                        </a>
                                        <a style="width: 100px;" class="btn btn btn-outline btn-success" onclick="DeleteDet2();">
                                            Confirm 
                                        </a> 
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <!--End Part 3-->
                <br>
                <!--</div>  end #wrapper-top--> 
            </div> <!-- end .container-fluid -->
        </div> <!-- end #wrapper -->
    </body>
</html>