<%@ include file="../fragments/taglibs.jsp" %>
<%@ include file="../fragments/imports.jsp" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.sql.*;" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>WMS</title>
        <!-- css :: vendors -->
        <jsp:include page = "../fragments/css.jsp" />
        <!-- additional custom :: my-style -->
        <link rel="stylesheet" href="../resources/assets/styles/myStyles.css">

        <!-- js :: vendors -->
        <jsp:include page = "../fragments/script.jsp" />
        <!-- additional custom :: my-script -->
        <script src="../resources/assets/scripts/myScripts.js" async></script>
        <script src="../resources/assets/scripts/toggleLoad.js" async></script> 
        <style>
            input[type=text], select{
                width: 90%;
                padding: 5px 5px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }

            input[type=date], input[type=number]{
                /*width: 100%;*/
                padding: 5px 5px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }

            input[type=submit] {
                width: 100%;
                background-color: #00399b;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=submit]:hover {
                background-color: #3973d6;
            }

            input[type=button] {
                width: 100%;
                background-color: #ef604a;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=button]:hover {
                background-color: #e51e00;
            }
        </style>
        <script>
            $(document).ready(function () {
                $('#showTable').DataTable({
//                    fixedHeader: true,
//                    "paging": false,
//                    "ordering": false,
                    "pagingType": "full_numbers",
                    "lengthMenu": [[10, 25, 50, 100], [10, 25, 50, 100]],
                    "bSortClasses": false,
                    "order": [[0, "desc"]],
                    columnDefs: [
                        {targets: 5, orderable: false}
                    ],
                    "ajax": "/TABLE2/ShowDataTablesXMS017?mode=display",
                    "sAjaxDataProp": "",
                    "aoColumns": [
                        {"mDataProp": "QSHTRDT"},
                        {"mDataProp": "QSHGRID"},
                        {"mDataProp": "QSHMTCTRL"},
                        {"mDataProp": "QSHUSER"},
                        {"mDataProp": "QSHQUEUE"},
                        {"mDataProp": "option"}
                    ],
                    "processing": true
                });

                // Setup - add a text input to each footer cell
                $('#showTable tfoot th').not(":eq(5)").each(function () {
                    var title = $(this).text();
                    $(this).html('<input type="text" />');
                });

                // DataTable
                var table = $('#showTable').DataTable();

                // Apply the search
                table.columns().every(function () {
                    var that = this;
                    $('input', this.footer()).on('keyup change', function () {
                        if (that.search() !== this.value) {
                            that.search(this.value).draw();
                        }
                    });
                });
            }
            );
        </script>
        <style>
            body {font-family: Arial, Helvetica, sans-serif;}

            /* The Modal (background) */
            .modal {
                display: none; /* Hidden by default */
                position: fixed; /* Stay in place */
                z-index: 1; /* Sit on top */
                padding-top: 100px; /* Location of the box */
                left: 0;
                top: 0;
                width: 100%; /* Full width */
                height: 100%; /* Full height */
                overflow: auto; /* Enable scroll if needed */
                background-color: rgb(0,0,0); /* Fallback color */
                background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
            }

            /* Modal Content */
            .modal-content {
                background-color: #fefefe;
                margin: auto;
                padding: 20px;
                border: 1px solid #888;
                width: fit-content;
                height: fit-content;
            }

            /* The Close Button */
            .close {
                color: #aaaaaa;
                float: right;
                font-size: 28px;
                font-weight: bold;
            }

            .close:hover,
            .close:focus {
                color: #000;
                text-decoration: none;
                cursor: pointer;
            }
        </style>
        <script>
            function Search() {
                if ($('#tranDateFrom').val() === '' || $('#tranDateTo').val() === '') {
                    alertify.error("กรุณากรอกช่วงวันที่ !");
                } else {

                    $.ajax({
                        url: "/TABLE2/ShowDataTablesXMS017?mode=display"
                                + "&wh=" + $('#wh').val()
                                + "&tranDateFrom=" + $('#tranDateFrom').val()
                                + "&tranDateTo=" + $('#tranDateTo').val()
                    }).done(function (result) {
                        ReTable(result);
                    }).fail(function (jqXHR, textStatus, errorThrown) {
                        // needs to implement if it fails
                    });

                }
            }

            function ReTable(result) {
                var table = $('#showTable').DataTable();
                table.clear().draw();
                table.rows.add(result).draw();

                alertify.success("Search Successfully " + table.data().count() + " rows");
            }

            function toWMSdisplay(gid) {
                var uid = sessionStorage.getItem('uid');
                window.location.href = "/WMS/WMS017/WEB?uid=" + uid + "&action=display&gid=" + gid;
            }

            function Create() {
                var uid = sessionStorage.getItem('uid');
                window.location.href = "display?uid=" + uid + "&action=create&wh=" + $('#wh').val();
            }

            var autoSearch = setInterval(function () {
                Search();
                clearInterval(autoSearch);
            }, 500);
        </script>
    </head>    
    <body onload="document.getElementById('userid').value = sessionStorage.getItem('uid');">
        <div id="wrapper">

            <!-- /#sidebar-wrapper -->
            <%@ include file="../fragments/sidebar.jsp" %>

            <!-- Page Content -->
            <!-- nav-head-custom -->
            <%@ include file="../fragments/nav_head.jsp" %>

            <div class="container-fluid">
                <!--<div id="wrapper-top">-->
                <%-- PART 2 --%>
                <!--                    <div class="row">
                                        <div class="col-lg-12">
                                            <div id="set-height" style="height:415px;margin-top:0px;">
                                                <div id="sidebar-wrapper-top" class="">
                                                    <b class="page-header" style="padding-left:5px;font-size:18px;">
                <%-- <hr style="margin:0px;margin-top:10px;margin-bottom:10px;"> --%>
            </b>
        </div>
    </div>
</div>
</div> -->
                <%-- PART 3 --%>
                <!--show Qdest table-->
                <form action="../resources/manual/XMS017.pdf" target="_blank">
                    <table width="100%">
                        <tr>
                            <td width="94%"></td>
                            <td width="6%" align="right"><input type="submit" value="Manual" /></td>
                        </tr>
                    </table>
                </form>
                <div style="border: 2px solid #ccc; border-radius: 5px; padding: 20px;">
                    <table width="100%">
                        <tr>
                            <td width="20%"><b style="color: #00399b;">Warehouse : </b>
                                <select name="wh" id="wh" style="width:55%; height: 35px;">
                                    <c:forEach items="${MCList}" var="p" varStatus="i">
                                        <option value="${p.uid}">${p.uid} : ${p.name}</option>
                                    </c:forEach>
                                </select>
                            </td>
                            <td width="20%"><b style="color: #00399b;">Select by Trans date : </b>
                                <input type="date" name="tranDateFrom" id="tranDateFrom" value="${date}" style=" height: 35px;">
                                -
                                <input type="date" name="tranDateTo" id="tranDateTo" value="${date}" style=" height: 35px;">
                            </td>
                            <td width="5%">
                                <a style=" width: 100%;" class="btn btn btn-outline btn-info" id="searchBtn" name="searchBtn" onclick="Search();">
                                    <i class="fa fa-search" style="font-size:20px;"></i> Search</a>
                            </td>
                        </tr>
                    </table>
                    <hr>
                    <table id="showTable" class="display" style="width:100%; border: 1px solid #ccc; border-radius: 4px;">
                        <thead>
                            <tr>
                                <th style="text-align: center;">Trans Date</th>
                                <th style="text-align: center;">Group QR ID</th>
                                <th style="text-align: center;">Material Control</th>
                                <th style="text-align: center;">User</th>
                                <th style="text-align: center;">Queue No.</th>
                                <th style="text-align: center;">Option</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th style="text-align: center;">
                                    <a style=" width: 100%; cursor: pointer;" onclick="Create();">
                                        <i class="fa fa-plus-circle" style="font-size:40px;"></i>
                                    </a>
                                </th>
                            </tr>
                        </tfoot>
                        <tbody>                         
                        </tbody>
                    </table>
                    <!--**********************-->
                    <!--End Part 3-->
                    <!--</div>  end #wrapper-top--> 
                </div> 
                <br>
            </div> <!-- end .container-fluid -->
        </div> <!-- end #wrapper -->
        <script>
            function print(gid, trdt, matCtrl, user, pids) {
                /* v1  */
                console.log("print..", gid);

                $.ajax({
                    url: '/wms-service/rest/wms017/report/generate?gid=' + gid + '&trdt=' + trdt + '&whse=' + $('#wh').val() + '&uid=' + user + '&matCtrl=' + matCtrl
                }).done(function (result) {
                    window.open('/wms-service/resources/report/wms017/output/pdf/WMS017-GroupQRID_' + gid + '.pdf');
                }).fail(function (jqXHR, textStatus, errorThrown) {
                    // needs to implement if it fails
                });

                console.log('parentIds = ' + pids);
                // OK
                window.open('/WMS/report/wms017RePrintParentIdReport.jsp?pids=' + pids);
            }
        </script>
    </body>
</html>