<!DOCTYPE html>
<html>
    <head>
        <title>Page Title</title>
    </head>
    <body onload="randomVal();">

        <script>
            function randomVal() {
                var input = document.getElementsByTagName('input');
                for (var i = 0; i < input.length; i++) {
                    var val = Math.floor(Math.random() * 1);
                    if (val === 0) {
                        input[i].value = 'X';
                    } else {
                        input[i].value = val;
                    }
                }
            }

            function switchVal(id, val, sts) {
                var x = parseInt(id.toString().substring(0, 1).charCodeAt(0));
                var y = parseInt(id.toString().substring(1));

                if (sts === 1) {
                    if (val === 'X') {
                        var input = document.getElementsByTagName('input');
                        for (var i = 0; i < input.length; i++) {
                            if (input[i].value === val) {
                                input[i].style.color = 'red';
                            }
                        }

                        alert('Game Over !');
                    }
                }

                if (x === 65) {
                    if (y === 1) {
                        document.getElementById(String.fromCharCode(x) + y.toString()).style.color = 'black';
                        if (document.getElementById(String.fromCharCode(x + 1) + y.toString()).value === val) {
                            document.getElementById(String.fromCharCode(x + 1) + y.toString()).style.color = 'black';
                            switchVal(String.fromCharCode(x + 1) + y.toString(), document.getElementById(String.fromCharCode(x + 1) + y.toString()).value, 0);
                        }
                        if (document.getElementById(String.fromCharCode(x) + (y + 1).toString()).value === val) {
                            document.getElementById(String.fromCharCode(x) + (y + 1).toString()).style.color = 'black';
                            switchVal(String.fromCharCode(x) + (y + 1).toString(), document.getElementById(String.fromCharCode(x) + (y + 1).toString()).value, 0);
                        }
                    } else if (y === 15) {
                        document.getElementById(String.fromCharCode(x) + y.toString()).style.color = 'black';
                        if (document.getElementById(String.fromCharCode(x + 1) + y.toString()).value === val) {
                            document.getElementById(String.fromCharCode(x + 1) + y.toString()).style.color = 'black';
                            switchVal(String.fromCharCode(x + 1) + y.toString(), document.getElementById(String.fromCharCode(x + 1) + y.toString()).value, 0);
                        }
                        if (document.getElementById(String.fromCharCode(x) + (y - 1).toString()).value === val) {
                            document.getElementById(String.fromCharCode(x) + (y - 1).toString()).style.color = 'black';
                            switchVal(String.fromCharCode(x) + (y - 1).toString(), document.getElementById(String.fromCharCode(x) + (y - 1).toString()).value, 0);
                        }
                    } else {
                        document.getElementById(String.fromCharCode(x) + y.toString()).style.color = 'black';
                        if (document.getElementById(String.fromCharCode(x + 1) + y.toString()).value === val) {
                            document.getElementById(String.fromCharCode(x + 1) + y.toString()).style.color = 'black';
                            switchVal(String.fromCharCode(x + 1) + y.toString(), document.getElementById(String.fromCharCode(x + 1) + y.toString()).value, 0);
                        }
                        if (document.getElementById(String.fromCharCode(x) + (y + 1).toString()).value === val) {
                            document.getElementById(String.fromCharCode(x) + (y + 1).toString()).style.color = 'black';
                            switchVal(String.fromCharCode(x) + (y + 1).toString(), document.getElementById(String.fromCharCode(x) + (y + 1).toString()).value, 0);
                        }
                        if (document.getElementById(String.fromCharCode(x) + (y - 1).toString()).value === val) {
                            document.getElementById(String.fromCharCode(x) + (y - 1).toString()).style.color = 'black';
                            switchVal(String.fromCharCode(x) + (y - 1).toString(), document.getElementById(String.fromCharCode(x) + (y - 1).toString()).value, 0);
                        }
                    }
                } else if (x === 81) {
                    if (y === 1) {
                        document.getElementById(String.fromCharCode(x) + y.toString()).style.color = 'black';
                        if (document.getElementById(String.fromCharCode(x - 1) + y.toString()).value === val) {
                            document.getElementById(String.fromCharCode(x - 1) + y.toString()).style.color = 'black';
                            switchVal(String.fromCharCode(x - 1) + y.toString(), document.getElementById(String.fromCharCode(x - 1) + y.toString()).value, 0);
                        }
                        if (document.getElementById(String.fromCharCode(x) + (y + 1).toString()).value === val) {
                            document.getElementById(String.fromCharCode(x) + (y + 1).toString()).style.color = 'black';
                            switchVal(String.fromCharCode(x) + (y + 1).toString(), document.getElementById(String.fromCharCode(x) + (y + 1).toString()).value, 0);
                        }
                    } else if (y === 15) {
                        document.getElementById(String.fromCharCode(x) + y.toString()).style.color = 'black';
                        if (document.getElementById(String.fromCharCode(x - 1) + y.toString()).value === val) {
                            document.getElementById(String.fromCharCode(x - 1) + y.toString()).style.color = 'black';
                            switchVal(String.fromCharCode(x - 1) + y.toString(), document.getElementById(String.fromCharCode(x - 1) + y.toString()).value, 0);
                        }
                        if (document.getElementById(String.fromCharCode(x) + (y - 1).toString()).value === val) {
                            document.getElementById(String.fromCharCode(x) + (y - 1).toString()).style.color = 'black';
                            switchVal(String.fromCharCode(x) + (y - 1).toString(), document.getElementById(String.fromCharCode(x) + (y - 1).toString()).value, 0);
                        }
                    } else {
                        document.getElementById(String.fromCharCode(x) + y.toString()).style.color = 'black';
                        if (document.getElementById(String.fromCharCode(x - 1) + y.toString()).value === val) {
                            document.getElementById(String.fromCharCode(x - 1) + y.toString()).style.color = 'black';
                            switchVal(String.fromCharCode(x - 1) + y.toString(), document.getElementById(String.fromCharCode(x - 1) + y.toString()).value, 0);
                        }
                        if (document.getElementById(String.fromCharCode(x) + (y + 1).toString()).value === val) {
                            document.getElementById(String.fromCharCode(x) + (y + 1).toString()).style.color = 'black';
                            switchVal(String.fromCharCode(x) + (y + 1).toString(), document.getElementById(String.fromCharCode(x) + (y + 1).toString()).value, 0);
                        }
                        if (document.getElementById(String.fromCharCode(x) + (y - 1).toString()).value === val) {
                            document.getElementById(String.fromCharCode(x) + (y - 1).toString()).style.color = 'black';
                            switchVal(String.fromCharCode(x) + (y - 1).toString(), document.getElementById(String.fromCharCode(x) + (y - 1).toString()).value, 0);
                        }
                    }
                } else {
                    if (y === 1) {
                        document.getElementById(String.fromCharCode(x) + y.toString()).style.color = 'black';
                        if (document.getElementById(String.fromCharCode(x + 1) + y.toString()).value === val) {
                            document.getElementById(String.fromCharCode(x + 1) + y.toString()).style.color = 'black';
                            switchVal(String.fromCharCode(x + 1) + y.toString(), document.getElementById(String.fromCharCode(x + 1) + y.toString()).value, 0);
                        }
                        if (document.getElementById(String.fromCharCode(x - 1) + y.toString()).value === val) {
                            document.getElementById(String.fromCharCode(x - 1) + y.toString()).style.color = 'black';
                            switchVal(String.fromCharCode(x - 1) + y.toString(), document.getElementById(String.fromCharCode(x - 1) + y.toString()).value, 0);
                        }
                        if (document.getElementById(String.fromCharCode(x) + (y + 1).toString()).value === val) {
                            document.getElementById(String.fromCharCode(x) + (y + 1).toString()).style.color = 'black';
                            switchVal(String.fromCharCode(x) + (y + 1).toString(), document.getElementById(String.fromCharCode(x) + (y + 1).toString()).value, 0);
                        }
                    } else if (y === 15) {
                        document.getElementById(String.fromCharCode(x) + y.toString()).style.color = 'black';
                        if (document.getElementById(String.fromCharCode(x + 1) + y.toString()).value === val) {
                            document.getElementById(String.fromCharCode(x + 1) + y.toString()).style.color = 'black';
                            switchVal(String.fromCharCode(x + 1) + y.toString(), document.getElementById(String.fromCharCode(x + 1) + y.toString()).value, 0);
                        }
                        if (document.getElementById(String.fromCharCode(x - 1) + y.toString()).value === val) {
                            document.getElementById(String.fromCharCode(x - 1) + y.toString()).style.color = 'black';
                            switchVal(String.fromCharCode(x - 1) + y.toString(), document.getElementById(String.fromCharCode(x - 1) + y.toString()).value, 0);
                        }
                        if (document.getElementById(String.fromCharCode(x) + (y - 1).toString()).value === val) {
                            document.getElementById(String.fromCharCode(x) + (y - 1).toString()).style.color = 'black';
                            switchVal(String.fromCharCode(x) + (y - 1).toString(), document.getElementById(String.fromCharCode(x) + (y - 1).toString()).value, 0);
                        }
                    } else {
                        document.getElementById(String.fromCharCode(x) + y.toString()).style.color = 'black';
                        if (document.getElementById(String.fromCharCode(x + 1) + y.toString()).value === val) {
                            document.getElementById(String.fromCharCode(x + 1) + y.toString()).style.color = 'black';
                            switchVal(String.fromCharCode(x + 1) + y.toString(), document.getElementById(String.fromCharCode(x + 1) + y.toString()).value, 0);
                        }
                        if (document.getElementById(String.fromCharCode(x - 1) + y.toString()).value === val) {
                            document.getElementById(String.fromCharCode(x - 1) + y.toString()).style.color = 'black';
                            switchVal(String.fromCharCode(x - 1) + y.toString(), document.getElementById(String.fromCharCode(x - 1) + y.toString()).value, 0);
                        }
                        if (document.getElementById(String.fromCharCode(x) + (y + 1).toString()).value === val) {
                            document.getElementById(String.fromCharCode(x) + (y + 1).toString()).style.color = 'black';
                            switchVal(String.fromCharCode(x) + (y + 1).toString(), document.getElementById(String.fromCharCode(x) + (y + 1).toString()).value, 0);
                        }
                        if (document.getElementById(String.fromCharCode(x) + (y - 1).toString()).value === val) {
                            document.getElementById(String.fromCharCode(x) + (y - 1).toString()).style.color = 'black';
                            switchVal(String.fromCharCode(x) + (y - 1).toString(), document.getElementById(String.fromCharCode(x) + (y - 1).toString()).value, 0);
                        }
                    }
                }

                if (sts === 1) {
                    if (val === 'X') {
                        document.getElementById(id).style.color = 'red';
                    }
                }

            }
        </script>

        <br>

        <table>
            <tr>
                <td>
                    <input type="button" id="A1" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="B1" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="C1" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="D1" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="E1" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="F1" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="G1" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="H1" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="I1" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="J1" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="K1" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="L1" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="M1" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="N1" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="O1" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="P1" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="Q1" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
            </tr>
            <tr>
                <td>
                    <input type="button" id="A2" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="B2" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="C2" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="D2" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="E2" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="F2" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="G2" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="H2" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="I2" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="J2" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="K2" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="L2" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="M2" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="N2" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="O2" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="P2" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="Q2" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
            </tr>
            <tr>
                <td>
                    <input type="button" id="A3" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="B3" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="C3" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="D3" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="E3" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="F3" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="G3" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="H3" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="I3" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="J3" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="K3" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="L3" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="M3" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="N3" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="O3" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="P3" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="Q3" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
            </tr>
            <tr>
                <td>
                    <input type="button" id="A4" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="B4" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="C4" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="D4" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="E4" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="F4" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="G4" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="H4" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="I4" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="J4" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="K4" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="L4" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="M4" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="N4" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="O4" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="P4" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="Q4" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
            </tr>
            <tr>
                <td>
                    <input type="button" id="A5" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="B5" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="C5" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="D5" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="E5" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="F5" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="G5" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="H5" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="I5" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="J5" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="K5" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="L5" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="M5" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="N5" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="O5" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="P5" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="Q5" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
            </tr>
            <tr>
                <td>
                    <input type="button" id="A6" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="B6" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="C6" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="D6" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="E6" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="F6" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="G6" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="H6" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="I6" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="J6" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="K6" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="L6" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="M6" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="N6" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="O6" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="P6" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="Q6" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
            </tr>
            <tr>
                <td>
                    <input type="button" id="A7" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="B7" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="C7" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="D7" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="E7" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="F7" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="G7" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="H7" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="I7" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="J7" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="K7" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="L7" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="M7" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="N7" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="O7" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="P7" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="Q7" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
            </tr>
            <tr>
                <td>
                    <input type="button" id="A8" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="B8" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="C8" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="D8" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="E8" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="F8" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="G8" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="H8" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="I8" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="J8" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="K8" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="L8" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="M8" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="N8" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="O8" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="P8" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="Q8" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
            </tr>
            <tr>
                <td>
                    <input type="button" id="A9" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="B9" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="C9" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="D9" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="E9" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="F9" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="G9" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="H9" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="I9" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="J9" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="K9" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="L9" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="M9" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="N9" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="O9" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="P9" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="Q9" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
            </tr>
            <tr>
                <td>
                    <input type="button" id="A10" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="B10" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="C10" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="D10" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="E10" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="F10" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="G10" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="H10" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="I10" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="J10" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="K10" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="L10" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="M10" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="N10" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="O10" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="P10" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="Q10" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
            </tr>
            <tr>
                <td>
                    <input type="button" id="A11" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="B11" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="C11" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="D11" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="E11" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="F11" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="G11" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="H11" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="I11" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="J11" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="K11" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="L11" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="M11" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="N11" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="O11" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="P11" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="Q11" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
            </tr>
            <tr>
                <td>
                    <input type="button" id="A12" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="B12" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="C12" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="D12" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="E12" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="F12" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="G12" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="H12" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="I12" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="J12" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="K12" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="L12" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="M12" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="N12" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="O12" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="P12" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="Q12" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
            </tr>
            <tr>
                <td>
                    <input type="button" id="A13" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="B13" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="C13" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="D13" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="E13" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="F13" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="G13" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="H13" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="I13" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="J13" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="K13" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="L13" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="M13" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="N13" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="O13" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="P13" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="Q13" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
            </tr>
            <tr>
                <td>
                    <input type="button" id="A14" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="B14" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="C14" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="D14" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="E14" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="F14" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="G14" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="H14" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="I14" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="J14" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="K14" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="L14" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="M14" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="N14" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="O14" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="P14" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="Q14" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
            </tr>
            <tr>
                <td>
                    <input type="button" id="A15" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="B15" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="C15" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="D15" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="E15" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="F15" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="G15" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="H15" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="I15" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="J15" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="K15" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="L15" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="M15" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="N15" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="O15" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="P15" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
                <td>
                    <input type="button" id="Q15" onclick="switchVal(this.id, this.value, 1);" style="width:30px; color: transparent;">
                </td>
            </tr>
        </table>

    </body>
</html>
