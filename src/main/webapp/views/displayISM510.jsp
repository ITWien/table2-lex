<%-- 
    Document   : displayISM510
    Created on : Feb 2, 2023, 8:32:07 AM
    Author     : 93176
--%>

<%@ include file="../fragments/taglibs.jsp" %>
<%@ include file="../fragments/imports.jsp" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.sql.*;" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>ISM</title>

        <!-- css :: vendors -->
        <jsp:include page="../fragments/css.jsp" />
        <!-- additional custom :: my-style -->
        <link rel="stylesheet" href="../resources/assets/styles/myStyles.css">
        <link rel="stylesheet" href="../resources/font/THsarabun/thsarabumnew.css">

        <!-- js :: vendors -->
        <jsp:include page="../fragments/script.jsp" />
        <!-- additional custom :: my-script -->
        <script src="../resources/assets/scripts/myScripts.js" async></script>
        <script src="../resources/assets/scripts/toggleLoad.js" async></script>

        <script>
            $(document).ready(function () {

                $('#uid').val(sessionStorage.getItem('uid'));

                $('#ism500Table').DataTable({
                    "columnDefs": [
                        {"width": "1%", "targets": 0, className: "text-left"},
                        {"width": "1%", "targets": 1, className: "text-left"},
                        {"width": "1%", "targets": 2, className: "text-left"},
                        {"width": "1%", "targets": 3, className: "text-right"},
                        {"width": "1%", "targets": 4, className: "text-right"},
                        {"width": "1%", "targets": 5, className: "text-right"}
                    ],
                    "bPaginate": false,
                    "ordering": false,
                    "bInfo": false
                });

                $('#invNoFrom').change(function () {
                    console.log("from");
                });

                $('#invNoTo').change(function () {

                    var invInputFrom = $('#invNoFrom').val();

                    if (invInputFrom === "") {
                        alertify.error("Please fill invoice.");
                    } else {

                        var invInputTo = this.value;

                        //CheckDetail
                        $.ajax({
                            url: "/TABLE2/ShowDataTablesISM500",
                            data: {mode: "CheckDetailData510", invno: invInputFrom, invto: invInputTo},
                            async: false
                        }).done(function (result) {

                            $('#invtext').text(invInputFrom + " - " + invInputTo);

//                            console.log("check : " + result);

                            if (result) {
                                $('#foundData').modal('show');
                            } else {

                                if (invInputFrom.length === 10 && invInputTo.length === 10) {

                                    console.log("in 500 ");

                                    //Download First time
                                    $.ajax({
                                        url: "/TABLE2/ShowDataTablesISM500",
                                        data: {mode: "dwnload510", invno: invInputFrom, invto: invInputTo, uid: $('#uid').val()},
                                        beforeSend: function () {
                                            $("#loadModal").modal("show");
                                        },
                                        complete: function () {
                                            $("#loadModal").modal("hide");
                                        }
                                    }).done(function (result) {

                                        if (result.map.resInsH) {
                                            if (result.map.resInsD) {
                                                if (result.map.resInsP) {
                                                    //Show Detail
                                                    showData(invInputFrom, invInputTo);
                                                } else {
                                                    $('#InsertError').modal('show');
                                                }
                                            } else {
                                                $('#InsertError').modal('show');
                                            }
                                        } else {
                                            $('#InsertError').modal('show');
                                        }

                                    }).fail(function (jqXHR, textStatus, errorThrown) {
                                        // needs to implement if it fails
                                    });
                                } else {
                                    console.log("out 500 ");
                                }

                            }

                        }).fail(function (jqXHR, textStatus, errorThrown) {
                            // needs to implement if it fails
                        });
                    }

                });

                $('#dwnAgainBtn').click(function () {

//                    alertify.success("Again");//DelHDP
                    let invText = $('#invtext').text();
//                    console.log("AgainBtn " + invText);

                    let invno = invText.toString().split(" - ")[0];
                    let invto = invText.toString().split(" - ")[1];

                    //Delete All Head Detail Partners
                    $.ajax({
                        url: "/TABLE2/ShowDataTablesISM500",
                        data: {mode: "DelHDP510", invno: invno, invto: invto},
                        async: false
                    }).done(function (result) {

                        if (result.map.resDelH) {
                            if (result.map.resDelD) {
                                if (result.map.resDelP) {

                                    console.log("del complete");

                                    //Download First time
                                    $.ajax({
                                        url: "/TABLE2/ShowDataTablesISM500",
                                        data: {mode: "dwnload510", invno: invno, invto: invto, uid: $('#uid').val()},
//                                        async: false,
                                        beforeSend: function () {
                                            $("#loadModal").modal("show");
                                        },
                                        complete: function () {
                                            $("#loadModal").modal("hide");
                                        }
                                    }).done(function (result) {

                                        if (result.map.resInsH) {
                                            if (result.map.resInsD) {
                                                if (result.map.resInsP) {
                                                    //Show Detail
                                                    console.log("insert complete");
                                                    showData(invText);
                                                } else {
                                                    $('#InsertError').modal('show');
                                                }
                                            } else {
                                                $('#InsertError').modal('show');
                                            }
                                        } else {
                                            $('#InsertError').modal('show');
                                        }

                                    }).fail(function (jqXHR, textStatus, errorThrown) {
                                        // needs to implement if it fails
                                    });

                                } else {
                                    alertify.error("Delete Failed.");
                                }
                            } else {
                                alertify.error("Delete Failed.");
                            }
                        } else {
                            alertify.error("Delete Failed.");
                        }

                    }).fail(function (jqXHR, textStatus, errorThrown) {
                        // needs to implement if it fails
                    });

                });

                $('#okBtn').click(function () {
                    let invText = $('#invtext').text();
                    console.log("okBtn " + invText);
                    let finv = invText.toString().split(" - ")[0];
                    let linv = invText.toString().split(" - ")[1];
                    showData(finv, linv);
                });

            });

            function showData(invno, invto) {

                $.ajax({
                    url: "/TABLE2/ShowDataTablesISM500",
                    data: {mode: "getdataHead510", invno: invno, invto: invto},
                    async: false
                }).done(function (result) {

                    $('#invNo').val(result.resData[0].VBELN);
                    $('#fkdate').val(result.resData[0].FKDAT);
                    $('#cusname').text(result.resData[0].NAMRG + " ( " + parseInt(result.resData[0].KUNRG) + " )");

                }).fail(function (jqXHR, textStatus, errorThrown) {
                    // needs to implement if it fails
                });

                //Show Detail
                if ($.fn.DataTable.isDataTable('#ism500Table')) { //before RE-Create New Datatable
                    $('#ism500Table').DataTable().destroy();
                }

                $('#ism500Table tbody').empty();
                $('#ism500Table').dataTable({
                    ajax: {
                        url: "/TABLE2/ShowDataTablesISM500",
                        data: {mode: "getdataDetail510", invno: invno, invto: invto},
                        dataSrc: 'resData'
                    },
                    "aoColumns": [
                        {//1
                            'mRender': function (data, type, full) {

                                if (full.ARKTX === "inv no." || full.MATNR === "detail") {
                                    return "";
                                } else {
                                    return full.CUST_MAT;
                                }

                            }
                        },
                        {//2
                            'mRender': function (data, type, full) {

                                if (full.ARKTX === "inv no." || full.MATNR === "detail") {
                                    return "";
                                } else {
                                    return full.MATNR;
                                }
                            }
                        },
                        {//3
                            'mRender': function (data, type, full) {
                                if (full.ARKTX === "inv no.") {
                                    return "<label>inv no.</label> <label style='color:green;'>/</label> <label>deli</label> <label style='color:green;'>/</label> <label>S/O</label>";
                                } else {
                                    if (full.MATNR === "detail") {
                                        return full.ARKTX + " <label style='color:green;'>/</label> " + full.VGBEL + " <label style='color:green;'>/</label> " + full.SALESORDER;
                                    } else {
                                        return full.ARKTX;
                                    }
                                }
                            }
                        },
                        {//4
                            'mRender': function (data, type, full) {

                                if (full.ARKTX === "inv no.") {
                                    return '';
                                } else {
                                    if (full.MATNR === "detail") {
                                        return '';
                                    } else {
                                        if (full.ARKTX.toString().includes("รวม")) {
                                            if (full.ARKTX.toString().includes("รวมทั้งหมด")) {
                                                return numberWithCommas((Math.round(full.LMENG * 100) / 100).toFixed(2));
                                            } else {
                                                return numberWithCommas((Math.round(full.LMENG * 100) / 100).toFixed(2));
//                                                return '';
                                            }
                                        } else {
                                            return numberWithCommas((Math.round(full.LMENG * 100) / 100).toFixed(2));
                                        }
                                    }

                                }
                            }
                        },
                        {//5
                            'mRender': function (data, type, full) {

                                if (full.ARKTX === "inv no.") {
                                    return '';
                                } else {
                                    if (full.MATNR === "detail") {
                                        return "";
                                    } else {

                                        if (full.ARKTX.toString().includes("รวม")) {
                                            if (full.ARKTX.toString().includes("รวมทั้งหมด")) {
                                                return '<label style="color:red;" >' + numberWithCommas((Math.round(full.ISDETM * 100) / 100).toFixed(2)) + '</label>';
                                            } else {
                                                return '<label style="color:red;" >' + numberWithCommas((Math.round(full.ISDETM * 100) / 100).toFixed(2)) + '</label>';
//                                                return '';
                                            }
                                        } else {
                                            return '<label style="color:red;" >' + numberWithCommas((Math.round(full.ISDETM * 100) / 100).toFixed(2)) + '</label>';
                                        }

                                    }
                                }

                            }
                        },
                        {//6
                            'mRender': function (data, type, full) {

                                if (full.ARKTX === "inv no.") {
                                    return 'qty';
                                } else {
                                    if (full.ARKTX.toString().includes("Delivery") || full.ARKTX.toString().includes("ภาษีมูลค่าเพิ่ม") || full.ARKTX.toString().includes("ทั้งสิ้น")) {
                                        return '';
                                    } else {
                                        return numberWithCommas((Math.round(full.FKLMG * 100) / 100).toFixed(2));
                                    }
                                }

                            }
                        }
                    ],
                    "columnDefs": [
                        {"width": "1%", "targets": 0, className: "text-left"},
                        {"width": "1%", "targets": 1, className: "text-left"},
                        {"width": "1%", "targets": 2, className: "text-left"},
                        {"width": "1%", "targets": 3, className: "text-right"},
                        {"width": "1%", "targets": 4, className: "text-right"},
                        {"width": "1%", "targets": 5, className: "text-right"}
                    ],
                    "bPaginate": false,
                    "ordering": false,
                    "bInfo": false,
                    "createdRow": function (row, data, index) {
                        $(row).css("color", "black"); //font color black

                        if (data.ARKTX === "inv no." || data.MATNR === "detail") {
                            $(row).eq(0).attr('hidden', "true");
                            $(row).eq(0).attr('id', "rowHide" + data.CUST_MAT);
                            $(row).eq(0).attr('name', "rowHide" + data.CUST_MAT);
                        } else {
                            $(row).eq(0).attr('id', "rowShow" + data.MATNR);
                            $(row).eq(0).attr('name', "rowShow" + data.MATNR);
                            $(row).eq(0).attr('onclick', "showHide('" + data.MATNR + "')");
                        }
                    }
                });
            }

            function showHide(matcode) {

                if ($('#rowHide' + matcode).attr("hidden")) {
                    $('tr[name="rowHide' + matcode + '"]').attr("hidden", false);
                    $('#rowShow' + matcode).css("cursor", "pointer");
                    $('#rowShow' + matcode).css("background-color", "#003682");
                    $('#rowShow' + matcode).css("color", "white");
                } else {
                    $('tr[name="rowHide' + matcode + '"]').attr("hidden", true);
                    $('#rowShow' + matcode).css("cursor", "");
                    $('#rowShow' + matcode).css("background-color", "");
                    $('#rowShow' + matcode).css("color", "black");
                }

            }

            function numberWithCommas(x) { //comma every 3 digits
                return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            }

        </script>
        <style>
            #SOrder,#invNoFrom,#invNoTo,#fkdate{
                font-size: 18px;
            }

            .loader {
                border: 16px solid #f3f3f3; /* Light grey */
                border-top: 16px solid #3498db; /* Blue */
                border-radius: 50%;
                width: 120px;
                height: 120px;
                animation: spin 2s linear infinite;
            }

            @keyframes spin {
                0% {
                    transform: rotate(0deg);
                }
                100% {
                    transform: rotate(360deg);
                }
            }

            input[type=submit] {
                clear:none;
                width: 100%;
                background-color: #00399b;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=submit]:hover {
                background-color: #3973d6;
            }

        </style>
    </head>
    <body>
        <div id="wrapper">
            <!-- /#sidebar-wrapper -->
            <%@ include file="../fragments/sidebar.jsp" %>

            <!-- Page Content -->
            <!-- nav-head-custom -->
            <%@ include file="../fragments/nav_head.jsp" %>

            <div class="container-fluid">

                <form action="../resources/manual/ISM510.pdf" target="_blank">
                    <table width="100%">
                        <tr>
                            <td width="94%" align="left">
                            </td>
                            <td width="6%" align="right"><input type="submit" value="Manual" style="font-weight: normal;" /></td>
                        </tr>
                    </table>
                </form>

                <input type="hidden" id="uid">
                <!--<br>-->

                <div class="col-lg-12" style="width: 100%; border:2px solid #ccc; margin-top: 5px; border-radius: 4px;">
                    <b class="page-header" style="padding-left:5px;font-size:18px;">

                        <div class="row">
                            <div class="col-xs-6" align="left">
                                <div class="col-xs-3" align="right">
                                    <!--<label style="margin-top:5px;" >Sales Order : </label>-->
                                    <!--<label>ชื่อลูกค้า : </label>-->
                                </div>
                                <div class="col-xs-3" align="left">
                                    <!--<label id="cusname" style="color:black;"></label>-->
                                    <!--<input id="SOrder" name="sales_input" class="form-control" readonly>-->
                                </div>
                                <div class="col-xs-6" align="right"></div>
                            </div>
                            <!--<div class="col-xs-1" align="left"></div>-->
                            <div class="col-xs-6" align="left">
                                <div class="col-xs-2" align="left"></div>
                                <div class="col-xs-6" align="right">
                                    <label style="margin-top:5px;" >เลขที่ : </label>
                                </div>
                                <div class="col-xs-2" align="left"><input id="invNoFrom" name="inv_input_from" class="form-control"></div>
                                <div class="col-xs-2" align="left"><input id="invNoTo" name="inv_input_to" class="form-control"></div>
                            </div>
                        </div>

                        <div class="row" style="height:10px;"></div>

                        <div class="row">
                            <div class="col-xs-6" align="left">
                                <div class="col-xs-3" align="right"></div>
                                <div class="col-xs-3" align="left"></div>
                                <div class="col-xs-6" align="right"></div>
                            </div>
                            <!--<div class="col-xs-1" align="left"></div>-->
                            <div class="col-xs-6" align="left">
                                <div class="col-xs-2" align="left"></div>
                                <div class="col-xs-6" align="right">
                                    <label style="margin-top:5px;" >วันที่ : </label>
                                </div>
                                <div class="col-xs-4" align="left"><input id="fkdate" name="fkdate_input" class="form-control" readonly></div>
                            </div>
                        </div>

                        <div class="row" style="height:10px;"></div>

                        <div class="row">
                            <div class="col-xs-6" align="left">
                                <div class="col-xs-3" align="right">

                                </div>
                                <div class="col-xs-7" align="left">

                                </div>
                                <div class="col-xs-2" align="right"></div>
                            </div>
                            <div class="col-xs-1" align="left"></div>
                            <div class="col-xs-5" align="left">
                                <div class="col-xs-2" align="left"></div>
                                <div class="col-xs-6" align="right"></div>
                                <div class="col-xs-4" align="left"></div>
                            </div>
                        </div>

                        <div class="row" style="height:10px;"></div>

                        <div class="row">
                            <div class="col-xs-6" align="left">
                                <div class="col-xs-3" align="right">
                                    <!--<label>ที่อยู่ : </label>-->
                                </div>
                                <div class="col-xs-7" align="left">
                                    <!--<label id="address12" style="color:black;"></label>-->
                                </div>
                                <div class="col-xs-2" align="right"></div>
                            </div>
                            <div class="col-xs-1" align="left"></div>
                            <div class="col-xs-5" align="left">
                                <div class="col-xs-2" align="left"></div>
                                <div class="col-xs-6" align="right"></div>
                                <div class="col-xs-4" align="left"></div>
                            </div>
                        </div>

                        <div class="row" style="height:10px;"></div>

                        <div class="row">
                            <div class="col-xs-6" align="left">
                                <div class="col-xs-3" align="right"></div>
                                <div class="col-xs-7" align="left">
                                    <!--<label id="address34" style="color:black;"></label>-->
                                </div>
                                <div class="col-xs-2" align="right"></div>
                            </div>
                            <div class="col-xs-1" align="left"></div>
                            <div class="col-xs-5" align="left">
                                <div class="col-xs-2" align="left"></div>
                                <div class="col-xs-6" align="right"></div>
                                <div class="col-xs-4" align="left"></div>
                            </div>
                        </div>

                        <!--<div class="row" style="height:10px;"></div>-->

                        <div class="row">
                            <div class="col-sm-1" align="right" style="width:1%;"></div>
                            <div class="col-sm-10" align="right" style="width:98%;">
                                <table id="ism500Table" class="display" style="width: 100%; border: 1px solid rgb(204, 204, 204); border-radius: 4px;">
                                    <thead>
                                        <tr>
                                            <th>Customer</th>
                                            <th>รหัสสินค้า</th>
                                            <th>รายการ</th>
                                            <th>QTY ตามสูตร</th>
                                            <th>เบิกจริง</th>
                                            <th>จำนวน Inv</th>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                            <div class="col-sm-1" align="right" style="width:1%;"></div>
                        </div>
                        <br>
                    </b>
                </div>

                <!-- Modal -->
                <div class="modal fade" id="foundData" role="dialog">
                    <div class="modal-dialog">
                        <center>
                            <!-- Modal content-->
                            <div class="modal-content" style="width:500px;">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title"></h4>
                                </div>
                                <div class="modal-body">
                                    <h4 class="modal-title" >พบข้อมูลของ Invoice : <label id="invtext"></label></h4>
                                </div>
                                <div class="modal-footer">
                                    <center>
                                        <button type="button" style="width: 130px;" id="dwnAgainBtn" class="btn btn-success" data-dismiss="modal">Download Again</button>
                                        <button type="button" style="width: 130px;" id="okBtn" class="btn btn-danger" data-dismiss="modal">OK</button>
                                    </center>
                                </div>
                            </div>
                        </center>
                    </div>
                </div>

                <!-- Modal -->
                <div class="modal fade" id="InsertError" role="dialog">
                    <div class="modal-dialog">
                        <center>
                            <!-- Modal content-->
                            <div class="modal-content" style="width:500px;">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title"></h4>
                                </div>
                                <div class="modal-body">
                                    <h4 class="modal-title" >Download Invoice : <label id="invtext"></label> ไม่สำเร็จ </h4>
                                </div>
                                <div class="modal-footer">
                                    <center>
                                        <button type="button" style="width: 130px;" id="" class="btn btn-danger" data-dismiss="modal">OK</button>
                                    </center>
                                </div>
                            </div>
                        </center>
                    </div>
                </div>

                <center>
                    <div id="loadModal" class="modal fade" role="dialog">
                        <div class="modal-dialog modal-sm">
                            <div class="modal-md col-md-6" style="top: 150px; left:300px; background-color: #ddd; border: 1px solid #ccc; border-radius: 4px;">
                                <div class="modal-header">
                                    <h4 class="modal-title" align="center">Loading ...</h4>
                                    <br>
                                    <div id="loadme" class="loader"></div>
                                    <br>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- devbanban.com -->
                </center>

            </div>
        </div>
    </body>
</html>
