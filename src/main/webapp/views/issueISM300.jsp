<%@ include file="../includes/taglibs.jsp" %>
<%@ include file="../includes/imports.jsp" %>
<!DOCTYPE html>
<html lang="en">
    <%@ include file="../includes/head.jsp" %>

    <body>

        <style>
            input[type=submit] {
                clear:none;
                width: 100%;
                background-color: #00399b;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=submit]:hover {
                background-color: #3973d6;
            }
        </style>

        <div id="wrapper">



            <!-- /#sidebar-wrapper -->
            <%@ include file="../fragments/sidebar.jsp" %>

            <!-- Page Content -->
            <!-- nav-head-custom -->
            <%@ include file="../includes/MSS_head.jsp" %>
            <div class="container-fluid">
                <form action="../resources/manual/ISM300.pdf" target="_blank">
                    <table width="100%">
                        <tr>
                            <td width="94%" align="left">
                            </td>
                            <td width="6%" align="right"><input type="submit" value="Manual" style="font-weight: normal;" /></td>
                        </tr>
                    </table>
                </form>

                <div id="wrapper-top">
                    <div class="row">
                        <div class="col-lg-12">
                            <div id="set-height" style="height:265px;margin-top:40px;">
                                <div id="sidebar-wrapper-top">
                                    <b class="page-header" style="padding-left:5px;font-size:18px;">
                                        <table>
                                            <tr>
                                                <td width="130px;">
                                                    Customer
                                                </td>
                                                <td width="20px;" align="center"> : </td>
                                                <td>
                                                    <input id="sPOFG" value="${POFG}" class="form-control" type="text" onkeyup="javascript:this.value = this.value.toUpperCase();getCustomerName();getDropDownGroup();getDropDownSQ();">
                                                </td>
                                                <td>
                                                    <div id="customerName" style="margin-left:20px;color:#777;font-size:16px;"></div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Mat-Ctrl
                                                </td>
                                                <td width="20px;" align="center"> : </td>
                                                <td>
                                                    <input id="sLGPBE" value="${LGPBE}" class="form-control" style="margin-top:10px;" type="text" onkeyup="javascript:this.value = this.value.toUpperCase();getMatName();">
                                                </td>
                                                <td>
                                                    <div id="MatName" style="margin-left:20px;color:#777;font-size:16px;"></div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Job no.
                                                </td>
                                                <td width="20px;" align="center"> : </td>
                                                <td>
                                                    <div id="dpGRPNO" style="margin-top:10px;">
                                                        <input class="form-control" type="text" disabled>
                                                    </div>
                                                </td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Sequence no.
                                                </td>
                                                <td width="20px;" align="center"> : </td>
                                                <td>
                                                    <div id="dpSEQNO" style="margin-top:10px;">
                                                        <input class="form-control" type="text" disabled>
                                                    </div>
                                                </td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td></td>
                                                <td height="34px;">
                                                    <div id="checkB" style="font-size:17px;margin-top:10px;padding-left:5px;color:#777">
                                                        <!-- <input id="sCHECK" type="checkbox" > Not enough -->
                                                    </div>
                                                </td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td></td>
                                                <td>
                                                    <button onclick="search()" style="width:100%;margin-top:10px;" class="btn btn-outline btn-primary">
                                                        <b><i class="fa fa-search icon-size-sm"></i> Search</b>
                                                    </button>
                                                </td>
                                                <td></td>
                                            </tr>
                                        </table>
                                    </b>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="toggle-top" style="position:relative;">
                        <button type="button" onclick="printList()" style="position:absolute;right:31.5% ;top:-40px;width:15%;z-index: 2000;font-weight:bold;" class="btn btn-outline btn-warning"><i class="fa fa-print"></i> Print MSS105</button>

                        <script type="text/javascript">
                            function printList() {
                                var POFG = $('#sPOFG').val();
                                var LGPBE = $("#sLGPBE").val();
                                var GRPNO = $("#sGRPNO").val();
                                var ISSUENO = $("#sISSUENO").val();
                                if (POFG == "" || LGPBE == "" || GRPNO == "undefined" || ISSUENO == "undefined") {
                                    alertify.error("Please fill out search form.");
                                } else {
                                    window.open("http://10.11.9.142:8080/MSS105/controller/GetDataCtrl.jsp?a=report&POFG=" + POFG + "&LGPBE=" + LGPBE + "&GRPNO=" + GRPNO + "&ISSUENO=" + ISSUENO);
                                }
                            }
                        </script>

                        <button type="button" onclick="printList2()" style="position:absolute;right:16% ;top:-40px;width:15%;z-index: 2000;font-weight:bold;" class="btn btn-outline btn-warning"><i class="fa fa-print"></i> Print MSS106</button>

                        <script type="text/javascript">
                            function printList2() {
                                var POFG = $('#sPOFG').val();
                                var LGPBE = $("#sLGPBE").val();
                                var GRPNO = $("#sGRPNO").val();
                                var ISSUENO = $("#sISSUENO").val();
                                if (POFG == "" || LGPBE == "" || GRPNO == "undefined" || ISSUENO == "undefined") {
                                    alertify.error("Please fill out search form.");
                                } else {
                                    window.open("http://10.11.9.142:8080/MSS106/controller/GetDataCtrl.jsp?a=report&POFG=" + POFG + "&LGPBE=" + LGPBE + "&GRPNO=" + GRPNO + "&ISSUENO=" + ISSUENO);
                                }
                            }
                        </script>

                        <a style="position:absolute;right:0 ;top:-40px;width:15%;z-index: 2000;" href="#menu-toggle2" class="btn btn-outline btn-default  custom-toggle-menu" id="menu-toggle2">
                            <div id="i-toggle-top"></div>
                        </a>
                        <hr style="margin:0px;margin-top:10px;margin-bottom:10px;">
                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-primary" style="min-height: 485px;">
                                <table class="pull-right" style="position:absolute;top:5.5px;right:25px;font-size:14px;">
                                    <tr>
                                        <td>
                                            <button onclick="backwardfunc()" class="btn btn-default btn-outline btn-sm custom-icon-w-b">
                                                <b style="color:white">Backward</b>
                                            </button>
                                            <script type="text/javascript">
                                                function backwardfunc() {
                                                    var jsbw = [];
                                                    var jsRow = [];
                                                    var sPOFG = $('#sPOFG').val();
                                                    var sLGPBE = $('#sLGPBE').val();
                                                    var sGRPNO = $('#sGRPNO').val();
                                                    var sISSUENO = $('#sISSUENO').val();
                                                    jsbw.push(sPOFG);
                                                    jsbw.push(sLGPBE);
                                                    jsbw.push(sGRPNO);
                                                    jsbw.push(sISSUENO);
                                                    $('#dataTables-example > tbody > tr').each(function () {
//                                                        console.log($(this).find('td').eq(0).text().trim());
                                                        jsRow.push($(this).find('td').eq(0).text().trim());
                                                    });

//                                                    jsbw.push(jsRow);
//                                                    console.log(jsbw);

                                                    var myJsonString = JSON.stringify(jsbw);
                                                    var myJsonString2 = JSON.stringify(jsRow);

                                                    if (myJsonString !== "[]") {
                                                        $.ajax({
                                                            url: "../ISM300AJAX",
                                                            data: {a: "300BackWard", jsonText: myJsonString, jsonText2: myJsonString2},
                                                            async: false
                                                        }).done(function (result) {

                                                        }).fail(function (jqXHR, textStatus, errorThrown) {
                                                            // needs to implement if it fails
                                                        });
                                                    } else {
                                                        alertify.error("Error Json Empty.");
                                                    }

//                                                    window.location = "MC?p=3&a=list&POFG=${POFG}&LGPBE=${LGPBE}&GRPNO=${GRPNO}&ISSUENO=${ISSUENO}";
                                                }
                                            </script>
                                        </td>
                                        <td>&nbsp;&nbsp;</td>
                                        <td>
                                            <button onclick="requestApproval()" class="btn btn-default btn-outline btn-sm custom-icon-w-b">
                                                <b style="color:white">Request Approval</b>
                                            </button>
                                            <script type="text/javascript">
                                                function requestApproval() {
                                                    window.location = "MC?p=3&a=list&POFG=${POFG}&LGPBE=${LGPBE}&GRPNO=${GRPNO}&ISSUENO=${ISSUENO}";
                                                }
                                            </script>
                                        </td>
                                    </tr>
                                </table>

                                <div class="panel-heading">
                                    <h4 style="margin:0px;">Materials Issue</h4>
                                </div>
                                <!-- scan data
                                        <b class="page-header" style="padding-left:10px;font-size:18px;">
                                                        <table width="99%" >
                                                                <td width="100px;"  align="center"  style="margin-top:2px;padding- top: 10px;" ><p>Scan  :
                            </td>
                            <td style="padding- top: 30px;">
                                <input id="ipScan" class="form-control" style="margin-top:2px;border-width: 0px 0px 1px 0px;width: 100%;height: 40px;"  type="text">
                            </td>
                                                        </table>
                </b>
                                -->

                                <!-- /.panel-heading -->
                                <div class="panel-body">
                                    <div class="dataTable_wrapper">
                                        <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                            <thead>
                                                <tr>
                                                    <th style="border-bottom:0px;">Material</th>
                                                    <th style="border-bottom:0px;">Description</th>
                                                    <th style="border-bottom:0px;min-width:70px;">Dest</th>
                                                    <th style="border-bottom:0px;min-width:70px;">Onhand</th>
                                                    <th style="border-bottom:0px;min-width:70px;">T. Req.</th>
                                                    <th style="border-bottom:0px;min-width:70px;">T. Pick.</th>
                                                    <th style="border-bottom:0px;min-width:70px;">Remain.</th>
                                                    <th style="border-bottom:0px;min-width:70px;">Pick.</th>
                                                    <th style="border-bottom:0px;min-width:80px;">Action</th>
                                                </tr>
                                            </thead>

                                            <tfoot>
                                            <th style="border-left:0px;">Material</th>
                                            <th style="border-left:0px;">Description</th>
                                            <th style="border-left:0px;">Dest</th>
                                            <th style="border-left:0px;">Onhand</th>
                                            <th style="border-left:0px;">T. Req.</th>
                                            <th style="border-left:0px;">T. Pick.</th>
                                            <th style="border-left:0px;">Remain.</th>
                                            <th style="border-left:0px;"></th>
                                            <th style="border-left:0px;"></th>
                                            </tfoot>

                                            <tbody>

                                                <c:forEach items="${listIssue}" var="MPDATAD">
                                                    <c:choose>
                                                        <c:when test="${MPDATAD.DIFF < 0}">
                                                            <tr style="color:red">
                                                            </c:when>
                                                            <c:otherwise>
                                                            <tr>
                                                            </c:otherwise>
                                                        </c:choose>

                                                        <td style="padding-top:14px;">
                                                            <c:out value="${MPDATAD.MATNR}" escapeXml="false" />
                                                        </td>
                                                        <td style="padding-top:14px;">
                                                            <c:out value="${MPDATAD.ARKTX}" escapeXml="false" />
                                                        </td>
                                                        <td align="right" style="padding-top:14px;">
                                                            <c:out value="${MPDATAD.TYPE}" escapeXml="false" />
                                                        </td>
                                                        <td align="right" style="padding-top:14px;">
                                                            <fmt:formatNumber type="number" pattern="###,##0.000" value="${MPDATAD.ONHAND}" />
                                                        </td>
                                                        <td align="right" style="padding-top:14px">
                                                            <fmt:formatNumber type="number" pattern="###,##0.000" value="${MPDATAD.KWMENG}" />
                                                        </td>

                                                        <td id="${fn:trim(MPDATAD.MATNR)}_ISSUETOPICK" align="right" style="padding-top:14px;">
                                                            <fmt:formatNumber type="number" pattern="###,##0.000" value="${MPDATAD.ISSUETOPICK}" />
                                                        </td>
                                                        <td id="${fn:trim(MPDATAD.MATNR)}_REMAIN" align="right" style="padding-top:14px;">
                                                            <fmt:formatNumber type="number" pattern="###,##0.000" value="${MPDATAD.REMAIN}" />
                                                        </td>
                                                        <td align="center">
                                                            <input id="${fn:trim(MPDATAD.MATNR)}_ISSUEPICK" style="margin:0px;text-align:right;font-size:15px;width:100px;" class="form-control input-sm" type="number">
                                                        </td>
                                                        <td align="center">

                                                            <button onclick="update('${fn:trim(MPDATAD.MATNR)}', '${MPDATAD.KWMENG}')" style="margin-right:10px;" type="button" class="btn btn btn-outline btn-success btn-sm btn-size-sm"><i class="fa fa-save icon-size-sm"></i></button>

                                                            <button onclick="packing('${MPDATAD.MATNR}', '${MPDATAD.KWMENG}')" type="button" class="btn btn btn-outline btn-warning btn-sm btn-size-sm" </button><i class="fa fa-dropbox icon-size-sm"></i></button>


                                                            <script type="text/javascript">
                                                                function update(id, total) {
                                                                    var ISSUEPICK = $('#' + id + '_ISSUEPICK').val();

                                                                    if (ISSUEPICK != "") {
                                                                        $.get('../ISM300AJAX', {
                                                                            a: 'update',
                                                                            GRPNO: '${GRPNO}',
                                                                            ISSUENO: '${ISSUENO}',
                                                                            MATNR: id,
                                                                            ISSUEPICK: ISSUEPICK,
                                                                            KWMENG: total
                                                                        }, function (responseText) {
                                                                            var text = responseText.split("|");
                                                                            $('#' + id + '_ISSUETOPICK').html(text[0]);
                                                                            $('#' + id + '_REMAIN').html(text[1]);
                                                                            $('#' + id + '_ISSUEPICK').val("");
                                                                            alertify.success("<b>Update successful.</b>");
                                                                        });
                                                                    } else {
                                                                        alertify.error("<b>Please fill out Pick from.</b>");
                                                                    }

                                                                }

                                                                function packing(MATNR, qtyreq) {
                                                                    //new mod
                                                                    //window.alert(qtyreq);
                                                                    var POFG = $('#sPOFG').val();
                                                                    var LGPBE = $("#sLGPBE").val();
                                                                    var GRPNO = $("#sGRPNO").val();
                                                                    var ISSUENO = $("#sISSUENO").val();

                                                                    window.location = "MC?&Topreq=" + qtyreq + "&p=2&a=list&POFG=" + POFG + "&LGPBE=" + LGPBE + "&GRPNO=" + GRPNO + "&ISSUENO=" + ISSUENO + "&MATNR=" + MATNR;
                                                                }
                                                            </script>

                                                        </td>
                                                    </tr>
                                                </c:forEach>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <script>
                var sidebarVisible = localStorage.getItem('wrapper');
                var sidebarVisible2 = localStorage.getItem('wrapper-top');
                var checkIMenu = $("#i-menu").html();
                var checkToggleTop = $("#i-toggle-top").html();

                $(document).ready(function () {

                    toggleClick();

                    toggleLoad();

                    toggleClick2();

                    toggleLoad2();

                    getTime();

                    getCustomerName();

                    getMatName();

                    getDropDownGroup();

                    getDropDownSQ();

                    if ("${CHECK}" == "1") {
                        $('#sCHECK').prop('checked', true);
                    }

                    setInterval(function () {
                        getTime();
                    }, 1000);

                    $('#dataTables-example').DataTable({
                        "aLengthMenu": [
                            [4, 20, 50, -1],
                            [4, 20, 50, "All"]
                        ],
                        "iDisplayLength": -1,
                        "bStateSave": true,
                        "aoColumnDefs": [{
                                'bSortable': false,
                                'aTargets': [1]
                            }, {
                                'bSortable': false,
                                'aTargets': [7]
                            }, {
                                'bSortable': false,
                                'aTargets': [8]
                            }],

                        responsive: true
                    });

                    $('#dataTables-example tfoot th').not(":eq(7),:eq(8)").each(function () {
                        var title = $(this).text();
                        $(this).html('<input type="text" class="form-control input-sm" style="width:100%" placeholder="' + title + '" />');
                    });

                    // DataTable
                    var table = $('#dataTables-example').DataTable();

                    // Apply the search
                    table.columns().every(function () {
                        var that = this;

                        $('input', this.footer()).on('keyup change', function () {
                            if (that.search() !== this.value) {
                                that.search(this.value).draw();
                            }
                        });
                    });

                    var state = table.state.loaded();
                    if (state) {
                        table.columns().eq(0).each(function (colIdx) {
                            var colSearch = state.columns[colIdx].search;

                            if (colSearch.search) {
                                $('input', table.column(colIdx).footer()).val(colSearch.search);
                            }
                        });

                        table.draw();
                    }

                });

                function toggleLoad2() {
                    if (sidebarVisible2 != null && sidebarVisible2 == 1) {
                        $("#wrapper-top").css({
                            "-webkit-transition": "all 0.0s ease"
                        }, {
                            "-moz-transition": "all 0.0s ease"
                        }, {
                            "-o-transition": "all 0.0s ease"
                        }, {
                            "transition": "all 0.0s ease"
                        });
                        $("#sidebar-wrapper-top").css({
                            "-webkit-transition": "all 0.0s ease"
                        }, {
                            "-moz-transition": "all 0.0s ease"
                        }, {
                            "-o-transition": "all 0.0s ease"
                        }, {
                            "transition": "all 0.0s ease"
                        });
                        $("#wrapper-top").toggleClass("toggled");
                        $("#i-toggle-top").html("<b class=\"custom-text\">Show search</b>");
                        // $("#set-height").css({"margin-top":"50px"});
                    } else {
                        $("#i-toggle-top").html("<b class=\"custom-text\">Hide search</b>");
                    }
                }

                function toggleClick2() {
                    $("#menu-toggle2").click(function (e) {

                        e.preventDefault();
                        $("#wrapper-top").toggleClass("toggled");

                        if ($("#i-toggle-top").html() == "<b class=\"custom-text\">Show search</b>") {
                            $("#wrapper-top").css({
                                "-webkit-transition": "all 0.5s ease"
                            }, {
                                "-moz-transition": "all 0.5s ease"
                            }, {
                                "-o-transition": "all 0.5s ease"
                            }, {
                                "transition": "all 0.5s ease"
                            });
                            $("#sidebar-wrapper-top").css({
                                "-webkit-transition": "all 0.5s ease"
                            }, {
                                "-moz-transition": "all 0.5s ease"
                            }, {
                                "-o-transition": "all 0.5s ease"
                            }, {
                                "transition": "all 0.5s ease"
                            });
                            $("#i-toggle-top").html("<b class=\"custom-text\">Hide search</b>");
                            // $("#toggle-top").css({"margin-top":"0px"});
                            localStorage.setItem('wrapper-top', 0);
                        } else {
                            $("#i-toggle-top").html("<b class=\"custom-text\">Show search</b>");
                            // $("#toggle-top").css({"margin-top":"50px"});
                            localStorage.setItem('wrapper-top', 1);
                        }

                    });
                }


                function getCustomerName() {
                    var POFG = $("#sPOFG").val();
                    $.get('../ISM300AJAX', {
                        a: 'getCustomerName',
                        POFG: POFG
                    }, function (responseText) {
                        $('#customerName').html(responseText);
                    });
                }

                function getMatName() {
                    var LGPBE = $("#sLGPBE").val();
                    $.get('../ISM300AJAX', {
                        a: 'getMatName',
                        LGPBE: LGPBE
                    }, function (responseText) {
                        $('#MatName').html(responseText);
                    });
                }

                function getDropDownGroup() {
                    var POFG = $("#sPOFG").val();
                    $.get('../ISM300AJAX', {
                        a: 'getDropDownGroup',
                        POFG: POFG
                    }, function (responseText) {
                        if (responseText != "null") {
                            $('#dpGRPNO').html(responseText);
                            $('#sGRPNO').val("${GRPNO}");
                        } else {
                            $('#dpGRPNO').html("<input class=\"form-control\" type=\"text\" disabled>");
                        }
                    });
                }

                function getDropDownSQ() {
                    var POFG = $("#sPOFG").val();
                    $.get('../ISM300AJAX', {
                        a: 'getDropDownSQ',
                        POFG: POFG
                    }, function (responseText) {
                        if (responseText != "null") {
                            $('#dpSEQNO').html(responseText);
                            $('#sISSUENO').val("${ISSUENO}");
                        } else {
                            $('#dpSEQNO').html("<input class=\"form-control\" type=\"text\" disabled>");
                        }
                    });
                }

                function search() {

                    var POFG = $('#sPOFG').val();
                    var LGPBE = $("#sLGPBE").val();
                    var GRPNO = $("#sGRPNO").val();
                    var ISSUENO = $("#sISSUENO").val();

                    if (POFG == "" || LGPBE == "" || GRPNO == "undefined" || ISSUENO == "undefined") {
                        alertify.error("Please fill out search form.");
                    } else {
                        window.location = "MC?a=list&POFG=" + POFG + "&LGPBE=" + LGPBE + "&GRPNO=" + GRPNO + "&ISSUENO=" + ISSUENO;
                    }

                }
            </script>
        </div>
    </body>

</html>