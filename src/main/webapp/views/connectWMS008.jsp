<%@ include file="../fragments/taglibs.jsp" %>
<%@ include file="../fragments/imports.jsp" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.sql.*;" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>FEATURE MODE</title>
        <!-- css :: vendors -->
        <jsp:include page = "../fragments/css.jsp" />
        <!-- additional custom :: my-style -->
        <link rel="stylesheet" href="../resources/assets/styles/myStyles.css">

        <!-- js :: vendors -->
        <jsp:include page = "../fragments/script.jsp" />
        <!-- additional custom :: my-script -->
        <script src="../resources/assets/scripts/myScripts.js" async></script>
        <script src="../resources/assets/scripts/toggleLoad.js" async></script>
        <script type="text/javascript"> /* add something code to your script */</script>
        ${sendMessage}
        <script>
            function Uppercase() {
                var x = document.getElementById("Scolumn");
                var y = document.getElementById("Fcolumn");
                x.value = x.value.toUpperCase();
                y.value = y.value.toUpperCase();
            }
        </script>
        <script>
            function isNumberKey(evt) {
                var charCode = (evt.which) ? evt.which : event.keyCode
                if (charCode > 31 && (charCode < 48 || charCode > 57))
                    return false;
                return true;
            }
        </script>
        <script>
            function isAlphabetKey(evt) {
                var charCode = (evt.which) ? evt.which : event.keyCode
                if ((charCode < 65 || charCode > 90) && (charCode < 97 || charCode > 122))
                    return false;
                return true;
            }
        </script>
        <style>
            input[type=text], input[type=password], input[type=number], select {
                width: 100%;
                padding: 5px 5px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }

            input[type=submit] {
                width: 100%;
                background-color: #2bd14a;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=submit]:hover {
                background-color: #45a049;
            }

            input[type=button] {
                width: 100%;
                background-color: #ef604a;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=button]:hover {
                background-color: #e51e00;
            }

            button[name=ok] {
                width: 25%;
                background-color: #008cff;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            button[name=ok]:hover {
                background-color: #008cff;
            }
        </style>
        <script>
            function change() // no ';' here
            {
                var elem = document.getElementById("generate");
                var x = document.getElementById("loadIcon");
                if (elem.value === "Generate") {
                    elem.value = "Generating";
                    if (x.style.display === "none") {
                        x.style.display = "block";
                    } else {
                        x.style.display = "none";
                    }
                }

            }
        </script>
    </head>    
    <body onload="document.getElementById('userid').value = sessionStorage.getItem('uid');">
        <div id="wrapper">

            <!-- /#sidebar-wrapper -->
            <%@ include file="../fragments/sidebar.jsp" %>

            <!-- Page Content -->
            <!-- nav-head-custom -->
            <%@ include file="../fragments/nav_head.jsp" %>

            <div class="container-fluid">
                <!--                <div id="wrapper-top" align="left">
                <%-- PART 2 --%>
                <div class="row">
                    <div class="col-lg-12">
                        <div id="set-height" style="height:415px;margin-top:0px;">
                            <div id="sidebar-wrapper-top" class="">
                                <b class="page-header" style="padding-left:5px;font-size:18px;">
                <%-- <hr style="margin:0px;margin-top:10px;margin-bottom:10px;"> --%>
            </b>
        </div>
    </div>
</div>
</div> -->

                <%-- PART 3 --%>
                <form action="connect" method="get" name="frm">
                    <input type="hidden" id="userid" name="userid">
                    <table frame="box" width="100%" bordercolor="#e0e0e0">
                        <tr>
                            <th height="50" bgcolor="#f8f8f8"></th>
                            <th height="50" bgcolor="#f8f8f8">Feature</th>
                            <th height="50" bgcolor="#f8f8f8">Start</th>
                            <th height="50" bgcolor="#f8f8f8">Finish</th>
                            <th height="50" bgcolor="#f8f8f8">Option
                                &nbsp;&nbsp;&nbsp;<a href="genrack?wh=${wh}"><img src="../resources/images/icons/generate.png" width="30" height="30" ></a>
                            </th>
                        </tr>
                        <br>
                        <tr>
                            <td width="10%"></td>
                            <td width="20%" align="left"><h4>Warehouse :</h4></td>
                            <td width="20%" align="left"><select name="wh"  onchange="this.form.submit()">
                                    <option value="${wh}" selected hidden>${wh} : ${whn}</option>
                                    <c:forEach items="${WHList}" var="p" varStatus="i">
                                        <option value="${p.code}">${p.code} : ${p.name}</option>
                                    </c:forEach>
                                </select></td>
                            <td width="20%"></td>
                            <td width="20%"></td>
                        </tr>
                        <tr>
                            <td width="10%"></td>
                            <td width="20%" align="left"><h4>Rack no. :</h4></td>
                            <td width="20%" align="left" style="padding-right: 150px;"><input type="text" id="Srkno" name="Srkno" maxlength="2" value="${Srkno}" onkeypress="return isNumberKey(event)"></td>
                            <td width="20%" align="left" style="padding-right: 150px;"><input type="text" id="Frkno" name="Frkno" maxlength="2" value="${Frkno}" onkeypress="return isNumberKey(event)"></td>
                            <td width="20%"></td>
                        </tr>
                        <tr>
                            <td width="10%"></td>
                            <td width="20%" align="left"><h4>Side(2) :</h4></td>
                            <td width="20%" align="left" style="padding-right: 150px;">
                                <select id="Sside" name="Sside">
                                    <option value="${Sside}" selected hidden>${Sside}</option>
                                    <option value=""></option>
                                    <option value="L">L</option>
                                </select>
                            </td>
                            <td width="20%" align="left" style="padding-right: 150px;">
                                <select id="Fside" name="Fside">
                                    <option value="${Fside}" selected hidden>${Fside}</option>
                                    <option value=""></option>
                                    <option value="R">R</option>
                                </select>
                            </td>
                            <td width="20%"></td>
                        </tr>
                        <tr>
                            <td width="10%"></td>
                            <td width="20%" align="left"><h4>Column :</h4></td>
                            <td width="20%" align="left" style="padding-right: 150px;"><input type="text" id="Scolumn" name="Scolumn" maxlength="1" value="${Scolumn}" onkeypress="return isAlphabetKey(event)" onkeyup="return Uppercase()"></td>
                            <td width="20%" align="left" style="padding-right: 150px;"><input type="text" id="Fcolumn" name="Fcolumn" maxlength="1" value="${Fcolumn}" onkeypress="return isAlphabetKey(event)" onkeyup="return Uppercase()"></td>
                            <td width="20%"></td>
                        </tr>
                        <tr>
                            <td width="10%"></td>
                            <td width="20%" align="left"><h4>Row :</h4></td>
                            <td width="20%" align="left" style="padding-right: 150px;"><input type="text" id="Srow" name="Srow" maxlength="1" value="${Srow}" onkeypress="return isNumberKey(event)"></td>
                            <td width="20%" align="left" style="padding-right: 150px;"><input type="text" id="Frow" name="Frow" maxlength="1" value="${Frow}" onkeypress="return isNumberKey(event)"></td>
                            <td width="20%"></td>
                        </tr>
                        <br>
                        <div align="center">
                            <tr>
                                <td></td>
                                <td></td>
                                <td width="20%" align="right"><input style="width: 100px;" type="button" value="Back" onclick="window.location.href = '/TABLE2/WMS008/display'"/>
                                    <input style="width: 100px;" type="submit" value="Generate" id="generate" formmethod="post" onclick="return change()"/></td>
                                <td><div id="loadIcon" style="height:100%; display: none;">&nbsp;&nbsp;<i class='fa fa-spinner fa-spin ' style="font-size:30px;"></i></div></td>
                                <td></td>
                            </tr>
                        </div>
                    </table>
                    <center>
                        <div id="myModal3" class="modal fade" role="dialog">
                            <div class="modal-dialog modal-sm"> <!-- ???????????? modal ?????????????? ???? xs, sm, md, lg -->
                                <div class="modal-md col-md-6" style="top: 150px; left:300px; background-color: #ddd; border: 1px solid #ccc; border-radius: 4px;">
                                    <div class="modal-header">
                                        <h4 class="modal-title" align="center">Generate Rack !</h4>
                                        <h4 align="left">Added Rack no. : 
                                            <c:forEach items="${addList}" var="p" varStatus="i">
                                                ${p.desc}
                                                <c:if test="${!i.last}">,</c:if>
                                            </c:forEach>
                                        </h4>
                                        <h4 align="left">Duplicate Rack no. : 
                                            <c:forEach items="${dupList}" var="p" varStatus="i">
                                                ${p.desc}
                                                <c:if test="${!i.last}">,</c:if>
                                            </c:forEach>
                                        </h4>
                                        <h4 align="left">Not found Rack no. : 
                                            <c:forEach items="${nuzList}" var="p" varStatus="i">
                                                ${p.desc}
                                                <c:if test="${!i.last}">,</c:if>
                                            </c:forEach>
                                        </h4>
                                        <br>
                                        <button name="ok" type="button" onclick="window.location.href = '/TABLE2/WMS008/connect?wh=${wh}'">
                                            <font color = "white">OK</font>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- devbanban.com -->
                    </center>
                    <center>
                        <div id="myModal1" class="modal fade" role="dialog">
                            <div class="modal-dialog modal-sm"> <!-- ???????????? modal ?????????????? ???? xs, sm, md, lg -->
                                <div class="modal-md col-md-6" style="top: 150px; left:300px; background-color: #ddd; border: 1px solid #ccc; border-radius: 4px;">
                                    <div class="modal-header">
                                        <h4 class="modal-title" align="center">Please fill in the blanks !</h4>
                                        <br>
                                        <button name="ok" type="button" class="close" data-dismiss="modal">
                                            <font color = "white">OK</font>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- devbanban.com -->
                    </center>
                </form>
                <!--End Part 3-->
                <!--</div>  end #wrapper-top -->
            </div> <!-- end .container-fluid -->
        </div> <!-- end #wrapper -->


    </body>

</html>