<%-- 
    Document   : displayISM401
    Created on : Nov 2, 2022, 9:30:06 AM
    Author     : 93176
--%>

<%@ include file="../fragments/taglibs.jsp" %>
<%@ include file="../fragments/imports.jsp" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>ISM</title>

        <!-- css :: vendors -->
        <jsp:include page="../fragments/css.jsp" />
        <!-- additional custom :: my-style -->
        <link rel="stylesheet" href="../resources/assets/styles/myStyles.css">
        <link rel="stylesheet" href="../resources/font/THsarabun/thsarabumnew.css">

        <!-- js :: vendors -->
        <jsp:include page="../fragments/script.jsp" />
        <!-- additional custom :: my-script -->
        <script src="../resources/assets/scripts/myScripts.js" async></script>
        <script src="../resources/assets/scripts/toggleLoad.js" async></script>

        <script>
            $(document).ready(function () {

                $('#uid').val(sessionStorage.getItem('uid'));

                $.ajax({
                    url: "/TABLE2/ShowDataTablesISM401",
                    data: {mode: "getCustomer"},
                    async: false
                }).done(function (result) {

                    $('#cus_sel').html('');
                    var options = '';

                    options += '<option value="empty" selected></option>';

                    for (var i = 0; i < result.length; i++) {
                        options += '<option value="' + parseInt(result[i].ISHCUNO) + '">' + parseInt(result[i].ISHCUNO) + " : " + result[i].ISHCUNM1 + '</option>';
                    }

                    $('#cus_sel').html(options);

                }).fail(function (jqXHR, textStatus, errorThrown) {
                    // needs to implement if it fails
                });

                $('#cus_sel').change(function () {
                    let cus_number = this.value;

                    if (cus_number !== "empty") {
                        $.ajax({
                            url: "/TABLE2/ShowDataTablesISM401",
                            data: {mode: "getSalesNumbers", cusno: cus_number},
                            async: false
                        }).done(function (result) {

                            $('#sale_sel_from').html('');
                            var options = '';

                            options += '<option value="empty" selected></option>';

                            for (var i = 0; i < result.length; i++) {
                                options += '<option value="' + result[i].ISHORD + '">' + result[i].ISHORD + '</option>';
                            }

                            $('#sale_sel_from').html(options);


                            $('#sale_sel_to').html('');
                            var options = '';

                            options += '<option value="empty" selected></option>';

                            for (var i = 0; i < result.length; i++) {
                                options += '<option value="' + result[i].ISHORD + '">' + result[i].ISHORD + '</option>';
                            }

                            $('#sale_sel_to').html(options);

                        }).fail(function (jqXHR, textStatus, errorThrown) {
                            // needs to implement if it fails
                        });
                    } else {
                        //reset if customer choose empty
                        $('#sale_sel_from').html('');
                        $('#sale_sel_to').html('');
                    }

                });

                $('#sale_sel_from').change(function () {
                    let cus_sel = $('#cus_sel').val();
                    let salesSelF = this.value;
                    let salesSelT = $('#sale_sel_to').val();

                    if (salesSelF !== "empty" && salesSelT !== "empty") {

                        $.ajax({
                            url: "/TABLE2/ShowDataTablesISM401",
                            data: {mode: "getSeq", cusno: cus_sel, salesf: salesSelF, salest: salesSelT},
                            async: false
                        }).done(function (result) {

                            $('#seq_sel_from').html('');
                            var options = '';
                            for (var i = 0; i < result.length; i++) {
                                options += '<option value="' + result[i].ISHSEQ + '">' + String.fromCharCode(parseInt(result[i].ISHSEQ) + 64) + '</option>';
                            }
                            $('#seq_sel_from').html(options);

                            $('#seq_sel_to').html('');

                            options = '';
                            for (var i = 0; i < result.length; i++) {
                                if (i === result.length - 1) {
                                    options += '<option value="' + result[i].ISHSEQ + '" selected>' + String.fromCharCode(parseInt(result[i].ISHSEQ) + 64) + '</option>';
                                } else {
                                    options += '<option value="' + result[i].ISHSEQ + '">' + String.fromCharCode(parseInt(result[i].ISHSEQ) + 64) + '</option>';
                                }
                            }
                            $('#seq_sel_to').html(options);

                        }).fail(function (jqXHR, textStatus, errorThrown) {
                            // needs to implement if it fails
                        });

                    } else {
                        $('#seq_sel_from').html('');
                        $('#seq_sel_to').html('');
                    }

                });

                $('#sale_sel_to').change(function () {
                    let cus_sel = $('#cus_sel').val();
                    let salesSelF = $('#sale_sel_from').val();
                    let salesSelT = this.value;

                    if (salesSelF !== "empty" && salesSelT !== "empty") {

                        $.ajax({
                            url: "/TABLE2/ShowDataTablesISM401",
                            data: {mode: "getSeq", cusno: cus_sel, salesf: salesSelF, salest: salesSelT},
                            async: false
                        }).done(function (result) {

                            $('#seq_sel_from').html('');
                            var options = '';
                            for (var i = 0; i < result.length; i++) {
                                options += '<option value="' + result[i].ISHSEQ + '">' + String.fromCharCode(parseInt(result[i].ISHSEQ) + 64) + '</option>';
                            }
                            $('#seq_sel_from').html(options);

                            $('#seq_sel_to').html('');

                            options = '';
                            for (var i = 0; i < result.length; i++) {
                                if (i === result.length - 1) {
                                    options += '<option value="' + result[i].ISHSEQ + '" selected>' + String.fromCharCode(parseInt(result[i].ISHSEQ) + 64) + '</option>';
                                } else {
                                    options += '<option value="' + result[i].ISHSEQ + '">' + String.fromCharCode(parseInt(result[i].ISHSEQ) + 64) + '</option>';
                                }
                            }
                            $('#seq_sel_to').html(options);

                        }).fail(function (jqXHR, textStatus, errorThrown) {
                            // needs to implement if it fails
                        });

                    } else {
                        $('#seq_sel_from').html('');
                        $('#seq_sel_to').html('');
                    }

                    alphabetizeList('#seq_sel_to');

                });

            });

            function alphabetizeList(gg) {
                var sel = $(gg);
                var selected = sel.val(); // cache selected value, before reordering
                var opts_list = sel.find('option');
                opts_list.sort(function (a, b) {
                    return $(a).text() < $(b).text() ? 1 : -1;
                });
                sel.html('').append(opts_list);
                sel.val(selected); // set cached selected value
            }

            function clickPrint() {

                let cust = $('#cus_sel').val();
                let salesf = $('#sale_sel_from').val();
                let salest = $('#sale_sel_to').val();
                let seqf = $('#seq_sel_from').val();
                let seqt = $('#seq_sel_to').val();

                window.open('/TABLE2/ISM401/print?cust=' + cust + '&salesf=' + salesf + '&salest=' + salest + '&seqf=' + seqf + '&seqt=' + seqt);
            }

        </script>

    </head>
    <body>
        <div id="wrapper">
            <!-- /#sidebar-wrapper -->
            <%@ include file="../fragments/sidebar.jsp" %>

            <!-- Page Content -->
            <!-- nav-head-custom -->
            <%@ include file="../fragments/nav_head.jsp" %>

            <div class="container-fluid">
                <input type="hidden" id="uid">
                <div class="col-lg-12" style="width: 100%; border:2px solid #ccc; margin-top: 5px; border-radius: 4px;">
                    <b class="page-header" style="padding-left:5px;font-size:18px;">

                        <div class="row">
                            <div class="col-sm-2" align="left">
                                <div class="col-sm-3" align="right"></div>
                                <div class="col-sm-7" align="left">Customer</div>
                                <div class="col-sm-2" align="right">:</div>
                            </div>
                            <div class="col-sm-2" align="left">
                                <select id="cus_sel" class="form-control"></select>
                            </div>
                        </div>

                        <div class="row" style="height:10px;"></div>

                        <div class="row">
                            <div class="col-sm-2" align="left">
                                <div class="col-sm-3" align="right"></div>
                                <div class="col-sm-7" align="left">Sale no.</div>
                                <div class="col-sm-2" align="right">:</div>
                            </div>
                            <div class="col-sm-2" align="left">
                                <select id="sale_sel_from" class="form-control">
                                    <option></option>
                                </select>
                            </div>
                            <div class="col-sm-2" align="left">
                                <select id="sale_sel_to" class="form-control">
                                    <option></option>
                                </select>
                            </div>
                            <div class="col-sm-6" align="left"></div>
                        </div>

                        <div class="row" style="height:10px;"></div>

                        <div class="row">

                            <div class="col-sm-2" align="left">
                                <div class="col-sm-3" align="right"></div>
                                <div class="col-sm-7" align="left">Sequence no.</div>
                                <div class="col-sm-2" align="right">:</div>
                            </div>
                            <div class="col-sm-2" align="left">
                                <select id="seq_sel_from" class="form-control">
                                    <option></option>
                                </select>
                            </div>
                            <div class="col-sm-2" align="left">
                                <select id="seq_sel_to" class="form-control">
                                    <option></option>
                                </select>
                            </div>
                            <div class="col-sm-6" align="left"></div>

                        </div>

                        <div class="row" style="height:10px;"></div>

                        <div class="row">

                            <div class="col-xs-2" align="left">
                                <div class="col-xs-3" align="left"></div>
                                <div class="col-xs-7" align="left"></div>
                                <div class="col-xs-2" align="right"></div>
                            </div>
                            <div class="col-xs-2" align="left">
                                <button class="form-control btn btn-warning btn-outline" onclick="clickPrint()">
                                    <b style="font-size: 14px;">
                                        <i class="fa fa-print"></i>
                                        Print ISM401
                                    </b>
                                </button>
                            </div>
                            <div class="col-xs-2" align="left"></div>
                            <div class="col-xs-6" align="left"></div>

                        </div>

                        <div class="row" style="height:10px;"></div>

                    </b>
                </div>
            </div>
        </div>
    </body>
</html>
