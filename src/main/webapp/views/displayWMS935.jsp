<%@ include file="../fragments/taglibs.jsp" %>
<%@ include file="../fragments/imports.jsp" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.sql.*;" %>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <title>WMS</title>
        <!-- css :: vendors -->
        <jsp:include page="../fragments/css.jsp" />
        <!-- additional custom :: my-style -->
        <link rel="stylesheet" href="../resources/assets/styles/myStyles.css">

        <!-- js :: vendors -->
        <jsp:include page="../fragments/script.jsp" />
        <!-- additional custom :: my-script -->
        <script src="../resources/assets/scripts/myScripts.js" async></script>
        <script src="../resources/assets/scripts/toggleLoad.js" async></script>
        <style>
            input[type=text],
            select {
                width: 100%;
                padding: 5px 5px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }

            input[type=date],
            input[type=number] {
                /*width: 100%;*/
                padding: 5px 5px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }

            input[type=submit] {
                width: 100%;
                background-color: #00399b;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=submit]:hover {
                background-color: #3973d6;
            }

            input[type=button] {
                width: 100%;
                background-color: #ef604a;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=button]:hover {
                background-color: #e51e00;
            }
        </style>
        <script>
            var tableOptions = {
                "paging": false,
                "lengthMenu": [[-1, 10, 25, 50, 100], ["All", 10, 25, 50, 100]],
                "bSortClasses": false,
                "ordering": false,
                "info": true,
                "fixedColumns": true,
                "columnDefs": [
                    {"targets": [1, 2, 3, 4, 5, 6], "className": "text-right"}
                ]
            };
        </script>
        <style>
            .displayTable {
                width: 100%;
                border: 1px solid #ccc;
                border-radius: 4px;
            }

            .displayContain {
                border: 2px solid #ccc;
                border-radius: 5px;
                padding: 20px;
            }

            .headHD{
                background-color: #b1d1fa;
            }

            .headFoot{
                background-color: #e8eaff;
            }

            .gtot{
                background-color: #fffca3;
            }

            .gtoth{
                text-align: right;
            }
        </style>
        <script>
            $(document).ready(function () {
                $('#getDataBtn').click(function () {
                    var wh = $('#warehouse').val();
                    var ohDate1000 = $('#onhandDate1000').val();
                    var ohDate1050 = $('#onhandDate1050').val();

                    $.ajax({
                        url: "/TABLE2/ShowDataTables935?mode=getTOT930"
                                + "&wh=" + wh
                                + "&oh1000=" + ohDate1000
                                + "&oh1050=" + ohDate1050
                    }).done(function (result) {
                        console.log(result);

                        $('#tableList').html('');

                        if (result.length > 0) {
                            for (var i = 0; i < result.length; i++) {

                                var tableText = '';
                                tableText += '<table id="showTable' + i + '" class="display displayTable">';
                                tableText += '<thead>';
                                tableText += '<tr class="headHD">';
                                tableText += '<th>' + result[i].DATALIST[0].TWHSE + '</th>';
                                tableText += '<th colspan="2" style="text-align: center;">1000</th>';
                                tableText += '<th colspan="2" style="text-align: center;">1050</th>';
                                tableText += '<th colspan="2" style="text-align: center;">TOTAL</th>';
                                tableText += '</tr>';
                                tableText += '<tr class="headFoot">';
                                tableText += '<th>Mat Ctrl</th>';
                                tableText += '<th>SKU</th>';
                                tableText += '<th>Amount</th>';
                                tableText += '<th>SKU</th>';
                                tableText += '<th>Amount</th>';
                                tableText += '<th>SKU</th>';
                                tableText += '<th>Amount</th>';
                                tableText += '</tr>';
                                tableText += '</thead>';

                                tableText += '<tbody>';
                                for (var j = 0; j < result[i].DATALIST.length; j++) {
                                    tableText += '<tr>';
                                    tableText += '<td>&nbsp;&nbsp;' + result[i].DATALIST[j].TMTCL + '</td>';
                                    tableText += '<td>' + result[i].DATALIST[j].SKU_1000 + '&nbsp;&nbsp;</td>';
                                    tableText += '<td>' + result[i].DATALIST[j].AMT_1000 + '&nbsp;&nbsp;</td>';
                                    tableText += '<td>' + result[i].DATALIST[j].SKU_1050 + '&nbsp;&nbsp;</td>';
                                    tableText += '<td>' + result[i].DATALIST[j].AMT_1050 + '&nbsp;&nbsp;</td>';
                                    tableText += '<td>' + result[i].DATALIST[j].SKU_TOT + '&nbsp;&nbsp;</td>';
                                    tableText += '<td>' + result[i].DATALIST[j].AMT_TOT + '&nbsp;&nbsp;</td>';
                                    tableText += '</tr>';
                                }
                                tableText += '</tbody>';

                                tableText += '<tfoot class="headFoot">';
                                tableText += '<tr>';
                                tableText += '<th>TOTAL</th>';
                                tableText += '<th>' + result[i].DATALIST[0].T1000_SKU + '</th>';
                                tableText += '<th>' + result[i].DATALIST[0].T1000_AMT + '</th>';
                                tableText += '<th>' + result[i].DATALIST[0].T1050_SKU + '</th>';
                                tableText += '<th>' + result[i].DATALIST[0].T1050_AMT + '</th>';
                                tableText += '<th>' + result[i].DATALIST[0].TTOT_SKU + '</th>';
                                tableText += '<th>' + result[i].DATALIST[0].TTOT_AMT + '</th>';
                                tableText += '</tr>';
                                tableText += '</tfoot>';

                                if (i === result.length - 1) {
                                    tableText += '<tfoot class="gtot">';
                                    tableText += '<tr>';
                                    tableText += '<th>GRAND TOTAL</th>';
                                    tableText += '<th class="gtoth">' + result[i].DATALIST[0].GT1000_SKU + '</th>';
                                    tableText += '<th class="gtoth">' + result[i].DATALIST[0].GT1000_AMT + '</th>';
                                    tableText += '<th class="gtoth">' + result[i].DATALIST[0].GT1050_SKU + '</th>';
                                    tableText += '<th class="gtoth">' + result[i].DATALIST[0].GT1050_AMT + '</th>';
                                    tableText += '<th class="gtoth">' + result[i].DATALIST[0].GTTOT_SKU + '</th>';
                                    tableText += '<th class="gtoth">' + result[i].DATALIST[0].GTTOT_AMT + '</th>';
                                    tableText += '</tr>';
                                    tableText += '</tfoot>';
                                }

                                tableText += '</table>';

                                $('#tableList').append(tableText);
                                $('#showTable' + i).DataTable(tableOptions);

                            }
                        }


                    }).fail(function (jqXHR, textStatus, errorThrown) {
                        // needs to implement if it fails
                    });
                });

                $('#exportExcel').click(function () {
                    var wh = $('#warehouse').val();
                    var whText = $("#warehouse option:selected").html();
                    var ohDate1000 = $('#onhandDate1000').val();
                    var ohDate1000Text = $("#onhandDate1000 option:selected").html();
                    var ohDate1050 = $('#onhandDate1050').val();
                    var ohDate1050Text = $("#onhandDate1050 option:selected").html();

                    window.location.href = 'print?wh=' + wh + '&oh1000=' + ohDate1000 + '&oh1050=' + ohDate1050 + '&oh1000Text=' + ohDate1000Text + '&oh1050Text=' + ohDate1050Text + '&whText=' + whText;
                });
            });
        </script>

    </head>

    <body>
        <div id="wrapper">

            <!-- /#sidebar-wrapper -->
            <%@ include file="../fragments/sidebar.jsp" %>

            <!-- Page Content -->
            <!-- nav-head-custom -->
            <%@ include file="../fragments/nav_head.jsp" %>

            <div class="container-fluid">
                <!--<div id="wrapper-top">-->
                <%-- PART 2 --%>
                <!--                    <div class="row">
                                                <div class="col-lg-12">
                                                    <div id="set-height" style="height:415px;margin-top:0px;">
                                                        <div id="sidebar-wrapper-top" class="">
                                                            <b class="page-header" style="padding-left:5px;font-size:18px;">
                <%-- <hr style="margin:0px;margin-top:10px;margin-bottom:10px;"> --%>
            </b>
        </div>
    </div>
    </div>
    </div> -->
                <%-- PART 3 --%>
                <!--show Qdest table-->
                <form action="../resources/manual/WMS935.pdf" target="_blank">
                    <table width="100%">
                        <tr>
                            <td width="94%" align="left">
                            </td>
                            <td width="6%" align="right"><input type="submit" value="Manual" /></td>
                        </tr>
                    </table>
                </form>
                <div class="displayContain">
                    <div>
                        <div class="row">
                            <div class="col-sm-4"></div>
                            <div class="col-sm-4">
                                <table width="100%">
                                    <tr>
                                        <th>Warehouse</th>
                                        <th></th>
                                        <th>
                                            <select id="warehouse" style="width: 280px;">
                                                <c:forEach items="${whList}" var="x">
                                                    <option value="${x.uid}">${x.uid} : ${x.name}</option>
                                                </c:forEach>
                                            </select>
                                        </th>
                                    </tr>
                                    <tr>
                                        <th>Plant 1000</th>
                                        <th></th>
                                        <th>
                                            <select id="onhandDate1000" style="width: 200px;">
                                                <option value="none"></option>
                                                <c:forEach items="${onhandDate1000List}" var="x">
                                                    <option value="${x.SAPTRDT}">${x.ONHANDDATE}</option>
                                                </c:forEach>
                                            </select> (Onhand Date)
                                        </th>
                                    </tr>
                                    <tr>
                                        <th>Plant 1050</th>
                                        <th></th>
                                        <th>
                                            <select id="onhandDate1050" style="width: 200px;">
                                                <option value="none"></option>
                                                <c:forEach items="${onhandDate1050List}" var="x">
                                                    <option value="${x.SAPTRDT}">${x.ONHANDDATE}</option>
                                                </c:forEach>
                                            </select> (Onhand Date)
                                        </th>
                                    </tr>
                                    <tr>
                                        <th></th>
                                        <th></th>
                                        <th>
                                            <button id="getDataBtn" type="button" class="btn btn-primary"
                                                    style="width: 200px;">
                                                <i class="fa fa-download" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;&nbsp;
                                                Enter
                                            </button>
                                        </th>
                                    </tr>
                                </table>
                            </div>
                            <div class="col-sm-4"></div>
                        </div>
                    </div>
                    <hr>
                    <table width="100%">
                        <tr>
                            <td width="20%"></td>
                            <td width="60%">
                                <div id="tableList"></div>
                            </td>
                            <td width="20%" style="vertical-align: top;">
                                <button id="exportExcel" type="button" class="btn btn-success" style="width: 200px;">
                                    <i class="fa fa-file-excel-o" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;&nbsp;
                                    Print Excel
                                </button>
                            </td>
                        </tr>
                    </table>
                    <!--**********************-->
                    <!--End Part 3-->
                    <!--</div>  end #wrapper-top-->
                </div>
                <br>
            </div> <!-- end .container-fluid -->
        </div> <!-- end #wrapper -->
    </body>

</html>