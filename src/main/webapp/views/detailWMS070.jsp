<%@ include file="../fragments/taglibs.jsp" %>
<%@ include file="../fragments/imports.jsp" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.sql.*;" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>DETAIL</title>
        <!-- css :: vendors -->
        <jsp:include page = "../fragments/css.jsp" />
        <!-- additional custom :: my-style -->
        <link rel="stylesheet" href="../resources/assets/styles/myStyles.css">

        <!-- js :: vendors -->
        <jsp:include page = "../fragments/script.jsp" />
        <!-- additional custom :: my-script -->
        <script src="../resources/assets/scripts/myScripts.js" async></script>
        <script src="../resources/assets/scripts/toggleLoad.js" async></script>
        <script type="text/javascript"> /* add something code to your script */</script>
        <style>
            input[type=text], input[type=password], input[type=number], select {
                width: 100%;
                padding: 5px 5px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }

            input[type=submit] {
                width: 100%;
                background-color: #2bd14a;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=submit]:hover {
                background-color: #45a049;
            }

            input[type=button] {
                width: 100%;
                background-color: #ef604a;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=button]:hover {
                background-color: #e51e00;
            }

            button[name=ok] {
                width: 25%;
                background-color: #008cff;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            button[name=ok]:hover {
                background-color: #008cff;
            }

            b, th, td {
                color: #134596;
            }
        </style>
    </head>    
    <body >
        <div id="wrapper">

            <!-- /#sidebar-wrapper -->
            <%@ include file="../fragments/sidebar.jsp" %>

            <!-- Page Content -->
            <!-- nav-head-custom -->
            <%@ include file="../fragments/nav_head.jsp" %>

            <div class="container-fluid">
                <!--                <div id="wrapper-top" align="left">
                <%-- PART 2 --%>
                <div class="row">
                    <div class="col-lg-12">
                        <div id="set-height" style="height:415px;margin-top:0px;">
                            <div id="sidebar-wrapper-top" class="">
                                <b class="page-header" style="padding-left:5px;font-size:18px;">
                <%-- <hr style="margin:0px;margin-top:10px;margin-bottom:10px;"> --%>
            </b>
        </div>
    </div>
</div>
</div> -->

                <%-- PART 3 --%>
                <br>
                <form>
                    <table frame="box" width="100%" bordercolor="#e0e0e0">
                        <tr>
                            <th height="50" bgcolor="#f8f8f8"></th>
                            <th height="50" bgcolor="#f8f8f8"><b>คลังสินค้า</b></th>
                            <td height="50" bgcolor="#f8f8f8">${wh} : ${whn}</td>
                            <th height="50" bgcolor="#f8f8f8"></th>
                            <th height="50" bgcolor="#f8f8f8"></th>
                            <th height="50" bgcolor="#f8f8f8"></th>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td height="30" style="border-right: 1px solid gainsboro;"></td>
                        </tr>
                        <tr>
                            <td width="5%" align="left">
                            <td width="20%" align="left"><b>Material Code</b></td>
                            <td width="25%" align="left">${qcod} : ${sdesc}</td>
                            <td width="15%" align="left"><b>Plant</b></td>
                            <td width="25%" align="left">${qplt}</td>
                            <td width="10%" align="left">
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td height="15" style="border-right: 1px solid gainsboro;"></td>
                        </tr>
                        <tr>
                            <td width="5%" align="left">
                            <td width="20%" align="left"><b>Trans Date & Time</b></td>
                            <td width="25%" align="left">${qtdt}</td>
                            <td width="15%" align="left"><b>Value Type</b></td>
                            <td width="25%" align="left">${qval}</td>
                            <td width="10%" align="left">
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td height="15" style="border-right: 1px solid gainsboro;"></td>
                        </tr>
                        <tr>
                            <td width="5%" align="left">
                            <td width="20%" align="left"><b>ID</b></td>
                            <td width="25%" align="left">${qid}</td>
                            <td width="15%" align="left"><b>Storage</b></td>
                            <td width="25%" align="left">${qstrg}</td>
                            <td width="10%" align="left">
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td height="15" style="border-right: 1px solid gainsboro;"></td>
                        </tr>
                        <tr>
                            <td width="5%" align="left">
                            <td width="20%" align="left"><b>Doc no.</b></td>
                            <td width="25%" align="left">${qdn}</td>
                            <td width="15%" align="left"><b>Movement Type</b></td>
                            <td width="25%" align="left">${qmvt}</td>
                            <td width="10%" align="left">
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td height="15" style="border-right: 1px solid gainsboro;"></td>
                        </tr>
                        <tr>
                            <td width="5%" align="left">
                            <td width="20%" align="left"><b>SAP Doc no.</b></td>
                            <td width="25%" align="left">${qsn}</td>
                            <td width="15%" align="left"><b>Transaction Type</b></td>
                            <td width="25%" align="left">${qtrt}</td>
                            <td width="10%" align="left">
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td height="15" style="border-right: 1px solid gainsboro;"></td>
                        </tr>
                        <tr>
                            <td width="5%" align="left">
                            <td width="20%" align="left"><b>Quantity</b></td>
                            <td width="25%" align="left">${qqty}&nbsp;&nbsp;${qbun}</td>
                            <td width="15%" align="left"><b>Material Control</b></td>
                            <td width="25%" align="left">${mc} : ${mcn}</td>
                            <td width="10%" align="left">
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td height="15" style="border-right: 1px solid gainsboro;"></td>
                        </tr>
                        <tr>
                            <td width="5%" align="left">
                            <td width="20%" align="left"><b>Piece/Roll</b></td>
                            <td width="25%" align="left">${qroll}</td>
                            <td width="15%" align="left"><b>Material Group</b></td>
                            <td width="25%" align="left">${mg} : ${mgn}</td>
                            <td width="10%" align="left">
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td height="30" style="border-right: 1px solid gainsboro;"></td>
                        </tr>
                        <tr>
                            <td width="5%" align="left" style="border-top: 1px solid gainsboro;">
                            <td width="20%" align="left" style="border-top: 1px solid gainsboro;"><b>Create Date & Time</b></td>
                            <td width="25%" align="left" style="border-top: 1px solid gainsboro;">${qcdt}</td>
                            <td width="15%" align="left" style="border-top: 1px solid gainsboro;"><b>User ID</b></td>
                            <td width="25%" align="left" style="border-top: 1px solid gainsboro;">${quser}</td>
                            <td width="10%" align="left" style="border-top: 1px solid gainsboro;"><input style="width: 100px;" type="button" value="Back" onclick="window.history.back()"/></td>
                        </tr>
                    </table>
                </form>
                <!--End Part 3-->
                <!--</div>  end #wrapper-top -->
            </div> <!-- end .container-fluid -->
        </div> <!-- end #wrapper -->


    </body>

</html>