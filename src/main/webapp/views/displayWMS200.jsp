<%@ include file="../fragments/taglibs.jsp" %>
<%@ include file="../fragments/imports.jsp" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.sql.*;" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>WMS</title>
        <!-- css :: vendors -->
        <jsp:include page = "../fragments/css.jsp" />
        <!-- additional custom :: my-style -->
        <link rel="stylesheet" href="../resources/assets/styles/myStyles.css">

        <!-- js :: vendors -->
        <jsp:include page = "../fragments/script.jsp" />
        <!-- additional custom :: my-script -->
        <script src="../resources/assets/scripts/myScripts.js" async></script>
        <script src="../resources/assets/scripts/toggleLoad.js" async></script> 

        <style>
            input[type=text], select{
                width: 100%;
                padding: 5px 5px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }

            input[type=date], input[type=number]{
                /*width: 100%;*/
                padding: 5px 5px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }

            input[type=submit], button {
                width: 100%;
                background-color: #00399b;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=submit]:hover, button:hover {
                background-color: #3973d6;
            }

            input[type=button] {
                width: 100%;
                background-color: #ef604a;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=button]:hover {
                background-color: #e51e00;
            }
        </style>
        <script>
            $(document).ready(function () {
                $('#showTable').DataTable({
//                    fixedHeader: true,
//                    "paging": false,
                    "lengthMenu": [[10, 25, 50, 100], [10, 25, 50, 100]],
                    "bSortClasses": false
                });

                // Setup - add a text input to each footer cell
                $('#showTable tfoot th').each(function () {
                    var title = $(this).text();
                    $(this).html('<input type="text" />');
                });

                // DataTable
                var table = $('#showTable').DataTable();

                // Apply the search
                table.columns().every(function () {
                    var that = this;
                    $('input', this.footer()).on('keyup change', function () {
                        if (that.search() !== this.value) {
                            that.search(this.value).draw();
                        }
                    });
                });

                checkStatus();

                setInterval(function () {
                    CheckValueFileUpload();
                }, 1000);

                var options = {

                    beforeSend: function () {
                        $('#cancleBtn').hide();
                        $("#percent").html("0 kb");
                        $('#getFile').loader('show', '<i style="font-size:100px;" class="fa fa-refresh fa-spin"></i>');
                    },

                    uploadProgress: function (event, position, total, percentComplete) {
                        temp = currencyFormat(Math.round(total / 1024));
                        $("#percent").html(percentComplete + ' %');
                    },

                    success: function () {
                        $("#percent").html('');
                        $("#total").html(temp + " kb");
                    },

                    complete: function (response) {
                        if (parseInt(response.responseText) == 1) {
                            alertify.alert('<b style="color:red;font-size:18px;"> The data has been changed ! </b>',
                                    function () {
                                        window.open("../MSS003/DP");
                                    }
                            );
                        }
                        $('#getFile').loader('hide');
                        $('#custom-body').css("background-color", "#EFFCE3");
                    },

                    error: function () {

                    }

                };

            }
            );
        </script>
        <style type="text/css">
            #image {margin-right: 20px;margin-top:0px;margin-left: 18px;}
            #dataPreview {margin-right: 50px;margin-left: 18px;}
            #total {font-size: 14px;}
            #cancleBtn {position:absolute;top:-20px;right:-20px;}
            #image, #dataPreview, #cancleBtn, #percent, #total { display:inline; }
        </style>
        <script>
            var sidebarVisible = localStorage.getItem('wrapper');
            var checkIMenu = $("#i-menu").html();
            var temp = "";

            function FakeFileUploadClick() {
                $('#custom-body').css("background-color", "#F7F7F7");
                $('#uploadme').val('');
                $('#cancleBtn').show();
                $("#total").html('');
                $('#noFile').show();
                $("#getFile").hide();
                $('#uploadme').click();
            }

            function CheckValueFileUpload() {
                var dataFileUpload = $('#uploadme').val();
                if (dataFileUpload.length != 0) {
                    var fileName = dataFileUpload.split("\\");
                    $('#noFile').hide();
                    $('#getFile').show();
                    // $('#image').html('<img src="../images/icons/file_upload.png" width="12" >');
                    $('#cancleBtn').html('<button onclick="ClearValueFileUpload()" class="btn btn-outline btn-danger" style="padding:0px;margin:0px;width:25px;"><i style="padding:0px;margin:0px;" class="fa fa-times icon-size-sm"></b></button>');
                    $('#dataPreview').html(fileName[fileName.length - 1]);
                } else {
                    $('#noFile').show();
                    $("#getFile").hide();
                }
            }

            function ClearValueFileUpload() {
                $('#uploadme').val('');
            }

            function currencyFormat(num) {
                return num.toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
            }

            function searchCustomer() {

                var POFG = $('#POFG').val();

                if (POFG == "") {
                    alertify.error("Please fill out customer form.");
                } else {
                    window.location = "display?a=list&POFG=" + POFG;
                }

            }

            function addGroup() {
                var checkData = "${fn:length(HDList)}";
                var gCount = $('#gCount').val();
                var gYear = $('#gYear').val().substring(2, 4);
                var POFG = $('#POFG').val();
                var WHS = $('#WHS').val();
                var message = POFG + gCount + "/" + gYear;
                if (parseInt(checkData) == 0) {
                    alertify.error("Customer data not found.");
                } else if (gCount == "" | gYear == "") {
                    alertify.error("Please fill out group set form.");
                } else {
                    alertify.confirm("<b class='head-custom' style='font-size:18px;'> Do you confirm this Group Set no.? </b> </br> </br> <b> Group Set no. : " + message + "</b> </br>", function (e) {
                        if (e) {
                            //after clicking OK
                            window.location = "display?a=update&POFG=" + POFG + "&GRPNO=" + message + "&WHS=" + WHS;
                        } else {
                            //after clicking Cancel
                        }
                    });
                }
            }

            function checkStatus() {
                if ("${fn:length(statusRe)}" != 0) {
                    alertify.alert("${statusRe}");
                }
            }
        </script>

    </head>    
    <body onload="document.getElementById('userid').value = sessionStorage.getItem('uid');">
        <div id="wrapper">

            <!-- /#sidebar-wrapper -->
            <%@ include file="../fragments/sidebar.jsp" %>

            <!-- Page Content -->
            <!-- nav-head-custom -->
            <%@ include file="../fragments/nav_head.jsp" %>

            <div class="container-fluid">
                <!--<div id="wrapper-top">-->
                <%-- PART 2 --%>
                <!--                    <div class="row">
                                        <div class="col-lg-12">
                                            <div id="set-height" style="height:415px;margin-top:0px;">
                                                <div id="sidebar-wrapper-top" class="">
                                                    <b class="page-header" style="padding-left:5px;font-size:18px;">
                <%-- <hr style="margin:0px;margin-top:10px;margin-bottom:10px;"> --%>
            </b>
        </div>
    </div>
    </div>
    </div> -->
                <%-- PART 3 --%>
                <!--show Qdest table-->
                <form action="../resources/manual/WMS200.pdf" target="_blank">
                    <table width="100%">
                        <tr id="dont">
                            <td width="94%" align="left"></td>
                            <td width="6%" align="right"><input type="submit" value="Manual" /></td>
                        </tr>
                    </table>
                </form>
                <div class="row">
                    <div class="col-lg-12">
                        <h4 style="margin-top:0;padding-left:10px;padding-right:10px;margin-bottom:0px;">
                            <table  class="pull-right" style="margin-top:3px;">
                                <tr>
                                    <td style="padding-right:20px;font-size:14px;color:red;">
                                        <c:out value="${maxGroup}" escapeXml="false" />
                                    </td>
                                    <td>
                                        <b class="head-custom"> Customer : </b>
                                    </td>
                                    <td width="120px;">
                                        <input id="POFG" value="${POFG}" class="form-control" type="text" style="width:90px;margin-left:5px;font-weight: bold;" onkeyup="javascript:this.value = this.value.toUpperCase()">
                                    </td>
                                    <td>
                                        <button onclick="searchCustomer()" style="min-width:40px;max-width:140px;width:15%" class="btn btn-outline btn-primary">
                                            <b><i class="fa fa-search icon-size-sm"></i></b>
                                        </button>
                                    </td>
                                </tr>
                            </table>
                            <table style="float:left;">
                                <tr>
                                    <td width="110px;">
                                        <form id="UploadForm" action="display" method="post" enctype="multipart/form-data" accept-charset="UTF-8">
                                            <input type="file" name="myfile" style="visibility:hidden;width:1px;height:1px;" id="uploadme" /> 
                                            <button type="button" style="min-width:40px;max-width:140px;width:15%;margin-right:1%" onclick="FakeFileUploadClick();" class="btn btn-outline btn-primary">
                                                <b><i class="fa fa-sign-in icon-size-sm"></i></b>
                                            </button>
                                            <button style="min-width:40px;max-width:140px;width:15%" class="btn btn-outline btn-primary" type="submit">
                                                <b><i class="fa fa-upload icon-size-sm"></i></b>
                                            </button>
                                        </form>
                                    </td>
                                    <td>
                                        <div id="custom-body" class="custom-body">
                                            <div id="noFile">
                                                <p style="color:#B4B4B4;text-align:left;font-size: 17px;padding-left:18px;">
                                                    <b> No file chosen </b>
                                                </p>
                                            </div>
                                            <div id="getFile" style="position:relative">
                                                <p style="color:#424242;text-align:center;font-size: 17px;position:absolute">
                                                    <b>
                                                        <!-- <div id="image" ></div> -->
                                                        <div id="dataPreview"></div>
                                                        <div id="cancleBtn"></div>
                                                        <div id="percent"></div>
                                                        <div id="total"></div>
                                                    </b>
                                                </p>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </h4>
                    </div>
                </div> 
                <hr style="margin:0px;margin-top:10px;margin-bottom:10px;">
                <div class="row">
                    <div class="col-lg-12">

                        <table class="pull-right" style="position:absolute;top:5.5px;right:32px;font-size:14px;">
                            <tr>
                                <td>
                                    <b style="color:white;">Warehouse : </b>
                                </td>
                                <td>
                                    <select name="WHS" id="WHS" style="font-weight: bold;width:200px;margin-left:5px;margin-right:5px;">
                                        <option value=""></option>
                                        <c:forEach items="${WHList}" var="p" varStatus="i">
                                            <option value="${p.code}">${p.code} : ${p.name}</option>
                                        </c:forEach>
                                    </select>
                                </td>
                                <td>
                                    <b style="color:white;">Group Set no. :  </b>
                                </td>
                                <td>
                                    <input id="gCount" class="form-control input-sm" type="number" style="font-weight: bold;width:60px;margin-left:5px;margin-right:5px;">
                                </td>
                                <td><b style="color:white;">/ Year :</b></td>
                                <td>
                                    <div id="inputDateDiv" style="position:relative;"> 
                                        <input id="gYear" class="form-control input-sm" type="text" style="font-weight: bold;width:70px;margin-left:5px;margin-right:5px;">
                                    </div>
                                    <script type="text/javascript">
                                        $(function () {
                                            $('#gYear').datetimepicker({
                                                format: 'YYYY',
                                            });
                                        });
                                    </script>
                                </td>
                                <td>
                                    <button onclick="addGroup()" style="margin:0px;margin-left:10px;" class="btn btn-default btn-outline btn-sm custom-icon-w-b">
                                        <b><i class="fa fa-refresh icon-size-sm"></i></b>
                                    </button>
                                </td>
                            </tr>
                        </table>

                        <div class="panel panel-primary" style="min-height: 485px;">
                            <div class="panel-heading" style=" height: 50px;">
                                <h4 style="margin:0px;">File Upload</h4>
                            </div>
                            <!-- /.panel-heading -->
                            <div class="panel-body">
                                <div class="dataTable_wrapper">
                                    <table id="showTable" class="display" style="width:100%; border: 1px solid #ccc; border-radius: 4px;">
                                        <thead>
                                            <tr id="dont">
                                                <th>Sale Order</th>
                                                <th>Doc Date</th>
                                                <th>PO Number</th>
                                                <th>Comp's PO</th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr id="dont">
                                                <th></th>
                                                <th></th>
                                                <th></th>
                                                <th></th>
                                            </tr>
                                        </tfoot>
                                        <tbody>
                                            <c:forEach items="${HDList}" var="x">
                                                <tr>
                                                    <td>${x.VBELN}</td>
                                                    <td>${x.AUDAT}</td>
                                                    <td>${x.BSTKD}</td>
                                                    <td>${x.POFG}</td>
                                                </tr>                          
                                            </c:forEach>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> 
                <br>
            </div> <!-- end .container-fluid -->
        </div> <!-- end #wrapper -->
    </body>
</html>