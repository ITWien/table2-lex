<%@ include file="../fragments/taglibs.jsp" %>
<%@ include file="../fragments/imports.jsp" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.sql.*;" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>WMS</title>
        <!-- css :: vendors -->
        <jsp:include page = "../fragments/css.jsp" />
        <!-- additional custom :: my-style -->
        <link rel="stylesheet" href="../resources/assets/styles/myStyles.css">

        <!-- js :: vendors -->
        <jsp:include page = "../fragments/script.jsp" />
        <!-- additional custom :: my-script -->
        <script src="../resources/assets/scripts/myScripts.js" async></script>
        <script src="../resources/assets/scripts/toggleLoad.js" async></script> 
        <style>
            input[type=text], select{
                width: 100%;
                padding: 5px 5px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }

            input[type=date], input[type=number]{
                /*width: 100%;*/
                padding: 5px 5px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }

            input[type=submit] {
                width: 100%;
                background-color: #00399b;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=submit]:hover {
                background-color: #3973d6;
            }

            input[type=button] {
                width: 100%;
                background-color: #ef604a;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=button]:hover {
                background-color: #e51e00;
            }
        </style>
        <script>
            function showWH(dest, close) {
                var modal = document.getElementById(dest);
                modal.style.display = "block";
                var span = document.getElementById(close);
                span.onclick = function () {
                    modal.style.display = "none";
                };
                window.onclick = function (event) {
                    if (event.target === modal) {
                        modal.style.display = "none";
                    }
                };
            }
        </script>
        <style>
            body {font-family: Arial, Helvetica, sans-serif;}

            /* The Modal (background) */
            .modal {
                display: none; /* Hidden by default */
                position: fixed; /* Stay in place */
                z-index: 1; /* Sit on top */
                padding-top: 100px; /* Location of the box */
                left: 0;
                top: 0;
                width: 100%; /* Full width */
                height: 100%; /* Full height */
                overflow: auto; /* Enable scroll if needed */
                background-color: rgb(0,0,0); /* Fallback color */
                background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
            }

            /* Modal Content */
            .modal-content {
                background-color: #fefefe;
                margin: auto;
                padding: 20px;
                border: 1px solid #888;
                width: fit-content;
                height: 320px;
            }

            /* The Close Button */
            .close {
                color: #aaaaaa;
                float: right;
                font-size: 28px;
                font-weight: bold;
            }

            .close:hover,
            .close:focus {
                color: #000;
                text-decoration: none;
                cursor: pointer;
            }
        </style>
        <script>
            $(document).ready(function () {
                $('#showTable').DataTable({
                    "lengthMenu": [[-1, 10, 25, 50, 100], ["All", 10, 25, 50, 100]],
                    "bSortClasses": false,
                    "columnDefs": [
                        {orderable: false, targets: [16]},
                        {"width": "9%", "targets": 10},
                        {"width": "15%", "targets": 15}
                    ]
                });

                // Setup - add a text input to each footer cell
                $('#showTable tfoot th').not(":eq(16)").each(function () {
                    var title = $(this).text();
                    $(this).html('<input type="text" />');
                });

                // DataTable
                var table = $('#showTable').DataTable();

                // Apply the search
                table.columns().every(function () {
                    var that = this;
                    $('input', this.footer()).on('keyup change', function () {
                        if (that.search() !== this.value) {
                            that
                                    .search(this.value)
                                    .draw();
                        }
                    });
                });
            });
        </script>
        <script>
            function checkAll(ele) {
                var checkboxes = document.getElementsByTagName('input');
                if (ele.checked) {
                    for (var i = 0; i < checkboxes.length; i++) {
                        if (checkboxes[i].name == 'selectCk') {
                            checkboxes[i].checked = true;
                        }
                    }
                } else {
                    for (var i = 0; i < checkboxes.length; i++) {
                        console.log(i)
                        if (checkboxes[i].name == 'selectCk') {
                            checkboxes[i].checked = false;
                        }
                    }
                }
            }
        </script>
        <script>
            SC01 = function () {
                var checkboxes = document.getElementsByTagName('input');
                var nodataSelect = false;

                for (var i = 0; i < checkboxes.length; i++) {
                    if (checkboxes[i].type === 'checkbox' && checkboxes[i].name !== 'selectAll') {
                        if (checkboxes[i].checked === true) {
                            nodataSelect = true;
                        }
                    }
                }

                if (nodataSelect) {
                    showWH('myModal', 'span');
                } else {
                    document.getElementById('myModal-4').style.display = 'block';
                }
            };

            SC02 = function () {
                var checkboxes = document.getElementsByTagName('input');
                var nodataSelect = false;
                var isNotStatus9 = false;

                for (var i = 0; i < checkboxes.length; i++) {
                    if (checkboxes[i].type === 'checkbox' && checkboxes[i].name !== 'selectAll') {
                        if (checkboxes[i].checked === true) {
                            nodataSelect = true;
                            if (checkboxes[i].value.toString().split("-")[5] !== "6") {
                                isNotStatus9 = true;
                            }
                        }
                    }
                }

                if (nodataSelect) {
                    if (isNotStatus9) {
                        document.getElementById('myModal-5').style.display = 'block';
                    } else {
                        showWH('myModal-1', 'span-1');
                    }
                } else {
                    document.getElementById('myModal-4').style.display = 'block';
                }
            };

            submitForms1 = function () {
                var checkboxes = document.getElementsByTagName('input');
                var nodataSelect = false;

                for (var i = 0; i < checkboxes.length; i++) {
                    if (checkboxes[i].type === 'checkbox' && checkboxes[i].name !== 'selectAll') {
                        if (checkboxes[i].checked === true) {
                            nodataSelect = true;
                        }
                    }
                }

                if (nodataSelect) {
                    document.getElementById("detail").action = "receive";
                    document.getElementById("detail").submit();
                } else {
                    document.getElementById('myModal-4').style.display = 'block';
                }
            };

            submitForms2 = function () {
                document.getElementById("detail").action = "return";
                document.getElementById("detail").submit();
            };

            submitForms3 = function () {
                var ship = document.getElementById("ship");
                var datecheck = document.getElementById("datecheck");
                if (ship.value === "") {
                    ship.style.cssText = "background-color: #ffdbdb;";
                    datecheck.innerHTML = "Please fill Transportation Round!";
                } else {
                    ship.style.cssText = "background-color: white;";
                    datecheck.innerHTML = "";
                    document.getElementById("detail").action = "round";
                    document.getElementById("detail").submit();
                }
            };

            submitForms4 = function () {
                var checkboxes = document.getElementsByTagName('input');
                var nodataSelect = false;
                var isNotStatus9 = false;

                for (var i = 0; i < checkboxes.length; i++) {
                    if (checkboxes[i].type === 'checkbox' && checkboxes[i].name !== 'selectAll') {
                        if (checkboxes[i].checked === true) {
                            nodataSelect = true;
                            if (checkboxes[i].value.toString().split("-")[5] !== "6") {
                                isNotStatus9 = true;
                            }
                        }
                    }
                }

                if (nodataSelect) {
                    if (isNotStatus9) {
                        document.getElementById('myModal-6').style.display = 'block';
                    } else {
                        var wh = document.getElementById("wh");
                        var dest = document.getElementById("dest");
                        var shipDate = document.getElementById("shipDate");

                        if (wh.value === "" || dest.value === "" || shipDate.value === "") {
                            document.getElementById('myModal-2').style.display = 'block';
                        } else {
                            document.getElementById("detail").action = "print";
                            document.getElementById("detail").target = "_blank";
                            document.getElementById("detail").submit();
                        }
                    }
                } else {
                    document.getElementById('myModal-4').style.display = 'block';
                }
            };
        </script>
        <SCRIPT language="javascript">
            function QR() {
                var qr = document.getElementById("qr");
                qr = qr.value.toString().trim();
                qr = qr.split("|");
                var checkboxes = document.getElementsByTagName('input');
                for (var i = 0; i < checkboxes.length; i++) {
                    if (checkboxes[i].id === 'selectCk-' + qr[0]) {
                        if (checkboxes[i].checked === true) {
                            checkboxes[i].checked = false;
                        } else if (checkboxes[i].checked === false) {
                            checkboxes[i].checked = true;
                        }
                    }
                }

                document.getElementById("qr").value = "";
            }
        </SCRIPT>
    </head>    
    <body onload="document.getElementById('userid').value = sessionStorage.getItem('uid');">
        <div id="wrapper">

            <!-- /#sidebar-wrapper -->
            <%@ include file="../fragments/sidebar.jsp" %>

            <!-- Page Content -->
            <!-- nav-head-custom -->
            <%@ include file="../fragments/nav_head.jsp" %>

            <div class="container-fluid">
                <!--<div id="wrapper-top">-->
                <%-- PART 2 --%>
                <!--                    <div class="row">
                                        <div class="col-lg-12">
                                            <div id="set-height" style="height:415px;margin-top:0px;">
                                                <div id="sidebar-wrapper-top" class="">
                                                    <b class="page-header" style="padding-left:5px;font-size:18px;">
                <%-- <hr style="margin:0px;margin-top:10px;margin-bottom:10px;"> --%>
            </b>
        </div>
    </div>
</div>
</div> -->
                <%-- PART 3 --%>
                <!--show Qdest table-->
                <form action="../resources/manual/WMS360.pdf" target="_blank">
                    <table width="100%">
                        <tr>
                            <td width="94%"></td>
                            <td width="6%" align="right"><input type="submit" value="Manual" /></td>
                        </tr>
                    </table>
                </form>
                <div style="border: 2px solid #ccc; border-radius: 5px; padding: 20px;">
                    <form id="frm" name="frm" action="display" method="post">
                        <table width="100%">
                            <tr>
                                <td width="25%"><b style="color: #00399b;">QR Code : </b>
                                    <input class="form-control" type="text" name="myInput" id="myInput" style="width:75%;" autofocus>
                                    <script>
                                        var input = document.getElementById("myInput");
                                        input.addEventListener("keyup", function (event) {
                                            if (event.keyCode === 13) {
                                                event.preventDefault();

                                                if (input.value.toString().split("|").length === 5) {
                                                    document.getElementById("dest").value = input.value.toString().split("|")[0];
                                                    document.getElementById("ship").value = input.value.toString().split("|")[2];
                                                    document.getElementById("wh").value = input.value.toString().split("|")[3];
                                                    document.getElementById("round").value = input.value.toString().split("|")[4];
                                                    document.getElementById("frm").submit();
                                                } else {
                                                    document.getElementById("myInput").value = "";
                                                    document.getElementById('myModal-7').style.display = 'block';
                                                }

                                            }
                                        });
                                    </script>
                                    <input type="text" style="width: 0px; border-color: white;">
                                </td>                                
                                <td width="20%" align="right"><b style="color: #00399b;">Destination : </b>
                                    <input style="width:65%;" type="text" name="dest" id="dest" value="${dest} : ${destn}">
                                </td>
                                <td width="22%" align="right"><b style="color: #00399b;">Shipment Date : </b>
                                    <input style="width:60%;" type="text" name="ship" id="ship" value="${shipDate}">
                                </td>
                                <td> 
                                </td>
                                <td width="7%">
                                    <a class="btn btn btn-outline btn-success" id="transported" name="transported" onclick="submitForms1();">
                                        <i class="fa fa-check-circle-o" style="font-size:20px;"></i> Received</a>
                                </td>
                                <!--                                <td width="7%">
                                                                    <a class="btn btn btn-outline btn-danger" id="cancelled" name="cancelled" onclick="SC01();">
                                                                        <i class="fa fa-times-circle-o" style="font-size:20px;"></i> Returned</a>
                                                                </td>-->
                            </tr>
                        </table>
                        <table width="100%">
                            <tr>
                                <td width="25%">
                                </td>
                                <td width="20%" align="right"><b style="color: #00399b;">Warehouse : </b>
                                    <input style="width:65%;" type="text" name="wh" id="wh" value="${wh} : ${whn}">
                                </td>
                                <td width="22%" align="right"><b style="color: #00399b;">Round : </b>
                                    <input style="width:60%;" type="text" name="round" id="round" value="${round}">
                                </td>
                                <td>
                                </td>
                                <td width="7%">
                                </td>
                                <td width="7%">
                                </td>
                            </tr>
                        </table>
                    </form>
                    <!-- The Modal -->
                    <div id="myModal" class="modal">
                        <!-- Modal content -->
                        <div class="modal-content" style=" height: 200px; width: 500px;">
                            <span id="span" class="close">&times;</span>
                            <p><b><font size="4"> </font></b></p>
                            <br>
                            <table width="100%">
                                <tr>
                                    <td align="center">
                                        <p><b><font size="4">Confirm to Return </font></b></p>
                                        <br>
                                        <a class="btn btn btn-outline btn-danger" onclick="document.getElementById('myModal').style.display = 'none';">
                                            <i class="fa fa-times-circle-o" style="font-size:20px;"></i> Cancel</a>  
                                        &nbsp;
                                        <a class="btn btn btn-outline btn-success" onclick="submitForms2();">
                                            <i class="fa fa-check-circle-o" style="font-size:20px;"></i> Confirm</a>                      
                                    </td>
                                </tr>
                            </table>
                        </div>

                    </div>
                    <div id="myModal-1" class="modal">
                        <!-- Modal content -->
                        <div class="modal-content">
                            <span id="span-1" class="close">&times;</span>
                            <p><b><font size="4"></font></b></p>
                            <br>
                            <table width="100%">
                                <tr>
                                    <td align="center">
                                        <br>
                                        <br>
                                        <p id="datecheck" style="color: red;"></p>
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <b><font size="4">Transportation Round : </font></b>
                                        <input type="number" name="ship" id="ship" onchange="document.getElementById('round').value = this.value;">
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <br>
                                        <br>
                                        <a class="btn btn btn-outline btn-danger" onclick="document.getElementById('myModal-1').style.display = 'none';">
                                            <i class="fa fa-times-circle-o" style="font-size:20px;"></i> Cancel</a>  
                                        &nbsp;
                                        <a class="btn btn btn-outline btn-success" onclick="submitForms3();">
                                            <i class="fa fa-check-circle-o" style="font-size:20px;"></i> Confirm</a>                      
                                    </td>
                                </tr>
                            </table>
                        </div>

                    </div>
                    <div id="myModal-2" class="modal">
                        <!-- Modal content -->
                        <div class="modal-content" style=" height: 240px;">
                            <span id="span-2" class="close" onclick="document.getElementById('myModal-2').style.display = 'none';">&times;</span>
                            <p><b><font size="4"></font></b></p>
                            <br>
                            <table width="100%">
                                <tr>
                                    <td align="center">
                                        <br>
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <b style="color: #00399b;"><font size="5">Please select Warehouse, Destination and Shipment Date !</font></b>
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <br>
                                        <br>
                                        <br>
                                        <a style=" width: 100px;" class="btn btn-primary" onclick="document.getElementById('myModal-2').style.display = 'none';">
                                            OK 
                                        </a>                     
                                    </td>
                                </tr>
                            </table>
                        </div>

                    </div>
                    <div id="myModal-4" class="modal">
                        <!-- Modal content -->
                        <div class="modal-content" style=" height: 200px; width: 500px;">
                            <span id="span-4" class="close" onclick="document.getElementById('myModal-4').style.display = 'none';">&times;</span>
                            <p><b><font size="4"></font></b></p>
                            <table width="100%">
                                <tr>
                                    <td align="center">
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <b style="color: #00399b;"><font size="5">Please select the data !</font></b>
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <br>
                                        <br>
                                        <br>
                                        <a style=" width: 100px;" class="btn btn btn-outline btn-danger" onclick="document.getElementById('myModal-4').style.display = 'none';">
                                            OK
                                        </a>                 
                                    </td>
                                </tr>
                            </table>
                        </div>

                    </div>
                    <div id="myModal-5" class="modal">
                        <!-- Modal content -->
                        <div class="modal-content" style=" height: 220px; width: 800px;">
                            <span id="span-5" class="close" onclick="document.getElementById('myModal-5').style.display = 'none';">&times;</span>
                            <p><b><font size="4"></font></b></p>
                            <table width="100%">
                                <tr>
                                    <td align="center">
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <b style="color: #00399b;"><font size="5">Only status is 6 : Transported that can generate Round !</font></b>
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <br>
                                        <br>
                                        <br>
                                        <a style=" width: 100px;" class="btn btn btn-outline btn-danger" onclick="document.getElementById('myModal-5').style.display = 'none';">
                                            OK
                                        </a>                 
                                    </td>
                                </tr>
                            </table>
                        </div>

                    </div>
                    <div id="myModal-6" class="modal">
                        <!-- Modal content -->
                        <div class="modal-content" style=" height: 220px; width: 800px;">
                            <span id="span-6" class="close" onclick="document.getElementById('myModal-6').style.display = 'none';">&times;</span>
                            <p><b><font size="4"></font></b></p>
                            <table width="100%">
                                <tr>
                                    <td align="center">
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <b style="color: #00399b;"><font size="5">Only status is 6 : Transported that can Print !</font></b>
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <br>
                                        <br>
                                        <br>
                                        <a style=" width: 100px;" class="btn btn btn-outline btn-danger" onclick="document.getElementById('myModal-6').style.display = 'none';">
                                            OK
                                        </a>                 
                                    </td>
                                </tr>
                            </table>
                        </div>

                    </div>
                    <div id="myModal-7" class="modal">
                        <!-- Modal content -->
                        <div class="modal-content" style=" height: 200px; width: 500px;">
                            <span id="span-7" class="close" onclick="window.location.href = 'display'">&times;</span>
                            <p><b><font size="4"></font></b></p>
                            <table width="100%">
                                <tr>
                                    <td align="center">
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <b style="color: #00399b;"><font size="5">Invalid QR Code !</font></b>
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <br>
                                        <br>
                                        <br>
                                        <a style=" width: 100px;" class="btn btn btn-outline btn-danger" onclick="window.location.href = 'display'">
                                            OK
                                        </a>                 
                                    </td>
                                </tr>
                            </table>
                        </div>

                    </div>
                    <form id="detail" method="post">
                        <input type="hidden" id="userid" name="userid">
                        <input type="hidden" name="wh" value="${wh}">
                        <input type="hidden" name="dest" value="${dest}">
                        <input type="hidden" name="shipDate" value="${shipDate}">
                        <input type="hidden" name="round" value="${round}">
                        <table id="showTable" class="display" style="width:100%; border: 1px solid #ccc; border-radius: 4px;">
                            <thead>
                                <tr>
                                    <th colspan="6" style="text-align: center; border-bottom: none;"></th>
                                    <th colspan="3" style="text-align: center; border-bottom-color: #ccc;">Package</th>
                                </tr>
                                <tr>
                                    <th style="text-align: center;">Group QR ID</th>
                                    <th style="text-align: center;">Material Code</th>
                                    <th style="text-align: center;">Description</th>
                                    <th style="text-align: center;">ID</th>
                                    <th style="text-align: center;">Quantity</th>
                                    <th style="text-align: center;">U/M</th>
                                    <th style="text-align: center;">BAG</th>
                                    <th style="text-align: center;">ROLL</th>
                                    <th style="text-align: center;">BOX</th>
                                    <th style="text-align: center;">M<sup>3</sup></th>
                                    <th style="text-align: center;">Status</th>
                                    <th style="text-align: center;">MVT</th>
                                    <th style="text-align: center;">Queue No.</th>
                                    <th style="text-align: center;">Running no.</th>
                                    <th style="text-align: center;">Doc no.</th>
                                    <th style="text-align: center;">Transporter</th>
                                    <td style="text-align: center;">Check All &nbsp;
                                        <input style="width: 30px; height: 30px;" type="checkbox" name="selectAll" onchange="checkAll(this)" checked>
                                    </td>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th>Material Code</th>
                                    <th>Description</th>
                                    <th>Group QR ID</th>
                                    <th>ID</th>
                                    <th>Quantity</th>
                                    <th>U/M</th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th>M<sup>3</sup></th>
                                    <th>Status</th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <td></td>
                                </tr>
                            </tfoot>
                            <tbody>
                                <c:forEach items="${detList}" var="x">
                                    <tr>
                                        <td>${x.GID}</td>
                                        <td>${x.matc}</td>
                                        <td>${x.desc}</td>
                                        <td>${x.id}</td>
                                        <td align="right">${x.qty}&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                        <td align="center">${x.um}</td>
                                        <td align="center">${x.bag}</td>
                                        <td align="center">${x.roll}</td>
                                        <td align="center">${x.box}</td>
                                        <td align="right">${x.m3}</td>
                                        <td>${x.sts}</td>
                                        <td align="center">${x.mvt} : ${x.mvtn}</td>
                                        <td align="center">${x.qno}</td>
                                        <td align="center">${x.shipmentDate}</td>
                                        <td align="center">${x.docno}</td>
                                        <td align="left">${x.transporter}</td>
                                        <td align="center">
                                            ${x.ckBox}
                                        </td>
                                    </tr>
                                </c:forEach>
                            </tbody>
                        </table>
                    </form>
                    <!--**********************-->
                    <!--End Part 3-->
                    <!--</div>  end #wrapper-top--> 
                </div> 
                <br>
            </div> <!-- end .container-fluid -->
        </div> <!-- end #wrapper -->
    </body>
</html>