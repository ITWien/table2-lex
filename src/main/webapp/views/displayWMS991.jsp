<%@ include file="../fragments/taglibs.jsp" %>
<%@ include file="../fragments/imports.jsp" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.sql.*;" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>WMS</title>
        <!-- css :: vendors -->
        <jsp:include page = "../fragments/css.jsp" /> 
        <!-- additional custom :: my-style -->
        <link rel="stylesheet" href="../resources/assets/styles/myStyles.css">

        <!-- js :: vendors -->
        <jsp:include page = "../fragments/script.jsp" />
        <!-- additional custom :: my-script -->
        <script src="../resources/assets/scripts/myScripts.js" async></script>
        <script src="../resources/assets/scripts/toggleLoad.js" async></script> 
        <style>
            input[type=text], select{
                width: 100%;
                padding: 5px 5px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }

            input[type=date], input[type=number]{
                /*width: 100%;*/
                padding: 5px 5px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }

            input[type=submit], #ok {
                width: 100%;
                background-color: #00399b;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=submit]:hover, #ok:hover {
                background-color: #3973d6;
            }

            input[type=button] {
                width: 100%;
                background-color: #ef604a;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=button]:hover {
                background-color: #e51e00;
            }
        </style>
        <script>
            $(document).ready(function () {
                $('#showTable').DataTable({
                    "lengthMenu": [[-1, 10, 25, 50, 100], ["All", 10, 25, 50, 100]],
                    "bSortClasses": false,
                    "columnDefs": [
                        {orderable: false, targets: [2]}
//                        {"width": "7%", "targets": 2}
                    ]
                });

                // Setup - add a text input to each footer cell
                $('#showTable tfoot th').not(":eq(2)").each(function () {
                    var title = $(this).text();
                    $(this).html('<input type="text" />');
                });

                // DataTable
                var table = $('#showTable').DataTable();

                // Apply the search
                table.columns().every(function () {
                    var that = this;

                    $('input', this.footer()).on('keyup change', function () {
                        if (that.search() !== this.value) {
                            that
                                    .search(this.value)
                                    .draw();
                        }
                    });
                });
            });
        </script>
        <script>
            function ValidForm() {
                var sinv2 = document.getElementById('sinv2').innerHTML.trim();

                if (sinv2 !== '') {
                    document.getElementById('frm').submit();
                    document.getElementById('loader').style.display = 'block';
                }
            }
        </script>
        <style>
            #loader {
                border: 16px solid #f3f3f3;
                border-radius: 100%;
                border-top: 16px solid #3498db;
                width: 240px;
                height: 240px;
                -webkit-animation: spin 2s linear infinite;
                animation: spin 2s linear infinite;
                margin: auto;
                /*margin-left:1200px;*/
                margin-top: 10px;
                margin-bottom: 10px;
            }   

            .panel-default {
                border-color: transparent;
                background-color: transparent;
            }

            @-webkit-keyframes spin {
                0% { -webkit-transform: rotate(0deg); }
                100% { -webkit-transform: rotate(360deg); }
            }

            @keyframes spin {
                0% { transform: rotate(0deg); }
                100% { transform: rotate(360deg); }
            }

        </style>
        <style>
            body {font-family: Arial, Helvetica, sans-serif;}

            /* The Modal (background) */
            .modal {
                display: none; /* Hidden by default */
                position: fixed; /* Stay in place */
                z-index: 1; /* Sit on top */
                padding-top: 100px; /* Location of the box */
                left: 0;
                top: 0;
                width: 100%; /* Full width */
                height: 100%; /* Full height */
                overflow: auto; /* Enable scroll if needed */
                background-color: rgb(0,0,0); /* Fallback color */
                background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
            }

            /* Modal Content */
            .modal-content {
                background-color: #fefefe;
                margin: auto;
                padding: 20px;
                border: 1px solid #888;
                width: fit-content;
                height: 320px;
            }

            /* The Close Button */
            .close {
                color: #aaaaaa;
                float: right;
                font-size: 28px;
                font-weight: bold;
            }

            .close:hover,
            .close:focus {
                color: #000;
                text-decoration: none;
                cursor: pointer;
            }
        </style>
        <script>
            Array.prototype.remove = function () {
                var what, a = arguments, L = a.length, ax;
                while (L && this.length) {
                    what = a[--L];
                    while ((ax = this.indexOf(what)) !== -1) {
                        this.splice(ax, 1);
                    }
                }
                return this;
            };

            var aList = [${aList}];

            function ToSelect() {
                var input = document.getElementsByTagName('input');
                for (var i = 0; i < input.length; i++) {
                    if (input[i].type === 'checkbox') {
                        if (input[i].name === 'avaiCheck') {
                            if (input[i].checked === true) {
                                var ck = '<label style="cursor: pointer;">'
                                        + '<input type="checkbox" name="selectCheck" value="' + input[i].value + '" style="width: 15px; height: 15px;">  '
                                        + input[i].value
                                        + '</label><br>';

                                if (!aList.includes(input[i].value)) {
                                    aList.push(input[i].value);
                                    document.getElementById('sinv').innerHTML += ck;
                                }
                            }
                        }
                    }
                }

                var checkboxes = document.getElementsByTagName('input');
                for (var i = 0; i < checkboxes.length; i++) {
                    if (checkboxes[i].type === 'checkbox') {
                        if (checkboxes[i].name === 'avaiCheck') {
                            checkboxes[i].checked = false;
                        }
                    }
                }

            }

            function ToAvai() {
                var input = document.getElementsByTagName('input');
                var sinv = document.getElementById('sinv').innerHTML;
                for (var i = 0; i < input.length; i++) {
                    if (input[i].type === 'checkbox') {
                        if (input[i].name === 'selectCheck') {
                            if (input[i].checked === true) {
                                var ck = '<label style="cursor: pointer;">'
                                        + '<input type="checkbox" name="selectCheck" value="' + input[i].value + '" style="width: 15px; height: 15px;">  '
                                        + input[i].value
                                        + '</label><br>';

                                aList.remove(input[i].value);
                                sinv = sinv.toString().replace(ck, '');
                            }
                        }
                    }
                }

                document.getElementById('sinv').innerHTML = sinv;

            }

            function ToAvaiAll() {
                document.getElementById('sinv').innerHTML = '';
                aList = [];

            }

            function AllSelect() {
                document.getElementById('sinv2').innerHTML = document.getElementById('sinv').innerHTML.replace(/<br>/g, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;');
                var input = document.getElementsByTagName('input');
                var isNotEmpty = false;
                for (var i = 0; i < input.length; i++) {
                    if (input[i].type === 'checkbox') {
                        if (input[i].name === 'selectCheck') {
                            input[i].checked = true;
                            isNotEmpty = true;
                        }
                    }
                }

                if (isNotEmpty) {
//                    document.getElementById('add-inv').submit();
                    document.getElementById('myModal-add').style.display = 'none';
//                    ValidForm();
                }
            }

            function AutoCheck() {
                var input = document.getElementsByTagName('input');
                for (var i = 0; i < input.length; i++) {
                    if (input[i].type === 'checkbox') {
                        if (input[i].name === 'selectCheck') {
                            input[i].checked = true;
                        }
                    }
                }
            }
        </script>
        <script>
            function checkAll(ele) {
                var checkboxes = document.getElementsByTagName('input');
                if (ele.checked) {
                    for (var i = 0; i < checkboxes.length; i++) {
                        if (checkboxes[i].type === 'checkbox') {
                            if (checkboxes[i].name === 'avaiCheck') {
                                checkboxes[i].checked = true;
                            }
                        }
                    }
                } else {
                    for (var i = 0; i < checkboxes.length; i++) {
                        if (checkboxes[i].type === 'checkbox') {
                            if (checkboxes[i].name === 'avaiCheck') {
                                checkboxes[i].checked = false;
                            }
                        }
                    }
                }
            }

            function subRow(qno, head, flagId) {
                var Checked = document.getElementById('labChecked');
                var OnhandSKU = document.getElementById('labOnhandSKU');
                var Amount = document.getElementById('labAmount');
                var Correct = document.getElementById('labCorrect');
                var Diff = document.getElementById('labDiff');
                var DiffPer = document.getElementById('labDiffPer');
                var Package = document.getElementById('labPackage');
                var Package2 = document.getElementById('labPackage2');

                var tr = document.getElementsByTagName('tr');

                for (var i = 0; i < tr.length; i++) {
                    var flag = document.getElementById(flagId);
                    var tt = tr[i].id.toString().split("-")[0] + '-' + tr[i].id.toString().split("-")[1];
                    if (flag.checked === false) {
                        if (tt === qno) {
                            tr[i].style.display = "none";
                            document.getElementById(head).style.cssText = "cursor: pointer; background-color: #e5e5e5; color: black;";
                        }
                    } else {
                        if (tt === qno) {
                            var h = tr[i].id.toString().split("-")[2];
                            if (h === 'ckd' && Checked.checked === true) {
                                tr[i].style.display = "";
                                document.getElementById(head).style.cssText = "cursor: pointer; background-color: #003682; color: white;";
                            } else if (h === 'onh' && OnhandSKU.checked === true) {
                                tr[i].style.display = "";
                                document.getElementById(head).style.cssText = "cursor: pointer; background-color: #003682; color: white;";
                            } else if (h === 'amt' && Amount.checked === true) {
                                tr[i].style.display = "";
                                document.getElementById(head).style.cssText = "cursor: pointer; background-color: #003682; color: white;";
                            } else if (h === 'cor' && Correct.checked === true) {
                                tr[i].style.display = "";
                                document.getElementById(head).style.cssText = "cursor: pointer; background-color: #003682; color: white;";
                            } else if (h === 'dif' && Diff.checked === true) {
                                tr[i].style.display = "";
                                document.getElementById(head).style.cssText = "cursor: pointer; background-color: #003682; color: white;";
                            } else if (h === 'dip' && DiffPer.checked === true) {
                                tr[i].style.display = "";
                                document.getElementById(head).style.cssText = "cursor: pointer; background-color: #003682; color: white;";
                            } else if (h === 'pkg' && Package.checked === true) {
                                tr[i].style.display = "";
                                document.getElementById(head).style.cssText = "cursor: pointer; background-color: #003682; color: white;";
                            } else if (h === 'pkg2' && Package2.checked === true) {
                                tr[i].style.display = "";
                                document.getElementById(head).style.cssText = "cursor: pointer; background-color: #003682; color: white;";
                            }
                        }
                    }
                }
            }

            function expand() {
                document.getElementById("exp").style.display = "none";
                document.getElementById("com").style.display = "";
                var tr = document.getElementsByTagName('tr');

                var Checked = document.getElementById('labChecked');
                var OnhandSKU = document.getElementById('labOnhandSKU');
                var Amount = document.getElementById('labAmount');
                var Correct = document.getElementById('labCorrect');
                var Diff = document.getElementById('labDiff');
                var DiffPer = document.getElementById('labDiffPer');
                var Package = document.getElementById('labPackage');
                var Package2 = document.getElementById('labPackage2');

                for (var i = 0; i < tr.length; i++) {
                    if (tr[i].id.toString().split("-")[0] === "H") {
                        tr[i].style.cssText = "cursor: pointer; background-color: #003682; color: white;";
                    } else if (tr[i].id.toString().split("-")[0] === "D") {
                        var h = tr[i].id.toString().split("-")[2];
                        if (h === 'ckd' && Checked.checked === true) {
                            tr[i].style.display = "";
                        } else if (h === 'onh' && OnhandSKU.checked === true) {
                            tr[i].style.display = "";
                        } else if (h === 'amt' && Amount.checked === true) {
                            tr[i].style.display = "";
                        } else if (h === 'cor' && Correct.checked === true) {
                            tr[i].style.display = "";
                        } else if (h === 'dif' && Diff.checked === true) {
                            tr[i].style.display = "";
                        } else if (h === 'dip' && DiffPer.checked === true) {
                            tr[i].style.display = "";
                        } else if (h === 'pkg' && Package.checked === true) {
                            tr[i].style.display = "";
                        } else if (h === 'pkg2' && Package2.checked === true) {
                            tr[i].style.display = "";
                        }
                    }
                }

                var input = document.getElementsByTagName('input');
                for (var i = 0; i < input.length; i++) {
                    if (input[i].name === 'checkH') {
                        input[i].checked = true;
                    }
                }
            }

            function compress() {
                document.getElementById("exp").style.display = "";
                document.getElementById("com").style.display = "none";
                var tr = document.getElementsByTagName('tr');

                for (var i = 0; i < tr.length; i++) {
                    if (tr[i].id.toString().split("-")[0] === "H") {
                        tr[i].style.cssText = "cursor: pointer; background-color: #e5e5e5; color: black;";
                    } else if (tr[i].id.toString().split("-")[0] === "D") {
                        tr[i].style.display = "none";
                    }
                }

                var input = document.getElementsByTagName('input');
                for (var i = 0; i < input.length; i++) {
                    if (input[i].name === 'checkH') {
                        input[i].checked = false;
                    }
                }
            }

            function lab(obj, header) {
                var tr = document.getElementsByTagName('tr');

                for (var i = 0; i < tr.length; i++) {
                    if (tr[i].id.toString().split("-")[0] === "D") {
                        var id = tr[i].id.toString().split("-")[1];
                        var hh = document.getElementById(id);
                        var h = tr[i].id.toString().split("-")[2];
                        if (h === header) {
                            if (obj.checked === true && hh.checked === true) {
                                tr[i].style.display = "";
                            } else {
                                tr[i].style.display = "none";
                            }
                        }
                    }
                }
            }

            window.addEventListener('scroll', function () {
                var element = document.querySelector('#invi');
                var position = element.getBoundingClientRect();

                if (position.top >= 0 && position.bottom <= window.innerHeight) {
                    console.log('Element is fully visible in screen');
                    var b = document.getElementsByTagName('b');

                    for (var i = 0; i < b.length; i++) {
                        if (b[i].id.toString() === "invi2") {
                            b[i].style.display = 'none';
                        }
                    }

                } else if (position.top < window.innerHeight && position.bottom >= 0) {
                    console.log('Element is partially visible in screen');
                    var b = document.getElementsByTagName('b');

                    for (var i = 0; i < b.length; i++) {
                        if (b[i].id.toString() === "invi2") {
                            b[i].style.display = '';
                        }
                    }

                } else {
                    console.log('Element is fully invisible in screen');
                    var b = document.getElementsByTagName('b');

                    for (var i = 0; i < b.length; i++) {
                        if (b[i].id.toString() === "invi2") {
                            b[i].style.display = '';
                        }
                    }

                }
            });
        </script>
        <style>
            .lab {
                cursor: pointer; 
                background-color: #e0e0e0; 
                width: 130px;
                border-radius: 15px;
                text-align: center;
            }
        </style>
    </head>    
    <body onload="document.getElementById('userid').value = sessionStorage.getItem('uid');
            AutoCheck()">
        <div id="wrapper">

            <!-- /#sidebar-wrapper -->
            <%@ include file="../fragments/sidebar.jsp" %>
            <!--test-->

            <!-- Page Content -->
            <!-- nav-head-custom -->
            <%@ include file="../fragments/nav_head.jsp" %>

            <div class="container-fluid">
                <!--<div id="wrapper-top">-->
                <%-- PART 2 --%>
                <!--                    <div class="row">
                                        <div class="col-lg-12">
                                            <div id="set-height" style="height:415px;margin-top:0px;">
                                                <div id="sidebar-wrapper-top" class="">
                                                    <b class="page-header" style="padding-left:5px;font-size:18px;">
                <%-- <hr style="margin:0px;margin-top:10px;margin-bottom:10px;"> --%>
            </b>
        </div>
    </div>
</div>
</div> -->
                <%-- PART 3 --%>
                <!--show Qdest table-->
                <form action="../resources/manual/WMS991.pdf" target="_blank">
                    <table width="100%">
                        <tr>
                            <td width="94%"></td>
                            <td width="6%" align="right"><input type="submit" value="Manual" /></td>
                        </tr>
                    </table>
                </form>
                <div style="border: 2px solid #ccc; border-radius: 5px; padding: 20px;">
                    <form id="frm" name="frm" action="display" method="post">
                        <table width="100%">
                            <tr>
                                <td width="10%">
                                    <b style="color: #00399b;">Check Stock Date </b>
                                </td>
                                <td width="10%">
                                    <a onclick="document.getElementById('myModal-add').style.display = 'block';" style="cursor: pointer;"><i class="glyphicon glyphicon-calendar" style="font-size:25px; padding-left: 10px"></i></a>
                                </td>
                                <td width="80%">
                                    <div id="sinv2" style="display: block;">
                                        ${sinv2}
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td width="10%">
                                    <b style="color: #00399b;">Dept. / WH. </b>
                                </td>
                                <td width="10%">
                                    <select id="whd" name="whd" style=" width: 60%;">
                                        <option value="${whd}" selected hidden>${whd}</option>
                                        <optgroup label="Dept">
                                            <c:forEach items="${deptList}" var="x">
                                                <option value="${x.DEPT}">${x.DEPT}</option>
                                            </c:forEach>
                                        </optgroup>
                                        <optgroup label="Warehouse">
                                            <c:forEach items="${whList}" var="x">
                                                <option value="${x.WH}">${x.WH}</option>
                                            </c:forEach>
                                        </optgroup>
                                    </select>
                                </td>
                                <td width="80%">
                                    <input id="ok" type="button" value="Confirm" onclick="ValidForm();" style=" width: 100px;"/>
                                    &nbsp;&nbsp;&nbsp;
                                    <label class="lab">
                                        <input type="checkbox" id="labChecked" name="labN" value="1" onchange="lab(this, 'ckd');" style="width: 20px; height: 20px;" ${checked1}>
                                        Checked
                                    </label>
                                    <label class="lab">
                                        <input type="checkbox" id="labOnhandSKU" name="labN" value="2" onchange="lab(this, 'onh');" style="width: 20px; height: 20px;" ${checked2}>
                                        Onhand SKU
                                    </label>
                                    <label class="lab">
                                        <input type="checkbox" id="labAmount" name="labN" value="3" onchange="lab(this, 'amt');" style="width: 20px; height: 20px;" ${checked3}>
                                        Amount
                                    </label>
                                    <label class="lab">
                                        <input type="checkbox" id="labCorrect" name="labN" value="4" onchange="lab(this, 'cor');" style="width: 20px; height: 20px;" ${checked4}>
                                        Correct
                                    </label>
                                    <label class="lab">
                                        <input type="checkbox" id="labDiff" name="labN" value="5" onchange="lab(this, 'dif');" style="width: 20px; height: 20px;" ${checked5}>
                                        Diff
                                    </label>
                                    <label class="lab">
                                        <input type="checkbox" id="labDiffPer" name="labN" value="6" onchange="lab(this, 'dip');" style="width: 20px; height: 20px;" ${checked6}>
                                        % Diff
                                    </label>
                                    <label class="lab" style="width: 200px;">
                                        <input type="checkbox" id="labPackage" name="labN" value="7" onchange="lab(this, 'pkg');" style="width: 20px; height: 20px;" ${checked7}>
                                        Package (WMS)
                                    </label>
                                    <label class="lab" style="width: 200px;">
                                        <input type="checkbox" id="labPackage2" name="labN" value="8" onchange="lab(this, 'pkg2');" style="width: 20px; height: 20px;" ${checked8}>
                                        Package (TABLET)
                                    </label>
                                </td>
                            </tr>
                            <tr>
                                <td width="10%">
                                    <b style="color: #00399b;">Plant </b>
                                </td>
                                <td width="10%">
                                    <select id="plant" name="plant" style=" width: 100px;">
                                        <option value="${plant}" selected hidden>${plant}</option>
                                        <option value="ALL">ALL</option>
                                        <option value="1000">1000</option>
                                        <option value="1050">1050</option>
                                    </select>
                                </td>
                                <td width="80%">
                                </td>
                            </tr>
                        </table>
                    </form>
                    <br>
                    <div id="loader" style="display: none;"></div>
                    <table id="showTable" class="display" style="width:100%; border: 1px solid #ccc; border-radius: 4px;">
                        <thead>
                            <tr id="invi">
                                <th style="text-align: center;">Mat Ctrl</th>
                                <th style="text-align: center;">Name</th>
                                <th style="text-align: center;"></th>
                                <th style="text-align: center;">JAN</th>
                                <th style="text-align: center;">FEB</th>
                                <th style="text-align: center;">MAR</th>
                                <th style="text-align: center;">APR</th>
                                <th style="text-align: center;">MAY</th>
                                <th style="text-align: center;">JUN</th>
                                <th style="text-align: center;">JUL</th>
                                <th style="text-align: center;">AUG</th>
                                <th style="text-align: center;">SEP</th>
                                <th style="text-align: center;">OCT</th>
                                <th style="text-align: center;">NOV</th>
                                <th style="text-align: center;">DEC</th>
                                <th style="text-align: center;">Total</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th></th>
                                <th></th>
                                <th>
                                    <a style=" width: fit-content; height: 30px;" class="btn btn-primary" id="exp" name="exp" onclick="expand();">
                                        Expand All
                                    </a>
                                    <a style=" width: fit-content; height: 30px; display: none;" class="btn btn-warning" id="com" name="com" onclick="compress();">
                                        Collapse All
                                    </a>
                                </th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                            </tr>
                        </tfoot>
                        <tbody>
                            <c:forEach items="${detailList}" var="p" varStatus="i">
                                <tr id="H-${p.PRODUCT}" onclick="subRow('D-${p.PRODUCT}', 'H-${p.PRODUCT}', '${p.PRODUCT}');
                                        document.getElementById('${p.PRODUCT}').click();" style="cursor: pointer;">
                                    <th>${p.PRODUCT}</th>
                                    <th>${p.NAME}</th>
                                    <th>
                                        <input type="checkbox" id="${p.PRODUCT}" name="checkH" style="display: none;">
                                    </th>
                                    <th style="text-align: center;"><b id="invi2" style="display: none;">JAN</b></th>
                                    <th style="text-align: center;"><b id="invi2" style="display: none;">FEB</b></th>
                                    <th style="text-align: center;"><b id="invi2" style="display: none;">MAR</b></th>
                                    <th style="text-align: center;"><b id="invi2" style="display: none;">APR</b></th>
                                    <th style="text-align: center;"><b id="invi2" style="display: none;">MAY</b></th>
                                    <th style="text-align: center;"><b id="invi2" style="display: none;">JUN</b></th>
                                    <th style="text-align: center;"><b id="invi2" style="display: none;">JUL</b></th>
                                    <th style="text-align: center;"><b id="invi2" style="display: none;">AUG</b></th>
                                    <th style="text-align: center;"><b id="invi2" style="display: none;">SEP</b></th>
                                    <th style="text-align: center;"><b id="invi2" style="display: none;">OCT</b></th>
                                    <th style="text-align: center;"><b id="invi2" style="display: none;">NOV</b></th>
                                    <th style="text-align: center;"><b id="invi2" style="display: none;">DEC</b></th>
                                    <th style="text-align: center;"><b id="invi2" style="display: none;">Total</b></th>
                                </tr>
                                <tr id="D-${p.PRODUCT}-ckd" style="display: none;">
                                    <td><b style="opacity: 0;">${p.PRODUCT}</b></td>
                                    <td><b style="opacity: 0;">${p.NAME}</b></td>
                                    <td>Checked</td>
                                    <td style="text-align: right;"><fmt:formatNumber pattern="###,###,###,##0" value="${p.CK_SKU1==0?'':p.CK_SKU1}" />&nbsp;&nbsp;</td>
                                    <td style="text-align: right;"><fmt:formatNumber pattern="###,###,###,##0" value="${p.CK_SKU2==0?'':p.CK_SKU2}" />&nbsp;&nbsp;</td>
                                    <td style="text-align: right;"><fmt:formatNumber pattern="###,###,###,##0" value="${p.CK_SKU3==0?'':p.CK_SKU3}" />&nbsp;&nbsp;</td>
                                    <td style="text-align: right;"><fmt:formatNumber pattern="###,###,###,##0" value="${p.CK_SKU4==0?'':p.CK_SKU4}" />&nbsp;&nbsp;</td>
                                    <td style="text-align: right;"><fmt:formatNumber pattern="###,###,###,##0" value="${p.CK_SKU5==0?'':p.CK_SKU5}" />&nbsp;&nbsp;</td>
                                    <td style="text-align: right;"><fmt:formatNumber pattern="###,###,###,##0" value="${p.CK_SKU6==0?'':p.CK_SKU6}" />&nbsp;&nbsp;</td>
                                    <td style="text-align: right;"><fmt:formatNumber pattern="###,###,###,##0" value="${p.CK_SKU7==0?'':p.CK_SKU7}" />&nbsp;&nbsp;</td>
                                    <td style="text-align: right;"><fmt:formatNumber pattern="###,###,###,##0" value="${p.CK_SKU8==0?'':p.CK_SKU8}" />&nbsp;&nbsp;</td>
                                    <td style="text-align: right;"><fmt:formatNumber pattern="###,###,###,##0" value="${p.CK_SKU9==0?'':p.CK_SKU9}" />&nbsp;&nbsp;</td>
                                    <td style="text-align: right;"><fmt:formatNumber pattern="###,###,###,##0" value="${p.CK_SKU10==0?'':p.CK_SKU10}" />&nbsp;&nbsp;</td>
                                    <td style="text-align: right;"><fmt:formatNumber pattern="###,###,###,##0" value="${p.CK_SKU11==0?'':p.CK_SKU11}" />&nbsp;&nbsp;</td>
                                    <td style="text-align: right;"><fmt:formatNumber pattern="###,###,###,##0" value="${p.CK_SKU12==0?'':p.CK_SKU12}" />&nbsp;&nbsp;</td>
                                    <td style="text-align: right;"><fmt:formatNumber pattern="###,###,###,##0" value="${p.CK_SKU==0?'':p.CK_SKU}" />&nbsp;&nbsp;</td>
                                </tr>
                                <tr id="D-${p.PRODUCT}-onh" style="display: none;">
                                    <td><b style="opacity: 0;">${p.PRODUCT}</b></td>
                                    <td><b style="opacity: 0;">${p.NAME}</b></td>
                                    <td>Onhand SKU</td>
                                    <td style="text-align: right;"><fmt:formatNumber pattern="###,###,###,##0" value="${p.OH_SKU1==0?'':p.OH_SKU1}" />&nbsp;&nbsp;</td>
                                    <td style="text-align: right;"><fmt:formatNumber pattern="###,###,###,##0" value="${p.OH_SKU2==0?'':p.OH_SKU2}" />&nbsp;&nbsp;</td>
                                    <td style="text-align: right;"><fmt:formatNumber pattern="###,###,###,##0" value="${p.OH_SKU3==0?'':p.OH_SKU3}" />&nbsp;&nbsp;</td>
                                    <td style="text-align: right;"><fmt:formatNumber pattern="###,###,###,##0" value="${p.OH_SKU4==0?'':p.OH_SKU4}" />&nbsp;&nbsp;</td>
                                    <td style="text-align: right;"><fmt:formatNumber pattern="###,###,###,##0" value="${p.OH_SKU5==0?'':p.OH_SKU5}" />&nbsp;&nbsp;</td>
                                    <td style="text-align: right;"><fmt:formatNumber pattern="###,###,###,##0" value="${p.OH_SKU6==0?'':p.OH_SKU6}" />&nbsp;&nbsp;</td>
                                    <td style="text-align: right;"><fmt:formatNumber pattern="###,###,###,##0" value="${p.OH_SKU7==0?'':p.OH_SKU7}" />&nbsp;&nbsp;</td>
                                    <td style="text-align: right;"><fmt:formatNumber pattern="###,###,###,##0" value="${p.OH_SKU8==0?'':p.OH_SKU8}" />&nbsp;&nbsp;</td>
                                    <td style="text-align: right;"><fmt:formatNumber pattern="###,###,###,##0" value="${p.OH_SKU9==0?'':p.OH_SKU9}" />&nbsp;&nbsp;</td>
                                    <td style="text-align: right;"><fmt:formatNumber pattern="###,###,###,##0" value="${p.OH_SKU10==0?'':p.OH_SKU10}" />&nbsp;&nbsp;</td>
                                    <td style="text-align: right;"><fmt:formatNumber pattern="###,###,###,##0" value="${p.OH_SKU11==0?'':p.OH_SKU11}" />&nbsp;&nbsp;</td>
                                    <td style="text-align: right;"><fmt:formatNumber pattern="###,###,###,##0" value="${p.OH_SKU12==0?'':p.OH_SKU12}" />&nbsp;&nbsp;</td>
                                    <td style="text-align: right;"><fmt:formatNumber pattern="###,###,###,##0" value="${p.OH_SKU==0?'':p.OH_SKU}" />&nbsp;&nbsp;</td>
                                </tr>
                                <tr id="D-${p.PRODUCT}-amt" style="display: none;">
                                    <td><b style="opacity: 0;">${p.PRODUCT}</b></td>
                                    <td><b style="opacity: 0;">${p.NAME}</b></td>
                                    <td>Amount</td>
                                    <td style="text-align: right;"><fmt:formatNumber pattern="###,###,###,##0.000" value="${p.AMOUNT_1==0?'':p.AMOUNT_1}" />&nbsp;&nbsp;</td>
                                    <td style="text-align: right;"><fmt:formatNumber pattern="###,###,###,##0.000" value="${p.AMOUNT_2==0?'':p.AMOUNT_2}" />&nbsp;&nbsp;</td>
                                    <td style="text-align: right;"><fmt:formatNumber pattern="###,###,###,##0.000" value="${p.AMOUNT_3==0?'':p.AMOUNT_3}" />&nbsp;&nbsp;</td>
                                    <td style="text-align: right;"><fmt:formatNumber pattern="###,###,###,##0.000" value="${p.AMOUNT_4==0?'':p.AMOUNT_4}" />&nbsp;&nbsp;</td>
                                    <td style="text-align: right;"><fmt:formatNumber pattern="###,###,###,##0.000" value="${p.AMOUNT_5==0?'':p.AMOUNT_5}" />&nbsp;&nbsp;</td>
                                    <td style="text-align: right;"><fmt:formatNumber pattern="###,###,###,##0.000" value="${p.AMOUNT_6==0?'':p.AMOUNT_6}" />&nbsp;&nbsp;</td>
                                    <td style="text-align: right;"><fmt:formatNumber pattern="###,###,###,##0.000" value="${p.AMOUNT_7==0?'':p.AMOUNT_7}" />&nbsp;&nbsp;</td>
                                    <td style="text-align: right;"><fmt:formatNumber pattern="###,###,###,##0.000" value="${p.AMOUNT_8==0?'':p.AMOUNT_8}" />&nbsp;&nbsp;</td>
                                    <td style="text-align: right;"><fmt:formatNumber pattern="###,###,###,##0.000" value="${p.AMOUNT_9==0?'':p.AMOUNT_9}" />&nbsp;&nbsp;</td>
                                    <td style="text-align: right;"><fmt:formatNumber pattern="###,###,###,##0.000" value="${p.AMOUNT_10==0?'':p.AMOUNT_10}" />&nbsp;&nbsp;</td>
                                    <td style="text-align: right;"><fmt:formatNumber pattern="###,###,###,##0.000" value="${p.AMOUNT_11==0?'':p.AMOUNT_11}" />&nbsp;&nbsp;</td>
                                    <td style="text-align: right;"><fmt:formatNumber pattern="###,###,###,##0.000" value="${p.AMOUNT_12==0?'':p.AMOUNT_12}" />&nbsp;&nbsp;</td>
                                    <td style="text-align: right;"><fmt:formatNumber pattern="###,###,###,##0.000" value="${p.AMOUNT==0?'':p.AMOUNT}" />&nbsp;&nbsp;</td>
                                </tr>
                                <tr id="D-${p.PRODUCT}-cor" style="display: none;">
                                    <td><b style="opacity: 0;">${p.PRODUCT}</b></td>
                                    <td><b style="opacity: 0;">${p.NAME}</b></td>
                                    <td>Correct</td>
                                    <td style="text-align: right;"><fmt:formatNumber pattern="###,###,###,##0" value="${p.COL_SKU1==0?'':p.COL_SKU1}" />&nbsp;&nbsp;</td>
                                    <td style="text-align: right;"><fmt:formatNumber pattern="###,###,###,##0" value="${p.COL_SKU2==0?'':p.COL_SKU2}" />&nbsp;&nbsp;</td>
                                    <td style="text-align: right;"><fmt:formatNumber pattern="###,###,###,##0" value="${p.COL_SKU3==0?'':p.COL_SKU3}" />&nbsp;&nbsp;</td>
                                    <td style="text-align: right;"><fmt:formatNumber pattern="###,###,###,##0" value="${p.COL_SKU4==0?'':p.COL_SKU4}" />&nbsp;&nbsp;</td>
                                    <td style="text-align: right;"><fmt:formatNumber pattern="###,###,###,##0" value="${p.COL_SKU5==0?'':p.COL_SKU5}" />&nbsp;&nbsp;</td>
                                    <td style="text-align: right;"><fmt:formatNumber pattern="###,###,###,##0" value="${p.COL_SKU6==0?'':p.COL_SKU6}" />&nbsp;&nbsp;</td>
                                    <td style="text-align: right;"><fmt:formatNumber pattern="###,###,###,##0" value="${p.COL_SKU7==0?'':p.COL_SKU7}" />&nbsp;&nbsp;</td>
                                    <td style="text-align: right;"><fmt:formatNumber pattern="###,###,###,##0" value="${p.COL_SKU8==0?'':p.COL_SKU8}" />&nbsp;&nbsp;</td>
                                    <td style="text-align: right;"><fmt:formatNumber pattern="###,###,###,##0" value="${p.COL_SKU9==0?'':p.COL_SKU9}" />&nbsp;&nbsp;</td>
                                    <td style="text-align: right;"><fmt:formatNumber pattern="###,###,###,##0" value="${p.COL_SKU10==0?'':p.COL_SKU10}" />&nbsp;&nbsp;</td>
                                    <td style="text-align: right;"><fmt:formatNumber pattern="###,###,###,##0" value="${p.COL_SKU11==0?'':p.COL_SKU11}" />&nbsp;&nbsp;</td>
                                    <td style="text-align: right;"><fmt:formatNumber pattern="###,###,###,##0" value="${p.COL_SKU12==0?'':p.COL_SKU12}" />&nbsp;&nbsp;</td>
                                    <td style="text-align: right;"><fmt:formatNumber pattern="###,###,###,##0" value="${p.COL_SKU==0?'':p.COL_SKU}" />&nbsp;&nbsp;</td>
                                </tr>
                                <tr id="D-${p.PRODUCT}-dif" style="display: none;">
                                    <td><b style="opacity: 0;">${p.PRODUCT}</b></td>
                                    <td><b style="opacity: 0;">${p.NAME}</b></td>
                                    <td>Diff</td>
                                    <td style="text-align: right;"><fmt:formatNumber pattern="###,###,###,##0" value="${p.DIFF_SKU1==0?'':p.DIFF_SKU1}" />&nbsp;&nbsp;</td>
                                    <td style="text-align: right;"><fmt:formatNumber pattern="###,###,###,##0" value="${p.DIFF_SKU2==0?'':p.DIFF_SKU2}" />&nbsp;&nbsp;</td>
                                    <td style="text-align: right;"><fmt:formatNumber pattern="###,###,###,##0" value="${p.DIFF_SKU3==0?'':p.DIFF_SKU3}" />&nbsp;&nbsp;</td>
                                    <td style="text-align: right;"><fmt:formatNumber pattern="###,###,###,##0" value="${p.DIFF_SKU4==0?'':p.DIFF_SKU4}" />&nbsp;&nbsp;</td>
                                    <td style="text-align: right;"><fmt:formatNumber pattern="###,###,###,##0" value="${p.DIFF_SKU5==0?'':p.DIFF_SKU5}" />&nbsp;&nbsp;</td>
                                    <td style="text-align: right;"><fmt:formatNumber pattern="###,###,###,##0" value="${p.DIFF_SKU6==0?'':p.DIFF_SKU6}" />&nbsp;&nbsp;</td>
                                    <td style="text-align: right;"><fmt:formatNumber pattern="###,###,###,##0" value="${p.DIFF_SKU7==0?'':p.DIFF_SKU7}" />&nbsp;&nbsp;</td>
                                    <td style="text-align: right;"><fmt:formatNumber pattern="###,###,###,##0" value="${p.DIFF_SKU8==0?'':p.DIFF_SKU8}" />&nbsp;&nbsp;</td>
                                    <td style="text-align: right;"><fmt:formatNumber pattern="###,###,###,##0" value="${p.DIFF_SKU9==0?'':p.DIFF_SKU9}" />&nbsp;&nbsp;</td>
                                    <td style="text-align: right;"><fmt:formatNumber pattern="###,###,###,##0" value="${p.DIFF_SKU10==0?'':p.DIFF_SKU10}" />&nbsp;&nbsp;</td>
                                    <td style="text-align: right;"><fmt:formatNumber pattern="###,###,###,##0" value="${p.DIFF_SKU11==0?'':p.DIFF_SKU11}" />&nbsp;&nbsp;</td>
                                    <td style="text-align: right;"><fmt:formatNumber pattern="###,###,###,##0" value="${p.DIFF_SKU12==0?'':p.DIFF_SKU12}" />&nbsp;&nbsp;</td>
                                    <td style="text-align: right;"><fmt:formatNumber pattern="###,###,###,##0" value="${p.DIFF_SKU==0?'':p.DIFF_SKU}" />&nbsp;&nbsp;</td>
                                </tr>
                                <tr id="D-${p.PRODUCT}-dip" style="display: none;">
                                    <td><b style="opacity: 0;">${p.PRODUCT}</b></td>
                                    <td><b style="opacity: 0;">${p.NAME}</b></td>
                                    <td>% Diff</td>
                                    <td style="text-align: right;"><fmt:formatNumber pattern="###,###,###,##0.0" value="${p.DIFF_SKU1_PER==0?'':p.DIFF_SKU1_PER}" /> ${p.DIFF_SKU1_PER==0?'':'%'}</td>
                                    <td style="text-align: right;"><fmt:formatNumber pattern="###,###,###,##0.0" value="${p.DIFF_SKU2_PER==0?'':p.DIFF_SKU2_PER}" /> ${p.DIFF_SKU2_PER==0?'':'%'}</td>
                                    <td style="text-align: right;"><fmt:formatNumber pattern="###,###,###,##0.0" value="${p.DIFF_SKU3_PER==0?'':p.DIFF_SKU3_PER}" /> ${p.DIFF_SKU3_PER==0?'':'%'}</td>
                                    <td style="text-align: right;"><fmt:formatNumber pattern="###,###,###,##0.0" value="${p.DIFF_SKU4_PER==0?'':p.DIFF_SKU4_PER}" /> ${p.DIFF_SKU4_PER==0?'':'%'}</td>
                                    <td style="text-align: right;"><fmt:formatNumber pattern="###,###,###,##0.0" value="${p.DIFF_SKU5_PER==0?'':p.DIFF_SKU5_PER}" /> ${p.DIFF_SKU5_PER==0?'':'%'}</td>
                                    <td style="text-align: right;"><fmt:formatNumber pattern="###,###,###,##0.0" value="${p.DIFF_SKU6_PER==0?'':p.DIFF_SKU6_PER}" /> ${p.DIFF_SKU6_PER==0?'':'%'}</td>
                                    <td style="text-align: right;"><fmt:formatNumber pattern="###,###,###,##0.0" value="${p.DIFF_SKU7_PER==0?'':p.DIFF_SKU7_PER}" /> ${p.DIFF_SKU7_PER==0?'':'%'}</td>
                                    <td style="text-align: right;"><fmt:formatNumber pattern="###,###,###,##0.0" value="${p.DIFF_SKU8_PER==0?'':p.DIFF_SKU8_PER}" /> ${p.DIFF_SKU8_PER==0?'':'%'}</td>
                                    <td style="text-align: right;"><fmt:formatNumber pattern="###,###,###,##0.0" value="${p.DIFF_SKU9_PER==0?'':p.DIFF_SKU9_PER}" /> ${p.DIFF_SKU9_PER==0?'':'%'}</td>
                                    <td style="text-align: right;"><fmt:formatNumber pattern="###,###,###,##0.0" value="${p.DIFF_SKU10_PER==0?'':p.DIFF_SKU10_PER}" /> ${p.DIFF_SKU10_PER==0?'':'%'}</td>
                                    <td style="text-align: right;"><fmt:formatNumber pattern="###,###,###,##0.0" value="${p.DIFF_SKU11_PER==0?'':p.DIFF_SKU11_PER}" /> ${p.DIFF_SKU11_PER==0?'':'%'}</td>
                                    <td style="text-align: right;"><fmt:formatNumber pattern="###,###,###,##0.0" value="${p.DIFF_SKU12_PER==0?'':p.DIFF_SKU12_PER}" /> ${p.DIFF_SKU12_PER==0?'':'%'}</td>
                                    <td style="text-align: right;"><fmt:formatNumber pattern="###,###,###,##0.0" value="${p.DIFF_SKU_PER==0?'':p.DIFF_SKU_PER}" /> ${p.DIFF_SKU_PER==0?'':'%'}</td>
                                </tr>
                                <tr id="D-${p.PRODUCT}-pkg" style="display: none;">
                                    <td><b style="opacity: 0;">${p.PRODUCT}</b></td>
                                    <td><b style="opacity: 0;">${p.NAME}</b></td>
                                    <td>Package (WMS)</td>
                                    <td style="text-align: right;"><fmt:formatNumber pattern="###,###,###,##0" value="${p.PACKPCS_1==0?'':p.PACKPCS_1}" />&nbsp;&nbsp;</td>
                                    <td style="text-align: right;"><fmt:formatNumber pattern="###,###,###,##0" value="${p.PACKPCS_2==0?'':p.PACKPCS_2}" />&nbsp;&nbsp;</td>
                                    <td style="text-align: right;"><fmt:formatNumber pattern="###,###,###,##0" value="${p.PACKPCS_3==0?'':p.PACKPCS_3}" />&nbsp;&nbsp;</td>
                                    <td style="text-align: right;"><fmt:formatNumber pattern="###,###,###,##0" value="${p.PACKPCS_4==0?'':p.PACKPCS_4}" />&nbsp;&nbsp;</td>
                                    <td style="text-align: right;"><fmt:formatNumber pattern="###,###,###,##0" value="${p.PACKPCS_5==0?'':p.PACKPCS_5}" />&nbsp;&nbsp;</td>
                                    <td style="text-align: right;"><fmt:formatNumber pattern="###,###,###,##0" value="${p.PACKPCS_6==0?'':p.PACKPCS_6}" />&nbsp;&nbsp;</td>
                                    <td style="text-align: right;"><fmt:formatNumber pattern="###,###,###,##0" value="${p.PACKPCS_7==0?'':p.PACKPCS_7}" />&nbsp;&nbsp;</td>
                                    <td style="text-align: right;"><fmt:formatNumber pattern="###,###,###,##0" value="${p.PACKPCS_8==0?'':p.PACKPCS_8}" />&nbsp;&nbsp;</td>
                                    <td style="text-align: right;"><fmt:formatNumber pattern="###,###,###,##0" value="${p.PACKPCS_9==0?'':p.PACKPCS_9}" />&nbsp;&nbsp;</td>
                                    <td style="text-align: right;"><fmt:formatNumber pattern="###,###,###,##0" value="${p.PACKPCS_10==0?'':p.PACKPCS_10}" />&nbsp;&nbsp;</td>
                                    <td style="text-align: right;"><fmt:formatNumber pattern="###,###,###,##0" value="${p.PACKPCS_11==0?'':p.PACKPCS_11}" />&nbsp;&nbsp;</td>
                                    <td style="text-align: right;"><fmt:formatNumber pattern="###,###,###,##0" value="${p.PACKPCS_12==0?'':p.PACKPCS_12}" />&nbsp;&nbsp;</td>
                                    <td style="text-align: right;"><fmt:formatNumber pattern="###,###,###,##0" value="${p.PACKPCS==0?'':p.PACKPCS}" />&nbsp;&nbsp;</td>
                                </tr>
                                <tr id="D-${p.PRODUCT}-pkg2" style="display: none;">
                                    <td><b style="opacity: 0;">${p.PRODUCT}</b></td>
                                    <td><b style="opacity: 0;">${p.NAME}</b></td>
                                    <td>Package (TABLET)</td>
                                    <td style="text-align: right;"><fmt:formatNumber pattern="###,###,###,##0" value="${p.PACKPCS2_1==0?'':p.PACKPCS2_1}" />&nbsp;&nbsp;</td>
                                    <td style="text-align: right;"><fmt:formatNumber pattern="###,###,###,##0" value="${p.PACKPCS2_2==0?'':p.PACKPCS2_2}" />&nbsp;&nbsp;</td>
                                    <td style="text-align: right;"><fmt:formatNumber pattern="###,###,###,##0" value="${p.PACKPCS2_3==0?'':p.PACKPCS2_3}" />&nbsp;&nbsp;</td>
                                    <td style="text-align: right;"><fmt:formatNumber pattern="###,###,###,##0" value="${p.PACKPCS2_4==0?'':p.PACKPCS2_4}" />&nbsp;&nbsp;</td>
                                    <td style="text-align: right;"><fmt:formatNumber pattern="###,###,###,##0" value="${p.PACKPCS2_5==0?'':p.PACKPCS2_5}" />&nbsp;&nbsp;</td>
                                    <td style="text-align: right;"><fmt:formatNumber pattern="###,###,###,##0" value="${p.PACKPCS2_6==0?'':p.PACKPCS2_6}" />&nbsp;&nbsp;</td>
                                    <td style="text-align: right;"><fmt:formatNumber pattern="###,###,###,##0" value="${p.PACKPCS2_7==0?'':p.PACKPCS2_7}" />&nbsp;&nbsp;</td>
                                    <td style="text-align: right;"><fmt:formatNumber pattern="###,###,###,##0" value="${p.PACKPCS2_8==0?'':p.PACKPCS2_8}" />&nbsp;&nbsp;</td>
                                    <td style="text-align: right;"><fmt:formatNumber pattern="###,###,###,##0" value="${p.PACKPCS2_9==0?'':p.PACKPCS2_9}" />&nbsp;&nbsp;</td>
                                    <td style="text-align: right;"><fmt:formatNumber pattern="###,###,###,##0" value="${p.PACKPCS2_10==0?'':p.PACKPCS2_10}" />&nbsp;&nbsp;</td>
                                    <td style="text-align: right;"><fmt:formatNumber pattern="###,###,###,##0" value="${p.PACKPCS2_11==0?'':p.PACKPCS2_11}" />&nbsp;&nbsp;</td>
                                    <td style="text-align: right;"><fmt:formatNumber pattern="###,###,###,##0" value="${p.PACKPCS2_12==0?'':p.PACKPCS2_12}" />&nbsp;&nbsp;</td>
                                    <td style="text-align: right;"><fmt:formatNumber pattern="###,###,###,##0" value="${p.PACKPCS2==0?'':p.PACKPCS2}" />&nbsp;&nbsp;</td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                    <div id="myModal-add" class="modal">
                        <!-- Modal content -->
                        <div class="modal-content" style=" height: 600px; width: 1000px;">
                            <span id="span-add" class="close" onclick="document.getElementById('myModal-add').style.display = 'none';">&times;</span>
                            <p><b><font size="4"></font></b></p>
                            <table width="100%">
                                <tr style=" background-color: white;">
                                    <td align="center">
                                        <b style="color: #00399b;">
                                            <font size="5">Select Onhand Date</font><hr>
                                            <table style=" width: 80%;">
                                                <tr>
                                                    <td style=" text-align: center;">
                                                        <font size="4">Available Onhand Date</font>
                                                    </td>
                                                    <td></td>
                                                    <td style=" text-align: center;">
                                                        <font size="4">Selected Onhand Date</font>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style=" text-align: center;">
                                                    </td>
                                                    <td style=" opacity: 0;">XXX</td>
                                                    <td style=" text-align: center;">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style=" text-align: left; width: 40%;">
                                                        <div id="ainv" style=" padding-left: 20px; padding-top: 20px; border: 1px solid #ccc; border-radius: 5px; width: 100%; height: 300px; overflow: auto;">
                                                            <label style="cursor: pointer;">
                                                                <input type="checkbox" name="selectAll" onchange="checkAll(this);" style="width: 15px; height: 15px;">
                                                                All
                                                            </label>
                                                            <br>
                                                            <c:forEach items="${ohdList}" var="p" varStatus="i">
                                                                <label style="cursor: pointer;">
                                                                    <input type="checkbox" name="avaiCheck" value="${p.OH_SKU}" style="width: 15px; height: 15px;">
                                                                    ${p.OH_SKU}
                                                                </label>
                                                                <br>
                                                            </c:forEach>
                                                        </div>
                                                    </td>
                                                    <td style=" text-align: center;">
                                                        <br><br>
                                                        <button class="btn btn btn-outline btn-info" onclick="ToSelect();"><i class="fa fa-angle-right" style="font-size:20px;"></i></button><br><br>
                                                        <button class="btn btn btn-outline btn-info" onclick="ToAvai();"><i class="fa fa-angle-left" style="font-size:20px;"></i></button><br><br>
                                                        <button class="btn btn btn-outline btn-info" onclick="ToAvaiAll();"><i class="fa fa-angle-double-left" style="font-size:20px;"></i></button>
                                                    </td>
                                                    <td style=" text-align: left; width: 40%;">
                                                        <form id="add-inv" action="create" method="post">
                                                            <input type="hidden" id="userid" name="userid">
                                                            <div id="sinv" style=" padding-left: 20px; padding-top: 20px; border: 1px solid #ccc; border-radius: 5px; width: 100%; height: 300px; overflow: auto;">
                                                                ${sinv}
                                                            </div>
                                                        </form>
                                                    </td>
                                                </tr>
                                            </table>
                                        </b>
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <br><br><br>
                                        <a class="btn btn btn-outline btn-danger" onclick="document.getElementById('myModal-add').style.display = 'none';">
                                            <i class="fa fa-times-circle-o" style="font-size:20px;"></i> Cancel</a>  
                                        &nbsp;
                                        <a class="btn btn btn-outline btn-success" onclick="AllSelect();">
                                            <i class="fa fa-check-circle-o" style="font-size:20px;"></i> Confirm</a>                    
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <!--**********************-->
                    <!--End Part 3-->
                    <!--</div>  end #wrapper-top--> 
                </div> 
                <br><br>
            </div> <!-- end .container-fluid -->
        </div> <!-- end #wrapper -->
    </body>
</html>