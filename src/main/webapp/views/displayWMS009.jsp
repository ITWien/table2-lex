<%@ include file="../fragments/taglibs.jsp" %>
<%@ include file="../fragments/imports.jsp" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.sql.*;" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>WMS</title>
        <!-- css :: vendors -->
        <jsp:include page = "../fragments/css.jsp" />
        <!-- additional custom :: my-style -->
        <link rel="stylesheet" href="../resources/assets/styles/myStyles.css">

        <!-- js :: vendors -->
        <jsp:include page = "../fragments/script.jsp" />
        <!-- additional custom :: my-script -->
        <script src="../resources/assets/scripts/myScripts.js" async></script>
        <script src="../resources/assets/scripts/toggleLoad.js" async></script>   
        <script>
            $(document).ready(function () {
                $('#showTable').DataTable({
                    "lengthMenu": [[10, 25, 50, 100], [10, 25, 50, 100]],
                    "bSortClasses": false
                });

                // Setup - add a text input to each footer cell
                $('#showTable tfoot th').not(":eq(6)").each(function () {
                    var title = $(this).text();
                    $(this).html('<input type="text" />');
                });

                // DataTable
                var table = $('#showTable').DataTable();

                // Apply the search
                table.columns().every(function () {
                    var that = this;

                    $('input', this.footer()).on('keyup change', function () {
                        if (that.search() !== this.value) {
                            that
                                    .search(this.value)
                                    .draw();
                        }
                    });
                });
            });

            function FNopenmodal(uid) {
                $('#modalCopyMenu').modal("show");
                $('#modal_uid_from').val(uid);

                $.ajax({
                    url: "/TABLE2/ReUseAJAX",
                    data: {mode: "getUserList"},
                    async: false
                }).done(function (result) {

                    for (var i = 0; i < result.length; i++) {
                        if (result[i].menuset !== "RESIGN" && result[i].menuset !== "EARLY") {
                            var o = new Option(result[i].uid + " : " + result[i].name, result[i].uid);
                            $("#modal_uid_to").append(o);
                        }
                    }

                }).fail(function (jqXHR, textStatus, errorThrown) {
                    // needs to implement if it fails
                });

            }

            function confCopyMenu() {
                let uidFrom = $('#modal_uid_from').val();
                let uidTo = $('#modal_uid_to').val();
                let whoEdit = sessionStorage.getItem('uid');

                //check menu uidTo is Empty [QPGMNU]
                $.ajax({
                    url: "/TABLE2/ReUseAJAX",
                    data: {mode: "getMenuByUID", uid: uidTo},
                    async: false
                }).done(function (result) {

                    if (result.length > 0) {  //Not --> Modal
                        $('#modalAlrt').modal('show');
                    } else { //Yes --> copy

                        $.ajax({
                            url: "/TABLE2/ReUseAJAX",
                            data: {mode: "copyMenuTo", uid: uidFrom, uidto: uidTo, whoEdit: whoEdit},
                            async: false
                        }).done(function (result) {

                            if (result) {
                                $('#modalAlrtSuccess').modal('show');
                            } else {
                                $('#modalAlrtFail').modal('show');
                            }

                        }).fail(function (jqXHR, textStatus, errorThrown) {
                            // needs to implement if it fails
                        });

                    }

                }).fail(function (jqXHR, textStatus, errorThrown) {
                    // needs to implement if it fails
                });

            }

        </script>
        <style>
            input[type=text], select {
                width: 100%;
                padding: 5px 5px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }

            input[type=submit] {
                width: 100%;
                background-color: #00399b;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=submit]:hover {
                background-color: #3973d6;
            }
        </style>
    </head>    
    <body >
        <div id="wrapper">

            <!-- /#sidebar-wrapper -->
            <%@ include file="../fragments/sidebar.jsp" %>

            <!-- Page Content -->
            <!-- nav-head-custom -->
            <%@ include file="../fragments/nav_head.jsp" %>

            <div class="container-fluid">
                <!--<div id="wrapper-top">-->
                <%-- PART 2 --%>
                <!--                    <div class="row">
                                        <div class="col-lg-12">
                                            <div id="set-height" style="height:415px;margin-top:0px;">
                                                <div id="sidebar-wrapper-top" class="">
                                                    <b class="page-header" style="padding-left:5px;font-size:18px;">
                <%-- <hr style="margin:0px;margin-top:10px;margin-bottom:10px;"> --%>
            </b>
        </div>
    </div>
</div>
</div> -->
                <%-- PART 3 --%>
                <!--show Qdest table-->
                <form action="../resources/manual/WMS009.pdf" target="_blank">
                    <table width="100%">
                        <tr>
                            <td width="94%"></td>
                            <td width="6%" align="right"><input type="submit" value="Manual" /></td>
                        </tr>
                    </table>
                </form>
                <div style="border: 2px solid #ccc; border-radius: 5px; padding: 20px;">
                    <table id="showTable" class="display" style="width:100%; border: 1px solid #ccc; border-radius: 4px;">
                        <thead>
                            <tr>
                                <th>User ID</th>
                                <th>Name</th>
                                <th>Level</th>
                                <th>Warehouse</th>
                                <th>Department</th>
                                <th>Menuset</th>
                                <th>Option<a href="create"><i class="glyphicon glyphicon-plus-sign" style="font-size:15px; padding-left: 10px"></i></a></th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>User ID</th>
                                <th>Name</th>
                                <th>Level</th>
                                <th>Warehouse</th>
                                <th>Department</th>
                                <th>Menuset</th>
                                <th></th>
                            </tr>
                        </tfoot>
                        <tbody>
                            <c:forEach items="${UAList}" var="p" varStatus="i">
                                <tr>
                                    <td>${p.uid}</td>
                                    <td>${p.name}</td>
                                    <td>${p.level}</td>
                                    <td>${p.warehouse}</td>
                                    <td>${p.department}</td>
                                    <td>${p.menuset}</td>
                                    <td>
                                        <a href="edit?uid=${p.uid}" ><i class="glyphicon glyphicon-edit" style="font-size:15px;"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <a href="delete?uid=${p.uid}" ><i class="glyphicon glyphicon-trash" style="font-size:15px;"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <a title="Program" href="ndisplay?uid=${p.uid}"><img src="../resources/images/icons/connection.png" width="20" height="20" ></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <a title="Mat Ctrl" href="nmat?uid=${p.uid}" ><i class="fa fa-male" aria-hidden="true" style=" font-size: 20px;"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <a title="Prod Grp" href="prdgrp?uid=${p.uid}"><img src="../resources/images/icons/brassiere.png" width="20" height="20" ></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <a title="Copy menu"><i class="fa fa-clone" style="cursor: pointer; font-size:15px;" onclick="FNopenmodal('${p.uid}')" aria-hidden="true"></i></a>
                                    </td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>

                    <!-- Modal -->
                    <center style="font-size: 23px;">
                        <div id="modalCopyMenu" class="modal fade" role="dialog">
                            <div class="modal-dialog">
                                <div class="modal-lg">
                                    <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title" style="font-size: 30px;">COPY MENU</h4>
                                        </div>

                                        <div class="modal-body">

                                            <div class="row">
                                                <div class="col-sm-1" align="right" ></div>
                                                <div class="col-sm-2" align="right" ><label style="font-size:16px;">FROM : </label></div>
                                                <div class="col-sm-3" align="right" >
                                                    <input id="modal_uid_from" class="form-control" readonly>
                                                </div>
                                                <div class="col-sm-2" align="right" ><label style="font-size:16px;">TO : </label></div>
                                                <div class="col-sm-3" align="right" >
                                                    <select id="modal_uid_to" style="margin: 0;" class="form-control"></select>
                                                </div>
                                                <div class="col-sm-1" align="right" ></div>
                                            </div>
                                        </div>

                                        <div class="modal-footer" style="text-align : center">
                                            <input type="button" style="font-size: 18px;" data-dismiss="modal" class="btn btn-danger" value="Cancel">
                                            &nbsp;
                                            <input type="button" style="font-size: 18px;" data-dismiss="modal" class="btn btn-success" onclick="confCopyMenu()" value="Confirm">
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </center>

                    <!-- Modal -->
                    <center style="font-size: 23px;">
                        <div id="modalAlrt" class="modal fade" role="dialog">
                            <div class="modal-dialog">
                                <div class="modal-lg">
                                    <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title" style="font-size: 30px;">Error !!</h4>
                                        </div>

                                        <div class="modal-body">
                                            <label>User ID ผูกเมนูไว้แล้ว ไม่สามารถ Copy ได้.</label>
                                        </div>

                                        <div class="modal-footer" style="text-align : center">
                                            <input type="button" style="font-size: 18px;" data-dismiss="modal" class="btn btn-success"  value="OK">
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </center>

                    <!-- Modal -->
                    <center style="font-size: 23px;">
                        <div id="modalAlrtSuccess" class="modal fade" role="dialog">
                            <div class="modal-dialog">
                                <div class="modal-lg">
                                    <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title" style="font-size: 30px;">Success !!</h4>
                                        </div>

                                        <div class="modal-body">
                                            <label>Copy Menu Success.</label>
                                        </div>

                                        <div class="modal-footer" style="text-align : center">
                                            <input type="button" style="font-size: 18px;" data-dismiss="modal" class="btn btn-success"  value="OK">
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </center>

                    <!-- Modal -->
                    <center style="font-size: 23px;">
                        <div id="modalAlrtFail" class="modal fade" role="dialog">
                            <div class="modal-dialog">
                                <div class="modal-lg">
                                    <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title" style="font-size: 30px;">Error !!</h4>
                                        </div>

                                        <div class="modal-body">
                                            <label>Copy Menu Failed.</label>
                                        </div>

                                        <div class="modal-footer" style="text-align : center">
                                            <input type="button" style="font-size: 18px;" data-dismiss="modal" class="btn btn-success"  value="OK">
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </center>

                </div>
                <!--End Part 3-->
                <br>
                <!--</div>  end #wrapper-top--> 
            </div> <!-- end .container-fluid -->
        </div> <!-- end #wrapper -->
    </body>
</html>