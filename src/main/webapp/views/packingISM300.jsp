<%@ include file="../includes/taglibs.jsp" %>
<%@ include file="../includes/imports.jsp" %>
<!DOCTYPE html>
<html lang="en">
    <%@ include file="../includes/head.jsp" %>

    <body>

        <div id="wrapper">

            <!-- /#sidebar-wrapper -->
            <%@ include file="../includes/slidebar.jsp" %>

            <!-- Page Content -->
            <!-- nav-head-custom -->
            <%@ include file="../includes/MSS_head.jsp" %>
            <div class="container-fluid">
                <div id="wrapper-top">
                    <div class="row">
                        <div class="col-lg-12">
                            <div id="set-height" style="height:265px;margin-top:40px;">
                                <div id="sidebar-wrapper-top">
                                    <b class="page-header" style="padding-left:5px;font-size:18px;">
                                        <table>
                                            <tr>
                                                <td width="130px;">
                                                    Customer
                                                </td>
                                                <td width="20px;" align="center"> : </td>
                                                <td>
                                                    <input disabled id="sPOFG" value="${POFG}" class="form-control"
                                                           type="text"
                                                           onkeyup="javascript:this.value = this.value.toUpperCase();getCustomerName();getDropDownGroup();getDropDownSQ();">
                                                </td>
                                                <td>
                                                    <div id="customerName"
                                                         style="margin-left:20px;color:#777;font-size:16px;"></div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Mat-Ctrl
                                                </td>
                                                <td width="20px;" align="center"> : </td>
                                                <td>
                                                    <input disabled id="sLGPBE" value="${LGPBE}" class="form-control"
                                                           style="margin-top:10px;" type="text"
                                                           onkeyup="javascript:this.value = this.value.toUpperCase();getMatName();">
                                                </td>
                                                <td>
                                                    <div id="MatName" style="margin-left:20px;color:#777;font-size:16px;">
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Job no.
                                                </td>
                                                <td width="20px;" align="center"> : </td>
                                                <td>
                                                    <div id="dpGRPNO" style="margin-top:10px;">
                                                        <input class="form-control" type="text" disabled>
                                                    </div>
                                                </td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Sequence no.
                                                </td>
                                                <td width="20px;" align="center"> : </td>
                                                <td>
                                                    <div id="dpSEQNO" style="margin-top:10px;">
                                                        <input class="form-control" type="text" disabled>
                                                    </div>
                                                </td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Material
                                                </td>
                                                <td width="20px;" align="center"> : </td>
                                                <td height="34px;">
                                                    <input disabled style="margin-top:10px;" id="sMATNR" value="${MATNR}"
                                                           class="form-control" type="text">
                                                </td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td></td>
                                                <td>
                                                    <!-- <button onclick="search()" style="width:100%;margin-top:10px;" class="btn btn-outline btn-primary">
                                                        <b><i class="fa fa-search icon-size-sm"></i> Search</b>
                                                    </button> -->
                                                </td>
                                                <td></td>
                                            </tr>
                                        </table>
                                    </b>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="toggle-top" style="position:relative;">
                        <a style="position:absolute;right:0 ;top:-40px;width:15%;z-index: 900;" href="#menu-toggle2"
                           class="btn btn-outline btn-default  custom-toggle-menu" id="menu-toggle2">
                            <div id="i-toggle-top"></div>
                        </a>
                        <hr style="margin:0px;margin-top:10px;margin-bottom:10px;">
                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <div style="float:left;width:30%;padding-right:20px;">
                                <table style="font-size:1.8em;font-weight: bold;width:100%;">
                                    <tr>
                                        <td style="color:#226097;">Total Request</td>
                                    </tr>
                                    <tr>
                                        <td style="text-align:right;">
                                            <fmt:formatNumber type="number" pattern="###,##0.000" value="${TOPREQ}" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="color:#226097;">Total Pick up</td>
                                    </tr>
                                    <tr>
                                        <td style="text-align:right;">
                                            <fmt:formatNumber type="number" pattern="###,##0.000" value="${ISSUETOPICK}" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="color:#226097;">Total Packing</td>
                                    </tr>
                                    <tr>
                                        <td style="text-align:right;">
                                            <fmt:formatNumber type="number" pattern="###,##0.000" value="${ISSUETOPACK}" />
                                            <hr class="custom-hr">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align:right;color:red;">
                                            <fmt:formatNumber type="number" pattern="###,##0.000" value="${ISSUETODIFF}" />
                                            <hr class="custom-hr">
                                            <hr class="custom-hr">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <button onclick="print('')" type="button"
                                                    style="width:100%;font-size:0.9em;font-weight:bold;"
                                                    class="btn btn-outline btn-warning" title=" Print Stricker Distribution"><i
                                                    class="fa fa-print"></i> Print Distribute</button>

                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <button onclick="closeWin()" type="button"
                                                    style="width:100%;font-size:0.9em;font-weight:bold;margin-top:10px;"
                                                    class="btn btn-outline btn-danger"><i class="fa fa-reply"></i> Exit</button>
                                        </td>
                                    <script type="text/javascript">

                                        function closeWin() {
//                                            close();
                                            window.location = "MC?a=list&POFG=${POFG}&LGPBE=${LGPBE}&GRPNO=${GRPNO}&ISSUENO=${ISSUENO}";
                                        }

                                    </script>
                                    </tr>
                                </table>
                            </div>
                            <div style="float:left;width:70%">
                                <div id="packing" class="panel panel-primary" style="min-height: 485px;position:relative">
                                    <table class="pull-right"
                                           style="position:absolute;top:5.5px;right:10px;font-size:14px;">
                                        <tr>
                                            <td>
                                                <button onclick="distribute()" style="margin:0px;"
                                                        class="btn btn-default btn-outline btn-sm custom-icon-w-b">
                                                    <b style="color:white"><i class="fa fa-refresh icon-size-sm"></i>
                                                        Distribute</b>
                                                </button>
                                                <script type="text/javascript">
                                                    function distribute() {
                                                        $('#packing').loader('show', '<i style="font-size:100px;" class="fa fa-refresh fa-spin"></i>');
                                                        $.get('../ISM300AJAX', {a: 'packdetail', GRPNO: '${GRPNO}', ISSUENO: '${ISSUENO}', MATNR: '${MATNR}'}, function (responseText) {
                                                            window.location = "MC?Topreq=${TOPREQ}&p=2&m=show&a=list&POFG=${POFG}&LGPBE=${LGPBE}&GRPNO=${GRPNO}&ISSUENO=${ISSUENO}&MATNR=${MATNR}";
                                                        });
                                                    }
                                                </script>
                                            </td>
                                            <td>
                                                <button onclick="callModal()" style="margin:0px;margin-left:10px;"
                                                        class="btn btn-default btn-outline btn-sm custom-icon-w-b">
                                                    <b style="color:white"><i class="fa fa-eye icon-size-sm"> </i> View</b>
                                                </button>
                                                <script type="text/javascript">
                                                    function callModal() {
                                                        window.location = "MC?Topreq=${TOPREQ}&p=2&m=show&a=list&POFG=${POFG}&LGPBE=${LGPBE}&GRPNO=${GRPNO}&ISSUENO=${ISSUENO}&MATNR=${MATNR}";
                                                    }
                                                </script>
                                            </td>
                                        </tr>
                                    </table>
                                    <div class="panel-heading">
                                        <h4 style="margin:0px;">Materials Packing</h4>
                                    </div>
                                    <!-- scan data  -->
                                    <b class="page-header" style="padding-left:10px;font-size:18px;">
                                        <table width="99%">
                                            <td width="100px;" align="center" style="margin-top:2px;padding-top: 10px;">
                                                <p>Scan :
                                            </td>
                                            <td style="padding-top: 30px;">
                                                <input id="ipScan" class="form-control"
                                                       style="margin-top:2px;border-width: 0px 0px 1px 0px;width: 100%;height: 40px;"
                                                       type="text">
                                            </td>
                                        </table>
                                    </b>

                                    <!-- /.panel-heading -->
                                    <div class="panel-body">
                                        <div class="dataTable_wrapper">
                                            <table class="table table-striped table-bordered table-hover"
                                                   id="dataTables-example">
                                                <thead>
                                                    <tr>
                                                        <th style="border-bottom:0px;">No</th>
                                                        <th style="border-bottom:0px;">Type</th>
                                                        <th style="border-bottom:0px;">Unit / ID</th>
                                                        <th style="border-bottom:0px;">@</th>
                                                        <th style="border-bottom:0px;">Action</th>
                                                    </tr>
                                                </thead>

                                                <tfoot>
                                                <th style="border-left:0px;"></th>
                                                <th style="border-left:0px;">
                                                    <select class="form-control input-sm" id="sType" style="width:100%"
                                                            disabled>
                                                        <option value="Box">Box</option>
                                                        <option value="Bag">Bag</option>
                                                        <option value="Roll">Roll</option>
                                                    </select>
                                                </th>
                                                <th style="border-left:0px;width:25%">
                                                    <input id="sUnit"
                                                           style="margin:0px;text-align:right;font-size:15px;width:100%;"
                                                           class="form-control input-sm" type="number" disabled>
                                                </th>
                                                <th style="border-left:0px;width:25%">
                                                    <input id="sValue"
                                                           style="margin:0px;text-align:right;font-size:15px;width:100%;"
                                                           class="form-control input-sm" type="number" disabled>
                                                </th>
                                                <th style="border-left:0px;text-align:center;width:20%">
                                                    <button onclick="add()" style="margin-right:10px;" type="button"
                                                            class="btn btn btn-outline btn-success btn-sm btn-size-sm"
                                                            disabled><i class="fa fa-save icon-size-sm"></i></button>

                                                    <button onclick="clearList()" type="button"
                                                            class="btn btn btn-outline btn-danger btn-sm btn-size-sm"
                                                            title="Delete All"><i class="fa fa-trash-o icon-size-sm"></i>
                                                        <i class="fa fa-th-list icon-size-sm"></i></button>


                                                    <script type="text/javascript">

                                                        function add() {
                                                            var Type = $('#sType').val();
                                                            var Unit = $('#sUnit').val();
                                                            var Value = $('#sValue').val();

                                                            if (Unit != "" && Value != "") {
                                                                if (parseFloat((parseFloat(Unit) * parseFloat(Value)).toFixed(2)) <= parseFloat(parseFloat('${ISSUETODIFF}').toFixed(2))) {
                                                                    $('#packing').loader('show', '<i style="font-size:100px;" class="fa fa-refresh fa-spin"></i>');
                                                                    $.get('../ISM300AJAX', {a: 'add', GRPNO: '${GRPNO}', ISSUENO: '${ISSUENO}', MATNR: '${MATNR}', TYPE: Type, UNIT: Unit, VALUE: Value}, function (responseText) {
                                                                        location.reload();
                                                                    });
                                                                } else {
                                                                    alertify.alert("<b style='color:red;font-size:18px;'> Don't collect more than what you have been ordered to ! </b>");
                                                                }

                                                            } else {
                                                                alertify.error("<b>Please fill out Pack from.</b>");
                                                            }

                                                        }

                                                        function clearList() {
                                                            var LGPBE = $("#sLGPBE").val();
                                                            alertify.confirm("<b class='head-custom' style='font-size:18px;'> Do you want to clear this data. </b>", function (e) {
                                                                if (e) {
                                                                    //after clicking OK
                                                                    $('#packing').loader('show', '<i style="font-size:100px;" class="fa fa-refresh fa-spin"></i>');
                                                                    $.get('../ISM300AJAX', {a: 'delete', GRPNO: '${GRPNO}', ISSUENO: '${ISSUENO}', MATNR: '${MATNR}', LGPBE: LGPBE}, function (responseText) {
                                                                        location.reload();
                                                                    });
                                                                } else {
                                                                    //after clicking Cancel
                                                                }
                                                            });

                                                        }


                                                    </script>
                                                </th>
                                                </tfoot>

                                                <tbody>
                                                    <c:forEach items="${list}" var="MPDATAP">
                                                        <tr>
                                                            <td style="padding-top:14px;text-align:center;">
                                                                <c:out value="${MPDATAP.NO}" escapeXml="false" />
                                                            </td>
                                                            <td style="padding-top:14px;">
                                                                <c:out value="${MPDATAP.TYPE}" escapeXml="false" />
                                                            </td>
                                                            <td align="right" style="padding-top:14px;min-width:90px;">1
                                                                &nbsp;&nbsp;/&nbsp;&nbsp;
                                                                ${MPDATAP.MATID}</td>
                                                            <td align="right" style="padding-top:14px;min-width:90px;">
                                                                <fmt:formatNumber type="number" pattern="###,##0.000"
                                                                                  value="${MPDATAP.VALUE}" />
                                                            </td>
                                                            <td align="center" style="min-width:100px;">

                                                                <button onclick="print('${MPDATAP.NO}')"
                                                                        style="width:100px;" type="button"
                                                                        class="btn btn btn-outline btn-warning btn-sm btn-size-sm"><i
                                                                        class="fa fa-print icon-size-sm"></i> <i
                                                                        style="margin-left:6px;"
                                                                        class="fa fa-tag icon-size-sm"></i></button>

                                                                <button
                                                                    onclick="clearListline('${MPDATAP.NO}', '${MPDATAP.MATID}', '${MPDATAP.VALUE}')"
                                                                    type="button"
                                                                    class="btn btn btn-outline btn-danger btn-sm btn-size-sm"
                                                                    title="Delete Line No. ${MPDATAP.NO}"
                                                                    id="DelLine${MPDATAP.MATID}"
                                                                    value="${MPDATAP.NO}|${MPDATAP.MATID}|${MPDATAP.VALUE}"><i
                                                                        class="fa fa-trash-o icon-size-sm"></i>
                                                                    <i class="fa fa-th-list icon-size-sm"></i></button>



                                                            </td>
                                                        </tr>
                                                    </c:forEach>


                                                <script type="text/javascript">

                                                    function clearListline(lineno, itemid, qtyidno) {

                                                        var LGPBE = $("#sLGPBE").val();

                                                        // window.alert($("#DelLine${MPDATAP.MATID}").val());
                                                        // window.alert(lineno + "=" + itemid + "=" + qtyidno);

                                                        alertify.confirm("<b class='head-custom' style='font-size:18px;'> Do you want to clear data. ID No." + itemid + " </b>", function (e) {
                                                            if (e) {
                                                                //after clicking OK  '${MPDATAP.NO}','${MPDATAP.MATID}','${MPDATAP.VALUE}'
                                                                // var datadelline = $("#DelLine${MPDATAP.MATID}").val().split("|");
                                                                // var datadelline =
                                                                //var lineno = lineno;   // datadelline[0];  // '${MPDATAP.NO}';
                                                                //var itemid = itemid;   // datadelline[1];
                                                                // var qtyidno = qtyidno;  // datadelline[2];
                                                                // window.alert("ok" + itemid + " " + lineno + " " + qtyidno);

                                                                $('#packing').loader('show', '<i style="font-size:100px;" class="fa fa-refresh fa-spin"></i>');
                                                                $.get('../ISM300AJAX', {a: 'deleteline', GRPNO: '${GRPNO}', ISSUENO: '${ISSUENO}', LINENO: lineno, QTYID: qtyidno, MATNR: '${MATNR}', MATNRid: itemid, LGPBE: LGPBE}, function (responseText) {
                                                                    location.reload();
                                                                });
                                                            } else {
                                                                //after clicking Cancel
                                                            }
                                                        });

                                                    }
                                                </script>




                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <style type="text/css">
                .custom-hr {
                    height: 2px;
                    background-color: #337ab7;
                    border: none
                }
            </style>

            <script>

                var sidebarVisible2 = localStorage.getItem('wrapper-top');
                var checkToggleTop = $("#i-toggle-top").html();
                var noBox;

                $(document).ready(function () {

                    var modal = "${modal}";

                    if (modal == "show") {
                        $('#myModal').modal('show');
                    }

                    $("#wrapper").css({"-webkit-transition": "all 0.0s ease"}, {"-moz-transition": "all 0.0s ease"}, {"-o-transition": "all 0.0s ease"}, {"transition": "all 0.0s ease"});

                    $("#sidebar-wrapper").css({"-webkit-transition": "all 0.0s ease"}, {"-moz-transition": "all 0.0s ease"}, {"-o-transition": "all 0.0s ease"}, {"transition": "all 0.0s ease"});

                    $("#menu-toggle").css({"display": "none"});

                    $("#mHead").css({"margin-top": "11px"});

                    $("#wrapper").toggleClass("toggled");

                    toggleClick2();

                    toggleLoad2();

                    getTime();

                    getCustomerName();

                    getMatName();

                    getDropDownGroup();

                    getDropDownSQ();

                    if ("${CHECK}" == "1") {
                        $('#sCHECK').prop('checked', true);
                    }

                    setInterval(function () {
                        getTime();
                    }, 1000);

                    $('#dataTables-example').DataTable({
                        "aLengthMenu": [[5, 20, 50, -1], [5, 20, 50, "All"]],
                        "iDisplayLength": -1,
                        "bStateSave": true,
                        "aoColumnDefs": [
                            {'bSortable': false, 'aTargets': [2]},
                            {'bSortable': false, 'aTargets': [4]}
                        ],

                        responsive: true
                    });

                });

                function toggleLoad2() {
                    if (sidebarVisible2 != null && sidebarVisible2 == 1) {
                        $("#wrapper-top").css({"-webkit-transition": "all 0.0s ease"}, {"-moz-transition": "all 0.0s ease"}, {"-o-transition": "all 0.0s ease"}, {"transition": "all 0.0s ease"});
                        $("#sidebar-wrapper-top").css({"-webkit-transition": "all 0.0s ease"}, {"-moz-transition": "all 0.0s ease"}, {"-o-transition": "all 0.0s ease"}, {"transition": "all 0.0s ease"});
                        $("#wrapper-top").toggleClass("toggled");
                        $("#i-toggle-top").html("<b class=\"custom-text\">Show content</b>");
                        // $("#set-height").css({"margin-top":"50px"});
                    } else {
                        $("#i-toggle-top").html("<b class=\"custom-text\">Hide content</b>");
                    }
                }

                function toggleClick2() {
                    $("#menu-toggle2").click(function (e) {

                        e.preventDefault();
                        $("#wrapper-top").toggleClass("toggled");

                        if ($("#i-toggle-top").html() == "<b class=\"custom-text\">Show content</b>") {
                            $("#wrapper-top").css({"-webkit-transition": "all 0.5s ease"}, {"-moz-transition": "all 0.5s ease"}, {"-o-transition": "all 0.5s ease"}, {"transition": "all 0.5s ease"});
                            $("#sidebar-wrapper-top").css({"-webkit-transition": "all 0.5s ease"}, {"-moz-transition": "all 0.5s ease"}, {"-o-transition": "all 0.5s ease"}, {"transition": "all 0.5s ease"});
                            $("#i-toggle-top").html("<b class=\"custom-text\">Hide content</b>");
                            // $("#toggle-top").css({"margin-top":"0px"});
                            localStorage.setItem('wrapper-top', 0);
                        } else {
                            $("#i-toggle-top").html("<b class=\"custom-text\">Show content</b>");
                            // $("#toggle-top").css({"margin-top":"50px"});
                            localStorage.setItem('wrapper-top', 1);
                        }

                    });
                }


                function getCustomerName() {
                    var POFG = $("#sPOFG").val();
                    $.get('../ISM300AJAX', {a: 'getCustomerName', POFG: POFG}, function (responseText) {
                        $('#customerName').html(responseText);
                    });
                }

                function getMatName() {
                    var LGPBE = $("#sLGPBE").val();
                    $.get('../ISM300AJAX', {a: 'getMatName', LGPBE: LGPBE}, function (responseText) {
                        $('#MatName').html(responseText);
                    });
                }

                function getDropDownGroup() {
                    var POFG = $("#sPOFG").val();
                    $.get('../ISM300AJAX', {a: 'getDropDownGroup', POFG: POFG}, function (responseText) {
                        if (responseText != "null") {
                            $('#dpGRPNO').html(responseText);
                            $('#sGRPNO').val("${GRPNO}");
                            $('#sGRPNO').attr('disabled', 'disabled');
                        } else {
                            $('#dpGRPNO').html("<input class=\"form-control\" type=\"text\" disabled>");
                        }
                    });
                }

                function getDropDownSQ() {
                    var POFG = $("#sPOFG").val();
                    $.get('../ISM300AJAX', {a: 'getDropDownSQ', POFG: POFG}, function (responseText) {
                        if (responseText != "null") {
                            $('#dpSEQNO').html(responseText);
                            $('#sISSUENO').val("${ISSUENO}");
                            $('#sISSUENO').attr('disabled', 'disabled');
                        } else {
                            $('#dpSEQNO').html("<input class=\"form-control\" type=\"text\" disabled>");
                        }
                    });
                }

                function search() {

                    var POFG = $('#sPOFG').val();
                    var LGPBE = $("#sLGPBE").val();
                    var GRPNO = $("#sGRPNO").val();
                    var ISSUENO = $("#sISSUENO").val();

                    if (POFG == "" || LGPBE == "" || GRPNO == "undefined" || ISSUENO == "undefined") {
                        alertify.error("Please fill out search form.");
                    } else {
                        window.location = "MC?Topreq=${TOPREQ}&a=list&POFG=" + POFG + "&LGPBE=" + LGPBE + "&GRPNO=" + GRPNO + "&ISSUENO=" + ISSUENO;
                    }

                }

                function closeModal() {
                    window.location = "MC?Topreq=${TOPREQ}&p=2&a=list&POFG=${POFG}&LGPBE=${LGPBE}&GRPNO=${GRPNO}&ISSUENO=${ISSUENO}&MATNR=${MATNR}";
                }

                function print(no) {
                    noBox = no;
                    $('#PrintModal').modal('show');
                    $('#gDate').val(localStorage.getItem('sDate'));
                }

            </script>

    </body>

</html>

<div id="myModal" class="modal fade" tabindex="-1" data-backdrop="static" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" onclick="closeModal()" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Distribute detail</h4>
            </div>
            <div class="modal-body">
                <table class="table table-striped table-bordered table-hover" style="margin-bottom:0px;">
                    <thead>
                        <tr>
                            <th style="border-bottom:0px;width:15%">Sale Order</th>
                            <th style="border-bottom:0px;width:10%">Style</th>
                            <th style="border-bottom:0px;width:10%">Color</th>
                            <th style="border-bottom:0px;width:10%">Lot</th>
                            <th style="border-bottom:0px;width:15%">Req</th>
                            <th style="border-bottom:0px;width:10%">No</th>
                            <th style="border-bottom:0px;width:10%">Type</th>
                            <th style="border-bottom:0px;width:15%">Pack</th>
                        </tr>
                    </thead>
                </table>
                <div id="custom-table-detail" style="height:350px;overflow:auto;">
                    <table class="table table-striped table-bordered table-hover" style="margin-bottom:0px;">
                        <tbody>
                            <c:set var="tempVBELN" value="0" />
                            <c:set var="total1" value="0" />
                            <c:set var="total2" value="0" />
                            <c:set var="gTotal1" value="0" />
                            <c:set var="gTotal2" value="0" />
                            <c:set var="count" value="0" />
                            <c:forEach items="${listDetail}" var="MPDATAP">
                                <c:set var="gTotal2" value="${gTotal2 + MPDATAP.VALUE}" />

                                <c:choose>
                                    <c:when test="${tempVBELN == 0}">
                                        <c:set var="tempVBELN" value="${MPDATAP.VBELN}" />
                                        <c:set var="total1" value="${MPDATAP.KWMENG}" />
                                        <c:set var="total2" value="${MPDATAP.VALUE}" />
                                        <c:set var="gTotal1" value="${gTotal1 + total1}" />

                                        <tr>
                                            <td style="width:15%" align="center">
                                                <c:out value="${MPDATAP.VBELN}" escapeXml="false" />
                                            </td>
                                            <td style="width:10%">
                                                <c:out value="${MPDATAP.STYLE}" escapeXml="false" />
                                            </td>
                                            <td style="width:10%">
                                                <c:out value="${MPDATAP.COLOR}" escapeXml="false" />
                                            </td>
                                            <td style="width:10%" align="center">
                                                <c:out value="${MPDATAP.LOT}" escapeXml="false" />
                                            </td>
                                            <td style="width:15%" align="right">
                                                <fmt:formatNumber type="number" pattern="###,##0.000"
                                                                  value="${MPDATAP.KWMENG}" />
                                            </td>
                                            <td style="width:10%" align="center">
                                                <c:out value="${MPDATAP.NO}" escapeXml="false" />
                                            </td>
                                            <td style="width:10%">
                                                <c:out value="${MPDATAP.TYPE}" escapeXml="false" />
                                            </td>
                                            <td style="width:15%" align="right">
                                                <fmt:formatNumber type="number" pattern="###,##0.000"
                                                                  value="${MPDATAP.VALUE}" />
                                            </td>
                                        </tr>
                                    </c:when>
                                    <c:when test="${tempVBELN == MPDATAP.VBELN}">
                                        <c:set var="total2" value="${total2+MPDATAP.VALUE}" />
                                        <tr>
                                            <td style="width:15%" align="center"></td>
                                            <td style="width:10%"></td>
                                            <td style="width:10%"></td>
                                            <td style="width:10%" align="center"></td>
                                            <td style="width:15%" align="right"></td>
                                            <td style="width:10%" align="center">
                                                <c:out value="${MPDATAP.NO}" escapeXml="false" />
                                            </td>
                                            <td style="width:10%">
                                                <c:out value="${MPDATAP.TYPE}" escapeXml="false" />
                                            </td>
                                            <td style="width:15%" align="right">
                                                <fmt:formatNumber type="number" pattern="###,##0.000"
                                                                  value="${MPDATAP.VALUE}" />
                                            </td>
                                        </tr>
                                    </c:when>
                                    <c:otherwise>
                                        <c:set var="tempVBELN" value="${MPDATAP.VBELN}" />
                                        <tr>
                                            <td style="width:15%" align="center"></td>
                                            <td style="width:10%"></td>
                                            <td style="width:10%"></td>
                                            <td style="width:10%"><b>To Req.<b></td>
                                                        <td style="width:15%" align="right"><b>
                                                                <fmt:formatNumber type="number" pattern="###,##0.000"
                                                                                  value="${total1}" /></b></td>
                                                        <td style="width:10%" align="center"></td>
                                                        <td style="width:10%"><b>To. Pack.</b></td>
                                                        <td style="width:15%" align="right"><b>
                                                                <fmt:formatNumber type="number" pattern="###,##0.000"
                                                                                  value="${total2}" /></b></td>
                                                        </tr>

                                                        <c:set var="total1" value="${MPDATAP.KWMENG}" />
                                                        <c:set var="gTotal1" value="${gTotal1 + total1}" />
                                                        <c:set var="total2" value="${MPDATAP.VALUE}" />

                                                        <tr>
                                                            <td style="width:15%" align="center">
                                                                <c:out value="${MPDATAP.VBELN}" escapeXml="false" />
                                                            </td>
                                                            <td style="width:10%">
                                                                <c:out value="${MPDATAP.STYLE}" escapeXml="false" />
                                                            </td>
                                                            <td style="width:10%">
                                                                <c:out value="${MPDATAP.COLOR}" escapeXml="false" />
                                                            </td>
                                                            <td style="width:10%" align="center">
                                                                <c:out value="${MPDATAP.LOT}" escapeXml="false" />
                                                            </td>
                                                            <td style="width:15%" align="right">
                                                                <fmt:formatNumber type="number" pattern="###,##0.000"
                                                                                  value="${MPDATAP.KWMENG}" />
                                                            </td>
                                                            <td style="width:10%" align="center">
                                                                <c:out value="${MPDATAP.NO}" escapeXml="false" />
                                                            </td>
                                                            <td style="width:10%">
                                                                <c:out value="${MPDATAP.TYPE}" escapeXml="false" />
                                                            </td>
                                                            <td style="width:15%" align="right">
                                                                <fmt:formatNumber type="number" pattern="###,##0.000"
                                                                                  value="${MPDATAP.VALUE}" />
                                                            </td>
                                                        </tr>
                                                    </c:otherwise>
                                                </c:choose>
                                                <c:set var="count" value="${count+1}" />
                                                <c:if test="${count == fn:length(listDetail)}">
                                                    <tr>
                                                        <td style="width:15%" align="center"></td>
                                                        <td style="width:10%"></td>
                                                        <td style="width:10%"></td>
                                                        <td style="width:10%"><b>To Req.<b></td>
                                                                    <td style="width:15%" align="right"><b>
                                                                            <fmt:formatNumber type="number" pattern="###,##0.000"
                                                                                              value="${total1}" /></b></td>
                                                                    <td style="width:10%" align="center"></td>
                                                                    <td style="width:10%"><b>To. Pack.</b></td>
                                                                    <td style="width:15%" align="right"><b>
                                                                            <fmt:formatNumber type="number" pattern="###,##0.000"
                                                                                              value="${total2}" /></b></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width:15%" align="center"></td>
                                                                        <td style="width:10%"></td>
                                                                        <td style="width:10%"><b>Grand<b></td>
                                                                                    <td style="width:10%"><b>To Req.<b></td>
                                                                                                <td style="width:15%" align="right"><b>
                                                                                                        <fmt:formatNumber type="number" pattern="###,##0.000"
                                                                                                                          value="${gTotal1}" /></b></td>
                                                                                                <td style="width:10%" align="center"></td>
                                                                                                <td style="width:10%"><b>To. Pack.</b></td>
                                                                                                <td style="width:15%" align="right"><b>
                                                                                                        <fmt:formatNumber type="number" pattern="###,##0.000"
                                                                                                                          value="${gTotal2}" /></b></td>
                                                                                                </tr>
                                                                                            </c:if>
                                                                                        </c:forEach>
                                                                                        </tbody>
                                                                                        </table>
                                                                                        </div>
                                                                                        </div>
                                                                                        <div class="modal-footer">
                                                                                        </div>
                                                                                        </div>
                                                                                        </div>
                                                                                        </div>

                                                                                        <div id="PrintModal" class="modal fade" tabindex="-1" data-backdrop="static" role="dialog">
                                                                                            <div class="modal-dialog smaller" role="document">
                                                                                                <div class="modal-content">
                                                                                                    <div class="modal-header">
                                                                                                        <button type="button" class="close" onclick="closeModal()" aria-label="Close"><span
                                                                                                                aria-hidden="true">&times;</span></button>
                                                                                                        <h4 class="modal-title">Print</h4>
                                                                                                    </div>
                                                                                                    <div class="modal-body">
                                                                                                        <div id="inputDateDiv" style="position:relative;">
                                                                                                            <input id="gDate" class="form-control" type="text" style="width:100%;margin-bottom:20px;">
                                                                                                        </div>
                                                                                                        <script type="text/javascript">
                                                                                                            $(function () {
                                                                                                                $('#gDate').datetimepicker({
                                                                                                                    format: 'DD/MM/YYYY',
                                                                                                                });
                                                                                                            });
                                                                                                        </script>

                                                                                                        <div class="modal-footer" style="padding:0px;">
                                                                                                            <button type="button" onclick="ConfirmPrint()"
                                                                                                                    style="width:100%;font-size:1.5em;font-weight:bold;margin-top:20px;"
                                                                                                                    class="btn btn-outline btn-warning"><i class="fa fa-print"></i> Confirm</button>
                                                                                                            <script type="text/javascript">
                                                                                                                function ConfirmPrint() {
                                                                                                                    var POFG = $('#sPOFG').val();
                                                                                                                    var LGPBE = $("#sLGPBE").val();
                                                                                                                    var GRPNO = $("#sGRPNO").val();
                                                                                                                    var ISSUENO = $("#sISSUENO").val();
                                                                                                                    var MATNR = $("#sMATNR").val();
                                                                                                                    localStorage.setItem('sDate', $('#gDate').val());
                                                                                                                    window.location = "MC?Topreq=${TOPREQ}&p=2&a=list&POFG=${POFG}&LGPBE=${LGPBE}&GRPNO=${GRPNO}&ISSUENO=${ISSUENO}&MATNR=${MATNR}";
                                                                                                                    // window.open("http://10.11.9.142:8080/QRCODE/controller/GetDataCtrl.jsp?a=report&POFG="+POFG+"&LGPBE="+LGPBE+"&GRPNO="+GRPNO+"&ISSUENO="+ISSUENO+"&MATNR="+MATNR+"&NO="+noBox);
                                                                                                                    window.open("http://10.11.9.142:8080/LABEL/controller/GetDataCtrl.jsp?a=report&POFG=" + POFG + "&LGPBE=" + LGPBE + "&GRPNO=" + GRPNO + "&ISSUENO=" + ISSUENO + "&MATNR=" + MATNR + "&type=&NO=" + noBox + "&DATE=" + $('#gDate').val());
                                                                                                                }
                                                                                                            </script>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>

                                                                                            <script>

                                                                                                function add() {
                                                                                                    var Type = $('#sType').val();
                                                                                                    var Unit = $('#sUnit').val();
                                                                                                    var Value = $('#sValue').val();

                                                                                                    if (Unit != "" && Value != "") {
                                                                                                        if (parseFloat((parseFloat(Unit) * parseFloat(Value)).toFixed(2)) <= parseFloat(parseFloat('${ISSUETODIFF}').toFixed(2))) {
                                                                                                            $('#packing').loader('show', '<i style="font-size:100px;" class="fa fa-refresh fa-spin"></i>');
                                                                                                            $.get('../ISM300AJAX', {a: 'add', GRPNO: '${GRPNO}', ISSUENO: '${ISSUENO}', MATNR: '${MATNR}', TYPE: Type, UNIT: Unit, VALUE: Value}, function (responseText) {
                                                                                                                location.reload();
                                                                                                            });
                                                                                                        } else {
                                                                                                            alertify.alert("<b style='color:red;font-size:18px;'> Don't collect more than what you have been ordered to ! </b>");
                                                                                                        }

                                                                                                    } else {
                                                                                                        alertify.error("<b>Please fill out Pack from.</b>");
                                                                                                    }

                                                                                                }

                                                                                                function checkFocus() {
                                                                                                    var vLength = '${fn:length(listIssue)}';
                                                                                                    if (vLength > 0) {
                                                                                                        if ($('#ipScan').is(":focus")) {

                                                                                                        } else {
                                                                                                            $('#ipScan').focus();
                                                                                                        }
                                                                                                    }
                                                                                                }


                                                                                                $(document).ready(function () {
                                                                                                    $('#ipScan').focus();
                                                                                                    //checkFocus();
                                                                                                });

                                                                                                $(document).keypress(function (e) {

                                                                                                    if (e.which == 13) {

                                                                                                        //  function scanerread(th,event){
                                                                                                        //   if(event.keyCode==13){
                                                                                                        var scanstring = $('#ipScan').val();
                                                                                                        //window.alert("stringscan= " + scanstring.length);
                                                                                                        if (scanstring.length > 10) {  //?????�??? ??�?????????????????? ??�??�??? scan????????�??�?????�??? 5

                                                                                                            var GRPNO = "";
                                                                                                            var ISSUENO = "";
                                                                                                            var MTctrl = "";

                                                                                                            var data = null;
                                                                                                            var iditem = ""; // $('#sUnit').val();
                                                                                                            var Type = "";  // $('#sType').val();
                                                                                                            var Unit = ""; //data[1]; // $('#sUnit').val();
                                                                                                            var Value = ""; // $('#sValue').val();
                                                                                                            var itemno = "";
                                                                                                            var typebox = "";

                                                                                                            if (scanstring.indexOf("+") > 0 || (scanstring.substring(0, 5)).indexOf(".") > 0) {   // ?????�????????�??�??� Barcode supplier
                                                                                                                //  alertify.alert("<b style='color:red;font-size:18px;'>" + scanstring + "</b>");
                                                                                                                GRPNO = $("#sGRPNO").val();
                                                                                                                ISSUENO = $("#sISSUENO").val();
                                                                                                                MTctrl = $("#sLGPBE").val();
                                                                                                                if (scanstring.indexOf("+") > 0) {
                                                                                                                    data = $('#ipScan').val().split("+");
                                                                                                                } else if (scanstring.indexOf(".") > 0) {
                                                                                                                    data = $('#ipScan').val().split(".");
                                                                                                                }

                                                                                                                iditem = scanstring; // data[2]; // item ;
                                                                                                                Type = ""; //data[6];  // $('#sType').val();
                                                                                                                Unit = "1"; //data[1]; // $('#sUnit').val();
                                                                                                                Value = data[4]; // $('#sValue').val();
                                                                                                                itemno = data[2];  //item
                                                                                                                typebox = "";

                                                                                                            } else {      /// ?????�????????�??�??� qrcode ??�?????�??????

                                                                                                                GRPNO = $("#sGRPNO").val();
                                                                                                                ISSUENO = $("#sISSUENO").val();
                                                                                                                MTctrl = $("#sLGPBE").val();

                                                                                                                data = $('#ipScan').val().split("|");
                                                                                                                iditem = data[0]; // $('#sUnit').val();
                                                                                                                Type = data[6];  // $('#sType').val();
                                                                                                                Unit = "1"; //data[1]; // $('#sUnit').val();
                                                                                                                Value = data[7]; // $('#sValue').val();
                                                                                                                itemno = data[3];
                                                                                                                typebox = "";
                                                                                                                if (data[13] != null) {   // check have a type box
                                                                                                                    typebox = data[13];
                                                                                                                }
                                                                                                                // window.alert(Unit + "=" + Value + "=" + itemno);
                                                                                                            }  // if check indexof

                                                                                                            if (Unit != "" && Value != "" && itemno == "${MATNR}") {
                                                                                                                // window.alert(itemno);
                                                                                                                // if (parseFloat((parseFloat(Unit)*parseFloat(Value)).toFixed(2)) <= parseFloat(parseFloat('${ISSUETODIFF}').toFixed(2))) {
                                                                                                                $('#packing').loader('show', '<i style="font-size:100px;" class="fa fa-refresh fa-spin"></i>');
                                                                                                                $.get('../ISM300AJAX', {a: 'add', GRPNO: '${GRPNO}', ISSUENO: '${ISSUENO}', MATNR: '${MATNR}', TYPE: Type, UNIT: Unit, VALUE: Value, MTCTRL: MTctrl, IDITEM: iditem, TYPBOX: typebox}, function (responseText) {


                                                                                                                    if (responseText != "null") {
                                                                                                                        // alertify.error("<b> Item number already exists /or" +responseText+" </b>");
                                                                                                                        alertify.confirm("<b> Item number already exists / " + responseText + " </b>",
                                                                                                                                function () {
                                                                                                                                    alertify.success('Ok');
                                                                                                                                    location.reload();
                                                                                                                                });
                                                                                                                        //alert(responseText);

                                                                                                                    } else {
                                                                                                                        location.reload();
                                                                                                                    }

                                                                                                                });
                                                                                                                // } else {
                                                                                                                //     alertify.alert("<b style='color:red;font-size:18px;'> Don't collect more than what you have been ordered to ! </b>");
                                                                                                                //     }

                                                                                                            } else {
                                                                                                                alertify.error("<b>Please fill out Pack from.</b>");
                                                                                                            }

                                                                                                        } // check length>10

                                                                                                    } //if check ascii=13

                                                                                                });
                                                                                            </script>