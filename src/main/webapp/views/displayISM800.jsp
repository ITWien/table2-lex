<%-- 
    Document   : displayISM800
    Created on : May 10, 2021, 9:49:30 AM
    Author     : 93176
--%>

<%@ include file="../fragments/taglibs.jsp" %>
<%@ include file="../fragments/imports.jsp" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.sql.*;" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>ISM</title>
        <!-- css :: vendors -->
        <jsp:include page = "../fragments/css.jsp" />
        <!-- additional custom :: my-style -->
        <link rel="stylesheet" href="../resources/assets/styles/myStyles.css">

        <!-- js :: vendors -->
        <jsp:include page = "../fragments/script.jsp" />
        <!-- additional custom :: my-script -->
        <script src="../resources/assets/scripts/myScripts.js" async></script>
        <script src="../resources/assets/scripts/toggleLoad.js" async></script>
        <style>
            input[type=text], select{
                width: 100%;
                padding: 5px 5px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }

            input[type=date], input[type=number]{
                /*width: 100%;*/
                padding: 5px 5px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }

            input[type=submit] {
                width: 100%;
                background-color: #00399b;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=submit]:hover {
                background-color: #3973d6;
            }

            input[type=button] {
                width: 100%;
                background-color: #ef604a;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=button]:hover {
                background-color: #e51e00;
            }
        </style>
        <script>
            $(document).ready(function () {

            ${ALRT}

//                sessionStorage.setItem('uid', '93176');

                var delV = sessionStorage.getItem("DelV");
                if (delV !== null) {
                    var NoDel = sessionStorage.getItem("NoDel");
                    if (delV) {
                        alertify.success("Delete No : " + NoDel + " Success");
                        sessionStorage.removeItem("DelV");
                        sessionStorage.removeItem("NoDel");
                    } else {
                        alertify.error("Delete No : " + NoDel + " Failed");
                        sessionStorage.removeItem("DelV");
                        sessionStorage.removeItem("NoDel");
                    }
                }

                $('#showTable').DataTable({
//                    "paging": false,
                    "lengthMenu": [[10, 25, 50, 100], [10, 25, 50, 100]],
                    "bSortClasses": false,
                    "order": [[0, "desc"]],
                    "columnDefs": [
//                        {orderable: false, targets: [12]},
//                        {"width": "15%", "targets": 12},
                        {"width": "5%", "targets": 0},
                        {"width": "5%", "targets": 1},
                        {"width": "10%", "targets": 2},
                        {"width": "5%", "targets": 3},
                        {"width": "5%", "targets": 4},
                        {"width": "5%", "targets": 5}
//                        {"width": "10%", "targets": 6}
                    ]
                });

                // Setup - add a text input to each footer cell
                $('#showTable tfoot th').not(":eq(5)").each(function () {
                    var title = $(this).text();
                    $(this).html('<input type="text" />');
                });

                // DataTable
                var table = $('#showTable').DataTable();

                // Apply the search
                table.columns().every(function () {
                    var that = this;
                    $('input', this.footer()).on('keyup change', function () {
                        if (that.search() !== this.value) {
                            that
                                    .search(this.value)
                                    .draw();
                        }
                    });
                });
            });

            function Delfunc(val) {
                $("#myModal").modal("show");
                $('#modaltextTAG').text(val);
            }

            function Delit() {
                console.log("Conf Del");
                var no = $('#modaltextTAG').text();

                let xhDEL = "";
                xhDEL = $.ajax({
                    url: "/TABLE2/ISM800GET",
                    type: "GET",
                    data: {mode: "DelDownload", no: no},
                    async: false
                }).responseJSON;

                sessionStorage.setItem("DelV", xhDEL);
                sessionStorage.setItem("NoDel", no);

//                if (xhDEL) { //true
//                    var text = "Delete No : " + no + " Success";
//                    alertify.success(text);
                location.reload();
//                } else {
//                    var text = "Delete No : " + no + " Failed";
//                    alertify.success(text);
//                    location.reload();
//                }
            }

        </script>
        <style>
            body {
                font-family: Arial, Helvetica, sans-serif;
            }

            /* The Modal (background) */
            .modal {
                display: none; /* Hidden by default */
                position: fixed; /* Stay in place */
                z-index: 1; /* Sit on top */
                padding-top: 100px; /* Location of the box */
                left: 0;
                top: 0;
                width: 100%; /* Full width */
                height: 100%; /* Full height */
                overflow: auto; /* Enable scroll if needed */
                background-color: rgb(0,0,0); /* Fallback color */
                background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
            }

            /* Modal Content */
            .modal-content {
                background-color: #fefefe;
                margin: auto;
                padding: 20px;
                border: 1px solid #888;
                width: fit-content;
                height: 320px;
            }

            /* The Close Button */
            .close {
                color: #aaaaaa;
                float: right;
                font-size: 28px;
                font-weight: bold;
            }

            .close:hover,
            .close:focus {
                color: #000;
                text-decoration: none;
                cursor: pointer;
            }

            button[name=ok],[name=Cancel] {
                width: 25%;
                background-color: #008cff;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            button[name=ok],[name=Cancel]:hover {
                background-color: #008cff;
            }

            table.dataTable thead th.sorting,
            table.dataTable thead th.sorting_asc,
            table.dataTable thead th.sorting_desc {
                background: none;
                padding: 4px 5px;
            }
        </style>

    </head>
    <body>
        <div id="wrapper">

            <!-- /#sidebar-wrapper -->
            <%@ include file="../fragments/sidebar.jsp" %>

            <!-- Page Content -->
            <!-- nav-head-custom -->
            <%@ include file="../fragments/nav_head.jsp" %>

            <div class="container-fluid">
                <!--<div id="wrapper-top">-->
                <%-- PART 2 --%>
                <!--                    <div class="row">
                                        <div class="col-lg-12">
                                            <div id="set-height" style="height:415px;margin-top:0px;">
                                                <div id="sidebar-wrapper-top" class="">
                                                    <b class="page-header" style="padding-left:5px;font-size:18px;">
                <%-- <hr style="margin:0px;margin-top:10px;margin-bottom:10px;"> --%>
            </b>
        </div>
    </div>
    </div>
    </div> -->
                <%-- PART 3 --%>
                <!--show Qdest table-->
                <form action="../resources/manual/ISM800.pdf" target="_blank">
                    <table width="100%">
                        <tr id="dont">
                            <td width="94%" align="left"></td>
                            <td width="6%" align="right"><input type="submit" value="Manual" /></td>
                        </tr>
                    </table>

                </form>
                <div style="border: 2px solid #ccc; border-radius: 5px; padding: 20px;">
                    <table id="showTable" class="display" style="width:100%; border: 1px solid #ccc; border-radius: 4px;">
                        <thead>
                            <tr>
                                <th>NO.</th>
                                <th>Trans Date</th>
                                <th>Customer</th>
                                <th>Business</th>
                                <!--<th>Create By</th>-->
                                <th>User</th>
                                <th style="text-align: center;">Option
                                    &nbsp;&nbsp;&nbsp;
                                    <a href="create"><i class="fa fa-plus-circle" aria-hidden="true" style=" font-size: 30px; color: #001384;"></i></a>
                                </th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <!--<th></th>-->
                                <th></th>
                                <td></td>
                            </tr>
                        </tfoot>
                        <tbody>
                            <c:forEach items="${aheadList}" var="x">
                                <tr>
                                    <td>${x.NUM}</td>
                                    <td>${x.ISHTRDT}</td>
                                    <td>${x.ISHCUNO} : ${x.ISHCUNM1}</td>
                                    <td>${x.ISHPGRP}</td>
                                    <!--<td>${x.STATUS}</td>-->
                                    <td>${x.ISHUSER}</td>
                                    <td style=" text-align: center;">
                                        <a href="edit?dt=${x.ISHTRDT}&cus=${x.ISHCUNO}&ord=${x.ISHORD}&no=${x.NUM}" style="cursor: pointer;"><i class="fa fa-2x fa-pencil-square-o editBtn" aria-hidden="true" style=" font-size: 30px; color: #001384;"></i></a>
                                        &nbsp;
                                        <a onclick="Delfunc(${x.NUM})" style="cursor: pointer;"><i class="fa fa-trash" aria-hidden="true" style=" font-size: 30px; color: #001384;"></i></a>
                                    </td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                    <!--**********************-->
                    <!--End Part 3-->
                    <!--</div>  end #wrapper-top-->
                </div>
                <br>

                <center>
                    <div id="myModal" class="modal fade" role="dialog">
                        <div class="modal-dialog modal-sm"> <!-- ???????????? modal ?????????????? ???? xs, sm, md, lg -->
                            <div class="modal-md col-md-6" style="top: 150px; left:300px; background-color: #ddd; border: 1px solid #ccc; border-radius: 4px;">
                                <div class="modal-header">
                                    <h4 class="modal-title" align="center">Delete No <b id="modaltextTAG"></b> ?</h4>
                                    <br>
                                    <button name="ok" type="button" onclick="Delit()">
                                        <font color = "white">YES</font>
                                    </button>
                                    <button name="Cancel" type="button" onclick="" data-dismiss="modal">
                                        <font color = "white">NO</font>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- devbanban.com -->
                </center>
            </div> <!-- end .container-fluid -->
        </div> <!-- end #wrapper -->
    </body>
</html>
