<%@ include file="../fragments/taglibs.jsp" %>
<%@ include file="../fragments/imports.jsp" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.sql.*;" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>WMS</title>
        <!-- css :: vendors -->
        <jsp:include page = "../fragments/css.jsp" />
        <!-- additional custom :: my-style -->
        <link rel="stylesheet" href="../resources/assets/styles/myStyles.css">

        <!-- js :: vendors -->
        <jsp:include page = "../fragments/script.jsp" />
        <!-- additional custom :: my-script -->
        <script src="../resources/assets/scripts/myScripts.js" async></script>
        <script src="../resources/assets/scripts/toggleLoad.js" async></script> 
        <style>
            input[type=text], select{
                width: 100%;
                padding: 5px 5px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }

            input[type=date], input[type=number]{
                /*width: 100%;*/
                padding: 5px 5px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }

            input[type=submit], #ok {
                width: 100%;
                background-color: #00399b;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=submit]:hover, #ok:hover {
                background-color: #3973d6;
            }

            input[type=button] {
                width: 100%;
                background-color: #ef604a;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=button]:hover {
                background-color: #e51e00;
            }
        </style>
        <script>
            $(document).ready(function () {
                $('#showTable').DataTable({
                    "lengthMenu": [[-1, 10, 25, 50, 100], ["All", 10, 25, 50, 100]],
                    "bSortClasses": false
                });

                // Setup - add a text input to each footer cell
                $('#showTable tfoot th').each(function () {
                    var title = $(this).text();
                    $(this).html('<input type="text" class="searchInput"/>');
                });

                // DataTable
                var table = $('#showTable').DataTable();

                // Apply the search
                table.columns().every(function () {
                    var that = this;

                    $('input', this.footer()).on('keyup change', function () {
                        if (that.search() !== this.value) {
                            that.search(this.value).draw();
                        }
                    });
                });
            });
        </script>
        <script>
            $(document).ready(function () {
                $('#showTable2').DataTable({
                    "lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
                    "bSortClasses": false
                });

                // Setup - add a text input to each footer cell
                $('#showTable2 tfoot th').each(function () {
                    var title = $(this).text();
                    $(this).html('<input type="text" />');
                });

                // DataTable
                var table = $('#showTable2').DataTable();

                // Apply the search
                table.columns().every(function () {
                    var that = this;

                    $('input', this.footer()).on('keyup change', function () {
                        if (that.search() !== this.value) {
                            that.search(this.value).draw();
                        }
                    });
                });
            });
        </script>
        <script>
            function ValidForm() {
                var sinv2 = document.getElementById('sinv2').innerHTML.trim();

                if (sinv2 !== '') {
                    document.getElementById('frm').submit();
                    document.getElementById('loader').style.display = 'block';
                }
            }
        </script>
        <style>
            #loader {
                border: 16px solid #f3f3f3;
                border-radius: 100%;
                border-top: 16px solid #3498db;
                width: 240px;
                height: 240px;
                -webkit-animation: spin 2s linear infinite;
                animation: spin 2s linear infinite;
                margin: auto;
                /*margin-left:1200px;*/
                margin-top: 10px;
                margin-bottom: 10px;
            }   

            .panel-default {
                border-color: transparent;
                background-color: transparent;
            }

            @-webkit-keyframes spin {
                0% { -webkit-transform: rotate(0deg); }
                100% { -webkit-transform: rotate(360deg); }
            }

            @keyframes spin {
                0% { transform: rotate(0deg); }
                100% { transform: rotate(360deg); }
            }

        </style>
        <style>
            body {font-family: Arial, Helvetica, sans-serif;}

            /* The Modal (background) */
            .modal {
                display: none; /* Hidden by default */
                position: fixed; /* Stay in place */
                z-index: 1; /* Sit on top */
                padding-top: 100px; /* Location of the box */
                left: 0;
                top: 0;
                width: 100%; /* Full width */
                height: 100%; /* Full height */
                overflow: auto; /* Enable scroll if needed */
                background-color: rgb(0,0,0); /* Fallback color */
                background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
            }

            /* Modal Content */
            .modal-content {
                background-color: #fefefe;
                margin: auto;
                padding: 20px;
                border: 1px solid #888;
                width: fit-content;
                height: fit-content;
            }

            /* The Close Button */
            .close {
                color: #aaaaaa;
                float: right;
                font-size: 28px;
                font-weight: bold;
            }

            .close:hover,
            .close:focus {
                color: #000;
                text-decoration: none;
                cursor: pointer;
            }
        </style>
        <script>
            Array.prototype.remove = function () {
                var what, a = arguments, L = a.length, ax;
                while (L && this.length) {
                    what = a[--L];
                    while ((ax = this.indexOf(what)) !== -1) {
                        this.splice(ax, 1);
                    }
                }
                return this;
            };

            var aList = [${aList}];

            function ToSelect() {
                var input = document.getElementsByTagName('input');
                for (var i = 0; i < input.length; i++) {
                    if (input[i].type === 'checkbox') {
                        if (input[i].name === 'avaiCheck') {
                            if (input[i].checked === true) {
                                var ck = '<label style="cursor: pointer;">'
                                        + '<input type="checkbox" name="selectCheck" value="' + input[i].value + '" style="width: 15px; height: 15px;">  '
                                        + input[i].value
                                        + '</label><br>';

                                if (!aList.includes(input[i].value)) {
                                    aList.push(input[i].value);
                                    document.getElementById('sinv').innerHTML += ck;
                                }
                            }
                        }
                    }
                }

                var checkboxes = document.getElementsByTagName('input');
                for (var i = 0; i < checkboxes.length; i++) {
                    if (checkboxes[i].type === 'checkbox') {
                        if (checkboxes[i].name === 'avaiCheck') {
                            checkboxes[i].checked = false;
                        }
                    }
                }

            }

            function ToAvai() {
                var input = document.getElementsByTagName('input');
                var sinv = document.getElementById('sinv').innerHTML;
                for (var i = 0; i < input.length; i++) {
                    if (input[i].type === 'checkbox') {
                        if (input[i].name === 'selectCheck') {
                            if (input[i].checked === true) {
                                var ck = '<label style="cursor: pointer;">'
                                        + '<input type="checkbox" name="selectCheck" value="' + input[i].value + '" style="width: 15px; height: 15px;">  '
                                        + input[i].value
                                        + '</label><br>';

                                aList.remove(input[i].value);
                                sinv = sinv.toString().replace(ck, '');
                            }
                        }
                    }
                }

                document.getElementById('sinv').innerHTML = sinv;

            }

            function ToAvaiAll() {
                document.getElementById('sinv').innerHTML = '';
                aList = [];

            }

            function AllSelect() {
                document.getElementById('sinv2').innerHTML = document.getElementById('sinv').innerHTML.replace(/<br>/g, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;');
                var input = document.getElementsByTagName('input');
                var isNotEmpty = false;
                for (var i = 0; i < input.length; i++) {
                    if (input[i].type === 'checkbox') {
                        if (input[i].name === 'selectCheck') {
                            input[i].checked = true;
                            isNotEmpty = true;
                        }
                    }
                }

                if (isNotEmpty) {
//                    document.getElementById('add-inv').submit();
                    document.getElementById('myModal-add').style.display = 'none';
//                    ValidForm();
                }
            }

            function AutoCheck() {
                var input = document.getElementsByTagName('input');
                for (var i = 0; i < input.length; i++) {
                    if (input[i].type === 'checkbox') {
                        if (input[i].name === 'selectCheck') {
                            input[i].checked = true;
                        }
                    }
                }
            }
        </script>
        <script>
            function checkAll(ele) {
                var checkboxes = document.getElementsByTagName('input');
                if (ele.checked) {
                    for (var i = 0; i < checkboxes.length; i++) {
                        if (checkboxes[i].type === 'checkbox') {
                            if (checkboxes[i].name === 'avaiCheck') {
                                checkboxes[i].checked = true;
                            }
                        }
                    }
                } else {
                    for (var i = 0; i < checkboxes.length; i++) {
                        if (checkboxes[i].type === 'checkbox') {
                            if (checkboxes[i].name === 'avaiCheck') {
                                checkboxes[i].checked = false;
                            }
                        }
                    }
                }
            }
        </script>
    </head>    
    <body onload="document.getElementById('userid').value = sessionStorage.getItem('uid');
            AutoCheck()">
        <div id="wrapper">

            <!-- /#sidebar-wrapper -->
            <%@ include file="../fragments/sidebar.jsp" %>
            <!--test-->

            <!-- Page Content -->
            <!-- nav-head-custom -->
            <%@ include file="../fragments/nav_head.jsp" %>

            <div class="container-fluid">
                <!--<div id="wrapper-top">-->
                <%-- PART 2 --%>
                <!--                    <div class="row">
                                        <div class="col-lg-12">
                                            <div id="set-height" style="height:415px;margin-top:0px;">
                                                <div id="sidebar-wrapper-top" class="">
                                                    <b class="page-header" style="padding-left:5px;font-size:18px;">
                <%-- <hr style="margin:0px;margin-top:10px;margin-bottom:10px;"> --%>
            </b>
        </div>
    </div>
</div>
</div> -->
                <%-- PART 3 --%>
                <!--show Qdest table-->
                <script>
                    function ckData(matctrl, matctrlname) {
                        var dateList = $('#sinv2 input[name=selectCheck]');
                        var whd = $('#whd').val();
                        var plant = $('#plant').val();
                        var dateText = "";
                        for (var i = 0; i < dateList.length; i++) {
                            if (i === 0) {
                                dateText += "'" + dateList[i].value.toString().split('-')[0] + '-' + dateList[i].value.toString().split('-')[1] + "'";
                            } else {
                                dateText += ",'" + dateList[i].value.toString().split('-')[0] + '-' + dateList[i].value.toString().split('-')[1] + "'";
                            }
                        }

                        $.ajax({
                            url: '/TABLE2/ShowDataTablesWMS990',
                            data: {
                                mode: 'getMatcodeList',
                                dateText: dateText,
                                whd: whd,
                                plant: plant,
                                matctrl: matctrl
                            }
                        }).done(function (data) {
                            $('#matCtrlModal').html(matctrl + ' ' + matctrlname);

                            var table = $('#showTable2').DataTable();
                            table.clear().draw();

                            for (var i = 0; i < data.length; i++) {
                                table.row.add([(i + 1), data[i].NAME, data[i].SUBNAME]).draw(false);
                            }

                            $('#matcodeModal').modal('show');

                        }).fail(function (jqXHR, textStatus, errorThrown) {

                        });

                    }
                </script>
                <!-- Modal -->
                <div class="modal fade" id="matcodeModal" role="dialog">
                    <div class="modal-dialog" style="width: fit-content;">

                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Movement SKU</h4>
                                <h5 class="modal-title">Mat Ctrl : <span id="matCtrlModal"></span></h5>
                            </div>
                            <div class="modal-body">
                                <table id="showTable2" class="display" style="width:100%; border: 1px solid #ccc; border-radius: 4px;">
                                    <thead>
                                        <tr>
                                            <th style="text-align: center; width: 100px;">No.</th>
                                            <th style="text-align: center;">Material Code</th>
                                            <th style="text-align: center;">Description</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                        </tr>
                                    </tfoot>
                                    <tbody id="matcodeModalBody">

                                    </tbody>
                                </table>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </div>

                    </div>
                </div>
                <form action="../resources/manual/WMS990.pdf" target="_blank">
                    <table width="100%">
                        <tr>
                            <td width="94%"></td>
                            <td width="6%" align="right"><input type="submit" value="Manual" /></td>
                        </tr>
                    </table>
                </form>
                <div style="border: 2px solid #ccc; border-radius: 5px; padding: 20px;">
                    <form id="frm" name="frm" action="display" method="post">
                        <table width="100%">
                            <tr>
                                <td width="10%">
                                    <b style="color: #00399b;">Check Stock Date </b>
                                </td>
                                <td width="10%">
                                    <a onclick="document.getElementById('myModal-add').style.display = 'block';" style="cursor: pointer;"><i class="glyphicon glyphicon-calendar" style="font-size:25px; padding-left: 10px"></i></a>
                                </td>
                                <td width="80%">
                                    <div id="sinv2" style="display: block;">
                                        ${sinv2}
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td width="10%">
                                    <b style="color: #00399b;">Dept. / WH. </b>
                                </td>
                                <td width="10%">
                                    <select id="whd" name="whd" style=" width: 60%;">
                                        <option value="${whd}" selected hidden>${whd}</option>
                                        <optgroup label="Dept">
                                            <c:forEach items="${deptList}" var="x">
                                                <option value="${x.DEPT}">${x.DEPT}</option>
                                            </c:forEach>
                                        </optgroup>
                                        <optgroup label="Warehouse">
                                            <c:forEach items="${whList}" var="x">
                                                <option value="${x.WH}">${x.WH}</option>
                                            </c:forEach>
                                        </optgroup>
                                    </select>
                                </td>
                                <td width="80%">
                                    <b style="color: #00399b;">Plant </b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    <select id="plant" name="plant" style=" width: 100px;">
                                        <option value="${plant}" selected hidden>${plant}</option>
                                        <option value="ALL">ALL</option>
                                        <option value="1000">1000</option>
                                        <option value="1050">1050</option>
                                    </select>
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    <input id="ok" type="button" value="Confirm" onclick="ValidForm();" style=" width: 100px;"/>
                                </td>
                            </tr>
                        </table>
                    </form>
                    <br>
                    <div id="loader" style="display: none;"></div>
                    <script>
                        $(document).ready(function () {
                            function currencyFormat3(num) {
                                return num.toFixed(3).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
                            }

                            function currencyFormat1(num) {
                                return num.toFixed(1).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
                            }

                            function currencyFormat0(num) {
                                return num.toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
                            }

                            $('.searchInput').on('keyup change', function () {
                                var MVTSKU = $('.det-MVTSKU');
                                var OH_SKU = $('.det-OH_SKU');
                                var AMOUNT = $('.det-AMOUNT');
                                var PACKPCS = $('.det-PACKPCS');
                                var CK_SKU = $('.det-CK_SKU');
                                var PACKPCS2 = $('.det-PACKPCS2');
                                var COL_SKU = $('.det-COL_SKU');
                                var DIFF_SKU = $('.det-DIFF_SKU');
                                var AMOUNT2 = $('.det-AMOUNT2');

                                var sumMVTSKU = 0;
                                var sumOH_SKU = 0;
                                var sumAMOUNT = 0.000;
                                var sumPACKPCS = 0;
                                var sumCK_SKU = 0;
                                var sumPACKPCS2 = 0;
                                var sumCOL_SKU = 0;
                                var sumDIFF_SKU = 0;
                                var sumAMOUNT2 = 0.000;

                                for (var i = 0; i < MVTSKU.length; i++) {
                                    sumMVTSKU += parseInt(MVTSKU[i].innerHTML.toString().replace(/,/g, ''));
                                    sumOH_SKU += parseInt(OH_SKU[i].innerHTML.toString().replace(/,/g, ''));
                                    sumAMOUNT += parseFloat(AMOUNT[i].innerHTML.toString().replace(/,/g, ''));
                                    sumPACKPCS += parseInt(PACKPCS[i].innerHTML.toString().replace(/,/g, ''));
                                    sumCK_SKU += parseInt(CK_SKU[i].innerHTML.toString().replace(/,/g, ''));
                                    sumPACKPCS2 += parseInt(PACKPCS2[i].innerHTML.toString().replace(/,/g, ''));
                                    sumCOL_SKU += parseInt(COL_SKU[i].innerHTML.toString().replace(/,/g, ''));
                                    sumDIFF_SKU += parseInt(DIFF_SKU[i].innerHTML.toString().replace(/,/g, ''));
                                    sumAMOUNT2 += parseFloat(AMOUNT2[i].innerHTML.toString().replace(/,/g, ''));
                                }

                                $('#sum-MVTSKU').html(currencyFormat0(sumMVTSKU));
                                $('#sum-OH_SKU').html(currencyFormat0(sumOH_SKU));
                                $('#sum-AMOUNT').html(currencyFormat3(sumAMOUNT));
                                $('#sum-PACKPCS').html(currencyFormat0(sumPACKPCS));
                                $('#sum-CK_SKU').html(currencyFormat0(sumCK_SKU));
                                $('#sum-CK_SKU_PER').html(currencyFormat1((sumCK_SKU / sumOH_SKU) * 100) + ' %');
                                $('#sum-PACKPCS2').html(currencyFormat0(sumPACKPCS2));
                                $('#sum-COL_SKU').html(currencyFormat0(sumCOL_SKU));
                                $('#sum-COL_SKU_PER').html(currencyFormat1((sumCOL_SKU / sumOH_SKU) * 100) + ' %');
                                $('#sum-DIFF_SKU').html(currencyFormat0(sumDIFF_SKU));
                                $('#sum-DIFF_SKU_PER').html(currencyFormat1((sumDIFF_SKU / sumOH_SKU) * 100) + ' %');
                                $('#sum-AMOUNT2').html(currencyFormat3(sumAMOUNT2));
                            });
                        });
                    </script>
                    <table id="showTable" class="display" style="width:100%; border: 1px solid #ccc; border-radius: 4px;">
                        <thead>
                            <tr>
                                <th colspan="2" style="text-align: center; border-bottom-color: transparent;"></th>
                                <th style="text-align: center; border-bottom-width: 3px;">TRANSACTION</th>
                                <th colspan="2" style="text-align: center; border-bottom-width: 3px; border-bottom-color: #ef604a; background-color: #bfe4ff;">SAP</th>
                                <th style="text-align: center; border-bottom-width: 3px; background-color: #a6ffa9;">WMS</th>
                                <th colspan="8" style="text-align: center; border-bottom-width: 3px; border-bottom-color: #ef604a;">TABLET</th>
                            </tr>
                            <tr>
                                <th style="text-align: center;">Mat Ctrl</th>
                                <th style="text-align: center;">Name</th>
                                <th style="text-align: center;">Movement SKU</th>
                                <th style="text-align: center; background-color: #bfe4ff;">Onhand SKU</th>
                                <th style="text-align: center; background-color: #bfe4ff;">Amount</th>
                                <th style="text-align: center; background-color: #a6ffa9;">Package Piece</th>
                                <th style="text-align: center;">Checked SKU</th>
                                <th style="text-align: center;">%</th>
                                <th style="text-align: center;">Package Piece</th>
                                <th style="text-align: center;">Correct SKU</th>
                                <th style="text-align: center;">%</th>
                                <th style="text-align: center;">Diff SKU</th>
                                <th style="text-align: center;">%</th>
                                <th style="text-align: center;">Amount</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th style="background-color: #bfe4ff;"></th>
                                <th style="background-color: #bfe4ff;"></th>
                                <th style="background-color: #a6ffa9;"></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                            </tr>
                        </tfoot>
                        <tbody>
                            <c:forEach items="${detailList}" var="p" varStatus="i">
                                <tr>
                                    <td>${p.PRODUCT}</td>
                                    <td>${p.NAME}</td>
                                    <td class="det-MVTSKU" title="คลิก เพื่อแสดงรายละเอียด" onclick="ckData('${p.PRODUCT}', '${p.NAME}');" style="text-align: right; cursor: pointer;"><fmt:formatNumber pattern="###,###,###,##0" value="${p.MVTSKU}" />&nbsp;&nbsp;</td>
                                    <td class="det-OH_SKU" style="text-align: right; background-color: #bfe4ff;"><fmt:formatNumber pattern="###,###,###,##0" value="${p.OH_SKU}" />&nbsp;&nbsp;</td>
                                    <td class="det-AMOUNT" style="text-align: right; background-color: #bfe4ff;"><fmt:formatNumber pattern="###,###,###,##0.000" value="${p.AMOUNT}" />&nbsp;&nbsp;</td>
                                    <td class="det-PACKPCS" style="text-align: right; background-color: #a6ffa9;"><fmt:formatNumber pattern="###,###,###,##0" value="${p.PACKPCS}" />&nbsp;&nbsp;</td>
                                    <td class="det-CK_SKU" style="text-align: right;"><fmt:formatNumber pattern="###,###,###,##0" value="${p.CK_SKU}" />&nbsp;&nbsp;</td>
                                    <td class="det-CK_SKU_PER" style="text-align: right;"><fmt:formatNumber pattern="###,###,###,##0.0" value="${p.CK_SKU_PER}" /> %&nbsp;&nbsp;</td>
                                    <td class="det-PACKPCS2" style="text-align: right;"><fmt:formatNumber pattern="###,###,###,##0" value="${p.PACKPCS2}" />&nbsp;&nbsp;</td>
                                    <td class="det-COL_SKU" style="text-align: right;"><fmt:formatNumber pattern="###,###,###,##0" value="${p.COL_SKU}" />&nbsp;&nbsp;</td>
                                    <td class="det-COL_SKU_PER" style="text-align: right;"><fmt:formatNumber pattern="###,###,###,##0.0" value="${p.COL_SKU_PER}" /> %&nbsp;&nbsp;</td>
                                    <td class="det-DIFF_SKU" style="text-align: right;"><fmt:formatNumber pattern="###,###,###,##0" value="${p.DIFF_SKU}" />&nbsp;&nbsp;</td>
                                    <td class="det-DIFF_SKU_PER" style="text-align: right;"><fmt:formatNumber pattern="###,###,###,##0.0" value="${p.DIFF_SKU_PER}" /> %&nbsp;&nbsp;</td>
                                    <td class="det-AMOUNT2" style="text-align: right;"><fmt:formatNumber pattern="###,###,###,##0.000" value="${p.AMOUNT2}" />&nbsp;&nbsp;</td>
                                </tr>
                            </c:forEach>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td></td>
                                <td></td>
                                <td style="text-align: right;"><b id="sum-MVTSKU"><fmt:formatNumber pattern="###,###,###,##0" value="${sum.MVTSKU}" /></b></td>
                                <td style="text-align: right; background-color: #bfe4ff;"><b id="sum-OH_SKU"><fmt:formatNumber pattern="###,###,###,##0" value="${sum.OH_SKU}" /></b></td>
                                <td style="text-align: right; background-color: #bfe4ff;"><b id="sum-AMOUNT"><fmt:formatNumber pattern="###,###,###,##0.000" value="${sum.AMOUNT}" /></b></td>
                                <td style="text-align: right; background-color: #a6ffa9;"><b id="sum-PACKPCS"><fmt:formatNumber pattern="###,###,###,##0" value="${sum.PACKPCS}" /></b></td>
                                <td style="text-align: right;"><b id="sum-CK_SKU"><fmt:formatNumber pattern="###,###,###,##0" value="${sum.CK_SKU}" /></b></td>
                                <td style="text-align: right;"><b id="sum-CK_SKU_PER"><fmt:formatNumber pattern="###,###,###,##0.0" value="${sum.CK_SKU_PER}" /> %</b></td>
                                <td style="text-align: right;"><b id="sum-PACKPCS2"><fmt:formatNumber pattern="###,###,###,##0" value="${sum.PACKPCS2}" /></b></td>
                                <td style="text-align: right;"><b id="sum-COL_SKU"><fmt:formatNumber pattern="###,###,###,##0" value="${sum.COL_SKU}" /></b></td>
                                <td style="text-align: right;"><b id="sum-COL_SKU_PER"><fmt:formatNumber pattern="###,###,###,##0.0" value="${sum.COL_SKU_PER}" /> %</b></td>
                                <td style="text-align: right;"><b id="sum-DIFF_SKU"><fmt:formatNumber pattern="###,###,###,##0" value="${sum.DIFF_SKU}" /></b></td>
                                <td style="text-align: right;"><b id="sum-DIFF_SKU_PER"><fmt:formatNumber pattern="###,###,###,##0.0" value="${sum.DIFF_SKU_PER}" /> %</b></td>
                                <td style="text-align: right;"><b id="sum-AMOUNT2"><fmt:formatNumber pattern="###,###,###,##0.000" value="${sum.AMOUNT2}" /></b></td>
                            </tr>
                        </tfoot>
                    </table>
                    <div id="myModal-add" class="modal">
                        <!-- Modal content -->
                        <div class="modal-content" style=" height: 600px; width: 1000px;">
                            <span id="span-add" class="close" onclick="document.getElementById('myModal-add').style.display = 'none';">&times;</span>
                            <p><b><font size="4"></font></b></p>
                            <table width="100%">
                                <tr style=" background-color: white;">
                                    <td align="center">
                                        <b style="color: #00399b;">
                                            <font size="5">Select Onhand Date</font><hr>
                                            <table style=" width: 80%;">
                                                <tr>
                                                    <td style=" text-align: center;">
                                                        <font size="4">Available Onhand Date</font>
                                                    </td>
                                                    <td></td>
                                                    <td style=" text-align: center;">
                                                        <font size="4">Selected Onhand Date</font>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style=" text-align: center;">
                                                    </td>
                                                    <td style=" opacity: 0;">XXX</td>
                                                    <td style=" text-align: center;">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style=" text-align: left; width: 40%;">
                                                        <div id="ainv" style=" padding-left: 20px; padding-top: 20px; border: 1px solid #ccc; border-radius: 5px; width: 100%; height: 300px; overflow: auto;">
                                                            <label style="cursor: pointer;">
                                                                <input type="checkbox" name="selectAll" onchange="checkAll(this);" style="width: 15px; height: 15px;">
                                                                All
                                                            </label>
                                                            <br>
                                                            <c:forEach items="${ohdList}" var="p" varStatus="i">
                                                                <label style="cursor: pointer;">
                                                                    <input type="checkbox" name="avaiCheck" value="${p.OH_SKU}" style="width: 15px; height: 15px;">
                                                                    ${p.OH_SKU}
                                                                </label>
                                                                <br>
                                                            </c:forEach>
                                                        </div>
                                                    </td>
                                                    <td style=" text-align: center;">
                                                        <br><br>
                                                        <button class="btn btn btn-outline btn-info" onclick="ToSelect();"><i class="fa fa-angle-right" style="font-size:20px;"></i></button><br><br>
                                                        <button class="btn btn btn-outline btn-info" onclick="ToAvai();"><i class="fa fa-angle-left" style="font-size:20px;"></i></button><br><br>
                                                        <button class="btn btn btn-outline btn-info" onclick="ToAvaiAll();"><i class="fa fa-angle-double-left" style="font-size:20px;"></i></button>
                                                    </td>
                                                    <td style=" text-align: left; width: 40%;">
                                                        <form id="add-inv" action="create" method="post">
                                                            <input type="hidden" id="userid" name="userid">
                                                            <div id="sinv" style=" padding-left: 20px; padding-top: 20px; border: 1px solid #ccc; border-radius: 5px; width: 100%; height: 300px; overflow: auto;">
                                                                ${sinv}
                                                            </div>
                                                        </form>
                                                    </td>
                                                </tr>
                                            </table>
                                        </b>
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <br><br><br>
                                        <a class="btn btn btn-outline btn-danger" onclick="document.getElementById('myModal-add').style.display = 'none';">
                                            <i class="fa fa-times-circle-o" style="font-size:20px;"></i> Cancel</a>  
                                        &nbsp;
                                        <a class="btn btn btn-outline btn-success" onclick="AllSelect();">
                                            <i class="fa fa-check-circle-o" style="font-size:20px;"></i> Confirm</a>                    
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <!--**********************-->
                    <!--End Part 3-->
                    <!--</div>  end #wrapper-top--> 
                </div> 
                <br><br>
            </div> <!-- end .container-fluid -->
        </div> <!-- end #wrapper -->
    </body>
</html>