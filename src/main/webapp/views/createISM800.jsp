<%-- 
    Document   : createISM800
    Created on : May 10, 2021, 1:11:58 PM
    Author     : 93176
--%>

<%@ include file="../fragments/taglibs.jsp" %>
<%@ include file="../fragments/imports.jsp" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.sql.*;" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>ISM</title>
        <!-- css :: vendors -->
        <jsp:include page = "../fragments/css.jsp" />
        <!-- additional custom :: my-style -->
        <link rel="stylesheet" href="../resources/assets/styles/myStyles.css">

        <!-- js :: vendors -->
        <jsp:include page = "../fragments/script.jsp" />
        <!-- additional custom :: my-script -->
        <script src="../resources/assets/scripts/myScripts.js" async></script>
        <script src="../resources/assets/scripts/toggleLoad.js" async></script>
        <script type="text/javascript"> /* add something code to your script */</script>
        <script>
            var show1 = function () {
                $('#myModal1').modal('show');
            };

            var show = function () {
                $('#myModal').modal('show');
            };

            var show2 = function () {
                $('#myModal-error').modal('show');
            };

            function validateForm() {
                var x = document.forms["frm"]["id"].value;

                if (x === "") {
                    window.setTimeout(show1, 0);
                    return false;
                }
            }
        </script>
        <script>
            $(document).ready(function () {

            ${sendMessage}

                $("#saledatef").datepicker({
                    format: 'dd/mm/yyyy'
                });
                $("#saledatet").datepicker({
                    format: 'dd/mm/yyyy'
                });
                $("#delidatef").datepicker({
                    format: 'dd/mm/yyyy'
                });
                $("#delidatet").datepicker({
                    format: 'dd/mm/yyyy'
                });

                $('#tableError').DataTable({
                    "paging": false,
                    "ordering": false
//                    columnDefs: [
//                        {targets: 0, className: 'dt-body-right'},
//                        {targets: 1, className: 'dt-body-right'},
//                        {targets: 2, className: 'dt-body-right'}
//                    ]
                });

                $('#tableError2').DataTable({
                    "paging": false,
                    "ordering": false
//                    columnDefs: [
//                        {targets: 0, className: 'dt-body-right'},
//                        {targets: 1, className: 'dt-body-right'},
//                        {targets: 2, className: 'dt-body-right'}
//                    ]
                });

//                $.ajax({
//                    url: "/TABLE2/ShowDataTablesISM100?mode=updateReqDate"
//                            + "&orderNo=" + orderNo
//                            + "&regDate=" + regDate
//                            + "&uid=" + sessionStorage.getItem('uid')
//
//                }).done(function (result) {
//                    console.log(result);
//
//                }).fail(function (jqXHR, textStatus, errorThrown) {
//                    // needs to implement if it fails
//                });

            });
        </script>
        <style>
            input[type=text], select {
                width: 100%;
                padding: 5px 5px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }

            input[type=date] {
                /*width: 100%;*/
                padding: 5px 5px;
                margin: 8px 0;
                height: 48px;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }

            input[type=submit] {
                width: 100%;
                background-color: #2bd14a;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=submit]:hover {
                background-color: #45a049;
            }

            /*            input[type=button] {
                            width: 100%;
                            background-color: #ef604a;
                            color: white;
                            padding: 5px 5px;
                            margin: 8px 0;
                            border: none;
                            border-radius: 4px;
                            cursor: pointer;
                        }*/

            /*            input[type=button]:hover {
                            background-color: #e51e00;
                        }*/

            #canbtn:hover{
                /*background-color: #e51e00;*/
            }

            #confbtn:hover{
                background-color: green;
            }

            /*            button[name=ok],[name=Cancel] {
                            width: 25%;
                            background-color: #008cff;
                            color: white;
                            padding: 5px 5px;
                            margin: 8px 0;
                            border: none;
                            border-radius: 4px;
                            cursor: pointer;
                        }*/

            button[name=ok],[name=Cancel]:hover {
                background-color: #008cff;
            }

            /* Customize the label (the container) */
            .form-check-label {
                display: block;
                position: relative;
                padding-left: 35px;
                margin-bottom: 12px;
                cursor: pointer;
                font-size: 22px;
                -webkit-user-select: none;
                -moz-user-select: none;
                -ms-user-select: none;
                user-select: none;
            }

            /* Hide the browser's default radio button */
            .form-check-label input {
                position: absolute;
                opacity: 0;
                cursor: pointer;
                height: 0;
                width: 0;
            }

            /* Create a custom radio button */
            .checkmark {
                position: absolute;
                top: 0;
                left: 0;
                height: 25px;
                width: 25px;
                background-color: #eee;
                border-radius: 50%;
            }

            /* On mouse-over, add a grey background color */
            .form-check-label:hover input ~ .checkmark {
                background-color: #ccc;
            }

            /* When the radio button is checked, add a blue background */
            .form-check-label input:checked ~ .checkmark {
                background-color: #2196F3;
            }

            /* Create the indicator (the dot/circle - hidden when not checked) */
            .checkmark:after {
                content: "";
                position: absolute;
                display: none;
            }

            /* Show the indicator (dot/circle) when checked */
            .form-check-label input:checked ~ .checkmark:after {
                display: block;
            }

            /* Style the indicator (dot/circle) */
            .form-check-label .checkmark:after {
                top: 9px;
                left: 9px;
                width: 8px;
                height: 8px;
                border-radius: 50%;
                background: white;
            }

            /*modal error msg*/

            /* The Modal (background) */
            .modal_error {
                display: none; /* Hidden by default */
                position: fixed; /* Stay in place */
                z-index: 1; /* Sit on top */
                padding-top: 100px; /* Location of the box */
                left: 0;
                top: 0;
                width: 100%; /* Full width */
                height: 100%; /* Full height */
                overflow: auto; /* Enable scroll if needed */
                background-color: rgb(0,0,0); /* Fallback color */
                background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
            }

            /* Modal Content */
            .modal-content_error {
                background-color: #fefefe;
                margin: auto;
                padding: 20px;
                border: 1px solid #888;
                width: fit-content;
                height: 145px;
            }

            /*modal error msg --- end */

        </style>
        <script>

            $(document).ready(function () {

                $("#saleOrdList").bind('paste', function (e) {
                    var elem = $(this);

                    let gettext = "";
                    setTimeout(function () {
                        // gets the copied text after a specified time (100 milliseconds)
                        var text = elem.val();
                        gettext = text.toString().split("\n");

                        $('#labelte').val("");

                        for (var i = 0; i < gettext.length; i++) {
                            if (i >= 30) {
                                console.log(gettext[i]);
                            } else {
                                if (gettext[i].length === 10) {
                                    $('#labelte').val($('#labelte').val() + " " + gettext[i]);
                                }
                            }
                        }

                        $('#saleOrdList').val("");

                        $('#saleOrdList').val($('#labelte').val().trim());

                    }, 100);

                });

                $('#saledatef').on('change', function () {
                    $('#confbtn').prop('disabled', false);
                });
                $('#saledatet').on('change', function () {
                    $('#confbtn').prop('disabled', false);
                });
                $('#delidatef').on('change', function () {
                    $('#confbtn').prop('disabled', false);
                });
                $('#delidatet').on('change', function () {
                    $('#confbtn').prop('disabled', false);
                });
                $('#saleorderf').on('change', function () {
                    $('#confbtn').prop('disabled', false);
                });
                $('#saleordert').on('change', function () {
                    $('#confbtn').prop('disabled', false);
                });
                $('#procurf').on('change', function () {
                    $('#confbtn').prop('disabled', false);
                });
                $('#procurt').on('change', function () {
                    $('#confbtn').prop('disabled', false);
                });
                $('#createbf').on('change', function () {
                    $('#confbtn').prop('disabled', false);
                });
                $('#createbt').on('change', function () {
                    $('#confbtn').prop('disabled', false);
                });

//                $("#cusSelect").val("210027");
//                $("#saledatef").val("2021-12-03");
//                $("#saledatet").val("2021-12-03");
//                $("#saleorderf").val("1420344185");

//                let xhProd = "";
//                xhProd = $.ajax({
//                    url: "/TABLE2/ISM800GET",
//                    type: "GET",
//                    data: {mode: "getProd"},
//                    async: false
//                }).responseJSON;

//                $('#SelectProd').append('<option value="Select" hidden>--Please Select an Option--</option>');

//                for (var i = 0; i < xhProd.Prod.length; i++) {
//                    $('#SelectProd').append('<option value=' + xhProd.Prod[i].Code + '>' + xhProd.Prod[i].Code + " : " + xhProd.Prod[i].QWHSUBPROD + '</option>');
//                }

            ${ALRT}
            ${myModal_alert_list}

            });

            function checkBF() {

                var cusSelect = $('#cusSelect').val();
                var saledatef = $('#saledatef').val();
                var saledatet = $('#saledatet').val();
                var saleorderf = $('#saleorderf').val();
                var saleordert = $('#saleordert').val();
//                var SelectProd = $('#SelectProd').val();
                var labelte = $('#labelte').val();
                var procurf = $('#procurf').val();
                var procurt = $('#procurt').val();
                var labelpro = $('#labelpro').val();
                var createbf = $('#createbf').val();
                var createbt = $('#createbt').val();
                var setroon = $('#setroon').val();
                var kaiperm = $('#kaiperm').val();

                console.log(cusSelect);
                console.log(saledatef + " " + saledatet);
                console.log(saleorderf + " " + saleordert);
                console.log(labelte);
                console.log(procurf + " " + procurt);
                console.log(labelpro);
                console.log(createbf + " " + createbt);
                console.log(setroon);
                console.log(kaiperm);

//                $('#frmsub').submit();

                var delidatef = $('#delidatef').val();
                var delidatet = $('#delidatet').val();

                if (delidatet < delidatef) {
                    $('#myModal_human_error_deli').modal('show');
                }
                if (saledatet < saledatef) {
                    $('#myModal_human_error_sales').modal('show');
                }
                if (saleordert < saleorderf) {
                    $('#myModal_human_error_sales_no').modal('show');
                }
                if (procurt < procurf) {
                    $('#myModal_human_error_pur').modal('show');
                }
//                if (createbt < createbf) {
//                    $('#myModal_human_error_createby').modal('show');
//                }

//                $('#frmsub').submit();

                if (labelte !== "" && saleorderf !== "") {
                    $('#myModal_just_one').modal('show');

                } else {
                    if (saledatef === "" || saledatet === "") {
                        alertify.error("Please select Sales Order date.");
                    } else {
                        $('#confbtn').prop('disabled', true);
                        $('#loadIcon').modal('show');
                        $('#frmsub').submit();
                    }
                }

            }

//            function againtwo() {
//
////                $('#saledatef').val("2020-10-29");
////                $('#saledatet').val("2020-10-29");
////                $('#saleorderf').val("1320184940");
////                $('#saleordert').val("1320184940");
//
//                $('#again').val("again2");
//                $('#frmsub').submit();
//            }

        </script>
    </head>
    <body onload="document.getElementById('userid').value = sessionStorage.getItem('uid');">
        <div id="wrapper">

            <!-- /#sidebar-wrapper -->
            <%@ include file="../fragments/sidebar.jsp" %>

            <!-- Page Content -->
            <!-- nav-head-custom -->
            <%@ include file="../fragments/nav_head.jsp" %>

            <div class="container-fluid">
                <!--                <div id="wrapper-top" align="left">
                <%-- PART 2 --%>
                <div class="row">
                    <div class="col-lg-12">
                        <div id="set-height" style="height:415px;margin-top:0px;">
                            <div id="sidebar-wrapper-top" class="">
                                <b class="page-header" style="padding-left:5px;font-size:18px;">
                <%-- <hr style="margin:0px;margin-top:10px;margin-bottom:10px;"> --%>
            </b>
        </div>
    </div>
</div>
</div> -->

                <%-- PART 3 --%>
                <form action="create" method="POST" name="frm" id="frmsub" onsubmit="return validateForm()">
                    <input type="hidden" id="userid" name="userid">
                    <input id="again" name="again" hidden>
                    <br>
                    <div style="width: 100%; height: 50px; background-color: #e0e0e0;">
                        <br>
                        <h4><b>CREATE MODE</b></h4>
                    </div>

                    <div style="float: right; margin: 8px 7px 2px 2px; height: 10px;" >
                        <input style="width: 100px;" type="button" class="btn btn-primary" id="canbtn" value="Back" onclick="window.location.href = '/TABLE2/ISM800/display?uid=' + sessionStorage.getItem('uid');"/>
                    </div>

                    <center>
                        <div style="border-style: solid; border-width: thin; border-color: #e0e0e0;">
                            <table style="margin: 20px; width: 60%; ">
                                <tr>
                                    <th style="width: 10px;"></th>
                                    <th style="width: 50px;"></th>
                                    <th style="width: 20px;">From</th>
                                    <th style="width: 20px;"></th>
                                    <th style="width: 20px;">To</th>
                                </tr>
                                <tr>
                                    <th style="width: 10px;"></th>
                                    <th style="width: 50px;">
                                        Customer no. :
                                    </th>
                                    <th colspan="3">
                                        <select id="cusSelect" name="cusSelect" style=" height: 33px; width: 95%;" >
                                            <option value="210025">210025 : บริษัท วาโก้กบินทร์บุรี จำกัด</option>
                                            <option value="210026">210026 : บริษัท วาโก้ลำพูน จำกัด</option>
                                            <option value="210027">210027 : บริษัท ภัทยากบินทร์บุรี จำกัด</option>
                                            <option value="210284">210284 : บริษัท วาโก้ศรีราชา จำกัด</option>
                                            <option value="211289">211289 : บริษัท เอ็ม เอส บี แอพพาเรล จำกัด</option>
                                        </select> <label style="color:red; font-size: 25px;">*</label>
                                    </th>
                                </tr>
                                <tr>
                                    <th style="width: 10px;"></th>
                                    <th style="width: 50px;">
                                        Sales Order date. :
                                    </th>
                                    <th style="width: 20px;">
                                        <input value="" class="form-control" type="text" id="saledatef" name="saledatef" placeholder="dd/mm/yyyy" style="width:142px; height: 48px; box-shadow: none;" required> <label style="color:red; font-size: 25px;">*</label>
                                    </th>
                                    <th style="width: 20px; text-align:center;"> - </th>
                                    <th style="width: 20px;">
                                        <input value="" class="form-control" type="text" id="saledatet" name="saledatet" placeholder="dd/mm/yyyy" style="width:142px; height: 48px; box-shadow: none;">
                                    </th>
                                </tr>

                                <tr>
                                    <th style="width: 10px;"></th>
                                    <th style="width: 50px;">
                                        Delivery date :
                                    </th>
                                    <th style="width: 20px;">
                                        <input value="" class="form-control" type="text" id="delidatef" name="delidatef" placeholder="dd/mm/yyyy" style="width:142px; height: 48px; box-shadow: none;" required>
                                    </th>
                                    <th style="width: 20px; text-align:center;"> - </th>
                                    <th style="width: 20px;">
                                        <input value="" class="form-control" type="text" id="delidatet" name="delidatet" placeholder="dd/mm/yyyy" style="width:142px; height: 48px; box-shadow: none;">
                                    </th>
                                </tr>

                                <tr style="border-top: 2pt solid lightgray; border-left: 2pt solid lightgray; border-right: 2pt solid lightgray;">
                                    <th style="width: 10px;"></th>
                                    <th style="width: 50px;">
                                        Sales Order no. :
                                    </th>
                                    <th style="width: 20px;">
                                        <input style="width:95%;" type="text" id="saleorderf" name="saleorderf" value="${sale}">
                                    </th>
                                    <th style="width: 20px; text-align:center;"> - </th>
                                    <th style="width: 20px;">
                                        <input style="width:95%;" type="text" id="saleordert" name="saleordert" value="${sale}">
                                    </th>
                                </tr>
                                <tr style="border-bottom: 2pt solid lightgray; border-left: 2pt solid lightgray; border-right: 2pt solid lightgray;">
                                    <th style="width: 10px;"></th>
                                    <th style="width: 50px;">
                                        Sales Order List :
                                    </th>
                                    <th colspan="3" style="">
                                        <input name="labelte" id="labelte" hidden>
                                        <div class="form-group">
                                            <textarea style="width:89%; margin:0; padding: 0;" class="form-control" rows="4" cols="10" name="saleOrdList" id="saleOrdList" onchange="$('#labelte').val(this.value)"></textarea>
                                            <!--<input style="width:89%;" type="text" id="salelist" name="salelist" maxlength="330" hidden>--> 
                                            <label style="font-size:12px;">*ไม่เกิน 30 Order</label>
                                        </div>
                                    </th>
                                </tr>
                                <tr>
                                    <th style="width: 10px;"></th>
                                    <th style="width: 50px;"></th>
                                    <th colspan="3"><label style="color:red; font-size: 25px; float: right; position: relative; left:1em; top:-1em;">*</label></th>
                                </tr>
                                <tr  style="border-top: 2pt solid lightgray; border-left: 2pt solid lightgray; border-right: 2pt solid lightgray;">
                                    <th style="width: 10px;"></th>
                                    <th style="width: 50px;">
                                        Purchasing Group. :
                                    </th>
                                    <th style="width: 20px;">
                                        <input style="width:95%;" type="text" id="procurf" name="procurf" value="${procurf}" oninput="this.value = this.value.toUpperCase()" >
                                    </th>
                                    <th style="width: 20px; text-align:center;"> - </th>
                                    <th style="width: 20px;">
                                        <input style="width:95%;" type="text" id="procurt" name="procurt" value="${procurt}" oninput="this.value = this.value.toUpperCase()">
                                    </th>
                                </tr>
                                <tr style="border-bottom: 2pt solid lightgray; border-left: 2pt solid lightgray; border-right: 2pt solid lightgray;">
                                    <th style="width: 10px;"></th>
                                    <th style="width: 50px;">
                                        Purchasing List :
                                    </th>
                                    <th colspan="3" style="">
                                        <input name="labelpro" id="labelpro" hidden>
                                        <div class="form-group">
                                            <textarea style="width:89%; margin:0; padding: 0;" class="form-control" rows="4" cols="10" name="proCurList" id="proCurList" oninput="this.value = this.value.toUpperCase()" onchange="$('#labelpro').val(this.value)" ></textarea>
                                        </div>

                                    </th>
                                </tr>
                                <tr style="height:10px;">
                                    <th style="width: 10px;"></th>
                                    <th style="width: 50px;"></th>
                                    <th colspan="3">
                                        <!--<label style="color:red; font-size: 25px; float: right; position: relative; left:1em; top:-1em;">*</label>-->
                                    </th>
                                </tr>
                                <tr>
                                    <th style="width: 10px;"></th>
                                    <th style="width: 50px;">
                                        Create By :
                                    </th>
                                    <th style="width: 20px;">
                                        <input type="text" id="createbf" name="createbf" value="${create}" oninput="this.value = this.value.toUpperCase()">
                                    </th>
                                    <th style="width: 20px; text-align:center;"> - </th>
                                    <th style="width: 20px;">
                                        <input type="text" id="createbt" name="createbt" value="${create}" oninput="this.value = this.value.toUpperCase()">
                                    </th>
                                </tr>
                                <tr style="height:10px;"></tr>
                                <tr>
                                    <th style="width: 10px;"></th>
                                    <th style="width: 50px;">
                                        <!--Create By :-->
                                    </th>
                                    <th style="width: 20px; text-align:center;">
                                        <div class="col-xs-12" align="left">
                                            <div class="row">
                                                <div class="col-xs-1" align="left"></div>
                                                <div class="col-xs-5" align="left">
                                                    <label class="form-check-label" style="font-size: 17px;">เซตรุ่น
                                                        <input type="radio" checked="checked" name="radio" id="setroon" value="เซตรุ่น">
                                                        <span class="checkmark"></span>
                                                    </label>
                                                </div>
                                                <div class="col-xs-5" align="left">
                                                    <label class="form-check-label" style="font-size: 17px;">ขายเพิ่ม
                                                        <input type="radio" name="radio" id="kaiperm" value="ขายเพิ่ม">
                                                        <span class="checkmark"></span>
                                                    </label>
                                                </div>
                                                <div class="col-xs-1" align="left"></div>
                                            </div>
                                        </div>
                                    </th>
                                    <th style="width: 20px; text-align:center;"></th>
                                    <th style="width: 20px;"></th>
                                </tr>

                            </table>
                    </center>
                    <br>
                    <center>
                        <!--<input type="button" style="width: 100px;" onclick="sendMailFunc()" value="SendMail">-->

                        <input style="width: 100px;" type="button" class="btn btn-success" id="confbtn" value="Confirm" onclick="checkBF()"/>
                        <!--<a onclick="window.location = '/TABLE2/ISM800/display?uid=' + sessionStorage.getItem('uid');"><button style="width: 100px;" class="btn btn-danger"  >Cancel</button></a>-->
                        <!--<button style="width: 100px;" class="btn btn-success"  onclick="checkBF()">Confirm</button>-->
                    </center>
                    <br>
                    </div>

                    <center>
                        <div id="loadIcon" class="modal fade" role="dialog">
                            <div class="modal-dialog modal-sm">
                                <div class="modal-md col-md-6" style="top: 150px; left:300px; background-color: #ddd; border: 1px solid #ccc; border-radius: 4px;">
                                    <div class="modal-header">
                                        <h3>
                                            Loading...
                                            <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
                                        </h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- devbanban.com -->
                    </center>

                    <!--                    <center>
                                            <div id="" class="hidden">
                                                <h3>
                                                    Loading...
                                                    <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
                                                </h3>
                                            </div>
                                        </center>-->

                    <center>
                        <div id="myModal" class="modal fade" role="dialog">
                            <div class="modal-dialog modal-sm"> <!-- ???????????? modal ?????????????? ???? xs, sm, md, lg -->
                                <div class="modal-md col-md-6" style="top: 150px; left:300px; background-color: #ddd; border: 1px solid #ccc; border-radius: 4px;">
                                    <div class="modal-header">
                                        <h4 class="modal-title" align="center">Found New Sales Detail Need To Sales More ?</h4>
                                        <br>
                                        <button name="ok" type="button" onclick="$('#frmsub').submit();">
                                            <font color = "white">YES</font>
                                        </button>
                                        <button name="Cancel" type="button" onclick="againtwo()" data-dismiss="modal">
                                            <font color = "white">NO</font>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- devbanban.com -->
                    </center>

                    <center>
                        <!-- Modal -->
                        <div class="modal fade" id="myModal_just_one" role="dialog">
                            <div class="modal-dialog" style="width:35%;">
                                <center>
                                    <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Please Choose Only One Method</h4>
                                        </div>
                                        <div class="modal-body">
                                            <!--                                            <h4>Requested Date : <input type="date" id="reqDate"></h4>-->
                                        </div>
                                        <div class="modal-footer">
                                            <center>
                                                <button type="button" class="btn btn-info" data-dismiss="modal">OK</button>
                                            </center>
                                        </div>
                                    </div>
                                </center>
                            </div>
                        </div>
                    </center>

                    <center>
                        <!-- Modal -->
                        <div class="modal fade" id="myModal_human_error_deli" role="dialog">
                            <div class="modal-dialog" style="width:35%;">
                                <center>
                                    <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Please Input Delivery Date From Before To.</h4>
                                        </div>
                                        <div class="modal-body"></div>

                                        <div class="modal-footer">
                                            <center>
                                                <button type="button" class="btn btn-info" data-dismiss="modal">OK</button>
                                            </center>
                                        </div>
                                    </div>
                                </center>
                            </div>
                        </div>
                    </center>

                    <center>
                        <!-- Modal -->
                        <div class="modal fade" id="myModal_human_error_sales" role="dialog">
                            <div class="modal-dialog" style="width:35%;">
                                <center>
                                    <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Please Input Sales Order Date From Before To.</h4>
                                        </div>
                                        <div class="modal-body"></div>

                                        <div class="modal-footer">
                                            <center>
                                                <button type="button" class="btn btn-info" data-dismiss="modal">OK</button>
                                            </center>
                                        </div>
                                    </div>
                                </center>
                            </div>
                        </div>
                    </center>

                    <center>
                        <!-- Modal -->
                        <div class="modal fade" id="myModal_human_error_sales_no" role="dialog">
                            <div class="modal-dialog" style="width:35%;">
                                <center>
                                    <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Please Input Sales Order No From Before To.</h4>
                                        </div>
                                        <div class="modal-body"></div>

                                        <div class="modal-footer">
                                            <center>
                                                <button type="button" class="btn btn-info" data-dismiss="modal">OK</button>
                                            </center>
                                        </div>
                                    </div>
                                </center>
                            </div>
                        </div>
                    </center>

                    <center>
                        <!-- Modal -->
                        <div class="modal fade" id="myModal_human_error_pur" role="dialog">
                            <div class="modal-dialog" style="width:35%;">
                                <center>
                                    <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Please Input Purchasing From Before To.</h4>
                                        </div>
                                        <div class="modal-body"></div>

                                        <div class="modal-footer">
                                            <center>
                                                <button type="button" class="btn btn-info" data-dismiss="modal">OK</button>
                                            </center>
                                        </div>
                                    </div>
                                </center>
                            </div>
                        </div>
                    </center>

                    <center>
                        <!-- Modal -->
                        <div class="modal fade" id="myModal_human_error_createby" role="dialog">
                            <div class="modal-dialog" style="width:35%;">
                                <center>
                                    <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Please Input Create By From Before To.</h4>
                                        </div>
                                        <div class="modal-body"></div>

                                        <div class="modal-footer">
                                            <center>
                                                <button type="button" class="btn btn-info" data-dismiss="modal">OK</button>
                                            </center>
                                        </div>
                                    </div>
                                </center>
                            </div>
                        </div>
                    </center>

                    <center>
                        <div id="myModal-error" class="modal fade" role="dialog">
                            <div class="modal-dialog modal-sm" style="height: 100%;"> <!-- ???????????? modal ?????????????? ???? xs, sm, md, lg -->
                                <div class="modal-md col-md-6" style="top: 50px; left:300px; background-color: #ffffff; border: 1px solid #ccc; border-radius: 4px;">
                                    <div class="modal-header">
                                        <span class="close" data-dismiss="modal">&times;</span>
                                        <h4 class="modal-title" align="center">${headModal}</h4>
                                        <h4 class="modal-title" align="center">${headModalDet}</h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- devbanban.com -->
                    </center>

                    <center>
                        <!-- Modal -->
                        <div class="modal fade" id="myModal_alert_list" role="dialog">
                            <div class="modal-dialog" style="width:35%;">
                                <center>
                                    <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Result</h4>
                                        </div>
                                        <div class="modal-body">
                                            <h4 class="modal-title">Complete</h4>

                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <label>
                                                        ${listtrue}
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-12" align="left">
                                                    <label>
                                                        ${truetotal}
                                                    </label>
                                                </div>
                                            </div>

                                            <h4 class="modal-title">Incomplete</h4>

                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <label>
                                                        ${listfalse}
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-12" align="left">
                                                    <label>
                                                        ${falsetotal}
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <center>
                                                <button type="button" class="btn btn-info" data-dismiss="modal">OK</button>
                                            </center>
                                        </div>
                                    </div>
                                </center>
                            </div>
                        </div>
                    </center>


                    <center>
                        <!-- Modal -->
                        <div class="modal_error fade" id="myModal_po_error" role="dialog">
                            <div class="modal-dialog">
                                <center>
                                    <!-- Modal content-->
                                    <div class="modal-content_error">
                                        <div class="modal-header" style="width:345px; height: 50px; padding: 0px;">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Please Check PO number Format</h4>
                                        </div>
                                        <!--<div class="modal-body"></div>-->

                                        <div class="modal-footer">
                                            <center>
                                                <button type="button" class="btn btn-info" data-dismiss="modal">OK</button>
                                            </center>
                                        </div>
                                    </div>
                                </center>
                            </div>
                        </div>
                    </center>

                </form>
                <!--End Part 3-->
                <!--</div>  end #wrapper-top -->
            </div> <!-- end .container-fluid -->
        </div> <!-- end #wrapper -->


    </body>

</html>