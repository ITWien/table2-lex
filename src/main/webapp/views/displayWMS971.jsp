<%@ include file="../fragments/taglibs.jsp" %>
<%@ include file="../fragments/imports.jsp" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.sql.*;" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>WMS</title>
        <!-- css :: vendors -->
        <jsp:include page = "../fragments/css.jsp" />
        <!-- additional custom :: my-style -->
        <link rel="stylesheet" href="../resources/assets/styles/myStyles.css">

        <!-- js :: vendors -->
        <jsp:include page = "../fragments/script.jsp" />
        <!-- additional custom :: my-script -->
        <script src="../resources/assets/scripts/myScripts.js" async></script>
        <script src="../resources/assets/scripts/toggleLoad.js" async></script> 
        <style>
            input[type=text], select{
                width: 100%;
                padding: 5px 5px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }

            input[type=date], input[type=number]{
                /*width: 100%;*/
                padding: 5px 5px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }

            input[type=submit] {
                width: 100%;
                background-color: #00399b;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=submit]:hover {
                background-color: #3973d6;
            }

            input[type=button] {
                width: 100%;
                background-color: #ef604a;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=button]:hover {
                background-color: #e51e00;
            }
        </style>
        <style>
            body {font-family: Arial, Helvetica, sans-serif;}

            /* The Modal (background) */
            .modal {
                display: none; /* Hidden by default */
                position: fixed; /* Stay in place */
                z-index: 1; /* Sit on top */
                padding-top: 100px; /* Location of the box */
                left: 0;
                top: 0;
                width: 100%; /* Full width */
                height: 100%; /* Full height */
                overflow: auto; /* Enable scroll if needed */
                background-color: rgb(0,0,0); /* Fallback color */
                background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
            }

            /* Modal Content */
            .modal-content {
                background-color: #fefefe;
                margin: auto;
                padding: 20px;
                border: 1px solid #888;
                width: fit-content;
                height: fit-content;
            }

            /* The Close Button */
            .close {
                color: #aaaaaa;
                float: right;
                font-size: 28px;
                font-weight: bold;
            }

            .close:hover,
            .close:focus {
                color: #000;
                text-decoration: none;
                cursor: pointer;
            }
        </style>
        <script>
            function ValidForm() {
                var code = document.getElementById('code').value;
                var plant = document.getElementById('plant').value;

                if (code !== '' && plant !== '') {
                    if (code.length >= 8) {
                        document.getElementById('frm').submit();
                        document.getElementById('loader').style.display = 'block';
                    }
                }
            }
        </script>
        <style>
            #loader {
                border: 16px solid #f3f3f3;
                border-radius: 100%;
                border-top: 16px solid #3498db;
                width: 240px;
                height: 240px;
                -webkit-animation: spin 2s linear infinite;
                animation: spin 2s linear infinite;
                margin: auto;
                /*margin-left:1200px;*/
                margin-top: 10px;
                margin-bottom: 10px;
            }   

            .panel-default {
                border-color: transparent;
                background-color: transparent;
            }

            @-webkit-keyframes spin {
                0% { -webkit-transform: rotate(0deg); }
                100% { -webkit-transform: rotate(360deg); }
            }

            @keyframes spin {
                0% { transform: rotate(0deg); }
                100% { transform: rotate(360deg); }
            }

        </style>
        <script>
            $(document).ready(function () {
                $('#showTable').DataTable({
                    "lengthMenu": [[-1, 10, 25, 50, 100], ["All", 10, 25, 50, 100]],
                    "bSortClasses": false,
                    "ordering": false
                });

                // Setup - add a text input to each footer cell
                $('#showTable tfoot.fff th').not(":eq(1)").not(":eq(1)")
                        .not(":eq(1)").not(":eq(1)").not(":eq(2)")
                        .not(":eq(2)").not(":eq(2)").not(":eq(2)").not(":eq(2)").each(function () {
                    var title = $(this).text();
                    $(this).html('<input type="text" />');
                });

                // DataTable
                var table = $('#showTable').DataTable();

                // Apply the search
                table.columns().every(function () {
                    var that = this;

                    $('input', this.footer()).on('keyup change', function () {
                        if (that.search() !== this.value) {
                            that.search(this.value).draw();
                            calSUM();
                        }
                    });
                });

                calSUM();

            });

            function calSUM() {
                var qty = $('input[name=qty]');
                var box = $('input[name=box]');
                var bag = $('input[name=bag]');
                var roll = $('input[name=roll]');
                var sumQty = 0.000;
                var sumBox = 0;
                var sumBag = 0;
                var sumRoll = 0;
                for (var i = 0; i < qty.length; i++) {
                    sumQty += parseFloat($(qty[i]).val().toString().replace(/,/g, ""));
                    sumBox += parseInt($(box[i]).val().toString().replace(/,/g, ""));
                    sumBag += parseInt($(bag[i]).val().toString().replace(/,/g, ""));
                    sumRoll += parseInt($(roll[i]).val().toString().replace(/,/g, ""));
                }
                $('#sumQty').html(currencyFormat(sumQty, 3));
                $('#sumBox').html(currencyFormat(sumBox, 0));
                $('#sumBag').html(currencyFormat(sumBag, 0));
                $('#sumRoll').html(currencyFormat(sumRoll, 0));

                $('.codeGrp').on('mouseover', function () {
                    $(this).css('opacity', '1');
                }).on('mouseleave', function () {
                    $(this).css('opacity', '0');
                });
            }

            function currencyFormat(num, fp) {
                return num.toFixed(fp).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
            }
        </script>
    </head>    
    <body onload="document.getElementById('userid').value = sessionStorage.getItem('uid');">
        <div id="wrapper">

            <!-- /#sidebar-wrapper -->
            <%@ include file="../fragments/sidebar.jsp" %>
            <!--test-->

            <!-- Page Content -->
            <!-- nav-head-custom -->
            <%@ include file="../fragments/nav_head.jsp" %>

            <div class="container-fluid">
                <!--<div id="wrapper-top">-->
                <%-- PART 2 --%>
                <!--                    <div class="row">
                                        <div class="col-lg-12">
                                            <div id="set-height" style="height:415px;margin-top:0px;">
                                                <div id="sidebar-wrapper-top" class="">
                                                    <b class="page-header" style="padding-left:5px;font-size:18px;">
                <%-- <hr style="margin:0px;margin-top:10px;margin-bottom:10px;"> --%>
            </b>
        </div>
    </div>
</div>
</div> -->
                <%-- PART 3 --%>
                <!--show Qdest table-->
                <form action="../resources/manual/WMS971.pdf" target="_blank">
                    <table width="100%">
                        <tr>
                            <td width="94%"></td>
                            <td width="6%" align="right"><input type="submit" value="Manual" /></td>
                        </tr>
                    </table>
                </form>
                <form id="matMaster" action="" target="_blank">
                    <input type="hidden" id="userid" name="uid">
                    <input type="hidden" id="action" name="action" value="findQRMasterByQRId">
                    <input type="hidden" id="id" name="id">
                </form>
                <div style="border: 2px solid #ccc; border-radius: 5px; padding: 20px;">
                    <form id="frm" name="frm" action="display" method="post">
                        <table width="100%">
                            <tr>
                                <td width="20%"><b style="color: #00399b;">Material Code : </b>
                                    <input type="text" name="code" id="code" style=" height: 30px; width: 60%;" value="${code}" autofocus>
                                    <script>
                                        var input = document.getElementById("code");
                                        input.addEventListener("keyup", function (event) {
                                            input.value = input.value.toUpperCase();
                                            if (event.keyCode === 13) {
                                                event.preventDefault();
                                                ValidForm();
                                            }
                                        });
                                    </script> 
                                </td>
                                <td width="20%">
                                    <b style="color: #00399b;">Plant : </b>
                                    <select id="plant" name="plant" style=" width: 60%;" onchange="ValidForm();">
                                        <option value="${plant}" selected hidden>${plant}</option>
                                        <option value="1000">1000</option>
                                        <option value="1050">1050</option>
                                        <option value="9000">9000</option>
                                    </select>
                                </td>
                                <td width="25%">
                                </td>
                                <td></td>
                                <td width="5%"></td>
                                <td width="5%"></td>
                                <td width="20%">
                                </td>
                            </tr>
                        </table>
                    </form>
                    <br>
                    <table id="showTable" class="display" style="width:100%; border: 1px solid #ccc; border-radius: 4px;">
                        <thead>
                            <tr>
                                <th>Material Code</th>
                                <th>Description</th>
                                <th>Group Description</th>
                                <th>Plant</th>
                                <th>V.T</th>
                                <th>QR Code</th>
                                <th>Quantity</th>
                                <th>Std. Unit</th>
                                <th>Box</th>
                                <th>Bag</th>
                                <th>Roll</th>
                                <th>Location</th>
                                <th>Loc Upd Dt</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tfoot class="fff">
                            <tr>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                            </tr>
                        </tfoot>
                        <tbody>
                            <c:forEach items="${headList}" var="p" varStatus="i">
                                <tr>
                                    <td>${p.CODE}</td>
                                    <td>${p.DESC}</td>
                                    <td>${p.GRPDESC}</td>
                                    <td style="text-align: center;">${p.PLANT}</td>
                                    <td style="text-align: center;">${p.VAL}</td>
                                    <td>${p.ID}</td>
                                    <td style="text-align: right;">${p.QTY}&nbsp;&nbsp;<input type="hidden" name="qty" value="${p.QTY}"></td>
                                    <td style="text-align: center;">${p.UNIT}</td>
                                    <td style="text-align: center;">${p.BOX}<input type="hidden" name="box" value="${p.BOX}"></td>
                                    <td style="text-align: center;">${p.BAG}<input type="hidden" name="bag" value="${p.BAG}"></td>
                                    <td style="text-align: center;">${p.ROLL}<input type="hidden" name="roll" value="${p.ROLL}"></td>
                                    <td>${p.LOCATION}</td>
                                    <td>${p.LCUPDT}</td>
                                    <td>${p.STS}</td>
                                </tr>
                            </c:forEach>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>Total</th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th>Quantity</th>
                                <th style="text-align: right;"><p id="sumQty">0</p></th>
                                <th></th>
                                <th style="text-align: center;"><p id="sumBox">0</p></th>
                                <th style="text-align: center;"><p id="sumBag">0</p></th>
                                <th style="text-align: center;"><p id="sumRoll">0</p></th>
                                <th></th>
                                <th></th>
                                <th></th>
                            </tr>
                        </tfoot>
                        <thead>
                            <tr>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th>Box</th>
                                <th>Bag</th>
                                <th>Roll</th>
                                <th></th>
                                <th></th>
                                <th></th>
                            </tr>
                        </thead>
                    </table>
                    <!--**********************-->
                    <!--End Part 3-->
                    <!--</div>  end #wrapper-top--> 
                </div> 
                <br><br>
            </div> <!-- end .container-fluid -->
        </div> <!-- end #wrapper -->
    </body>
</html>