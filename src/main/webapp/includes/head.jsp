<%@ page contentType="text/html;charset=UTF-8"%>

<head>
    <meta charset="UTF-8">
    <!-- <meta http-equiv="Content-Type" contentType="text/html;charset=windows-874" > -->
    <!-- <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">-->

    <link rel="icon" type="image/gif/png" href="../images/icons/title_icon.png">
    <title>ISMS</title>

    <script type="text/javascript" src="../js/jquery.min.js"></script>
    <script type="text/javascript" src="../js/moment.js"></script>
    <script type="text/javascript" src="../js/bootstrap.min.js"></script>
    <script type="text/javascript" src="../js/bootstrap-datetimepicker.min.js"></script>
    <script type="text/javascript" src="../js/metisMenu.min.js"></script>
    <script type="text/javascript" src="../js/sb-admin-2.js"></script>
    <script type="text/javascript" src="../js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="../js/dataTables.bootstrap.min.js"></script>
    <script type="text/javascript" src="../js/myScripts.js"></script>
    <script type="text/javascript" src="../js/alertify.min.js"></script>
    <script type="text/javascript" src="../js/center-loader.js"></script>
    <script type="text/javascript" src="../js/jquery.form.js"></script>


    <link rel="stylesheet" href="../css/bootstrap.min.css" />
    <link rel="stylesheet" href="../css/bootstrap-datetimepicker.min.css" />
    <link rel="stylesheet" href="../css/sb-admin-2.css" />
    <link rel="stylesheet" href="../css/font-awesome.min.css" type="text/css" />
    <link rel="stylesheet" href="../css/simple-sidebar.css" />
    <link rel="stylesheet" href="../css/dataTables.bootstrap.css" />

    <link rel="stylesheet" href="../css/alertify.default.css" />
    <link rel="stylesheet" href="../css/alertify.core.css" />
    <link rel="stylesheet" href="../css/alertify.bootstrap.css" />

    <style type="text/css">
        #sidebar-wrapper {
            background-color: #f8f8f8;
            border-right: 1px solid #e5e5e5;
        }

        body {
            background-color: #FFFFFF;
            font-size: 15px;
            min-width: 1024px;
        }

        .head-menu {
            margin-bottom: 11px;
            margin-top: 11px;
            color: #777;
            font-weight: bold;
        }

        .head-custom {
            margin: 0px;
            color: #2E3E92;
            font-weight: bold;
        }

        .modal-title {
            font-size: 20px;
            color: #2E3E92;
            font-weight: bold;
        }

        .modal .modal-dialog {
            width: 65%;
            min-width: 700px;
        }

        .modal .modal-dialog.smaller {
            width: 30%;
            min-width: 250px;
        }

        .custom-toggle-menu {
            margin: 0px;
            margin-right: 5px;
        }

        .nav-head-custom {
            height: 42px;
            width: 100%;
            background-color: #f8f8f8;
            border-bottom: 1px solid #e5e5e5;
        }

        .page-header {
            color: #226097;
            font-weight: bold;
        }

        .icon-size-sm {
            font-size: 16px;
        }

        .icon-size-lg {
            font-size: 20px;
        }

        .btn-size-sm {
            padding-top: 7px;
            padding-bottom: 3px;
        }

        .custom-body {
            border-radius: 10px;
            background-color: #F7F7F7;
            width: 320px;
            padding: 10px;
            height: 40px;
            border-width: 1.8px;
            border-style: dashed;
            border-color: #C1C1C1;
        }

        .custom-icon-w-b i {
            color: white;
        }

        .custom-text {
            color: #777;
        }

        tfoot input {
            width: 100%;
            padding: 3px;
            box-sizing: border-box;
        }

        thead,
        tfoot {
            display: table-header-group;
        }

        .dataTables_filter {
            display: none;
        }
        /*.table-striped>tbody>tr:nth-child(odd)>td,
.table-striped>tbody>tr:nth-child(odd)>th {
  background-color: #ECF4FB;
}
.table tbody tr:hover td,
.table tbody tr:hover th,
.table-stripe body tr:hover td {
  background-color: #D8E5F6;
}
.sidebar-nav li a {
  display: block;
  text-decoration: none;
  color: #CCDEFF;
}
.navbar-default .nav li a:hover,
.navbar-default .nav li a:focus {
    color: #0F1D5F;
    background-color: #D8E5F6;
}

.navbar-default .nav li .active  {
  color: #3D53B9;
  background-color: #ECF4FB;
}*/
    </style>
</head>