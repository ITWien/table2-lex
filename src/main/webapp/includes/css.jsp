<!-- icon -->
<link rel="icon" type="image/gif/png" href="../images/icons/title_icon.png">

<!-- font :: Awesome -->
<link rel="stylesheet" href="../css/font-awesome.min.css" type="text/css" />

<!-- bootstrap -->
<link rel="stylesheet" href="../js/vendors/bootstrap/dist/css/bootstrap.min.css">

<!-- bootstrap date-time-picker -->
<link rel="stylesheet" href="../css/bootstrap-datetimepicker.min.css" />

<!-- theme :: sb-admin-2 -->
<link rel="stylesheet" href="../css/sb-admin-2.css" />

<!-- theme :: simple-sidebar -->
<link rel="stylesheet" href="../css/simple-sidebar.css" />

<!-- custom :: my-style -->
<link rel="stylesheet" href="../css/assets/myStyles.css">

<!-- dataTables-bootstrap -->
<link rel="stylesheet" href="../js/vendors/angular-datatables-master/dist/plugins/bootstrap/datatables.bootstrap.min.css">


<!-- alertify -->
  <link rel="stylesheet" href="../css/alertify.default.css" />
  <link rel="stylesheet" href="../css/alertify.core.css" />
  <link rel="stylesheet" href="../css/alertify.bootstrap.css" />