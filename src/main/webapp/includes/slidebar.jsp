<div id="sidebar-wrapper">
    <div class="navbar-default sidebar" role="navigation" style="margin: 0">
        <ul class="nav navbar-default" id="side-menu">
            <li>
                <h4 class="head-menu">
                    <table style="width:100%;">
                        <tr>
                            <td align="center">Materials Shipment System</td>
                        </tr>
                    </table>
                </h4>
            </li>
            <li>
                <a href="/MSSX/STATUS/DP"><i class="fa fa-info-circle fa-fw"></i> Status</a>
            </li>
            <li>
                <a href="/MSSX/MSS500/DP" target="_blank"><i class="fa fa-info-circle fa-fw"></i> System Status</a>
            </li>
            <li>
                <a href="#"><i class="fa fa-institution fa-fw"></i> Stock RM <span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li><a href="/MSSX/MSS001/FU"><i class="fa fa-upload fa-fw"></i> File Upload</a></li>
                    <li><a href="/MSSX/MSS002/DP"><i class="fa fa-sort-numeric-asc fa-fw"></i> Display on hand</a></li>
                    <li><a href="/MSSX/MSS100/MC"><i class="fa fa-list-alt fa-fw"></i> Materials Control</a></li>
                    <li><a href="/MSSX/MSS101/AP"><i class="fa fa-edit fa-fw"></i> Approved</a></li>
                    <li><a href="http://10.11.9.142:8080/MSS3/MSS310/Display"><i class="fa fa-edit fa-fw"></i> Sum
                            Packed</a></li>
                    <li>
                        <a href="/MSSX/REPORT_S/"><i class="fa fa-copy fa-fw"></i> Reports <span
                                class="fa arrow"></span></a>
                        <ul class="nav nav-third-level">
                            <li style="padding-left:5px"><a href="/MSSX/REPORT_S/PAGE?id=105"><i
                                        class="fa fa-file-pdf-o fa-fw"></i> MSS105</a></li>
                            <li style="padding-left:5px"><a href="/MSSX/REPORT_S/PAGE?id=106"><i
                                        class="fa fa-file-pdf-o fa-fw"></i> MSS106</a></li>
                            <li style="padding-left:5px"><a href="/MSSX/REPORT_S/PAGE?id=107"><i
                                        class="fa fa-file-pdf-o fa-fw"></i> MSS107</a></li>
                            <li style="padding-left:5px"><a href="/MSSX/REPORT_S/PAGE?id=108"><i
                                        class="fa fa-file-pdf-o fa-fw"></i> MSS108</a></li>
                            <li style="padding-left:5px"><a href="/MSSX/REPORT_L/PAGE?id=216"><i
                                        class="fa fa-file-pdf-o fa-fw"></i> MSS216</a></li>
                            <li role="separator" class="divider"></li>
                        </ul>
                    </li>
                </ul>
            </li>
            <li>
                <a href="#"><i class="fa fa-truck fa-fw"></i> Logistics <span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li><a href="/MSSX/MSS201/RE"><i class="fa fa-edit fa-fw"></i> Received</a></li>
                    <li><a href="/MSSX/MSS202/PR"><i class="fa fa-tag fa-fw"></i> Print Tags</a></li>
                    <li><a href="/MSSX/MSS203/LG"><i class="fa fa-cubes fa-fw"></i> Shipping</a></li>
                    <li><a href="/MSSX/REPORT_L/PAGE?id=205"><i class="fa fa-file-pdf-o fa-fw"></i> MSS205</a></li>
                    <li><a href="/MSSX/REPORT_L/PAGE?id=206"><i class="fa fa-file-pdf-o fa-fw"></i> MSS206</a></li>
                    <li><a href="/MSSX/REPORT_L/PAGE?id=209"><i class="fa fa-file-pdf-o fa-fw"></i> MSS209</a></li>
                    <li><a href="/MSSX/REPORT_L/PAGE?id=210"><i class="fa fa-file-pdf-o fa-fw"></i> MSS210</a></li>
                </ul>
            </li>
            <li>
                <a href="/MSSX/MSS300/DISPLAY"><i class="fa fa-list fa-fw"></i> Transaction</a>
            </li>
        </ul>
    </div>
</div>