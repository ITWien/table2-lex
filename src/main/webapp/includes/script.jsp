<!-- moment.js -->
<script src="../js/vendors/moment/moment.js"></script>

<!-- Underscore.js -->
<script src="../js/vendors/underscore/underscore-min.js"></script>

<!-- jQuery -->
<script src="../js/vendors/jquery/dist/jquery.min.js"></script>

<!-- Angular -->
<script src="../js/vendors/angular/angular.min.js"></script>
<!-- ngDirective :: additional -->
	<!-- <script src="../js/vendors/alertify/ngAlertify.js"></script> -->
	<script src="../js/vendors/angular-datatables-master/vendor/angular-resource/angular-resource.min.js"></script>

<!-- bootstrap -->
<script type="text/javascript" src="../js/vendors/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- bootstrap-datetimepicker -->
<script type="text/javascript" src="../js/bootstrap-datetimepicker.min.js"></script>

<!-- jQuery-dataTables -->
<script src="../js/vendors/angular-datatables-master/vendor/datatables/media/js/jquery.dataTables.min.js"></script>

<!-- dataTables-bootstrap ver.3 -->
<script src="../js/vendors/angular-datatables-master/vendor/datatables/media/js/dataTables.bootstrap.min.js"></script>

<!-- Angular-DataTables -->
<script src="../js/vendors/angular-datatables-master/dist/angular-datatables.min.js"></script>

<!-- Plugins :: dataTables-column-filter -->
<script src="../js/vendors/angular-datatables-master/vendor/datatables-columnfilter/js/dataTables.columnFilter.js"></script>
<script src="../js/vendors/angular-datatables-master/dist/plugins/columnfilter/angular-datatables.columnfilter.min.js"></script>

<!-- alertify js -->
<script type="text/javascript" src="../js/alertify.min.js"></script>